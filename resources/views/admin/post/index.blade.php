@extends('admin.post.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          <h3 class="box-title">List of Posts</h3>
        </div>
        <div class="col-sm-4">
          @include('admin.partials.flash')
          <a class="btn btn-primary" href="{{route('post.create')}}">Add new Post</a>
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>
      <form method="POST" action="#">
         
      </form>
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <table id="example30" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                <th width="3%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">ID</th>
                <th width="5%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Birthdate: activate to sort column ascending">Image</th>
                <th width="5%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Division: activate to sort column ascending">Title</th>
                <th width="5%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Desc</th>
                <th width="10%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Body</th>
                <th width="3%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Date</th>
                <th width="3%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Feature</th>
                <th width="5%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Views</th>
                <th width="5%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Address: activate to sort column ascending">Category</th>
                <th width="8%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending">Author</th>
                <th width="5%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending">Status</th>
                <th width="5%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending">Is Blog</th>
                <th width="2%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Birthdate: activate to sort column ascending">Order</th>
                
                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="2" aria-label="Action: activate to sort column ascending">Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach($posts as $value)
                <tr data-post-id="{{ $value->id }}" role="row" class="odd">
                  <td class="sorting_1">{{$value->id}}</td>
                  <td>
                    <a href="{{route('blog',$value->slug)}}" target="_blank">
                      <img src="{{ asset('storage/'.$value->image) }}" width="50px" height="50px"/></a>
                  </td>
                  <td class="hidden-xs">{{$value->name}}</td>
                  
                  <td class="hidden-xs">{{substr($value->desc,0,10)}}</td>
                  <td class="hidden-xs">{{substr($value->body,0,10)}}</td>
                  <td class="hidden-xs">{{$value->date}}</td>
                  <td class="hidden-xs">
                      <i class="{{$value->feature == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                   </td>
                  <td class="hidden-xs">{{$value->views}}</td>
                  <td class="hidden-xs">{{$value->mcategory->name}}</td>
                  <td class="hidden-xs">{{$value->author->name}}</td>
                  <td class="hidden-xs">
                    <i class="{{$value->status == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                  </td>
                  <td class="hidden-xs">
                    <i class="{{$value->is_blog == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                  </td>
                  <td>{{$value->order}}</td>
                  
                  <td>
                    <form class="row" method="POST" action="{{route('post.destroy', $value->id)}}" onsubmit = "return confirm('Are you sure?')">
                        {{csrf_field()}}
                        {{ method_field('DELETE') }}
                        <a href="{{route('post.edit', $value->id)}}" class="btn btn-warning col-sm-3 col-xs-5 btn-margin">
                        <i class="fa fa-pencil-square-o large"></i>
                        </a>
                         <button type="submit" class="btn btn-danger col-sm-3 col-xs-5 btn-margin">
                          <i class="fa fa-trash large"></i>
                        </button>
                    </form>
                  </td>
              </tr>
           

               @endforeach           
            </tbody>
           
          </table>
        </div>
      </div>
      {{$posts->links()}}
    </div>
  </div>
  
</div>
    </section>
  </div>
@endsection