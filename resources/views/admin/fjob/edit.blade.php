@extends('admin.fjob.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{route('fjob.update',$fjob->id)}}" enctype="multipart/form-data">
                       {{csrf_field()}}
                       {{method_field('PATCH')}}
                       
                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Title / name *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="name" value="{{ old('name',$fjob->name) }}">
                                <span class="text-danger">{{ $errors->first('name') }}</span>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Expired At *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="expired_at" value="{{ old('expired_at',$fjob->expired_at) }}">
                                <span class="text-danger">{{ $errors->first('expired_at') }}</span>

                            </div>
                        </div>                                    

                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Budget *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="budget" value="{{ old('budget',$fjob->budget) }}">
                                <span class="text-danger">{{ $errors->first('budget') }}</span>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Budget To*</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="budget_to" value="{{ old('budget_to',$fjob->budget_to) }}">
                                <span class="text-danger">{{ $errors->first('budget_to') }}</span>

                            </div>
                        </div>

                        <div class="form-group">
                      <label class="col-md-2 control-label">Duration *</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="duration_id">
                                  @foreach(\App\Models\Freelancers\Duration::all() as $value)
                                    @if ($fjob->duration_id == $value->id)
                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                      @else
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endif
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('duration_id') }}</span>
                            </div>
                        </div>
                    


                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Address *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="address" value="{{ old('address',$fjob->address) }}">
                                <span class="text-danger">{{ $errors->first('address') }}</span>

                            </div>
                        </div>
                        
                        
                  <div class="form-group">
                      <label class="col-md-2 control-label">Category</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="fcategory_id">
                              @foreach($fcategories as $jobcategory)
                                @if ($fjob->category_id == $jobcategory->id)
                                    <option value="{{ $jobcategory->id }}" selected>{{ $jobcategory->name }}</option>
                                      @else
                                    <option value="{{ $jobcategory->id }}">{{ $jobcategory->name }}</option>
                                @endif
                              @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('fcategory_id') }}</span>
                            </div>
                        </div>

                    

                    <div class="form-group">
                      <label class="col-md-2 control-label">User</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="user_id">
                                  <option value="{{ $fjob->user->id }}" selected>{{ $fjob->user->name }}</option>
                                     
                                </select>
                                <span class="text-danger">{{ $errors->first('user_id') }}</span>
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="col-md-2 control-label">Last Date *</label>
                            <div class="col-md-10">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" value="{{ $fjob->endate }}" name="endate" class="form-control pull-right" id="birthDate">
                                    <span class="text-danger">{{ $errors->first('endate') }}</span>
                                </div>
                            </div>
                        </div>


                         <div class="form-group">
                            <label for="post" class="col-md-2 control-label">Body*</label>

                            <div class="col-md-10">
                                <textarea class="form-control" rows="7" id="news" type="text" name="body" value="{{old('body')}}">{{old('body', $fjob->body)}}</textarea>

                                    <span class="text-danger">{{ $errors->first('body') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Skills </label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="skills" value="@foreach($fjob->skillss as $value){{$value->skill}},@endforeach">
                                <span class="text-danger">{{ $errors->first('skills') }}</span>

                            </div>
                        </div>


                    <div class="form-group">
                      <label class="col-md-2 control-label">Job Type</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="jobtype_id">
                                  @foreach($jobtypes as $value)
                                  @if ($fjob->jobtype_id == $value->id)
                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                      @else
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                  @endif
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('jobtype_id') }}</span>
                            </div>
                        </div>

          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="feature" value="0">
                  <input name="feature" type="checkbox" value="1" {{ $fjob->feature == 1 ? 'checked' : '' }}>
                      Feature
                    </label>
                  </div>
              </div>
          </div>


          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="status" value="0">
                  <input name="status" type="checkbox" value="1" {{ $fjob->status == 1 ? 'checked' : '' }}>
                      Status
                    </label>
                  </div>
              </div>
          </div>

                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
