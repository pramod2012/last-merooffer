<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
use App\Models\Attribute;

class AdshowController extends Controller
{

    private $page = "site.";

    public function product_details($slug)
    {
        $product = Product::where('slug', $slug)->firstOrFail();
        $productAttributes = $product->productattributes;
        $id = $product->id;
        $config = array();
        $config['map_height'] = '100%';
        $config['center'] = "$product->latitude,$product->longitude";
        $config['onboundschanged'] = 'if (!centreGot) {
            var mapCentre = map.getCenter();
            marker_0.setOptions({
                position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
            });
        }
        centreGot = true;';

        app('map')->initialize($config);

        // set up the marker ready for positioning
        // once we know the users location
        $marker = array();
        $marker['icon'] = 'https://merooffer.com/frontend/images/icon-services.png';
        $marker['draggable'] = true;
        $marker['ondragend'] = '
        iw_' . app('map')->map_name . '.close();
        reverseGeocode(event.latLng, function(status, result, mark){
            if(status == 200){
                iw_' . app('map')->map_name . '.setContent(result);
                iw_' . app('map')->map_name . '.open(' . app('map')->map_name . ', mark);
            }
        }, this);
        ';
        app('map')->add_marker($marker);

        $map = app('map')->create_map();

        app('db')->table('products')->where('id', $id)->increment('views');
        $product = Product::findOrFail($id);
        $related = Product::where('status', 1)->orderBy('views', 'asc')->get();
        return view($this->page . 'product_details', compact("product", "map", "related", 'productAttributes'));
    }

    public function searchProduct(Request $request)
    {
        $products = $this->filterProductsBasedOnSearchCriteria($request);
        return view($this->page . 'search-product', compact("products"));
    }
    public function filterProductsBasedOnSearchCriteria(Request $request)
    {
        $products = Product::searchable()->sortSearchableProducts()->locationFilter()->where('status', 1)->with(['images', 'adtype', 'user', 'city', 'categories' => function ($q) {
            $q->with('category')->whereNotNull('parent_id');
        }])->where(function ($query) use ($request) {
            $title = $request->input('name');
            $address = $request->input('address');
            $city = $request->input('city_id');
            $category = $request->input('category_id');
            $adtype = $request->input('adtype_id');
            $min_price = $request->input('min_price');
            $max_price = $request->input('max_price');
            $condition = $request->input('condition_id');
            $pricetype = $request->input('pricetype_id');

            if ($request->has('assurance') && $request->assurance == 'true') {
                $query->where('assured_id', '!=', 1);
            }
            if (isset($title)) {
                $query->where('name', 'like', "%$title%");
            }
            if ($request->has('searchText')) {
                $query->where('name', 'like', "%" . $request->searchText . "%")->orWhere('desc', 'like', "%" . $request->searchText . "%");
            }
            if (isset($city)) {
                $query->where('city_id', '=', $city);
            }
            if (isset($condition)) {
                $query->where('condition_id', '=', $condition);
            }
            if (isset($category)) {
                $query->whereHas('categories', function ($q) use ($category) {
                    $q->whereCategoryId($category);
                });
            }
            if (isset($address)) {
                $query->where('address', 'like', "%$address%");
            }
            if (isset($adtype)) {
                $query->where('adtype_id', '=', $adtype);
            }
            if (isset($pricetype)) {
                $query->where('pricetype_id', '=', $pricetype);
            }
            if (isset($min_price) && isset($max_price)) {
                $query->where('price', '>=', $min_price)
                    ->where('price', '<=', $max_price);
            }
            $attributes_dropdown = Attribute::where('type', 'dropdown')->get();

            foreach ($attributes_dropdown as $key => $value) {
                $id = $request->input("$value->code");
                if (isset($id)) {
                    $query->whereHas('attributevalues', function ($q) use ($id, $value) {
                        $q->where('attribute_id', $value->id)->whereAttributevalueId($id);
                    });
                }
            }
            $attributes_radio = Attribute::where('type', 'radio')->get();

            foreach ($attributes_radio as $key => $value) {
                $id = $request->input("$value->code");
                if (isset($id)) {
                    $query->whereHas('attributevalues', function ($q) use ($id, $value) {
                        $q->where('attribute_id', $value->id)->whereAttributevalueId($id);
                    });
                }
            }

            $attributes_text = Attribute::where('type', 'text')->get();

            foreach ($attributes_text as $key => $value) {
                $id = $request->input("$value->code");
                if (isset($id)) {
                    $query->whereHas('attributevalues', function ($q) use ($id, $value) {
                        $q->where('attribute_id', $value->id)->where('value', 'LIKE', "%$id%");
                    });
                }
            }
            $attributes_number = Attribute::where('type', 'number')->where('interval', 0)->get();
            foreach ($attributes_number as $key => $value) {
                $id = $request->input("$value->code");
                if (isset($id)) {
                    $query->whereHas('attributevalues', function ($q) use ($id, $value) {
                        $q->where('attribute_id', $value->id)->where('value', $id);
                    });
                }
            }

            $attributes_interval = Attribute::where('type', 'number')->where('interval', 1)->get();

            foreach ($attributes_interval as $key => $value) {
                $id_min = $request->input("min_$value->code");
                $id_max = $request->input("max_$value->code");
                if (isset($id_min) && isset($id_max)) {
                    $query->whereHas('attributevalues', function ($q) use ($id_min, $id_max, $value) {
                        $q->where('attribute_id', $value->id)->where('value', '>=', $id_min)
                            ->where('attribute_id', $value->id)->where('value', '<=', $id_max);
                    });
                }
            }
        })->paginate(4)->appends($request->all());
        return $products;
    }

    public function contactUs()
    {
        $config = array();
        $config['map_height'] = "100%";
        $config['center'] = "27.6915196, 85.3420486";

        $config['onboundschanged'] = 'if (!centreGot) {
            var mapCentre = map.getCenter();
            marker_0.setOptions({
                position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
            });
        }
        centreGot = true;';

        app('map')->initialize($config); // Initialize Map with custom configuration
        // set up the marker ready for positioning
        $marker = array();
        $marker['draggable'] = true;
        $marker['ondragend'] = '
        iw_' . app('map')->map_name . '.close();
        reverseGeocode(event.latLng, function(status, result, mark){
            if(status == 200){
                iw_' . app('map')->map_name . '.setContent(result);
                iw_' . app('map')->map_name . '.open(' . app('map')->map_name . ', mark);
            }
        }, this);
        ';
        $marker['infowindow_content'] = 'New Baneshwor, Kathmandu 44600';
        app('map')->add_marker($marker);

        @$map = app('map')->create_map();
        return view('site.pages.contact-us', compact('map'));
    }

    public function map()
    {
        $products = Product::where('status', 1)->get();
        $config = array();
        $config['center'] = "27.7172453,85.3239605";
        $config['zoom'] = '10';
        $config['map_height'] = '599px';
        $config['scrollwheel'] = false;
        $config['geocodeCaching'] = true;

        foreach ($products as $value) {
            if ($value->latitude == !null) {

                app('map')->initialize($config);
                $marker['position'] = "$value->latitude,$value->longitude";
                foreach ($value->categories as $cat) {
                    $cat = $cat->name;
                }
                foreach ($value->categories->where('parent_id', null) as $cat) {
                    $icon = $cat->slug . '.png';
                    $cat = $cat->name;
                }
                if ($value->images->count() > 0) {
                    $image = asset('images_small/' . $value->images->first()->full);
                } else {
                    $image = asset('storage/' . config('settings.error_image'));
                }
                $marker['icon'] = 'https://merooffer.com/icon/' . $icon;
                $marker['infowindow_content'] = '<div class="leaflet-popup-content-wrapper"><div class="leaflet-popup-content" style="width: 401px;"><a class="classiera_map_div" href="' . url('/ad', $value->slug) . '"><img class="classiera_map_div__img" src="' . $image . '" alt="images"><div class="classiera_map_div__body"><p class="classiera_map_div__price">Price : <span>Rs.' . number_format($value->price) . '</span></p><h5 class="classiera_map_div__heading">' . $value->name . '</h5><p class="classiera_map_div__cat">Category : ' . $cat . '</p></div></a></div></div>';
                app('map')->add_marker($marker);
            }
        }
        $config['onboundschanged'] = 'if (!centreGot) {
            var mapCentre = map.getCenter();
            marker_0.setOptions({
                position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
            });
        }
        centreGot = true;';

        app('map')->initialize($config);

        // set up the marker ready for positioning
        // once we know the users location
        $marker = array();
        $marker['draggable'] = true;
        $marker['ondragend'] = '
        iw_' . app('map')->map_name . '.close();
        reverseGeocode(event.latLng, function(status, result, mark){
            if(status == 200){
                iw_' . app('map')->map_name . '.setContent(result);
                iw_' . app('map')->map_name . '.open(' . app('map')->map_name . ', mark);
            }
        }, this);
        ';
        app('map')->add_marker($marker);

        $map = app('map')->create_map();

        return view($this->page . 'map', compact("map"));
    }
}
