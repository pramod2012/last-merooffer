@extends('site.partials.main_shop')
@section('title', 'Mero Offer Shop')
@section('content')
<body class="home page-template page-template-template-homepage-v5 page-template-template-homepage-v5-php page page-id-6 logged-in">
@include('site.partials.navbar')
@include('site.partials.search')
<!--search-section-->
<section class="classiera-simple-bg-slider">
	<div class="classiera-simple-bg-slider-content text-center">
		<h1>Mero Offer <span class="color">Pasal</span></h1>
		<h4>Browse by Popular products Categories</h4>
		<div class="category-slider-small-box text-center">
			<ul class="list-inline list-unstyled">
							
	            					<li>
						<a class="match-height" href="https://merooffer.com/menu/lifestyle" style="height: 92px;">
							<i class="zmdi zmdi-female"></i>
					        <p>Lifestyle</p>
						</a>
					</li>
	            					<li>
						<a class="match-height" href="https://merooffer.com/menu/electronics" style="height: 92px;">
							<i class="zmdi zmdi-radio"></i>
					        <p>Electronics</p>
						</a>
					</li>
	            					<li>
						<a class="match-height" href="https://merooffer.com/menu/2-restaurants-cafe" style="height: 92px;">
							<i class="zmdi zmdi-cutlery"></i>
					        <p>Restaurants &amp; Cafe</p>
						</a>
					</li>
							<li>
						<a class="match-height" href="https://merooffer.com/menu/home-furnitures" style="height: 92px;">
							<i class="zmdi zmdi-seat"></i>
					        <p>Home & Furnitures</p>
						</a>
					</li>
	            					<li>
						<a class="match-height" href="https://merooffer.com/menu/musical-instrument-services" style="height: 92px;">
							<i class="zmdi zmdi-audio"></i>
					        <p>Musical Instrument & Services</p>
						</a>
					</li>
	            			</ul><!--list-inline list-unstyled-->
		</div><!--category-slider-small-box-->
	</div><!--classiera-simple-bg-slider-content-->
</section><!--classiera-simple-bg-slider-->

<section class="classiera-premium-ads-v5 border-bottom section-pad-80">
	<div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 center-block">
                    <h3 class="text-capitalize">PREMIUM PRODUCTS</h3>
                    <p>Top Preminum Products of Nepal</p>
                </div>
            </div>
        </div>
    </div>
	<div style="overflow: hidden; padding-top:10px;">
		<div style="margin-bottom: 40px;">			
			<div id="owl-demo" class="owl-carousel premium-carousel-v5" data-car-length="5" data-items="5" data-loop="true" data-nav="false" data-autoplay="true" data-autoplay-timeout="3000" data-dots="false" data-auto-width="false" data-auto-height="true" data-right="false" data-responsive-small="1" data-autoplay-hover="true" data-responsive-medium="2" data-responsive-large="5" data-responsive-xlarge="7" data-margin="10">
				@forelse($featured as $key => $value)
				<div class="classiera-box-div-v5 item match-height">
					<figure>
						<div class="premium-img">
							<div class="featured-tag">
								<span class="left-corner"></span>
								<span class="right-corner"></span>
								<div class="featured">
									<p>Featured</p>
								</div>
							</div><!--featured-tag-->
							@if ($value->images->count() > 0)
                                    <img class="img-responsive" src="{{ asset('images_small/'.$value->images->first()->full) }}" alt="{{$value->name}}">
                                    @else
                                    <img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
                                    @endif	
														<span class="hover-posts">
								<a href="{{ route('product_details', $value->slug) }}">view ad</a>
							</span>
						<span class="price">
								Rs. {{ number_format($value->price) }}</span>
								<span class="classiera-buy-sel">
                            {{@$value->adtype->name}}</span>
													</div><!--premium-img-->
						<figcaption>
							<h5><a href="{{ route('product_details', $value->slug) }}">{{$value->name}}</a></h5>
							<div class="category">
								<span>
					Category : 
					@foreach($value->categories->where('parent_id',!null) as $cat)
                    <a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a> 
                    @endforeach							</div>
						</figcaption>
					</figure>
				</div>
				@empty
				<div class="classiera-box-div-v5 item match-height">
					<figure>
						<div class="premium-img">
							<div class="featured-tag">
								<span class="left-corner"></span>
								<span class="right-corner"></span>
								<div class="featured">
									<p>Featured</p>
								</div>
							</div><!--featured-tag-->
							
                                    <img class="img-responsive" src="{{ asset('frontend/images/empty.png') }}" alt="">
                                    	
														<span class="hover-posts">
								<a href="{{ url('ads-list') }}">Submit Ad</a>
							</span>
						<span class="price">
								Go on, it's Easy</span>
								<span class="classiera-buy-sel">
                            Want to see your stuff here??</span>
													</div><!--premium-img-->
						<figcaption>
							<h5><a href="{{ url('ads-list') }}">Start Selling</a></h5>
							
						</figcaption>
					</figure>
				</div>
				@endforelse
				
															</div>
		</div>
		<div class="navText">
			<a class="prev btn btn-primary radius outline btn-style-five">
				<i class="icon-left zmdi zmdi-arrow-back"></i>
				Previous			</a>
			<a class="next btn btn-primary radius outline btn-style-five">
				Next				<i class="icon-right zmdi zmdi-arrow-forward"></i>
			</a>
		</div>
	</div>
</section>
<section class="classiera-advertisement advertisement-v5 section-pad-80 border-bottom">
	<div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 center-block">
                    <h3 class="text-capitalize">LATEST PRODUCTS</h3>
                    <p>A product section where we have two types of ads listing one with grids and the other one with list view latest products, popular products &amp; random products also featured products will show below.</p>
                </div>
            </div>
        </div>
    </div>
	<div class="tab-divs">
		<div class="view-head">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-sm-7 col-xs-8">
                        <div class="tab-button">
                            <ul class="nav nav-tabs" role="tablist">								
                                <li role="presentation" class="active">
									<a href="#all" aria-controls="all" role="tab" data-toggle="tab">
										Latest Products										<span class="arrow-down"></span>
									</a>
                                </li>
                                <li role="presentation">                                   
									<a href="#popular" aria-controls="popular" role="tab" data-toggle="tab">
										Popular Products										<span class="arrow-down"></span>
									</a>
                                </li>
                                <li role="presentation">                                    
									<a href="#jobs" aria-controls="jobs" role="tab" data-toggle="tab">
										Random Products								<span class="arrow-down"></span>
									</a>
                                </li>
                            </ul><!--nav nav-tabs-->
                        </div><!--tab-button-->
                    </div><!--col-lg-6 col-sm-8-->
					<div class="col-lg-6 col-sm-5 col-xs-4">
						<div class="view-as text-right flip">
							<a id="grid" class="grid active" href="#">
								<i class="fas fa-th-large"></i>
							</a>
							<a id="grid" class="grid-medium" href="#">
								<i class="fa fa-th"></i>
							</a>
							<a id="list" class="list" href="#">
								<i class="fas fa-th-list"></i>
							</a>							
                        </div><!--view-as tab-button-->
					</div><!--col-lg-6 col-sm-4 col-xs-12-->
				</div><!--row-->
			</div><!--container-->
		</div><!--view-head-->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active" id="all">
				<div class="container">
					<div class="row">
					<!--FeaturedAds-->
					@forelse($latest as $key => $value)
					<div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
	<div class="classiera-box-div classiera-box-div-v5">
		<figure class="clearfix">
			<div class="premium-img">
				@if($value->featured == 1)
								<div class="featured-tag">
						<span class="left-corner"></span>
						<span class="right-corner"></span>
						<div class="featured">
							<p>Featured</p>
						</div>
					</div>
					@endif
					@if ($value->images->count() > 0)
					<img class="img-responsive" src="{{ asset('images_small/'.$value->images->first()->full) }}" alt="{{$value->name}}" style="max-height: 100%">
					@else
					<img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
					@endif
									<span class="hover-posts">											
					<a href="{{ route('product_details', $value->slug) }}">View Ad</a>
				</span>
				<span class="price">
						PRICE: Rs.{{ number_format($value->price) }}</span>
						<span class="classiera-buy-sel">{{@$value->adtype->name}}</span>
							</div><!--premium-img-->
			<div class="detail text-center">
								<span class="price">Rs. {{ number_format($value->price) }}</span>
								<div class="box-icon">@if($value->user->show_email == 1)
					<a href="mailto:{{ $value->user->email }}?subject">
						<i class="fas fa-envelope"></i>
					</a>@endif       @if($value->user->show_mobile == 1)
										<a href="tel:{{ $value->user->mobile}}"><i class="fas fa-phone"></i></a>@endif
									</div>
				<a href="{{ route('product_details', $value->slug) }}" class="btn btn-primary outline btn-style-five">View Ad</a>
			</div><!--detail text-center-->
			<figcaption>
								<span class="price visible-xs">
					Rs.{{ number_format($value->price) }}</span>
								<h5><a href="{{ route('product_details',$value->slug) }}">{{$value->name}}</a></h5>
				<div class="category">
					<span>
					Category : 
					@foreach($value->categories->where('parent_id',!null) as $cat)
					<a href="{{route('menu', $cat->category->slug)}}" title="View all posts in {{$cat->category->name}}">{{$cat->category->name}}</a> /
					<a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a>
				@endforeach </span>
										<span>Location : 
						<a href="{{route('findByCity',$value->city->slug)}}" title="View all products in {{$value->city->name}}">{{$value->city->name}}</a>
					</span>
				</div>
				<p class="description">{{substr($value->desc, 0, 340)}}</p>
			</figcaption>
		</figure>
	</div><!--row-->
</div><!--col-lg-4-->
@empty
<div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
	<div class="classiera-box-div classiera-box-div-v5">
		<figure class="clearfix">
			<div class="premium-img">
				
					<img class="img-responsive" src="{{ asset('frontend/images/empty.png') }}" alt="Submit Ads">
									<span class="hover-posts">											
					<a href="{{ url('ads-list') }}">Submit Ad</a>
				</span>
				<span class="price">
						Go On, It's Easy</span>
						<span class="classiera-buy-sel">Want to see your stuff here??</span>
							</div><!--premium-img-->
			<div class="detail text-center">
								<span class="price">Go On, It's Easy</span>
								<div class="box-icon">
									</div>
				<a href="{{ url('ads-list') }}" class="btn btn-primary outline btn-style-five">Submit Ad</a>
			</div><!--detail text-center-->
			<figcaption>
								<span class="price visible-xs">
					Go On, It's Easy</span>
								<h5><a href="{{ url('ads-list') }}">Start Selling</a></h5>
				<div class="category">
					
										
				</div>
				<p class="description">Make some extra cash by selling things in your community. Go on, It's Quick and Easy .</p>
			</figcaption>
		</figure>
	</div><!--row-->
</div><!--col-lg-4-->
@endforelse

					</div><!--row-->
				</div><!--container-->
			</div><!--tabpanel-->
			<div role="tabpanel" class="tab-pane fade" id="jobs">
				<div class="container">
					<div class="row">
						@foreach($old as $key => $value)
					<div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
	<div class="classiera-box-div classiera-box-div-v5">
		<figure class="clearfix">
			<div class="premium-img">
				@if($value->featured == 1)
								<div class="featured-tag">
						<span class="left-corner"></span>
						<span class="right-corner"></span>
						<div class="featured">
							<p>Featured</p>
						</div>
					</div>
					@endif
					@if ($value->images->count() > 0)
					<img class="img-responsive" src="{{ asset('images_small/'.$value->images->first()->full) }}" alt="{{$value->name}}" style="max-height: 100%">
					@else
					<img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
					@endif
									<span class="hover-posts">											
					<a href="{{ route('product_details', $value->slug) }}">View Ad</a>
				</span>
				<span class="price">
						PRICE: Rs.{{ number_format($value->price) }}</span>
						<span class="classiera-buy-sel">{{@$value->adtype->name}}</span>
							</div><!--premium-img-->
			<div class="detail text-center">
								<span class="price">Rs. {{ number_format($value->price) }}</span>
								<div class="box-icon">
									@if($value->user->show_email == 1)
					<a href="mailto:{{ $value->user->email }}?subject">
						<i class="fas fa-envelope"></i>
					</a>@endif      @if($value->user->show_mobile == 1)
										<a href="tel:{{ $value->user->mobile}}"><i class="fas fa-phone"></i></a>@endif
									</div>
				<a href="{{ route('product_details', $value->slug) }}" class="btn btn-primary outline btn-style-five">View Ad</a>
			</div><!--detail text-center-->
			<figcaption>
								<span class="price visible-xs">
					Rs.{{ number_format($value->price) }}</span>
								<h5><a href="{{ route('product_details',$value->slug) }}">{{$value->name}}</a></h5>
				<div class="category">
					<span>
					Category : 
					@foreach($value->categories->where('parent_id',!null) as $cat)
					<a href="{{route('menu', $cat->category->slug)}}" title="View all posts in {{$cat->category->name}}">{{$cat->category->name}}</a> /
					<a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a>
				@endforeach </span>
										<span>Location : 
						<a href="{{route('findByCity',$value->city->slug)}}" title="View all ads in {{$value->city->name}}">{{$value->city->name}}</a>
					</span>
				</div>
				<p class="description">{{substr($value->desc, 0, 340)}}</p>
			</figcaption>
		</figure>
	</div><!--row-->
</div><!--col-lg-4-->
@endforeach
</div><!--row-->
				</div><!--container-->
			</div><!--tabpanel Random-->
			<div role="tabpanel" class="tab-pane fade" id="popular">
				<div class="container">
					<div class="row">
						@foreach($popular as $key => $value)
					<div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
	<div class="classiera-box-div classiera-box-div-v5">
		<figure class="clearfix">
			<div class="premium-img">
				@if($value->featured == 1)
								<div class="featured-tag">
						<span class="left-corner"></span>
						<span class="right-corner"></span>
						<div class="featured">
							<p>Featured</p>
						</div>
					</div>
					@endif
					@if ($value->images->count() > 0)
					<img class="img-responsive" src="{{ asset('images_small/'.$value->images->first()->full) }}" alt="{{$value->name}}" style="max-height: 100%">
					@else
					<img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
					@endif
									<span class="hover-posts">											
					<a href="{{ route('product_details',$value->slug) }}">View Ad</a>
				</span>
				<span class="price">
						PRICE: Rs.{{ number_format($value->price) }}</span>
						<span class="classiera-buy-sel">{{@$value->adtype->name}}</span>
							</div><!--premium-img-->
			<div class="detail text-center">
								<span class="price">
					Rs.{{ number_format($value->price) }}</span>
								<div class="box-icon">
									@if($value->user->show_email == 1)
					<a href="mailto:{{$value->user->email}}?subject">
						<i class="fas fa-envelope"></i>
					</a>@endif
					@if($value->user->show_phone == 1)
										<a href="tel:{{$value->user->mobile}}"><i class="fas fa-phone"></i></a>@endif
									</div>
				<a href="{{ route('product_details', $value->slug) }}" class="btn btn-primary outline btn-style-five">View Ad</a>
			</div><!--detail text-center-->
			<figcaption>
								<span class="price visible-xs">
					Rs.{{ number_format($value->price) }}</span>
								<h5><a href="{{ route('product_details',$value->slug) }}">{{$value->name}}</a></h5>
				<div class="category">
					<span>
					Category : 
				@foreach($value->categories->where('parent_id',!null) as $cat)
					<a href="{{route('menu', $cat->category->slug)}}" title="View all posts in {{$cat->category->name}}">{{$cat->category->name}}</a> /
					<a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a>
				@endforeach </span>
										<span>Location : 
						<a href="{{route('findByCity',$value->city->slug)}}" title="View all posts in {{$value->city->name}}">{{$value->city->name}}</a>
					</span>
				</div>
				<p class="description">{{substr($value->desc, 0, 340)}}</p>
			</figcaption>
		</figure>
	</div><!--row-->
</div><!--col-lg-4-->
@endforeach
				</div><!--row-->
				</div><!--container-->
			</div><!--tabpanel popular-->
						<div class="view-all text-center">               
				<a href="{{route('shop-products')}}" class="btn btn-primary outline btn-md btn-style-five">
					View All Products				</a>
            </div>
		</div><!--tab-content-->
	</div><!--tab-divs-->
</section>

<section class="section-pad-80 category-v5">
	<div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-9 center-block">
                    <h1 class="text-capitalize">Product Categories</h1>
                    <p></p>
                </div><!--col-lg-7-->
            </div><!--row-->
        </div><!--container-->
    </div><!--section-heading-v1-->
	<div class="container">
		<ul class="list-inline list-unstyled categories">
						<!--col-lg-4-->
						<!--col-lg-4-->
						<li class="match-height" style="height: 177px;">
				<div class="category-content">					
						<i style="color:#86267e;" class="zmdi zmdi-female"></i>
											<h4>
						<a href="https://merooffer.com/menu/lifestyle">
							Lifestyle						</a>
					</h4>
				</div><!--category-box-->
			</li><!--col-lg-4-->
						<li class="match-height" style="height: 177px;">
				<div class="category-content">					
						<i style="color:#39444c;" class="zmdi zmdi-radio"></i>
											<h4>
						<a href="https://merooffer.com/menu/electronics">
							Electronics						</a>
					</h4>
				</div><!--category-box-->
			</li><!--col-lg-4-->
						<li class="match-height" style="height: 177px;">
				<div class="category-content">					
						<i style="color:#c25762;" class="zmdi zmdi-seat"></i>
											<h4>
						<a href="https://merooffer.com/menu/home-furnitures">
							Home &amp; Furnitures						</a>
					</h4>
				</div><!--category-box-->
			</li><!--col-lg-4-->
						<li class="match-height" style="height: 196px;">
				<div class="category-content">					
						<i style="color:#44baff;" class="zmdi zmdi-wrench"></i>
											<h4>
						<a href="https://merooffer.com/menu/services">
							Services						</a>
					</h4>
				</div><!--category-box-->
			</li><!--col-lg-4-->
						<li class="match-height" style="height: 196px;">
				<div class="category-content">					
						<i style="color:#9bc2a4;" class="zmdi zmdi-audio"></i>
											<h4>
						<a href="https://merooffer.com/menu/musical-instrument-services">
							Musical Instrument &amp; Services						</a>
					</h4>
				</div><!--category-box-->
			</li><!--col-lg-4-->
						<li class="match-height" style="height: 196px;">
				<div class="category-content">					
						<i style="color:#86267e;" class="zmdi zmdi-play"></i>
											<h4>
						<a href="https://merooffer.com/menu/video-games-toys">
							Video Games &amp; Toys						</a>
					</h4>
				</div><!--category-box-->
			</li><!--col-lg-4-->
						<li class="match-height" style="height: 196px;">
				<div class="category-content">					
						<i style="color:#d8690c;" class="zmdi zmdi-book"></i>
											<h4>
						<a href="https://merooffer.com/menu/sportsbooks-hobbies">
							Sports,books &amp; Hobbies						</a>
					</h4>
				</div><!--category-box-->
			</li><!--col-lg-4-->
						<li class="match-height" style="height: 196px;">
				<div class="category-content">					
						<i style="color:#509fa6;" class="zmdi zmdi-flight-takeoff"></i>
											<h4>
						<a href="https://merooffer.com/menu/travel-tourism">
							Travel &amp; Tourism						</a>
					</h4>
				</div><!--category-box-->
			</li><!--col-lg-4-->
						<li class="match-height" style="height: 158px;">
				<div class="category-content">					
						<i style="color:#adb337;" class="zmdi zmdi-face"></i>
											<h4>
						<a href="https://merooffer.com/menu/pets-animals">
							Pets &amp; Animals						</a>
					</h4>
				</div><!--category-box-->
			</li><!--col-lg-4-->
						<li class="match-height" style="height: 158px;">
				<div class="category-content">					
						<i style="color:;" class="zmdi zmdi-cutlery"></i>
											<h4>
						<a href="https://merooffer.com/menu/2-restaurants-cafe">
							Restaurants &amp; Cafe						</a>
					</h4>
				</div><!--category-box-->
			</li><!--col-lg-4-->
								</ul>
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="view-all text-center">
				<a href="https://merooffer.com/all-categories" class="btn btn-primary outline btn-style-five">View All Categories</a>
			</div>
		</div>
	</div>
</section>

@if($partners->count() > 0)

<section class="partners-v3 section-gray-bg">   
    <div class="container" style="overflow: hidden;">
        <div class="row">   
            <div class="col-lg-12">
                <div class="owl-carousel partner-carousel-v3 partner-carousel-v4" data-car-length="6" data-items="6" data-loop="true" data-nav="false" data-autoplay="true" data-autoplay-timeout="2000" data-dots="false" data-auto-width="false" data-auto-height="true" data-right="false" data-responsive-small="2" data-autoplay-hover="true" data-responsive-medium="4" data-responsive-xlarge="6" data-margin="30">
                    @foreach($partners as $value)
                        <div class="item partner-img match-height">
                            <span class="helper"></span>
                            <a href="//{{$value->link}}" target="_blank">
                                <img src="{{@asset('images_small/'.$value->image)}}" alt="partner">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div><!--col-lg-12-->  
        </div><!--row-->
    </div><!--container-->
</section>
@endif
@include('site.partials.footer')
@endsection