<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Product;

class SendOrderToProductOwner extends Mailable
{
    use Queueable, SerializesModels;
    protected $order;
    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'order' => $this->order,
            'products' => $this->order->products->where('user_id', $this->user->id),
            'user' => $this->order->user,
            'courier' => $this->order->courier,
            'address' => $this->order->address,
            'status' => $this->order->orderStatus
        ];
        return $this->from('noreply@merooffer.com', 'Mero Offer')
            ->subject('Order Received-' . $this->order->reference)
            ->view('emails.SendOrderToProductOwner', $data);
    }
}
