<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Contracts\AttributeContract;
use App\Models\Attribute;
use App\Contracts\AmenityContract;

class AmenityController extends BaseController
{
    /**
     * @var AttributeContract
     */
    protected $amenityRepo;
    protected $attributeRepo;

    /**
     * amenityController constructor.
     * @param amenityContract $attributeRepository
     */
    public function __construct(
        AttributeContract $attributeRepo,
         AmenityContract $amenityRepo
     )
    {
        $this->attributeRepo = $attributeRepo;
        $this->amenityRepo = $amenityRepo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $list = $this->amenityRepo->list('created_at', 'desc');
        $datas = $this->amenityRepo->paginateArrayResults($list->all(), 5);

        $this->setPageTitle('Amenities', 'List of all amenities');
        return view('admin.amenity.index', compact('datas'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $options = Attribute::where('type','checkbox')->get();
        $this->setPageTitle('Amenities', 'Create Amenity');
        return view('admin.amenity.create',compact('options'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $attribute = $this->amenityRepo->store($request);

        if (!$attribute) {
            return $this->responseRedirectBack('Error occurred while creating attribute.', 'error', true, true);
        }
        return $this->responseRedirect('amenity.index', 'Attribute added successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data = $this->amenityRepo->findById($id);
        $options = Attribute::where('type','checkbox')->get();

        $this->setPageTitle('Amenities', 'Edit Amenities : '.$data->name);
        return view('admin.amenity.edit', compact('data','options'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request,$id)
    {
        $data = $this->amenityRepo->edit($request,$id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('amenity.index', 'Record updated successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $data = $this->amenityRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting attribute.', 'error', true, true);
        }
        return $this->responseRedirect('amenity.index', 'Attribute deleted successfully' ,'success',false, false);
    }
}
