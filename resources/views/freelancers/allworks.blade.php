@extends('site.partials.main_index')
@section('title','Freelancers - Mero Offer')
@section('content')
<body class="archive category category-uncategorized category-1 logged-in">
    @include('freelancers.partials.navbarwork')
@include('freelancers.partials.searchwork')
    <section class="inner-page-content border-bottom top-pad-50">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4">
                <!--SearchForm-->
@include('freelancers.partials.advanced_search')
           </div><!--col-lg-3 col-md-4-->
            <!--EndSidebar-->
            <!--ContentArea-->
            <div class="col-lg-9 col-md-8">
                                <section class="classiera-advertisement advertisement-v5 section-pad-80 border-bottom">
                        <div class="tab-divs">
                            <div class="view-head">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-8 col-sm-9 col-xs-10">
                                            <div class="total-post">
                                            <p> {{$works->count()}} Works Found 
                                                
                                            </p>
                                        </div>
                     </div><!--col-lg-6 col-sm-8-->
                     <div class="col-lg-4 col-sm-3 col-xs-2">
                        <div class="view-as text-right flip">
                            <a id="grid" class="grid active" href="#"><i class="fas fa-th"></i></a>
                            <a id="list" class="list " href="#"><i class="fas fa-th-list"></i></a>                          
                        </div><!--view-as tab-button-->
                    </div><!--col-lg-6 col-sm-4 col-xs-12-->
                </div><!--row-->
            </div><!--container-->
        </div><!--view-head-->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="all">
                <div class="container products">
                    <div id="load" class="row">
                     <!--FeaturedPosts-->@foreach($works as $key => $value)
                     <div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
                        <div class="classiera-box-div classiera-box-div-v5">
                            <figure class="clearfix">
                                <div class="premium-img">@if($value->jobtype->id !== 1)
                                    <div class="featured-tag">
                                        <span class="left-corner"></span>
                                        <span class="right-corner"></span>
                                        <div class="featured">
                                            <p>{{$value->jobtype->name}}</p>
                                        </div>
                                    </div>@endif
                                    
                    <img class="img-responsive" src="{{ asset('frontend/images/empty.png') }}" alt="{{$value->name}}">
                    
                                    <span class="hover-posts">                                          
                                        <a href="{{route('freelancers.show',$value->slug)}}">View Project</a>
                                    </span>
                                    <span class="price">
                                        Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif 
                                    </span>
                                        <span class="classiera-buy-sel">
                                            {{$value->duration->name}} </span>
                                        </div><!--premium-img-->
                                        <div class="detail text-center">
                                            <span class="price">
                                                Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif           </span>
                                                <div class="box-icon">
                                                    @if($value->user->show_email)
                                                    <a href="mailto:{{ $value->user->email }}?subject">
                                                        <i class="fas fa-envelope"></i>
                                                    </a>@endif
                                                    @if($value->user->show_mobile)
                                                    <a href="tel:{{ $value->user->mobile}}"><i class="fas fa-phone"></i></a>@endif
                                                </div>
                                                <a href="{{route('freelancers.show',$value->slug)}}" class="btn btn-primary outline btn-style-five">View</a>
                                            </div><!--detail text-center-->
                                            <figcaption>
                                                <span class="price visible-xs">Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif
                                                </span>
                                                <h5><a href="{{route('freelancers.show',$value->slug)}}">{{$value->name}}</a></h5>
                                                <div class="category">
                                                    <span>
                                                        Category :
                                
                    <a href="{{route('freelancers.category',$value->fcategory->slug)}}" title="View all posts in category">{{$value->fcategory->name}}</a>
                    
                 </span>
                                                        <span>Location : 
                                                            <a href="#" title="View all posts in sdf">{{$value->address}}</a>
                                                        </span>
                                                    </div>
                                                    <p class="description">{{$value->body}}</p>
                                                </figcaption>
                                            </figure>
                                        </div><!--row-->
                                    </div><!--col-lg-4-->
                                    @endforeach
                                    <br>
                                </div><!--row-->
                                <div class="classiera-pagination">
                                    <nav aria-label="Page navigation">
                                        <div id="pagination">
                                         {!!$works->links()!!}
                                     </div>
                                 </nav>
                             </div><!--tab-divs-->
                         </section><!-- advertisement -->
<!-- end advertisement style 5-->
</div>
<!--ContentArea-->
        </div><!--row-->
    </div><!--container-->  
</section>
            @include('site.partials.footer')
            @section('page-script')
    <script type='text/javascript' src="{{ asset('frontend/js/validator.min.js') }}"></script>
@stop
            @endsection