<?php

namespace App\Repo;

use App\Models\Pricelabel;
use App\Contracts\PricelabelContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

class PricelabelRepo extends BaseRepo implements PricelabelContract
{
    /**
     * PricelabelRepository constructor.
     * @param Pricelabel $model
     */
    public function __construct(Pricelabel $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }
    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Category|mixed
     */
    public function store($request)
    {
        try {
            $params = $request->all();
            $collection = collect($params);

            $status = $collection->has('status') ? 1 : 0;

            $merge = $collection->merge(compact('status'));

            $pricelabel = new Pricelabel($merge->all());

            $pricelabel->save();

            if ($collection->has('category_id')) {
                $pricelabel->categories()->sync($params['category_id']);
            }

            return $pricelabel;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
        $params = $request->all();

        $pricelabel = $this->findById($id);

        $collection = collect($params);

        $status = $collection->has('status') ? 1 : 0;

        $merge = $collection->merge(compact('status'));

        $pricelabel->update($merge->all());

         if ($collection->has('category_id')) {
                $pricelabel->categories()->sync($params['category_id']);
            }

        return $pricelabel;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $pricelabel = $this->findById($id);
        $pricelabel->delete();
        return $pricelabel;
    }
}
