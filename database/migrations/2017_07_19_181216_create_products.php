<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('sold')->default(0);
            $table->bigInteger('order');
            $table->boolean('featured')->default(0);
            $table->bigInteger('views')->default(0);
            $table->boolean('status')->default(0);
            $table->boolean('bid');
            $table->string('slug')->unique();
            $table->integer('quantity')->default(1);
            $table->decimal('weight')->default(0)->nullable();

            $table->string('address')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('cell')->nullable();
            $table->string('url')->nullable();

            $table->string('name');
            $table->decimal('price',18,2)->nullable();
            $table->decimal('rgr_price',18,2)->nullable();
            $table->longText('desc');
            $table->dateTime('expired_at')->nullable();
            $table->bigInteger('condition_id')->unsigned()->nullable();
            $table->bigInteger('pricelabel_id')->unsigned()->nullable();
            $table->bigInteger('assured_id')->unsigned()->default(1);
            $table->foreign('assured_id')->references('id')->on('assureds')->onDelete('cascade');
            $table->bigInteger('pricetype_id')->unsigned();
            $table->foreign('pricetype_id')->references('id')->on('pricetypes')->onDelete('cascade');
            $table->bigInteger('day_id')->unsigned();
            $table->foreign('day_id')->references('id')->on('days')->onDelete('cascade');
            $table->bigInteger('adtype_id')->unsigned();
            $table->foreign('adtype_id')->references('id')->on('adtypes')->onDelete('cascade');
            $table->bigInteger('city_id')->unsigned()->nullable();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
