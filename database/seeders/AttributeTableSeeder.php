<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Attribute;
use App\Models\Attributevalue;

class AttributeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $att = new Attribute();
        $att->name = "Property Type";
        $att->code = "property_type";
        $att->type = "radio";
        $att->required = 1;
        $att->filterable = 1;
        $att->data_type = "required|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $property_type = [
             'Residential',
             'Commercial',
             'Agricultural',
         ];
         foreach ($property_type as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 1,]);
                }

        $att = new Attribute();
        $att->name = "Property Face";
        $att->code = "property_face";
        $att->type = "dropdown";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $property_face = [
             'East',
             'West',
             'North',
             'South',
             'South-East',
             'South-West',
             'North-East',
             'North-West',
         ];
         foreach ($property_face as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 2,]);
               }
                

        $att = new Attribute();
        $att->name = "Furnishing";
        $att->code = "furnishing";
        $att->type = "radio";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(1,3,4,5,6,7,8,9));

        $furnishing = [
             'Semi',
             'Non',
             'Full',
         ];
         foreach ($furnishing as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 3,]);
               }

        $att = new Attribute();
        $att->name = "Road Access";
        $att->code = "road_access";
        $att->type = "dropdown";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $access_road = [
             'Less than 5 feet',
             '5 to 8 Feet',
             '9 to 12 Feet',
             '13 to 20 Feet',
             'Above 20 Feet',
             'Goreto Bato',
             'No Road Access',
         ];
         foreach ($access_road as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 4,]);
               }

        $att = new Attribute();
        $att->name = "Road Type";
        $att->code = "road_type";
        $att->type = "radio";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $road_type = [
             'Soil Stabilized',
             'Paved',
             'Gavelled',
             'Blacktopped',
             'Alley',
         ];
         foreach ($road_type as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 5,]);
               }

        $att = new Attribute();
        $att->name = "Total Area(in aana/dhur)";
        $att->code = "total_area";
        $att->type = "text";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,5,6,7,8,9));

        $att = new Attribute();
        $att->name = "Built Up Area (sq.ft)";
        $att->code = "built_up";
        $att->type = "text";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $att = new Attribute();
        $att->name = "Floors";
        $att->code = "floors";
        $att->type = "text";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(1,3,5,6,7,8,9));

        $att = new Attribute();
        $att->name = "Bedroom";
        $att->code = "bedroom";
        $att->type = "text";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(1,3,5,6,7,8,9));

        $att = new Attribute();
        $att->name = "Bathroom";
        $att->code = "bathroom";
        $att->type = "text";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(1,3,5,6,7,8,9));

        $att = new Attribute();
        $att->name = "Living Room";
        $att->code = "living_room";
        $att->type = "text";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(1,3,5,6,7,8,9));
        //cars
        $att = new Attribute();
        $att->name = "Brand";
        $att->code = "car_brand";
        $att->type = "dropdown";
        $att->required = 1;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(11,13,14,15));

        $car_brand = [
        'Chevrolet',
        'Daihatsu',
        'Datsun',
        'Eicher',
        'Fiat',
        'Ford',
        'Geely',
        'Honda',
        'Hyundai',
        'Kia',
        'Land Rover',
        'Mahindra',
        'Maruti Suzuki',
        'Mazda',
        'Mitsubishi',
        'Nissan',
        'Other Brands',
        'Other Chinese Brands',
        'Perodua',
        'Renault',
        'Skoda',
        'Ssangyong',
        'Tata',
        'Toyota',
        'Volkswagen',
         ];
         foreach ($car_brand as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 12,]);
               }


        $att = new Attribute();
        $att->name = "Type";
        $att->code = "car_type";
        $att->type = "dropdown";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(11,13,14,15));

        $car_type = [
             'Small Hatchback',
             'Mid Size Hatchback',
             'Sedan',
             'CUV / Compact SUV',
             'Jeep / SUV',
             'PickUp',
             'Others'
         ];
         foreach ($car_type as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 13,]);
               }
        $att = new Attribute();
        $att->name = "Make Year";
        $att->code = "car_make_year";
        $att->type = "text";
        $att->required = 1;
        $att->filterable = 1;
        $att->data_type = "required|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(11,13,14,15));

        $att = new Attribute();
        $att->name = "Brand";
        $att->code = "motorbike_brand";
        $att->type = "dropdown";
        $att->required = 1;
        $att->filterable = 1;
        $att->data_type = "required|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(12));

        $motorcycle_brand = [
        'Vespa',
        'Yamaha',
        'TVS',
        'Honda',
        'Hero',
        'Bajaj',
        'Apollo - Rieju',
        'Aprilia',
        'Um',
        'Royal Enfield',
        'Benelli',
        'Crossfire',
        'Hartford',
        'KTM',
        'Suzuki',
        'Mahindra',
        'Other Electric Bike',
        'Other Motorcycle Brands',
         ];
         foreach ($motorcycle_brand as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 15,]);
               }

        $att = new Attribute();
        $att->name = "Make Year";
        $att->code = "motorbike_make_year";
        $att->type = "text";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(12));

        

        $att = new Attribute();
        $att->name = "Type";
        $att->code = "motorbike_type";
        $att->type = "radio";
        $att->required = 1;
        $att->filterable = 1;
        $att->data_type = "required|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(12));

        $bike_type = [
             'Standard',
             'Cruiser',
             'Sports',
             'Dirt',
             'Scooty',
         ];
         foreach ($bike_type as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 17,]);
               }

        $att = new Attribute();
        $att->name = "Engine (CC)";
        $att->code = "engine";
        $att->type = "text";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(11,12,13,14,15));

        $att = new Attribute();
        $att->name = "Kilometers";
        $att->code = "kilometers";
        $att->type = "text";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(11,12,13,14,15));

        $att = new Attribute();
        $att->name = "Colour";
        $att->code = "colour";
        $att->type = "text";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|string";
        $att->save();
        $att->categories($att->id)->sync(array(11,12,13,14,15));

        $att = new Attribute();
        $att->name = "Fuel";
        $att->code = "fuel";
        $att->type = "radio";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|string";
        $att->save();
        $att->categories($att->id)->sync(array(11,12,13,14,15));

        $fuel = [
             'Petrol',
             'Diesel',
             'Electric',
             'Hybrid'
         ];
         foreach ($fuel as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 21,]);
               }

       $att = new Attribute();
        $att->name = "Transmission";
        $att->code = "transmission";
        $att->type = "radio";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(11,13,14,15));

        $transmission = [
             'Manual Gear-2WD',
             'Manual Gear-4WD',
             'Automatic Gear-2WD',
             'Automatic Gear-4WD'
         ];
         foreach ($transmission as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 22,]);
               }

        $att = new Attribute();
        $att->name = "Lot Number";
        $att->code = "lot_number";
        $att->type = "text";
        $att->required = 1;
        $att->filterable = 1;
        $att->data_type = "required|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(12));

        $att = new Attribute();
        $att->name = "Mileage (km / l)";
        $att->code = "mileage";
        $att->type = "text";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(12));

        $att = new Attribute();
        $att->name = "Real Estate Amnities";
        $att->code = "real_estate_feature";
        $att->type = "checkbox";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $att = new Attribute();
        $att->name = "Car Amnities";
        $att->code = "car_feature";
        $att->type = "checkbox";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(11,13,14));

        $att = new Attribute();
        $att->name = "Bike Amnities";
        $att->code = "bike_feature";
        $att->type = "checkbox";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(12));

        $att = new Attribute();
        $att->name = "Type";
        $att->code = "cloth_type";
        $att->type = "dropdown";
        $att->required = 1;
        $att->filterable = 1;
        $att->data_type = "required|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(19,20,22));

        $cloth_type = [
             'Jacket & Coats',
             'Hoodie & Sweat Shirts',
             'T-shirt',
             'Shirt',
             'Jeans',
             'Polo',
             'Pants',
             'Leggings',
             'Party Wear',
             'Sweaters & Cardigans',
             'Tops',
             'Shorts',
             'Dress & Skirts',
             'Undergarments',
             'Sports Wear',
             'Others'
         ];
         foreach ($cloth_type as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 28,]);
               }

        $att = new Attribute();
        $att->name = "Type";
        $att->code = "shoes_type";
        $att->type = "dropdown";
        $att->required = 1;
        $att->filterable = 1;
        $att->data_type = "required|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(21,23));

        $shoes_type = [
             'Sports',
             'Casual',
             'Boot',
             'Formal',
             'Canvas',
             'Flat',
             'Heel',
             'Sandal',
             'Wedges',
             'Others',
         ];
         foreach ($shoes_type as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 29,]);
               }

        $att = new Attribute();
        $att->name = "Type";
        $att->code = "watches_type";
        $att->type = "radio";
        $att->required = 1;
        $att->filterable = 1;
        $att->data_type = "required|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(27));

        $watches_type = [
             'Ladies',
             'Gents',
             'Both',
         ];
         foreach ($watches_type as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 30,]);
               }

        $att = new Attribute();
        $att->name = "Brand";
        $att->code = "mobile_brand";
        $att->type = "dropdown";
        $att->required = 1;
        $att->filterable = 1;
        $att->data_type = "required|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(35));

        $mobile_brand = [
             'Oppo',
             'OnePlus',
             'Vivo',
             'Samsung',
             'Mi(Xiaomi)',
             'Huawei - Honor',
             'HTC',
             'Sony',
             'Apple',
             'Colors',
             'Blackberry',
             'Gionee',
             'Asus',
             'Lava-Xolo',
             'Google',
             'Micromax',
             'Nokia',
             'Motorola',
             'LG',
             'Lenovo',
             'Other Brands',
             'Other Copy-Clone Phones'
         ];
         foreach ($mobile_brand as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 31,]);
               }

        $att = new Attribute();
        $att->name = "RAM";
        $att->code = "mobile_ram";
        $att->type = "dropdown";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(35));

        $mobile_ram = [
             '512 MB Or Less',
             '1 GB',
             '1.5 GB',
             '2 GB',
             '3 GB',
             '4 GB',
             '6 GB',
             '8 GB or More',
             '16 GB',
         ];
         foreach ($mobile_ram as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 32,]);
               }

        $att = new Attribute();
        $att->name = "Internal Storage";
        $att->code = "mobile_internal_storage";
        $att->type = "dropdown";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(35));

        $mobile_internal_storage = [
             'Less than 512 MB',
             '512 MB',
             '1 GB',
             '2 GB',
             '4 GB',
             '8 GB',
             '16 GB',
             '32 GB',
             '64 GB',
             '128 GB',
             '256 GB or more',
             '512 GB'
         ];
         foreach ($mobile_internal_storage as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 33,]);
               }

        $att = new Attribute();
        $att->name = "CPU Core";
        $att->code = "mobile_cpu_core";
        $att->type = "dropdown";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(35));

        $mobile_cpu_core = [
             'Single',
             'Dual - 2',
             'Quad - 4',
             'Hexa - 6',
             'Octa - 8',
             'Deca - 10',
         ];
         foreach ($mobile_cpu_core as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 34,]);
               }

        $att = new Attribute();
        $att->name = "Operating System";
        $att->code = "mobile_os";
        $att->type = "dropdown";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(35));

        $mobile_os = [
             'Android 5.0 (Lollipop) or below',
             'Android 6.0 (Marshmarllow)',
             'Android 7.0 (Nougat)',
             'Android 8.0 (Oreo)',
             'Android 9.0 (Pie)',
             'Android 10 (Q)',
             'Apple iOS 9 or below',
             'Apple iOS 10',
             'Apple iOS 11',
             'Apple iOS 12',
             'Apple iOS 13',
             'Windows 10',
             'Windows 8.x or below',
             'Symbian',
             'RIM Blackberry',
             'Firefox',
             'Tizen',
             'Other OS',

         ];
         foreach ($mobile_os as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 35,]);
               }

        $att = new Attribute();
        $att->name = "Sim Slot";
        $att->code = "mobile_sim_slot";
        $att->type = "dropdown";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(35));

        $mobile_sim_slot = [
             'Single Sim - 2G',
             'Single Sim - 3G',
             'Single Sim - 4G (LTE)',
             'Single Sim - CDMA',
             'Dual Sim - 2G + 2G',
             'Dual Sim - 3G + 2G',
             'Dual Sim - 4G + 3G',
             'Dual Sim - 4G + 4G',
             'Dual Sim - GSM + CDMA',
             'Triple Sim',
         ];
         foreach ($mobile_sim_slot as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 36,]);
               }

        $att = new Attribute();
        $att->name = "Screen Size";
        $att->code = "mobile_screen_size";
        $att->type = "dropdown";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(35));

        $mobile_screen_size = [
             'Less than 4 inch',
             '4.0 to 4.4 inch',
             '4.5 to 4.9 inch',
             '5.0 to 5.4 inch',
             '5.5 to 5.9 inch',
             '6.0 to 6.4 inch',
             '6.5 inch or More',
         ];
         foreach ($mobile_screen_size as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 37,]);
               }

        $att = new Attribute();
        $att->name = "Front Camera";
        $att->code = "mobile_front_camera";
        $att->type = "dropdown";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(35));

        $mobile_front_camera = [
             'No',
             'VGA',
             '1 MP',
             '2 MP',
             '3 MP',
             '5 MP',
             '8 MP',
             '13 MP or More',
         ];
         foreach ($mobile_front_camera as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 38,]);
               }

        $att = new Attribute();
        $att->name = "Back Camera";
        $att->code = "mobile_back_camera";
        $att->type = "dropdown";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(35));

        $mobile_back_camera = [
             'No',
             '1 MP or Less',
             '2 MP - 2.9 MP',
             '3 MP - 4.9 MP',
             '5 MP - 7.9 MP',
             '8 MP - 12.9 MP',
             '13 MP - 19.9 MP',
             '20 MP or More'
         ];
         foreach ($mobile_back_camera as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 39,]);
               }

        $att = new Attribute();
        $att->name = "Mobile Features";
        $att->code = "mobile_amnities";
        $att->type = "checkbox";
        $att->required = 0;
        $att->filterable = 0;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(35));

        //laptops

        $att = new Attribute();
        $att->name = "RAM(GB)";
        $att->code = "laptop_ram";
        $att->type = "text";
        $att->required = 1;
        $att->filterable = 1;
        $att->data_type = "required|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(37));

        $att = new Attribute();
        $att->name = "HDD (GB/TB)";
        $att->code = "laptop_hdd";
        $att->type = "text";
        $att->required = 1;
        $att->filterable = 1;
        $att->data_type = "required|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(37));

        $att = new Attribute();
        $att->name = "Video Card";
        $att->code = "laptop_video_card";
        $att->type = "text";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(37));

        $att = new Attribute();
        $att->name = "Screen Size (inch)";
        $att->code = "laptop_screen_size";
        $att->type = "text";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(37));

        $att = new Attribute();
        $att->name = "Processor";
        $att->code = "laptop_processor";
        $att->type = "dropdown";
        $att->required = 1;
        $att->filterable = 1;
        $att->data_type = "required|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(37));

        $laptop_processor = [
             'Intel Pentium 4 or Lower',
             'Intel Pentium M',
             'Intel Pentium Dual/Quad',
             'Intel Celeron',
             'Intel Celeron Dual/Quad',
             'Intel Core M',
             'Intel Core 2',
             'Intel Atom',
             'Intel Core i3',
             'Intel Core i5',
             'Intel COre i7',
             'Intel Core i9',
             'Intel - Others',
             'AMD Ryzen 3',
             'AMD Ryzen 5',
             'AMD Ryzen 7',
             'AMD A-Series APU',
             'AMD E-Series APU',
             'AMD - Others',
             'Others',
         ];
         foreach ($laptop_processor as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 45,]);
               }

        $att = new Attribute();
        $att->name = "Processor Generation";
        $att->code = "laptop_processor_generation";
        $att->type = "dropdown";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(37));

        $laptop_processor_generation = [
             '2009 before',
             '1st Gen (2010)',
             '2st Gen (2011)',
             '3st Gen (2012)',
             '4st Gen (2013 - 2014)',
             '5st Gen (2015)',
             '6st Gen (2016)',
             '7st Gen (2017)',
             '8st Gen (2018)',
             '9st Gen (2019)',
             '10st Gen (2019-2020)',
             'Other Gen',
         ];
         foreach ($laptop_processor_generation as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 46,]);
               }

        $att = new Attribute();
        $att->name = "Screen Type";
        $att->code = "laptop_screen_type";
        $att->type = "radio";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(37,44));

        $laptop_screen_type = [
             'CRT',
             'LCD',
             'LED',
             'None',
         ];
         foreach ($laptop_screen_type as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 47,]);
               }

        $att = new Attribute();
        $att->name = "Battery";
        $att->code = "laptop_battery";
        $att->type = "dropdown";
        $att->required = 0;
        $att->filterable = 1;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(37));

        $laptop_battery = [
             'No Battery',
             'Less than 30 minutes',
             '30 - 60 minutes',
             '1 - 2 hour',
             '2 - 3 hour',
             '3 - 4 hour',
             '4 - 5 hour',
             '5 - 6 hour',
             '6 - 7 hour',
             '7 - 8 hour',
             '8 - 10 hour',
             'Above 10 hours'
         ];
         foreach ($laptop_battery as $value){
                   Attributevalue::create([
                    'value' => $value,
                'attribute_id' => 48,]);
               }

        $att = new Attribute();
        $att->name = "Laptop Features";
        $att->code = "laptop_amnities";
        $att->type = "checkbox";
        $att->required = 0;
        $att->filterable = 0;
        $att->data_type = "nullable|numeric";
        $att->save();
        $att->categories($att->id)->sync(array(37));

    }
}
