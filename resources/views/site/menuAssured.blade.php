@extends('site.partials.main_index')
@section('title')
Classified Ads in {{$category->name}}
@endsection
@section('content')
<body class="archive category category-uncategorized category-1 logged-in">
    @include('site.partials.navbar')
    
    @include('site.partials.search')
    <!--search-section--><!-- page content -->
<section class="inner-page-content border-bottom top-pad-50">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-9">
                <!-- advertisement -->
<section class="classiera-advertisement advertisement-v5 section-pad-80 border-bottom">
    <div class="tab-divs">
        <div class="view-head">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-sm-9 col-xs-10">
                        <div class="tab-button">
                            <ul class="nav nav-tabs" role="tablist">                                
                                <li role="presentation"  <?php if(Request::get('price') != 'asc' && Request::get('price') != 'desc' && Request::get('view') != 'desc' ){ 
                                    echo 'class="active" ';
                                } ?>>
                                    <a href="{{ route('menuAssured',$category->slug) }}" aria-controls="all">
                                        Recent Ads
                                                                            <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation" <?php if(Request::get('price') == 'asc'){ 
                                    echo 'class="active" ';
                                } ?>>                                    
                                    <a href="{{ route('menuAssured', ['slug'=>$category->slug, 'price' =>'asc']) }}" aria-controls="random">
                                        Price ASC                                      <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation"<?php if(Request::get('price') == 'desc'){ 
                                    echo 'class="active" ';
                                } ?>>                                    
                                    <a href="{{ route('menuAssured', ['slug'=>$category->slug, 'price' =>'desc']) }}" aria-controls="random">
                                        Price DESC                                      <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation" <?php if(Request::get('view')){ 
                                    echo 'class="active" ';
                                } ?>>                                   
                                    <a href="{{ route('menuAssured', ['slug'=>$category->slug, 'view' => 'desc']) }}" aria-controls="popular">
                                       Popular Ads                                    <span class="arrow-down"></span>
                                    </a>
                                </li>
                            </ul><!--nav nav-tabs-->
                        </div><!--tab-button-->
                    </div><!--col-lg-6 col-sm-8-->
                    <div class="col-lg-4 col-sm-3 col-xs-2">
                        <div class="view-as text-right flip">
                            <a id="grid" class="grid active" href="#"><i class="fas fa-th"></i></a>
                            <a id="list" class="list " href="#"><i class="fas fa-th-list"></i></a>                          
                        </div><!--view-as tab-button-->
                    </div><!--col-lg-6 col-sm-4 col-xs-12-->
                </div><!--row-->
            </div><!--container-->
        </div><!--view-head-->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="all">
                <div class="container products">
                    <div id="load" class="row">
                   <!--FeaturedPosts-->@forelse($products as $key => $value)
<div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
    <div class="classiera-box-div classiera-box-div-v5">
        <figure class="clearfix">
            <div class="premium-img">@if($value->featured == 1)
                                <div class="featured-tag">
                        <span class="left-corner"></span>
                        <span class="right-corner"></span>
                        <div class="featured">
                            <p>Featured</p>
                        </div>
                    </div>@endif
                    @if ($value->images->count() > 0)
                    <img class="image-responsive" src="{{ asset('images_small/'.$value->images->first()->full) }}" alt="{{$value->name}}" style="max-height: 100%">
                    @else
                    <img class="image-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
                    @endif
                                    <span class="hover-posts">                                          
                    <a href="{{ route('product_details',$value->slug) }}">View Ad</a>
                </span>
                                    <span class="price">
                        PRICE: Rs.{{ number_format($value->price) }}</span>
                                                <span class="classiera-buy-sel">
                {{@$value->adtype->name}}              </span>
                            </div><!--premium-img-->
            <div class="detail text-center">
                                <span class="price">
                    Rs. {{ number_format($value->price) }}             </span>
                                <div class="box-icon">@if($value->user->show_email)
                    <a href="mailto:{{ $value->user->email }}?subject">
                        <i class="fas fa-envelope"></i>
                    </a>@endif
                    @if($value->user->show_mobile)
                                        <a href="tel:{{ $value->user->mobile}}"><i class="fas fa-phone"></i></a>@endif
                                    </div>
                <a href="{{ route('product_details',$value->slug) }}" class="btn btn-primary outline btn-style-five">View Ad</a>
            </div><!--detail text-center-->
            <figcaption>
                <span class="price visible-xs">Rs.{{ number_format($value->price) }}
            </span>
            <h5><a href="{{ route('product_details',$value->slug) }}">{{$value->name}}</a></h5>
                <div class="category">
                    <span>
                    Category : 
                    @foreach($value->categories->where('parent_id',!null) as $cat)
                    <a href="{{route('menu', $cat->category->slug)}}" title="View all posts in {{$cat->category->name}}">{{$cat->category->name}}</a> /
                    <a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a>
                @endforeach </span>
                                        <span>Location : 
                        <a href="{{route('findByCity', $value->city->slug)}}" title="View all posts in {{$value->city->name}}">{{$value->city->name}}</a>
                    </span>
                </div>
                <p class="description">{{substr($value->desc, 0, 245)}}</p>
            </figcaption>
        </figure>
    </div><!--row-->
</div><!--col-lg-4-->
@empty
<div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
    <div class="classiera-box-div classiera-box-div-v5">
        <figure class="clearfix">
            <div class="premium-img">
                    <img class="img-responsive" src="{{ asset('frontend/images/empty.png') }}" alt="Submit Ads">
                                    <span class="hover-posts">                                          
                    <a href="{{ url('ads-list') }}">Submit Ad</a>
                </span>
                                    <span class="price">
                        Go On, It's Easy</span>
                                                <span class="classiera-buy-sel">
                Want to see your stuff here??</span>
                            </div><!--premium-img-->
            <div class="detail text-center">
                                <span class="price">Go On, It's Easy</span>
                                <div class="box-icon">
                                    </div>
                <a href="{{ url('ads-list') }}" class="btn btn-primary outline btn-style-five">Post Ad</a>
            </div><!--detail text-center-->
            <figcaption>
                <span class="price visible-xs">Go On, It's Easy
            </span>
            <h5><a href="{{ url('ads-list') }}">Start Selling</a></h5>
                <div class="category">
                </div>
                <p class="description">Make some extra cash by selling things in your community. Go on, It's Quick and Easy .</p>
            </figcaption>
        </figure>
    </div><!--row-->
</div><!--col-lg-4-->
@endforelse
<br>
</div><!--row-->
<div class="classiera-pagination">
    <nav aria-label="Page navigation">
        <div id="pagination">
       {!!$products->links()!!}
        </div>
    </nav>
</div><!--tab-divs-->
</section><!-- advertisement -->
            </div><!--col-md-8-->
            <div class="col-md-4 col-lg-3">
                <aside class="sidebar">
                    <div class="row">
                        <!--subcategory-->
                        <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                            <div class="widget-box">
                                <div class="widget-title">
                                    <h4>
                                        <i class="{{$category->font}}" style="color:{{$category->color}};"></i>
                                        {{$category->name}}                                 </h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="category">
                                        @if($category->children->count() > 0)
                                        @foreach($category->children as $value)
                                        <li>
                                            <a href="{{ route('menu',$value->slug) }}">
                                                <i class="fas fa-angle-right"></i>
                                               {{$value->name}}                                      <span class="pull-right flip">
                                                {{$value->products->where('status',1)->count()}}
                                                </span>
                                            </a>
                                        </li>
                                        @endforeach
                                        @else 
                                        
                                        <li><a href="{{ route('menu', $category->category->slug) }}">
                                            <i class="fas fa-angle-right"></i>{{$category->category->name}}</a></li>
                                        
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
<!--subcategory-->
<!--advancesearch-->
<div class="col-lg-12 col-md-12 col-sm-6 match-height">
<div class="widget-box">
@include('site.partials.advance_search_menu')
</div>
</div>
<!--advancesearch-->

</div><!--row-->
                </aside>
            </div><!--row-->
        </div><!--row-->
    </div><!--container-->
</section>  
<!-- page content -->


@include('site.partials.footer')
@section('page-script')
    <script type='text/javascript' src="{{ asset('frontend/js/validator.min.js') }}"></script>
@stop
@endsection