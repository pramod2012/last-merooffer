<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Firmuser;
use App\Http\Requests\StoreFirmuserRequest;

class FirmuserController extends Controller
{
    public function index(){
    	return view('cart.address');
    }

    public function store(Request $request){
    	$data = new Firmuser();

            if ($request->has('logo')) {
                $logo = $this->uploadOne($request->logo, 'firmusers');
                $data->logo = $logo;
            }
            if ($request->has('img1')) {
                $img1 = $this->uploadOne($request->img1, 'firmusers');
                $data->img1 = $img1;
            }
            if ($request->has('img2')) {
                $img2 = $this->uploadOne($request->img2, 'firmusers');
                $data->img2 = $img2;
            }

            $data->name = $request->name;
            $data->desc = $request->desc;
            $data->email = $request->email;
            $data->show_phone = $request->show_phone;
            $data->show_email = $request->show_email;
            $data->url = $request->url;
            $data->phone = $request->phone;
            $data->user_id = auth()->user()->id;

            $data->insta = $request->insta;
            $data->fb = $request->fb;
            $data->twitter = $request->twitter;
            $data->youtube = $request->youtube;
            $data->linkedin = $request->linkedin;

            $data->save();
    	return redirect('/profile');
    }

    public function destroy($id){
    	$data = Address::find($id);
    	$data->delete();
        return redirect('/profile');
    }
}
