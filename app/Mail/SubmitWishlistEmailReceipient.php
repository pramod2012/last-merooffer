<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Wishlist;

class SubmitWishlistEmailReceipient extends Mailable
{
    use Queueable, SerializesModels;
    protected $wishlist;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Wishlist $wishlist)
    {
        $this->wishlist = $wishlist;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@merooffer.com')
                    ->subject('Submit Wishlist Email Receipient')
                    ->view('emails.submitWishlistEmailReceipient')
                    ->with([
                        'wishlist_by' => $this->wishlist->wishlist_by->name,
                        'wishlist_to' => $this->wishlist->users_product->name,
                        'product_name' => $this->wishlist->product->name,
                        'product_slug' => $this->wishlist->product->slug,
                        'wishlist_by_email' => $this->wishlist->wishlist_by->email,
                        'wishlist_to_email' => $this->wishlist->users_product->email,
                    ]);
    }
}
