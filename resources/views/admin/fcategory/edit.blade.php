@extends('admin.fcategory.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update FCategory</div>
                <div class="panel-body">
                    <form method="POST" action="{{ route('fcategory.update',$data->id) }}" aria-label="mainmenu" enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{ method_field('PATCH') }}
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $data->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Children of </label>
                            <div class="col-md-6">
                                <select class="form-control" name="parent_id">
                                    @if($data->parent_id == null)<option value="">Null</option>@endif
                                    @foreach($categories as $value)
                                    @if($data->parent_id == $value->id)
                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                    @else
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>@endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Font</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('font') ? ' is-invalid' : '' }}" name="font" value="{{ $data->font }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Color</label>

                            <div class="col-md-6">
                                <input id="color" type="text" class="form-control{{ $errors->has('color') ? ' is-invalid' : '' }}" name="color" value="{{ $data->color }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Description</label>

                            <div class="col-md-6">
                                <textarea id="color" type="text" class="form-control" name="desc" value="{{ $data->desc }}">{{ $data->desc }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Image</label>

                            <div class="col-md-6">
                                <input type="file" class="form-control" name="image"/>
                            </div>
                        </div>

        <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                  <input name="featured" type="checkbox" {{$data->featured?"checked":""}}>
                      Featured
                    </label>
                  </div>
              </div>
          </div>

          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                    <input name="status" type="checkbox" {{$data->status?"checked":""}}>
                      Status
                    </label>
                  </div>
              </div>
          </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                   Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
