@extends('site.partials.main_index')
@section('title', 'Login')
@section('content')  
<body class="page-template page-template-template-login page-template-template-login-php page page-id-28">
@include('site.partials.navbar')
<section class="inner-page-content border-bottom top-pad-50">
    <div class="login-register login-register-v1">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-11 col-sm-12 center-block">

                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif

                    

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="social-login border-bottom">
                                <h5 class="text-uppercase text-center">
                                Log in or sign up with a social account                             </h5>
                                <!--NextendSocialLogin-->
                                                                <!--AccessPress Socil Login-->
                                    <div class="apsl-login-networks theme-1 clearfix">
                                <div class="social-networks">
                                    
                                    
                                <a href="/login/facebook"><img src="{{asset('frontend/social-icons/facebook.png')}}" height="50" width="50"></a><span></span>
                                <a href="/login/google"><img src="{{asset('frontend/social-icons/google.png')}}" height="50" width="50"></a><br><br>
                        
                    </a>
                            </div>
    </div>
                                <!--AccessPress Socil Login-->
                                <!--Social Plugins-->
                                <div class="social-login-or">
                                    <span>OR</span>
                                </div>
                            </div>
                        </div><!--col-lg-12-->
                    </div><!--row-->
                    <div class="row">
                        <div class="col-lg-8">
                            <form method="POST" action="{{ route('login') }}" id="classiera_login_form" name="classiera_login_form" aria-label="{{ __('Login') }}">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3 single-label">
                                            <label for="username">Email : <span class="text-danger">*</span></label>
                                        </div><!--col-lg-3-->
                                        <div class="col-lg-9">
                                            <div class="inner-addon left-addon">
                                                <i class="left-addon form-icon fas fa-envelope"></i>
                                                <input type="text" id="email" name="email" class="form-control form-control-md sharp-edge" placeholder="Your Email">
                                                <div>{{ $errors->first('email') }}</div>
                                            </div>
                                        </div><!--col-lg-9-->
                                    </div><!--row-->
                                </div><!--UserName-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3 single-label">
                                            <label for="password">Password : <span class="text-danger">*</span></label>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="inner-addon left-addon">
                                                <i class="left-addon form-icon fas fa-lock"></i>
                                                <input id="password" type="password" name="password" class="form-control form-control-md sharp-edge" placeholder="Enter Password" data-error="Password required" value="{{old('password')}}">
                                                <div>{{ $errors->first('password') }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--Password-->
                                <div class="col-lg-9 pull-right flip">
                                    <div class="form-group clearfix">
                                        <div class="checkbox pull-left flip">
                                            <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label for="remember">Remember me</label>
                                        </div>
                                        <p class="forget-pass pull-right flip">
                                            <a href="{{ route('password.request') }}">Forget Password?</a>
                                        </p>
                                    </div>
                                    <!--Google-->
                                                                        
                                    <!--Google-->
                                    <div class="form-group">
                                        <input type="hidden" id="submitbtn" name="submit" value="Login" />              
                                        <button class="btn btn-primary sharp btn-md btn-style-one" id="edit-submit" name="op" value="Login" type="submit">LOGIN NOW</button>
                                    </div>                                  
                                    <div class="form-group">
                                        <p>If you don’t have account? 
                                            <a href="{{ route('register') }}">Create an account.</a>
                                        </p>
                                    </div>
                                </div><!--Rememberme-->
                            </form>
                        </div><!--col-lg-8-->
                    </div><!--row-->
                </div><!--col-lg-10-->
            </div><!--row-->    
        </div><!--container-->  
    </div><!--login-register login-register-v1-->
</section>
<!-- page content -->
@include('site.partials.footer')
@endsection