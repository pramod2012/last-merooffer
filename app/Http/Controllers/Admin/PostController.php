<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\PostContract;
use App\Contracts\McategoryContract;
use App\Contracts\TagContract;
use App\Contracts\AuthorContract;
use App\Http\Controllers\BaseController;

class PostController extends BaseController
{
    private $page="admin.post.";

    protected $postRepo;
    protected $mcategoryRepo;
    protected $tagRepo;
    protected $authorRepo;

    public function __construct(
        PostContract $postRepo,
        McategoryContract $mcategoryRepo,
        TagContract $tagRepo,
        AuthorContract $authorRepo
    )
    {
        $this->postRepo = $postRepo;
        $this->mcategoryRepo = $mcategoryRepo;
        $this->tagRepo = $tagRepo;
        $this->authorRepo = $authorRepo;
    }

    public function index()
    {
        $list = $this->postRepo->list('order', 'asc');
        $posts = $this->postRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('Posts', 'List of Posts');
        return view($this->page.'index',compact('posts'));
    }

    public function create()
    {
        $mcategories = $this->mcategoryRepo->list();
        $tags = $this->tagRepo->list();
        $authors = $this->authorRepo->list();
        $this->setPageTitle('Posts', 'Create post');
        return view($this->page.'create',compact('mcategories','tags','authors'));
    }

    public function store(Request $request)
    {
        $data = $this->postRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('post.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $post = $this->postRepo->findById($id);
        $mcategories = $this->mcategoryRepo->list();
        $tags = $this->tagRepo->list();
        $authors = $this->authorRepo->list();
        $this->setPageTitle('Posts', 'Edit post');
        return view($this->page.'edit', compact('post','mcategories','tags','authors'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->postRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('post.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->postRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('post.index', 'Record deleted successfully' ,'success',false, false);
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('posts');
        foreach($allArticles as $_article){
            $article = $this->postRepo->findById($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
