import moment from "moment";
import Vue from "vue";

export default {
    GAPI: "AIzaSyBV-GD5AgIYUu8op1bHFZlXEfpdy3X7MyE",
    formatDate(date) {
        return moment(date).format("dddd, MMMM Do YYYY");
    },
    formatNumber(number) {
        return parseInt(number).toLocaleString("en-IN");
    },
    formatObject(obj) {
        let o = {};
        for (let i in obj) {
            if (obj[i] && !isNaN(obj[i])) {
                o[i] = parseFloat(obj[i]);
            } else if (obj[i] && obj[i] != "null" && obj[i] != "") {
                o[i] = obj[i];
            }
        }
        return o;
    },
    event: new Vue(),
    prepareIfInEditMode(product) {}
};
