<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\JobcategoryContract;
use App\Http\Controllers\BaseController;

class JobcategoryController extends BaseController
{
    private $page="admin.jobcategory.";

    protected $jobcategoryRepo;

    public function __construct(
        JobcategoryContract $jobcategoryRepo
    )
    {
        $this->jobcategoryRepo = $jobcategoryRepo;
    }

    public function index()
    {
        $datas = $this->jobcategoryRepo->list('order', 'asc');
        $this->setPageTitle('Job Categories', 'List of Job Categories');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('Job Categories', 'Create Job Categories');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->jobcategoryRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('jobcategory.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->jobcategoryRepo->findById($id);
        $this->setPageTitle('Job Categories', 'Edit jobcategory');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->jobcategoryRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('jobcategory.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->jobcategoryRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('jobcategory.index', 'Record deleted successfully' ,'success',false, false);
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('datas');
        foreach($allArticles as $_article){
            $article = $this->jobcategoryRepo->findById($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
