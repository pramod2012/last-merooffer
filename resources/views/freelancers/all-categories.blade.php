@extends('site.partials.main_index')
@section('title','List of All Categories')
@section('content')
<body class="archive category category-uncategorized category-1 logged-in">
    @include('freelancers.partials.navbarwork')
@include('freelancers.partials.searchwork')
    <!--search-section--><!-- page content -->
                <!-- advertisement -->
 <section class="section-pad-80 category-v5">
        <div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-9 center-block">
                    <h1 class="text-capitalize">Categories</h1>
                                        <p>Get work done in different categories </p>
                                    </div><!--col-lg-7-->
            </div><!--row-->
        </div><!--container-->
    </div><!--section-heading-v1-->
        <div class="container">
        <ul class="list-inline list-unstyled categories">
            
        @foreach(\App\Models\Freelancers\Fcategory::where('parent_id',null)->get() as $value)
            <li class="match-height">
                <div class="category-content">                  
                        <i style="color:{{$value->color}};" class="{{$value->font}}"></i>
                                            <h4>
                        <a href="{{ route('freelancers.category', $value->slug) }}">
                            {{$value->name}}                        </a>
                    </h4>
                </div><!--category-box-->
            </li><!--col-lg-4-->
            @endforeach
                        
        </ul>
    </div>
</section>
<!-- page content -->

<section class="classiera-premium-ads-v5 border-bottom section-pad-80">
    <div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 center-block">
                    <h3 class="text-capitalize">PREMIUM WORKS</h3>
                    <p>Top Preminum Works of Nepal</p>
                </div>
            </div>
        </div>
    </div>
    <div style="overflow: hidden; padding-top:10px;">
        <div style="margin-bottom: 40px;">          
            <div id="owl-demo" class="owl-carousel premium-carousel-v5" data-car-length="5" data-items="5" data-loop="true" data-nav="false" data-autoplay="true" data-autoplay-timeout="3000" data-dots="false" data-auto-width="false" data-auto-height="true" data-right="false" data-responsive-small="1" data-autoplay-hover="true" data-responsive-medium="2" data-responsive-large="5" data-responsive-xlarge="7" data-margin="10">
                @foreach(\App\Models\Freelancers\Fjob::where('feature',1)->limit(10)->get() as $value)
                <div class="classiera-box-div-v5 item match-height">
                    <figure>
                        <div class="premium-img">@if($value->jobtype->id !== 1)
                                    <div class="featured-tag">
                                        <span class="left-corner"></span>
                                        <span class="right-corner"></span>
                                        <div class="featured">
                                            <p>{{$value->jobtype->name}}</p>
                                        </div>
                                    </div>@endif
                           
                    <img class="media-object" src="{{ asset('frontend/images/empty.png') }}" alt="{{$value->name}}">
                    
                                                        <span class="hover-posts">
                                <a href="{{route('freelancers.show',$value->slug)}}">view project</a>
                            </span>
                        <span class="price">
                            Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif
                        </span>
                                <span class="classiera-buy-sel">
                            {{ ucwords($value->duration->name) }}</span>
                                                    </div><!--premium-img-->
                        <figcaption>
                            <h5><a href="{{route('freelancers.show',$value->slug)}}">{{$value->name}}</a></h5>
                            <div class="category">
                                <span>
                    Category : 
                    <a href="#" title="View all posts in {{ ucwords($value->fcategory->name) }}">{{ ucwords($value->fcategory->name) }}</a></span>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                @endforeach
                
                                                            </div>
        </div>
        <div class="navText">
            <a class="prev btn btn-primary radius outline btn-style-five">
                <i class="icon-left zmdi zmdi-arrow-back"></i>
                Previous            </a>
            <a class="next btn btn-primary radius outline btn-style-five">
                Next                <i class="icon-right zmdi zmdi-arrow-forward"></i>
            </a>
        </div>
    </div>
</section>


@include('site.partials.footer')
@endsection