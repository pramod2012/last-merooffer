<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\CityContract;
use App\Http\Controllers\BaseController;

class CityController extends BaseController
{
    private $page="admin.city.";

    protected $cityRepo;

    public function __construct(
        CityContract $cityRepo
    )
    {
        $this->cityRepo = $cityRepo;
    }

    public function index()
    {
        $list = $this->cityRepo->list('created_at', 'desc');
        $cities = $this->cityRepo->paginateArrayResults($list->all(), 10);
        $this->setPageTitle('Cities', 'List of Cities');
        return view($this->page.'index',compact('cities'));
    }

    public function create()
    {
        $this->setPageTitle('Cities', 'Create City');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->cityRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('cities.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $city = $this->cityRepo->findById($id);
        $this->setPageTitle('Cities', 'Edit City');
        return view($this->page.'edit', compact('city'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->cityRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('cities.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->cityRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('cities.index', 'Record deleted successfully' ,'success',false, false);
    }
}
