<?php

namespace App\Contracts;

/**
 * Interface CategoryContract
 * @package App\Contracts
 */
interface McategoryContract extends MyBaseContract
{
	public function findBySlug($slug);
	
    public function treeList();
}
