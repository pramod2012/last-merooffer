@extends('site.partials.main_index')
@section('title')
Jobs from {{$user->name}}
@endsection
@section('content')
<body class="archive category category-uncategorized category-1 logged-in">
    @include('freelancers.partials.navbarwork')
@include('freelancers.partials.searchwork')
    <!--search-section--><!-- page content -->
<section class="inner-page-content border-bottom top-pad-50">
    <div class="container">
        <ul class="breadcrumb"><li><a rel="v:url" property="v:title" href="{{route('index')}}"><i class="fas fa-home"></i></a></li>&nbsp; <li class="cAt"><a rel="v:url" href="{{url('freelancers')}}">Freelancers</a></li>&nbsp; <li class="cAt"><a rel="v:url" href="{{route('freelancers.user',$user->slug)}}">User : {{$user->name}}</a></li> </ul>
        <div class="row">
            <div class="col-md-8 col-lg-9">
                <!-- advertisement -->
<section class="classiera-advertisement advertisement-v5 section-pad-80 border-bottom">
    <div class="tab-divs">
        <div class="view-head">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-sm-9 col-xs-10">
                        <div class="tab-button">
                            <ul class="nav nav-tabs" role="tablist">                                
                                <li role="presentation" @if(Request::get('budget_avg') != 'asc' && Request::get('budget_avg') != 'desc' && Request::get('views') != 'desc' ) class="active" @endif>
                                    <a href="{{route('freelancers.user',$user->slug)}}" aria-controls="all">
                                        Recent
                                        <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation" @if(Request::get('budget_avg') == 'asc') 
                                    class="active" @endif>                                    
                                    <a href="{{ route('freelancers.user',['slug' => $user->slug, 'budget_avg' =>'asc']) }}" aria-controls="random">
                                        Budget ASC                                      <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation" @if(Request::get('budget_avg') == 'desc') 
                                    class="active" @endif>                                    
                                    <a href="{{ route('freelancers.user',['slug' => $user->slug, 'budget_avg' =>'desc']) }}" aria-controls="random">
                                        Budget DESC                                      <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation" @if(Request::get('views') == 'desc') 
                                    class="active" @endif>                                   
                                    <a href="{{ route('freelancers.user',['slug' => $user->slug, 'views' =>'desc']) }}" aria-controls="popular">
                                       Popular                                    <span class="arrow-down"></span>
                                    </a>
                                </li>
                            </ul><!--nav nav-tabs-->
                        </div><!--tab-button-->
                    </div><!--col-lg-6 col-sm-8-->
                    <div class="col-lg-4 col-sm-3 col-xs-2">
                        <div class="view-as text-right flip">
                            <a id="grid" class="grid active" href="#"><i class="fas fa-th"></i></a>
                            <a id="list" class="list " href="#"><i class="fas fa-th-list"></i></a>                          
                        </div><!--view-as tab-button-->
                    </div><!--col-lg-6 col-sm-4 col-xs-12-->
                </div><!--row-->
            </div><!--container-->
        </div><!--view-head-->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="all">
                <div class="container products">
                    <div id="load" class="row">
                   <!--FeaturedPosts-->@forelse ($works as $value)
                        <div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
                        <div class="classiera-box-div classiera-box-div-v5">
                            <figure class="clearfix">
                                <div class="premium-img">@if($value->jobtype->id !== 1)
                                    <div class="featured-tag">
                                        <span class="left-corner"></span>
                                        <span class="right-corner"></span>
                                        <div class="featured">
                                            <p>{{$value->jobtype->name}}</p>
                                        </div>
                                    </div>@endif
                                    
                    <img class="img-responsive" src="{{ asset('frontend/images/empty.png') }}" alt="{{$value->name}}">
                    
                                    <span class="hover-posts">                                          
                                        <a href="{{route('freelancers.show',$value->slug)}}">View Project</a>
                                    </span>
                                    <span class="price">
                                        Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif
                                    </span>
                                        <span class="classiera-buy-sel">
                                            {{$value->duration->name}}             </span>
                                        </div><!--premium-img-->
                                        <div class="detail text-center">
                                            <span class="price">
                                                Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif            </span>
                                                <div class="box-icon">
                                                    @if($value->user->show_email)
                                                    <a href="mailto:{{ $value->user->email }}?subject">
                                                        <i class="fas fa-envelope"></i>
                                                    </a>@endif
                                                    @if($value->user->show_mobile)
                                                    <a href="tel:{{ $value->user->mobile}}"><i class="fas fa-phone"></i></a>@endif
                                                </div>
                                                <a href="{{route('freelancers.show',$value->slug)}}" class="btn btn-primary outline btn-style-five">View</a>
                                            </div><!--detail text-center-->
                                            <figcaption>
                                                <span class="price visible-xs">Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif
                                                </span>
                                                <h5><a href="{{route('freelancers.show',$value->slug)}}">{{$value->name}}</a></h5>
                                                <div class="category">
                                                    <span>
                                                        Category :
                                
                    <a href="{{route('freelancers.category',$value->fcategory->slug)}}" title="View all posts in {{$value->fcategory->name}}">{{$value->fcategory->name}}</a>
                    
                 </span>
                                                        <span>Location : 
                                                            <a href="#" title="View all posts in sdf">{{$value->address}}</a>
                                                        </span>
                                                    </div>
                                                    <p class="description">{{$value->body}}</p>
                                                </figcaption>
                                            </figure>
                                        </div><!--row-->
                                    </div><!--col-lg-4-->
@empty
<div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
    <div class="classiera-box-div classiera-box-div-v5">
        <figure class="clearfix">
            <div class="premium-img">
                    <img class="img-responsive" src="{{ asset('frontend/images/empty.png') }}" alt="Submit Projects">
                                    <span class="hover-posts">                                          
                    <a href="{{route('project.create')}}">Submit Project</a>
                </span>
                                    <span class="price">
                        Go On, It's Easy</span>
                                                <span class="classiera-buy-sel">
                Want to see your project here??</span>
                            </div><!--premium-img-->
            <div class="detail text-center">
                                <span class="price">Go On, It's Easy</span>
                                <div class="box-icon">
                                    </div>
                <a href="{{route('project.create')}}" class="btn btn-primary outline btn-style-five">Post Work</a>
            </div><!--detail text-center-->
            <figcaption>
                <span class="price visible-xs">Go On, It's Easy
            </span>
            <h5><a href="{{route('project.create')}}">Start Hiring</a></h5>
                <div class="category">
                </div>
                <p class="description">Post project and find your employee you're looking for in and around your local community. Go on, It's Quick and Easy .</p>
            </figcaption>
        </figure>
    </div><!--row-->
</div><!--col-lg-4-->
@endforelse
<br>
</div><!--row-->
<div class="classiera-pagination">
    <nav aria-label="Page navigation">
        <div id="pagination">
       {!!$works->links()!!}
        </div>
    </nav>
</div><!--tab-divs-->
</section><!-- advertisement -->
            </div><!--col-md-8-->
            <div class="col-md-4 col-lg-3">
                <aside class="sidebar">
                    <div class="row">
                        <!--subcategory-->
                        <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                            <div class="widget-box">
                                <div class="widget-title">
                                    <h4>
                                        <i class="fas fa-paper-plane" style="color:#c29bc2;"></i>
                                        Categories                                 </h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="category">
                                        @foreach(\App\Models\Freelancers\Fcategory::all() as $value)
                                        <li>
                                            <a href="{{ route('freelancers.category',$value->slug) }}">
                                                <i class="fas fa-angle-right"></i>
                                               {{$value->name}}                                               <span class="pull-right flip">
                                                    {{$value->fjobs->where('status',1)->count()}}
                                                    </span>
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                                                <!--subcategory-->
                                                    <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                                <div class="widget-box">
                                <!--SearchForm-->
@include('freelancers.partials.advanced_search')
<!--SearchForm-->
</div>
                            </div>        
                            </div><!--row-->
                </aside>
            </div><!--row-->
        </div><!--row-->
    </div><!--container-->
</section>  
<!-- page content -->

@include('site.partials.footer')
@section('page-script')
    <script type='text/javascript' src="{{ asset('frontend/js/validator.min.js') }}"></script>
@stop
@endsection