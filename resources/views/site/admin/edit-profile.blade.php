@extends('site.partials.main_index')
@section('title','Edit Profile')
@section('content')
<body class="page-template page-template-template-submit-ads page-template-template-submit-ads-php page page-id-44 logged-in">
@include('site.partials.navbar')
<section class="user-pages section-gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4">
                @include('site.partials.sidebar')
            </div><!--col-lg-3 col-md-4-->
            <div class="col-lg-9 col-md-8 user-content-height">
                <div class="user-detail-section section-bg-white">
                    @include('partials.alert')
                    @if(Auth::user()->cell_activated==0)
                    <div class="user-social-profile-links">
                        <h4 class="user-detail-section-heading text-uppercase">
                            Mobile Phone Verification                        </h4>
                        <ul class="list-unstyled list-inline">
                            
      <b>Please complete your profile and fill up your mobile number.
        
        
                        </ul>
                    </div>
                    @endif

                    <div class="user-ads user-profile-settings">
                        <h4 class="user-detail-section-heading text-uppercase">
                            Edit Profile                        </h4>                       
                        <form role="form" method="POST" action="{{route('updateProfile')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PATCH')}}
                            <input type="hidden" name="id" value="{{Auth::user()->id}}">
                            <!-- upload avatar -->
                            <div class="media">
                                <div class="media-left uploadImage">
                                    <img class="media-object img-circle author-avatar" src="{{@asset('images_small/'. Auth::user()->image)}}" style="height: 150px; width: 150px;" alt="{{Auth::user()->name}}">
                                </div>
                                <div class="media-body">
                                    <h5 class="media-heading text-uppercase">
                                        Update your Profile Photo                                   </h5>
                                    <p>Update your profile avatar manually.</p>
                                    <div class="choose-image">
                                        <input type="file" id="file-1" name="image" class="inputfile inputfile-1 author-UP" data-multiple-caption="{count} files selected" multiple />
                                        <label for="file-1" class="upload-author-image"><i class="fas fa-camera"></i>
                                            <span>Upload Profile Photo</span>
                                        </label>
                                    </div>                                  
                                </div>
                            </div><!-- /.upload avatar -->
                            <!-- user basic information -->
                            <h4 class="user-detail-section-heading text-uppercase">
                                Basic Information                           </h4>
                            <div class="form-inline row form-inline-margin">
                                <div class="form-group col-sm-5">
                                    <label for="first-name">Name</label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="name" class="form-control form-control-sm" placeholder="" value="{{Auth::user()->name}}">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                </div><!--Firstname-->
                                
                                <div class="form-group col-sm-5">
                                    <label for="first-name">Mobile Number <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="mobile" class="form-control form-control-sm" placeholder="" value="{{Auth::user()->mobile}}">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </div>
                                </div><!--Firstname-->
                                <div class="form-group col-sm-5">
                                    <label for="last-name">Phone</label>
                                    <div class="inner-addon">
                                        <input type="text" id="last-name" name="phone" class="form-control form-control-sm" placeholder="" value="{{Auth::user()->phone}}">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </div>
                                </div><!--Last name-->
                                <div class="form-group col-sm-5">
                                    <label for="last-name">Address</label>
                                    <div class="inner-addon">
                                        <input type="text" id="last-name" name="address" class="form-control form-control-sm" placeholder="" value="{{Auth::user()->address}}">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </div>
                                </div><!--Last name-->
                                
                                <div class="form-group col-sm-5">
                                    <div class="checkbox">
                                        <input type="hidden" name="show_mobile" value="0">
                                        <input type="checkbox" name="show_mobile" id="gdpr" value="1" {{Auth::user()->show_mobile ==1?'checked':''}}>
                                        <label for="gdpr">Show Mobile Number</label>
                                    </div>
                                </div>
                                <div class="form-group col-sm-5">
                                    <div class="checkbox">
                                        <input type="hidden" name="show_phone" value="0">
                                        <input type="checkbox" name="show_phone" id="gdpr1" value="1" {{ Auth::user()->show_phone ==1?'checked':'' }}>
                                        <label for="gdpr1">Show Phone Number</label>
                                    </div>
                                </div>
                                <div class="form-group col-sm-5">
                                    <div class="checkbox">
                                        <input type="hidden" name="show_email" value="0">
                                        <input type="checkbox" name="show_email" id="gdpr2" value="1" {{ Auth::user()->show_email ==1?'checked':'' }}>
                                        <label for="gdpr2">Show Email</label>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- user basic information -->
                            <button type="submit" name="op" value="submit" class="btn btn-primary sharp btn-sm btn-style-one">Update Now</button>
                            <!-- Update your Password -->
                        </form>
                    </div><!--user-ads user-profile-settings-->
                </div><!--user-detail-section-->
            </div><!--col-lg-9-->
        </div><!--row-->
    </div><!--container-->  
</section><!--user-pages section-gray-bg-->

@include('site.partials.footer')
@endsection