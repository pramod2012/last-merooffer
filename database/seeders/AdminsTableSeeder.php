<?php

namespace Database\Seeders;

use App\Models\Admin;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\Day;
use App\Models\Pricetype;
use App\Models\Pricelabel;
use App\Models\Adtype;
use App\Models\City;
use App\JobCategory;
use App\Skill;
use App\Industry;
use App\Level;
use App\Positiontype;
use App\Models\Reportname;
use App\Models\Mcategory;
use App\Models\Condition;
use App\Models\Freelancers\Duration;
use App\Models\Freelancers\Fcategory;
use App\SocialIdentity;
use App\Models\Assured;
use App\Firmuser;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ne_NP');

        \App\Models\Admin::create([
            'name'      =>  $faker->name,
            'email'     =>  'hr@merooffer.com',
            'password'  =>  bcrypt('khandbari11'),
        ]);

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => $faker->firstName,
            'mobile' => ('9849551992'),
            'email' => ('pramodbhandari68@gmail.com'),
            'password' => bcrypt('password'),
            'activation_key' => str_random(6),
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'ip' => request()->ip(),
            'cell_activated' => 1,
            ]);
        }
        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Delta Tech Pvt. Ltd.'),
            'mobile' => ('9804067116'),
            'phone' => ('021531317'),
            'email' => ('career@deltatechnepal.com'),
            'password' => ('$2y$10$1myhvxKNr9sFEgRULv1wge6Eb/I1LTUYxa33lGQcs6f9Na/2rk7b6'),
            'activation_key' => ('DMb1bI'),
            'ip' => request()->ip(),
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'cell_activated' => 1,
            ]);
        }
        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Joseph Karki'),
            'mobile' => ('9869374448'),
            'email' => ('josephkarki8@gmail.com'),
            'password' => ('$2y$10$/JJtxvlmEUNmJnSVdecEz.Kb.EN25VPRHYIkHZNzJxLJj0vtSdHe.'),
            'activation_key' => ('dENw47'),
            'ip' => request()->ip(),
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'cell_activated' => 1,
            ]);
        }
        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Anish Raj'),
            'mobile' => ('+9779849441738'),
            'email' => ('anish9849@gmail.com'),
            'password' => ('$2y$10$colxYcaViqdJ9ZJit8PKaObrueB47lja16a2FJ6k2B6IFcjtERbNq'),
            'activation_key' => ('eshklD'),
            'ip' => request()->ip(),
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'cell_activated' => 1,
            ]);
        }
        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Kanchen Kattel'),
            'mobile' => ('9804058545'),
            'email' => ('kanchan_kattel@yahoo.com'),
            'password' => ('$2y$10$RTXDRgolqfZMvQtmxbSlwuIEauG4dVX7DNoBSZe95xvbRbIuxmi4K'),
            'activation_key' => ('UdxY2a'),
            'ip' => request()->ip(),
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'cell_activated' => 1,
            ]);
        }
        
        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Saurav Shrestha'),
            'mobile' => ('9860305485'),
            'email' => ('sauravshrestha13@gmail.com'),
            'password' => ('$2y$10$QRFQ7WCBSRmc1.CiBxQWFuPdhy.syLC0dlMOAQjDpDmSTtf.cxx5e'),
            'activation_key' => ('vzoRXz'),
            'ip' => request()->ip(),
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'cell_activated' => 1,
            ]);
        }
        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('i-creation'),
            'mobile' => ('9843274641'),
            'email' => ('apply@icreationtech.com'),
            'password' => bcrypt('icreation2020'),
            'activation_key' => ('lrthLA'),
            'ip' => request()->ip(),
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'cell_activated' => 1,
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Pujan Shrestha'),
            'mobile' => ('9849824952'),
            'email' => ('world_history18@yahoo.com'),
            'password' => ('$2y$10$Bbpkx6iL2It9n/Hp7UObhOKjNdZ6VkJeJIycjzOcr5eKQRijmHyA6'),
            'activation_key' => ('wUj26a'),
            'ip' => ('113.199.242.52'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('TY5IaEMaTGZ55L2khbgu3nyHYkbQP9iXh0x3LJTguA9YEHvLV9OqNsVRzgMt'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Pradhan Papers'),
            'mobile' => ('9857044022'),
            'email' => ('pradhanpapers2020@gmail.com'),
            'password' => ('$2y$10$nKulyhSTv.DL7QCGKtYs0u5LgGsfDtdOn5T10VRbCm/z04sFOO.H.'),
            'activation_key' => ('YkeSbj'),
            'ip' => ('113.199.242.52'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('WLKO04TNDGp6bu96R5P3jBBu6cahkTfDuRY59JUZrS4Az7Ud2RONpo0v3IHm'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Kshitiz Pradhan'),
            'email' => ('kshitizpradhan10@gmail.com'),
            'password' => ('$2y$10$4atCVDdroULBQ2Pd5nt5D.xY5RWgObkWXWreOAPpVwpLdw2.R8W2a'),
            'activation_key' => ('Kp51Gz'),
            'ip' => ('27.34.48.98'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('GFbk4i6Mj4nw7QnHTnDAwiNXnAEs6NWjKUofAtKf4VCDykhFyrEdh7c7DmLh'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('mother care'),
            'email' => ('mcare967@gmail.com'),
            'password' => ('$2y$10$16gGT88zWrht/BSCU38HxeuHQkqU/Bm7rUr5O8bQ0hIDAbZJaKJl.'),
            'activation_key' => ('dpJ0ck'),
            'ip' => ('110.34.15.137'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('YbBgNbLIHAurCU4ps1lwHREQqhwiPwl7XvoA5TQWXvSq3ZNO15zpyytiwXt5'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Raju Lamichhane'),
            'mobile' => ('9861977022'),
            'email' => ('rajulamichhane204@gmail.com'),
            'image' => ('users/rf0NdQXPT3LvRqKcS16Xka5pt.JPG'),
            'password' => ('$2y$10$8M/NMZis6r3S5A/mbkIZ.es3lcqk/rojiyLcX4jdESWJ3deMsJWki'),
            'activation_key' => ('TgdbMs'),
            'ip' => ('27.34.20.76'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('XFWAGAy7eOriZSVnNDh3p50QqatW24rR5jEoGKQOz971LhgH8FRlAxudMX9A'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Buenostore.np'),
            'mobile' => ('9849575165'),
            'email' => ('bidhankhawas23@gmail.com'),
            'image' => ('users/k83GVfEDOWib61DwNbfl6urbA.jpeg'),
            'password' => ('$2y$10$AWHJxWvsuG.374dIzf5lSOZHsD7oP2P5wFt1soaPzCkDrrWbEEYVO'),
            'activation_key' => ('dPt30h'),
            'ip' => ('27.34.20.76'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('rl8Ub0NeEDILHFopVs6oKaZukAzrBqrSCWArmw2WXBol5fHFAg6wKj8f8ahN'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Ram Chandra'),
            'mobile' => ('9849494516'),
            'email' => ('cram87351@gmail.com'),
            'password' => ('$2y$10$rW3NL60I34OUuTG4cgI3k.WdqCQZmCubXelGeUq37nJJm/SG/WBdG'),
            'activation_key' => ('ZlzZ7p'),
            'ip' => ('120.89.104.45'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('j8kd17rmC04VPkaZ7Ovxpgx6AKIzBDuJFLMDcHHan7817V6pnhNSGJT37AZC'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Gambir Lama'),
            'email' => ('nirvanagambir123@gmail.com'),
            'password' => ('$2y$10$mh2y8ebNXJDEnmlOqZJmj.LiDaqo6QLjpoYFlwmpKcMY/HZphnaPe'),
            'activation_key' => ('AT3usp'),
            'ip' => ('27.34.68.78'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('OMNneg6LicmAzo4Kl89m0MzXTQO4285MWBr5Gibforc8u60AtGpGEFPNxDr0'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('pujan shrestha'),
            'email' => ('spujan573@gmail.com'),
            'password' => ('$2y$10$EhraQi.xgeSfIJKZyCvPleZtHy2/IOJ5gBAVBo6rDOmCnnoTVI6U.'),
            'activation_key' => ('mFtop2'),
            'ip' => ('103.94.220.72'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('2MKCJAAkexzy3PC16KQzBzn9HHj3bBME1d0sN5d9Rlr3K01uZ5VEfCVK20GI'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Sandip tamang'),
            'mobile' => ('9866658865'),
            'email' => ('sandiptamang65886450@gmail.com'),
            'password' => ('$2y$10$pctnaDH6mfZqJmZ/Y86Wz.PWmylohwhUzjPFcaTKUd1FfREhlMMqS'),
            'activation_key' => ('L43uYU'),
            'ip' => ('27.34.20.205'),
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'cell_activated' => 1,
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('GTS S'),
            'mobile' => ('9808091906'),
            'email' => ('gtmobileconcern@gmail.com'),
            'password' => ('$2y$10$ZbiooRodsckpRXEhmCFqDemeeSUI.KAUjYjZubQIlFBJiSCy1CCJG'),
            'activation_key' => ('le9VXa'),
            'ip' => ('27.34.20.129'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('IHHwQ7ByOyVzU4tAO54t8sGc27JIpoJImy81dsZ0exHkjgYTIVlnaBjY87Il'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('S Confectioner'),
            'mobile' => ('9869212429'),
            'email' => ('sconfectionerandpatisserie@gmail.com'),
            'password' => ('$2y$10$7sS3f3OHfRws6HuSLE2XGO0XKX6XUqPe5v8edq0j.FkhgiaKbEj22'),
            'activation_key' => ('JVFosF'),
            'ip' => ('182.93.79.67'),
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'cell_activated' => 1,
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Aman Shrestha'),
            'email' => ('aman.shrestha471@gmail.com'),
            'password' => ('$2y$10$YCsvce6bgpSwEmUt.XAf8.dBM2deUNVzq06Q/kuk84.9rANGN1Bfi'),
            'activation_key' => ('SVwHtu'),
            'ip' => ('27.34.17.8'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('d7aWIHRg3Ddk1nI3ZrphQKv2VgotWGRtmXXFSU6mz1NBDfTFf96wQqPrSjKE'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('aakrish Mainali'),
            'email' => ('aakrishmainali@gmail.com'),
            'password' => ('$2y$10$Ni8y.flEH9i7Ro4tPhTN6OD.I/tYaQXHxWREVJ5gZ/TiGZGYQHeRq'),
            'activation_key' => ('ZNNn4M'),
            'ip' => ('27.34.68.40'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('3QvBzr5rqGIUZipMRukQVnSZCvXZHImQTPxIHSeIAi7KNmAqEofpRU16ayoV'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('pramod'),
            'mobile' => ('9813999208'),
            'email' => ('pramodbhandari069@gmail.com'),
            'password' => ('$2y$10$Y4gyUFe24rjHf65svpPUXe7A7rtQX.hyEhqdpNOupcr8vS3d9k7lO'),
            'activation_key' => ('pymOuv'),
            'ip' => ('27.34.20.93'),
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'cell_activated' => 1,
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('लोकसेवाको राजाहुने चाहाना'),
            'email' => ('khardarbaje@gmail.com'),
            'password' => ('$2y$10$.1pElUK26R.3Kw6dVasXFupvr6qQjkMdfBygKZ94em52KzAySX2r2'),
            'activation_key' => ('HplfqT'),
            'ip' => ('27.34.20.93'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('IN2a0wb5ed0h2Ehtcyeh7Og9LwipcVhCArRdnPhagwW5eVVFt9AoRWCQ3Qer'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Kaisav Bhattarai'),
            'email' => ('coolkeshab222@yahoo.com'),
            'password' => ('$2y$10$WBY/ucB0uCbkzheSazOEhOU3oRXQSC0ZM/4zNkkmQas4/y.wAkBWm'),
            'activation_key' => ('TJ8eBq'),
            'ip' => ('27.34.12.31'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('S49GuJGxw0iHaGlsyZPOo9XtFjMMPDHyTESrKxPWM7ZYPaYm26kTQL6GldDx'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Manish Jung Thapa'),
            'email' => ('manishjung980@gmail.com'),
            'password' => ('$2y$10$4.zVLqldf1Hm/FnfaK5VpO7fW.gLRMVjfhyjW8uasPn78dpRpVEkG'),
            'activation_key' => ('drN4C0'),
            'ip' => ('27.34.68.151'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('iO1ayjK4TSpWxLneXaegtqTqUacLGkKUapxeZFj8pNmIoYECbZC0D21ewJvi'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('शिृ कृष्ण भट्टराई'),
            'email' => ('krishnabhattarai14@yahoo.com'),
            'password' => ('$2y$10$iMPq49FBvP9JfTVuBXF5Z.lWfTFub9oDasNepzVW0bNf/c24BTPwu'),
            'activation_key' => ('VBsw4L'),
            'ip' => ('27.34.12.20'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('R2RLlJSEdmTyHw99w3TFEUJ7NygRPNGeIPObXOvQzwNpinpUMgExT3x7Pj9Y'),
            ]);
        }

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\User::create([
            'name' => ('Fiber Jobs'),
            'email' => ('jobsatpspl@gmail.com'),
            'password' => ('$2y$10$cHICg.RVkQjKazi.ClW7oOIfuCU5f/MgXWbUcaBxIK0NMChmlhqSu'),
            'activation_key' => ('Re6N19'),
            'ip' => ('27.34.12.26'),
            'cell_activated' => 1,
            'email_verified_at' => ('2020-10-30 15:27:20'),
            'remember_token' => ('R2RLlJSEdmTyHw99w3TFEUJ7NygRPNGeIPObXOvQzwNpinpUMgExT3x7Pj9Y'),
            ]);
        }

        \App\OrderStatus::create([
            'name' => ('Ordered'),
            'color' => ('green'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 1,
            'provider_name' => ('facebook'),
            'provider_id' => ('3016258721744330'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 1,
            'provider_name' => ('google'),
            'provider_id' => ('101716233686111298599'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 8,
            'provider_name' => ('facebook'),
            'provider_id' => ('3634179079971709'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 9,
            'provider_name' => ('google'),
            'provider_id' => ('106110588256003720257'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 10,
            'provider_name' => ('google'),
            'provider_id' => ('113400656993196451881'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 11,
            'provider_name' => ('google'),
            'provider_id' => ('115601249912182821437'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 12,
            'provider_name' => ('google'),
            'provider_id' => ('103203790273154611143'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 13,
            'provider_name' => ('google'),
            'provider_id' => ('116423431068988074889'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 14,
            'provider_name' => ('google'),
            'provider_id' => ('108559602430157403814'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 15,
            'provider_name' => ('google'),
            'provider_id' => ('108977045803289557691'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 16,
            'provider_name' => ('google'),
            'provider_id' => ('110463831025115387920'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 18,
            'provider_name' => ('google'),
            'provider_id' => ('117751946393128085798'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 20,
            'provider_name' => ('google'),
            'provider_id' => ('107716984682218308664'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 21,
            'provider_name' => ('google'),
            'provider_id' => ('102338328564744304768'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 23,
            'provider_name' => ('facebook'),
            'provider_id' => ('1040680776447804'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 24,
            'provider_name' => ('facebook'),
            'provider_id' => ('10223350192958437'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 25,
            'provider_name' => ('facebook'),
            'provider_id' => ('3531032020315467'),
            ]);

        \App\SocialIdentity::create([
            'user_id' => 26,
            'provider_name' => ('facebook'),
            'provider_id' => ('4070431252984119'),
            ]);

        \App\Firmuser::create([
            'name' => ('i-creation'),
            'logo' => ('firmusers/81799NXxyQ6gojGJijzl5IVwq.jpg'),
            'url' => ('http://icreationtech.com'),
            'desc' => ('We believe in evolution, expansion, and extension. Our team works for your professional success, dedicating our time and energy to designing innovative ideas for business houses to drive growth and stand out in a virtual world. Our main objective is to provide our clients with leading ideas and leverage the use of technology to soothe the process of achieving business success. We are a research-based technology company with advanced data analytics using the latest technology and digital marketing for the client’s organizational growth.'),
            'user_id' => 7,
            ]);

        \App\Firmuser::create([
            'name' => ('Incube Technologies'),
            'logo' => ('firmusers/a0j5PdQQRPmCo8kA1Tkp3sszj.png'),
            'url' => ('www.incubeweb.com'),
            'desc' => (''),
            'user_id' => 6,
            ]);

        \App\Firmuser::create([
            'name' => ('Delta Tech'),
            'logo' => ('firmusers/VFWnxJQvUftiZ9yFS33dq8Wzd.png'),
            'url' => ('http://dtechnepal.com/'),
            'desc' => ('Delta Tech is a Software Development Company based in Nepal. Established in 2016, we have a brilliant team of software developers, designers and technical support members, and over 1500 happy clients globally. We are a member of RK Golchha Group and Golchha Organization, one of the oldest and most esteemed business houses in Nepal. Golchha Organization, which was established in 1934, employs over 20,000 people across Nepal and has an annual turnover of over 400 million USD

Delta Tech started as a web and mobile app development company. Initially, we entered the tech business with a motive to become a top service-based technology company. After a few months of continuous market research, we realized that many organizations need user-friendly business software to enhance their productivity, performance, and ROI. Thus, we started focusing on building SaaS applications that can solve common business problems.

Our current suite of products include:

1. Delta Sales App

An app to track field sales employees and manage sales of business


2. Delta Inventory

A cloud-based inventory management solution with CRM, smart stock alerts and analytics

3. Delta Sales CRM

Sales CRM software to manage prospective customers and convert more leads into sales


4. Delta Retailer App

Delta Retailer App is an easy-to-use mobile app that connects Retailers, Distributors and Brands, by automating the ordering process for Retail Chains, Kiranas and Mom-and-Pop Stores.

These products are designed to make operations smoother and hasslefree for entrepreneurs and enterprises. Our innovative and user-friendly software can help in transforming the way businesses work and improve productivity. At Delta Tech, we believe customer satisfaction is a crucial factor that determines a company’s success. Considering customer convenience and usability, our software applications ensure easy configuration and setup. Our highly responsive support team will always be at your service to resolve any difficulties while using any of our products. We also provide tutorials, webinars, demo, and other resources so that you can make the best use of our product.

We also provide IT services which include:

Web Designing
E-commerce Solutions
Web Application Development
Mobile App Development
Customized Software Development
Search Engine Optimization (SEO)
Graphics Designing'),
            'email' => ('career@deltatechnepal.com'),
            'phone' => ('9804067116'),
            'user_id' => 2,
            'fb' => ('https://www.facebook.com/deltatechnepal/'),
            'insta' => ('https://www.instagram.com/deltatechnepal'),
            'twitter' => ('https://twitter.com/deltatechnepal'),
            'youtube' => ('https://www.youtube.com/channel/UCDH4jR0cUgoTHV1pDrDoeHg'),
            'linkedin' => ('https://www.linkedin.com/company/13199442/admin/'),
            ]);

        \App\Firmuser::create([
            'name' => ('Fiber Jobs'),
            'logo' => ('firmusers/I2JhwMecfNxtzJGpjgvzOpRAN.png'),
            'url' => ('www.asalnepal.com'),
            'phone' => ('9851155371'),
            'email' => ('jobsatpspl@gmail.com'),
            'user_id' => 27,
            ]);

        \App\Courier::create([
            'name' => ('Courier Service'),
            'description' => ('this is a courier service'),
            'is_free' => 0,
            'cost' => 100,
            'status' => 1,
            ]);

        for ($i = 1; $i <= 1; $i++) {
         \App\Models\Author::create([
            'name' => ('Admin'),
            'phone' => ('9813999208'),
            'email' => ('pramodbhandari68@gmail.com'),
            'remarks' => ('dummy'),
            'username' => ('pramodpb'),
            'status' => 1,
            ]);
        }

        $experiences = [
             'No Experience',
             '1/2 year',
             '0-1 year',
             '1-2 years',
             '2-3 years',
             '3-4 years',
             '4-5 years',
             '5-6 years',
             '6-7 years',
             '7-10 years',
             '10+ years',
         ];
         foreach ($experiences as $value){
                         \App\Experience::create([
                    'name' => $value]);
                }

        $assures = [
             'Warrenty',
             '7 days return policy',
         ];
         foreach ($assures as $value){
                         Assured::create([
                    'name' => $value]);
                }

        $durations = [
             'Less than 1 week',
             '1 week to 2 weeks',
             '2 weeks to 4 weeks',
             '1 months to 2 months',
             '2 months to 3 months',
             '3 months to 4 months',
             '4 months to 5 months',
             '5 months to 6 months',
             'Over 6 months / Ongoing',
             'Unspecified',
         ];
         foreach ($durations as $value){
                         Duration::create([
                    'name' => $value]);
                }
        

        $mcategories = [
             'Legal Policy',
             'Job News',
             'Tech News',
         ];
         foreach ($mcategories as $value){
                         Mcategory::create([
                    'name' => $value]);
                }
        $salarytypes = [
             'Negotiable',
             'Monthly',
             'Daily',
             'Weekly',
             'Hourly',
             'Yearly',
         ];
         foreach ($salarytypes as $salary){
                         \App\Salarytype::create([
                    'name' => $salary]);
                }
        $jobtypes = [
             'General',
             'Featured',
             'Top',
             'Hot',
             'Standard',
             'Platinum',
             'Premium',
         ];
         foreach ($jobtypes as $value){
                         \App\Jobtype::create([
                    'name' => $value]);
                }

        $adtypes = [
             'For Sale',
             'Want to Buy',
             'Want to lease',
             'For Rent',
             'For Hire',
             'Lost and found',
             'An event',
             'Professional Service',
             'Free',
             'Exchange',
         ];
         foreach ($adtypes as $adtype){
                         Adtype::create([
                    'name' => $adtype]);
                }

        $reportname = [
             'This is illegal / fraudulent',
             'This ad is spam',
             'This ad is a duplicate',
             'This ad is in wrong category',
             'This ad goes againt posting rules',
         ];
         foreach ($reportname as $value){
                         Reportname::create([
                    'name' => $value]);
                }
        $conditions = [
             'Unboxed',
             'Brand New',
             'Almost Like New',
             'Excellent',
             'Good / Fair',
             'Not Working',
         ];
         foreach ($conditions as $value){
                         Condition::create([
                    'name' => $value]);
                }

        $cities = [
             'Kathmandu',
             'Bhaktapur',
             'Lalitpur',
             'Chitwan',
             'Biratnagar',
             'Pokhara',
             'Dharan',
             'Butwal',
             'Others'
         ];
         foreach ($cities as $city){
                         City::create([
                    'name' => $city]);
                }

         $levels = [
             'Junior Level',
             'Mid Level',
             'Senior Level',
         ];
         foreach ($levels as $value){
                         Level::create([
                    'name' => $value]);
                }

        $positions = [
             'Full Time',
             'Part Time',
             'Contract',
             'Temporary',
             'Internship',
             'Commission',
         ];
         foreach ($positions as $value){
                         Positiontype::create([
                    'name' => $value]);
                }

        $jobcategories = [
             'Graphics & Design',
             'Programming & Tech',
             'Digital Marketing',
             'Writing & Translation',
             'Marketing & Customer Service',
             'Video & Animation',
             'IT & Telecommunication',
             'Agriculture & Engineering',
             'Banking, Management & Finanace',
             'Protective & Security Services',
             'NGO/INGO & Social Work',
             'Healthcare/Pharma & Biotech',
             'Teaching/Education',
             'Secretarial/Frontoffice & Data Entry',
             'Human Resource & ORG Development',
             'Production & Quality Maintenance',
             'Commercial/Logistics & Supply Chain',
             'Research & Development',
             'Journalism & Media/Editor',
             'Sales & Public Relation',
             'Others'
         ];
         foreach ($jobcategories as $cat){
                         JobCategory::create([
                    'name' => $cat]);
                }
        $industries = [
             'Banks',
             'Construction / Real Estate',
             'Information / Computer / Tech',
             'Software Companies',
             'Education- School & Colleges',
             'Finance Companies',
             'ISP',
             'NGO / INGO / Development Projects',
             'Mandufacturing / Engineering',
             'Travel Agents / Tour Operators',
             'Advertising Agency',
             'Distribution Companies',
             'Poultry / Dairy / Veterinary',
             'Designing / Printing / Publication',
             'BPO / Call Center / ITES',
             'Training Institutes',
             'Architecture / Interior Designing',
             'Security Service Company',
             'Garments / Carpet Industries',
             'Retail / Shops',
             'Logistics / Courier / Air Express Companies',
             'Research Firms'
         ];
         foreach ($industries as $ind){
                         Industry::create([
                    'name' => $ind]);
                }

        

        $days = [
             '15 Days',
             '30 Days',
             '45 Days',
             '60 Days'
         ];
         foreach ($days as $day){
                         Day::create([
                    'name' => $day]);
                }

    $pricetypes = [
             'Price Negotiable',
             'Fixed Price',
             'Price On Call',
             'Price On Email'
         ];
         foreach ($pricetypes as $value){
                         Pricetype::create([
                    'name' => $value,]);
                }


                $data = [
             'Real Estate' => [
             'Land',
             'House',
             'Shutter',
             'Property',
             'Office Space',
             'Flat',
             'Apartment',
             'Hostel'
         ],
             'Automotive' => [
                'Cars',
                'Motorbikes',
                'Heavy Vehicles',
                'Van & Trucks',
                'Mechanics & Garages',
                'Parts & accessories For Car',
                'Parts & Accessories for Motorcycles'
            ],
            'Lifestyle' => [
                'Kid\'s Clothing',
                'Women\'s Clothing',
                'Women\'s Shoes',
                'Men\'s Clothing',
                'Men\'s Shoes',
                'Kid\'s Accessories',
                'Men\'s Accessories',
                'Women\'s Accessories',
                'Watches',
                'Jewellery',
                'Bags/Luggage',
                'Sunglasses',
                'Other Accessories',
                'Men Grooming Tools',
                'Women Grooming Tools',
            ],
            'Electronics' => [
                'Mobile Phone Sets',
                'Mobile Accessories',
                'Laptops & Computer',
                'Laptop Accessories',
                'Camera Lens & Accessories',
                'Drone & Quadcopter',
                'Projectors',
                'Storage & Optical Devices',
                'Networking Equipments',
                'Monitors',
                'Desktops & Accessories',
                'Tablets & Accessories',
                'TV & DVDs'
            ],
            'Home & Furnitures' => [

                    'Home Furnitures',
                    'Construction',
                    'Household & Tools',
                    'Lighting',
                    'Appliances',
                    'Garden Supplies',
                    'Home Decor and Interiors',
                    'Kitchenware & Utensils',
                    'Others'
],
            'Services' => [
                    'Software & Games',
                    'Web Development',
                    'Education & Classes',
                    'Cleaning Services',
                    'Photography',
                    'Technical Maintenance',
                    'Others',
                    'Hotel Management'
],
            'Musical Instrument & Services' => [

                    'Guitars',
                    'Drums',
                    'Keyboard / Piano',
                    'Dj and lightening',
                    'Amp and Speakers',
                    'Band Baja Services',
                    'Musical Instruments for Hire',
                    'Other Musical instrument'
],
            'Video Games & Toys' => [
                'Gaming Accessories',
                'Gaming Console',
                'Puzzle and Educational Toys',
                'General Toys',
                'Kid\'s Pretend Play',
                'Toys - Remote Control',
                'Gaming Disc and Cartiges',
                'Toys - Stuffed, Dolls & Figures',
                'Sports and Outdoor Play',
                'Blocks and Building Toys',
                'Others'
],
            'Sports,books & Hobbies' => [

                'Bicycles',
                'Fitness and Gyms Equipment',
                'Other Sports',
                'Bicycle Parts and Accessories',
                'Outdoor and Hiking',
                'Novels',
                'Course Books',
                'Other Books'
],
            'Travel & Tourism' => [
                'Air Ticket',
                'Bus Ticket',
                'Hotel',
                'Tour Package-Domestic',
                'Tour Package-International',
                'Treeking Package',
                'Other Tour Packages'
],
            'Pets & Animals' => [       
                'Dogs',
                'Cats',
                'Fish and Aquarium',
                'Rabbits',
                'Others',
                'Pet Services'
],

'Restaurant & Cafe' => [       
                'Cake Services',
],

'Celebrity' => [
                'Actors',
                'Actress',
                'Comedians',
                'Singers',
                'Anchor',
                'Artists',
                'Youtuber',
                'Director',
                'Musicians',
                'Story Tellers'
        ],

            ];
            foreach ($data as $category => $subCategories)
                        {
                 $id = Category::create([
                    'name' => $category,
                    ])->id;
                 foreach ($subCategories as $subCategory) 
                {
                Category::create([
                'parent_id' => $id,
                'name' => $subCategory,
                ]);
                }
            }

        $cat = Category::find(1);
        $cat->color = "#86267e";
        $cat->font = "zmdi zmdi-city-alt";
        $cat->featured = 1;
        $cat->save();

        $cat = Category::find(10);
        $cat->color = "#46b75d";
        $cat->font = "zmdi zmdi-car";
        $cat->featured = 1;
        $cat->save();

        $cat = Category::find(18);
        $cat->color = "#86267e";
        $cat->font = "zmdi zmdi-female";
        $cat->featured = 1;
        $cat->save();

        $cat = Category::find(34);
        $cat->color = "#39444c";
        $cat->font = "zmdi zmdi-radio";
        $cat->featured = 1;
        $cat->save();

        $cat = Category::find(48);
        $cat->color = "#c25762";
        $cat->font = "zmdi zmdi-seat";
        $cat->save();

        $cat = Category::find(58);
        $cat->color = "#44baff";
        $cat->font = "zmdi zmdi-wrench";
        $cat->save();

        $cat = Category::find(67);
        $cat->color = "#9bc2a4";
        $cat->font = "zmdi zmdi-audio";
        $cat->save();

        $cat = Category::find(76);
        $cat->color = "#86267e";
        $cat->font = "zmdi zmdi-play";
        $cat->save();

        $cat = Category::find(88);
        $cat->color = "#d8690c";
        $cat->font = "zmdi zmdi-book";
        $cat->save();

        $cat = Category::find(97);
        $cat->color = "#509fa6";
        $cat->font = "zmdi zmdi-flight-takeoff";
        $cat->save();

        $cat = Category::find(105);
        $cat->color = "#adb337";
        $cat->font = "zmdi zmdi-face";
        $cat->save();

        $cat = Category::find(112);
        $cat->color = "#46b75d";
        $cat->font = "zmdi zmdi-cutlery";
        $cat->featured = 1;
        $cat->save();

        $cat = Category::find(114);
        $cat->color = "#e76b77";
        $cat->font = "zmdi zmdi-male-female";
        $cat->featured = 1;
        $cat->save();

        $cat = JobCategory::find(1);
        $cat->color = "#86267e";
        $cat->font = "fas fa-paint-brush";
        $cat->save();

        $cat = JobCategory::find(2);
        $cat->color = "#46b75d";
        $cat->font = "fas fa-code";
        $cat->featured = 1;
        $cat->save();

        $cat = JobCategory::find(3);
        $cat->color = "#86267e";
        $cat->font = "fas fa-chart-bar";
        $cat->save();

        $cat = JobCategory::find(4);
        $cat->color = "#39444c";
        $cat->font = "fas fa-pencil-alt";
        $cat->save();

        $cat = JobCategory::find(5);
        $cat->color = "#c25762";
        $cat->font = "fas fa-bell";
        $cat->save();

        $cat = JobCategory::find(6);
        $cat->color = "#44baff";
        $cat->font = "fas fa-video";
        $cat->save();

        $cat = JobCategory::find(7);
        $cat->color = "#c29bc2";
        $cat->font = "fas fa-globe";
        $cat->featured = 1;
        $cat->save();

        $cat = JobCategory::find(8);
        $cat->color = "#86267e";
        $cat->font = "fas fa-cog";
        $cat->save();

        $cat = JobCategory::find(9);
        $cat->color = "#86267e";
        $cat->font = "fas fa-chart-pie";
        $cat->featured = 1;
        $cat->save();

        $cat = JobCategory::find(10);
        $cat->color = "#509fa6";
        $cat->font = "fas fa-star";
        $cat->save();

        $cat = JobCategory::find(11);
        $cat->color = "#adb337";
        $cat->font = "fas fa-certificate";
        $cat->save();

        $cat = JobCategory::find(12);
        $cat->color = "#86267e";
        $cat->font = "fas fa-plus";
        $cat->featured = 1;
        $cat->save();

        $cat = JobCategory::find(13);
        $cat->color = "#46b75d";
        $cat->font = "fas fa-bookmark";
        $cat->save();

        $cat = JobCategory::find(14);
        $cat->color = "#86267e";
        $cat->font = "fas fa-keyboard";
        $cat->save();

        $cat = JobCategory::find(15);
        $cat->color = "#39444c";
        $cat->font = "fas fa-users";
        $cat->featured = 1;
        $cat->save();

        $cat = JobCategory::find(16);
        $cat->color = "#c25762";
        $cat->font = "fas fa-tint";
        $cat->save();

        $cat = JobCategory::find(17);
        $cat->color = "#44baff";
        $cat->font = "fas fa-ambulance";
        $cat->save();

        $cat = JobCategory::find(18);
        $cat->color = "#86267e";
        $cat->font = "fas fa-cogs";
        $cat->save();

        $cat = JobCategory::find(19);
        $cat->color = "#86267e";
        $cat->font = "fas fa-edit";
        $cat->featured = 1;
        $cat->save();

        $cat = JobCategory::find(20);
        $cat->color = "#509fa6";
        $cat->font = "fas fa-chart-pie";
        $cat->save();

        $cat = JobCategory::find(21);
        $cat->color = "#adb337";
        $cat->font = "fas fa-certificate";
        $cat->save();


        $fcategories = [
             'Graphics & Design',
             'Programming & Tech',
             'Websites and apps',
             'Digital Marketing',
             'Writing & Content',
             'Data Entry & Admin',
             'Marketing & Customer Service',
             'Video & Animation',
             'Agriculture',
             'Business & Accounting',
             'Local Jobs & Services',
             'Protective & Security Services',
             'Teaching/Education',
             'Human Resource & ORG Development',
             'Commercial/Logistics & Supply Chain',
             'Research & Development',
             'Journalism & Media/Editor',
             'Product Sourcing & Manufacturing',
             'Others'
         ];
         foreach ($fcategories as $cat){
                         Fcategory::create([
                    'name' => $cat]);
                }


        $cat = Fcategory::find(1);
        $cat->color = "#86267e";
        $cat->font = "fas fa-paint-brush";
        $cat->featured = 1;
        $cat->save();

        $cat = Fcategory::find(2);
        $cat->color = "#469990";
        $cat->font = "fas fa-code";
        $cat->featured = 1;
        $cat->save();

        $cat = Fcategory::find(3);
        $cat->color = "#c25762";
        $cat->font = "fas fa-globe";
        $cat->save();

        $cat = Fcategory::find(4);
        $cat->color = "#86267e";
        $cat->font = "fas fa-chart-bar";
        $cat->featured = 1;
        $cat->save();

        $cat = Fcategory::find(5);
        $cat->color = "#46b75d";
        $cat->font = "fas fa-pencil-alt";
        $cat->save();

        $cat = Fcategory::find(6);
        $cat->color = "#39444c";
        $cat->font = "fas fa-keyboard";
        $cat->featured = 1;
        $cat->save();

        $cat = Fcategory::find(7);
        $cat->color = "#86267e";
        $cat->font = "fas fa-bullhorn";
        $cat->featured = 1;
        $cat->save();

        $cat = Fcategory::find(8);
        $cat->color = "#39444c";
        $cat->font = "fas fa-video";
        $cat->save();

        $cat = Fcategory::find(9);
        $cat->color = "#46b75d";
        $cat->font = "fas fa-leaf";
        $cat->save();

        $cat = Fcategory::find(10);
        $cat->color = "#964000";
        $cat->font = "fas fa-chart-bar";
        $cat->save();

        $cat = Fcategory::find(11);
        $cat->color = "#4B3A26";
        $cat->font = "fas fa-fire";
        $cat->featured = 1;
        $cat->save();

        $cat = Fcategory::find(12);
        $cat->color = "#0B6623";
        $cat->font = "fas fa-star";
        $cat->save();

        $cat = Fcategory::find(13);
        $cat->color = "#0E4C92";
        $cat->font = "fas fa-bookmark";
        $cat->save();

        $cat = Fcategory::find(14);
        $cat->color = "#784B84";
        $cat->font = "fas fa-users";
        $cat->save();

        $cat = Fcategory::find(15);
        $cat->color = "#DE3163";
        $cat->font = "fas fa-ambulance";
        $cat->save();

        $cat = Fcategory::find(16);
        $cat->color = "#dcbeff";
        $cat->font = "fas fa-cogs";
        $cat->save();

        $cat = Fcategory::find(17);
        $cat->color = "#964000";
        $cat->font = "fas fa-edit";
        $cat->save();

        $cat = Fcategory::find(18);
        $cat->color = "#F8E473";
        $cat->font = "fas fa-chart-pie";
        $cat->save();

        $cat = Fcategory::find(19);
        $cat->color = "#88807B";
        $cat->font = "fas fa-tint";
        $cat->save();


        $att = new Pricelabel();
        $att->name = "Per Month";
        $att->status = 1;
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $att = new Pricelabel();
        $att->name = "Onwards";
        $att->status = 1;
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $att = new Pricelabel();
        $att->name = "Per Sq. Feet";
        $att->status = 1;
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $att = new Pricelabel();
        $att->name = "Per Ropani";
        $att->status = 1;
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $att = new Pricelabel();
        $att->name = "Per Paisa";
        $att->status = 1;
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $att = new Pricelabel();
        $att->name = "Per Aana";
        $att->status = 1;
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $att = new Pricelabel();
        $att->name = "Per Dhur";
        $att->status = 1;
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $att = new Pricelabel();
        $att->name = "Per Kattha";
        $att->status = 1;
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $att = new Pricelabel();
        $att->name = "Per Bigha";
        $att->status = 1;
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $att = new Pricelabel();
        $att->name = "Per Daam";
        $att->status = 1;
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $att = new Pricelabel();
        $att->name = "Per Acres";
        $att->status = 1;
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $att = new Pricelabel();
        $att->name = "Per Haat";
        $att->status = 1;
        $att->save();
        $att->categories($att->id)->sync(array(1,2,3,4,5,6,7,8,9));

        $att = new Pricelabel();
        $att->name = "Per Pound";
        $att->status = 1;
        $att->save();
        $att->categories($att->id)->sync(array(113));
    }
}
