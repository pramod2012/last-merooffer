@extends('site.partials.main_index')
@section('title','Search Ads - Mero Offer')
@section('content')

<body class="archive category category-uncategorized category-1 logged-in">
    <div>
        @include('site.partials.navbar')
        <section id="classiera_map">
            <div id="log" style="display:none;"></div>
            <input id="latitude" type="hidden" value="">
            <input id="longitude" type="hidden" value="">



            <div id="classiera_main_map" style="width:100%; height:600px;">
                {{-- {!! $map['html'] !!}
            <script type='text/javascript'>
                var centreGot = false;
            </script>
            {!! $map['js'] !!} --}}
                <google-map/>
            </div>
        </section>
        
        @include('site.partials.footer')
    </div>
    @section('page-script')
    <script type='text/javascript' src="{{ asset('frontend/js/validator.min.js') }}"></script>
    @stop
    @section('extra-script')
    <script type='text/javascript'>
        //     jQuery(document).ready(function() {

    //     jQuery(document).on('click', '.pagination a', function(event) {
    //         event.preventDefault();
    //         jQuery('html, body').animate({
    //     scrollTop: jQuery("#scrollbar").offset().top
    // }, 200);
    //         var page = jQuery(this).attr('href').split('page=')[1];
    //         fetch_data(page);
    //         var l = window.location;
    //         window.history.pushState("", "", l.origin + l.pathname + "?page=" + page);
    //     });

    //     function fetch_data(page) {
    //         jQuery.ajax({
    //             url: "/welcome/pagination?page=" + page,
    //             success: function(products) {
    //                 jQuery('#table_data').html(products);
    //             }
    //         });
    //     }

    // });
    </script>
    @stop
    @endsection
