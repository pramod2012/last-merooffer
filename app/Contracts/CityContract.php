<?php

namespace App\Contracts;

/**
 * Interface CityContract
 * @package App\Contracts
 */
interface CityContract extends MyBaseContract
{
	public function findBySlug($slug);
}
