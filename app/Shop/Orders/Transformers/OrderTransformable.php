<?php

namespace App\Shop\Orders\Transformers;

use App\Address;
use App\Repo\AddressRepo;
use App\Courier;
use App\Repo\CourierRepo;
use App\Models\User;
use App\Repo\UserRepo;
use App\Models\Order;
use App\OrderStatus;
use App\Repo\OrderStatusRepo;

trait OrderTransformable
{
    /**
     * Transform the order
     *
     * @param Order $order
     * @return Order
     */
    protected function transformOrder(Order $order) : Order
    {
        $courierRepo = new CourierRepo(new Courier());
        $order->courier = $courierRepo->findById($order->courier_id);

        $customerRepo = new UserRepo(new User());
        $order->user = $customerRepo->findCustomerById($order->user_id);

        $addressRepo = new AddressRepo(new Address());
        $order->address = $addressRepo->findById($order->address_id);

        $orderStatusRepo = new OrderStatusRepo(new OrderStatus());
        $order->status = $orderStatusRepo->findById($order->order_status_id);

        return $order;
    }
}
