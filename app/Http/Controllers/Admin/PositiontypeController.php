<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\PositiontypeContract;
use App\Http\Controllers\BaseController;

class PositiontypeController extends BaseController
{
    private $page="admin.positiontype.";

    protected $positiontypeRepo;

    public function __construct(
        PositiontypeContract $positiontypeRepo
    )
    {
        $this->positiontypeRepo = $positiontypeRepo;
    }

    public function index()
    {
        $list = $this->positiontypeRepo->list('created_at', 'desc');
        $datas = $this->positiontypeRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('Positiontype', 'List of Positiontype');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('Positiontype', 'Create positiontype');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->positiontypeRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('positiontype.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->positiontypeRepo->findById($id);
        $this->setPageTitle('Positiontype', 'Edit positiontype');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->positiontypeRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('positiontype.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->positiontypeRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('positiontype.index', 'Record deleted successfully' ,'success',false, false);
    }
}
