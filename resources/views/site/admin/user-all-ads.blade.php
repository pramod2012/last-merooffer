@extends('site.partials.main_index')
@section('title','User All Ads')
@section('content')

<body data-rsssl=1
    class="page-template page-template-template-user-all-ads page-template-template-user-all-ads-php page page-id-38 logged-in">
    @include('site.partials.navbar')
    <!-- user pages -->
    <section class="user-pages section-gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    @include('site.partials.sidebar')
                </div>
                <!--col-lg-3-->
                <div class="col-lg-9 col-md-8 user-content-height">
                    <div class="user-detail-section section-bg-white">
                        <!-- my ads -->
                        <div class="user-ads user-my-ads">
                            @if(app('request')->has('flash_success'))
                            <div class="alert alert-success">
                                {{app('request')->get('flash_success')}}
                            </div>
                            @endif
                            <h4 class="user-detail-section-heading text-uppercase">
                                User Ads </h4>

                            @forelse($products as $value)
                            <div class="media border-bottom">
                                <div class="media-left">
                                    @if ($value->images->count() > 0)
                                    <img class="media-object"
                                        src="{{ asset('images_small/'.$value->images->first()->full) }}"
                                        alt="{{$value->name}}">
                                    @else
                                    <img class="media-object"
                                        src="{{ asset('storage/'.config('settings.error_image')) }}"
                                        alt="{{$value->name}}">
                                    @endif
                                </div>
                                <!--media-left-->
                                <div class="media-body">
                                    <h5 class="media-heading">
                                        <a href="{{ route('product_details',$value->slug) }}" target="_blank">
                                            {{$value->name}} </a>
                                    </h5>
                                    <p>
                                        <span class="published">
                                            <i class="{{$value->status == 1?" fa fa-check-circle":"fa
                                                fa-times-circle"}}"></i>
                                            Published</span>
                                        <span>
                                            <i class="far fa-eye"></i>
                                            {{$value->views}} </span>
                                        <span>
                                            <i class="far fa-clock"></i>
                                            {{$value->created_at->format('F j, Y')}}</span>
                                        <span>
                                            <i class="removeMargin fas fa-hashtag"></i>
                                            ID : {{$value->id}}</span>
                                    </p>
                                </div>
                                <!--media-body-->
                                <div class="classiera_posts_btns">
                                    <!--PayPerPostbtn-->
                                    <!--PayPerPostbtn-->
                                    <!--BumpAds-->
                                    <!--BumpAds-->
                                    <a href="{{route('submit-ad-edit',$value->id)}}"
                                        class="btn btn-primary sharp btn-style-one btn-sm"><i
                                            class="icon-left far fa-edit"></i>Edit</a>
                                    <form method="POST" action="{{route('product-delete', ['id' => $value->id])}}"
                                        onsubmit="return confirm('Are you sure?')">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button class="thickbox btn btn-primary sharp btn-style-one btn-sm"
                                            type="submit" value="submit"><i
                                                class="icon-left fas fa-trash-alt"></i>Delete</button>
                                    </form>
                                    <!--Mark As Sold-->
                                    <a class="thickbox btn btn-primary sharp btn-style-one btn-sm"
                                        id="{{'btn_sold'.$value->id}}" data-product_id="{{ $value->id }}"
                                        onclick="addToSold(this)" {{$value->sold==1?"disabled":""}}>
                                        <i class="icon-left far fa-check-square"></i>
                                        {{$value->sold==1?"Sold Out":"Mark as Sold"}}</a>

                                    <!--Mark As Sold-->
                                    <!--Restore Button for Expired Ads-->
                                    <!--Restore Button for Expired Ads-->
                                </div>
                                <!--classiera_posts_btns-->
                            </div>
                            <!--media border-bottom-->
                            @empty
                            <div class="media border-bottom">You haven't posted any ads! </div>
                            @endforelse
                            {{$products->links()}}
                        </div>
                        <!--user-ads user-my-ads-->
                        <!-- my ads -->
                    </div>
                    <!--user-detail-section-->
                </div>
                <!--col-lg-9-->
            </div>
            <!--row-->
        </div><!-- container-->
    </section>

    <script>
        function addToSold(el){
  var btn=jQuery(el);
  var product_id=btn.attr('data-product_id');
  jQuery.ajax({
    url:'{{route("sold_action")}}',
    method:"get",
    data:{'product_id':product_id},
    success:function(data){
      if(data.sold=="1"){
        console.log("added");
      }else{
        console.log("else");
        jQuery("#btn_sold"+product_id).attr("disabled",true).html("Sold Out");
      }
    },
    error:function(data){
      console.log("error");
    }
  });
}
    </script>
    @include('site.partials.footer')
    @endsection
