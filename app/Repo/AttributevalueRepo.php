<?php

namespace App\Repo;

use App\Models\Attributevalue;
use App\Contracts\AttributevalueContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

class AttributevalueRepo extends BaseRepo implements AttributevalueContract
{
    /**
     * AttributevalueRepository constructor.
     * @param Attributevalue $model
     */
    public function __construct(Attributevalue $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Category|mixed
     */
    public function store($request)
    {
        try {
            $params = $request->all();
            $collection = collect($params);

            $filterable = $collection->has('filterable') ? 1 : 0;
            $required = $collection->has('required') ? 1 : 0;

            $merge = $collection->merge(compact('filterable', 'required'));

            $data = new Attributevalue($merge->all());

            $data->save();

            return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
        $params = $request->all();

        $data = $this->findById($id);

        $collection = collect($params);

        $filterable = $collection->has('filterable') ? 1 : 0;
        $required = $collection->has('required') ? 1 : 0;

        $merge = $collection->merge(compact('filterable', 'required'));

        $data->update($merge->all());

        return $data;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);
        $data->delete();
        return $data;
    }
}
