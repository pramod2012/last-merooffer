@extends('site.partials.main_index')
@section('title','Cart Items')
@section('content')
<body class="page-template page-template-template-favorite page-template-template-favorite-php page page-id-22 logged-in">
@include('site.partials.navbar')
<section class="user-pages section-gray-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-4">
      @include('site.partials.sidebar')     
</div><!--col-lg-3-->
      <div class="col-lg-9 col-md-8 user-content-height">
        <div class="user-detail-section section-bg-white">
            @include('partials.alert')
          <!-- favorite ads -->
          <div class="user-ads favorite-ads">
            <h4 class="user-detail-section-heading text-uppercase">
            Cart Items {{Cart::count()}}            </h4>
                                                <!--singlepost-->
                @if(!$cartItems->isEmpty())
                   <div class="row">
                    <div class="col-md-12">
                        <!-- <div class="row header hidden-xs hidden-sm"> -->
                        <div class="row hidden-xs hidden-sm" style="height: 40px;">
                            
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><b>Cover</b></div>
                                </div>
                            </div>

                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3"><b>Name</b></div>
                                    <div class="col-lg-3 col-md-3"><b>Quantity</b></div>
                                    <div class="col-lg-1 col-md-1"><b>Remove</b></div>
                                    <div class="col-lg-2 col-md-2"><b>Price</b></div>
                                    <div class="col-lg-2 col-md-2"><b>Total</b></div>
                                </div>
                            </div>

              
              
                        </div>
                        @foreach($cartItems as $cartItem)
                                    <div class="row">
                                
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                            <a href="#" class="hover-border">
                                                @if(isset($cartItem->cover))
                                                    <img src="{{ asset('watermark/'.$cartItem->cover) }}" alt="{{ $cartItem->name }}" class="img-responsive img-thumbnail">
                                                @else
                                                    <img src="https://placehold.it/120x120" alt="" class="img-responsive img-thumbnail">
                                                @endif
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-9 col-xs-8">
                                    <div class="row">
                                        
                                        
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <h4 style="margin-bottom:5px;">{{$cartItem->name}}</h4>

                                            @if($cartItem->options->has('combination'))
                                                <div style="margin-bottom:5px;">
                                                @foreach($cartItem->options->combination as $option)
                                                    <small class="label label-primary">{{$option['value']}}</small>
                                                @endforeach
                                                </div>
                                            @endif
                                            <!-- </div> -->
                                        </div>
                                        
                                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-8">
                                            <form action="{{ route('cart.update', $cartItem->rowId) }}" class="form-inline" method="post">
                                                @csrf
                                                {{method_field('PATCH')}}
                                                <div class="input-group">
                                                <input type="text" name="quantity" value="{{ $cartItem->qty }}" class="form-control input-sm" />
                                                    <span class="input-group-btn"><button class="btn btn-primary btn-sm">Update</button></span>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-8 col-xs-4"> 
                                            <form action="{{ route('cart.destroy', $cartItem->rowId) }}" method="post">
                                                @csrf
                                                {{method_field('DELETE')}}
                                                <button onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                                            </form>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                            <span class="hidden-lg hidden-md"><small>Price: </span>
                                            Rs. {{ number_format($cartItem->price, 2) }}</small>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                            <span class="hidden-lg hidden-md"><small>Total: </span>
                                            Rs. {{ number_format(($cartItem->qty*$cartItem->price), 2) }}</small>
                                        </div>

                                    </div>
                                </div>                       
                                
                            </div>
                            <br>
                            @endforeach
                        

                    </div>
                </div>
               

                <div class="row">
                    <div class="col-md-12 content">
                        <table class="table table-striped">
                            <tfoot>
                                <tr>
                                    <td class="bg-warning">Subtotal</td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning"></td> 
                                    <td class="bg-warning">Rs. {{ number_format($subtotal, 2, '.', ',') }}</td>
                                </tr>
                                @if(isset($shippingFee) && $shippingFee != 0)
                                <tr>
                                    <td class="bg-warning">Shipping</td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning">Rs. {{ $shippingFee }}</td>
                                </tr>@endif
                                                                <tr>
                                    <td class="bg-warning">Tax</td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning">Rs. {{ $tax }}</td>
                                </tr>
                                <tr>
                                    <td class="bg-success">Total</td>
                                    <td class="bg-success"></td>
                                    <td class="bg-success"></td>
                                    <td class="bg-success"></td>
                                    <td class="bg-success">Rs. {{ number_format($total, 2, '.', ',') }}</td>
                                </tr>
                            </tfoot>
                        </table>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="btn-group pull-right">
                                    <a href="{{url('/checkout')}}" class="btn btn-primary btn-submit active">Go to checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>

            @else
                <div class="row">
                    <div class="col-md-12">
                        <p class="alert alert-warning">No items in cart yet. <a href="{{ route('index') }}">Back to Home!</a></p>
                    </div>
                </div>
            @endif

            <!--singlepost-->
                    
                      </div><!--user-ads-->
          
          <!-- favorite ads -->
        </div><!--user-detail-->
      </div><!--col-lg-9-->
    </div><!--row-->
  </div><!--container-->
</section><!--user-pages-->
<!-- Company Section Start-->
<!-- Company Section End--> 
@include('site.partials.footer')
@endsection