<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prd = new Product();
        $prd->featured = '1';
        $prd->views = 1;
        $prd->status = 1;
        $prd->bid = 1;
        $prd->quantity = 1;
        $prd->slug = 'pramod-bhandari';
        $prd->address = 'Kathmandu, Nepal';
        $prd->latitude = '27.7172453';
        $prd->longitude = '85.3239605';
        $prd->cell = '9849551992';
        $prd->name = 'Pramod Bhandari';
        $prd->price = '999999';
        $prd->desc = 'skdfjdsalsjdflsakjdfalksdf';
        $prd->expired_at = '2020-10-30 09:52:15';
        $prd->condition_id = 2;
        $prd->pricetype_id = 2;
        $prd->day_id = 1;
        $prd->adtype_id = 3;
        $prd->city_id = 2;
        $prd->user_id = 1;
        $prd->save();

        $prd = new Product();
        $prd->featured = '0';
        $prd->slug = 'disposable-products';
        $prd->views = 85;
        $prd->status = 1;
        $prd->bid = 1;
        $prd->quantity = 1;
        $prd->address = 'Kalika Nagar, Butwal, Nepal';
        $prd->latitude = '27.6809387';
        $prd->longitude = '83.4663557';
        $prd->cell = '9857044022';
        $prd->name = 'Disposable Products';
        $prd->price = '60';
        $prd->desc = 'Manufacturer and suppiers of
Paper Plates , Paper Cups, Facial Tissue, Napkins, Aluminium Conatainer, Aluminium Foil, Silver Packaging Bags, Disposal Glasses, Disposable Spoons and other disposable Items.
Paper plates price starts from Rs 8 to Rs 32 as per their size.
Paper cup
 130 ml = Rs 55 per roll
140 ml = Rs 59 per roll
150 ml = Rs 60 per roll
210 ml = Rs 95 per roll
250 ml = Rs 115 per roll';
        $prd->expired_at = '2021-01-07 15:12:47';
        $prd->condition_id = 2;
        $prd->pricetype_id = 1;
        $prd->day_id = 4;
        $prd->adtype_id = 1;
        $prd->city_id = 8;
        $prd->user_id = 9;
        $prd->save();
        $prd->categories($prd->id)->sync(array(57,48));

        $prd = new Product();
        $prd->featured = '0';
        $prd->slug = '35-life-t-shirt-special-tihar-offer';
        $prd->views = 32;
        $prd->status = 1;
        $prd->bid = 1;
        $prd->quantity = 1;
        $prd->address = 'Machhapokhari, Balaju, Kathmandu, Nepal';
        $prd->cell = '9849824952';
        $prd->name = '35%  Life t-shirt special tihar offer';
        $prd->price = '500';
        $prd->desc = 'Only 800 for 2 pieces of T-shirt 35%  Life t-shirt
What percentage of life I\'m done with.... it reminds us that time is valuable.
? These are 100% cotton t-shirt which is made of pre-shrunk 
cotton and is soft & smooth with a high thread-count and are comfortable and durable.
? T-shirt 35%  Life 
? t-shirt is available in - M, L, XL, and XXL perfect for both males and females.
If you are interested send me an inbox ?
or, please contact +977-9849824952 | 9880695377 | 9810043268
? mcare967@gmail.com
Home Delivery for Kathmandu Bhaktapur, and Lalitpur.';
        $prd->expired_at = '2021-01-07 16:27:01';
        $prd->condition_id = 1;
        $prd->pricetype_id = 2;
        $prd->day_id = 4;
        $prd->adtype_id = 1;
        $prd->city_id = 1;
        $prd->user_id = 8;
        $prd->save();
        $prd->categories($prd->id)->sync(array(65,58));

        $prd = new Product();
        $prd->featured = '0';
        $prd->slug = 'face-mask-and-print-custom-design';
        $prd->views = 72;
        $prd->status = 1;
        $prd->bid = 1;
        $prd->quantity = 10;
        $prd->address = 'Machhapokhari, Balaju, Kathmandu, Nepal';
        $prd->cell = '9849824952';
        $prd->name = 'Face mask and print custom design';
        $prd->price = '80';
        $prd->desc = 'Nobody said that wearing a protective face mask can’t be stylish. Introducing Custom Dye Sublimation Face Masks, offering a fully personalized solution for facemasks. Masks are designed with your logo and are suitable for everyday use';
        $prd->expired_at = '2021-01-07 16:39:21';
        $prd->condition_id = 1;
        $prd->pricetype_id = 2;
        $prd->day_id = 4;
        $prd->adtype_id = 8;
        $prd->city_id = 1;
        $prd->user_id = 8;
        $prd->save();
        $prd->categories($prd->id)->sync(array(65,58));
    
        $prd = new Product();
        $prd->featured = '0';
        $prd->slug = 'pyari-bahini-tihar-themed-printed-cushion-or-custom-design-cushion';
        $prd->views = 91;
        $prd->status = 1;
        $prd->bid = 1;
        $prd->quantity = 10;
        $prd->address = 'Machhapokhari, Balaju, Kathmandu, Nepal';
        $prd->cell = '9849824952';
        $prd->name = 'Pyari Bahini Tihar Themed Printed Cushion or custom design cushion';
        $prd->price = '650';
        $prd->desc = 'Pyari Bahini 100%satin cover
high quality korean fiber cushions
fully washable
Guarantee on prints
great to gift your sister
perfect gift for tihar ocassionThemed Printed Cushion';
        $prd->expired_at = '2021-01-07 16:39:21';
        $prd->condition_id = 1;
        $prd->pricetype_id = 2;
        $prd->day_id = 4;
        $prd->adtype_id = 8;
        $prd->city_id = 1;
        $prd->user_id = 8;
        $prd->save();
        $prd->categories($prd->id)->sync(array(65,58));

        $prd = new Product();
        $prd->featured = '0';
        $prd->slug = 'toshiba-tv-32-inch';
        $prd->views = 18;
        $prd->status = 1;
        $prd->bid = 1;
        $prd->quantity = 1;
        $prd->address = 'Seshmati Bridge, Nepaltar, Tarakeshwar, Nepal';
        $prd->latitude = '27.744073';
        $prd->longitude = '85.30015019999999';
        $prd->cell = '9849494516';
        $prd->name = 'toshiba tv 32 inch';
        $prd->price = '1600';
        $prd->desc = 'Toshiba tv for sale ramrai chalirako tv ho ekdam fine cha serious buyer please contact';
        $prd->expired_at = '2021-01-28 04:30:59';
        $prd->condition_id = 3;
        $prd->pricetype_id = 1;
        $prd->day_id = 4;
        $prd->adtype_id = 1;
        $prd->city_id = 1;
        $prd->user_id = 14;
        $prd->save();
        $prd->categories($prd->id)->sync(array(47,34));

        $prd = new Product();
        $prd->featured = '0';
        $prd->slug = 'tickets-available-for-metro-air-bus-namaste-jhapa';
        $prd->views = 18;
        $prd->status = 1;
        $prd->bid = 1;
        $prd->quantity = 1;
        $prd->address = 'Seshmati Bridge, Nepaltar, Tarakeshwar, Nepal';
        $prd->latitude = '27.7172453';
        $prd->longitude = '85.3239605';
        $prd->cell = '9801073555';
        $prd->name = 'Tickets available for metro air bus (namaste jhapa)';
        $prd->price = '500';
        $prd->desc = 'Available tickets for following district 

Kathmandu to kakarvitta 
Kathmandu to dharan 
Kathmandu to bhadrapur 
Kathmandu to ilam
Kathmandu to kakarvitt
Kathmandu to siliguri 

Mobile number: 9801073555';
        $prd->expired_at = '2021-02-05 10:31:28';
        $prd->condition_id = 3;
        $prd->pricetype_id = 3;
        $prd->day_id = 4;
        $prd->adtype_id = 8;
        $prd->city_id = 1;
        $prd->user_id = 21;
        $prd->save();
        $prd->categories($prd->id)->sync(array(99,97));

        $prd = new Product();
        $prd->featured = '0';
        $prd->slug = 'landyard-keyring';
        $prd->views = 26;
        $prd->status = 1;
        $prd->bid = 1;
        $prd->quantity = 1;
        $prd->address = 'Balaju, Kathmandu, Nepal';
        $prd->latitude = '27.7309123';
        $prd->longitude = '85.2955242';
        $prd->cell = '9849824952';
        $prd->name = 'Landyard keyring';
        $prd->price = '500';
        $prd->desc = 'Landyard printing
A very effective way to advertise your business.
Price rs 40 Per pcs for keyring
Price rs 70 per pcs for I\'d holder
Both side digital print?
Minimum order 20 pcs for Keyring
Minimum order 30 pcs for I\'d card dori
Deilivery all over Nepal❤️
call/viber: 9849824952
Mcare967@gmail.com';
        $prd->expired_at = '2021-02-05 10:31:28';
        $prd->condition_id = 3;
        $prd->pricetype_id = 2;
        $prd->day_id = 4;
        $prd->adtype_id = 8;
        $prd->city_id = 1;
        $prd->user_id = 11;
        $prd->save();
        $prd->categories($prd->id)->sync(array(65,58));
        
    }
}
