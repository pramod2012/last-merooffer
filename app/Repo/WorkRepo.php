<?php

namespace App\Repo;

use App\Work;
use App\Contracts\WorkContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

/**
 * Class WorkRepository
 *
 * @package \App\Repo
 */
class WorkRepo extends BaseRepo implements WorkContract
{
    /**
     * WorkRepository constructor.
     * @param Work $model
     */
    public function __construct(Work $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function store($request)
    {
        try {
            $params = $request->all();

            $collection = collect($params);

            $status = $collection->has('status') ? 1 : 0;

            $merge = $collection->merge(compact('status'));

            $data = new Work($merge->all());

            $data->save();

            return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
        try
        {
        $params = $request->all();

        $data = $this->findById($id);

        $collection = collect($params);

        $status = $collection->has('status') ? 1 : 0;

        $merge = $collection->merge(compact('status'));

        $data->update($merge->all());

        return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);
        $data->delete();
        return $data;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $data = Work::where('slug', $slug)->first();
        return $data;
    }
}