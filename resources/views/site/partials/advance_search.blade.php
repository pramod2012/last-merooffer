<!--SearchForm-->
<form method="get" action="{{route('search-product')}}">
    <div class="search-form border">
        <div class="search-form-main-heading">
            <a href="#innerSearch" role="button" data-toggle="collapse" aria-expanded="true"
                aria-controls="innerSearch">
                <i class="fas fa-sync-alt"></i>
                Category Search </a>
        </div>
        <!--search-form-main-heading-->
        <div id="innerSearch" class="collapse in classiera__inner">
            <!--Price Range-->
            <div class="inner-search-box">
                <h5 class="inner-search-heading"><i class="fas fa-list"></i>
                    Category </h5>
                <div class="inner-addon right-addon">
                    <i class="right-addon form-icon fas fa-sort"></i>
                    <select class="form-control form-control-sm" name="category_id">
                        <option value="" selected disabled>Select Category</option>
                        @foreach(\App\Models\Category::where('status',1)->orderBy('order','asc')->where('parent_id',null)->get()
                        as $value)
                        @if(@$category->id == $value->id || Request::get('category_id') == $value->id)
                        <option value="{{$value->id}}" selected>{{$value->name}}</option>
                        @else
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endif
                        @foreach($value->children as $subcat)
                        @if(@$category->id == $subcat->id || Request::get('category_id') == $subcat->id)
                        <option value="{{$subcat->id}}" selected>&nbsp;&nbsp;&nbsp;{{$subcat->name}}</option>
                        @else
                        <option value="{{$subcat->id}}">&nbsp;&nbsp;&nbsp;{{$subcat->name}}</option>
                        @endif
                        @endforeach
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="inner-search-box">
                <h5>
                    <span class="currency__symbol">
                        Rs.
                    </span>
                    Price Range </h5>

                <div class="classiera_price_slider">
                    <p>
                        <input data-cursign="Rs." type="text" id="amount" readonly
                            style="border:0; color:#f6931f; font-weight:bold;">
                    </p>
                    <div id="slider-range"></div>
                    <input type="hidden" id="classieraMaxPrice" value="100000">
                    <input type="hidden" id="range-first-val" name="min_price" value="{{Request::get('min_price')}}">
                    <input type="hidden" id="range-second-val" name="max_price" value="{{Request::get('max_price')}}">
                </div>
            </div>
            @if(Request::has('category_id'))
            @php
            $category = \App\Models\Category::findOrFail(Request::has('category_id') ? Request::get('category_id') :
            null);
            @endphp

            @foreach($category->attributes->where('filterable',1)->where('type','dropdown') as $attribute)
            <div class="inner-search-box">
                <h5 class="inner-search-heading">{{$attribute->name}}</h5>
                <div class="inner-addon right-addon">
                    <i class="right-addon form-icon fas fa-sort"></i>
                    <select class="form-control form-control-sm" name="{{$attribute->code}}">
                        <option value="">Select {{$attribute->name}}</option>
                        @foreach($attribute->attributevalues as $value)
                        @if(Request::get("$attribute->code") == $value->id)
                        <option value="{{$value->id}}" selected>{{$value->value}}</option>
                        @else
                        <option value="{{$value->id}}">{{$value->value}}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
            </div>
            @endforeach
            @foreach($category->attributes->where('filterable',1)->where('type','radio') as $attribute)
            <div class="inner-search-box-child">
                <p>{{$attribute->name}}</p>
                <div class="radio">
                    @foreach($attribute->attributevalues as $value)
                    @if(Request::get("$attribute->code") == $value->id)
                    <input value="{{$value->id}}" id="{{$value->value}}" type="radio" name="{{$attribute->code}}"
                        checked>
                    <label for="{{$value->value}}">{{$value->value}}</label>
                    @else
                    <input id="{{$value->value}}" value="{{$value->id}}" type="radio" name="{{$attribute->code}}">
                    <label for="{{$value->value}}">{{$value->value}}</label>
                    @endif
                    @endforeach
                </div>
            </div>
            <hr>
            @endforeach
            @foreach($category->attributes->where('filterable',1)->where('type','text')->where('interval',0) as
            $attribute)
            <h5 class="inner-search-heading">
                {{$attribute->name}}
            </h5>
            <div class="inner-addon right-addon">
                <input type="text" name='{{$attribute->code}}' value="{{Request::get("$attribute->code")}}"
                    placeholder="{{$attribute->name}}" class='form-control form-control-sm'>
            </div>
            <hr>
            @endforeach
            @foreach($category->attributes->where('filterable',1)->where('type','text')->where('interval',1) as
            $attribute)
            <h5 class="inner-search-heading">
                {{$attribute->name}}
            </h5>
            <div class="inner-addon right-addon">
                <input type="text" name="min_{{$attribute->code}}" value="{{Request::get("min_$attribute->code")}}"
                    placeholder="Min {{$attribute->name}}" class='form-control form-control-sm'>
                <input type="text" name="max_{{$attribute->code}}" value="{{Request::get("max_$attribute->code")}}"
                    placeholder="Max {{$attribute->name}}" class='form-control form-control-sm'>
            </div>
            <hr>
            @endforeach
            @endif
            <div class="inner-search-box">
                <h5 class="inner-search-heading"><i class="fas fa-map-marker"></i>
                    City </h5>
                <div class="inner-addon right-addon">
                    <i class="right-addon form-icon fas fa-sort"></i>
                    <select class="form-control form-control-sm" name="city_id">
                        <option value="" selected>Select City</option>
                        @foreach(\App\Models\City::all() as $value)
                        @if(Request::get('city_id') == $value->id)
                        <option value="{{$value->id}}" selected>{{$value->name}}</option>
                        @else
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <!--Price Range-->
            <!--Locations-->
            <!--Locations-->
            <!--Categories-->
            <div class="inner-search-box">
                <h5 class="inner-search-heading"><i class="far fa-folder-open"></i>
                    Address </h5>
                <!--SelectCategory-->
                <div class="inner-addon right-addon">
                    <input type="text" name='address' value="{{Request::get('address')}}" placeholder="address"
                        class='form-control form-control-sm'>
                </div>
                <!--Select Sub Category-->
                <!--Select Ads Type-->
                <div class="inner-search-box-child">
                    <p>Condition</p>
                    <div class="radio">
                        @foreach( \App\Models\Condition::all() as $value)
                        @if(Request::get('condition_id') == $value->id)
                        <input id="{{$value->name}}" type="radio" name="condition_id" value="{{$value->id}}" checked>
                        <label for="{{$value->name}}">{{$value->name}}</label>
                        @else
                        <input id="{{$value->name}}" type="radio" name="condition_id" value="{{$value->id}}">
                        <label for="{{$value->name}}">{{$value->name}}</label>
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="inner-search-box-child">
                    <p>Type of Ad</p>
                    <div class="inner-addon right-addon">
                        <i class="right-addon form-icon fas fa-sort"></i>
                        <select class="form-control form-control-sm" name="adtype_id">
                            <option value="" selected>Select Adtype</option>
                            @foreach(\App\Models\Adtype::all() as $value)
                            @if(Request::get('adtype_id') == $value->id)
                            <option value="{{$value->id}}" selected>{{$value->name}}</option>
                            @else
                            <option value="{{$value->id}}">{{$value->name}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="inner-search-box-child">
                    <p>Price Type</p>
                    <div class="inner-addon right-addon">
                        <i class="right-addon form-icon fas fa-sort"></i>
                        <select class="form-control form-control-sm" name="pricetype_id">
                            <option value="" selected>Select Pricetype</option>
                            @foreach(\App\Models\Pricetype::all() as $value)
                            @if(Request::get('adtype_id') == $value->id)
                            <option value="{{$value->id}}" selected>{{$value->name}}</option>
                            @else
                            <option value="{{$value->id}}">{{$value->name}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <!--Select Ads Type-->
            </div>
            <!--inner-search-box-->
            <button type="submit" class="btn btn-primary sharp btn-sm btn-style-one btn-block"
                value="Search">Search</button>
        </div>
        <!--innerSearch-->
    </div>
    <!--search-form-->
</form>
<!--SearchForm-->
