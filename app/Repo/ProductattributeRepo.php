<?php

namespace App\Repo;

use App\Models\Productattribute;
use App\Contracts\ProductattributeContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

/**
 * Class ProductattributeRepository
 *
 * @package \App\Repo
 */
class ProductattributeRepo extends BaseRepo implements ProductattributeContract
{
    /**
     * ProductattributeRepository constructor.
     * @param Productattribute $model
     */
    public function __construct(Productattribute $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function store($request)
    {
        try {

            $data = new Productattribute();
            $data->quantity = $request->productAttributeQuantity;
            $data->price = $request->productAttributePrice;
            $data->sale_price = $request->salePrice;
            $data->default = $request->default;
            $data->product_id = $request->product_id;
            $data->save();

            $code = $request->input("attributeValue");
                    $data->attributesValues()->sync($code);

            return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
        try
        {
        $params = $request->all();

        $data = $this->findById($id);

        $collection = collect($params);

        $status = $collection->has('status') ? 1 : 0;

        $merge = $collection->merge(compact('status'));

        $data->update($merge->all());

        return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);
        $data->delete();
        return $data;
    }
}
