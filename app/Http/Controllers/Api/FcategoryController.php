<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Contracts\FcategoryContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests as RulesOf;
use App\Utils as Utils;

class FcategoryController extends BaseController
{
    protected $fcategoryRepo;

    public function __construct(
        FcategoryContract $fcategoryRepo
    )
    {
        $this->fcategoryRepo = $fcategoryRepo;
    }

    public function index()
    {
        try{
            $list = $this->fcategoryRepo->list('order', 'asc');
            $data = new Utils\SuccessJsonResponse($list,"success");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function show($id)
    {
        try{
            $data1 = $this->fcategoryRepo->findById($id);
            $data = new Utils\SuccessJsonResponse($data1,"success");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function store(Request $request)
    {
        try{
            $data1 = $this->fcategoryRepo->store($request);
            $data = new Utils\SuccessJsonResponse($data1, "Record added Successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $data1 = $this->fcategoryRepo->edit($request, $id);
            $data = new Utils\SuccessJsonResponse($data1, "Record updated successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function destroy($id)
    {
        try{
            $data1 = $this->fcategoryRepo->destroy($id);
            $data = new Utils\SuccessJsonResponse($data1, "Record deleted Successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }
}
