<header>
    <!-- NavBar -->
    <section class="classiera-navbar classieraNavAffix classiera-navbar-v5 navbar-fixed-top">
        <div class="">
            <!-- mobile off canvas nav -->
            <nav id="myNavmenu"
                class="navmenu navmenu-default navmenu-fixed-left offcanvas offcanvas-light navmenu-fixed-left offcanvas-dark"
                role="navigation">
                <div class="navmenu-brand clearfix">
                    <a href="{{route('index')}}">
                        <img src="{{ asset('storage/'.config('settings.logo')) }}" alt="{{config('settings.logo')}}"
                            height="42" width="161">
                    </a>
                    <button type="button" class="offcanvas-button" data-toggle="offcanvas" data-target="#myNavmenu">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
                <!--navmenu-brand clearfix-->
                <div class="log-reg-btn text-center">
                    @if (!Auth::check())
                    <a href="{{route('login')}}" class="offcanvas-log-reg-btn">
                        Login </a>

                    @else
                    <a href="{{route('logout')}}" class="offcanvas-log-reg-btn">
                        Log out </a>
                    @endif
                </div>

                <div class="menu-footer-menu-container">
                    <ul id="menu-footer-menu" class="nav navmenu-nav">
                        <li id="menu-item-0"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-0">
                            <a title="contact-us" href="{{route('contact-us')}}">Contact Us</a>
                        </li>
                        @foreach(\App\Models\Post::where('is_blog',0)->where('feature',1)->where('status',1)->orderBy('order','asc')->orderBy('updated_at',
                        'desc')->limit(4)->get() as $value)
                        <li id="menu-item-{{$value->id}}"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{{$value->id}}">
                            <a title="{{$value->name}}" href="{{route('blog',$value->slug)}}">{{$value->name}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="submit-post">
                    <a href="{{ route('submit-ad')}}" class="btn btn-block btn-primary btn-md active">
                        Submit Ad </a>
                </div>
                <!--submit-post-->
                <div class="social-network">
                    <h5>Social network</h5>
                    <!--FacebookLink-->
                    <a href="//{{config('settings.facebook')}}" class="social-icon social-icon-sm offcanvas-social-icon"
                        target="_blank">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <!--twitter-->
                    <a href="//{{config('settings.twitter')}}" class="social-icon social-icon-sm offcanvas-social-icon"
                        target="_blank">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <!--Dribbble-->
                    <!--Flickr-->
                    <!--Github-->
                    <!--Pinterest-->
                    <!--YouTube-->
                    <!--Google-->
                    <!--Linkedin-->
                    <!--Instagram-->
                    <!--Vimeo-->
                    <!--VK-->
                    <!--OK-->
                </div>
            </nav>
            <!-- mobile off canvas nav -->
            <!--Primary Nav-->
            <nav class="navbar navbar-default ">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target="#myNavmenu"
                        data-canvas="body">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand-custom" href="{{route('index')}}">
                        <img src="{{ asset('storage/'.config('settings.logo')) }}" alt="{{config('settings.logo')}}"
                            height="42" width="161">
                    </a>
                </div>
                <!--navbar-header-->
                <div class="collapse navbar-collapse visible-lg" id="navbarCollapse">
                    <div class="custom-menu-v5">
                        <a href="#" class="pull-left flip menu-btn">
                            <i class="fas fa-bars"></i>
                        </a>
                        <ul id="menu-footer-menu-1" class="nav navbar-nav navbar-right nav-margin-top flip nav-ltr">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-0">
                                <a title="contact-us" href="{{route('contact-us')}}">Contact Us</a>
                            </li>
                            @foreach(\App\Models\Post::where('is_blog',0)->where('feature',1)->where('status',1)->orderBy('order','asc')->orderBy('updated_at',
                            'desc')->limit(4)->get() as $value)
                            <li
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{{$value->id}}">
                                <a title="{{$value->name}}" href="{{route('blog',$value->slug)}}">{{$value->name}}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="navbar-right login-reg flip">
                            @if (!Auth::check())

                            <a href="{{route('login')}}" class="lr-with-icon">
                                <i class="zmdi zmdi-lock"></i>Login </a>
                            <a href="{!! route('register') !!}" class="lr-with-icon">
                                <i class="zmdi zmdi-border-color"></i>Get Registered</a>

                            @else

                            <a href="{{route('profile')}}" class="lr-with-icon">
                                <i class="zmdi zmdi-lock"></i>My Account </a>
                            <a href="{{route('logout')}}" class="lr-with-icon">
                                <i class="zmdi zmdi-border-color"></i>Log out </a>

                            @endif
                            <a href="{{ route('submit-ad')}}" class="btn btn-primary btn-submit active">
                                Submit Ad </a>
                        </div>
                    </div>
                    <!--custom-menu-v5-->
                </div>
                <!--collapse navbar-collapse visible-lg-->
            </nav>
            <!--Primary Nav-->
        </div>
        <!--container-->
    </section>
    <!-- NavBar -->
    <!-- Mobile App button -->
    <div class="mobile-submit affix">
        <ul class="list-unstyled list-inline mobile-app-button">
            @if (!Auth::check())
            <li>
                <a href="{{route('login')}}">
                    <i class="fas fa-sign-in-alt"></i>
                    <span>Login</span>
                </a>
            </li>
            <li>
                <a href="{{ route('submit-ad')}}">
                    <i class="fas fa-edit"></i>
                    <span>Submit Ad</span>
                </a>
            </li>
            <li>
                <a href="{{route('register')}}">
                    <i class="fas fa-user"></i>
                    <span>Get Registered</span>
                </a>
            </li>
            @else

            <li>
                <a href="{{ route('submit-ad')}}">
                    <i class="fas fa-edit"></i>
                    <span>Submit Ad</span>
                </a>
            </li>
            <li>
                <a href="{{ route('job-user.create')}}">
                    <i class="fas fa-briefcase"></i>
                    <span>Submit Job</span>
                </a>
            </li>
            <li>
                <a href="{{route('profile')}}">
                    <i class="fas fa-user"></i>
                    <span>My Account</span>
                </a>
            </li>
            @endif
        </ul>
    </div>
    <!-- Mobile App button -->
</header>
