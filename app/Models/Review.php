<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Review extends Model
{
    protected $fillable = [
        'comment',
        'reply',
        'commentator_id',
        'seller_id'
    ];

    public function user(){
        return $this->belongsTo(User::class,'commentator_id');
    }
    public function seller(){
        return $this->belongsTo(User::class,'seller_id');
    }
}
