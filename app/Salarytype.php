<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salarytype extends Model
{
    protected $table = 'salarytypes';

    protected $fillable = [
        'name',
        'status'
    ];

    function jobs() {
    	return $this->hasMany('App\Job');
    }
}
