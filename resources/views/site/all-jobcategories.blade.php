@extends('site.partials.main_index')
@section('title', 'List of all Job Categories')
@section('content')
<body class="archive category category-uncategorized category-1 logged-in">
    @include('site.partials.navbarjob')
    @include('site.partials.searchjob')
    <!--search-section--><!-- page content -->
                <!-- advertisement -->
<section class="classiera-premium-ads-v6 border-bottom section-pad">
        <div class="section-heading-v6">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 center-block">
                    <h3 class="text-capitalize">PREMIUM JOBS</h3>
                                        <p>List of all the top premium jobs of Nepal</p>
                                    </div>
            </div>
        </div>
    </div>
        <div style="overflow: hidden;">
        <div style="margin-bottom: 40px;">          
            <div id="owl-demo" class="owl-carousel premium-carousel-v6" data-car-length="4" data-items="4" data-loop="true" data-nav="false" data-autoplay="true" data-autoplay-timeout="3000" data-dots="false" data-auto-width="false" data-auto-height="true" data-right="false" data-responsive-small="1" data-autoplay-hover="true" data-responsive-medium="2" data-responsive-large="4" data-responsive-xlarge="6" data-margin="15">
@foreach(\App\Job::where('feature',1)->limit(8)->get() as $job)
            <div class="classiera-box-div-v6 item match-height">
                    <figure>
                        <div class="premium-img">@if($job->jobtype->id !== 1)
                            <div class="featured">
                                <p>{{$job->jobtype->name}}</p>
                            </div>@endif<!--featured-tag-->
                    @if (!empty($job->user->firmuser->logo))
                    <img class="media-object" src="{{ asset('storage/'.$job->user->firmuser->logo) }}" alt="{{$job->name}}">
                    @else
                    <img class="media-object" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$job->name}}">
                    @endif
                            <span class="price btn btn-primary round btn-style-six active">
                                Salary :@if($job->salary==!null) NRs. {{ number_format($job->salary) }}@endif @if($job->salary_to == !null) - {{number_format($job->salary_to)}} @endif {{$job->salarytype->name}}                        </span>
                                <span class="classiera-buy-sel">{{ ucwords($job->positiontype->name) }}</span>
                                                    </div><!--premium-img-->
                        <figcaption>
                            <div class="content">
                                <h5><a href="{{ route('job_post',$job->slug) }}">{{$job->name}}</a></h5>
                                <div class="category">
                                    <span>Category : 
                                        <a href="{{ route('jobcat', $job->category->slug) }}">{{ ucwords($job->category->name) }}</a>
                                    </span>
                                </div>
                                <div class="description">
                                    <p>{{ substr($job->body,0,245) }}</p>
                                </div>
                                <a href="{{ route('job_post', $job->slug) }}">view job <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                @endforeach
        </div>
        </div>
        <div class="navText">
            <a class="prev btn btn-primary round outline btn-style-six">
                <i class="icon-left fas fa-long-arrow-alt-left"></i>
                Previous            </a>
            <a class="next btn btn-primary round outline btn-style-six">
                Next                <i class="icon-right fas fa-long-arrow-alt-right"></i>
            </a>
        </div>
    </div>
</section>
            <section class="section-pad-80 category-v5">
        <div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-9 center-block">
                    <h1 class="text-capitalize">Ads categories</h1>
                                        <p>List of all Job Categories </p>
                                    </div><!--col-lg-7-->
            </div><!--row-->
        </div><!--container-->
    </div><!--section-heading-v1-->
        <div class="container">
        <ul class="list-inline list-unstyled categories">
        @foreach(\App\JobCategory::orderBy('order','asc')->get() as $value)
            <li class="match-height">
                <div class="category-content">                  
                        <i style="color:{{$value->color}};" class="{{$value->font}}"></i>
                                            <h4>
                        <a href="{{ url('search-job?category_id='.$value->id) }}">
                            {{$value->name}}                        </a>
                    </h4>
                </div><!--category-box-->
            </li><!--col-lg-4-->
            @endforeach
                        
        </ul>
    </div>
</section>
<!-- page content -->

@include('site.partials.footer')
@endsection