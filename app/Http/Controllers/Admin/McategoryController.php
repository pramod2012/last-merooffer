<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\McategoryContract;
use App\Http\Controllers\BaseController;

class McategoryController extends BaseController
{
    private $page="admin.mcategory.";

    protected $mcategoryRepo;

    public function __construct(
        McategoryContract $mcategoryRepo
    )
    {
        $this->mcategoryRepo = $mcategoryRepo;
    }

    public function index()
    {
        $list = $this->mcategoryRepo->list('created_at', 'desc');
        $mcategories = $this->mcategoryRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('Blog Categories', 'List of Blog Categories');
        return view($this->page.'index',compact('mcategories'));
    }

    public function create()
    {
        $categories = $this->mcategoryRepo->list();
        $this->setPageTitle('Blog Categories', 'Create Blog Category');
        return view($this->page.'create',compact('categories'));
    }

    public function store(Request $request)
    {
        $data = $this->mcategoryRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('mcategory.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $mcategory = $this->mcategoryRepo->findById($id);
        $this->setPageTitle('Blog Categories', 'Edit Blog Category');
        return view($this->page.'edit', compact('mcategory'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->mcategoryRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('mcategory.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->mcategoryRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('mcategory.index', 'Record deleted successfully' ,'success',false, false);
    }
}
