<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\LevelContract;
use App\Http\Controllers\BaseController;

class LevelController extends BaseController
{
    private $page="admin.level.";

    protected $levelRepo;

    public function __construct(
        levelContract $levelRepo
    )
    {
        $this->levelRepo = $levelRepo;
    }

    public function index()
    {
        $list = $this->levelRepo->list('created_at', 'desc');
        $datas = $this->levelRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('Level', 'List of Level');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('Level', 'Create level');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->levelRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('level.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->levelRepo->findById($id);
        $this->setPageTitle('Level', 'Edit level');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->levelRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('level.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->levelRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('level.index', 'Record deleted successfully' ,'success',false, false);
    }
}
