@extends('site.partials.main_index')
@section('title','All Products')
@section('content')
<body class="archive category category-uncategorized category-1 logged-in">
    @include('site.partials.navbar')
    @include('site.partials.search')
    <!--search-section--><!-- page content -->
    <section class="inner-page-content border-bottom top-pad-50">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <!--category description section-->
                    <!--category description section-->
                    <!--Google Section-->
                    <!--Google Section-->
                    <!-- advertisement -->
                    <section class="classiera-advertisement advertisement-v5 section-pad-80 border-bottom">
                        <div class="tab-divs">
                            <div class="view-head">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-8 col-sm-9 col-xs-10">
                                            <div class="tab-button">
                                                <ul class="nav nav-tabs" role="tablist">                                
                                                    <li role="presentation"  <?php if(Request::get('price') != 'asc' && Request::get('price') != 'desc' && Request::get('view') != 'desc' ){ 
                                                        echo 'class="active" ';
                                                    } ?>>
                                                    <a href="{{ route('shop-products') }}" aria-controls="all">
                                                        Recent Ads
                                                        <span class="arrow-down"></span>
                                                    </a>
                                                </li>
                                                <li role="presentation" <?php if(Request::get('price') == 'asc'){ 
                                                    echo 'class="active" ';
                                                } ?>>                                    
                                                <a href="{{ route('shop-products', ['price' =>'asc']) }}" aria-controls="random">
                                                    Price ASC                                      <span class="arrow-down"></span>
                                                </a>
                                            </li>
                                            <li role="presentation"<?php if(Request::get('price') == 'desc'){ 
                                                echo 'class="active" ';
                                            } ?>>                                    
                                            <a href="{{ route('shop-products', ['price' =>'desc']) }}" aria-controls="random">
                                                Price DESC                                      <span class="arrow-down"></span>
                                            </a>
                                        </li>
                                        <li role="presentation" <?php if(Request::get('view')){ 
                                            echo 'class="active" ';
                                        } ?>>                                   
                                        <a href="{{ route('shop-products', ['view' =>'desc']) }}" aria-controls="popular">
                                         Popular Ads                                    <span class="arrow-down"></span>
                                     </a>
                                 </li>
                             </ul><!--nav nav-tabs-->
                         </div><!--tab-button-->
                     </div><!--col-lg-6 col-sm-8-->
                     <div class="col-lg-4 col-sm-3 col-xs-2">
                        <div class="view-as text-right flip">
                            <a id="grid" class="grid active" href="#"><i class="fas fa-th"></i></a>
                            <a id="list" class="list " href="#"><i class="fas fa-th-list"></i></a>                          
                        </div><!--view-as tab-button-->
                    </div><!--col-lg-6 col-sm-4 col-xs-12-->
                </div><!--row-->
            </div><!--container-->
        </div><!--view-head-->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="all">
                <div class="container products">
                    <div id="load" class="row">
                     <!--FeaturedPosts-->@foreach($products as $key => $value)
                     <div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
                        <div class="classiera-box-div classiera-box-div-v5">
                            <figure class="clearfix">
                                <div class="premium-img">@if($value->featured == 1)
                                    <div class="featured-tag">
                                        <span class="left-corner"></span>
                                        <span class="right-corner"></span>
                                        <div class="featured">
                                            <p>Featured</p>
                                        </div>
                                    </div>@endif
                                    @if ($value->images->count() > 0)
                    <img class="img-responsive" src="{{ asset('images_small/'.$value->images->first()->full) }}" alt="{{$value->name}}" style="max-height: 100%">
                    @else
                    <img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
                    @endif
                                    <span class="hover-posts">                                          
                                        <a href="{{ route('product_details',$value->slug) }}">View Ad</a>
                                    </span>
                                    <span class="price">
                                        PRICE: Rs.{{ number_format($value->price) }}</span>
                                        <span class="classiera-buy-sel">
                                            {{@$value->adtype->name}}              </span>
                                        </div><!--premium-img-->
                                        <div class="detail text-center">
                                            <span class="price">
                                                Rs. {{ number_format($value->price) }}             </span>
                                                <div class="box-icon">
                                                    @if($value->user->show_email)
                                                    <a href="mailto:{{ $value->user->email }}?subject">
                                                        <i class="fas fa-envelope"></i>
                                                    </a>@endif
                                                    @if($value->user->show_mobile)
                                                    <a href="tel:{{ $value->user->mobile}}"><i class="fas fa-phone"></i></a>@endif
                                                </div>
                                                <a href="{{ route('product_details',$value->slug) }}" class="btn btn-primary outline btn-style-five">View Ad</a>
                                            </div><!--detail text-center-->
                                            <figcaption>
                                                <span class="price visible-xs">Rs.{{ number_format($value->price) }}
                                                </span>
                                                <h5><a href="{{ route('product_details',$value->slug) }}">{{$value->name}}</a></h5>
                                                <div class="category">
                                                    <span>
                                                        Category :
                                @foreach($value->categories->where('parent_id',!null) as $cat)
                    <a href="{{route('menu', $cat->category->slug)}}" title="View all posts in {{$cat->category->name}}">{{$cat->category->name}}</a> /
                    <a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a>
                @endforeach </span>
                                                        <span>Location : 
                                                            <a href="{{route('findByCity',$value->city->slug)}}" title="View all posts in {{$value->city->name}}">{{$value->city->name}}</a>
                                                        </span>
                                                    </div>
                                                    <p class="description">{{substr($value->desc, 0, 340)}}</p>
                                                </figcaption>
                                            </figure>
                                        </div><!--row-->
                                    </div><!--col-lg-4-->
                                    @endforeach
                                    <br>
                                </div><!--row-->
                                <div class="classiera-pagination">
                                    <nav aria-label="Page navigation">
                                        <div id="pagination">
                                         {!!$products->links()!!}
                                     </div>
                                 </nav>
                             </div><!--tab-divs-->
                         </section><!-- advertisement -->
                     </div><!--col-md-8-->
                     <div class="col-md-4 col-lg-3">
                        <aside class="sidebar">
                            <div class="row">
                                <!--cit-->
                                <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                                    <div class="widget-box">
                                        <div class="widget-title">
                                            <h4>
                                                <i class="zmdi zmdi-menu" style="color:#c29bc2;"></i>
                                                All Ads                             </h4>
                                            </div>
                                            <div class="widget-content">
                                                <ul class="category">
                                                    
                                                    <li>
                                                        <a href="{{route('index')}}">
                                                            <i class="fas fa-angle-right"></i>
                                                            Home
                                                        </a>
                                                    </li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!--cit-->
                                    <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                                        <div class="widget-box">
                                            <div class="widget-title">
                                                <h4>Premium Products</h4>
                                            </div>
                                            <div class="widget-content">
                                                @foreach($featured as $key => $value)
                                                <div class="media footer-pr-widget-v1">
                                                    <div class="media-left">
                                                        <a class="media-img" href="{{ route('product_details',$value->slug) }}">
                                                            @if ($value->images->count() > 0)
                    <img class="media-object" src="{{ asset('images_small/'.$value->images->first()->full) }}" alt="{{$value->name}}" style="max-height: 100%">
                    @else
                    <img class="media-object" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
                    @endif
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <span class="price">Rs. {{$value->price}}</span>
                                                        <h4 class="media-heading">
                                                            <a href="{{ route('product_details',$value->slug) }}">{{$value->name}}</a>
                                                        </h4>
                                                        <span class="category">
                                                            @foreach($value->categories as $val)
                                                            <a href="{{route('menu',$val->slug)}}">{{$val->name}},</a>
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                                        <div class="widget-box">
                                            <div class="widget-title">
                                                <h4>Popular Products</h4>
                                            </div>
                                            <div class="widget-content">
                                                @foreach($popular as $key => $value)
                                                <div class="media footer-pr-widget-v1">
                                                    <div class="media-left">
                                                        <a class="media-img" href="{{ route('product_details',$value->slug) }}">
                                                            @if ($value->images->count() > 0)
                    <img class="media-object" src="{{ asset('images_small/'.$value->images->first()->full) }}" alt="{{$value->name}}" style="max-height: 100%">
                    @else
                    <img class="media-object" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
                    @endif
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <span class="price">Rs. {{$value->price}}</span>
                                                        <h4 class="media-heading">
                                                            <a href="{{ route('product_details', $value->slug) }}">{{$value->name}}</a>
                                                        </h4>
                                                        
                                                        <span class="category"> 
                                                            @foreach($value->categories as $val)
                                                            <a href="{{route('menu',$val->slug)}}">{{$val->name}},</a>
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div><!--row-->
                            </aside>
                        </div><!--row-->
                    </div><!--row-->
                </div><!--container-->
            </section>  
            <!-- page content -->

            @include('site.partials.footer')
            @endsection