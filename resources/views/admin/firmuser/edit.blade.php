@extends('admin.firmuser.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form method="POST" action="{{ route('firmusers.update',$data->id) }}" aria-label="category" enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{method_field('PATCH')}}
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name',$data->name) }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="logo" class="col-md-4 col-form-label text-md-right">Logo</label>

                            <div class="col-md-6">
                                <input type="file" class="form-control" name="logo"/>
                                <img src="{{@asset('storage/'.$data->logo)}}" height="70">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="img1" class="col-md-4 col-form-label text-md-right">Image</label>

                            <div class="col-md-6">
                                <input type="file" class="form-control" name="img1"/>
                                <img src="{{@asset('storage/'.$data->img1)}}" height="70">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="img2" class="col-md-4 col-form-label text-md-right">Image 2</label>

                            <div class="col-md-6">
                                <input type="file" class="form-control" name="img2"/>
                                <img src="{{@asset('storage/'.$data->img2)}}" height="70">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="url" class="col-md-4 col-form-label text-md-right">URL</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="url" value="{{old('url',$data->url)}}" />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="img1" class="col-md-4 col-form-label text-md-right">Phone</label>

                            <div class="col-md-6">
                                <input value="{{old('phone',$data->phone)}}" type="text" class="form-control" name="phone"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="img1" class="col-md-4 col-form-label text-md-right">EMail</label>

                            <div class="col-md-6">
                                <input value="{{old('email',$data->email)}}" type="text" class="form-control" name="email"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Description</label>

                            <div class="col-md-6">
                                <textarea rows="5" type="text" class="form-control" name="desc" value="{{ old('desc') }}">{{ old('desc',$data->desc) }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fb" class="col-md-4 col-form-label text-md-right">Facebook</label>

                            <div class="col-md-6">
                                <input value="{{old('fb',$data->fb)}}" type="text" class="form-control" name="fb"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="insta" class="col-md-4 col-form-label text-md-right">insta</label>

                            <div class="col-md-6">
                                <input value="{{old('insta',$data->insta)}}" type="text" class="form-control" name="insta"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="twitter" class="col-md-4 col-form-label text-md-right">twitter</label>

                            <div class="col-md-6">
                                <input value="{{old('twitter',$data->twitter)}}" type="text" class="form-control" name="twitter"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="youtube" class="col-md-4 col-form-label text-md-right">youtube</label>

                            <div class="col-md-6">
                                <input value="{{old('youtube',$data->youtube)}}" type="text" class="form-control" name="youtube"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="linkedin" class="col-md-4 col-form-label text-md-right">linkedin</label>

                            <div class="col-md-6">
                                <input value="{{old('linkedin',$data->linkedin)}}" type="text" class="form-control" name="linkedin"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">User</label>
                            <div class="col-md-6">
                                <select name="user_id" class="form-control">
                                    <option value="{{$data->user_id}}">{{$data->user->name}}</option>
                                </select>
                            </div>
                        </div>



    <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                        <input type="hidden" name="show_phone" value="0">
                  <input name="show_phone" type="checkbox" value="1" {{$data->show_phone?"checked":""}}>
                      Show Phone
                    </label>
                  </div>
              </div>
          </div>

          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                <input type="hidden" name="show_email" value="0">
                  <input name="show_email" type="checkbox" value="1" {{$data->show_email?"checked":""}}>
                      Show Email
                    </label>
                  </div>
              </div>
          </div>

          
                    
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
