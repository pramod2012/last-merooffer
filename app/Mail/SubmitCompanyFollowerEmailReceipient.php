<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Follower;

class SubmitCompanyFollowerEmailReceipient extends Mailable
{
    use Queueable, SerializesModels;
    protected $follower;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Follower $follower)
    {
        $this->follower = $follower; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@merooffer.com')
                    ->subject('Submit Company Follower Email Receipient')
                    ->view('emails.submitCompanyFollowerEmailReceipient')
                    ->with([
                        'followed_by' => $this->follower->follower->name,
                        'followed_to' => $this->follower->following->name,
                        'company_name' => $this->follower->following->firmuser->name,
                        'followed_by_email' => $this->follower->follower->email,
                        'followed_to_email' => $this->follower->following->email,
                    ]);
    }
}
