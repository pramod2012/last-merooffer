<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFapplicantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'application_letter' => 'required',
            'day' => 'required',
            'attachment' => 'nullable|mimes:doc,pdf,docx',
            'bid' => 'integer',
        ];
    }

    public function attributes(){
        return [
        'application_letter' => 'Cover Letter',
        'bid' => 'offer budget',
        ];
    }
}
