@extends('site.partials.main_index')
@section('title','List of Emails')
@section('content')
<body class="page-template page-template-template-profile page-template-template-profile-php page page-id-32 logged-in">
@include('site.partials.navbar') 
    <!-- user pages -->
<section class="user-pages section-gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4">
    @include('site.partials.sidebar')
         </div><!--col-lg-3-->
            <div class="col-lg-9 col-md-8 user-content-height">
                <div class="user-detail-section section-bg-white">
                    <!-- about me -->
                    <div class="about-me">
                        <h4 class="user-detail-section-heading text-uppercase">My Emails</h4>
                        <ul class="list-unstyled">
                            @foreach($contacts as $value)
                                <a>
                                    {{$value->name}}({{$value->email}}) says '{{$value->subject}}' - {{$value->desc}}
                                </a><br>
                                @endforeach
                            </li>
                        </ul>
                    </div>
                    <!-- about me -->
                    <!-- contact details -->
                   
                    <!-- contact details -->
                    <!-- social profile -->
                    
                    
                </div>
            </div>
        </div><!--row-->
    </div><!--container-->
</section>
<!-- user pages -->
@include('site.partials.footer')
@endsection