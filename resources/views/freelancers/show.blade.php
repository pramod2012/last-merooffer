@extends('site.partials.main_index')
@section('title',"Mero Offer - $work->name")
@section('content') 
<body class="post-template-default single single-post postid-445 single-format-standard logged-in">
    @include('freelancers.partials.navbarwork')
@include('freelancers.partials.searchwork')
<section class="inner-page-content single-post-page">
    <div class="container">
        <!-- breadcrumb -->
        <div class="row"><div class="col-lg-12"><ul class="breadcrumb"><li><a rel="v:url" property="v:title" href="{{route('index')}}"><i class="fas fa-home"></i></a></li>&nbsp;<li class="cAt"><a rel="v:url" href="{{url('freelancers')}}">Freelancers</a></li>&nbsp;<li class="cAt"><a rel="v:url" href="{{route('freelancers.category',$work->fcategory->slug)}}">{{$work->fcategory->name}}</a></li>&nbsp;<li class="active">{{$work->name}}</li></ul></div></div>     <!-- breadcrumb -->
        <!--Google Section-->
                        <!--Google Section-->
                <div class="row">
            <div class="col-md-8">
                <!-- single post -->
                <div class="single-post">
                    <!-- post title-->
<div class="single-post-title">
            <div class="post-price visible-xs visible-sm">      
        <h4>
              @if($work->budget==!null) Rs. {{ number_format($work->budget) }}@endif @if($work->budget_to == !null) - {{number_format($work->budget_to)}} @endif
        </h4>
    </div>
        <h4 class="text-uppercase">
        <a href="#">{{$work->name}}</a>
        <!--Edit Ads Button-->
    </h4>
    <p class="post-category">
            <i class="far fa-folder-open"></i>:
        <span>
        <a href="{{route('freelancers.category',$work->fcategory->slug)}}" title="View all posts in {{$work->fcategory->name}}" >{{$work->fcategory->name}}</a>    </span>
                <i class="fas fa-map-marker-alt"></i>:<span><a href="#">{{$work->address}}</a></span>
    </p>
</div><br>
<!-- post title-->
                                       <!-- ad deails -->
                    <div class="border-section border details">
                        <h4 class="border-section-heading text-uppercase"><i class="far fa-file-alt"></i>General Details</h4>
                        <div class="post-details">
                            <ul class="list-unstyled clearfix">
                                <li>
                                    <p>work ID: 
                                    <span class="pull-right flip">
                                        <i class="fas fa-hashtag IDIcon"></i>
                                        {{$work->id}}</span>
                                    </p>
                                </li><!--PostDate-->
                                <li>
                                    <p>Added: 
                                    <span class="pull-right flip">5 days ago
                                    </span>
                                    </p>
                                </li><!--PostDate-->                                
                                <!--Price Section -->
                                <li>
                                    <p>Views: 
                                    <span class="pull-right flip">
                                        {{$work->views}}
                                    </span>
                                    </p>
                                </li><!--Views-->
                                <li>
                                    <p>Last Date to Apply: 
                                    <span class="pull-right flip">
                                        {{$work->endate}}</span>
                                    </p>
                                </li><!--Sale Price-->
                                
                                <li>
                                    <p>
                                        Budget <span class="pull-right flip">
                                              @if($work->budget==!null) Rs. {{ number_format($work->budget) }}@endif @if($work->budget_to == !null) - {{number_format($work->budget_to)}} @endif 
                                        </span>
                                    </p>
                                </li>
                                <li>
                                    <p>Average Budget: 
                                    <span class="pull-right flip">
                                        Rs. {{$work->budget_avg}}</span>
                                    </p>
                                </li><!--Sale Price-->
                                <li>
                                    <p>Duration: 
                                    <span class="pull-right flip">
                                       {{$work->duration->name}}</span>
                                    </p>
                                </li><!--Sale Price-->
                                
                                
                                                            </ul>
                        </div><!--post-details-->
                    </div>
                    <!-- ad details -->                 
                    <!--PostVideo-->
                                        <!--PostVideo-->
                    <!-- post description -->
                    <div class="border-section border description">
                        <h4 class="border-section-heading text-uppercase">
                        Description
                    </h4>
                        <p><?php
$str = $work->body;
echo nl2br($str);
?></p></p>
@if($work->skillss->count() > 0)
<div class="tags">
                            <span>
                                <i class="fas fa-tags"></i>
                                Skills :
                            </span>
                            @foreach($work->skillss as $skill)
                            <a href="{{route('freelancers.skill',$skill->slug)}}" rel="tag">{{$skill->skill}}</a>
                            @endforeach

                           </div>
@endif
            
                    </div>
                    <!-- post description -->






                    <div class="border-section border bids">
                        <h4 class="border-section-heading text-uppercase">Offers</h4>
                        @if(Session::has('bid'))
                            <div class="alert alert-success">
                                {{Session::get('bid')}}
                            </div>
                        @endif
                        
                        <div class="classiera_bid_stats">
                            <p class="classiera_bid_stats_text"> 
                                <strong>OFFER Stats :</strong> 
                                {{$work->fapplicants->count()}}&nbsp;
                                Offers posted on this ad   
                                                   </p>
                            @if($work->fapplicants->count() > 0)
                            <div class="classiera_bid_stats_prices">
                                
                                <p class="classiera_bid_price_btn high_price"> 
                                    <span>Highest Offer:</span>
                                    Rs.{{$work->fapplicants->max('bid')}}
                                </p>
                                <p class="classiera_bid_price_btn"> 
                                    <span>Lowest Offer:</span> 
                                    Rs.{{$work->fapplicants->min('bid')}}
                                </p>
                               
                            </div><!--classiera_bid_stats_prices-->
                            @endif
                        </div><!--classiera_bid_stats-->
                        <div class="classiera_bid_comment_section">
                            @foreach($work->fapplicants as $value)
                            <div class="classiera_bid_media">
                                <img class="classiera_bid_media_img img-thumbnail" src="{{asset('frontend/images/user.png')}}" alt="bepsa">
                                <div class="classiera_bid_media_body">
                                    <h6 class="classiera_bid_media_body_heading">
                                        {{$value->user->name}}                                        <span>Offering</span>
                                    </h6>
                                    <p class="classiera_bid_media_body_time">
                                        <i class="far fa-clock"></i>{{$value->created_at}}</p>
                                    <p class="classiera_bid_media_body_decription">
                                        {{$value->application_letter}}
                                    </p>
                                </div>
                                <div class="classiera_bid_media_price">
                                    <p class="classiera_bid_price_btn">
                                        <span>Offer:</span> 
                                        Rs.{{$value->bid}} in {{$value->day}} days
                                    </p>
                                </div>
                            </div>
                            @endforeach
                            
                        </div><!--classiera_bid_comment_section-->
                        <div class="comment-form classiera_bid_comment_form">
                            <div class="comment-form-heading">
                                <h4 class="text-uppercase">
                                    Let&rsquo;s Create Your Offer
                                </h4>
                                <p>Only registered user can post offer
                                    <span class="text-danger">*</span>
                                </p>
                            </div><!--comment-form-heading-->
                            <div class="classiera_ad_price_comment">
                                <p> Budget Range</p>
                                <h3>@if($work->budget==!null) Rs. {{ number_format($work->budget) }}@endif @if($work->budget_to == !null) - {{number_format($work->budget_to)}} @endif 
                                                               </h3>
                            </div><!--classiera_ad_price_comment-->
                            <form data-toggle="validator" action="{{route('freelancers.applicant.store')}}" method="post"  enctype="multipart/form-data">
                                {{ csrf_field()}}
                                <div class="form-group">
                                    <div class="form-inline row">
                                        <div class="form-group col-sm-12">
                                            <label class="text-capitalize">
                                                Enter your offering price :
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="inner-addon left-addon">
                                                <input type="number" value="{{old('bid')}}" class="form-control form-control-sm offer_price" name="bid" placeholder="Enter offering price" data-error="Only integer" required>
                                                <strong>{{ $errors->first('bid') }}</strong>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div><!--form-group col-sm-12-->                                    
                                        <div class="form-group col-sm-12">
                                            <label class="text-capitalize">
                                                Cover Letter :
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="inner-addon">
                                                <textarea value="{{old('application_letter')}}" id="breakText" data-error="Say something about offer" name="application_letter" placeholder="Say something" required></textarea>
                                                <strong>{{ $errors->first('application_letter') }}</strong>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div><!--form-group col-sm-12-->

                                        <script type="text/javascript">
  //Some text string that has line breaks in it that need to be removed
  var someText = "{{old('application_letter')}}";
  //This javascript code replaces all 3 types of line breaks with a single space
  someText = someText.replace(/(\r\n|\n|\r)/gm," ");
  //Replace all double white spaces with single spaces
  someText = someText.replace(/\s+/g," ");
  
  //Put the newly treated text string into the textarea to show the result
  document.getElementById("breakText").value = someText;
</script>
                                        <div class="form-group col-sm-12">
                                            <label class="text-capitalize">
                                                I can deliver within (day/s):
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="inner-addon left-addon">
                                                <input type="number" value="{{old('day')}}" class="form-control form-control-sm offer_price" name="day" placeholder="Enter offering days" data-error="Only integer" required>
                                                <strong>{{ $errors->first('day') }}</strong>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div><!--form-group col-sm-12-->
                                        <div class="form-group col-sm-12">
                                            <label class="text-capitalize">
                                               Attachment:
                                            </label>
                                            <div class="inner-addon left-addon">
                                                <input type="file" value="{{old('attachment')}}" class="form-control form-control-sm offer_price" name="attachment" placeholder="Upload attachment" data-error="Upload attachment">
                                                <strong>{{ $errors->first('attachment') }}</strong>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div><!--form-group col-sm-12-->
                                    </div><!--form-inline row-->
                                </div><!--form-group-->
                                
                                <input type="hidden" name="fjob_id" value="{{$work->id}}">
                                <div class="form-group">
                                    <button type="submit" name="submit" class="btn btn-primary sharp btn-md btn-style-one" value="send">
                                        Send offer</button>
                                </div>
                            </form>
                        </div><!--comment-form classiera_bid_comment_form-->
                    </div>
<!-- classiera bid system -->







                </div>
                <!-- single post -->
            </div><!--col-md-8-->
            <div class="col-md-4">
                <aside class="sidebar">
                    <div class="row">
                                                <!--Widget for style 1-->
                        <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                            <div class="widget-box ">
                                                                <div class="widget-title price">
                                    <h3 class="post-price">                                     
                                     @if($work->budget==!null) Rs. {{ number_format($work->budget) }}@endif @if($work->budget_to == !null) - {{number_format($work->budget_to)}} @endif 
                                        <span class="ad_type_display">
                                              {{$work->jobtype->name}} Work                                         </span>
                                          </h3>
                                    
                                </div><!--price-->
                                    
                                <div class="widget-content widget-content-post">
                                    <div class="author-info border-bottom widget-content-post-area">
                                        <div class="media">
                                            <div class="media-left">
                                                <img class="media-object" src="{{ asset('frontend/images/user.png') }}" alt="{{$work->user->name}}">
                                            </div><!--media-left-->
                                            <div class="media-body">
                                                <h5 class="media-heading text-uppercase">
                                                    {{$work->user->name}}
                                                </h5>
                                                <p>Member Since: &nbsp;{{$work->user->created_at}}</p>
                                                <a href="{{route('freelancers.user',$work->user->slug)}}">See all works</a>


                                        <div class="user-make-offer-message widget-content-post-area">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="btnWatch"> 
                                                <form method="get" class="fav-form clearfix">
                                     <?php if(Auth::check()){    
                                                   $auth_id = Auth::user()->id;
                                                   } ?>
                      @if(!Auth::check())<a href="{{url('login')}}">@endif          
            <button class="watch-later text-uppercase" id="{{'btn____'.$work->user_id}}" data-auth_id="@if(Auth::check()) {{ Auth::user()->id }} @endif" data-user_id="{{ $work->user_id }}" type="button" onclick="addToFollow(this)" 
                                    {{$work->user->isAddedToFollowedList($work->user_id)?"disabled":""}}>
                                    
                {{$work->user->isAddedToFollowedList($work->user_id)?"Followed":"Follow"}} </button>
            @if(!Auth::check())</a>@endif
            </form>
        </li>
    </ul>
</div>

 <script type="text/javascript">
function addToFollow(el){
  var btn=jQuery(el);
  var auth_id=btn.attr('data-auth_id');
  var user_id=btn.attr('data-user_id');
  jQuery.ajax({
    url:'{{route("addSeller")}}',
    method:"get",
    data:{'user_id':user_id,'auth_id':auth_id,'_token':'{{csrf_token()}}'},
    success:function(data){
      if(data.status=="1"){
        console.log("added");
          jQuery("#btn____"+user_id).attr("disabled",true).html("Added");
      }else{
        console.log("else");
      }
    },
    error:function(data){
      console.log("error");
    }
  });
  // alert("Hi");
}
</script>
        <div class="clearfix"></div>
                                                        
                                            </div><!--media-body-->
                                        </div><!--media-->
                                    </div><!--author-info-->
                                </div><!--widget-content-->
                                <div class="widget-content widget-content-post">
                                    <div class="contact-details widget-content-post-area">
            @if($work->user->show_email ==1 || $work->user->show_mobile == 1 || $work->user->show_phone == 1)           
                                        <h5 class="text-uppercase">Contact Details :</h5>
                                        @endif
                                        <ul class="list-unstyled fa-ul c-detail">
                                            @if($work->user->show_email ==1)
                                            <li><i class="fas fa-li fa-envelope"></i> 
                                                <a href="mailto:{{$work->user->email}}">
                                                    {{$work->user->email}}
                                                </a>
                                            </li>
                                            @endif
                                            @if($work->user->show_mobile == 1)
                                            <li><i class="fas fa-li fa-mobile"></i>&nbsp;
                                                <span class="phNum" data-replace="{{$work->user->mobile}}">{{$work->user->mobile}}</span>
                                                <button type="button" id="showNum">Reveal</button>
                                            </li>@endif
                                            @if($work->user->show_phone == 1)
                                            <li><i class="fas fa-li fa-phone"></i>&nbsp;
                                                <span data-replace="{{$work->user->phone}}">{{$work->user->phone}}</span>
                                            </li>@endif
                                        </ul>
                                    </div><!--contact-details-->
                                </div><!--widget-content-->
                                                            </div><!--widget-box-->
                        </div><!--col-lg-12 col-md-12 col-sm-6 match-height-->
                                                <!--Widget for style 1-->
                        <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                            <div class="widget-box">
                                <div class="widget-title">
                                    <h4>Related Works</h4>
                                </div>
                                @foreach($related as $value)
                                
                                <div class="widget-content">
                                    <div class="media footer-pr-widget-v1">
                                        
                                        <div class="media-body">
                                            <span class="price"> Budget: @if($value->budget==!null) Rs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif</span>
                                            <h4 class="media-heading">
                                                <a href="{{route('freelancers.show',$value->slug)}}">{{$value->name}}</a>
                                            </h4>
                                            <span class="category">
                                                Category :
                                                <a href="{{route('freelancers.category',$value->fcategory->slug)}}">{{$value->fcategory->name}}</a>
                                                
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                           
                        </div><!--col-lg-12 col-md-12 col-sm-6 match-height-->
                                                <!--Social Widget-->

                    </div><!--row-->
                </aside><!--sidebar-->
            </div><!--col-md-4-->
        </div><!--row-->
    </div><!--container-->
</section>
<!-- related post section -->

@include('site.partials.footer')
    @endsection