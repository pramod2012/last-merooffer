<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Attribute;

class ExtendedUpdateProductRequest extends UpdateProductRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                'category_id' => 'required',
                'sub_category_id' => 'required'
            ]
        );
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'category_id' => 'Category',
            'sub_category_id' => 'Sub Category'
        ]);
    }
}
