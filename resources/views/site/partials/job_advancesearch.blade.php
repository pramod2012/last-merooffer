<!--SearchForm-->
<form method="get" action="{{route('search-job')}}">
    <div class="search-form border">
        <div class="search-form-main-heading">
            <a href="#innerSearch" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="innerSearch">
                <i class="fas fa-sync-alt"></i>
                Advance Job Search         </a>
        </div><!--search-form-main-heading-->
        <div id="innerSearch" class="collapse in classiera__inner">
            <!--Price Range-->
                        <div class="inner-search-box">
                <h5>
                    <span class="currency__symbol">
                    Rs. 
                    </span>
                Salary Range             </h5>
                    
                    <div class="classiera_price_slider">
                        <p>
                          <input data-cursign="Rs." type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
                        </p>                     
                        <div id="slider-range"></div>
                        <input type="hidden" id="classieraMaxPrice" value="100000">
                        <input type="hidden" id="range-first-val" name="min_salary" value="{{Request::get('min_salary')}}">
                        <input type="hidden" id="range-second-val" name="max_salary" value="{{Request::get('min_salary')}}">
                    </div>                  
                </div>
            <div class="inner-search-box">
                <h5 class="inner-search-heading"><i class="fas fa-list"></i>
                Job Category               </h5>
                <div class="inner-addon right-addon">
                    <i class="right-addon form-icon fas fa-sort"></i>
                    <select class="form-control form-control-sm" name="category_id">
                        <option value="" selected>Select Job Category</option>
                        @foreach(\App\JobCategory::get() as $value)
                        @if(@$job_category->id == $value->id || Request::get('category_id') == $value->id)
                        <option value="{{$value->id}}" selected>{{$value->name}}</option>
                        @else
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="inner-search-box">
                <h5 class="inner-search-heading"><i class="fas fa-list"></i>
                Industry               </h5>
                <div class="inner-addon right-addon">
                    <i class="right-addon form-icon fas fa-sort"></i>
                    <select class="form-control form-control-sm" name="industry_id">
                        <option value="" selected>Select Industry</option>
                        @foreach(\App\Industry::get() as $value)
                        @if(Request::get('industry_id') == $value->id)
                        <option value="{{$value->id}}" selected>{{$value->name}}</option>
                        @else
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
            </div>
                <div class="inner-search-box">
                <h5 class="inner-search-heading"><i class="fas fa-tag"></i>
                Position                </h5>
                <div class="inner-addon right-addon">
                    <i class="right-addon form-icon fas fa-sort"></i>
                    <select class="form-control form-control-sm" name="positiontype_id">
                        <i class="right-addon form-icon fas fa-sort"></i>
                        <option value="" selected>Select Position</option>
                        @foreach(\App\Positiontype::all() as $value)
                        @if(Request::get('positiontype_id') == $value->id)
                        <option value="{{$value->id}}" selected>{{$value->name}}</option>
                        @else
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endif
                        @endforeach
                    </select>
                    
                </div>
            </div>

            <div class="inner-search-box">
                <h5 class="inner-search-heading"><i class="fas fa-tag"></i>
                Experience                </h5>
                <div class="inner-addon right-addon">
                    <i class="right-addon form-icon fas fa-sort"></i>
                    <select class="form-control form-control-sm" name="experience_id">
                        <i class="right-addon form-icon fas fa-sort"></i>
                        <option value="" selected>Select Experience</option>
                        @foreach(\App\Experience::all() as $value)
                        @if(Request::get('experience_id') == $value->id)
                        <option value="{{$value->id}}" selected>{{$value->name}}</option>
                        @else
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endif
                        @endforeach
                    </select>
                    
                </div>
            </div>
            <!--Price Range-->
            <!--Locations-->
                        <!--Locations-->
            <!--Categories-->
            
                
                <!--SelectCategory-->
                <div class="inner-search-box">
                <h5 class="inner-search-heading"><i class="fas fa-map-marker-alt"></i>
                Location </h5>
                <div class="inner-addon right-addon">
                    <i class="right-addon form-icon fas fa-sort"></i>
                    <select class="form-control form-control-sm" name="city_id">
                        <option value="" selected>Select City</option>
                        @foreach(\App\Models\City::get() as $value)
                        @if(Request::get('city_id') == $value->id)
                        <option value="{{$value->id}}" selected>{{$value->name}}</option>
                        @else
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
            
                <!--Select Sub Category-->
                <!--Item Condition-->
                <div class="inner-search-box-child">
                    <p>Select Level</p>
                    <div class="radio">
                        
                        @foreach( \App\Level::all() as $value)
                        @if(Request::get('level_id') == $value->id)
                            <input id="{{$value->name}}" type="radio" name="level_id" value="{{$value->id}}" checked>
                            <label for="{{$value->name}}">{{$value->name}}</label>
                            @else
                            <input id="{{$value->name}}" type="radio" name="level_id" value="{{$value->id}}">
                            <label for="{{$value->name}}">{{$value->name}}</label>
                            @endif
                        @endforeach
                    </div>
                </div>
                                <!--Item Condition-->
                <!--Select Ads Type-->
                                <div class="inner-search-box-child">
                    <p>Job Type</p>
                    <div class="radio">
                        
                        @foreach( \App\Jobtype::all() as $value)
                            @if(Request::get('jobtype_id') == $value->id)
                            <input id="{{$value->name}}" type="radio" name="jobtype_id" value="{{$value->id}}" checked>
                            <label for="{{$value->name}}">{{$value->name}}</label>
                            @else
                            <input id="{{$value->name}}" type="radio" name="jobtype_id" value="{{$value->id}}">
                            <label for="{{$value->name}}">{{$value->name}}</label>
                            @endif
                        @endforeach
                    </div>
                </div>
                                <!--Select Ads Type-->
            </div><!--inner-search-box-->
            <button type="submit" class="btn btn-primary sharp btn-sm btn-style-one btn-block" value="Search">Search</button>
        </div><!--innerSearch-->
    </div><!--search-form-->
</form>
<!--SearchForm-->
