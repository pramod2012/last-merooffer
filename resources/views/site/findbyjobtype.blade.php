@extends('site.partials.main_index')
@section('title')
Jobs from {{$jobtype->name}}
@endsection
@section('content')
<body class="archive category category-uncategorized category-1 logged-in">
    @include('site.partials.navbarjob')
    @include('site.partials.searchjob')
    <!--search-section--><!-- page content -->
<section class="inner-page-content border-bottom top-pad-50">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-9">
                <!-- advertisement -->
<section class="classiera-advertisement advertisement-v5 section-pad-80 border-bottom">
    <div class="tab-divs">
        <div class="view-head">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-sm-9 col-xs-10">
                        <div class="tab-button">
                            <ul class="nav nav-tabs" role="tablist">                                
                                <li role="presentation" @if(Request::get('salary_avg') != 'asc' && Request::get('salary_avg') != 'desc' && Request::get('views') != 'desc' ) class="active" @endif>
                                    <a href="{{route('jobtype',$jobtype->slug)}}" aria-controls="all">
                                        Recent Jobs
                                        <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation" @if(Request::get('salary_avg') == 'asc') 
                                    class="active" @endif>                                    
                                    <a href="{{ route('jobtype',['slug' => $jobtype->slug, 'salary_avg' =>'asc']) }}" aria-controls="random">
                                        Salary ASC                                      <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation" @if(Request::get('salary_avg') == 'desc') 
                                    class="active" @endif>                                    
                                    <a href="{{ route('jobtype',['slug' => $jobtype->slug, 'salary_avg' =>'desc']) }}" aria-controls="random">
                                        Salary DESC                                      <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation" @if(Request::get('views') == 'desc') 
                                    class="active" @endif>                                   
                                    <a href="{{ route('jobtype',['slug' => $jobtype->slug, 'views' =>'desc']) }}" aria-controls="popular">
                                       Popular Jobs                                    <span class="arrow-down"></span>
                                    </a>
                                </li>
                            </ul><!--nav nav-tabs-->
                        </div><!--tab-button-->
                    </div><!--col-lg-6 col-sm-8-->
                    <div class="col-lg-4 col-sm-3 col-xs-2">
                        <div class="view-as text-right flip">
                            <a id="grid" class="grid" href="#"><i class="fas fa-th"></i></a>
                            <a id="list" class="list active" href="#"><i class="fas fa-th-list"></i></a>                          
                        </div><!--view-as tab-button-->
                    </div><!--col-lg-6 col-sm-4 col-xs-12-->
                </div><!--row-->
            </div><!--container-->
        </div><!--view-head-->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="all">
                <div class="container products">
                    <div id="load" class="row">
                   <!--FeaturedPosts-->@forelse ($jobs as $job)
                        <div class="col-lg-4 col-md-4 col-sm-6 match-height item item-list">
    <div class="classiera-box-div classiera-box-div-v5">
        <figure class="clearfix">
            <div class="premium-img">@if($job->jobtype->id !== 1)
                                <div class="featured-tag">
                        <span class="left-corner"></span>
                        <span class="right-corner"></span>
                        <div class="featured">
                            <p>{{$job->jobtype->name}}</p>
                        </div>
                    </div>@endif
                    @if (!empty($job->user->firmuser->logo))
                    <img class="media-object" src="{{ asset('storage/'.$job->user->firmuser->logo) }}" alt="{{$job->name}}">
                    @else
                    <img class="media-object" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$job->name}}">
                    @endif
                                    <span class="hover-posts">                                          
                    <a href="{{ route('job_post', $job->slug) }}">View Job</a>
                </span>
                                    <span class="price">
                                        @if($job->salary==!null) NRs. {{ number_format($job->salary) }}@endif @if($job->salary_to == !null) - {{number_format($job->salary_to)}} @endif {{$job->salarytype->name}}
                                    </span>
                                                <span class="classiera-buy-sel">
                {{ ucwords($job->positiontype->name) }}              </span>
                            </div><!--premium-img-->
            <div class="detail text-center">
                                <span class="price">
                                    @if($job->salary==!null) NRs. {{ number_format($job->salary) }}@endif @if($job->salary_to == !null) - {{number_format($job->salary_to)}} @endif {{$job->salarytype->name}}
                                </span>
                <div class="box-icon">
                    @if(@$job->user->firmuser->show_email == 1)
                    <a href="mailto:{{$job->user->firmuser->email}}?subject">
                        <i class="fas fa-envelope"></i>
                    </a>@endif
                    @if(@$job->user->firmuser->show_phone == 1)
                    <a href="tel:{{$job->user->firmuser->phone}}"><i class="fas fa-phone"></i></a>@endif
                </div>
                <a href="{{ route('job_post',$job->slug) }}" class="btn btn-primary outline btn-style-five">View Job</a>
            </div><!--detail text-center-->
            <figcaption>
                                <span class="price visible-xs">
                                    @if($job->salary==!null) NRs. {{ number_format($job->salary) }}@endif @if($job->salary_to == !null) - {{number_format($job->salary_to)}} @endif {{$job->salarytype->name}}
                                </span>
                                <h5><a href="{{ route('job_post',$job->slug) }}">{{$job->name}}</a></h5>
                <div class="category">
                    <span>
                    Category : 
                    <a href="{{route('jobcat',$job->category->slug)}}" title="View all posts in Services">{{ ucwords($job->category->name) }}</a>                 </span>
                                        <span>Location : 
                        <a href="{{route('jobcity',$job->city->slug)}}">{{$job->city->name}}</a>
                    </span>
                </div>
                <p class="description">{{ substr($job->body,0,245) }}</p>
            </figcaption>
        </figure>
    </div><!--row-->
</div><!--col-lg-4-->
@empty
<div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
    <div class="classiera-box-div classiera-box-div-v5">
        <figure class="clearfix">
            <div class="premium-img">
                    <img class="img-responsive" src="{{ asset('frontend/images/empty.png') }}" alt="Submit Ads">
                                    <span class="hover-posts">                                          
                    <a href="{{route('job-user.create')}}">Submit Job</a>
                </span>
                                    <span class="price">
                        Go On, It's Easy</span>
                                                <span class="classiera-buy-sel">
                Want to see your job post here??</span>
                            </div><!--premium-img-->
            <div class="detail text-center">
                                <span class="price">Go On, It's Easy</span>
                                <div class="box-icon">
                                    </div>
                <a href="{{route('job-user.create')}}" class="btn btn-primary outline btn-style-five">Post Job</a>
            </div><!--detail text-center-->
            <figcaption>
                <span class="price visible-xs">Go On, It's Easy
            </span>
            <h5><a href="{{route('job-user.create')}}">Start Hiring</a></h5>
                <div class="category">
                </div>
                <p class="description">Post job and find your employee you're looking for in and around your local community. Go on, It's Quick and Easy .</p>
            </figcaption>
        </figure>
    </div><!--row-->
</div><!--col-lg-4-->
@endforelse
<br>
</div><!--row-->
<div class="classiera-pagination">
    <nav aria-label="Page navigation">
        <div id="pagination">
       {!!$jobs->links()!!}
        </div>
    </nav>
</div><!--tab-divs-->
</section><!-- advertisement -->
            </div><!--col-md-8-->
            <div class="col-md-4 col-lg-3">
                <aside class="sidebar">
                    <div class="row">
                        <!--subcategory-->
                        <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                            <div class="widget-box">
                                <div class="widget-title">
                                    <h4>
                                        <i class="fas fa-bars" style="color:#c29bc2;"></i>
                                        Categories                                 </h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="category">
                                        @foreach(\App\JobCategory::all() as $value)
                                        <li>
                                            <a href="{{ route('jobcat',$value->slug) }}">
                                                <i class="fas fa-angle-right"></i>
                                               {{$value->name}}                                               <span class="pull-right flip">
                                                    {{$value->jobs->where('status',1)->count()}}
                                                    </span>
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                                                <!--subcategory-->
                                                    <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                                <div class="widget-box">
                                <!--SearchForm-->
@include('site.partials.job_advancesearch')
<!--SearchForm-->
</div>
                            </div>        
                            </div><!--row-->
                </aside>
            </div><!--row-->
        </div><!--row-->
    </div><!--container-->
</section>  
<!-- page content -->

@include('site.partials.footer')
@section('page-script')
    <script type='text/javascript' src="{{ asset('frontend/js/validator.min.js') }}"></script>
@stop
@endsection