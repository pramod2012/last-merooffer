<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Firmuser extends Model
{
    protected $table = 'firmusers';

    function user() {
    	return $this->belongsTo('App\Models\User');
   }
}
