<?php

namespace App\Models\Freelancers;

use Illuminate\Database\Eloquent\Model;

class fjob_skill extends Model
{
    protected $fillable = [
    	'skill_id',
    	'fjob_id'
    ];
}
