<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Contracts\AddressContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests as RulesOf;
use App\Utils as Utils;

class AddressController extends BaseController
{
    protected $addressRepo;

    public function __construct(
        AddressContract $addressRepo
    )
    {
        $this->addressRepo = $addressRepo;
    }

    public function index()
    {
        try{
            $list = $this->addressRepo->list('created_at', 'desc');
            $datas = $this->addressRepo->paginateArrayResults($list->all(), 5);
            $data = new Utils\SuccessJsonResponse($datas,"success");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function show($id)
    {
        try{
            $data1 = $this->addressRepo->findById($id);
            $data = new Utils\SuccessJsonResponse($data1,"success");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function store(Request $request)
    {
        try{
            app()->make(RulesOf\StoreAddressRequest::class);
            $data1 = $this->addressRepo->store($request);
            $data = new Utils\SuccessJsonResponse($data1, "Record added Successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function update(Request $request, $id)
    {
        try{
            app()->make(RulesOf\UpdateAddressRequest::class);
            $data1 = $this->addressRepo->edit($request, $id);
            $data = new Utils\SuccessJsonResponse($data1, "Record updated successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
        
    }

    public function destroy(Request $request, $id)
    {
        try{
            $data1 = $this->addressRepo->destroy($id);
            $data = new Utils\SuccessJsonResponse($data1, "Record deleted Successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }
}
