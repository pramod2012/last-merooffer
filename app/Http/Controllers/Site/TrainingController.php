<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Training;

class TrainingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function storeTraining(Request $request) {
    	$data = new Training();
    	$data->name = $request->name;
    	$data->org_name = $request->org_name;
    	$data->duration = $request->duration;    	
    	$data->completion_year = $request->completion_year;
    	$data->user_id = auth()->user()->id; 
    	$data->save();
    }

    public function updateTraining(Request $request) {    	
     	$id = $request->id;
    	$data = Training::findOrFail($id);
    	$data->name = $request->name;
    	$data->org_name = $request->org_name;
    	$data->duration = $request->duration;    	
    	$data->completion_year = $request->completion_year;
    	$data->user_id = auth()->user()->id; 
    	$data->save();
    }

    public function deleteTraining(Request $request) { 
    	$id = $request->id;
   		Training::findOrFail($id)->delete();
    }
}
