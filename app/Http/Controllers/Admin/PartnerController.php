<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\PartnerContract;
use App\Http\Controllers\BaseController;

class PartnerController extends BaseController
{
    private $page="admin.partner.";

    protected $partnerRepo;

    public function __construct(
        partnerContract $partnerRepo
    )
    {
        $this->partnerRepo = $partnerRepo;
    }

    public function index()
    {
        $list = $this->partnerRepo->list('created_at', 'desc');
        $datas = $this->partnerRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('partner', 'List of partner');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('partner', 'Create partner');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->partnerRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('partner.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->partnerRepo->findById($id);
        $this->setPageTitle('partner', 'Edit partner');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->partnerRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('partner.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->partnerRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('partner.index', 'Record deleted successfully' ,'success',false, false);
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('partners');
        foreach($allArticles as $_article){
            $article = $this->partnerRepo->findById($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
