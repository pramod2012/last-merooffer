<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\BidContract;
use App\Http\Controllers\BaseController;

class BidController extends BaseController
{
    private $page="admin.bid.";

    protected $bidRepo;

    public function __construct(
        BidContract $bidRepo
    )
    {
        $this->bidRepo = $bidRepo;
    }

    public function index()
    {
        $list = $this->bidRepo->list('created_at', 'desc');
        $bids = $this->bidRepo->paginateArrayResults($list->all(), 10);
        $this->setPageTitle('bids', 'List of bids');
        return view($this->page.'index',compact('bids'));
    }

    public function create()
    {
        $this->setPageTitle('bids', 'Create bid');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->bidRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('bid.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $bid = $this->bidRepo->findById($id);
        $this->setPageTitle('bid', 'Edit bid');
        return view($this->page.'edit', compact('bid'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->bidRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('bid.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->bidRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('bid.index', 'Record deleted successfully' ,'success',false, false);
    }
}
