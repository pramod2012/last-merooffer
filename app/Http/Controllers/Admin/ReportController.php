<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\ReportContract;
use App\Http\Controllers\BaseController;

class ReportController extends BaseController
{
    private $page="admin.report.";

    protected $reportRepo;

    public function __construct(
        ReportContract $reportRepo
    )
    {
        $this->reportRepo = $reportRepo;
    }

    public function index()
    {
        $list = $this->reportRepo->list('created_at', 'desc');
        $datas = $this->reportRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('report', 'List of report');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('report', 'Create report');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->reportRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('report.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->reportRepo->findById($id);
        $this->setPageTitle('report', 'Edit report');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->reportRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('report.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->reportRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('report.index', 'Record deleted successfully' ,'success',false, false);
    }
}
