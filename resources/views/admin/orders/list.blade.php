@extends('admin.partner.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          <h3 class="box-title">{{$subTitle}}</h3>
        </div>
        <div class="col-sm-4">
          @include('admin.partials.flash')
          <a class="btn btn-primary" href="{{ route('partner.create')}}">Add New</a>
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>
      <form method="POST" action="#">
         
      </form>
    <div id="examplekj_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <table id="example5kj" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                <th width="2%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">ID</th>
                <th width="10%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Date</th>
                <th width="7%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Customer</th>
                <th width="10%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Courier</th>

                <th width="10%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Total</th>
                <th width="12%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Address: activate to sort column ascending">Status</th>
              </tr>
            </thead>
            <tbody>
           @foreach($orders as $order)
                <tr role="row" class="odd">
                    <td>{{$order->id}}</td>
                  <td class="sorting_1"><a title="Show order" href="{{ route('orders.show', $order->id) }}">{{ date('M d, Y h:i a', strtotime($order->created_at)) }}</a></td>
                  <td class="hidden-xs">{{$order->user->name}}</td>
                  <td class="hidden-xs">{{$order->courier_id}}</td>
                  <td>
                        <span class="label @if($order->total != $order->total_paid) label-danger @else label-success @endif">Rs. {{ $order->total }}</span>
                    </td>
                    <td><p class="text-center" style="color: #ffffff; background-color: {{ $order->orderStatus->color }}">{{ $order->orderStatus->name }}</p></td>
                  
              </tr>
        @endforeach      
            </tbody>
            
          </table>
        </div>
      </div>
     {{ $orders->links() }}
    </div>
  </div>
  
</div>
    </section>
    
  </div>
@endsection