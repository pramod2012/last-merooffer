@extends('site.partials.main_index')
@section('content')  
<body class="page-template page-template-template-login page-template-template-login-php page page-id-28">
@include('site.partials.navbar')
    <!-- page content -->
<section class="inner-page-content border-bottom top-pad-50">
    <div class="login-register login-register-v1">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-11 col-sm-12 center-block">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="classiera-login-register-heading border-bottom text-center">
                                <h3 class="text-uppercase">Recover Your Password</h3>
                            </div><!--classiera-login-register-heading-->
                                                        
                                    
                                </div>
                            </div><!--social-login-->
                                                    </div><!--col-lg-12-->
                    </div><!--row-->
                    <div class="row">
                        <div class="col-lg-8 center-block">
                            <form method="POST" action="{{ route('password.request') }}" id="register" name="register">
                                {{csrf_field()}}
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3 single-label">
                                            <label for="username">Email : <span class="text-danger">*</span></label>
                                        </div><!--col-lg-3-->
                                        <div class="col-lg-9">
                                            <div class="inner-addon left-addon">
                                                <i class="left-addon form-icon fas fa-envelope"></i>
                                                <input type="text" id="email" name="email" value="{{ old('email') }}" class="form-control form-control-md sharp-edge" placeholder="Your Email">
                                                @if ($errors->has('email'))
                                                <div class="help-block with-errors">{{ $errors->first('email') }}</div>@endif
                                            </div>
                                        </div><!--col-lg-9-->
                                    </div><!--row-->
                                </div><!--UserName-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3 single-label">
                                            <label for="password">New Password : <span class="text-danger">*</span></label>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="inner-addon left-addon">
                                                <i class="left-addon form-icon fas fa-lock"></i>
                                                <input id="password" type="password" name="password" class="form-control form-control-md sharp-edge" placeholder="Enter New Password">
                                                @if ($errors->has('password'))
                                                <div class="help-block with-errors">{{ $errors->first('password') }}</div>@endif
                                            </div>
                                        </div>
                                    </div>
                                </div><!--Password-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3 single-label">
                                            <label for="password">Confirm New Password : <span class="text-danger">*</span></label>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="inner-addon left-addon">
                                                <i class="left-addon form-icon fas fa-lock"></i>
                                                <input id="password-confirm" type="password" name="password_confirmation" class="form-control form-control-md sharp-edge" placeholder="Retype Password">
                                                @if ($errors->has('password_confirmation'))
                                                <div class="help-block with-errors">{{ $errors->first('password_confirmation') }}</div>@endif
                                            </div>
                                        </div>
                                    </div>
                                </div><!--Confirm Password-->
                                <div class="col-lg-9 pull-right flip">
                                    <div class="form-group clearfix">
                                        
                                        <p class="forget-pass pull-right flip">
                                            <a href="{{ route('login') }}">Login?</a>
                                        </p>
                                    </div>
                                    <!--Google-->
                                                                        
                                    <!--Google-->
                                    <div class="form-group">
                                        <input type="hidden" id="submitbtn" name="submit" value="Login" />              
                                        <button class="btn btn-primary sharp btn-md btn-style-one" id="edit-submit" value="Login" type="submit">Submit</button>
                                    </div>                                  
                                    <div class="form-group">
                                        <p>If you don’t have account? 
                                            <a href="{{ route('register') }}">Create an account.</a>
                                        </p>
                                    </div>
                                </div><!--Rememberme-->
                            </form>
                        </div><!--col-lg-8-->
                    </div><!--row-->
                </div><!--col-lg-10-->
            </div><!--row-->    
        </div><!--container-->  
    </div><!--login-register login-register-v1-->
</section>
<!-- page content -->
@include('site.partials.footer')
@endsection