<?php

namespace App\Contracts;

/**
 * Interface FjobContract
 * @package App\Contracts
 */
interface FjobContract extends MyBaseContract
{
	public function findBySlug($slug);

    public function storeFjob($request);

    public function editFjob($request,$id);

}
