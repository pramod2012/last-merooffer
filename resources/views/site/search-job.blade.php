@extends('site.partials.main_index')
@foreach(\App\JobCategory::where('status',1)->get() as $category)
@if(Request::get('category_id') == $category->id)
@section('title')
    {{$category->meta_title}}
@endsection
@section('meta-tags')
<meta name="description" content="{{$category->meta_body}}" />
<meta name="keywords" content="{{$category->meta_keywords}}" />
<meta name="author" content="merooffer.com" />
@endsection
@section('og-tags')
<meta property='og:title' content="{{$category->name}}" />
@if($category->desc == !null)
<meta property='og:description' content='{{$category->desc}}' />@endif
<meta property='og:url' content="{{app('request')->url()}}">
<meta property=" og:type" content="website" />
<meta property="og:site_name" content="{{app('request')->url()}}" />
@endsection

@endif
@endforeach
@section('content')

<body class="archive category category-uncategorized category-1 logged-in">
    @include('site.partials.navbarjob')

    @include('site.partials.searchjob')
    <section class="inner-page-content border-bottom top-pad-50" id="app">
        <div class="container">
            <search-job />
            <!--row-->
        </div>
        <!--container-->
    </section>
    @include('site.partials.footer')
    @section('page-script')
    <script type='text/javascript' src="{{ asset('frontend/js/validator.min.js') }}"></script>
    @stop
    @endsection
