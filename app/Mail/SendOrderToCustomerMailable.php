<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Order;

class SendOrderToCustomerMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'order' => $this->order,
            'products' => $this->order->products,
            'user' => $this->order->user,
            'courier' => $this->order->courier,
            'address' => $this->order->address,
            'status' => $this->order->orderStatus
        ];
        return $this->from('noreply@merooffer.com','Mero Offer')
                    ->subject('Order Sent-'.$this->order->reference)
                    ->view('emails.SendOrderToCustomerMailable',$data);
    }
}
