<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contracts\CartContract;
use App\Contracts\CourierContract;
use App\Contracts\ProductContract;
use App\Contracts\AddressContract;
use App\Shop\Products\Transformations\ProductTransformable;

class CheckoutController extends Controller
{
    use ProductTransformable;
	private $cartRepo;
	private $courierRepo;
	private $productRepo;
	private $addressRepo;

	public function __construct(
        CartContract $cartRepo,
        CourierContract $courierRepo,
        ProductContract $productRepo,
        AddressContract $addressRepo
    )
    {
        $this->cartRepo = $cartRepo;
        $this->courierRepo = $courierRepo;
        $this->productRepo = $productRepo;
        $this->addressRepo = $addressRepo;
    }

    public function index(Request $request){
    	$products = $this->cartRepo->getCartItems();
        $customer = $request->user();
        $billingAddress = $customer->addresses()->first();
        $courier = $this->courierRepo->findById(request()->session()->get('courierId', 1));
        $shippingFee = $this->cartRepo->getShippingFee($courier);

        return view('cart.checkout', [
            'customer' => $customer,
            'billingAddress' => $billingAddress,
            'addresses' => $customer->addresses()->get(),
            'products' => $this->cartRepo->getCartItems(),
            'subtotal' => $this->cartRepo->getSubTotal(),
            'tax' => $this->cartRepo->getTax(),
            'shippingFee' => $shippingFee,
            'total' => $this->cartRepo->getTotal(2, $shippingFee),
            'cartItems' => $this->cartRepo->getCartItemsTransformed()
        ]);
    }
}
