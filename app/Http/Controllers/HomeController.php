<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreFirmuserRequest;
use App\Contracts\FirmuserContract;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(FirmuserContract $firmuserRepo)
    {
        $this->firmuserRepo = $firmuserRepo;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function storeCompany(StoreFirmuserRequest $request) { 
        $this->firmuserRepo->storeFirmuser($request);
        return redirect()->back();
    }
}