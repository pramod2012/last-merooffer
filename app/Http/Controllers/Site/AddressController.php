<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Address;
use App\Http\Requests\StoreAddressRequest;

class AddressController extends Controller
{
    public function index(){
    	return view('cart.address');
    }

    public function store(StoreAddressRequest $request){
    	$data = new Address();
    	$data->alias = $request->alias;
    	$data->address_1 = $request->address_1;
    	$data->address_2 = $request->address_2;
    	$data->province = $request->province;
    	$data->country = $request->country;
    	$data->city = $request->city;
    	$data->state_code = $request->state_code;
    	$data->zip = $request->zip;
    	$data->phone = $request->phone;
    	$data->user_id = auth()->user()->id;
    	$data->status = 1;
    	$data->save();
    	return redirect('/profile');
    }

    public function destroy($id){
    	$data = Address::find($id);
    	$data->delete();
        return redirect('/profile');
    }
}
