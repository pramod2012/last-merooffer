<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFjobRequest;
use App\Contracts\FjobContract;
use App\Contracts\UserContract;
use App\Models\User;
use App\Models\Freelancers\Fjob;

class ProjectController extends Controller
{
    protected $jobRepo;
    public function __construct(FjobContract $fjobRepo)
    {
        $this->middleware(['auth','verified']);
        $this->fjobRepo = $fjobRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()  { 
        $works = Fjob::where('user_id',auth()->user()->id)->orderBy('created_at','desc')->paginate(4);
       
       return view('freelancers.project.index',compact('works'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->cell_activated == 0){
            return redirect()->route('edit-profile')->with('success','Please enter your mobile number and verify your mobile number to post a poject ! Your mobile will be verified within 1 hour or call us in our number for immediate post !');
        }else{
        return view('freelancers.project.create');
    }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFjobRequest $request)
    {
        $fjob = $this->fjobRepo->storeFjob($request);

        return redirect()->route('project.index')->with('success', "Job Posting Created");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $fjob = Fjob::where('user_id',auth()->user()->id)->findOrFail($id);
       if($fjob->user_id != auth()->user()->id) {
          return redirect('/')->with('error', 'Unauthorize Page');
       } 
       return view('freelancers.project.edit',compact('fjob'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreFjobRequest $request, $id)
    {
        $fjob = $this->fjobRepo->editFjob($request, $id);

        return redirect()->route('project.index')->with('success', "Work Posting Edited");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $job = Fjob::where('user_id',auth()->user()->id)->findOrFail($id);
        $job->delete();

        return back()->with('success', "Work Posting Deleted");
    }
}
