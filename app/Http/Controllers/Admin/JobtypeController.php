<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\JobtypeContract;
use App\Http\Controllers\BaseController;

class JobtypeController extends BaseController
{
    private $page="admin.jobtype.";

    protected $jobtypeRepo;

    public function __construct(
        jobtypeContract $jobtypeRepo
    )
    {
        $this->jobtypeRepo = $jobtypeRepo;
    }

    public function index()
    {
        $list = $this->jobtypeRepo->list('created_at', 'desc');
        $datas = $this->jobtypeRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('Job Types', 'List of Job Types');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('Jobtype', 'Create jobtype');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->jobtypeRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('jobtype.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->jobtypeRepo->findById($id);
        $this->setPageTitle('Cities', 'Edit jobtype');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->jobtypeRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('jobtype.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->jobtypeRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('jobtype.index', 'Record deleted successfully' ,'success',false, false);
    }
}
