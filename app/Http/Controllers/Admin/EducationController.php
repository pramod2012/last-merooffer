<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\EducationContract;
use App\Http\Controllers\BaseController;

class EducationController extends BaseController
{
    private $page="admin.education.";

    protected $educationRepo;

    public function __construct(
        EducationContract $educationRepo
    )
    {
        $this->educationRepo = $educationRepo;
    }

    public function index()
    {
        $list = $this->educationRepo->list('created_at', 'desc');
        $datas = $this->educationRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('education', 'List of education');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('education', 'Create education');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->educationRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('education.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->educationRepo->findById($id);
        $this->setPageTitle('education', 'Edit education');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->educationRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('education.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->educationRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('education.index', 'Record deleted successfully' ,'success',false, false);
    }
}
