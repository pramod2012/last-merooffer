<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFjobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fjobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('feature')->default(0);
            $table->string('name');
            $table->text('body');
            $table->float('budget')->nullable();
            $table->float('budget_to')->nullable();
            $table->float('budget_avg')->nullable();
            $table->integer('views')->default(0);
            $table->integer('status')->default(0);
            $table->bigInteger('order');
            $table->string('address')->nullable();
            $table->integer('latitude')->nullable();
            $table->integer('longitude')->nullable();
            $table->dateTime('expired_at')->nullable();
            $table->bigInteger('jobtype_id')->unsigned()->default(1);
            $table->foreign('jobtype_id')->references('id')->on('jobtypes')->onDelete('cascade');
            $table->bigInteger('fcategory_id')->unsigned();
            $table->foreign('fcategory_id')->references('id')->on('fcategories')->onDelete('cascade');
            $table->string('endate');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('duration_id')->unsigned();
            $table->foreign('duration_id')->references('id')->on('durations')->onDelete('cascade');
            $table->string('slug')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fjobs');
    }
}
