<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use App\Models\User;

class Wishlist extends Model
{
    protected $fillable = [
       'product_id',
        'auth_id',
        'status',
        'user_id',
    ];

    public function product(){
    	return $this->belongsTo(Product::class,'product_id');
    }

    public function users_product(){
    	return $this->belongsTo(User::class,'user_id');
    }

    public function wishlist_by(){
        return $this->belongsTo(User::class,'auth_id');
    }
}
