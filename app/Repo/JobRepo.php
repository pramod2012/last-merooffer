<?php

namespace App\Repo;

use App\Job;
use App\Traits\UploadAble;
use Illuminate\Http\UploadedFile;
use App\Contracts\JobContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubmitJobEmail;
use App\Models\User;
use Carbon\Carbon;
use App\Skill;

/**
 * Class jobRepository
 *
 * @package \App\Repositories
 */
class JobRepo extends BaseRepo implements JobContract
{
    use UploadAble;

    /**
     * jobRepository constructor.
     * @param job $model
     */
    public function __construct(Job $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return job|mixed
     */
    public function store($request)
    {
        try {
            $job = new Job();
            $job->name = $request->input('name');
            $job->body = $request->input('body');
            $job->salary = $request->input('salary');
            $job->salary_to = $request->input('salary_to');
            $job->salarytype_id = $request->input('salarytype_id');
            $job->category_id = $request->input('category_id');
            $job->positiontype_id = $request->input('positiontype_id');
            $job->endate = $request->input('endate');
            $job->education = $request->input('education');
            $job->experience_id = $request->input('experience_id');
            $job->gender = $request->input('gender');
            $job->age = $request->input('age');
            $job->vac_no = $request->input('vac_no');
            $job->level_id = $request->input('level_id');
            $job->industry_id = $request->input('industry_id');
            $job->city_id = $request->input('city_id');
            $job->day_id = $request->input('day_id');
            $job->address = $request->input('address');
            $job->user_id = $request->input('user_id');
            $job->salary_avg = collect([$request->salary, $request->salary_to])->avg();
            $job->slug = $this->createSlug($request->name);
            $job->feature = $request->input('feature');
            $job->status = $request->input('status');
            $job->jobtype_id = $request->input('jobtype_id');
            $job->save();

            if($request->day_id == 1)
            {
            $new = Job::find($job->id);
            $new->expired_at = Carbon::now()->addDays(15);
            $new->save();
            }
            if($request->day_id == 2){
            $new = Job::find($job->id);
            $new->expired_at = Carbon::now()->addDays(30);
            $new->save();
            }
            if($request->day_id == 3){
            $new = Job::find($job->id);
            $new->expired_at = Carbon::now()->addDays(45);
            $new->save();
            }
            if($request->day_id == 4){
            $new = Job::find($job->id);
            $new->expired_at = Carbon::now()->addDays(60);
            $new->save();
            }

            $this->handleTags($request, $job);

            return $job;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {

        $job = $this->findById($id);

        $job->name = $request->input('name');
            $job->body = $request->input('body');
            $job->salary = $request->input('salary');
            $job->salary_to = $request->input('salary_to');
            $job->salarytype_id = $request->input('salarytype_id');
            $job->category_id = $request->input('category_id');
            $job->positiontype_id = $request->input('positiontype_id');
            $job->endate = $request->input('endate');
            $job->education = $request->input('education');
            $job->experience_id = $request->input('experience_id');
            $job->gender = $request->input('gender');
            $job->age = $request->input('age');
            $job->vac_no = $request->input('vac_no');
            $job->level_id = $request->input('level_id');
            $job->industry_id = $request->input('industry_id');
            $job->city_id = $request->input('city_id');
            $job->day_id = $request->input('day_id');
            $job->address = $request->input('address');
            $job->user_id = $request->input('user_id');
            $job->salary_avg = collect([$request->salary, $request->salary_to])->avg();
            $job->jobtype_id = $request->input('jobtype_id');
            $job->feature = $request->input('feature');
            $job->status = $request->input('status');

        $job->update();

        $job->skillss()->detach();

        $this->handleTags($request, $job);

        if($request->has('status') == 1)
        {
            $when = now()->addMinutes(10);
            Mail::to($job->user->email)->later($when, new SubmitJobEmail($job));
        }

        return $job;
    }

    public function storeJob($request)
    {
        try {
            $job = new Job();
            $job->name = $request->input('name');
            $job->body = $request->input('body');
            $job->salary = $request->input('salary');
            $job->salary_to = $request->input('salary_to');
            $job->salarytype_id = $request->input('salarytype_id');
            $job->category_id = $request->input('category_id');
            $job->positiontype_id = $request->input('positiontype_id');
            $job->endate = $request->input('endate');
            $job->education = $request->input('education');
            $job->experience_id = $request->input('experience_id');
            $job->gender = $request->input('gender');
            $job->age = $request->input('age');
            $job->vac_no = $request->input('vac_no');
            $job->level_id = $request->input('level_id');
            $job->industry_id = $request->input('industry_id');
            $job->city_id = $request->input('city_id');
            $job->day_id = $request->input('day_id');
            $job->address = $request->input('address');
            $job->user_id = auth()->user()->id;
            $job->salary_avg = collect([$request->salary, $request->salary_to])->avg();
            $job->slug = $this->createSlug($request->name);
            $job->save();

            if($request->day_id == 1)
            {
            $new = Job::find($job->id);
            $new->expired_at = Carbon::now()->addDays(15);
            $new->save();
            }
            if($request->day_id == 2){
            $new = Job::find($job->id);
            $new->expired_at = Carbon::now()->addDays(30);
            $new->save();
            }
            if($request->day_id == 3){
            $new = Job::find($job->id);
            $new->expired_at = Carbon::now()->addDays(45);
            $new->save();
            }
            if($request->day_id == 4){
            $new = Job::find($job->id);
            $new->expired_at = Carbon::now()->addDays(60);
            $new->save();
            }

            $this->handleTags($request, $job);

            return $job;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function editJob($request, $id)
    {
        $job = $this->findById($id);
            $job->name = $request->input('name');
            $job->body = $request->input('body');
            $job->salary = $request->input('salary');
            $job->salary_to = $request->input('salary_to');
            $job->salarytype_id = $request->input('salarytype_id');
            $job->category_id = $request->input('category_id');
            $job->positiontype_id = $request->input('positiontype_id');
            $job->endate = $request->input('endate');
            $job->education = $request->input('education');
            $job->experience_id = $request->input('experience_id');
            $job->gender = $request->input('gender');
            $job->age = $request->input('age');
            $job->vac_no = $request->input('vac_no');
            $job->level_id = $request->input('level_id');
            $job->industry_id = $request->input('industry_id');
            $job->city_id = $request->input('city_id');
            $job->day_id = $request->input('day_id');
            $job->address = $request->input('address');
            $job->user_id = auth()->user()->id; 
            $job->salary_avg = collect([$request->salary, $request->salary_to])->avg();
        $job->update();

        $job->skillss()->detach();

        $this->handleTags($request, $job);

        return $job;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $job = $this->findById($id);
        $job->delete();
        return $job;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $job = Job::where('slug', $slug)->firstOrFail();
        return $job;
    }

    public function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Job::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }

    public function handleTags($request, $job){
    if( ! empty($tags = $request->get('skills'))){

        $tagList = explode(",", $tags);
        // Loop through the tag array that we just created
        foreach ($tagList as $tags) {
        // Get any existing tags
            $tag = Skill::where('skill', '=', $tags)->first();
            // If the tag exists, sync it, otherwise create it
            if ($tag != null) {
                $job->skillss()->attach($tag->id);
            } else {
                $tag = new Skill();
                $tag->skill = $tags;
                $tag->slug = $this->createSlugSkill($tags);
                $tag->save();
                $job->skillss()->attach($tag->id);
            }
        }
      }
        
    }


    public function createSlugSkill($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getSkillRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    
    protected function getSkillRelatedSlugs($slug, $id = 0)
    {
        return Skill::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }

    public function getJobByUser()
    {
        $user_id = Auth()->user()->id;
       $user = User::findOrFail($user_id);
       return Job::where('user_id', $user->id)
               ->orderBy('created_at', 'desc')
               ->paginate(5);
    }
}
