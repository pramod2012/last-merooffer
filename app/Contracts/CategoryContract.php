<?php

namespace App\Contracts;

/**
 * Interface CategoryContract
 * @package App\Contracts
 */
interface CategoryContract extends MyBaseContract
{
    public function findBySlug($slug);

    public function listParent();

    public function treeList();

    public function listAll(); 
}
