@extends('admin.resume.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('resume.store') }}" enctype="multipart/form-data">
                       {{csrf_field()}}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="firstname" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">E-Mail Address*</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('email') }}
                                        </strong>
                                    </span>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">CV / Resume</label>

                            <div class="col-md-6">
                                <input id="cv" type="file" class="form-control" name="cv" value="{{ old('cv') }}">
                                
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('cv') }}
                                        </strong>
                                    </span>
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Message</label>

                            <div class="col-md-6">
                                <textarea rows="4" id="message" type="text" class="form-control" name="message">{{ old('message') }}</textarea>
                                
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('message') }}
                                        </strong>
                                    </span>
                                
                            </div>
                        </div>

                    

                    <div class="form-group">
                      <label class="col-md-4 control-label">Job</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="job_id">
                                  @foreach($jobs as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('job_id') }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
