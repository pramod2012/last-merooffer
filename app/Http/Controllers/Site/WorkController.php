<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Work;

class WorkController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function storeWork(Request $request) {

    	// $this->validate($request, [
     //        'position' => 'required',
     //        'company' => 'required',
     //        'year' => 'required'
     //    ]);

    	$new_work = new Work();
    	$new_work->position = $request->position;
    	$new_work->company = $request->company;
    	$new_work->year = $request->year;    	
    	$new_work->description = $request->description;
    	$new_work->user_id = auth()->user()->id; 
    	$new_work->save();
    }

    public function updateWork(Request $request) {    	
     	$id = $request->id;
    	$work = Work::find($id);
    	$work->position = $request->position;
    	$work->company = $request->company;
    	$work->year = $request->workyear;    	
    	$work->description = $request->description;
    	$work->save();
    }

    public function deleteWork(Request $request) { 
    	$id = $request->id;
   		Work::findOrFail($id)->delete(); 
    }
}
