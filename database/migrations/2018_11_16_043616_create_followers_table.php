<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('followers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('status')->default(0);
            $table->bigInteger('leader_id')->unsigned();
            $table->foreign('leader_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('followed_by')->unsigned();
            $table->foreign('followed_by')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('is_company')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('followers');
    }
}
