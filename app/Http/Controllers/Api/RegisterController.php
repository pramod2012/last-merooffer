<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Rules\Lowercase;
use App\Utils as Utils;
   
class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users', new Lowercase],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'mobile' => 'required|regex:/9[78][\d]{7}\d$/|unique:users',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;
   
        return $this->sendResponse($success, 'User register successfully.');
    }
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

        try{
            if(Auth::attempt(['email' => $request->email, 'password' => $request->password]))
            { 
                $user = Auth::user(); 
                $success['token'] =  $user->createToken('MyAppToken')->accessToken; 
                $success['name'] =  $user->name;
   
                $data = new Utils\SuccessJsonResponse($success,"Logged in Successfully");
                return response()->json($data->json());
            } 
            else{ 
                return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
            } 
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }
}