<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wishlist;

class WishlistController extends Controller
{
    public function post_wishlist(Request $request){
        // return " jk";
        $this->validate($request, [
            'auth_id' => 'required',
            'product_id' => 'required',
            'user_id' => 'required',
        ]);
        $data['auth_id'] = $request->auth_id;
        $data['product_id'] = $request->product_id;
        $data['user_id'] = $request->user_id;
        
        if (Wishlist::create($data)) {
          return array('status'=>'1');
            // return redirect()->back()->with('success', 'Added to your wish list');
        }
        return array('status'=>'0');
        // return redirect()->back();
    }

    public function destroy($id)
    {
        Wishlist::where('id', $id)->delete();
         return redirect()->back()->with('success', 'Data Deleted');
    }
}
