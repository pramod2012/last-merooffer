<?php

namespace App\Repo;

use App\Models\User;
use App\Contracts\UserContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use App\Traits\UploadAble;
use Illuminate\Http\UploadedFile;
use Carbon\Carbon;

/**
 * Class UserRepository
 *
 * @package \App\Repo
 */
class UserRepo extends BaseRepo implements UserContract
{
    use UploadAble;
    /**
     * UserRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function store($request)
    {
        try {
            $data = new User();

            if ($request->has('image')) {
                $image = $this->uploadOne($request->image, 'users');
                $data->image = $image;
            }

            $data->name = $request->name;
            $data->email = $request->email;
            $data->mobile = $request->mobile;
            $data->phone = $request->phone;
            $data->address = $request->address;

            $data->show_email = $request->show_email;
            $data->show_mobile = $request->show_mobile;
            $data->show_phone = $request->show_phone;
            $data->email_verified_at = $request->email_verified_at;

            $data->cell_activated = $request->cell_activated;
            $data->password = bcrypt($request->password);
            $data->activation_key = str_random(6);
            $data->ip = request()->ip();

            $data->save();

            return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
    	try
    	{

        $data = $this->findById($id);

        if ($request->has('image')) {
            if ($data->image != null) {
                    $this->deleteOne($data->image);
                }
            $image = $this->uploadOne($request->image, 'users');
            $data->image = $image;
        }
            $data->name = $request->name;
            $data->email = $request->email;
            $data->mobile = $request->mobile;
            $data->phone = $request->phone;
            $data->address = $request->address;

            $data->show_email = $request->show_email;
            $data->show_mobile = $request->show_mobile;
            $data->show_phone = $request->show_phone;
            $data->email_verified_at = $request->email_verified_at;
            
            $data->cell_activated = $request->cell_activated;

        $data->update();

        return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    public function editUser($request, $id)
    {
        try
        {

        $data = User::where('id', auth()->user()->id)->findOrFail($id);

        if ($request->has('image')) {
            if ($data->image != null) {
                    $this->deleteOne($data->image);
                }
            $image = $this->uploadOne($request->image, 'users');
            $data->image = $image;
        }
        $data->name = $request->name;
        if(auth()->user()->mobile !== $request->mobile )
        {
            $data->cell_activated = 0;
            $data->activation_key = rand(100000, 999999);
        }
        $data->mobile = $request->mobile;
        $data->phone = $request->phone;
        $data->show_phone = $request->show_phone;
        $data->show_email = $request->show_email;
        $data->show_mobile = $request->show_mobile;
        

        $data->update();

        return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);
        $data->delete();
        return $data;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $data = User::where('slug', $slug)->first();
        return $data;
    }
}
