@extends('site.partials.main_index')
@section('title','Favourite Ads')
@section('content')
<body class="page-template page-template-template-favorite page-template-template-favorite-php page page-id-22 logged-in">
@include('site.partials.navbar')
<section class="user-pages section-gray-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-4">
      @include('site.partials.sidebar')     
</div><!--col-lg-3-->
      <div class="col-lg-9 col-md-8 user-content-height">
        <div class="user-detail-section section-bg-white">
          <!-- favorite ads -->
          <div class="user-ads favorite-ads">
            <h4 class="user-detail-section-heading text-uppercase">
            Favorite Ads            </h4>
                                                <!--singlepost-->
                                                @forelse($wishlists as $value)
            <div class="media border-bottom">
                            <div class="media-left">
                              @if ($value->product->images->count() > 0)
                    <img class="media-object" src="{{ asset('images_small/'.$value->product->images->first()->full) }}" alt="{{$value->product->name}}">
                    @else
                    <img class="media-object" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
                    @endif
                                            </div><!--media-left-->
                            <div class="media-body">
                                <h5 class="media-heading">
                  <a href="{{ route('product_details', $value->product->slug) }}">
                    {{$value->product->name}}                </a>
                </h5>
                                <p>
                                                      <span>
                                        <i class="fas fa-user"></i>
                    {{$value->product->user->name}}             </span>
                                                      <span>
                                        <i class="far fa-clock"></i>
                            {{$value->product->created_at->format('F j, Y')}}                                    </span>
                                </p>
                            </div><!--media-body-->
                            <div class="media-right">
                                                <h4>
                  Rs. {{number_format($value->product->price)}}              </h4>
                                    <form method="post" action="{{route('wishlist.destroy',$value->id)}}" class="unfavorite">
                        {{ csrf_field() }}
                        {{ method_field('DELETE')}}
      <div class="unfavorite">
        <button type="submit" name="unfavorite" class="btn btn-primary sharp btn-style-one btn-sm"><i class="icon-left far fa-heart"></i>unfavorite</button>
      </div>
    </form>
                                </div><!--media-right-->
                        </div><!--media border-bottom-->
            <!--singlepost-->
            @empty
            <div  class="media border-bottom">You do not have any favourite ads ! </div>
              @endforelse         
                      </div><!--user-ads-->
          
          <!-- favorite ads -->
        </div><!--user-detail-->
      </div><!--col-lg-9-->
    </div><!--row-->
  </div><!--container-->
</section><!--user-pages-->
<!-- Company Section Start-->
<!-- Company Section End--> 
@include('site.partials.footer')
@endsection