<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJattributeJobCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jattribute_job_category', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('jattribute_id')->unsigned();
            $table->foreign('jattribute_id')->references('id')->on('attributes')->onDelete('cascade');
            $table->bigInteger('jobcategory_id')->unsigned();
            $table->foreign('jobcategory_id')->references('id')->on('job_categories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jattribute_job_category');
    }
}
