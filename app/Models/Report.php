<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Reportname;
use App\Models\Product;
use App\Models\User;

class Report extends Model
{
    protected $fillable = [
    	'body',
       'product_id',
        'user_id',
        'reportname_id',
        'auth_id',
        'url',
        'status'
    ];

    public function reportname(){
    	return $this->belongsTo(Reportname::class,'reportname_id');
    }

    public function product(){
    	return $this->belongsTo(Product::class,'product_id');
    }

    public function user(){
    	return $this->belongsTo(User::class,'user_id');
    }

    public function authuser(){
        return $this->belongsTo(User::class,'auth_id');
    }
}
