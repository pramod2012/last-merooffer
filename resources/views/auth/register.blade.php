@extends('site.partials.main_index')
@section('title','Register at Mero Offer')
@section('content')  
<body class="page-template page-template-template-login page-template-template-login-php page page-id-28">
@include('site.partials.navbar')
    <!-- page content -->
<section class="inner-page-content border-bottom top-pad-50">
    <div class="login-register login-register-v1">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-11 col-sm-12 center-block">
                    <div class="row">
                        <div class="col-lg-12">
                    <div class="classiera-login-register-heading border-bottom text-center">
                                <h3 class="text-uppercase">Register</h3>
                    </div>

                    
                            <div class="social-login border-bottom">
                                <h5 class="text-uppercase text-center">
                                Log in or sign up with a social account                             </h5>
                                <!--NextendSocialLogin-->
                                                                <!--AccessPress Socil Login-->
                                    <div class="apsl-login-networks theme-1 clearfix">
                                <div class="social-networks">
                                    
                                    
                                <a href="/login/facebook"><img src="{{asset('frontend/social-icons/facebook.png')}}" height="50" width="50"></a><span></span>
                                <a href="/login/google"><img src="{{asset('frontend/social-icons/google.png')}}" height="50" width="50"></a><br><br>
                        
                    </a>
                            </div>
    </div>
                                <!--AccessPress Socil Login-->
                                <!--Social Plugins-->
                                <div class="social-login-or">
                                    <span>OR</span>
                                </div>
                            </div>
                        </div><!--col-lg-12-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col-lg-8">
                            <form method="POST" action="{{ route('register') }}">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3 single-label">
                                            <label for="username">Full Name : <span class="text-danger">*</span></label>
                                        </div><!--col-lg-3-->
                                        <div class="col-lg-9">
                                            <div class="inner-addon left-addon">
                                                <i class="left-addon form-icon fas fa-user"></i>
                                                <input type="text" id="mobile" name="name" value="{{old('name')}}" class="form-control form-control-md sharp-edge" placeholder="Your Full Name" data-error="Full Name is required">
                                @if ($errors->has('name'))
                                    <div>
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                                            </div>
                                        </div><!--col-lg-9-->
                                    </div><!--row-->
                                </div><!--UserName-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3 single-label">
                                            <label for="email">Email : <span class="text-danger">*</span></label>
                                        </div><!--col-lg-3-->
                                        <div class="col-lg-9">
                                            <div class="inner-addon left-addon">
                                                <i class="left-addon form-icon fas fa-envelope"></i>
                                                <input type="text" id="email" name="email" value="{{old('email')}}" class="form-control form-control-md sharp-edge" placeholder="Your Email Address" data-error="Email is required">
                                @if ($errors->has('email'))
                                    <div>
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                @endif
                                            </div>
                                        </div><!--col-lg-9-->
                                    </div><!--row-->
                                </div><!--UserName-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3 single-label">
                                            <label for="username">Mobile Number : <span class="text-danger">*</span></label>
                                        </div><!--col-lg-3-->
                                        <div class="col-lg-9">
                                            <div class="inner-addon left-addon">
                                                <i class="left-addon form-icon fas fa-mobile"></i>
                                                <input type="text" id="mobile" name="mobile" value="{{old('mobile')}}" class="form-control form-control-md sharp-edge" placeholder="Your Mobile Number" data-error="Mobile number is required">
                                @if ($errors->has('mobile'))
                                    <div>
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </div>
                                @endif
                                            </div>
                                        </div><!--col-lg-9-->
                                    </div><!--row-->
                                </div><!--UserName-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3 single-label">
                                            <label for="password">Password : <span class="text-danger">*</span></label>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="inner-addon left-addon">
                                                <i class="left-addon form-icon fas fa-lock"></i>
                                                <input id="password" type="password" name="password" class="form-control form-control-md sharp-edge" placeholder="Enter Password" data-error="Password required" value="{{old('password')}}">
                                                <div>{{ $errors->first('password') }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--Password-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3 single-label">
                                            <label for="password">Confirm Password:</label>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="inner-addon left-addon">
                                                <i class="left-addon form-icon fas fa-lock"></i>
                                                <input id="password-confirm" type="password" name="password_confirmation" class="form-control form-control-md sharp-edge" placeholder="Re-enter Password" data-error="Password required" value="{{old('password_confirmation')}}">
                                                <div>{{ $errors->first('password_confirmation') }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--Password-->
                                
                                <div class="col-lg-9 pull-right flip">
                                    <div class="form-group clearfix">
                                        <div class="checkbox pull-left flip">
                                            
                                            <label for="remember">By signing up I agree to <a href="{{url('blog/terms-of-use')}}" target="_blank">Merooffer's Terms of Use</a>  and <a href="{{url('blog/privacy-policy')}}" target="_blank">Privacy Policy</a>, <a href="{{url('blog/posting-policy')}}" target="_blank">Posting Policy</a> and I consent to receiving marketing from Merooffer.</label>
                                        </div>
                                        
                                    </div>
                                    <!--Google-->
                                                                        
                                    <!--Google-->
                                    <div class="form-group">
                                        <input type="hidden" id="submitbtn" name="submit" value="Login" />              
                                        <button class="btn btn-primary sharp btn-md btn-style-one" id="edit-submit" name="op" value="Login" type="submit">Register</button>
                                    </div>                                  
                                    <div class="form-group">
                                        <p>Already have account? 
                                            <a href="{{ route('login') }}">Login.</a>
                                        </p>
                                    </div>
                                </div><!--Rememberme-->
                            </form>
                        </div><!--col-lg-8-->
                    </div><!--row-->
                </div><!--col-lg-10-->
            </div><!--row-->    
        </div><!--container-->  
    </div><!--login-register login-register-v1-->
</section>
<!-- page content -->
@include('site.partials.footer')
@endsection