<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Contracts\AttributeContract;
use App\Models\Attribute;
use App\Contracts\AttributevalueContract;

class AttributevalueController extends BaseController
{
    /**
     * @var AttributeContract
     */
    protected $attributevalueRepo;
    protected $attributeRepo;

    /**
     * AttributevalueController constructor.
     * @param AttributevalueContract $attributeRepository
     */
    public function __construct(
        AttributeContract $attributeRepo,
         AttributevalueContract $attributevalueRepo
     )
    {
        $this->attributeRepo = $attributeRepo;
        $this->attributevalueRepo = $attributevalueRepo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $list = $this->attributevalueRepo->list('created_at', 'desc');
        $datas = $this->attributevalueRepo->paginateArrayResults($list->all(), 10);

        $this->setPageTitle('Attribute Values', 'List of all attribute values');
        return view('admin.attributevalue.index', compact('datas'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $options = Attribute::where('type','<>','checkbox')->get();
        $this->setPageTitle('Attribute Values', 'Create Attribute Values');
        return view('admin.attributevalue.create',compact('options'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $attribute = $this->attributevalueRepo->store($request);

        if (!$attribute) {
            return $this->responseRedirectBack('Error occurred while creating attribute.', 'error', true, true);
        }
        return $this->responseRedirect('attributevalue.index', 'Attribute added successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data = $this->attributevalueRepo->findById($id);
        $options = Attribute::where('type','<>','checkbox')->get();

        $this->setPageTitle('Attribute Values', 'Edit Attribute Values : '.$data->name);
        return view('admin.attributevalue.edit', compact('data','options'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request,$id)
    {
        $data = $this->attributevalueRepo->edit($request,$id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('attributevalue.index', 'Record updated successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $data = $this->attributevalueRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting attribute.', 'error', true, true);
        }
        return $this->responseRedirect('attributevalue.index', 'Attribute deleted successfully' ,'success',false, false);
    }
}
