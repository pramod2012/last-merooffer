<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\Day;
use App\Models\Pricetype;
use App\Models\User;
use App\Models\City;
use App\Models\Adtype;
use App\Models\Comment;
use App\Models\Bid;
use App\Models\Attributevalue;
use App\Models\Amenity;
use App\Models\Pricelabel;
use Gloudemans\Shoppingcart\Contracts\Buyable;
use App\Models\ProductImage;
use App\Models\Assured;
use App\Models\Productattribute;

class Product extends Model implements Buyable
{    
    protected $fillable = [
       'price','desc','weight'
    ];
    /**
     * @var string
     */
    protected $table = 'products';

    /**
     * @var array
     */

    /**
     * @var array
     */
    protected $casts = [
        'status'    =>  'boolean',
        'featured'  =>  'boolean'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->order = $model->max('order') + 1;
        });
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function condition(){
        return $this->belongsTo(Condition::class,'condition_id');
    }
    
    public function pricelabel(){
        return $this->belongsTo('App\Models\Pricelabel','pricelabel_id');
    }

    public function pricetype(){
        return $this->belongsTo(Pricetype::class,'pricetype_id');
    }

    public function city(){
        return $this->belongsTo(City::class,'city_id');
    }

    public function adtype(){
        return $this->belongsTo(Adtype::class,'adtype_id');
    }

     public function day(){
        return $this->belongsTo(Day::class,'day_id');
    }

    public function assured(){
        return $this->belongsTo(Assured::class,'assured_id');
    }

    public function categories(){
        return $this->belongsToMany(Category::class,'product_categories')->orderBy('id','asc');
    }

    public function comments(){
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }

    public function bids(){
        return $this->morphMany(Bid::class, 'bidable')->whereNull('parent_id');
    }

    public function isAddedToLikelist($product_id){
        if (Auth::check()){
        $user = Auth::user()->id;
        $count=app('db')->table('likes')->where('signup_id', $user)->where('product_id',$product_id)->where('status', 1)->count();
        return $count?true:false;
        }   
    }

    public function isAddedToDislikelist($product_id){
        if (Auth::check()){
        $user = Auth::user()->id;
        $count=app('db')->table('likes')->where('signup_id', $user)->where('product_id',$product_id)->where('status', 0)->count();
        return $count?true:false;
        }   
    }

     public function isAddedToLovelist($product_id){
        if (Auth::check()){
        $user = Auth::user()->id;
        $count=app('db')->table('wishlists')->where('auth_id', $user)->where('product_id',$product_id)->count();
        return $count?true:false;
        }   
    }

    public function isAddedToRatelist($product_id){
        if (Auth::check()){
        $user = Auth::user()->id;
        $count=app('db')->table('ratings')->where('signup_id', $user)->where('product_id',$product_id)->count();
        return $count?true:false;
        }
    }

    public function attributevalues(){
        return $this->belongsToMany(Attributevalue::class,'product_attributevalues');
    }

    public function order(){
        return $this->belongsToMany(Order::class,'order_product');
    }

    public function productattributes()
    {
        return $this->hasMany(Productattribute::class);
    }

    public function amenities(){
        return $this->belongsToMany(Amenity::class,'product_amenities');
    }

    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    /**
     * Get the description or title of the Buyable item.
     *
     * @param null $options
     * @return string
     */
    public function getBuyableDescription($options = null)
    {
        return $this->name;
    }

    /**
     * Get the price of the Buyable item.
     *
     * @param null $options
     * @return float
     */
    public function getBuyablePrice($options = null)
    {
        return $this->price;
    }

    public function getBuyableWeight($options = null)
    {
        return $this->weight;
    }
}
