<?php

namespace App\Contracts;

use App\Models\Product;
use Illuminate\Support\Collection;
use App\Models\Productattribute;
use App\Models\Attributevalue;

/**
 * Interface ProductContract
 * @package App\Contracts
 */
interface ProductContract extends MyBaseContract
{
    public function storeProduct($request);

    public function updateProduct($request,$id);

    public function myAds();
}
