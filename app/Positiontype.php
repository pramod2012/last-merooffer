<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Positiontype extends Model
{
    protected $table = 'positiontypes';

    protected $fillable = [
        'name'
    ];
}
