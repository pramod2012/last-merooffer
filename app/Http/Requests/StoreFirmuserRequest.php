<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFirmuserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'logo' => 'required|image|mimes:jpeg,jpg,png,gif|max:10000',
            'img1' => 'nullable|mimes:jpeg,jpg,png,gif|max:10000',
            'img2' => 'nullable|mimes:jpeg,jpg,png,gif|max:10000',
            'url' => 'nullable',
            'desc' => 'nullable',
            'phone' => 'required|numeric',
            'email' => 'nullable|email',
            'fb' => 'nullable',
            'twitter' => 'nullable',
            'linkedin' => 'nullable',
            'insta' => 'nullable',
            'youtube' => 'nullable',
        ];
    }

    public function attributes()
    {
        return [
        'name' => 'company name',
        'img1' => 'Banner',
        'img2' => 'Banner',
        'desc' => 'Description',
        ];
    }
}
