<?php

namespace App\Contracts;

/**
 * Interface JobContract
 * @package App\Contracts
 */
interface JobContract extends MyBaseContract
{
	public function findBySlug($slug);
    
    public function storeJob($request);

    public function editJob($request,$id);

    public function getJobByUser();
}
