<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Bid;

class SubmitBidEmailReceipient extends Mailable
{
    use Queueable, SerializesModels;
    protected $bid;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Bid $bid)
    {
        $this->bid = $bid;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@merooffer.com')
                    ->subject('Submit Offer Email Receipient')
                    ->view('emails.submitBidEmailReceipient')
                    ->with([
                        'id' => $this->bid->product->id,
                        'price' => $this->bid->product->price,
                        'name' =>$this->bid->product->user->name,
                        'email' => $this->bid->product->user->email,
                        'posted_by' =>$this->bid->user->name,
                        'posted_by_email'=>$this->bid->user->email,
                        'body' => $this->bid->body,
                        'url' => $this->bid->url,
                    ]);
    }
}
