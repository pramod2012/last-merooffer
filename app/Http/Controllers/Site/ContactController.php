<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\ContactContract;
use App\Http\Requests\StoreContactRequest;
use App\Utils\Util;

class ContactController extends Controller
{
    protected $contactRepo;

    public function __construct(
        ContactContract $contactRepo
    ) {
        $this->contactRepo = $contactRepo;
    }

    public function storeContact(StoreContactRequest $request)
    {
        $contact = $this->contactRepo->store($request);
        return Util::successJson([], "Thank you! We will get back to you as soon as possible.");
        // return back()->with('purpose', 'Thank you! We will get back to you as soon as possible.');
    }
}
