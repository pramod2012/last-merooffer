@extends('admin.training.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('training.update', $data->id) }}" enctype="multipart/form-data">
                       {{csrf_field()}}
                       {{method_field('PATCH')}}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name',$data->name) }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="org_name" class="col-md-4 control-label">Org Name</label>

                            <div class="col-md-6">
                                <input rows="4" id="org_name" type="text" class="form-control" name="org_name" value="{{ old('org_name', $data->org_name) }}">
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('org_name') }}
                                        </strong>
                                    </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="duration" class="col-md-4 control-label">Duration</label>
                            <div class="col-md-6">
                                <input id="duration" type="text" class="form-control" name="duration" value="{{ old('duration', $data->duration) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="completion_year" class="col-md-4 control-label">Completion Year</label>
                            <div class="col-md-6">
                                <input id="completion_year" type="text" class="form-control" name="completion_year" value="{{ old('completion_year', $data->completion_year) }}">
                            </div>
                        </div>

                        

                    <div class="form-group">
                      <label class="col-md-4 control-label">User ID</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="user_id">
                              <option value="{{$data->user_id}}">{{$data->user->name}}</option>
                            </select>
                                <span class="text-danger">{{ $errors->first('user_id') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
