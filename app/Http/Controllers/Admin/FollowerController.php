<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\FollowerContract;
use App\Http\Controllers\BaseController;

class FollowerController extends BaseController
{
    private $page="admin.follower.";

    protected $followerRepo;

    public function __construct(
        FollowerContract $followerRepo
    )
    {
        $this->followerRepo = $followerRepo;
    }

    public function index()
    {
        $list = $this->followerRepo->list('created_at', 'desc');
        $datas = $this->followerRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('Follower', 'List of Follower');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('Follower', 'Create follower');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->followerRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('follower.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->followerRepo->findById($id);
        $this->setPageTitle('Follower', 'Edit follower');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->followerRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('follower.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->followerRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('follower.index', 'Record deleted successfully' ,'success',false, false);
    }
}
