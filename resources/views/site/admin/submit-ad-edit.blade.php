@extends('site.partials.main_index')
@section('title','Submit Edit Ads')
@section('content')

<body
    class="page-template page-template-template-submit-ads page-template-template-submit-ads-php page page-id-44 logged-in">
    @include('site.partials.navbar')
    <section class="user-pages section-gray-bg" id="app">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    @include('site.partials.sidebar')
                </div>
                <!--col-lg-3 col-md-4-->

                <div class="col-lg-9 col-md-8 user-content-height">
                    <div class="submit-post section-bg-white">
                        <ad-submit-form mode="edit" :product="{{$product}}" />
                    </div>
                    <!--submit-post-->
                </div>
                <!--col-lg-9 col-md-8 user-content-heigh-->
            </div>
            <!--row-->
        </div>
        <!--container-->
    </section>
    <!--user-pages-->
    <script>
        function initMap() {
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
      center: {lat: 22.3038945, lng: 70.80215989999999},
      zoom: 13
    });
    var input = document.getElementById('address');

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();

        /* If the place has a geometry, then present it on a map. */
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
        marker.setIcon(({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);

        /* Location details */
        document.getElementById('location-snap').innerHTML = place.formatted_address;
        document.getElementById('lat-span').innerHTML = place.geometry.location.lat();
        document.getElementById('lon-span').innerHTML = place.geometry.location.lng();
        document.getElementById("lat").value = place.geometry.location.lat();
    document.getElementById("lng").value = place.geometry.location.lng();

    });
}
    </script>

    <style>
        /* Set the size of the div element that contains the map */
        #map {
            height: 400px;
            /* The height is 400 pixels */
            width: 100%;
            /* The width is the width of the web page */
        }
    </style>
    {{-- <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBV-GD5AgIYUu8op1bHFZlXEfpdy3X7MyE&libraries=places&callback=initMap"
        async defer></script> --}}
    @include('site.partials.footer')
    @section('page-script')
    <script type='text/javascript' src="{{ asset('frontend/js/validator.min.js') }}"></script>
    @stop
    @endsection
