@extends('admin.report.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          <h3 class="box-title">{{$subTitle}}</h3>
        </div>
        <div class="col-sm-4">
          @include('admin.partials.flash')
          <a class="btn btn-primary" href="#">Add New Report</a>
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>
      <form method="POST" action="#">
         
      </form>
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                <th width="2%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">Id</th>
                <th width="8%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">Report Name</th>
                <th width="8%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">Body</th>
                <th width="8%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">Product</th>
                <th width="12%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">Product of User</th>
                <th width="12%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">Reported by User</th>
                <th width="12%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">URL</th>
                <th width="2%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">Status</th>
                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="2" aria-label="Action: activate to sort column ascending">Action</th>
              </tr>
            </thead>
            <tbody>
@foreach($datas as $value)
                <tr role="row" class="odd">
                  <td class="sorting_1">{{$value->id}}</td>
                  <td class="sorting_1"> {{@$value->reportname->name}}</td>
                  <td class="sorting_1"> {{$value->body}}</td>
                  <td class="hidden-xs">{{$value->product->name}}</td>
                  <td class="sorting_1">{{$value->user->name}}</td>
                  <td class="sorting_1">{{$value->authuser->name}}</td>
                  <td class="sorting_1">{{$value->url}}</td>
                  <td class="sorting_1">
                    <i class="{{$value->status == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                  </td>
                  <td>
                    <form class="row" method="POST" action="{{ route('report.destroy', $value->id) }}" onsubmit = "return confirm('Are you sure?')">
                        {{ csrf_field() }}
                      {{ method_field('DELETE')}}
                      <a href="{{ route('report.edit',$value->id) }}" class="btn btn-warning col-sm-3 col-xs-5 btn-margin">
                        Update
                        </a>
                         <button type="submit" class="btn btn-danger col-sm-3 col-xs-5 btn-margin">
                          Delete
                        </button>
                    </form>
                  </td>
              </tr>
           @endforeach
            </tbody>
        
          </table>
        </div>
      </div>
      {{$datas->links()}}
    </div>
  
  </div>
   
</div>
    </section>
    
  </div>
@endsection