<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference')->unique();
            $table->bigInteger('courier_id')->unsigned()->index();
            $table->foreign('courier_id')->references('id')->on('couriers');
            $table->string('courier')->nullable();
            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->bigInteger('address_id')->unsigned()->index();
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->bigInteger('order_status_id')->unsigned()->index();
            $table->foreign('order_status_id')->references('id')->on('order_statuses');
            $table->string('payment');
            $table->decimal('discounts',18,2)->default(0.00);
            $table->decimal('total_products',18,2);
            $table->decimal('total_shipping',18,2)->default(0);
            $table->decimal('tax',18,2)->default(0.00);
            $table->decimal('total',18,2);
            $table->decimal('total_paid',18,2)->default(0.00);
            $table->string('invoice')->nullable();
            $table->string('label_url')->nullable();
            $table->string('tracking_number')->nullable();
            $table->longText('cancel_reason')->nullable();
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
