<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\WishlistContract;
use App\Http\Controllers\BaseController;

class WishlistController extends BaseController
{
    private $page="admin.wishlist.";

    protected $wishlistRepo;

    public function __construct(
        WishlistContract $wishlistRepo
    )
    {
        $this->wishlistRepo = $wishlistRepo;
    }

    public function index()
    {
        $list = $this->wishlistRepo->list('created_at', 'desc');
        $datas = $this->wishlistRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('wishlist', 'List of wishlist');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('wishlist', 'Create wishlist');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->wishlistRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('wishlist.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->wishlistRepo->findById($id);
        $this->setPageTitle('wishlist', 'Edit wishlist');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->wishlistRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('wishlist.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->wishlistRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('wishlist.index', 'Record deleted successfully' ,'success',false, false);
    }
}
