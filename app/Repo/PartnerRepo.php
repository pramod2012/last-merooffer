<?php

namespace App\Repo;

use App\Models\Partner;
use App\Contracts\PartnerContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use App\Traits\UploadAble;
use Illuminate\Http\UploadedFile;
/**
 * Class PartnerRepository
 *
 * @package \App\Repo
 */
class PartnerRepo extends BaseRepo implements PartnerContract
{
	use UploadAble;
    /**
     * PartnerRepository constructor.
     * @param Partner $model
     */
    public function __construct(Partner $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function store($request)
    {
        try {
            $params = $request->all();

            $collection = collect($params);

            $image = null;

            if ($collection->has('image') && ($params['image'] instanceof  UploadedFile)) {
                $image = $this->uploadOne($params['image'], 'partners');
            }

            $status = $collection->has('status') ? 1 : 0;

            $merge = $collection->merge(compact('status', 'image'));

            $data = new Partner($merge->all());

            $data->save();

            return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
    	try
    	{

    	$params = $request->all();

        $data = $this->findById($id);

        $collection = collect($params);

        if ($collection->has('image') && ($params['image'] instanceof  UploadedFile)) {

            if ($data->image != null) {
                $this->deleteOne($data->image);
            }

            $image = $this->uploadOne($params['image'], 'partners');
        }
        $status = $collection->has('status') ? 1 : 0;

        $merge = $collection->merge(compact('image','status'));

        $data->update($merge->all());

        return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);
        if ($data->image != null) {
            $this->deleteOne($data->image);
        }
        $data->delete();
        return $data;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $data = Partner::where('slug', $slug)->firstOrFail();
        return $data;
    }
}