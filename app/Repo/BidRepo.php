<?php

namespace App\Repo;

use App\Models\Bid;
use App\Models\Product;
use App\Contracts\BidContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubmitBidEmail;
use App\Mail\SubmitBidEmailReceipient;

/**
 * Class BidRepository
 *
 * @package \App\Repo
 */
class BidRepo extends BaseRepo implements BidContract
{
    /**
     * BidRepository constructor.
     * @param Bid $model
     */
    public function __construct(Bid $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function store($request)
    {
        try {
            $bid = new Bid;
            $bid->bid = $request->get('bid');
            $bid->url = $request->get('url');
            $bid->body = $request->get('body');
            $bid->user()->associate($request->user());
            $product = Product::find($request->get('product_id'));
            $product->bids()->save($bid);

            Mail::to($product->user->email)->send(new SubmitBidEmailReceipient($bid));
            Mail::to($bid->user->email)->send(new SubmitBidEmail($bid));

            return $product;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
        try
        {
            $bid = Bid::findOrFail($id);
            $bid->bid = $request->get('bid');
            $bid->url = $request->get('url');
            $bid->body = $request->get('body');
            $bid->status = $request->get('status');
            $bid->user_id = $request->get('user_id');
            $product = Product::find($request->get('product_id'));
            $product->bids()->save($bid);
            if($request->status == '1')
            {
                Mail::to($product->user->email)->send(new SubmitBidEmailReceipient($bid));
                Mail::to($bid->user->email)->send(new SubmitBidEmail($bid));
            }
            return $product;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);
        $data->delete();
        return $data;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $data = Bid::where('slug', $slug)->firstOrFail();
        return $data;
    }
}
