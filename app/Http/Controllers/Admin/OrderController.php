<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\OrderContract;
use App\Contracts\UserContract;
use App\Contracts\OrderStatusContract;
use App\Http\Controllers\BaseController;

class OrderController extends BaseController
{
    private $page="admin.orders.";

    protected $orderRepo;
    protected $userRepo;
    protected $orderStatusRepo;

    public function __construct(
        OrderContract $orderRepo,
        UserContract $userRepo,
        OrderStatusContract $orderStatusRepo
    )
    {
        $this->orderRepo = $orderRepo;
        $this->userRepo = $userRepo;
        $this->orderStatusRepo = $orderStatusRepo;
    }

    public function index()
    {
        $list = $this->orderRepo->listOrders('created_at', 'desc');
        $orders = $this->orderRepo->paginateArrayResults($list->all(), 5);
        $this->setPageTitle('order', 'List of order');
        return view($this->page.'list', ['orders' => $orders]);
    }

    public function show($id)
    {
        $this->setPageTitle('order', 'Show order');
        $order = $this->orderRepo->findOrderById($id);

        $orderRepo = new \App\Repo\OrderRepo($order);
        $order->courier = $orderRepo->getCouriers()->first();
        $order->address = $orderRepo->getAddresses()->first();
        $items = $orderRepo->listOrderedProducts();

        return view('admin.orders.show', [
            'order' => $order,
            'items' => $items,
            'customer' => $this->userRepo->findById($order->user_id),
            'currentStatus' => $this->orderStatusRepo->findById($order->order_status_id),
            'payment' => $order->payment,
            'user' => auth()->user()
        ]);
    }

    public function create()
    {
        $this->setPageTitle('order', 'Create order');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->orderRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('order.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $this->setPageTitle('order', 'Edit order');
        $order = $this->orderRepo->findOrderById($id);

        $orderRepo = new \App\Repo\OrderRepo($order);
        $order->courier = $orderRepo->getCouriers()->first();
        $order->address = $orderRepo->getAddresses()->first();
        $items = $orderRepo->listOrderedProducts();

        return view('admin.orders.edit', [
            'statuses' => $this->orderStatusRepo->list(),
            'order' => $order,
            'items' => $items,
            'customer' => $this->userRepo->findById($order->user_id),
            'currentStatus' => $this->orderStatusRepo->findById($order->order_status_id),
            'payment' => $order->payment,
            'user' => auth()->user()
        ]);
    }

    /**
     * @param Request $request
     * @param $orderId
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $order = $this->orderRepo->findOrderById($id);
        $orderRepo = new \App\Repo\OrderRepo($order);

        if ($request->has('total_paid') && $request->input('total_paid') != null) {
            $orderData = $request->except('_method', '_token');
        } else {
            $orderData = $request->except('_method', '_token', 'total_paid');
        }

        $orderRepo->updateOrder($orderData);

        return redirect()->route('orders.edit', $id);
    }

    /**
     * Generate order invoice
     *
     * @param int $id
     * @return mixed
     */
    public function generateInvoice(int $id)
    {
        $order = $this->orderRepo->findOrderById($id);

        $data = [
            'order' => $order,
            'products' => $order->products,
            'customer' => $order->customer,
            'courier' => $order->courier,
            'address' => $this->transformAddress($order->address),
            'status' => $order->orderStatus,
            'payment' => $order->paymentMethod
        ];

        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadView('invoices.orders', $data)->stream();
        return $pdf->stream();
    }

    /**
     * @param Collection $list
     * @return array
     */
    private function transFormOrder(Collection $list)
    {
        $courierRepo = new CourierRepository(new Courier());
        $customerRepo = new CustomerRepository(new Customer());
        $orderStatusRepo = new OrderStatusRepository(new OrderStatus());

        return $list->transform(function (Order $order) use ($courierRepo, $customerRepo, $orderStatusRepo) {
            $order->courier = $courierRepo->findCourierById($order->courier_id);
            $order->customer = $customerRepo->findCustomerById($order->customer_id);
            $order->status = $orderStatusRepo->findOrderStatusById($order->order_status_id);
            return $order;
        })->all();
    }

    public function cancelOrder(){
        
    }
}
