<?php

namespace App\Contracts;

/**
 * Interface JobtypeContract
 * @package App\Contracts
 */
interface JobtypeContract extends MyBaseContract
{
	public function findBySlug($slug);
}
