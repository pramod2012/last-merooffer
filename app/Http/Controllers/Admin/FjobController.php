<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\FcategoryContract;
use App\Contracts\PositiontypeContract;
use App\Contracts\SalarytypeContract;
use App\Contracts\DayContract;
use App\Contracts\FjobContract;
use App\Contracts\JobtypeContract;
use App\Contracts\UserContract;
use App\Contracts\LevelContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests\StoreFjobRequest;

class FjobController extends BaseController
{
    protected $fcategoryRepo;
    protected $salarytypeRepo;
    protected $fjobRepo;
    protected $userRepo;
    protected $jobtypeRepo;
    protected $levelRepo;
    protected $positiontypeRepo;

    public function __construct(
        FcategoryContract $fcategoryRepo,
        SalarytypeContract $salarytypeRepo,
        FjobContract $fjobRepo,
        UserContract $userRepo,
        JobtypeContract $jobtypeRepo,
        LevelContract $levelRepo,
        PositiontypeContract $positiontypeRepo
    )
    {
        $this->fcategoryRepo = $fcategoryRepo;
        $this->salarytypeRepo = $salarytypeRepo;
        $this->fjobRepo = $fjobRepo;
        $this->userRepo = $userRepo;
        $this->jobtypeRepo = $jobtypeRepo;
        $this->levelRepo = $levelRepo;
        $this->positiontypeRepo = $positiontypeRepo;
    }

    public function index()
    {
        $fjobs = $this->fjobRepo->list();

        $this->setPageTitle('Freelancer Jobs', 'Freelancer Jobs List');
        return view('admin.fjob.index', compact('fjobs'));
    }

    public function create()
    {
        $fcategories = $this->fcategoryRepo->list('name', 'asc');
        $salarytypes = $this->salarytypeRepo->list('name', 'asc');
        $jobtypes = $this->jobtypeRepo->list();
        $users = $this->userRepo->list();
        $levels = $this->levelRepo->list();
        $positiontypes = $this->positiontypeRepo->list();

        $this->setPageTitle('Freelancer Jobs', 'Create Freelancer Job');
        return view('admin.fjob.create', compact('fcategories','salarytypes','users','levels','jobtypes','positiontypes'));
    }

    public function store(Request $request)
    {
        $job = $this->fjobRepo->store($request);

        if (!$job) {
            return $this->responseRedirectBack('Error occurred while creating job.', 'error', true, true);
        }
        return $this->responseRedirect('fjob.index', 'job added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $fjob = $this->fjobRepo->findById($id);
        $fcategories = $this->fcategoryRepo->list('name', 'asc');
        $salarytypes = $this->salarytypeRepo->list('name', 'asc');
        $jobtypes = $this->jobtypeRepo->list();
        $users = $this->userRepo->list();
        $levels = $this->levelRepo->list();
        $positiontypes = $this->positiontypeRepo->list();

        $this->setPageTitle('fjobs', 'Edit fjob');
        return view('admin.fjob.edit', compact('fcategories','salarytypes','users','levels','jobtypes','positiontypes','fjob'));
    }

    public function update(Request $request, $id)
    {
        $fjob = $this->fjobRepo->edit($request, $id);

        if (!$fjob) {
            return $this->responseRedirectBack('Error occurred while updating job.', 'error', true, true);
        }
        return $this->responseRedirect('fjob.index', 'job updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->fjobRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('fjob.index', 'Record deleted successfully' ,'success',false, false);
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('fjobs');
        foreach($allArticles as $_article){
            $article = $this->fjobRepo->findById($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
