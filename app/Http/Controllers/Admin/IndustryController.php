<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\IndustryContract;
use App\Http\Controllers\BaseController;

class IndustryController extends BaseController
{
    private $page="admin.industry.";

    protected $industryRepo;

    public function __construct(
        IndustryContract $industryRepo
    )
    {
        $this->industryRepo = $industryRepo;
    }

    public function index()
    {
        $datas = $this->industryRepo->list();
        $this->setPageTitle('industrys', 'List of industrys');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('industry', 'Create industry');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->industryRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('industry.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->industryRepo->findById($id);
        $this->setPageTitle('industry', 'Edit industry');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->industryRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('industry.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->industryRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('industry.index', 'Record deleted successfully' ,'success',false, false);
    }
}
