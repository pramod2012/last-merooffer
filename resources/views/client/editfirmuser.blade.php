@extends('site.partials.main_index')
@section('title','Edit Employer Profile')
@section('content')
<body class="page-template page-template-template-submit-ads page-template-template-submit-ads-php page page-id-44 logged-in">
@include('site.partials.navbar')
<section class="user-pages section-gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4">
                @include('site.partials.sidebar')
            </div><!--col-lg-3 col-md-4-->

            <div class="col-lg-9 col-md-8 user-content-height">
                @include('partials.alert')
                                <div class="user-detail-section section-bg-white">
                    <div class="user-ads user-profile-settings">
                        @if($firmuser ==!null)                    
                        <form role="form" method="POST" action="{{route('firmuser.update1',$firmuser->id)}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{method_field('PATCH')}}
                            <!-- upload avatar -->
                            <div class="media">
                                <div class="media-left uploadImage">
                                    <img class="media-object img-circle author-avatar" src="{{asset('storage/'. $firmuser->logo)}}" style="height: 150px; width: 150px;" alt="{{Auth::user()->name}}">
                                </div>
                                <div class="media-body">
                                    <h5 class="media-heading text-uppercase">
                                        Update your Company Logo                                </h5>
                                    <p>Update your logo manually. </p>
                                    <div class="choose-image">
                                        <input type="file" id="file-1" name="logo" class="inputfile inputfile-1 author-UP" data-multiple-caption="{count} files selected" multiple />
                                        <label for="file-1" class="upload-author-image"><i class="fas fa-camera"></i>
                                            <span>Upload Logo</span>
                                        </label>
                                    </div>                                  
                                </div>
                            </div><!-- /.upload avatar -->
                            <!-- user basic information -->
                            <h4 class="user-detail-section-heading text-uppercase">
                                Employer Profile                           </h4>
                            <div class="form-inline row form-inline-margin">
                                
                                <div class="form-group col-sm-5">
                                    <label for="first-name">Banner *</label>
                                    <div class="inner-addon">
                                        <input type="file" id="first-name" name="img1" class="form-control form-control-sm" placeholder="Name" value="{{$firmuser->img1}}">
                                        @if($firmuser->img1 == !null)
                                        <img src="{{asset('storage/'. $firmuser->img1)}}" width="100%">@endif
                                        <strong>{{ $errors->first('img1') }}</strong>
                                    </div>
                                </div><!--Firstname-->
                                <div class="form-group col-sm-5">
                                    <label for="last-name">Banner 2</label>
                                    <div class="inner-addon">
                                        <input type="file" id="url" name="img2" class="form-control form-control-sm" value="{{$firmuser->img2}}">
                                        @if($firmuser->img2 == !null)
                                        <img src="{{url('storage/'. $firmuser->img2)}}" width="100%">@endif
                                        <strong>{{ $errors->first('img2') }}</strong>
                                    </div>
                                </div><!--Last name-->
                                <div class="form-group col-sm-5">
                                    <label for="first-name">Organization Name *</label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="name" class="form-control form-control-sm" placeholder="Name" value="{{$firmuser->name}}">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                </div><!--Firstname-->
                                <div class="form-group col-sm-5">
                                    <label for="last-name">Organization URL</label>
                                    <div class="inner-addon">
                                        <input type="text" id="url" name="url" class="form-control form-control-sm" placeholder="enter your website link" value="{{$firmuser->url}}">
                                    </div>
                                </div><!--Last name-->
                                <div class="form-group col-sm-12">
                                    <label for="bio">Org. Description</label>
                                    <div class="inner-addon">
                                        <textarea name="desc" id="bio" placeholder="enter your org. info.">{{$firmuser->desc}}</textarea>
                                    </div>
                                </div><!--biography-->
                                
                                
                            </div>
                            <!-- user basic information -->
                            <!-- Contact Details -->
                            <h4 class="user-detail-section-heading text-uppercase">
                                Contact Details                         </h4>
                            <div class="form-inline row form-inline-margin">
                                <div class="form-group col-sm-5">
                                    <label for="phone">Organization Phone Number</label>
                                    <div class="inner-addon">
                                        <input type="tel" name="phone" class="form-control form-control-sm" placeholder="phone"  value="{{$firmuser->phone}}">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </div>
                                </div><!--Phone Number-->
                                <div class="form-group col-sm-5">
                                    <label for="phone">Organization Email</label>
                                    <div class="inner-addon">
                                        <input type="email" name="email" class="form-control form-control-sm" placeholder="email"  value="{{$firmuser->email}}">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                </div><!--Phone Number-->
                                <div class="form-group col-sm-5">
                                    <div class="checkbox">
                                        <input type="hidden" name="show_phone" value="0">
                                        <input type="checkbox" name="show_phone" id="gdpr1" value="1" {{ $firmuser->show_phone ==1?'checked':'' }}>
                                        <label for="gdpr1">Show Phone Number</label>
                                    </div>
                                </div>
                                <div class="form-group col-sm-5">
                                    <div class="checkbox">
                                        <input type="hidden" name="show_email" value="0">
                                        <input type="checkbox" name="show_email" id="gdpr2" value="1" {{ $firmuser->show_email ==1?'checked':'' }}>
                                        <label for="gdpr2">Show Email</label>
                                    </div>
                                </div>
                            </div>
                            <!-- Contact Details -->
                            <!-- Social Profile Links -->
                            <h4 class="user-detail-section-heading text-uppercase">
                                Social Profile Links                            </h4>                           
                            <div class="form-inline row form-inline-margin">
                                <div class="form-group col-sm-5">
                                    <label for="facebook">Facebook</label>
                                    <div class="inner-addon">
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-addon-width-sm"><i class="fab fa-facebook-f"></i></div>
                                            <input type="url" id="facebook" name="fb" class="form-control form-control-sm" placeholder="Enter your facebook url" value="{{$firmuser->fb}}">
                                        </div>
                                    </div>
                                </div><!--facebook-->
                                <div class="form-group col-sm-5">
                                    <label for="twitter">Twitter</label>
                                    <div class="inner-addon">
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-addon-width-sm"><i class="fab fa-twitter"></i></div>
                                            <input type="url" id="twitter" name="twitter" class="form-control form-control-sm" placeholder="Enter your twitter url" value="{{$firmuser->twitter}}">
                                        </div>
                                    </div>
                                </div><!--twitter-->
                                <div class="form-group col-sm-5">
                                    <label for="instagram">instagram</label>
                                    <div class="inner-addon">
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-addon-width-sm"><i class="fab fa-instagram"></i></div>
                                            <input type="url" id="instagram" name="insta" class="form-control form-control-sm" placeholder="Enter your instagram url" value="{{$firmuser->insta}}">
                                        </div>
                                    </div>
                                </div><!--instagram-->
                                
                                <div class="form-group col-sm-5">
                                    <label for="linkedin">Linkedin</label>
                                    <div class="inner-addon">
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-addon-width-sm"><i class="fab fa-linkedin"></i></div>
                                            <input type="url" id="linkedin" name="linkedin" class="form-control form-control-sm" placeholder="Enter your Linkedin url" value="{{$firmuser->linkedin}}">
                                        </div>
                                    </div>
                                </div><!--linkedin-->
                                <div class="form-group col-sm-5">
                                    <label for="youtube">Youtube</label>
                                    <div class="inner-addon">
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-addon-width-sm"><i class="fab fa-youtube"></i></div>
                                            <input type="url" id="youtube" name="youtube" class="form-control form-control-sm" placeholder="Enter your YouTube url" value="{{$firmuser->youtube}}">
                                        </div>
                                    </div>
                                </div><!--youtube-->
                            </div><!--form-inline-->
                            <!-- Socail Profile Links -->
                            <button type="submit" value="submit" class="btn btn-primary sharp btn-sm btn-style-one">Update Now</button>
                            <!-- Update your Password -->
                        </form>
                        @else                    
                        <form role="form" method="POST" action="{{route('firmuser.store1')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <!-- upload avatar -->
                            <div class="media">
                                <div class="media-left uploadImage">
                                    <img class="media-object img-circle author-avatar" src="https://secure.gravatar.com/avatar/f5246b0de46bc4a3203500a12cc67295?s=130&d=mm&r=g" style="height: 150px; width: 150px;" alt="">
                                </div>
                                <div class="media-body">
                                    <h5 class="media-heading text-uppercase">
                                        Update your COmpany Logo  <span>*</span>                              </h5>
                                    <p>Update your logo manually.</p>
                                    <div class="choose-image">
                                        <input type="file" id="file-1" name="logo" class="inputfile inputfile-1 author-UP" data-multiple-caption="{count} files selected" multiple />
                                        <label for="file-1" class="upload-author-image"><i class="fas fa-camera"></i>
                                            <span>Upload Logo</span>
                                        </label>
                                    </div>                                  
                                </div>
                            </div><!-- /.upload avatar -->
                            <!-- user basic information -->
                            <h4 class="user-detail-section-heading text-uppercase">
                                Employer Profile                         </h4>
                            <div class="form-inline row form-inline-margin">
                                
                                <div class="form-group col-sm-5">
                                    <label for="first-name">Banner</label>
                                    <div class="inner-addon">
                                        <input type="file" id="first-name" name="img1" class="form-control form-control-sm" placeholder="Name" value="{{old('img1')}}">
                                        <strong>{{ $errors->first('img1') }}</strong>
                                    </div>
                                </div><!--Firstname-->
                                <div class="form-group col-sm-5">
                                    <label for="last-name">Banner 2</label>
                                    <div class="inner-addon">
                                        <input type="file" id="url" name="img2" class="form-control form-control-sm" placeholder="enter your website link" value="">
                                        <strong>{{ $errors->first('img2') }}</strong>
                                    </div>
                                </div><!--Last name-->
                                <div class="form-group col-sm-5">
                                    <label for="first-name">Company Name <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="name" class="form-control form-control-sm" placeholder="Name" value="{{old('name')}}">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                </div><!--Firstname-->
                                <div class="form-group col-sm-5">
                                    <label for="last-name">Company URL</label>
                                    <div class="inner-addon">
                                        <input type="text" id="url" name="url" class="form-control form-control-sm" placeholder="enter your website link" value="{{old('url')}}">
                                    </div>
                                </div><!--Last name-->
                                <div class="form-group col-sm-12">
                                    <label for="bio">Org. Description</label>
                                    <div class="inner-addon">
                                        <textarea name="desc" id="bio" placeholder="enter your org. info.">{{old('desc')}}</textarea>
                                    </div>
                                </div><!--biography-->
                                
                                
                            </div>
                            <!-- user basic information -->
                            <!-- Contact Details -->
                            <h4 class="user-detail-section-heading text-uppercase">
                                Contact Details                         </h4>
                            <div class="form-inline row form-inline-margin">
                                <div class="form-group col-sm-5">
                                    <label for="phone">Organization Phone Number <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="tel" name="phone" class="form-control form-control-sm" placeholder="phone" value="{{old('phone')}}">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </div>
                                </div><!--Phone Number-->
                                <div class="form-group col-sm-5">
                                    <label for="phone">Organization Email</label>
                                    <div class="inner-addon">
                                        <input type="email" name="email" class="form-control form-control-sm" placeholder="email" value="{{old('email')}}">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                </div><!--Phone Number-->
                                <div class="form-group col-sm-5">
                                    <div class="checkbox">
                                        <input type="hidden" name="show_phone" value="0">
                                        <input type="checkbox" name="show_phone" id="gdpr1" value="1">
                                        <label for="gdpr1">Show Phone Number</label>
                                    </div>
                                </div>
                                <div class="form-group col-sm-5">
                                    <div class="checkbox">
                                        <input type="hidden" name="show_email" value="0">
                                        <input type="checkbox" name="show_email" id="gdpr2" value="1">
                                        <label for="gdpr2">Show Email</label>
                                    </div>
                                </div>
                            </div>
                            <!-- Contact Details -->
                            <!-- Social Profile Links -->
                            <h4 class="user-detail-section-heading text-uppercase">
                                Social Profile Links                            </h4>                           
                            <div class="form-inline row form-inline-margin">
                                <div class="form-group col-sm-5">
                                    <label for="facebook">Facebook</label>
                                    <div class="inner-addon">
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-addon-width-sm"><i class="fab fa-facebook-f"></i></div>
                                            <input type="url" id="facebook" name="fb" class="form-control form-control-sm" placeholder="Enter your facebook url" value="{{old('fb')}}">
                                        </div>
                                    </div>
                                </div><!--facebook-->
                                <div class="form-group col-sm-5">
                                    <label for="twitter">Twitter</label>
                                    <div class="inner-addon">
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-addon-width-sm"><i class="fab fa-twitter"></i></div>
                                            <input type="url" id="twitter" name="twitter" class="form-control form-control-sm" placeholder="Enter your twitter url" value="{{old('twitter')}}">
                                        </div>
                                    </div>
                                </div><!--twitter-->
                                <div class="form-group col-sm-5">
                                    <label for="instagram">instagram</label>
                                    <div class="inner-addon">
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-addon-width-sm"><i class="fab fa-instagram"></i></div>
                                            <input type="url" id="instagram" name="insta" class="form-control form-control-sm" placeholder="Enter your instagram url" value="{{old('insta')}}">
                                        </div>
                                    </div>
                                </div><!--instagram-->
                                
                                <div class="form-group col-sm-5">
                                    <label for="linkedin">Linkedin</label>
                                    <div class="inner-addon">
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-addon-width-sm"><i class="fab fa-linkedin"></i></div>
                                            <input type="url" id="linkedin" name="linkedin" class="form-control form-control-sm" placeholder="Enter your Linkedin url" value="">
                                        </div>
                                    </div>
                                </div><!--linkedin-->
                                
                                <div class="form-group col-sm-5">
                                    <label for="youtube">Youtube</label>
                                    <div class="inner-addon">
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-addon-width-sm"><i class="fab fa-youtube"></i></div>
                                            <input type="url" id="youtube" name="youtube" class="form-control form-control-sm" placeholder="Enter your YouTube url" value="{{old('youtube')}}">
                                        </div>
                                    </div>
                                </div><!--youtube-->
                            </div><!--form-inline-->
                            <!-- Socail Profile Links -->
                            <button type="submit" name="op" value="submit" class="btn btn-primary sharp btn-sm btn-style-one">Update</button>
                            <!-- Update your Password -->
                        </form>
                        @endif
                    </div><!--user-ads user-profile-settings-->
                </div><!--user-detail-section-->
            </div><!--col-lg-9-->
        </div><!--row-->
    </div><!--container-->  
</section><!--user-pages section-gray-bg-->

@include('site.partials.footer')
@endsection


