<!DOCTYPE html>
<!--
  This is a starter template page. Use this page to start your new project from
  scratch. This page gets rid of all links and provides the needed markup only.
  -->
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SM | System Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="{{ asset("_all-skins.min.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/AdminLTE/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link href="{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/AdminLTE/plugins/datepicker/datepicker3.css")}}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/bower_components/AdminLTE/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
      page. However, you can choose any other skin. Make sure you
      apply the skin class to the body tag so the changes take effect.
      -->
    <link href="{{ asset("/bower_components/AdminLTE/dist/css/skins/_all-skins.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/bower_components/css/app-template.css') }}" rel="stylesheet">
    <link href="{{ asset('/bower_components/AdminLTE/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css">
    @yield('styles')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
  <body class="hold-transition skin-blue sidebar-collapse sidebar-mini">
    <div class="wrapper">
    <!-- Main Header -->
    @include('admin.layouts.header')
    <!-- Sidebar -->
    @include('admin.layouts.sidebar')
    @yield('content')
    <!-- /.content-wrapper -->
    <!-- Footer -->
    @include('admin.layouts.footer')
    <!-- ./wrapper -->
    <!-- REQUIRED JS SCRIPTS -->
    <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
    <script src="{{ asset("bower_components/AdminLTE/plugins/select2/select2.full.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("/ckeditor/ckeditor.js") }}"></script>
    <script src="{{ asset("/ckfinder/ckfinder.js") }}"></script>
    <script>
          ClassicEditor.create(document.getElementById('body'));
         var editor = CKEDITOR.replace('body');
         CKEDITOR.config.extraPlugins = 'colorbutton';
         CKFinder.setupCKEditor( editor );
    </script>

    <script  src="{{ asset ("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js") }}" type="text/javascript" ></script>
    <script  src="{{ asset ("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js") }}" type="text/javascript" ></script>
    <script  src="{{ asset ("/bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js") }}" type="text/javascript" ></script>
    <script  src="{{ asset ("/bower_components/AdminLTE/plugins/fastclick/fastclick.js") }}" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script  src="{{ asset ("/bower_components/AdminLTE/plugins/input-mask/jquery.inputmask.js") }}" type="text/javascript" ></script>
    <script  src="{{ asset ("/bower_components/AdminLTE/plugins/input-mask/jquery.inputmask.date.extensions.js") }}" type="text/javascript" ></script>
    <script  src="{{ asset ("/bower_components/AdminLTE/plugins/input-mask/jquery.inputmask.extensions.js") }}" type="text/javascript" ></script>
    <script src="{{ asset("/bower_components/AdminLTE/jqueryui/jquery-ui.min.js") }}" type="text/javascript"></script>
    <script  src="{{ asset ("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js") }}" type="text/javascript" ></script>
    <script  src="{{ asset ("/bower_components/AdminLTE/plugins/datepicker/bootstrap-datepicker.js") }}" type="text/javascript" ></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
    @stack('scripts')
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->

      <script>
  var apiBase = "{{ url('api/') }}"

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
      }
  });

  $(document).ready(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    $('[name="txtsubcategory"]').on('change', function(e){

      var selectedSubCategory = $(this).val();
      var apiUrl = apiBase + '/' + selectedSubCategory + '/category';
      console.log('Calling Ajax')
      $.ajax({
        'method': 'GET',
        'url': apiUrl,
        'success': function(res){
          $('[name="txtcategory"]').val(res.id)
        }
      })

    });

    $('#example60 tbody').sortable({
      beforeStop: function(e, ui){
        var orders = []
        $(this).find('tr').each(function(obj, i){
          var id = $(i).attr('data-product-id')
          if( id ){
            orders.push({
              id: parseInt(id),
              order: obj + 1
            })
          }
        });
        
        if( orders.length > 0){
          $.ajax({
            'method': 'POST',
            'dataType': 'application/json',
            'url': apiBase + '/product/save_orders',
            'data': {'products': orders},
            'success': function(res){
              console.log(res)
            }
          })
        }
      } 
    })
  });
</script>

 <script>
  var apiBase = "{{ url('api/') }}"

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
      }
  });

  $(document).ready(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    $('[name="txtsubcategory"]').on('change', function(e){

      var selectedSubCategory = $(this).val();
      var apiUrl = apiBase + '/' + selectedSubCategory + '/category';
      console.log('Calling Ajax')
      $.ajax({
        'method': 'GET',
        'url': apiUrl,
        'success': function(res){
          $('[name="txtcategory"]').val(res.id)
        }
      })

    });

    $('#example5 tbody').sortable({
      beforeStop: function(e, ui){
        var orders = []
        $(this).find('tr').each(function(obj, i){
          var id = $(i).attr('data-partner-id')
          if( id ){
            orders.push({
              id: parseInt(id),
              order: obj + 1
            })
          }
        });
        
        if( orders.length > 0){
          $.ajax({
            'method': 'POST',
            'dataType': 'application/json',
            'url': apiBase + '/partner/save_orders',
            'data': {'partners': orders},
            'success': function(res){
              console.log(res)
            }
          })
        }
      } 
    })
  });
</script>

<script>
  var apiBase = "{{ url('api/') }}"

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
      }
  });

  $(document).ready(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    $('[name="txtsubcategory"]').on('change', function(e){

      var selectedSubCategory = $(this).val();
      var apiUrl = apiBase + '/' + selectedSubCategory + '/category';
      console.log('Calling Ajax')
      $.ajax({
        'method': 'GET',
        'url': apiUrl,
        'success': function(res){
          $('[name="txtcategory"]').val(res.id)
        }
      })

    });

    $('#example30 tbody').sortable({
      beforeStop: function(e, ui){
        var orders = []
        $(this).find('tr').each(function(obj, i){
          var id = $(i).attr('data-post-id')
          if( id ){
            orders.push({
              id: parseInt(id),
              order: obj + 1
            })
          }
        });
        
        if( orders.length > 0){
          $.ajax({
            'method': 'POST',
            'dataType': 'application/json',
            'url': apiBase + '/post/save_orders',
            'data': {'posts': orders},
            'success': function(res){
              console.log(res)
            }
          })
        }
      } 
    })
  });
</script>

<script>
  var apiBase = "{{ url('api/') }}"

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
      }
  });

  $(document).ready(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    $('[name="txtsubcategory"]').on('change', function(e){

      var selectedSubCategory = $(this).val();
      var apiUrl = apiBase + '/' + selectedSubCategory + '/category';
      console.log('Calling Ajax')
      $.ajax({
        'method': 'GET',
        'url': apiUrl,
        'success': function(res){
          $('[name="txtcategory"]').val(res.id)
        }
      })

    });

    $('#example9 tbody').sortable({
      beforeStop: function(e, ui){
        var orders = []
        $(this).find('tr').each(function(obj, i){
          var id = $(i).attr('data-category-id')
          if( id ){
            orders.push({
              id: parseInt(id),
              order: obj + 1
            })
          }
        });
        
        if( orders.length > 0){
          $.ajax({
            'method': 'POST',
            'dataType': 'application/json',
            'url': apiBase + '/category/save_orders',
            'data': {'datas': orders},
            'success': function(res){
              console.log(res)
            }
          })
        }
      } 
    })
  });
</script>

<script>
  var apiBase = "{{ url('api/') }}"

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
      }
  });

  $(document).ready(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    $('[name="txtsubcategory"]').on('change', function(e){

      var selectedSubCategory = $(this).val();
      var apiUrl = apiBase + '/' + selectedSubCategory + '/category';
      console.log('Calling Ajax')
      $.ajax({
        'method': 'GET',
        'url': apiUrl,
        'success': function(res){
          $('[name="txtcategory"]').val(res.id)
        }
      })

    });

    $('#example3 tbody').sortable({
      beforeStop: function(e, ui){
        var orders = []
        $(this).find('tr').each(function(obj, i){
          var id = $(i).attr('data-subcategory-id')
          if( id ){
            orders.push({
              id: parseInt(id),
              order: obj + 1
            })
          }
        });
        
        if( orders.length > 0){
          $.ajax({
            'method': 'POST',
            'dataType': 'application/json',
            'url': apiBase + '/subcategory/save_orders',
            'data': {'datas': orders},
            'success': function(res){
              console.log(res)
            }
          })
        }
      } 
    })
  });
</script>
<script>
  var apiBase = "{{ url('api/') }}"

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
      }
  });

  $(document).ready(function () {
    //Initialize Select2 Elements

    $('#example99 tbody').sortable({
      beforeStop: function(e, ui){
        var orders = []
        $(this).find('tr').each(function(obj, i){
          var id = $(i).attr('data-jobcategory-id')
          if( id ){
            orders.push({
              id: parseInt(id),
              order: obj + 1
            })
          }
        });
        
        if( orders.length > 0){
          $.ajax({
            'method': 'POST',
            'dataType': 'application/json',
            'url': apiBase + '/jobcategory/save_orders',
            'data': {'datas': orders},
            'success': function(res){
              console.log(res)
            }
          })
        }
      } 
    })
  });
</script>

<script>
  var apiBase = "{{ url('api/') }}"

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
      }
  });

  $(document).ready(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    $('[name="txtsubcategory"]').on('change', function(e){

      var selectedSubCategory = $(this).val();
      var apiUrl = apiBase + '/' + selectedSubCategory + '/category';
      console.log('Calling Ajax')
      $.ajax({
        'method': 'GET',
        'url': apiUrl,
        'success': function(res){
          $('[name="txtcategory"]').val(res.id)
        }
      })

    });

    $('#examplejobs tbody').sortable({
      beforeStop: function(e, ui){
        var orders = []
        $(this).find('tr').each(function(obj, i){
          var id = $(i).attr('data-jobs-id')
          if( id ){
            orders.push({
              id: parseInt(id),
              order: obj + 1
            })
          }
        });
        
        if( orders.length > 0){
          $.ajax({
            'method': 'POST',
            'dataType': 'application/json',
            'url': apiBase + '/job/save_orders',
            'data': {'jobs': orders},
            'success': function(res){
              console.log(res)
            }
          })
        }
      } 
    })
  });
</script>

<script>
  var apiBase = "{{ url('api/') }}"

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
      }
  });

  $(document).ready(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    $('[name="txtsubcategory"]').on('change', function(e){

      var selectedSubCategory = $(this).val();
      var apiUrl = apiBase + '/' + selectedSubCategory + '/category';
      console.log('Calling Ajax')
      $.ajax({
        'method': 'GET',
        'url': apiUrl,
        'success': function(res){
          $('[name="txtcategory"]').val(res.id)
        }
      })

    });

    $('#exampleAttribute tbody').sortable({
      beforeStop: function(e, ui){
        var orders = []
        $(this).find('tr').each(function(obj, i){
          var id = $(i).attr('data-attribute-id')
          if( id ){
            orders.push({
              id: parseInt(id),
              order: obj + 1
            })
          }
        });
        
        if( orders.length > 0){
          $.ajax({
            'method': 'POST',
            'dataType': 'application/json',
            'url': apiBase + '/attribute/save_orders',
            'data': {'datas': orders},
            'success': function(res){
              console.log(res)
            }
          })
        }
      } 
    })
  });
</script>


  </body>
</html>