<?php

namespace App\Models\Freelancers;

use Illuminate\Database\Eloquent\Model;
use App\Skill;

class Fjob extends Model
{
   public static function boot()
  {
        parent::boot();
        self::creating(function ($model) {
            $model->order = $model->max('order') + 1;
        });
  }

   public function fcategory() {
        return $this->belongsTo('App\Models\Freelancers\Fcategory','fcategory_id');
   }
   public function user() {
        return $this->belongsTo('App\Models\User');
   }
   
   public function jobtype() {
      return $this->belongsTo('App\Jobtype');
   }

   public function duration() {
      return $this->belongsTo('App\Models\Freelancers\Duration');
   }

   public function fapplicants() {
        return $this->hasMany('App\Models\Freelancers\Fapplicant');
    }

    public function skillss(){
        return $this->belongsToMany(Skill::class,'fjob_skill')->withTimestamps();
    }
}
