<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\ExperienceContract;
use App\Http\Controllers\BaseController;

class ExperienceController extends BaseController
{
    private $page="admin.experience.";

    protected $experienceRepo;

    public function __construct(
        ExperienceContract $experienceRepo
    )
    {
        $this->experienceRepo = $experienceRepo;
    }

    public function index()
    {
        $datas = $this->experienceRepo->list();
        $this->setPageTitle('experiences', 'List of experiences');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('experience', 'Create experience');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->experienceRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('experience.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->experienceRepo->findById($id);
        $this->setPageTitle('experience', 'Edit experience');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->experienceRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('experience.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->experienceRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('experience.index', 'Record deleted successfully' ,'success',false, false);
    }
}
