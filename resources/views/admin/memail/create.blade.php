@extends('admin.memail.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('memail.store') }}" enctype="multipart/form-data">
                       {{csrf_field()}}

                        <div class="form-group{{ $errors->has('from') ? ' has-error' : '' }}">
                            <label for="from" class="col-md-2 control-label">from</label>

                            <div class="col-md-10">
                                <input id="from" type="text" class="form-control" name="from" value="{{ old('from') }}">

                                @if ($errors->has('from'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('from') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">
                            <label for="to" class="col-md-2 control-label">TO</label>

                            <div class="col-md-10">
                                <input id="from" type="text" class="form-control" name="to" value="{{ old('to') }}">

                                @if ($errors->has('to'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('to') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="greet" class="col-md-2 control-label">Greet</label>

                            <div class="col-md-10">
                                <input id="greet" type="text" class="form-control" name="greet" value="{{ old('greet') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="subject" class="col-md-2 control-label">Subject</label>

                            <div class="col-md-10">
                                <input id="subject" type="text" class="form-control" name="subject" value="{{ old('subject') }}">
                                
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('subject') }}
                                        </strong>
                                    </span>
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="body" class="col-md-2 control-label">Body</label>

                            <div class="col-md-10">
                                <textarea class="ckeditor" id="body" type="text" class="form-control" name="body">{{ old('body') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="end_greet" class="col-md-2 control-label">End Greet</label>

                            <div class="col-md-10">
                                <textarea id="end_greet" type="text" class="form-control" name="end_greet">{{ old('end_greet') }}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
