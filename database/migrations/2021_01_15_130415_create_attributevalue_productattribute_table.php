<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributevalueProductattributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributevalue_productattribute', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('attributevalue_id');
            $table->foreign('attributevalue_id')->references('id')->on('attributevalues')->onDelete('cascade');
            $table->unsignedBigInteger('productattribute_id');
            $table->foreign('productattribute_id')->references('id')->on('productattributes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributevalue_productattribute');
    }
}
