<?php

namespace App\Traits;

use Carbon\Carbon;


trait Searchable
{
    public function scopeSearchable($q)
    {
        $request = app('request');
        if ($request->has('query')) {
            $this->buildSearchQuery($q, $request->get('query'));
        }
        return $q;
    }
    protected function buildSearchQuery($q, $val)
    {
        $q->where(function ($qq) use ($val) {
            foreach ($this->searchable as $col) {
                $qq->orWhere($col, 'LIKE', '%' . $val . '%');
            }
        });
    }
    public function scopeSortSearchableProducts($q)
    {
        $request = app('request');
        if ($request->has('sort')) {
            switch ($request->get('sort')) {
                case 'lowest_price':
                    $q->orderBy('price', 'asc');
                    break;
                case 'highest_price':
                    $q->orderBy('price', 'desc');
                    break;
                case 'oldest_items':
                    $q->orderBy('created_at', 'asc');
                    break;
                case 'popular_items':
                    $q->orderBy('views', 'desc');
                    break;
                case 'random_items':
                    $q->inRandomOrder();
                    break;
                default:
                    $q->orderBy('created_at', 'desc');
                    break;
            }
        }
    }
    public function scopeSortSearchableJobs($q)
    {
        $request = app('request');
        if ($request->has('sort')) {
            switch ($request->get('sort')) {
                case 'lowest_salary':
                    $q->orderBy('salary', 'asc');
                    break;
                case 'highest_salary':
                    $q->orderBy('salary', 'desc');
                    break;
                case 'oldest_jobs':
                    $q->orderBy('created_at', 'asc');
                    break;
                default:
                    $q->orderBy('created_at', 'desc');
                    break;
            }
        }
    }
    public function scopeLocationFilter($q)
    {
        $request = app('request');
        $lat = $request->lat;
        $lng = $request->lng;
        $search_radius = ($request->search_radius ?? config('app.search_radius')) * 1000;
        if ($lat && $lng && $search_radius) {
            $q->whereRaw('st_distance_sphere(point( latitude  ,longitude),point("' . $lat . '","' . $lng . '"))<' . $search_radius);
        }
        logger($q->toSql());
    }
}
