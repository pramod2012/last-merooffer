<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use App\Models\Cart;
use App\Models\Comment;
use App\Models\Bid;
use App\Address;
use App\Order;
use App\Models\Product;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','mobile','activation_key','ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'activation_key',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return string
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        if (static::whereSlug($slug = str_slug($value))->exists())
        {
            $slug = $this->incrementSlug($slug);
        }
        $this->attributes['slug'] = $slug;
    }

    public function incrementSlug($slug)
    {
        $original = $slug;
        $count = 2;
        while (static::whereSlug($slug)->exists()) 
        {
           $slug = $count++ . "-{$original}";
        }
        return $slug;
    }

    public function addresses()
    {
        return $this->hasMany(Address::class)->whereStatus(true);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function identities() {
        return $this->hasMany('App\SocialIdentity');
    }

    public function cart(){
       return $this->hasMany(Cart::class);  
    }

    public function products(){
       return $this->hasMany(Product::class);  
    }

    public function assuredProducts(){
        return $this->hasMany(Product::class)->where('status',1)->where('assured','<>',1);
    }

    public function featuredProducts(){
        return $this->hasMany(Product::class)->where('status',1)->where('featured',1);
    }

    public function latestProducts(){
        return $this->hasMany(Product::class)->where('status',1)->orderBy('created_at','desc');
    }

    public function popularProducts(){
        return $this->hasMany(Product::class)->where('status',1)->orderBy('views', 'desc');
    }

    public function randomProducts(){
        return $this->hasMany(Product::class)->where('status',1)->inRandomOrder();
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function bids(){
        return $this->hasMany(Bid::class);
    }

    function jobs() {
        return $this->hasMany('App\Job');
    }

    function role() {
        return $this->belongsTo('App\Role');
    }
    
    function profile() {
        return $this->hasOne('App\Profile', 'id', 'user_id');
    }

    function firmuser() {
        return $this->hasOne('App\Firmuser', 'user_id', 'id');
    }

    function skills() {
        return $this->belongsToMany('App\Skill')->withTimeStamps();
    }

    function educations() {
        return $this->hasMany('App\Education');
    }

    function works() {
        return $this->hasMany('App\Work');
    }

    function applicant() {
        return $this->hasOne('App\Applicant', 'id', 'user_id');
    }

    function following() {
        return $this->hasMany('App\Models\Follower', 'followed_by');
    }

    public function isAddedToFollowedList($user_id){
        if (Auth::check()){
        $user = Auth::user()->id;
        $count=app('db')->table('followers')->where('is_company',0)->where('leader_id', $user_id)->where('followed_by',$user)->count();
        return $count?true:false;
        }   
    }

    public function isAddedToFollowedCompany($user_id){
        if (Auth::check()){
        $user = Auth::user()->id;
        $count=app('db')->table('followers')->where('is_company',1)->where('leader_id', $user_id)->where('followed_by',$user)->count();
        return $count?true:false;
        }   
    }
}
