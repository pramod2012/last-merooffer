<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Attribute;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'user_id' => 'required|numeric',
            'city_id' => 'required|numeric',
            'adtype_id' => 'required|numeric',
            'day_id' => 'required|numeric',
            'pricetype_id' => 'required|numeric',
            'condition_id' => 'nullable|numeric',
            'name' => 'required|min:5|max:255',
            'price' => 'required|numeric',
            'rgr_price' => 'nullable|numeric',
            'bid' => 'required',
            'desc' => 'required|min:43',
            'address' => 'required',
            'latitude' => ['nullable', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'longitude' => ['nullable', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            'cell' => 'nullable|regex:/9[78][\d]{7}\d$/',
        ];

        $attribute = Attribute::where('type', '<>', 'checkbox')->whereHas('categories', function ($q) {
            $q->where('category_id', $this->category_id);
        })->get();
        foreach ($attribute as $key => $val) {
            if ($this->request->has("$val->code")) {
                $rules["$val->code"] = "$val->data_type";
            }
        }


        return $rules;
    }

    public function attributes()
    {
        return [
            'desc' => 'description',
            'rgr_price' => 'regular price',
            'day_id' => ' right plan option'
        ];
    }
}
