<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Amenity;

class AmenitySeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $real_estate_feature = [
             'Lawn',
             'Parking Space',
             'Garage',
             'Security Staff',
             'Drainage',
             'Balcony',
             'Fencing',
             'Backyard',
             'Frontyard',
             'Water Supply',
             'Modular Kitchen',
             'Solar Water',
             'Wifi',
             'Water Well',
             'Water Tank',
             'Garden',
             'TV Cable',
             'Air Condition',
             'Electricity Backup',
             'Store Room',
             'Lift',
             'Microwave',
             'CCTV',
             'Gym',
             'Jacuzzi',
             'Deck',
             'Swimming Pool',
             'Washing Machine',
             'Kids Playground',
             'Cafeteria'

         ];
         foreach ($real_estate_feature as $value){
                   Amenity::create([
                    'value' => $value,
                'attribute_id' => 25,]);
               }

        $car_amnities = [
            'LCD Touchscreen',
            'Power Window',
            'Electric Side Mirror (ORVM)',
            'Tubeless Tyres',
            'Air Conditioner - Automatic',
            'Air Bags',
            'Anti-lock Braking (ABS)',
            'Keyless Remote Entry',
            'Air Conditioner - Manual',
            'Power Steering',
            'Steering Mounted Controls',
            'Projected Headlight',
            'Alloy Wheels',
            'Rear AC Vent',
            'Central Lock',
            'Fog Lights',
            'Push Engine Start Button',
            
        ];

        foreach ($car_amnities as $value){
                   Amenity::create([
                    'value' => $value,
                'attribute_id' => 26,]);
               }

        $bike_amnities = [
            'Front Disc Brake',
            'Electric Start',
            'Projected Headlight',
            'Alloy Wheels',
            'Tubeless Tyres',
            'LED Tail Light',
            'Split Seat',
            'Rear Disc Brake',
            'Low Fuel Indicator',
            'Mono Suspension',
            'Tripmeter',
            'Anti-lock Braking (ABS)',
            'Digital Display Panel',
                    ];

        foreach ($bike_amnities as $value){
                   Amenity::create([
                    'value' => $value,
                'attribute_id' => 27,]);
               }

        $mobile_amnities = [
            'Sales bill',
            'Stamped warranty card',
            'IMEI matching box',
            'Original purchase bill (for used phone)',
                'Memory Card Slot',
                'WiFi',
                'Fingerprint Sensor',
                'Gorilla Glass Screen',
                'Water & Dust Proof (IP)',
                'Compass Sensor',
                'Heart Rate Sensor',
                'Front LED Flash',
                'Proximity Sensor',
                'GPS',
                'Dual Camera - Back',
                'Dual Camera - Front',
                'NFC',
                    ];

        foreach ($mobile_amnities as $value){
                   Amenity::create([
                    'value' => $value,
                'attribute_id' => 40,]);
               }

        $laptop_amnities = [
                'SSD Storage',
                'HDMI',
                'DVD-CD Combo',
                'Webcam',
                'Blu-ray Drive',
                'SD Card Slot',
                'WiFi',
                'Thunderbolt Port',
                'DVD-CD Rewritable',
                'USB Type-C Port',
                'Touch Screen',
                'Bluetooth',
                'Fingerprint Recognition',
                'Aluminium Metal Body',
                'Surround Speaker',
                'Backlit Keyboard',
                    ];

        foreach ($laptop_amnities as $value){
                   Amenity::create([
                    'value' => $value,
                'attribute_id' => 49,]);
               }
    }
}
