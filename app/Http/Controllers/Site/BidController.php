<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\BidContract;
use App\Http\Requests\StoreBidRequest;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;

class BidController extends Controller
{
    protected $bidRepo;

    public function __construct(BidContract $bidRepo)
    {
        $this->bidRepo = $bidRepo;
        $this->middleware('auth', ['except' => ['getStatsOfProduct']]);
    }

    public function storeBid(StoreBidRequest $request)
    {
        $bid = $this->bidRepo->store($request);
        return response()->json([
            'status' => 1,
            'message' => 'Thank for your Offer ! We will get back to you soon !',
            'data' => $bid
        ]);

        // return back()->with('bid', 'Thank for your Offer ! We will get back to you soon !');
    }

    public function getStatsOfProduct(Request $request)
    {
        $product_id = $request->product_id;
        $product = Product::with(['bids.user:id,name', 'pricelabel'])->find($product_id);
        if ($product) {
            $data = [
                'status' => 1,
                'message' => 'success',
                'data' => [
                    'bid_count' => $product->bids->count(),
                    'bid_high' => $product->bids()->max('bid'),
                    'bid_low' => $product->bids()->min('bid'),
                    'product' => $product,
                    'logged_in' => Auth::check() ? Auth::user() : null
                ]
            ];
            return response()->json($data);
        }
    }
}
