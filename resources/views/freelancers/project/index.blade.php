@extends('site.partials.main_index')
@section('title','List of Works')
@section('content')
<body class="page-template page-template-template-profile page-template-template-profile-php page page-id-32 logged-in">
@include('freelancers.partials.navbarwork')
    <!-- user pages -->
<section class="user-pages section-gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4">
                @include('site.partials.sidebar')
                         </div><!--col-lg-3-->
            <div class="col-lg-9 col-md-8 user-content-height">
                <div class="user-detail-section section-bg-white">
                    <!-- about me -->
                    <!-- my ads -->
                    <div class="user-ads user-my-ads">
                        @include('partials.alert')
                        <h4 class="user-detail-section-heading text-uppercase">
                            MY PROJECTS                     </h4>
                            @if(count($works) > 0)
                            <div class="row table-responsive">
                            <table class="table table-striped">
                              
                              <tbody>
                                @foreach($works as $work)
	                   		<tr>
						      <td>
						      	<a href="{{route('freelancers.show',$work->slug)}}" target="_blank">{{ $work->name }}</a>
						      		<h4><a href="{{url('fshortlist',$work->slug)}}"><span class="badge badge-success w-100">Shortlist</span></a></h4>
						      </td>
						      <td>
						      		<a href="{{route('project.edit',$work->id)}}" data-toggle="tooltip" data-placement="top" title="Edit">
						      			<p class="text-primary text-center">
						      				<i class="far fa-edit fa-2x"></i>
							      		</p>
							      	</a>
						      	
						      </td>
                              <td>
                                <form method="POST" action="{{ route('project.destroy',$work->id) }}" onsubmit = "return confirm('Are you sure?')">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="fas fa-trash h5 text-danger"></button>
                                </form>
                              </td>
						    </tr>
	                   	@endforeach
                              </tbody>
                            </table>
                            {{$works->links()}}
                             
                          </div>
                           @else
                        <p class="my-5 text-center">You haven't posted any works.</p>
                    @endif

                        
                    </div>
                    <!-- my ads -->
                    
                </div>
            </div>
        </div><!--row-->
    </div><!--container-->
</section>
<!-- user pages -->
@include('site.partials.footer')
@endsection