<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFapplicantFjobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fapplicant_fjobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('fapplicant_user_id')->unsigned();
            $table->foreign('fapplicant_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('fjob_id')->unsigned();
            $table->foreign('fjob_id')->references('id')->on('fjobs')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fapplicant_fjobs');
    }
}
