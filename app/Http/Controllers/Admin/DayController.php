<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\DayContract;
use App\Http\Controllers\BaseController;

class DayController extends BaseController
{
    private $page="admin.day.";

    protected $dayRepo;

    public function __construct(
        DayContract $dayRepo
    )
    {
        $this->dayRepo = $dayRepo;
    }

    public function index()
    {
        $list = $this->dayRepo->list('created_at', 'desc');
        $days = $this->dayRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('Days', 'List of Days');
        return view($this->page.'index',compact('days'));
    }

    public function create()
    {
        $this->setPageTitle('Days', 'Create day');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->dayRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('day.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $day = $this->dayRepo->findById($id);
        $this->setPageTitle('Days', 'Edit day');
        return view($this->page.'edit', compact('day'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->dayRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('day.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->dayRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('day.index', 'Record deleted successfully' ,'success',false, false);
    }
}
