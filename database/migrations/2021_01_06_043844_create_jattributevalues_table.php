<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJattributevaluesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jattributevalues', function (Blueprint $table) {
            $table->id();
            $table->string('value');
            $table->string('image')->nullable();
            $table->string('font')->nullable();
            $table->bigInteger('jattribute_id')->unsigned();
            $table->foreign('jattribute_id')->references('id')->on('jattributes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jattributevalues');
    }
}
