<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ProductImage;

class ProductImageSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prd = new ProductImage();
        $prd->full = 'products/ZjV0lAxgnX1jvs1xpSQyeYiMi.jpg';
        $prd->product_id = 1;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/FoUPwpYnrGsHuajwwlMVqylu0.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/CfCoObOrqVMhtjTyOij8mbrux.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/ASOimGp2jFSh1Q7c4xIECglK4.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/DjZKoyamfwM3Sfr1wHRhNs5ne.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/4hHVKJhhtsHokUwi8HalVPlzh.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/wIFZcpdEhri2AnGAbKmzok4wi.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/xcHwI0A5s6oY80qbFB0PihJPc.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/wZ482VsGyh6KKvpC4FjmGQBmU.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/Noe4yF3P0P0YXWnFTtr7YsAoj.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/U6e7jivPxcPt1GRVJx3fBPLy2.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/hbgNpXqNvIessSvrgjI1OZXLg.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/73OxzY6Mt768p09ACuecd6sQt.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/fHujBonMpjsGS56K2GZj9IcW6.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/34uoU9joMZDsWDoZthpEO7SNQ.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/8u0tlUN6G01xhgO9O0db9Zmqv.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/ExJ0t4eHtJlHui0OwuX09Aq2G.jpg';
        $prd->product_id = 2;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/WmLJTeKSN0iH9WVgz0FpqNhiL.jpg';
        $prd->product_id = 3;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/lKIdSDf1gCDSnO4OMfo8SL4r3.jpg';
        $prd->product_id = 4;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/I8X0EV2cVmifdMpZvCvfgZMos.jpeg';
        $prd->product_id = 4;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/mOQMyXUtOqQVztzWVbatZZQMo.jpg';
        $prd->product_id = 4;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/hrHX2ufik5TBw3ftABz8HfSHp.png';
        $prd->product_id = 4;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/QdsEojtfO5inCmPcrlQ8lSJcs.jpg';
        $prd->product_id = 4;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/m67qus3QI5mh618iHMD6SMrJX.jpeg';
        $prd->product_id = 4;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/T8dtAMUmqRwKX2WgV5Ju2yGFK.png';
        $prd->product_id = 4;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/fy6JZ1yoRsYk9AuZJxnevNr9W.png';
        $prd->product_id = 4;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/je0Od1si4EXv3nbwgCXPELcfK.png';
        $prd->product_id = 4;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/iJgNk2GgTn5EfrLZ41Nfb8DYf.jpg';
        $prd->product_id = 4;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/TcFUJSEtfyekD4CwoBZpjvYy8.jpg';
        $prd->product_id = 4;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/ddkNbpvpKbQIgDTloMkepEbgR.jpg';
        $prd->product_id = 5;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/XU2vllenxQnLi6PBWpu0vKcb2.jpg';
        $prd->product_id = 5;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/PZYXEGjlpIKTHlylieLiveoJC.jpg';
        $prd->product_id = 5;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/9T9blViVBmWLjHDYTH9cx2RIf.jpg';
        $prd->product_id = 5;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/w5VUjYLMFvco45bGjajgVmEbZ.jpg';
        $prd->product_id = 5;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/KSMWPXhavFc1JrSeJswH8s3p7.jpg';
        $prd->product_id = 5;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/ZY2HbSaNfMV91dhpub7q35R8g.jpeg';
        $prd->product_id = 7;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/TXQXY1KrcERI5R8EMXYSxo0VC.jpeg';
        $prd->product_id = 7;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/DysMfB5yEimGukwWKi7ysjYhB.jpg';
        $prd->product_id = 8;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/JEHT3Wb4OLfNDuJADtVg0Io6z.jpg';
        $prd->product_id = 8;
        $prd->save();

        $prd = new ProductImage();
        $prd->full = 'products/HpM9bDX33oEpQpPsD4XG6uKuu.jpg';
        $prd->product_id = 8;
        $prd->save();
    }
}
