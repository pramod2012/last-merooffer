<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Contracts\AttributeContract;
use App\Contracts\CategoryContract;

class AttributeController extends BaseController
{
    /**
     * @var AttributeContract
     */
    protected $attributeRepo;
    protected $categoryRepo;

    /**
     * AttributeController constructor.
     * @param AttributeContract $attributeRepository
     */
    public function __construct(
        AttributeContract $attributeRepo,
         CategoryContract $categoryRepo
     )
    {
        $this->attributeRepo = $attributeRepo;
        $this->categoryRepo = $categoryRepo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $datas = $this->attributeRepo->list('order', 'asc');

        $this->setPageTitle('Attributes', 'List of all attributes');
        return view('admin.attribute.index', compact('datas'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $options = $this->categoryRepo->list();
        $this->setPageTitle('Attributes', 'Create Attribute');
        return view('admin.attribute.create',compact('options'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $attribute = $this->attributeRepo->store($request);

        if (!$attribute) {
            return $this->responseRedirectBack('Error occurred while creating attribute.', 'error', true, true);
        }
        return $this->responseRedirect('attribute.index', 'Attribute added successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data = $this->attributeRepo->findById($id);
        $categories = $this->categoryRepo->list();

        $this->setPageTitle('Attributes', 'Edit Attribute : '.$data->name);
        return view('admin.attribute.edit', compact('data','categories'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request,$id)
    {
        $attribute = $this->attributeRepo->edit($request,$id);

        if (!$attribute) {
            return $this->responseRedirectBack('Error occurred while updating attribute.', 'error', true, true);
        }
        return $this->responseRedirect('attribute.index', 'Attribute updated successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $attribute = $this->attributeRepo->destroy($id);

        if (!$attribute) {
            return $this->responseRedirectBack('Error occurred while deleting attribute.', 'error', true, true);
        }
        return $this->responseRedirect('attribute.index', 'Attribute deleted successfully' ,'success',false, false);
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('datas');
        foreach($allArticles as $_article){
            $article = \App\Models\Attribute::find($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
