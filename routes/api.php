<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'Api\RegisterController@register');
Route::post('login', 'Api\RegisterController@login');

Route::get('menuAll', 'Api\CategoryController@menuAll');
Route::get('parentCategory', 'Api\CategoryController@parentCategory');
Route::get('childCategory/{id}', 'Api\CategoryController@childCategory');

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('profile', 'Api\UserController@profile');
    Route::get('myAds', 'Api\ProductController@myAds');
    Route::get('wishlistAds', 'Api\WishlistController@wishlistAds');
    Route::get('myContactEmails', 'Api\ContactController@myContactEmails');

    Route::get('myFollower', 'Api\FollowerController@myFollower');
    Route::get('myFollowing', 'Api\FollowerController@myFollowing');
});

Route::get('product', 'Api\ProductController@index');
Route::post('contact', 'Api\ContactController@store');
Route::post('report', 'Api\ReportController@store');
Route::post('comment', 'Api\CommentController@store');
Route::post('bid', 'Api\BidController@store');
//Product Api Controller

// Job Api Controller
Route::get('filter-jobs-based-on-criteria', 'Site\SearchController@filterJobsBasedOnSearchCriteria')->name('filterJobsBasedOnSearchCriteria');
Route::get('get-search-attributes-for-job-category', 'Site\SearchController@getAttributesOfJobCategory')->name('get-search-attributes-for-job-category');
Route::get('get-meta-data-for-navbar-job-search', 'Site\SearchController@getMetaDataForNavbarJobSearch')->name('getMetaDataForNavbarJobSearch');
Route::post('job', 'Api\JobController@store');
Route::patch('job/{job}', 'Api\JobController@update');
Route::get('job', 'Api\JobController@index');

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('employerprofile', 'Api\UserController@profile');
    Route::get('myAds', 'Api\ProductController@myAds');
    Route::get('wishlistAds', 'Api\WishlistController@wishlistAds');
    Route::get('myContactEmails', 'Api\ContactController@myContactEmails');

    Route::get('myFollower', 'Api\FollowerController@myFollower');
    Route::get('myFollowing', 'Api\FollowerController@myFollowing');
});

Route::apiResource('follower', 'Api\FollowerController');
Route::apiResource('wishlist', 'Api\WishlistController');

Route::apiResource('cart', 'Api\CartController');

Route::post('images/upload', 'Api\ProductImageController@upload')->name('products.images.upload');
Route::delete('images/{id}/delete', 'Api\ProductImageController@delete')->name('products.images.delete');

Route::get('product-comments/{id}', 'Site\ProductController@getCommentsForProduct');
Route::get('bid-stats/{product_id}', 'Site\BidController@getStatsOfProduct')->name('bid-stats');
Route::get('categories-for-search', 'Site\SearchController@getCategoriesForSearch')->name('categories-for-search');
Route::get('get-search-attributes-for-category', 'Site\SearchController@getAttributesOfCategory')->name('get-search-attributes-for-category');

Route::get('filter-products-based-on-criteria', 'Site\SearchController@filterProductsBasedOnSearchCriteria')->name('filterProductsBasedOnSearchCriteria');


Route::get('filter-products-based-on-location', 'Site\SearchController@filterProductsBasedOnLatitudeAndLongitude')->name('filterProductsBasedOnLatitudeAndLongitude');

Route::get('search-products-for-drop-down', 'Site\SearchController@searchProductsForDropDown')->name('searchProductsForDropDown');
Route::get('get-basic-ad-posting-data', 'Site\SearchController@getBasicAdPostingData')->name('getBasicAdPostingData');
Route::post('store-ad', 'Site\SearchController@storeAd')->name('storeAd')->middleware(['auth']);


Route::post('verify-mobile-number', 'Site\SearchController@verifyMobile')->middleware(['auth', 'throttle:1,1'])->name('verify-mobile-number');
Route::post('verify-mobile-otp', 'Site\SearchController@verifyMobileOtp')->middleware(['auth'])->name('verify-mobile-otp');