<?php

namespace App\Contracts;

/**
 * Interface UserContract
 * @package App\Contracts
 */
interface UserContract extends MyBaseContract
{
    public function editUser($request, $id);
}
