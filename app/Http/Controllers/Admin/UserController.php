<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\UserContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;

class UserController extends BaseController
{
    private $page="admin.user.";

    protected $userRepo;

    public function __construct(
        userContract $userRepo
    )
    {
        $this->userRepo = $userRepo;
    }

    public function index()
    {
        $list = $this->userRepo->list('created_at', 'desc');
        $users = $this->userRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('User', 'List of User');
        return view($this->page.'index',compact('users'));
    }

    public function create()
    {
        $this->setPageTitle('User', 'Create user');
        return view($this->page.'create');
    }

    public function store(StoreUserRequest $request)
    {
        $data = $this->userRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('user.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $user = $this->userRepo->findById($id);
        $this->setPageTitle('Users', 'Edit user');
        return view($this->page.'edit', compact('user'));
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $data = $this->userRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('user.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->userRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('user.index', 'Record deleted successfully' ,'success',false, false);
    }
}
