@extends('admin.product.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">Create Products</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{route('product.store')}}" enctype="multipart/form-data">
                       {{csrf_field()}}
                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Title/Name *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="description" class="col-md-2 control-label">Description / Summary *</label>

                            <div class="col-md-10">
                                <textarea rows="5" type="text" class="form-control" name="desc" value="{{ old('desc') }}">{{old('desc')}}</textarea>
                                <span class="text-danger">{{ $errors->first('desc') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
        <label class="col-md-2 control-label">Category</label>
              <div class="col-md-10">
                <select class="form-control select2" multiple="multiple" data-placeholder="Categories" style="width: 100%;" name="category_id[]">
                  @foreach($categories as $value)      
                  <option value="{{$value->id}}">{{$value->name}}</option>
                  @endforeach
                </select>
              </div>
  </div>

                         

                    <div class="form-group">
                            <label class="col-md-2 control-label">Price</label>

                            <div class="col-md-2">
                                <input type="number" class="form-control" name="price" value="{{old('price')}}">
                            </div>
                            <level class="col-md-2 control-label"><b>Regular Price</b></level>
                            <div class="col-md-2">
                                <input type="number" class="form-control" name="rgr_price" value="{{old('rgr_price')}}">
                            </div>
                        </div>

                        <div class="form-group">
                      <label class="col-md-2 control-label">Pricelabel</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="pricelabel_id">
                              <option disabled>Chose one pricelabel</option>
                                @foreach(\App\Models\Pricelabel::all() as $value)
                              <option value="{{ $value->id }}">{{$value->name}}</option>
                                @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('condition_id') }}</span>
                            </div>
                        </div>

                        
                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Cell/Mobile Number</label>
                            <div class="col-md-10">
                                <input type="number" class="form-control" name="cell" value="{{ old('cell') }}">
                                <span class="text-danger">{{ $errors->first('cell') }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Youtube URL</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="url" value="{{ old('url') }}">
                                <span class="text-danger">{{ $errors->first('url') }}</span>
                            </div>
                        </div>
                        

                        <div class="form-group">
                      <label class="col-md-2 control-label">Condition</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="condition_id">
                              <option disabled>Chose one condition</option>
                                @foreach(\App\Models\Condition::all() as $value)
                              <option value="{{ $value->id }}">{{$value->name}}</option>
                                @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('condition_id') }}</span>
                            </div>
                        </div>

                        


                        <div class="form-group">
                      <label class="col-md-2 control-label">Price Type</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="pricetype_id">
                                @foreach($pricetypes as $value)
                              <option value="{{ $value->id }}">{{$value->name}}</option>
                                @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('pricetype_id') }}</span>
                            </div>
                        </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label">Day</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="day_id">
                                @foreach($days as $value)
                              <option value="{{$value->id}}">{{$value->name}}</option>
                              @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('day_id') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                      <label class="col-md-2 control-label">Adtype</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="adtype_id">
                              @foreach($adtype as $value)
                              <option value="{{$value->id}}">{{$value->name}}</option>
                              @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('adtype_id') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                      <label class="col-md-2 control-label">City</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="city_id">
                                @foreach($city as $value)
                              <option value="{{$value->id}}">{{$value->name}}</option>
                              @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('city_id') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                      <label class="col-md-2 control-label">Posed By</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="user_id">
                                @foreach($users as $value)
                              <option value="{{$value->id}}">{{$value->name}}</option>
                              @endforeach
                            </select>
                                <span class="text-danger">{{ $errors->first('user_id') }}</span>
                            </div>
                        </div>
                



                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Address</label>
                            <div class="col-md-10">
                                <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}">
                                <span class="text-danger">{{ $errors->first('address') }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                                    <div class="col-sm-9">         
                                        <div id="map-canvas"></div>
                                    </div>
                            </div>

<div class="col-sm-9">  
<ul id="geoData">
    <li>Full Address: <span id="location-snap"></span></li>
    <li>Latitude: <span id="lat-span"></span></li>
    <li>Longitude: <span id="lon-span"></span></li>
    <input type="text" id="lat" name="latitude" value="{{old('latitude')}}">
    <input type="text" id="lng" name="longitude" value="{{old('longitude')}}">
</ul>
<div class="help-block">{{ $errors->first('latitude') }}</div>
<div class="help-block">{{ $errors->first('longitude') }}</div>
</div>
          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="featured" value="0">
                  <input name="featured" type="checkbox" value="1">
                      Featured
                    </label>
                  </div>
              </div>
          </div>


          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="status" value="0">
                      <input name="status" type="checkbox" value="1">
                      Status 
                    </label>
                  </div>
              </div>
          </div>

          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="sold" value="0">
                  <input name="sold" type="checkbox" value="1">
                      Sold
                    </label>
                  </div>
              </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="bid" value="0">
                  <input name="bid" type="checkbox" value="1">
                      Bid
                    </label>
                  </div>
              </div>
          </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function initMap() {
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
      center: {lat: 22.3038945, lng: 70.80215989999999},
      zoom: 13
    });
    var input = document.getElementById('address');
   
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
  
    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });
  
    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
    
        /* If the place has a geometry, then present it on a map. */
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
        marker.setIcon(({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);
      
        var address = '';
        if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }
      
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
        
        /* Location details */
        document.getElementById('location-snap').innerHTML = place.formatted_address;
        document.getElementById('lat-span').innerHTML = place.geometry.location.lat();
        document.getElementById('lon-span').innerHTML = place.geometry.location.lng();
        document.getElementById("lat").value = place.geometry.location.lat();
    document.getElementById("lng").value = place.geometry.location.lng();

    });
}
</script>

<style>
      /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQuDQmtiHkS7CcriyEiYXWja3ODrG4vFI&libraries=places&callback=initMap" async defer></script>
@endsection
