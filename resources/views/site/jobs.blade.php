@extends('site.partials.main_index')
@section('title', 'Search Jobs in Nepal - Job Vacancies in Nepal | Mero Offer')
@section('meta-tags')
<meta name="description" content="Find most recent jobs in Nepal at merooffer. We offer career opportunities &amp; advice for job seekers and hiring &amp; recruitment services for organization." />
<meta name="keywords" content="merooffer.com,jobs in Nepal, jobsite in Nepal, job vacancies in Nepal, job vacancy in Nepal, job site in Nepal recent job vacancies in Nepal, job opportunities in Nepal, job sites in Nepal, find jobs in Nepal, find jobs in Nepal, job search in Nepal, job search." />
<meta name="author" content="merooffer.com" />
@endsection
@section('og-tags')
<meta property='og:title' content="Search Jobs in Nepal - Job Vacancies in Nepal | Mero Offer" />
<meta property='og:image' content="{{asset('frontend/images/merooffer.jpg')}}" />
<meta property='og:description' content='Find most recent jobs in Nepal at merooffer. We offer career opportunities and advice for job seekers and hiring and recruitment services for organization.' />
<meta property='og:url' content="{{app('request')->url()}}">
<meta property=" og:type" content="website" />
<meta property="og:site_name" content="{{app('request')->url()}}" />
@endsection
@section('content')
<body
    class="home page-template page-template-template-homepage-v5 page-template-template-homepage-v5-php page page-id-6 logged-in">
    @include('site.partials.navbarjob')
    @include('site.partials.searchjob')
    <!--search-section-->
    <section class="classiera-simple-bg-slider">
        <div class="classiera-simple-bg-slider-content text-center">
            <h1>Welcome To <span class="color">Mero Job Offer</span></h1>
            <h4>Browse jobs by Popular job Categories</h4>
            <div class="category-slider-small-box  text-center">
                <ul class="list-inline list-unstyled">
                    @foreach(\App\JobCategory::where('featured',1)->limit(6)->get() as $value)
                    <li>
                        <a class="match-height" href="{{ url('search-job?category_id='.$value->id) }}">
                            <i class="{{$value->font}}"></i>
                            <p>{{$value->name}}</p>
                        </a>
                    </li>
                    @endforeach
                </ul>
                <!--list-inline list-unstyled-->
            </div>
            <!--category-slider-small-box-->
        </div>
        <!--classiera-simple-bg-slider-content-->
    </section>
    <!--classiera-simple-bg-slider-->


    <section class="classiera-advertisement advertisement-v5 section-pad-80 border-bottom">
        <div class="section-heading-v5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 center-block">
                        <h3 class="text-capitalize">Jobs</h3>
                        <p>A job section where we have six types of jobs listing one with top, hot, featured, premium,
                            platinum and the other one with general job listing.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-8 col-lg-6">
                    <aside class="sidebar">
                        <div class="row">
                            <!--cit-->

                            <!--cit-->
                            <div class="col-lg-12 col-md-12 col-sm-6 match-height" style="height: 730px;">
                                <div class="widget-box">
                                    <div class="widget-title">
                                        <h4>Top Jobs</h4>
                                    </div>
                                    <div class="widget-content">
                                        @foreach(\App\Job::orderBy('created_at','desc')->where('jobtype_id',3)->limit(10)->get()->chunk(2)
                                        as $jobs)
                                        <div class="media footer-pr-widget-v1">
                                            @foreach($jobs as $job)
                                            <div class="col-md-10 col-md-8 col-lg-6">

                                                <div class="media-left">
                                                    <a class="media-img" href="{{route('jobuser',$job->user->id)}}">
                                                        @if (!empty($job->user->firmuser->logo))
                                                        <img class="media-object"
                                                            src="{{ asset('storage/'.$job->user->firmuser->logo) }}"
                                                            alt="{{$job->name}}" style="max-height: 100%">
                                                        @else
                                                        <img class="media-object"
                                                            src="{{ asset('storage/'.config('settings.error_image')) }}"
                                                            alt="{{$job->name}}">
                                                        @endif
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <span class="price">{{$job->user->firmuser->name}}</span>
                                                    <h4 class="media-heading">
                                                        <a href="{{ route('job_post', $job->slug) }}">{{$job->name}}</a>
                                                    </h4>
                                                    <span class="category">
                                                </div>
                                            </div>
                                            @endforeach

                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-6 match-height" style="">
                                <div class="widget-box">
                                    <div class="widget-title">
                                        <h4>Hot Jobs</h4>
                                    </div>
                                    <div class="widget-content">
                                        @foreach(\App\Job::orderBy('created_at','desc')->where('jobtype_id',4)->limit(10)->get()->chunk(2)
                                        as $jobs)
                                        <div class="media footer-pr-widget-v1">
                                            @foreach($jobs as $job)
                                            <div class="col-md-10 col-md-8 col-lg-6">

                                                <div class="media-left">
                                                    <a class="media-img" href="{{route('jobuser',$job->user->id)}}">
                                                        @if (!empty($job->user->firmuser->logo))
                                                        <img class="media-object"
                                                            src="{{ asset('storage/'.$job->user->firmuser->logo) }}"
                                                            alt="{{$job->name}}" style="max-height: 100%">
                                                        @else
                                                        <img class="media-object"
                                                            src="{{ asset('storage/'.config('settings.error_image')) }}"
                                                            alt="{{$job->name}}">
                                                        @endif
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <span class="price">{{$job->user->firmuser->name}}</span>
                                                    <h4 class="media-heading">
                                                        <a href="{{ route('job_post', $job->slug) }}">{{$job->name}}</a>
                                                    </h4>
                                                    <span class="category">
                                                </div>
                                            </div>
                                            @endforeach

                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--row-->
                    </aside>
                </div>




                <div class="col-md-6 col-md-4 col-lg-3">
                    <aside class="sidebar">
                        <div class="column row">
                            <!--cit-->

                            <!--cit-->
                            <div class="col-lg-12 col-md-12 col-sm-6 match-height" style="height: 730px;">
                                <div class="widget-box">
                                    <div class="widget-title">
                                        <h4>Premium Jobs</h4>
                                    </div>
                                    <div class="widget-content">
                                        @foreach(\App\Job::orderBy('created_at','desc')->where('jobtype_id',7)->limit(10)->get()
                                        as $job)
                                        <div class="media footer-pr-widget-v1">
                                            <div class="media-left">
                                                <a class="media-img" href="{{route('jobuser',$job->user->id)}}">
                                                    @if (!empty($job->user->firmuser->logo))
                                                    <img class="media-object"
                                                        src="{{ asset('storage/'.$job->user->firmuser->logo) }}"
                                                        alt="{{$job->name}}" style="max-height: 100%">
                                                    @else
                                                    <img class="media-object"
                                                        src="{{ asset('storage/'.config('settings.error_image')) }}"
                                                        alt="{{$job->name}}">
                                                    @endif
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <span class="price">{{$job->user->firmuser->name}}</span>
                                                <h4 class="media-heading">
                                                    <a href="{{ route('job_post', $job->slug) }}">{{$job->name}}</a>
                                                </h4>
                                                <span class="category">
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!--row-->
                    </aside>
                </div>

                <div class="col-md-6 col-md-4 col-lg-3">
                    <aside class="sidebar">
                        <div class="column row">
                            <!--cit-->

                            <!--cit-->
                            <div class="col-lg-12 col-md-12 col-sm-6 match-height" style="height: 730px;">
                                <div class="widget-box">
                                    <div class="widget-title">
                                        <h4>Platinum Jobs</h4>
                                    </div>
                                    <div class="widget-content">
                                        @foreach(\App\Job::orderBy('created_at','desc')->where('jobtype_id',6)->limit(10)->get()
                                        as $job)
                                        <div class="media footer-pr-widget-v1">
                                            <div class="media-left">
                                                <a class="media-img" href="{{route('jobuser',$job->user->id)}}">
                                                    @if (!empty($job->user->firmuser->logo))
                                                    <img class="media-object"
                                                        src="{{ asset('storage/'.$job->user->firmuser->logo) }}"
                                                        alt="{{$job->name}}" style="max-height: 100%">
                                                    @else
                                                    <img class="media-object"
                                                        src="{{ asset('storage/'.config('settings.error_image')) }}"
                                                        alt="{{$job->name}}">
                                                    @endif
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <span class="price">{{$job->user->firmuser->name}}</span>
                                                <h4 class="media-heading">
                                                    <a href="{{ route('job_post', $job->slug) }}">{{$job->name}}</a>
                                                </h4>
                                                <span class="category">
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!--row-->
                    </aside>
                </div>





            </div>
        </div>

        <div class="view-all text-center">
            <a href="{{url('all-jobs')}}" class="btn btn-primary outline btn-md btn-style-five">
                View All Jobs </a>
        </div>

    </section>
    <section class="section-pad-80 category-v5">
        <div class="section-heading-v5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-9 center-block">
                        <h1 class="text-capitalize">Job categories</h1>
                        <p></p>
                    </div>
                    <!--col-lg-7-->
                </div>
                <!--row-->
            </div>
            <!--container-->
        </div>
        <!--section-heading-v1-->
        <div class="container">
            <ul class="list-inline list-unstyled categories">
                @foreach(\App\JobCategory::orderBy('order','asc')->limit(12)->get() as $value)
                <li class="match-height">
                    <div class="category-content">
                        <i style="color:{{$value->color}};" class="{{$value->font}}"></i>
                        <h4>
                            <a href="{{ url('search-job?category_id='.$value->id) }}">
                                {{$value->name}} </a>
                        </h4>
                    </div>
                    <!--category-box-->
                </li>
                <!--col-lg-4-->
                @endforeach
            </ul>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="view-all text-center">
                    <a href="{{url('/all-jobcategories')}}" class="btn btn-primary outline btn-style-five">View All Job
                        Categories</a>
                </div>
            </div>
        </div>
    </section>
    <section class="locations locations-v5 section-pad-80 border-bottom">
        <div class="section-heading-v5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 center-block">
                        <h3 class="text-capitalize">Job Type</h3>
                        <p>Mero Offer provided you job type section where you can search for particular job type. </p>
                    </div>
                    <!--col-lg-8 col-md-8 center-block-->
                </div>
                <!--row-->
            </div>
            <!--container-->
        </div>
        <!--section-heading-v1-->
        <div class="location-content-v5">
            <div class="container">
                <ul class="list-inline list-unstyled">
                    @foreach(\App\Jobtype::all() as $value)
                    <li>
                        <div class="location-content match-height">
                            <i class="fas fa-star fa-4x"></i>
                            <h5>
                                <a href="{{route('jobtype',$value->slug)}}">{{$value->name}} Jobs (
                                    {{$value->jobs->count()}} )</a>
                            </h5>
                        </div>
                    </li>@endforeach
                </ul>
                <!--container-->
            </div>
            <!--container-->
        </div>
        <!--location-content-->
    </section>

    @if($posts->count() > 0)
    <section class="blog-post-section blog-post-section-bg section-pad-80 border-bottom">
        <!-- section heading with icon -->
        <div class="section-heading-v5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 center-block">
                        <h1 class="text-capitalize">Latest Blog Posts</h1>
                    </div>
                </div>
            </div>
        </div><!-- section heading with icon -->
        <div class="container">
            <div class="row">
                @foreach($posts as $post)
                <div class="col-lg-3 col-sm-4 col-md-3">
                    <div class="blog-post blog-post-v2 match-height">
                        <div class="blog-post-img-sec">
                            <div class="blog-img">

                                <img width="800" height="500" src="{{@asset('images_small/'. $post->image)}}"
                                    class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="blog post"
                                    srcset="{{@asset('images_small/'. $post->image)}}"
                                    sizes="(max-width: 800px) 100vw, 800px" />
                                <span class="hover-posts">
                                    <a href="{{route('blog',$post->slug)}}"
                                        class="btn btn-primary radius btn-md active">
                                        View Post
                                    </a>
                                </span>
                            </div>
                        </div>
                        <div class="blog-post-content">
                            <h4><a href="{{route('blog',$post->slug)}}">{{$post->name}}</a></h4>
                            <p>
                                <span>
                                    <i class="fas fa-user"></i>
                                    <a href="#">{{$post->author->name}}</a>
                                </span>
                                <span><i class="far fa-clock"></i>{{$value->created_at->format('M j, Y')}}</span>
                            </p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="view-all text-center">
            <a href="{{route('blog.all')}}" class="btn btn-primary btn-style-five">View All</a>
        </div>
    </section><!-- /.blog post -->
    @endif
    <section class="call-to-action parallax__400" style="background-image:url()">
        <div class="container">
            <div class="row gutter-15">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="call-to-action-box match-height">
                        <div class="action-box-heading">
                            <div class="heading-content">
                                <img src="{{ asset('frontend/images/user-icon.png') }}" alt="About Us">
                            </div>
                            <div class="heading-content">
                                <h3>About Us</h3>
                            </div>
                        </div>
                        <div class="action-box-content">
                            <p>
                                MeroOffer.com is free online classified website. The Mero Offer Jobs website makes it
                                easier for you to find the job you're looking for in and around your local community.
                            </p>
                        </div>
                    </div>
                </div>
                <!--End About Section-->
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="call-to-action-box match-height">
                        <div class="action-box-heading">
                            <div class="heading-content">
                                <img src="{{ asset('frontend/images/apply-png.png') }}" alt="Sell Safely">
                            </div>
                            <div class="heading-content">
                                <h3>Apply Now</h3>
                            </div>
                        </div>
                        <div class="action-box-content">
                            <p>
                                You can start applying jobs once you signup your account. You can post your resumes
                                without any registration and company will call you for an interview.
                            </p>
                        </div>
                    </div>
                </div>
                <!--End Sell Safely Section-->
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="call-to-action-box match-height">
                        <div class="action-box-heading">
                            <div class="heading-content">
                                <img src="{{ asset('frontend/images/hired.png') }}" alt="Buy Safely">
                            </div>
                            <div class="heading-content">
                                <h3>Get Hired</h3>
                            </div>
                        </div>
                        <div class="action-box-content">
                            <p>
                                You might get shortlisted for an interview. If you have completed the profile you might
                                get notifications from the website. Always be prepared for interview and get hired.
                            </p>
                        </div>
                    </div>
                </div>
                <!--End Buy Safely Section-->
            </div>
            <!--row gutter-15-->
        </div>
        <!--container-->
    </section>
    <!--call-to-action-->

    @if($partners->count() > 0)

    <section class="partners-v3 section-gray-bg">
        <div class="container" style="overflow: hidden;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="owl-carousel partner-carousel-v3 partner-carousel-v4" data-car-length="6" data-items="6"
                        data-loop="true" data-nav="false" data-autoplay="true" data-autoplay-timeout="2000"
                        data-dots="false" data-auto-width="false" data-auto-height="true" data-right="false"
                        data-responsive-small="2" data-autoplay-hover="true" data-responsive-medium="4"
                        data-responsive-xlarge="6" data-margin="30">
                        @foreach($partners as $value)
                        <div class="item partner-img match-height">
                            <span class="helper"></span>
                            <a href="//{{$value->link}}" target="_blank">
                                <img src="{{@asset('images_small/'.$value->image)}}" alt="partner">
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
                <!--col-lg-12-->
            </div>
            <!--row-->
        </div>
        <!--container-->
    </section>
    @endif

    @include('site.partials.footer')
    @endsection
