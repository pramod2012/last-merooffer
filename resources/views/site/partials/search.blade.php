{{-- <section class="search-section search-section-v5" style="background: #39444c;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form data-toggle="validator" role="form" class="search-form search-form-v5 form-inline"
                    action="{{ route('search-product')}}" method="get" novalidate="true">
                    <!--Select Category-->
                    <div class="form-group clearfix" style="background-color: white">
                        <div class="input-group side-by-side-input inner-addon right-addon pull-left flip">
                            <i class="form-icon form-icon-size-small fas fa-sort"></i>
                            <select class="form-control form-control-sm" name="category_id">
                                <option value="" selected disabled>All Categories</option>
                                @foreach(\App\Models\Category::where('status',1)->orderBy('order','asc')->where('parent_id',null)->get()
                                as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                                @foreach($category->children as $value)
                                @if(Request::get('category_id') == $value->id)
                                <option value="{{$value->id}}" selected>&nbsp;&nbsp;&nbsp;{{$value->name}}</option>
                                @else
                                <option value="{{$value->id}}">&nbsp;&nbsp;&nbsp;{{$value->name}}</option>
                                @endif
                                @endforeach
                                @endforeach
                            </select>
                        </div>
                        <div class="side-by-side-input pull-left flip classieraAjaxInput">
                            <input type="text" name="name" value="{{ Request::get('name') }}" id="title"
                                class="form-control form-control-sm" placeholder="Enter keyword or title..."
                                data-error="Please Type some words..!">
                            <div class="help-block with-errors"></div>
                            <span class="classieraSearchLoader" style="display: none;"><img
                                    src="https://demo.joinwebs.com/classiera/ivy/wp-content/themes/classiera/images/loader.gif"
                                    alt="classiera loader"></span>
                        </div>
                    </div>
                    <!--Select Category-->
                    <!--Locations-->
                    <div class="form-group">
                        <div class="input-group inner-addon right-addon">
                            <div class="input-group-addon input-group-addon-width-sm"><i
                                    class="fas fa-map-marker-alt"></i></div>
                            <input type="text" id="getCity" name="address" value="{{ Request::get('address') }}"
                                class="form-control form-control-sm" placeholder="Please type location">
                            <a id="getLocation" href="#" class="form-icon form-icon-size-small"
                                title="Click here to get your own location">
                                <i class="fas fa-crosshairs"></i>
                            </a>
                        </div>
                    </div>
                    <!--Locations-->
                    <!--PriceRange-->
                    <div class="form-group clearfix">
                        <div class="inner-addon right-addon">
                            <i class="form-icon form-icon-size-small fas fa-sort"></i>
                            <select class="form-control form-control-sm" data-placeholder="Select price range"
                                name="city_id">
                                <option value="" selected disabled>City Name</option>
                                @foreach(\App\Models\City::all() as $value)
                                @if(Request::get('city_id') == $value->id)
                                <option value="{{$value->id}}" selected>{{$value->name}}</option>
                                @else
                                <option value="{{$value->id}}">{{$value->name}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <!--PriceRange-->
                    <div class="form-group">
                        <button class="radius" type="submit">Search Now</button>
                    </div>
                </form>
            </div>
            <!--col-md-12-->
        </div>
        <!--row-->
    </div>
    <!--container-->
</section> --}}
<div id="nav-bar-search">

    <nav-bar-search></nav-bar-search>
    
    <chat-head></chat-head>

</div>
