<?php

namespace App\Repo;

use App\Models\Comment;
use App\Models\Product;
use App\Contracts\CommentContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubmitCommentEmail;
use App\Mail\SubmitCommentEmailReceipient;

/**
 * Class CommentRepository
 *
 * @package \App\Repo
 */
class CommentRepo extends BaseRepo implements CommentContract
{
    /**
     * CommentRepository constructor.
     * @param Comment $model
     */
    public function __construct(Comment $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function store($request)
    {
        try {
            $comment = new Comment;
            $comment->bid = $request->get('bid');
            $comment->url = $request->get('url');
            $comment->body = $request->get('body');
            $comment->user()->associate($request->user());
            $product = Product::find($request->get('product_id'));
            $product->comments()->save($comment);

            Mail::to($product->user->email)->send(new SubmitCommentEmailReceipient($comment));
            Mail::to($comment->user->email)->send(new SubmitCommentEmail($comment));

            return $product;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    public function replyStore($request)
    {
        $reply = new Comment();
        $reply->body = $request->get('body');
        $reply->user()->associate($request->user());
        $reply->parent_id = $request->get('parent_id');
        $post = Product::find($request->get('product_id'));
        $post->comments()->save($reply);
        return $post;
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
        try
        {
            $comment = Comment::findOrFail($id);
            $comment->bid = $request->get('bid');
            $comment->url = $request->get('url');
            $comment->body = $request->get('body');
            $comment->status = $request->get('status');
            $comment->user_id = $request->get('user_id');
            $product = Product::find($request->get('product_id'));
            $product->comments()->save($comment);

            if($request->status == '1') {
            Mail::to($product->user->email)->send(new SubmitCommentEmailReceipient($comment));
            Mail::to($comment->user->email)->send(new SubmitCommentEmail($comment));
            }

            return $product;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);
        $data->delete();
        return $data;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $data = Comment::where('slug', $slug)->first();
        return $data;
    }
}
