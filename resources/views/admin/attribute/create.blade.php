@extends('admin.attribute.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Attribute</div>
                <div class="panel-body">
                    <form method="POST" action="{{ route('attribute.store') }}" aria-label="subcategory">
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">Name</label>

                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">Type</label>
                            <div class="col-md-6">
                                <select class="form-control" name="type">
                                    <option value="dropdown">Dropdown</option>
                                    <option value="text">Text</option>
                                    <option value="radio">Radio</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="data_type" class="col-md-2 col-form-label text-md-right">Data Type</label>
                            <div class="col-md-8">
                                <input id="data_type" type="text" class="form-control{{ $errors->has('data_type') ? ' is-invalid' : '' }}" name="data_type" value="{{ old('data_type') }}">
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">Unique Code</label>

                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" value="{{ old('code') }}">
                            </div>
                        </div>

                        

                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">Category</label>
                            <div class="col-md-8">
                                <select class="form-control select2" multiple="multiple" data-placeholder="Category" style="width: 100%;" name="category_id[]">
                                    @foreach($options as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

        <div class="form-group">
            <div class="col-md-6">
                  <div class="checkbox">
                    <label class="col-md-12 control-label">
                  <input name="required" type="checkbox">
                      Required
                    </label>
                  </div>
              </div>
          </div>

          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                  <input name="filterable" type="checkbox">
                      Filterable
                    </label>
                  </div>
              </div>
          </div>

          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                <input type="hidden" name="has_combin" value="0">
                  <input name="has_combin" type="checkbox" value="1">
                      Has Combination
                    </label>
                  </div>
              </div>
          </div>
          
          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                <input type="hidden" name="interval" value="0">
                  <input name="interval" type="checkbox" value="1">
                      Has Interval
                    </label>
                  </div>
              </div>
          </div>
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
