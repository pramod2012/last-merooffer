<?php

namespace App\Contracts;

/**
 * Interface FirmuserContract
 * @package App\Contracts
 */
interface FirmuserContract extends MyBaseContract
{
    public function storeFirmuser($request);

    public function editFirmuser($request, $id);
}
