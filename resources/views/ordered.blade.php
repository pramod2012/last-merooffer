@extends('site.partials.main_index')
@section('title','Ordered Status')
@section('content')
<body class="page-template page-template-template-favorite page-template-template-favorite-php page page-id-22 logged-in">
@include('site.partials.navbar')
<section class="user-pages section-gray-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-4">
      @include('site.partials.sidebar')     
</div><!--col-lg-3-->
      <div class="col-lg-9 col-md-8 user-content-height">
        <div class="user-detail-section section-bg-white">
          @include('partials.alert')
          <!-- favorite ads -->
          <div class="user-ads favorite-ads">
            <h4 class="user-detail-section-heading text-uppercase">
            Order Items of your product          </h4>
                                                <!--singlepost-->
               
                  <div class="row">
                                <div class="col-md-12">
                                    
                                    <table class="table table-striped">
                                        <thead>
                                            <th>Id</th>
                                            <th>Price</th>
                                            <th>Shipping</th>
                                            <th>Total</th>
                                            <th>Date</th>
                                            <th>Ordered By</th>
                                            <th>Status</th>
                                            <th></th>

                                        </thead>
                                        <tbody>
                                            @foreach($products as $product)
                                            @foreach($product->orders as $order)
                                                <tr>
                                                    <td>{{$order->id}}</td>
                                                    <td>{{$order->total_products}}</td>
                                                    <td>{{$order->total_shipping}}</td>
                                                    <td>{{$order->total_paid}}</td>
                                                    <td>{{$order->created_at->format('F j, Y')}}</td>
                                                    <td>{{$order->user->name}}, {{$order->address->phone}}</td>
                                                    <td>{{$order->orderStatus->name}}</td>
                                                    <td><i class="fa fa-edit"></i></td>
                                                </tr>
                                            @endforeach
                                            @endforeach
                                            
                                        </tbody>
                                        
                                    </table>
                                </div>
                            </div>

            

            <!--singlepost-->
                    
                      </div><!--user-ads-->
          
          <!-- favorite ads -->
        </div><!--user-detail-->
      </div><!--col-lg-9-->
    </div><!--row-->
  </div><!--container-->
</section><!--user-pages-->
<!-- Company Section Start-->
<!-- Company Section End--> 
@include('site.partials.footer')
@endsection