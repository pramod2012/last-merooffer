<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Job;
use App\Models\User;

class Resume extends Model
{
    protected $fillable = [
       'name','email','cv','message','job_id','user_id','status',
    ];

    
    public function job()
    {
        return $this->belongsTo(Job::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
