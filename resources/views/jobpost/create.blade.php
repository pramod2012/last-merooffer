@extends('site.partials.main_index')
@section('title','Create a Job Post')
@section('content')
<body class="page-template page-template-template-submit-ads page-template-template-submit-ads-php ">
@include('site.partials.navbar')
<section class="user-pages section-gray-bg">
    <div class="container">
        <div class="row">
            <!--col-lg-3 col-md-4-->
                <div class="col-lg-12 col-md-10 user-content-height">
					<div class="user-detail-section section-bg-white">
					<div class="user-ads user-profile-settings">
						<h4 class="user-detail-section-heading text-uppercase">
							Create a Job Post					</h4>						
						<form data-toggle="validator" role="form" method="POST" id="primaryPostForm" action="{{route('job-user.store')}}" enctype="multipart/form-data">
							{{ csrf_field() }}
							<!-- user basic information -->
							<div class="form-inline row form-inline-margin">
								<div class="form-group col-sm-10">
                                    <label for="first-name">Title of Job Posting <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="name" class="form-control form-control-sm" placeholder="Example: Web Developer with Ecommerce Experience.." value="{{old('name')}}">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                </div><!--Firstname-->
								<div class="form-group col-sm-12">
                                    <label for="bio">Job Description <span>*</span></label>
                                    <div class="inner-addon">
                                        <textarea name="body" id="breakText" placeholder="enter your job details..">{{old('body')}}</textarea>
                                        <p>{{$errors->first('body')}}</p>
                                    </div>
                                </div><!--biography-->

                                                   <script type="text/javascript">
  //Some text string that has line breaks in it that need to be removed
  var someText = "{{old('body')}}";
  //This javascript code replaces all 3 types of line breaks with a single space
  someText = someText.replace(/(\r\n|\n|\r)/gm," ");
  //Replace all double white spaces with single spaces
  someText = someText.replace(/\s+/g," ");
  
  //Put the newly treated text string into the textarea to show the result
  document.getElementById("breakText").value = someText;
</script>
							
							<!-- user basic information -->
							<!-- Contact Details -->
								<div class="form-group col-sm-2">
                                    <label for="phone">Salary From </label>
                                    <div class="inner-addon">
                                        <input type="number" id="salary" class="form-control form-control-sm" placeholder="Salary" name="salary" value="{{old('salary')}}">
                                        {{ $errors->first('salary') }}
                                    </div>
                                </div>
                                <div class="form-group col-sm-3">
                                    <label for="phone">To </label>
                                    <div class="inner-addon">
                                        <input type="number" id="salary" class="form-control form-control-sm" placeholder="Salary" name="salary_to" value="{{old('salary_to')}}">
                                        {{ $errors->first('salary_to') }}
                                    </div>
                                </div>
                                
                                <div class="form-group col-sm-5">
                                    <label for="phone"> Salary Type *</label>
                                    <div class="inner-addon">
                                        <select class="form-control form-control-sm" name="salarytype_id">
                                            @foreach(\App\Salarytype::all() as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                        {{ $errors->first('salarytype_id') }}
                                    </div>
                                </div>
                                <!--Phone Number-->
								<div class="form-group col-sm-5">
                                    <label for="mobile">Job Category <span>*</span></label>
                                    <div class="inner-addon">
                                        
                                    	<select class="form-control form-control-sm" id="jobcategory" name="category_id">

						    				<option selected disabled>...Select Job Category</option>
			                        		@foreach ($categories as $category)
                                              @if(old('category_id') == $category->id)
                                              <option value="{{$category->id}}" selected>{{$category->name}}</option>
                                              @else
			                            	<option value="{{$category->id}}">{{$category->name}}</option>
                                                @endif
			                        		@endforeach
						    			</select>
                                        {{$errors->first('category_id')}}
                                    </div>
                                </div><!--Mobile Number-->
								<div class="form-group col-sm-5">
                                    <label for="website">Position Type <span>*</span></label>
                                    <div class="inner-addon">
                                        <select class="form-control form-control-sm" id="positionType" name="positiontype_id" id="positionType">
						    				<option selected disabled>...Select Position Type</option>
                                        @foreach($positiontypes as $value)
                                            @if(old('positiontype_id') == $value->id)
                                            <option value="{{$value->id}}" selected>{{$value->name}}</option>
                                            @else
			                    			<option value="{{$value->id}}">{{$value->name}}</option>
                                            @endif
			                   				@endforeach
						    			</select>
                                        {{$errors->first('positiontype_id')}}
                                    </div>
                                </div><!--Your Website-->
								<div class="form-group col-sm-5">
                                    <label for="email">Last Date of Apply <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="endate" name="endate" class="form-control form-control-sm" placeholder="{{ date('Y/m/d') }}" value="{{ date('Y/m/d') }}">
                                        {{$errors->first('endate')}}
                                    </div>
                                </div><!--Your Email-->
								<div class="form-group col-sm-5">
                                    <label for="email">Education <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="education" name="education" class="form-control form-control-sm" placeholder="enter education ie.MBA" value="{{old('education')}}">
                                        {{$errors->first('education')}}
                                    </div>
                                </div><!--Your Country-->
								<div class="form-group col-sm-5">
                                    <label for="email">Experience <span>*</span></label>
                                    <div class="inner-addon">
                                        <select class="form-control form-control-sm" id="experience_id" name="experience_id">
                                            <option selected disabled>...Select Experience</option>
                                            @foreach(\App\Experience::all() as $value)
                                            @if(old('experience_id') == $value->id)
                                            <option value="{{$value->id}}" selected>{{$value->name}}</option>
                                            @else
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        {{$errors->first('experience_id')}}
                                    </div>
                                </div><!--Your Post Code-->
                                <div class="form-group col-sm-5">
                                    <label for="email">Gender</label>
                                    <div class="inner-addon">
                                        <input type="text" id="gender" name="gender" class="form-control form-control-sm" placeholder="enter sex ie. Male or Female" value="{{old('gender')}}">
                                        {{$errors->first('gender')}}
                                    </div>
                                </div><!--Your Country-->
                                <div class="form-group col-sm-5">
                                    <label for="email">Age</label>
                                    <div class="inner-addon">
                                        <input type="text" id="age" name="age" class="form-control form-control-sm" placeholder="age" value="{{old('age')}}">
                                        {{$errors->first('age')}}
                                    </div>
                                </div><!--Your State-->
								<div class="form-group col-sm-5">
                                    <label for="email">Number of Vacancy <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="number" id="vac_no" name="vac_no" class="form-control form-control-sm" placeholder="Number of Vacancy" value="{{old('vac_no')}}">
                                        {{$errors->first('vac_no')}}
                                    </div>
                                </div><!--Your City-->
								<div class="form-group col-sm-5">
                                    <label for="email">Level <span>*</span></label>
                                    <div class="inner-addon">
                                        <select class="form-control form-control-sm" id="level_id" name="level_id">
						    				<option selected disabled>...Select Job Level</option>
						    				@foreach($levels as $value)
                                            @if(old('level_id') == $value->id)
                                            <option value="{{$value->id}}" selected>{{$value->name}}</option>
                                            @else
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                            @endif
			                   				@endforeach
						    			</select>
                                        {{$errors->first('level_id')}}
                                    </div>
                                </div><!--Your Post Code-->
								<div class="form-group col-sm-5">
                                    <label for="address">Industry <span>*</span></label>
                                    <div class="inner-addon">
                                        <select class="form-control form-control-sm" id="positionType" name="industry_id" id="positionType">
						    				<option selected disabled>...Select Industry</option>
						    				@foreach($industries as $value)
                                            @if(old('industry_id') == $value->id)
                                            <option value="{{$value->id}}" selected>{{$value->name}}</option>
                                            @else
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                            @endif
			                   				@endforeach
						    			</select>
                                        {{$errors->first('industry_id')}}
                                    </div>
                                </div><!--industry-->
                                <div class="form-group col-sm-5">
                                    <label for="mobile">Days to run<span>*</span></label>
                                    <div class="inner-addon">
                                        
                                        <select class="form-control form-control-sm" id="jobcategory" name="day_id">

                                            <option selected disabled>Days to run</option>
                        @foreach (\App\Models\Day::all() as $value)
                                              @if(old('day_id') == $value->id)
                                              <option value="{{$value->id}}" selected>{{$value->name}}</option>
                                              @else
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        {{$errors->first('day_id')}}
                                    </div>
                                </div><!--Mobile Number-->
                                <div class="form-group col-sm-5">
                                    <label for="mobile">Location/City<span>*</span></label>
                                    <div class="inner-addon">
                                        
                                        <select class="form-control form-control-sm" id="city_id" name="city_id">

                                            <option selected disabled>City</option>
                        @foreach (\App\Models\City::all() as $value)
                                              @if(old('city_id') == $value->id)
                                              <option value="{{$value->id}}" selected>{{$value->name}}</option>
                                              @else
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        {{$errors->first('city_id')}}
                                    </div>
                                </div><!--Address-->
                                <div class="form-group col-sm-5">
                                    <label for="email">Address</label>
                                    <div class="inner-addon">
                                        <input type="text" id="address" name="address" class="form-control form-control-sm" placeholder="address" value="{{old('address')}}">
                                        {{$errors->first('address')}}
                                    </div>
                                </div>

                                <div class="form-group col-sm-5">
                                    <label for="email">Skills</label>
                                    <div class="inner-addon">
                                        <input type="text" id="skills" name="skills" class="form-control form-control-sm" placeholder="type skill separated by comma" value="{{old('skills')}}">
                                        {{$errors->first('skills')}}
                                    </div>
                                </div>

                        <div class="form-group col-sm-10">
                                    <div class="inner-addon">
                                        <div class="checkbox pull-left flip">
                                            <label for="remember">By submitting I agree to <a href="{{url('blog/terms-of-use')}}" target="_blank">Merooffer's Terms of Use</a>  and <a href="{{url('blog/privacy-policy')}}" target="_blank">Privacy Policy</a>, <a href="{{url('blog/posting-policy')}}" target="_blank">Posting Policy</a> and I consent to receiving marketing from Merooffer.</label>
                                        </div>
                            <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
							<button type="submit" name="op" value="submit" class="btn btn-primary sharp btn-sm btn-style-one">Submit Now</button>
                        </div>
                    </div>
            </div>
            <!-- Update your Password -->
						</form>
					</div><!--user-ads user-profile-settings-->
				</div><!--user-detail-section-->
			</div><!--col-lg-9-->
		</div><!--row-->
	</div><!--container-->	
</section><!--user-pages section-gray-bg-->

@include('site.partials.footer')
@endsection