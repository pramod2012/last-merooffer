<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('feature')->default(0);
            $table->string('name');
            $table->text('body');
            $table->float('salary')->nullable();
            $table->float('salary_to')->nullable();
            $table->float('salary_avg')->nullable();
            $table->string('education');
            $table->string('vac_no');
            $table->integer('views')->default(0);
            $table->integer('status')->default(0);
            $table->bigInteger('order');
            $table->string('address')->nullable();
            $table->integer('latitude')->nullable();
            $table->integer('longitude')->nullable();
            $table->string('gender')->nullable();
            $table->string('age')->nullable();
            $table->dateTime('expired_at')->nullable();
            $table->bigInteger('experience_id')->unsigned();
            $table->foreign('experience_id')->references('id')->on('experiences')->onDelete('cascade');
            $table->bigInteger('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->bigInteger('day_id')->unsigned();
            $table->foreign('day_id')->references('id')->on('days')->onDelete('cascade');
            $table->bigInteger('jobtype_id')->unsigned()->default(1);
            $table->foreign('jobtype_id')->references('id')->on('jobtypes')->onDelete('cascade');
            $table->bigInteger('salarytype_id')->unsigned()->nullable();
            $table->bigInteger('level_id')->unsigned();
            $table->foreign('level_id')->references('id')->on('levels')->onDelete('cascade');
            $table->bigInteger('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('job_categories')->onDelete('cascade');
            $table->bigInteger('positiontype_id')->unsigned();
            $table->foreign('positiontype_id')->references('id')->on('positiontypes')->onDelete('cascade');
            $table->string('endate');
            $table->bigInteger('industry_id')->unsigned();
            $table->foreign('industry_id')->references('id')->on('industries')->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('slug')->unique();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
