<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\FcategoryContract;
use App\Http\Controllers\BaseController;

class FcategoryController extends BaseController
{
    private $page="admin.fcategory.";

    protected $categoryRepo;

    public function __construct(
        FcategoryContract $fcategoryRepo
    )
    {
        $this->fcategoryRepo = $fcategoryRepo;
    }

    public function index()
    {
        $list = $this->fcategoryRepo->list('order', 'asc');
        $datas = $this->fcategoryRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('Freelancer Categories', 'List of Freelancer Categories');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $fcategories = $this->fcategoryRepo->list();
        $this->setPageTitle('Fcategories', 'Create fcategory');
        return view($this->page.'create',compact('fcategories'));
    }

    public function store(Request $request)
    {
        $data = $this->fcategoryRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('category.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->fcategoryRepo->findById($id);
        $categories = $this->fcategoryRepo->list();
        $this->setPageTitle('Fcategories', 'Edit Fcategory');
        return view($this->page.'edit', compact('data','categories'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->fcategoryRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('category.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->fcategoryRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('category.index', 'Record deleted successfully' ,'success',false, false);
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('datas');
        foreach($allArticles as $_article){
            $article = \App\Models\Fcategory::find($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
