<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Post;

class BlogSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post = new Post();
        $post->name = 'Terms of Use';
        $post->desc = 'Terms of Use';
        $post->body = '<p style="text-align:justify"><span style="color:#000000"><strong>TERMS OF USE</strong>:&nbsp;Effective 12 May 2020, the Services (as defined below) will be provided to you by Skyfall Technologies Pvt. Ltd, the company currently providing the site to you.&nbsp;<br />
Mero Offer Terms of Use<br />
&nbsp; &nbsp; <strong>1. Introduction</strong>. Welcome to <strong>www.merooffer.com</strong>. Thanks for stopping by. These <strong>Terms of Use</strong>, the <strong>Privacy Policy</strong>, and all policies posted on our site set out the terms on which we offer you access to and use of our site, services, applications and tools (collectively &ldquo;Services&rdquo;). You agree to comply with the full Terms of Use when accessing or using our Services.<br />
The Services are currently provided to you by <strong>Skyfall Technologies Pvt. Ltd</strong>.</span></p>

<p style="text-align:justify"><br />
<span style="color:#000000">&nbsp; &nbsp; <strong>2. Your Account</strong>. To access and use some of the Services, you may need to register with us and set up an account with your email address and a password (your &ldquo;Account&rdquo;). The email address you register with will be your email address, and you are solely responsible for maintaining the confidentiality of your password. You are solely responsible for all activities that happen under your Account.<br />
If you believe your Account has been compromised or misused, <strong>contact us</strong> immediately at Mero Offer Customer Support.</span></p>

<p style="text-align:justify"><br />
<span style="color:#000000">&nbsp; &nbsp; <strong>3. Using Mero Offer</strong>.&nbsp;To use the Services, you must be over 18 years old. You agree that you will only post in relation to goods or services in Nepal in the appropriate category or area and you agree that you will not do any of the following bad things:<br />
&nbsp; &nbsp; &bull; violate any laws or the <strong>Posting Rules</strong>;<br />
&nbsp; &nbsp; &bull; post any threatening, abusive, defamatory, obscene or indecent material;<br />
&nbsp; &nbsp; &bull; be false or misleading;<br />
&nbsp; &nbsp; &bull; infringe any third-party right;<br />
&nbsp; &nbsp; &bull; distribute or send communications that contain spam, chain letters, or pyramid schemes;<br />
&nbsp; &nbsp; &bull; distribute viruses or any other technologies that may harm Mero Offer, the Services or the interests or property of Mero Offer users;<br />
&nbsp; &nbsp; &bull; impose an unreasonable load on our infrastructure or interfere with the proper working of the Services;<br />
&nbsp; &nbsp; &bull; copy, modify, or distribute any other person&rsquo;s content without their consent;<br />
&nbsp; &nbsp; &bull; use any robot spider, scraper or other automated means to access the Services and collect content for any purpose without our express written permission;<br />
&nbsp; &nbsp; &bull; harvest or otherwise collect information about others, including email addresses, without their consent<br />
&nbsp; &nbsp; &bull; bypass measures used to prevent or restrict access to the Services; and/or<br />
&nbsp; &nbsp; &bull; post an ad with the intention to profit from a natural disaster, health or public safety concern, or tragic event.</span></p>

<p style="text-align:justify"><br />
<span style="color:#000000">&nbsp; &nbsp; <strong>4. Abusing Mero Offer Services</strong>. Mero Offer and the Mero Offer community work together to keep the Services working properly and the community safe. Please report problems, offensive content and policy breaches to us using the reporting system. You are solely responsible for all information that you give to Mero Offer and any consequences that may result from your posts. We can at our discretion refuse, delete or take down content that we think is inappropriate or breaching these Terms of Use. We also can at our discretion restrict a user&rsquo;s usage of the Services either temporarily or permanently, or refuse a user&rsquo;s registration. Without limiting other remedies, we may issue warnings, limit or terminate our Services, remove hosted content and take technical and legal steps to keep users off the Services if we think that they are creating problems or acting inconsistently with the letter or spirit of our policies. However, whether we take any of these steps, we don&rsquo;t accept any liability for monitoring the Services or for unauthorized or unlawful content on the Services or use of the Services by users. You also accept that Mero Offer is not under any obligation to monitor any data or content which is submitted to or available on the Services.&nbsp;</span></p>

<p style="text-align:justify"><br />
<span style="color:#000000">&nbsp; &nbsp;<strong> 5. Fees and Services</strong>.&nbsp;Using the Services is generally free. We may sometimes charge a fee for certain features or Services. If the feature you use incurs a fee, you will be able to review and accept that charge before purchase. Our fees are quoted in Nepali Rupees, and we may sometimes change them. We&rsquo;ll notify you of changes to our fees by posting the changes on the site. We may sometimes temporarily change our fees for testing purposes, promotional events or new features, these changes take effect from the time the price change is posted to the site. Our fees are non-refundable after the feature is supplied, and you are responsible for paying them when they are due. If you don&rsquo;t, we may limit your ability to use the Services.</span></p>

<p style="text-align:justify"><br />
<span style="color:#000000">&nbsp; &nbsp; <strong>6. Content</strong>.&nbsp;Mero Offer&rsquo;s Services contain content from us, you, and other users. Mero Offer is protected by copyright laws of Nepal. You agree not to copy, distribute the Services or modify content from the Services, our trademarks or copyrights without our express written consent. You may not disassemble or decompile, reverse engineer or otherwise attempt to discover any source code contained in the Services. Without limiting the foregoing, you agree not to reproduce, copy, sell, resell, or exploit for any purposes any aspect of the Services (other than your own content). When you give us content, including pictures, you grant us and represent that you have the right to grant us, a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, sub-licensable (through multiple tiers) right to exercise any and all copyright, publicity, trademarks, design, database and intellectual property rights to that content, in any media whether now known or to be discovered in the future, including third party sites and applications. You also waive all moral rights you have in the content to the fullest extent permitted by law. We reserve the right to remove content where we have grounds for suspecting the violation of these terms or the rights of any other party.</span></p>

<p style="text-align:justify"><br />
<span style="color:#000000">&nbsp; &nbsp; <strong>7. Intellectual Property Infringements .</strong>&nbsp;Do not post content that infringes the rights of third parties. This includes, but is not limited to, content that infringes on intellectual property rights such as copyright and trademark (e.g. offering counterfeit items for sale). We can remove content where we have grounds for suspecting the violation of these terms, our policies or of any party&rsquo;s rights.&nbsp;</span></p>

<p style="text-align:justify"><br />
<span style="color:#000000">&nbsp; &nbsp; <strong>8. Limitation of Liability.</strong> &nbsp;Nothing in these Terms of Use excludes, restricts or modifies any rights or statutory guarantees that you may have under applicable laws that cannot be excluded, restricted or modified, including any such rights or statutory guarantees under the Nepal Consumer Law and Act. To the extent that these Terms of Use are found to exclude, restrict or modify any such rights or statutory guarantees, those rights and/or statutory guarantees prevail to the extent of the inconsistency. &nbsp;Services are provided &ldquo;as is&rdquo; and &ldquo;as available&rdquo;. You agree not to hold us responsible for things other users post or do. As most of the content on the Services comes from other users, we do not guarantee the accuracy of postings or user communications or the quality, safety, or legality of what is offered. We also cannot guarantee continuous or secure access to our Services. While we will use reasonable efforts to maintain an uninterrupted service, we cannot guarantee this and, to the extent permitted by law, we do not give any promises or warranties (whether express or implied) about the availability of our Services or that the Services will be uninterrupted or error-free. Notification functionality in the Services may not occur in real time. That functionality is subject to delays beyond our control, including without limitation, delays or latency due to your physical location or your wireless data service provider&rsquo;s network. &nbsp;To the extent permitted by law, we are not liable for the posting of any unlawful, threatening, abusive, defamatory, obscene or indecent information, or material of any kind by a user of the Service which violates or infringes upon your rights, including without limitation any transmissions constituting or encouraging conduct that would constitute a criminal offense, give rise to civil liability or otherwise violate any applicable law. To the extent permitted by law, and without limiting any rights that you may have under the Nepal Law, Mero Offer&rsquo;s liability to you for any failure by Mero Offer to comply with any statutory guarantee under the Nepal&rsquo;s Law is limited to Mero Offer supplying the Services again or paying you the cost of having the Services supplied again.Mero Offer excludes any liability to you for any loss or damage suffered by you as a result of Mero Offer failing to comply with an applicable statutory guarantee under the Nepal&rsquo;s Law if you suffering such loss or damage was not reasonably foreseeable and was not directly caused by Mero Offer.<br />
&nbsp;</span></p>

<p style="text-align:justify"><span style="color:#000000">&nbsp; <strong>9. Indemnification.</strong>&nbsp;You will indemnify and hold harmless Mero Offer and our affiliates and our and their respective officers, directors, agents and employees (each an &ldquo;Indemnified Party&rdquo;), from any claim made by any third party, together with any amounts payable to the third party whether in settlement or as may otherwise be awarded, and reasonable legal costs incurred by any of the Indemnified Parties, arising from or relating to your use of the Services, any alleged violation by you of the applicable terms, and any alleged violation by you of any applicable law or regulation. We reserve the right, at our own expense, to assume the exclusive defence and control of any matter subject to indemnification by you, but doing so will not excuse your indemnity obligations. &nbsp;</span></p>

<p style="text-align:justify"><br />
<span style="color:#000000">&nbsp; &nbsp; <strong>10. Release.</strong>&nbsp;If you have a dispute with one or more Mero Offer users, you release us (and our officers, directors, agents, subsidiaries, joint ventures and employees) from any and all claims, demands and damages (actual and consequential) of every kind and nature, known or unknown, arising out of or in any way connected with such disputes. &nbsp;</span></p>

<p style="text-align:justify"><br />
<span style="color:#000000">&nbsp; &nbsp; <strong>11. Personal Information.</strong>&nbsp;By using the Services, you agree to the collection, transfer, storage and use of your personal information by us (the &ldquo;data controller&rdquo;) on servers located in the United States and in the European Union as further described in our privacy policy. &nbsp;</span></p>

<p style="text-align:justify"><br />
<span style="color:#000000">&nbsp; &nbsp; <strong>12. Severability.</strong>&nbsp;&nbsp;If a provision of these Terms of Use is illegal or unenforceable in any relevant jurisdiction, it may be severed for the purposes of that jurisdiction without affecting the enforceability of the other provisions of these Terms of Use. &nbsp;</span></p>

<p style="text-align:justify"><br />
<span style="color:#000000">&nbsp; &nbsp; <strong>13. General.</strong>&nbsp;These Terms of Use and the other policies posted on the Services set out the entire agreement between Mero Offer and you, overriding any prior agreements.</span><br />
&nbsp;</p>';
				$post->mcategory_id = 1;
				$post->status = 1;
				$post->is_blog = 0;
				$post->feature = 1;
				$post->image = 'posts/terms.jpg';
				$post->author_id = 1;
				$post->date = '2020/05/11';
				$post->save();


				$post = new Post();
        		$post->name = 'Posting Policy';
        		$post->desc = 'Posting Policy';
        		$post->body = '<p style="text-align:justify"><strong>General Mero Offer Posting Policies</strong><br />
As a condition of use of <strong>Mero Offer</strong> you agree that you are at least 18 years of age.<br />
At Mero Offer we want to make sure that the site is as clean, friendly and usable as possible for everyone. Ads that fall outside the posting rules stated in our Help sections or our <strong>Terms of Use</strong> may be removed from the site.<br />
You are solely responsible for all information that you submit to Mero Offer and any consequences that may result from your post. We reserve the right at our discretion to refuse or delete content that we believe is inappropriate or breaching our <strong>Terms of Use</strong>. We also reserve the right at our discretion to restrict a user&#39;s usage of the site either temporarily or permanently, or refuse a user&#39;s registration.</p>

<p style="text-align:justify"><strong>General reasons for ads being deleted are:</strong><br />
&nbsp; &nbsp; 1. Ad breaches Mero Offer <strong>Posting Policies</strong><br />
&nbsp; &nbsp; 2. Breaches of Nepal&rsquo;s law. It is the responsibility of the advertiser before posting an ad on Mero Offer to ensure that content advertised adheres to Mero Offer <strong>posting policies</strong> as well as Nepal&rsquo;s applicable laws. As a condition of your use of Mero Offer specified under our <strong>Terms of Use</strong>, you agree that you will not violate any laws<br />
&nbsp; &nbsp; 3. &nbsp;All the item being offered for sale is not allowed to be sold on Mero Offer.&nbsp;&nbsp;See <strong>&ldquo;What is not allowed in Mero Offer? &ldquo;</strong> page to see what can&#39;t be posted for sale on Mero Offer.<br />
&nbsp; &nbsp; 4. Including information in your ad which Mero Offer believes is designed to manipulate search, including keyword stuffing and adding tags to your ad.&nbsp;<br />
&nbsp; &nbsp; 5. The ad is a duplicate of another ad previously posted.<br />
&nbsp; &nbsp; 6. Posted under wrong category (You must choose the single most relevant category for your ad)<br />
&nbsp; &nbsp; 7. Ads posted in a language other than English or Nepali. We only accept ads in English or Nepali. It is better to post in english language for better search results.<br />
&nbsp; &nbsp; 8. Ads posted from overseas or from behind a VPN unless the ad is posted in anticipation of you being in Nepali (eg. to find a place to live or a job while you are here). Mero Offer is for Nepali based individuals and businesses only.<br />
&nbsp; &nbsp; 9. Ad contains external links: No external website links are allowed within your ad to other property / job / classified or auction sites<br />
&nbsp; &nbsp; 10. Not descriptive enough: Ads that do not provide enough detail will be placed on hold or removed as this makes for a bad browsing experience<br />
&nbsp; &nbsp; 11. Inappropriate language<br />
&nbsp; &nbsp; 12. Inappropriate photo / image<br />
&nbsp; &nbsp; 13. Discriminatory on race / religion / nationality / gender / etc<br />
&nbsp; &nbsp; 14. Ads that report other fraudulent ads. Please report potentially fraudulent ads via the &quot;report ad&quot; option located within each ad or Contact Us with ad details (ad id, email address) and reasons why these ads should be reviewed<br />
&nbsp; &nbsp; 15. Ads that are intended to profit off natural disasters, health or public safety concerns, or tragic events<br />
Mero Offer reserves the right to remove any ad that we feel is not relevant, or of value to the Mero Offer community, with or without notice to the ad poster.<br />
There are several ways that your ad may be found to be in breach of policy and removed from the site including:</p>

<p style="text-align:justify">&nbsp; &nbsp; <strong>&bull; Your ad has been reported to us.</strong><br />
When this happens your ad may be temporarily suspended until we review it.<br />
We check reported ads as quickly as we can. If we conclude that the ad hasn&#39;t broken any <strong>Posting Policies</strong> or <strong>Terms of Use</strong> we will activate the ad again promptly. Check back after a few hours to see if this is the case.</p>

<p style="text-align:justify">&nbsp; &nbsp;<strong> &bull; Your ad has been removed by our moderation tools</strong><br />
Ads identified by our automated tools as inappropriate, that we then find do break our <strong>Posting Policies</strong> or <strong>Terms of Use</strong> will be removed from the site.<br />
In most cases we email you to let you know when we have had to remove your ad. These emails sometimes get queued and not delivered or sometimes directed to junk folders so please look out for Mero Offer emails. You may wish to add Mero Offer to your safe senders list if you have one.<br />
If you have checked out all of these possibilities and none of them apply to your ad then please let us know and we&#39;ll be happy to help you out.<br />
&nbsp;</p>';
				$post->mcategory_id = 1;
				$post->status = 1;
				$post->is_blog = 0;
				$post->feature = 1;
				$post->image = 'posts/posting_policy.jpg';
				$post->author_id = 1;
				$post->date = '2020/05/11';
				$post->save();

				$post = new Post();
        		$post->name = 'Privacy Policy';
        		$post->desc = 'Privacy Policy';
        		$post->body = '<p style="text-align:justify"><strong>Privacy Policy</strong></p>

<p style="text-align:justify"><strong>Skyfall Technologies Pvt. Ltd.</strong> is the operator of merooffer.com (&ldquo;the Site&rdquo;). This Privacy Notice describes:<br />
&nbsp; &nbsp; &bull; the personal information we collect and how we use that information<br />
&nbsp; &nbsp; &bull; when we might disclose your personal information; and<br />
&nbsp; &nbsp; &bull; how we keep and protect your personal information.</p>

<p style="text-align:justify">The Privacy Notice applies to this Site and to any applications, services or tools (collectively &ldquo;Services&rdquo;) where this Privacy Notice is referenced. By using our Services and/or registering for an account, you are accepting the terms of this Privacy Notice and our Terms of Use</p>

<p style="text-align:justify"><strong>We collect information you give us including:</strong></p>

<p style="text-align:justify">&nbsp; &nbsp; <strong>&bull; When you register for an account:</strong> Information such as your name, addresses, telephone numbers, email addresses or user ID (where applicable) when you register for an account with us<br />
&nbsp; &nbsp; <strong>&bull; When we verify you or your account:</strong> we may collect and process information (as permitted by law) to authenticate you or your account, or to verify the information that you provided to us<br />
&nbsp; &nbsp;&bull;<strong> When you transact on or use our Services:</strong> such as when you post an ad, reply to an ad, communicate with us or other users, information you provide for the Services that you use or during a transaction or other transaction-based content. We may also collect your financial information (such as credit card or bank account numbers) if you buy a feature from us or are required to pay fees to us<br />
&nbsp; &nbsp; <strong>&bull; When you engage with our community:</strong> such as when you submit a web form or participate in community discussions or chats<br />
&nbsp; &nbsp; <strong>&bull; When you interact with your account:</strong> such as updating or adding information to your account, adding items to alerts lists and saving searches. Sometimes you may also give us your age, gender, interests and favourites<br />
&nbsp; &nbsp;<strong> &bull; When you contact us:</strong> such as through a web form, chat or dispute resolution or when we otherwise communicate with each other. We may also record our calls with you (if we have your consent to do so)</p>

<p style="text-align:justify"><br />
Your <strong>resume</strong> if you choose to submit it to advertisers on our site for consideration We collect information automatically including:<br />
&nbsp; &nbsp; &bull; Information from the devices you use when interacting with us or our Services such as device ID or unique user ID, device type, ID for advertising and unique device token<br />
&nbsp; &nbsp; &bull; Information about your location such as geo-location<br />
&nbsp; &nbsp; &bull; Computer and connection information such as statistics on your page views, traffic to and from the sites, referral URL, ad data, your IP address, your browsing history and your web log information</p>

<p style="text-align:justify"><strong>We collect information using cookies and similar technologies including:</strong><br />
&nbsp; &nbsp; &bull; Information about the pages you view, the links you click and other actions you take on our Services, or within our advertising or email content.&nbsp;</p>

<p style="text-align:justify"><strong>We use your personal information to provide, improve and personalise our Services. </strong></p>

<p style="text-align:justify">Your personal information allows us to:<br />
&nbsp; &nbsp; &bull; Provide you with access to and use of our Services as well as access to your history, internal messages and other features we may provide<br />
&nbsp; &nbsp; &bull; Offer you site content that includes items and services that you may like<br />
&nbsp; &nbsp; &bull; Provide you with credit offers and opportunities on behalf of other members of our corporate family and their financial institution partners. However, we don&rsquo;t share financial information without your explicit consent<br />
&nbsp; &nbsp; &bull; Customise, measure and improve our Services<br />
&nbsp; &nbsp; &bull; Provide other services requested by you as described when we collect the information<br />
&nbsp; &nbsp; &bull; To provide you with location-based services (such as advertising, search results and other personalised content)</p>

<p style="text-align:justify"><strong>Cookie Policy:</strong></p>

<p style="text-align:justify">When you visit or interact with our sites, services, applications, tools or messaging, we or our authorized service providers may use cookies, web beacons, and other similar technologies for storing information to help provide you with a better, faster, and safer experience and for advertising purposes.<br />
This page is designed to help you understand more about these technologies and our use of them on our sites and in our services, applications, and tools. Below is a summary of a few key things you should know about our use of such technologies.&nbsp;<br />
Our cookies and similar technologies have different functions. They are either necessary for the functioning of our services, help us improve our performance, give you extra functionalities, or help us to serve you relevant and targeted ads. We use cookies and similar technologies that only remain on your device for as long as you keep your browser active (session) and cookies and similar technologies that remain on your device for a longer period (persistent).You are free to block, delete, or disable these technologies if your device permits so. You can manage your cookies and your cookie preferences in your browser or device settings.</p>

<p style="text-align:justify"><strong>Skyfall Technologies Pvt. Ltd.</strong>, is responsible for the collection, use, disclosure, retention and protection of your personal information and third party services you use under the Nepal&rsquo;s National laws. We acknowledge that we do not sell or rent your personal information which is against the Nepal&rsquo;s Law.</p>';
				$post->mcategory_id = 1;
				$post->status = 1;
				$post->is_blog = 0;
				$post->feature = 1;
				$post->image = 'posts/privacy-policy.jpg';
				$post->author_id = 1;
				$post->date = '2020/05/11';
				$post->save();

				$post = new Post();
        		$post->name = 'Safety Tips';
        		$post->desc = 'Safety Tips';
        		$post->body = '<p><strong>Mero Offer&rsquo;s Safety Guidelines</strong><br />
At&nbsp;Mero Offer&nbsp;we want to make sure all our users have a safe, successful and hassle-free experience. Whether you&rsquo;re buying or selling, make sure you follow our safety guidelines.</p>

<p><br />
1.&nbsp;When buying or selling, you should always meet in-person to see the item and exchange money. If possible, take a friend or family member with you, or make sure there is someone else at your home or workplace.</p>

<p><br />
2.&nbsp;Never send money to anyone you don&rsquo;t know. Mero Offer does not allow users to transfer money via the website, and advocates face-to-face transactions. This includes mailing a cheque or using payment services like E-sewa, Western Union or Money Gram to pay for items found on Mero offer.</p>

<p><br />
3.&nbsp;If you receive an SMS message requesting you reply via email please ignore it! This is most likely an attempted fraud. You should only trade with local buyers you can meet in person.</p>

<p><br />
4.Mero Offer doesn&rsquo;t offer any sort of buyer protection or payment programs. Any emails you receive that talk about such systems should be ignored, even if they may have the Mero Offer logo. If you receive any emails promoting these services, please report it to us.</p>

<p><br />
5.&nbsp;Always use common sense &ndash; If it looks too good to be true it probably is!</p>

<p><br />
If you see any ad you are concerned about please report it to us by using the &lsquo;Report Ad&rsquo; function, which is clearly displayed on every ad published on the website.<br />
&nbsp;</p>';
				$post->mcategory_id = 1;
				$post->status = 1;
				$post->is_blog = 0;
				$post->feature = 1;
				$post->image = 'posts/safety_tips.jpg';
				$post->author_id = 1;
				$post->date = '2020/05/11';
				$post->save();

				$post = new Post();
        		$post->name = 'What is not allowed in Mero Offer';
        		$post->desc = 'Not allowed in Mero Offer';
        		$post->body = '<p><strong>The list below details what can&#39;t be posted on Mero Offer in either offering or wanted ads:</strong></p>

<p>&nbsp; &nbsp; 1. Material that infringes copyright, including but not limited to software or other digital goods which you are not authorized to sell<br />
&nbsp; &nbsp; 2. Escort services that offer or indicate sexual services<br />
&nbsp; &nbsp; 3. Alcohol, E-cigarette and Tobacco Products<br />
&nbsp; &nbsp; 4. Identity Documents, Personal Financial Records &amp; Personal Information<br />
&nbsp; &nbsp; 5. Blood, Bodily Fluids and Body Parts<br />
&nbsp; &nbsp; 6. Burglary Tools<br />
&nbsp; &nbsp; 7. Counterfeit Products, replicas or knock-off brand name goods<br />
&nbsp; &nbsp; 8. Government and Transit Badges, Uniforms, IDs, Documents and Licenses<br />
&nbsp; &nbsp; 9. Illegal Goods/Embargoed Goods/Contraband goods<br />
&nbsp; &nbsp; 10. Endangered or protected species, or any part of any endangered or protected species<br />
&nbsp; &nbsp; 11. Fireworks, Destructive Devices and Explosives<br />
&nbsp; &nbsp; 12. Hazardous Materials including but not limited to radioactive, toxic and explosive materials<br />
&nbsp; &nbsp; 13. Illegal Drugs, controlled substances, substances and items used to manufacture controlled substances and drugs, &amp; drug paraphernalia<br />
&nbsp; &nbsp; 14. Illegal items and services<br />
&nbsp; &nbsp; 15. Illegal telecommunication and electronics equipment such as access cards, password sniffers, radar scanners, traffic signal control devices or cable descrambler<br />
&nbsp; &nbsp; 16. Items issued to any Armed Force that have not been disposed of in accordance with that country&#39;s demilitarization policies<br />
&nbsp; &nbsp; 17. Items which encourage or facilitate illegal activity<br />
&nbsp; &nbsp; 18. Lottery Tickets, Sweepstakes Entries and Slot Machines<br />
&nbsp; &nbsp; 19. Material that is obscene, pornographic, adult in nature, or harmful to minors<br />
&nbsp; &nbsp; 20. New merchandise or services from network marketing companies, work-from-home, independent franchisees or distributors, or similar representatives<br />
&nbsp; &nbsp; 21. Personal information or mailing lists. We do not accept the sale of bulk email, Internet Protocol (IP), Instant Messenger (IM), or mailing lists that contain names, addresses, phone numbers, or other personal identifying information. Any tools or software designed predominantly to send unsolicited commercial messages (UCE or &quot;spam&quot;) are also not permitted.<br />
&nbsp; &nbsp; 22. Selling or offering services for supplements/medicine general or pharmaceutical<br />
&nbsp; &nbsp; 23. Nonprescription drugs, drugs that make false or misleading treatment claims, or treatment claims that require therapeutic goods administration Nepal&rsquo;s Government approval<br />
&nbsp; &nbsp; 24. Commercial tanning units or commercial tanning services wanted or offered<br />
&nbsp; &nbsp; 25. Offensive material- examples include ethnically or racially offensive material<br />
&nbsp; &nbsp; 26. Pesticides or hazardous materials<br />
&nbsp; &nbsp; 27. Pictures or images that contain nudity<br />
&nbsp; &nbsp; 28. Plants and insects that are restricted or regulated<br />
&nbsp; &nbsp; 29. Prescription drugs and devices<br />
&nbsp; &nbsp; 30. Prostitution or ads that offer sex, sexual favors or sexual actions in exchange for anything<br />
&nbsp; &nbsp; 31. Recalled items, banned products or products&nbsp;<br />
&nbsp; &nbsp; 32. Stocks and other securities including Bitcoins and related mining equipment<br />
&nbsp; &nbsp; 33. Stolen property<br />
&nbsp; &nbsp; 34. Tobacco products and related items, including e-cigarettes<br />
&nbsp; &nbsp; 35. Used cosmetics<br />
&nbsp; &nbsp; 36. Used or rebuilt batteries or batteries containing mercury<br />
&nbsp; &nbsp; 37. We do not accept ads selling body parts/bodily fluids, adoption or surrogacy anywhere on the site<br />
&nbsp; &nbsp; 38. Weapons and related items (including, but not limited to firearms, firearm accessories, parts and magazines, ammunition, paintball guns, gel blaster guns, BB and pellet guns, spearguns, tear gas, tasers, stun guns, switchblade knives, martial arts weapons, archery and/or bow and arrow equipment)<br />
&nbsp; &nbsp; 39. Ivory, rhino horn or any animal parts or&nbsp;hunting trophies&nbsp;<br />
&nbsp; &nbsp; 40. Votes in elections administered&nbsp;<br />
&nbsp; &nbsp; 41. Census or other survey papers issued by the Nepal&rsquo;s Bureau of Statistic<br />
&nbsp; &nbsp; 42. Education certificates(fake) for High school diplomas, university, medals, etc.<br />
&nbsp; &nbsp; 43. All ads for COVID-19 associated items, including:<br />
&nbsp; &nbsp; &nbsp; &nbsp; a. Surgical and respiratory masks<br />
&nbsp; &nbsp; &nbsp; &nbsp; b. Hand sanitisers and gels<br />
&nbsp; &nbsp; &nbsp; &nbsp; c. Toilet paper<br />
&nbsp; &nbsp; &nbsp; &nbsp; d. Disinfectant wipes&nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; e. Medical protection disposable gowns and gloves</p>

<p>It is the responsibility of the advertiser before posting an ad on Mero Offer that content advertised adheres to Mero Offer Posting Policies as well as applicable laws. As a condition of your use of Mero Offer specified under our Terms of Use, you agree that you will not violate any laws of Nepal&rsquo;s Government.</p>';
				$post->mcategory_id = 1;
				$post->status = 1;
				$post->is_blog = 0;
				$post->feature = 1;
				$post->image = 'posts/not_allowed.jpg';
				$post->author_id = 1;
				$post->date = '2020/05/11';
				$post->save();
    }
}
