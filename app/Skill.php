<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
	protected $fillable = ['skill','slug'];

	public function setNameAttribute($value)
    {
        $this->attributes['skill'] = $value;

        if (static::whereSlug($slug = str_slug($value))->exists())
        {
            $slug = $this->incrementSlug($slug);
        }

        $this->attributes['slug'] = $slug;
    }

    public function incrementSlug($slug)
    {
        $original = $slug;
        $count = 2;
        while (static::whereSlug($slug)->exists()) 
        {
           $slug = $count++ . "-{$original}";
        }
        return $slug;
    }

    function users() {
        return $this->belongsToMany('App\Models\User');
    }

    function fjobs() {
        return $this->belongsToMany('App\Models\Freelancers\Fjob');
    }
}
