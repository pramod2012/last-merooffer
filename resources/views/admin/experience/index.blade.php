@extends('admin.experience.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          <h3 class="box-title">List of {{$pageTitle}}</h3>
        </div>
        <div class="col-sm-4">
          @include('admin.partials.flash')
          <a class="btn btn-primary" href="{{ route('experience.create') }}">Add New Experience</a>
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>
      <form method="POST" action="#">
         
      </form>
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                <th width="3%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">ID</th>
                <th width="10%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">Slug</th>
                <th width="10%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">Name</th>
                <th width="3%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">Status</th>
                <th width="12%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">Created AT</th>
                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="2" aria-label="Action: activate to sort column ascending">Action</th>
              </tr>
            </thead>
            <tbody>
@foreach($datas as $value)
                <tr role="row" class="odd">
                  <td class="sorting_1"> {{$value->id}}</td>
                  <td class="hidden-xs">{{$value->slug}}</td>
                  <td class="hidden-xs">{{$value->name}}</td>
                  <td class="sorting_1">
                    <i class="{{$value->status == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                  </td>
                  <td class="sorting_1">{{$value->created_at}}</td>
                  <td>
                    <form class="row" method="POST" action="{{ route('experience.destroy',$value->id) }}" onsubmit = "return confirm('Are you sure?')">
                        {{ csrf_field() }}
                      {{ method_field('DELETE')}}
                        <a href="{{ route('experience.edit', $value->id) }}" class="btn btn-warning col-sm-3 col-xs-5 btn-margin">
                        Update
                        </a>
                         <button type="submit" class="btn btn-danger col-sm-3 col-xs-5 btn-margin">
                          Delete
                        </button>
                    </form>
                  </td>
              </tr>
           @endforeach
            </tbody>
        
          </table>
        </div>
      </div>

      
    </div>
  
  </div>
   
</div>
    </section>
    
  </div>
@endsection