@extends('admin.user.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update new user</div>
                <div class="panel-body">
                    @include('admin.partials.flash')
                    <form class="form-horizontal" role="form" method="POST" action="{{route('user.update',$user->id)}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{ method_field('PATCH') }}
                        <div class="form-group">
                            <label for="firstname" class="col-md-4 control-label">Name*</label>
                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control" name="name" value="{{ $user->name}}">
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('name') }}
                                        </strong>
                                    </span>
                        
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">E-Mail Address*</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email}}">
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('email') }}
                                        </strong>
                                    </span>
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="mobile" class="col-md-4 control-label">Mobile Number*</label>

                            <div class="col-md-6">
                                <input id="mobile" type="text" class="form-control" name="mobile" value="{{ $user->mobile}}">
                            
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('mobile') }}
                                        </strong>
                                    </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="firstname" class="col-md-4 control-label">Address</label>
                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control" name="address" value="{{ $user->address}}">
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('address') }}
                                        </strong>
                                    </span>
                        
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">Phone Number</label>
                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ $user->phone}}">
                            
                            </div>
                        </div>

                        
                        <div class="form-group">
                            <label for="p" class="col-md-4 control-label">Role</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="role" value="{{ $user->role}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="role" class="col-md-4 control-label">Email Verified</label>
                            <div class="col-md-6">
                                <input id="email_verified_at" type="text" class="form-control" name="email_verified_at" value="{{ old('email_verified_at',$user->email_verified_at) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="image" class="col-md-4 control-label">Image</label>
                            <div class="col-md-6">
                                <input type="file" name="image" class="form-control">
                                <img src="{{asset('images_small/'.$user->image)}}" height="70">
                            </div>
                        </div>



          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-4 control-label">
                      <input type="hidden" name="cell_activated" value="0">
                      <input name="cell_activated" type="checkbox" value="1" {{$user->cell_activated?"checked":""}}>
                      Cell Activated
                    </label>
                  </div>
              </div>
          </div>

          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-4 control-label">
                      <input type="hidden" name="show_mobile" value="0">
                      <input name="show_mobile" type="checkbox" value="1"  {{$user->show_mobile?"checked":""}}>
                      Show Mobile
                    </label>
                  </div>
              </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-4 control-label">
                      <input type="hidden" name="show_email" value="0">
                      <input name="show_email" value="1" type="checkbox"  {{$user->show_email?"checked":""}}>
                      Show Email
                    </label>
                  </div>
              </div>
          </div><div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-4 control-label">
                      <input type="hidden" name="show_phone" value="0">
                      <input name="show_phone" value="1" type="checkbox" {{$user->show_phone?"checked":""}}>
                      Show Phone
                    </label>
                  </div>
              </div>
          </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
