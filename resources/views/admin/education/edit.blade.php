@extends('admin.education.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('education.update', $data->id) }}" enctype="multipart/form-data">
                       {{csrf_field()}}
                       {{method_field('PATCH')}}
                        <div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
                            <label for="course" class="col-md-4 control-label">course</label>

                            <div class="col-md-6">
                                <input id="course" type="text" class="form-control" name="course" value="{{ old('course',$data->course) }}">

                                @if ($errors->has('course'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('course') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="school" class="col-md-4 control-label">School</label>

                            <div class="col-md-6">
                                <textarea rows="4" id="school" type="text" class="form-control" name="school">{{ old('school', $data->school) }}</textarea>
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('school') }}
                                        </strong>
                                    </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="year" class="col-md-4 control-label">Year</label>
                            <div class="col-md-6">
                                <input id="year" type="text" class="form-control" name="year" value="{{ old('year', $data->year) }}">
                                <span class="text-danger">{{ $errors->first('year') }}</span>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="achievement" class="col-md-4 control-label">achievement</label>
                            <div class="col-md-6">
                                <input id="achievement" type="text" class="form-control" name="achievement" value="{{ old('achievement', $data->achievement) }}">
                                <span class="text-danger">{{ $errors->first('achievement') }}</span>
                            </div>
                        </div>

                        

                    <div class="form-group">
                      <label class="col-md-4 control-label">User ID</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="user_id">
                              <option value="{{$data->user_id}}">{{$data->user->name}}</option>
                            </select>
                                <span class="text-danger">{{ $errors->first('user_id') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
