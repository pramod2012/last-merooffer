<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\PricetypeContract;
use App\Http\Controllers\BaseController;

class PricetypeController extends BaseController
{
    private $page="admin.pricetype.";

    protected $pricetypeRepo;

    public function __construct(
        PricetypeContract $pricetypeRepo
    )
    {
        $this->pricetypeRepo = $pricetypeRepo;
    }

    public function index()
    {
        $list = $this->pricetypeRepo->list('created_at', 'desc');
        $datas = $this->pricetypeRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('Pricetype', 'List of Pricetype');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('Pricetype', 'Create pricetype');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->pricetypeRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('pricetype.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->pricetypeRepo->findById($id);
        $this->setPageTitle('Pricetype', 'Edit pricetype');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->pricetypeRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('pricetype.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->pricetypeRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('pricetype.index', 'Record deleted successfully' ,'success',false, false);
    }
}
