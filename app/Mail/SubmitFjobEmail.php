<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Freelancers\Fjob;

class SubmitFjobEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $fjob;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Fjob $fjob)
    {
        $this->fjob = $fjob;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@merooffer.com','Mero Offer')
                    ->subject('Submit Project Email')
                    ->view('emails.submitFjobEmail')
                    ->with([
                        'user' => $this->fjob->user->name,
                        'title' => $this->fjob->name,
                        'slug' => $this->fjob->slug,
                    ]);
    }
}
