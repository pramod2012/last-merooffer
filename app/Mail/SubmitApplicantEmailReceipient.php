<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Applicant;

class SubmitApplicantEmailReceipient extends Mailable
{
    use Queueable, SerializesModels;
    protected $applicant;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Applicant $applicant)
    {
        $this->applicant = $applicant;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@merooffer.com')
                    ->subject('Submit Applicant Email Receipient')
                    ->view('emails.submitApplicantEmailReceipient')
                    ->with([
                        'application_letter' => $this->applicant->application_letter,
                        'send_by' => $this->applicant->user->name,
                        'send_to' => $this->applicant->job->user->name,
                        'send_by_email' => $this->applicant->user->email,
                        'send_to_email' => $this->applicant->job->user->email,
                    ]);
    }
}
