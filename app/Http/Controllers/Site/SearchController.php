<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExtendedUpdateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Category;
use App\Models\City;
use App\Models\Day;
use App\Models\Product;
use App\Models\ProductImage;
use App\Repo\CategoryRepo;
use App\Repo\ProductRepo;
use App\Salarytype;
use App\Shop\Products\Exceptions\NumberAlreadyVerifiedException;
use App\Traits\UploadAble;
use App\Utils\ExceptionJsonResponse;
use App\Utils\SuccessJsonResponse;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class SearchController extends Controller
{
    use UploadAble;
    protected $CategoryRepo;
    protected $productRepo;

    public function __construct(CategoryRepo $categoryRepo, ProductRepo $productRepo)
    {
        $this->CategoryRepo = $categoryRepo;
        $this->productRepo = $productRepo;
    }

    public function getCategoriesForSearch(Request $request)
    {

        $categories = \App\Models\Category::where('status', 1)->orderBy('order', 'asc')->where('parent_id', null)->with([
            'children', 'pricelabels', 'attributes.attributevalues', 'attributes.amenities',
            'children.pricelabels', 'children.attributes.attributevalues', 'children.attributes.amenities'
        ])->get();
        return response()->json([
            'status' => 1,
            'message' => 'success',
            'data' => [
                'categories' => $categories
            ]
        ]);
    }
    public function getAttributesOfCategory(Request $request)
    {
        try {
            $cat = Category::with(['attributes.attributevalues'])->find($request->category_id);
            $dropdown = $cat->attributes->where('filterable', 1)->where('type', 'dropdown');
            $radio = $cat->attributes->where('filterable', 1)->where('type', 'radio');
            $nonInterval = $cat->attributes->where('filterable', 1)->where('type', 'number')->where('interval', 0);
            $text = $cat->attributes->where('filterable', 1)->where('type', 'text');
            $interval = $cat->attributes->where('filterable', 1)->where('type', 'number')->where('interval', 1);
            return response()->json([
                'status' => 1,
                'message' => 'success',
                'data' => [
                    'dropdown' => $dropdown,
                    'radio' => $radio,
                    'nonInterval' => $nonInterval,
                    'interval' => $interval,
                    'cities' => \App\Models\City::all(),
                    'conditions' => \App\Models\Condition::all(),
                    'adType' => \App\Models\Adtype::all(),
                    'priceType' => \App\Models\Pricetype::all(),
                    'text' => $text

                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 0,
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function getAttributesOfJobCategory(Request $request)
    {
        try {
            return response()->json([
                'status' => 1,
                'message' => 'success',
                'data' => [
                    'categories' => \App\JobCategory::get(),
                    'industries' => \App\Industry::get(),
                    'positions' => \App\Positiontype::all(),
                    'experiences' => \App\Experience::all(),
                    'cities' => \App\Models\City::get(),
                    'levels' => \App\Level::all(),
                    'jobtypes' => \App\Jobtype::all(),
                    'salarytypes' => Salarytype::all()
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 0,
                'message' => $e->getMessage(),
            ]);
        }
    }
    public function filterProductsBasedOnSearchCriteria(Request $request, AdshowController $adshowController)
    {
        $request->merge(['query' => $request->searchText]);
        $products = $adshowController->filterProductsBasedOnSearchCriteria($request);
        return response()->json(
            [
                'status' => 1,
                'message' => 'Success',
                'data' => $products,
                'test' => $this->calculateDistance(
                    53.32055555555556,
                    -1.7297222222222221,
                    53.31861111111111,
                    -1.6997222222222223
                )
            ]
        );
    }
    public function filterJobsBasedOnSearchCriteria(Request $request, JobController $jobController)
    {
        $jobs = $jobController->filterJobsBasedOnSearchCriteria($request);
        return response()->json(
            [
                'status' => 1,
                'message' => 'Success',
                'data' => $jobs
            ]
        );
    }
    public static function calculateDistance($lat1, $lon1, $lat2, $lon2, $unit = "K")
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }

    /**
     * FOr displaying marker on map
     */
    public function filterProductsBasedOnLatitudeAndLongitude(Request $request)
    {
        $lat = $request->lat;
        $lng = $request->lng;
        $products = Product::with(['categories', 'images'])->where('status', 1)->whereRaw('st_distance_sphere(point( latitude  ,longitude),point("' . $lat . '","' . $lng . '"))<1000000')->paginate();
        return response()->json(
            [
                'status' => 1,
                'message' => 'Success',
                'data' => $products
            ]
        );
    }
    /**
     * For Drop down functionality in navbar
     */
    public function searchProductsForDropDown(Request $request)
    {
        $request->merge(['query' => $request->searchText]);
        $products = Product::where('status', 1)
            ->searchable();
        if ($request->has('category_id') && $request->category_id) {
            $products->whereHas('categories', function ($q) use ($request) {
                $q->where('product_categories.category_id', $request->category_id);
            });
        }
        if ($request->has('lat') && $request->has('lng')) {
            $lat = $request->lat;
            $lng = $request->lng;
            $products->whereRaw('st_distance_sphere(point( latitude  ,longitude),point("' . $lat . '","' . $lng . '"))<1000000');
        }

        return response()->json(
            [
                'status' => 1,
                'message' => 'Success',
                'data' => $products->paginate()
            ]
        );
    }

    public function getBasicAdPostingData(Request $request)
    {
        return response()->json([
            'status' => 1,
            'message' => 'success',
            'data' => [
                'conditions' => \App\Models\Condition::all(),
                'adType' => \App\Models\Adtype::all(),
                'priceType' => \App\Models\Pricetype::all(),
                'cities' => City::all(),
                'days' => Day::all(),
                'auth' => Auth::check() ? Auth::user()->id : null
            ]
        ]);
    }
    public function storeAd(Request $request)
    {
        try {
            FacadesRequest::merge([
                'user_id' => Auth::user()->id,
            ]);
            app()->make(ExtendedUpdateProductRequest::class);
            FacadesRequest::merge([
                'category_id' => [
                    $request->category_id,
                    $request->sub_category_id
                ]
            ]);
            $product = null;
            if ($request->has('id')) {
                $product = $this->productRepo->updateProduct($request, $request->id);
            } else {
                $product = $this->productRepo->storeProduct($request);
            }

            $files = $request->file;
            if (is_array($files)) {
                foreach ($files as $fileData) {
                    $randomFileName = Str::random() . '.' . explode('.', $fileData['name'])[1];
                    $imgData = explode(',', $fileData['file'])[1];
                    Storage::put('temp/' . $randomFileName, base64_decode($imgData));
                    $file = new UploadedFile(storage_path('app/temp/' . $randomFileName), $fileData['name']);
                    $image = $this->uploadOne($file, 'products');

                    $productImage = new ProductImage([
                        'full'      =>  $image,
                    ]);

                    $product->images()->save($productImage);
                    // dd($file);
                }
            }
            // return redirect()->route('submit-ad-photo', $product->id)->with('success', 'Your Ad has been submitted and pending for review. After review your Ad will be live for all users. You can upload image in this step.');

            $data = [
                'status' => 1,
                'message' => 'Your Ad has been submitted and pending for review. After review your Ad will be live for all users'
            ];
            return response()->json($data);
        } catch (\Exception $e) {
            $resp = new ExceptionJsonResponse($e, false);
            return response()->json($resp->json());
        }
    }

    /**
     * Get all attributes required for job searching attributes in the navbar
     *
     */
    public function getMetaDataForNavbarJobSearch()
    {
        try {
            return response()->json([
                'status' => 1,
                'message' => 'success',
                'data' => [
                    'categories' => \App\JobCategory::get(),
                    'industries' => \App\Industry::get(),
                    'cities' => \App\Models\City::get(),
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 0,
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function verifyMobile()
    {
        try {
            $user = Auth::user();

            if ($user->cell_activated == 0) {
                $response = Http::post(
                    "https://sms.aakashsms.com/sms/v3/send",
                    [
                        "auth_token" => env("SMS_TOKEN"),
                        "to" => $user->mobile,
                        "text" => "Your Activation Key for " . env('APP_NAME', 'Mero Offer') . " is " . $user->activation_key,
                    ]
                );
                if ($response->successful()) {
                    return response()->json([
                        'status' => 1,
                        'message' => 'success',
                    ]);
                }
                throw new \Exception('SMS not sent');
            } else {
                return response()->json([
                    'status' => 0,
                    'message' => "Your Number is already verified.",
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => 0,
                'message' => "We couldn't send SMS due to some technical reasons. Please try again later.",
            ]);
        }
    }
    
    public function verifyMobileOtp(Request $request)
    {
        try {
            $this->validate($request, [
                'otp' => 'required',
            ]);
            $user = Auth::user();
            if ($request->otp == $user->activation_key) {
                $user->cell_activated = 1;
                $user->save();
                return response()->json([
                    'status' => 1,
                    'message' => 'success',
                ]);
            } else {
                return response()->json([
                    'status' => 0,
                    'message' => "Invalid OTP.",
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => 0,
                'message' => "We couldn't verify mobile due to some technical reasons. Please try again later.",
            ]);
        }
    }
}
