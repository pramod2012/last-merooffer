@extends('admin.mcontact.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('mcontact.store') }}" enctype="multipart/form-data">
                       {{csrf_field()}}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="firstname" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">E-Mail Address*</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('email') }}
                                        </strong>
                                    </span>
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Subject</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="subject" value="{{ old('subject') }}">
                                <span class="text-danger">
                                        <strong>{{ $errors->first('subject') }}
                                        </strong>
                                    </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">Mobile</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                                <span class="text-danger">
                                        <strong>{{ $errors->first('phone') }}
                                        </strong>
                                    </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Body</label>

                            <div class="col-md-6">
                                <textarea type="text" class="form-control" name="body">{{ old('body') }}</textarea>
                                <span class="text-danger">
                                        <strong>{{ $errors->first('body') }}
                                        </strong>
                                    </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
