<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Contracts\FollowerContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests as RulesOf;
use App\Utils as Utils;

class FollowerController extends BaseController
{
    protected $followerRepo;

    public function __construct(
        FollowerContract $followerRepo
    )
    {
        $this->followerRepo = $followerRepo;
    }

    public function index()
    {
        try{
            $list = $this->followerRepo->list('created_at', 'desc');
            $datas = $this->followerRepo->paginateArrayResults($list->all(), 5);
            $data = new Utils\SuccessJsonResponse($datas,"success");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function show($id)
    {
        try{
            $data1 = $this->followerRepo->findById($id);
            $data = new Utils\SuccessJsonResponse($data1,"success");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function store(Request $request)
    {
        try{
            $data1 = $this->followerRepo->store($request);
            $data = new Utils\SuccessJsonResponse($data1, "Record added Successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $data1 = $this->followerRepo->edit($request, $id);
            $data = new Utils\SuccessJsonResponse($data1, "Record updated successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function destroy($id)
    {
        try{
            $data1 = $this->followerRepo->destroy($id);
            $data = new Utils\SuccessJsonResponse($data1, "Record deleted Successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function myFollower()
    {
        try{
            $data1 = $this->followerRepo->myFollower();
            $data = new Utils\SuccessJsonResponse($data1, "success");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function myFollowing()
    {
        try{
            $data1 = $this->followerRepo->myFollowing();
            $data = new Utils\SuccessJsonResponse($data1, "success");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }
}
