<product-comment id="{{$product->id}}"></product-comment>
{{-- <div class="border-section border comments">
    <h4 class="border-section-heading text-uppercase">Comments</h4>
    <!--<div class="border-section border comments">-->
    @if(session('aim'))
    <div class="alert alert-success w-50 mx-auto mt-3 text-center">
        {{session('aim')}}
</div>
@endif
<div class="user-comments border-bottom">
    <ul class="media-list">
        @php $comments = $product->comments()->paginate(5); @endphp
        @foreach($comments as $value)
        <li id="comment-2" class="comment even thread-even depth-1 parent media">
            <div class="media-left">
                <a href="<?php echo url()->full(); ?>/#comment-2" class="media-object">
                    <img class="media-object img-thumbnail" src="{{asset('frontend/images/user.png')}}">
                </a>
            </div>
            <div class="media-body" id="div-comment-2">
                <h5 class="media-heading"><a href='#' rel='external nofollow'
                        class='url'>{{$value->user->name}}</a> &nbsp;
                    <span class="normal">Said :</span>
                    <span class="time pull-right flip">{{$value->created_at->format('F j, Y')}}</span>
                </h5>
                <p>{{$value->body}}</p>
                <!-- .comment-content -->
                <h5 class="text-right flip"><i class="fas fa-share"></i>&nbsp;<a rel='nofollow'
                        class='comment-reply-link' href='<?php echo url()->full(); ?>/?replytocom=2#respond'
                        data-commentid="2" data-postid="134" data-belowelement="media-body-2"
                        data-respondelement="respond" aria-label='Reply to admin'>Reply to comment</a></h5>
                <div class="reply-comment-div">
                    <button type="button" class="reply-tg-button btn btn-primary btn-md btn-style-one sharp">close
                        reply</button>
                    <div class="comment-form">
                        <form action="{{route('reply.add')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-inline row">
                                <p>
                                    Logged in as
                                    <a href="{{route('profile')}}">
                                        {{@Auth::user()->name}} </a>&nbsp;
                                    <a href="{{route('logout')}}" title="">Log out &raquo;</a>
                                </p>
                                <div class="form-group col-sm-12">
                                    <label class="text-capitalize">Message : <span class="text-danger">*</span></label>
                                    <div class="inner-addon">
                                        <textarea data-error="Type your comment here" name="body"
                                            placeholder="Type your reply here..." required></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <!--Message-->
                                <input type="hidden" name="url" value="<?php echo url()->full(); ?>">
                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                <input type="hidden" name="parent_id" value="{{$value->id}}">
                            </div>
                            <div class="form-group">
                                <button type="submit" name="submit" class="btn btn-primary sharp btn-md btn-style-one"
                                    value="Send">Post
                                    Comment</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!--mediabody-->
        </li>
        @foreach($value->replies as $reply)
        <ul class="children">
            <li id="comment-3" class="comment odd alt depth-2 parent media">
                <div class="media-left">
                    <a href="<?php echo url()->full(); ?>/#comment-3" class="media-object">
                        <img class="media-object img-thumbnail" src="{{asset('frontend/images/user.png')}}">
                    </a>
                </div>

                <div class="media-body" id="div-comment-3">
                    <h5 class="media-heading"><a href='http://www.joinwebs.com' rel='external nofollow'
                            class='url'>{{$reply->user->name}}</a> &nbsp;
                        <span class="normal">Said :</span>
                        <span class="time pull-right flip">{{$reply->created_at->format('F j, Y')}}</span>
                    </h5>
                    <p>{{$reply->body}}</p>
                    <!-- .comment-content -->
                    <h5 class="text-right flip">&nbsp;</h5>
                </div>
                <!--mediabody-->
            </li>
        </ul>
        @endforeach

    </ul>
    @endforeach

    {{$comments->links()}}
</div>
<div class="comment-form">

    <div class="comment-form-heading">
        <h4 class="text-uppercase">Leave a Comment</h4>
    </div>
    <!--Form-->
    <form action="{{route('comment-store')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <div class="form-inline row">
                <div class="col-sm-12">
                    <p>
                        Logged in as
                        <a href="{{route('profile')}}">
                            {{@Auth::user()->name}} </a>&nbsp;
                        <a href="{{route('logout')}}" title="">Log out &raquo;</a>
                    </p>
                </div>
                <div class="form-group col-sm-12">
                    <label class="text-capitalize">Message : <span class="text-danger">*</span></label>
                    <div class="inner-addon">
                        <textarea data-error="Type your comment here" name="body"
                            placeholder="Type your comment here..." required></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <!--Message-->
            </div>
        </div>
        <!--form-group-->
        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-primary sharp btn-md btn-style-one" value="Send">Post
                Comment</button>
        </div>
        <input type="hidden" name="url" value="<?php echo url()->full(); ?>">
        <input type="hidden" name="product_id" value="{{$product->id}}">
    </form>
    <div class="hidden">
    </div>
</div>
<!--comment-form-->
<!--loguserComment-->
</div> --}}
