@extends('admin.pricelabel.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form method="POST" action="{{ route('pricelabel.update', $data->id) }}" aria-label="subcategory">
                        {{ csrf_field() }}
                        {{method_field('PATCH')}}
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">Name</label>

                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $data->name }}" required autofocus>
                            </div>
                        </div>

                        
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">Category</label>
                            <div class="col-md-8">
                                <select class="form-control select2" multiple="multiple" data-placeholder="Category" style="width: 100%;" name="category_id[]">
                                    @foreach($data->categories as $value)
                                    <option value="{{$value->id}}" selected>{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

        <div class="form-group">
            <div class="col-md-6">
                  <div class="checkbox">
                    <label class="col-md-12 control-label">
                  <input name="status" type="checkbox" {{$data->status == 1 ? 'checked':''}}>
                      Status
                    </label>
                  </div>
              </div>
          </div>
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
