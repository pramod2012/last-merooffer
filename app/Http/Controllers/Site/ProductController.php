<?php

namespace App\Http\Controllers\Site;

use App\Contracts\ProductContract;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Utils\ExceptionJsonResponse;
use App\Utils\SuccessJsonResponse;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    protected $productRepo;

    public function __construct(
        ProductContract $productRepo
    ) {
        $this->productRepo = $productRepo;
    }
    public function getCommentsForProduct(Request $request, $id)
    {
        try {
            $datas = $this->productRepo->getCommentsForProduct($id);
            $data = ['product' => $datas, 'logged_in' => Auth::check() ? Auth::user() : null];
            $data = new SuccessJsonResponse($data, "success");
            return response()->json($data->json());
        } catch (\Exception $e) {
            $data = new ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }
}
