<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reportname extends Model
{
    protected $fillable = [
    	'name',
    ];
}
