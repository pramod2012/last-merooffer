<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\OrderStatusContract;
use App\Http\Controllers\BaseController;

class OrderStatusController extends BaseController
{
    private $page="admin.orderstatus.";

    protected $orderstatusRepo;

    public function __construct(
        OrderStatusContract $orderstatusRepo
    )
    {
        $this->orderstatusRepo = $orderstatusRepo;
    }

    public function index()
    {
        $list = $this->orderstatusRepo->list('created_at', 'desc');
        $datas = $this->orderstatusRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('orderstatus', 'List of orderstatuss');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('orderstatus', 'Create orderstatus');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->orderstatusRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('orderstatus.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->orderstatusRepo->findById($id);
        $this->setPageTitle('orderstatus', 'Edit orderstatus');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->orderstatusRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('orderstatus.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->orderstatusRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('orderstatus.index', 'Record deleted successfully' ,'success',false, false);
    }
}
