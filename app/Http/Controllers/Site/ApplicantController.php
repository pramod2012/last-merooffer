<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Job;
use App\Applicant;
use App\Models\User;
use App\Skill;
use App\Education;
use App\Work;
use App\Profile;
use App\Training;
use App\applicant_job;
use DB;

class ApplicantController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)  {
        if(Profile::where('user_id', auth()->user()->id)->count() == null){
            $user = str_slug(strtolower(auth()->user()->name), '-');
            return redirect("/profile/$user")->with('success','Please complete profile to apply !');
        }
        else{
            $job = Job::find($id);
            return view('jobpost.application')->withJob($job);
        }
       	
    }

    public function store(Request $request, $id) {
    	$this->validate($request, [
            'application_letter' => 'required',
            'job_id' => 'required',
        ]);

    	$applicant = new Applicant;
    	$applicant->application_letter = $request->input('application_letter');
        $applicant->job_id = $request->input('job_id');
    	$applicant->astatus = 'pending';
    	$applicant->user_id = auth()->user()->id;   
    	$applicant->save();

    	$applicant_job = new applicant_job;
        $applicant_job->applicant_user_id = auth()->user()->id;
        $applicant_job->job_id = $request->input('job_id');
        $applicant_job->save();

    	return redirect("/my-jobs");
    }
    public function view($id)  {
        $user = User::findOrFail($id);        
        $skills = Skill::orderBy('skill', 'asc')->get();     
        $profile = Profile::where('user_id', $id)->firstOrFail();
        $educations = Education::where('user_id', $id)
                    ->orderBy('created_at', 'desc')
                    ->get();
        $works = Work::where('user_id', $id)
                    ->orderBy('created_at', 'desc')
                    ->get();
        $trainings = Training::where('user_id', $id)
                    ->orderBy('created_at', 'desc')
                    ->get();            
        return view('freelancer.applicant', compact('user', 'profile', 'skills', 'educations', 'works','trainings'));
    }
}
