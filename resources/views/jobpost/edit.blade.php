@extends('site.partials.main_index')
@section('title','Edit your Job Post')
@section('content')
<body class="page-template page-template-template-submit-ads page-template-template-submit-ads-php page page-id-44 logged-in">
@include('site.partials.navbar')
<section class="user-pages section-gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4">
                @include('site.partials.sidebar')
                 </div><!--col-lg-3 col-md-4-->
<div class="col-lg-9 col-md-8 user-content-height">
								<div class="user-detail-section section-bg-white">
					<div class="user-ads user-profile-settings">
						<h4 class="user-detail-section-heading text-uppercase">
							Edit Job Post					</h4>						
						<form data-toggle="validator" role="form" method="POST" id="primaryPostForm" action="{{route('job-user.update',$job->id)}}" enctype="multipart/form-data">
							{{ csrf_field() }}
							{{ method_field('PATCH') }}
							<!-- user basic information -->
							<div class="form-inline row form-inline-margin">
								<div class="form-group col-sm-10">
                                    <label for="first-name">Title of Job Posting <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="name" class="form-control form-control-sm" placeholder="Example: Web Developer with Ecommerce Experience.." value="{{ $job->name }}">
                                    </div>
                                </div><!--Firstname-->
								<div class="form-group col-sm-12">
                                    <label for="bio">Job Description <span>*</span></label>
                                    <div class="inner-addon">
                                        <textarea name="body" id="bio" placeholder="enter your short info.">{{ $job->body }}</textarea>
                                    </div>
                                </div><!--biography-->
							
							<!-- user basic information -->
							<!-- Contact Details -->
							
							
								<div class="form-group col-sm-2">
                                    <label for="phone">Salary</label>
                                    <div class="inner-addon">
                                        <input type="number" id="budget" class="form-control form-control-sm" placeholder="Salary" name="salary" value="{{ $job->salary }}">
                                    </div>
                                </div><!--Phone Number-->
                                <div class="form-group col-sm-3">
                                    <label for="phone">To </label>
                                    <div class="inner-addon">
                                        <input type="number" id="salary" class="form-control form-control-sm" placeholder="Salary" name="salary_to" value="{{ $job->salary_to }}">
                                        {{ $errors->first('salary_to') }}
                                    </div>
                                </div>
                                <div class="form-group col-sm-5">
                                    <label for="phone"> Salary Type *</label>
                                    <div class="inner-addon">
                                        <select class="form-control form-control-sm" name="salarytype_id">
                                            @foreach(\App\Salarytype::all() as $value)
                                            @if($job->salarytype_id == $value->id || old('salarytype_id') == $value->id)
                                            <option value="{{$value->id}}" selected>{{$value->name}}</option>
                                            @else
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        {{ $errors->first('salarytype_id') }}
                                    </div>
                                </div>
								<div class="form-group col-sm-5">
                                    <label for="mobile">Job Category <span>*</span></label>
                                    <div class="inner-addon">
                                    	<select class="form-control form-control-sm" id="jobcategory" name="category_id">
						    				<?php $value=$job->category_id; ?>
			                        		@foreach ($categories as $category)
			                        		@if($value == $category->id || old('category_id') == $category->id)
			                            	<option value="{{$category->id}}" selected>{{$category->name}}</option>
			                            	@else
			                            	<option value="{{$category->id}}">{{$category->name}}</option>
			                            	@endif
			                        		@endforeach
						    			</select>
                                    </div>
                                </div><!--Mobile Number-->
								<div class="form-group col-sm-5">
                                    <label for="website">Position Type <span>*</span></label>
                                    <div class="inner-addon">
                                        <select class="form-control form-control-sm" id="positionType" name="positiontype_id" id="positiontype_id">
						    				<?php $value = $job->positiontype_id; ?>
                                            @foreach($positiontypes as $pos)
                                            @if($value == $pos->id || old('positiontype_id') == $pos->id)
                                            <option value="{{$pos->id}}" selected>{{$pos->name}}</option>
                                            @else
			                    			<option value="{{$pos->id}}">{{$pos->name}}</option>
                                            @endif
                                            @endforeach
						    			</select>
                                    </div>
                                </div><!--Your Website-->
								<div class="form-group col-sm-5">
                                    <label for="email">Last Date of Apply <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="endate" name="endate" class="form-control form-control-sm" placeholder="{{ date('Y/m/d') }}" value="{{ $job->endate }}">
                                    </div>
                                </div><!--Your Email-->
								<div class="form-group col-sm-5">
                                    <label for="email">Education <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="education" name="education" class="form-control form-control-sm" placeholder="enter education ie.MBA" value="{{$job->education}}">
                                    </div>
                                </div><!--Your Country-->
								<div class="form-group col-sm-5">
                                    <label for="email">Experience <span>*</span></label>
                                    <div class="inner-addon">
                                        <select class="form-control form-control-sm" id="experience_id" name="experience_id">
                                            <option selected disabled>...Select Experience</option>
                                            @foreach(\App\Experience::all() as $pos)
                                            @if($job->experience_id == $pos->id || old('experience_id') == $pos->id)
                                            <option value="{{$pos->id}}" selected>{{$pos->name}}</option>
                                            @else
                                            <option value="{{$pos->id}}">{{$pos->name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        {{$errors->first('experience_id')}}
                                    </div>
                                </div>
                                <div class="form-group col-sm-5">
                                    <label for="email">Gender</label>
                                    <div class="inner-addon">
                                        <input type="text" id="gender" name="gender" class="form-control form-control-sm" placeholder="enter sex ie. Male or Female" value="{{$job->gender}}">
                                        {{$errors->first('gender')}}
                                    </div>
                                </div><!--Your Country-->
                                <div class="form-group col-sm-5">
                                    <label for="email">Age</label>
                                    <div class="inner-addon">
                                        <input type="text" id="age" name="age" class="form-control form-control-sm" placeholder="age" value="{{$job->age}}">
                                        {{$errors->first('age')}}
                                    </div>
                                </div><!--Your State-->
								<div class="form-group col-sm-5">
                                    <label for="email">Vacancy Number <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="number" id="vac_no" name="vac_no" class="form-control form-control-sm" placeholder="Number of Vacancy" value="{{$job->vac_no}}">
                                    </div>
                                </div><!--Your City-->
								<div class="form-group col-sm-5">
                                    <label for="email">Level <span>*</span></label>
                                    <div class="inner-addon">
                                        <select class="form-control form-control-sm" id="positionType" name="level_id">
                                            <?php $value = $job->level_id; ?>
                                            @foreach($levels as $pos)
                                            @if($value == $pos->id || old('level_id') == $pos->id)
                                            <option value="{{$pos->id}}" selected>{{$pos->name}}</option>
                                            @else
                                            <option value="{{$pos->id}}">{{$pos->name}}</option>
                                            @endif
                                            @endforeach
						    			</select>
                                    </div>
                                </div><!--Your Post Code-->
								<div class="form-group col-sm-5">
                                    <label for="address">Industry <span>*</span></label>
                                    <div class="inner-addon">
                                        <select class="form-control form-control-sm" name="industry_id" id="industry_id">
						    				<?php $value = $job->industry_id; ?>
                                            @foreach($industries as $pos)
                                            @if($value == $pos->id || old('industry_id') == $pos->id)
                                            <option value="{{$pos->id}}" selected>{{$pos->name}}</option>
                                            @else
                                            <option value="{{$pos->id}}">{{$pos->name}}</option>
                                            @endif
                                            @endforeach
						    			</select>
                                    </div>
                                </div><!--industry-->
                                <div class="form-group col-sm-5">
                                    <label for="mobile">Days to run<span>*</span></label>
                                    <div class="inner-addon">
                                        
                                        <select class="form-control form-control-sm" id="jobcategory" name="day_id">

                                            <option selected disabled>Days to run</option>
                                            <?php $day = $job->day_id; ?>
                        @foreach (\App\Models\Day::all() as $value)
                                              @if($day == $value->id || old('day_id') == $value->id)
                                              <option value="{{$value->id}}" selected>{{$value->name}}</option>
                                              @else
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        {{$errors->first('day_id')}}
                                    </div>
                                </div><!--Mobile Number-->
                                <div class="form-group col-sm-5">
                                    <label for="mobile">Location/City<span>*</span></label>
                                    <div class="inner-addon">
                                        
                                        <select class="form-control form-control-sm" id="city_id" name="city_id">

                                            <option selected disabled>Days to run</option>
                                            <?php $city = $job->city_id; ?>
                        @foreach (\App\Models\City::all() as $value)
                                              @if($city == $value->id || old('city_id') == $value->id)
                                              <option value="{{$value->id}}" selected>{{$value->name}}</option>
                                              @else
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        {{$errors->first('city_id')}}
                                    </div>
                                </div><!--Address-->
                                <div class="form-group col-sm-5">
                                    <label for="email">Address</label>
                                    <div class="inner-addon">
                                        <input type="text" id="address" name="address" class="form-control form-control-sm" placeholder="address" value="{{$job->address}}">
                                        {{$errors->first('address')}}
                                    </div>
                                </div>

                                <div class="form-group col-sm-5">
                                    <label for="email">Skills</label>
                                    <div class="inner-addon">
                                        <input type="text" id="skills" name="skills" class="form-control form-control-sm" placeholder="type skills separated by comma" value="@foreach($job->skillss as $skill){{$skill->skill}},@endforeach">
                                        {{$errors->first('skills')}}
                                    </div>
                                </div>
							<div class="form-group col-sm-10">
                                    <div class="inner-addon">
                                        <div class="checkbox pull-left flip">
                                            <label for="remember">By clicking Update Now I agree to <a href="{{url('blog/terms-of-use')}}" target="_blank">Merooffer's Terms of Use</a>  and <a href="{{url('blog/privacy-policy')}}" target="_blank">Privacy Policy</a>, <a href="{{url('blog/posting-policy')}}" target="_blank">Posting Policy</a> and I consent to receiving marketing from Merooffer.</label>
                                        </div>
                            
                            <button type="submit" name="op" value="submit" class="btn btn-primary sharp btn-sm btn-style-one">Update Now</button>
                        </div>
                    </div>
            </div>
							<!-- Update your Password -->
						</form>
					</div><!--user-ads user-profile-settings-->
				</div><!--user-detail-section-->
			</div><!--col-lg-9-->
		</div><!--row-->
	</div><!--container-->	
</section><!--user-pages section-gray-bg-->

@include('site.partials.footer')
@endsection