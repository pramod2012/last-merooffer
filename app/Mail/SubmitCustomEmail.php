<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubmitCustomEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $email;
    protected $user;
    

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $user)
    {
        $this->email = $email;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->email->from,'Mero Offer')
                    ->subject($this->email->subject)
                    ->view('emails.customEmail')
                    ->with([
                        'subject' => $this->email->subject,
                        'from' => $this->email->from,
                        'body' => $this->email->body,
                        'greet' => $this->email->greet,
                        'end_greet' => $this->email->end_greet,
                        'user' => $this->user->name,
                    ]);
    }
}
