<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;

class OrderController extends Controller
{
    public function index(){
    	$orders['orders'] = Order::orderBy('created_at','desc')->where('user_id',auth()->user()->id)->get();
    	return view('cart.order', $orders);
    }
}
