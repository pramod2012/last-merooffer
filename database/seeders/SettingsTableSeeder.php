<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * @var array
     */
    protected $settings = [
        [
            'key'                       =>  'site_name',
            'value'                     =>  'E-Commerce Application',
        ],
        [
            'key'                       =>  'site_title',
            'value'                     =>  'Mero Offer: Nepal\'s Free Marketplace. Search project,job,realestate & more.',
        ],
        [
            'key'                       =>  'email',
            'value'                     =>  'hr@merooffer.com',
        ],
        [
            'key'                       =>  'currency_code',
            'value'                     =>  'GBP',
        ],
        [
            'key'                       =>  'currency_symbol',
            'value'                     =>  '£',
        ],
        [
            'key'                       =>  'location',
            'value'                     =>  'Kathmandu',
        ],
        [
            'key'                       =>  'company_name',
            'value'                     =>  'Skyfall Technologies Pvt. Ltd.',
        ],
        [
            'key'                       =>  'error_image',
            'value'                     =>  'img/dummy.jpg',
        ],
        [
            'key'                       =>  'description',
            'value'                     =>  'Merooffer.com is the next generation of free online classifieds. It provides a simple solution to the complications involved in selling, buying, trading, discussing and meeting people near you.',
        ],
        [
            'key'                       =>  'phone',
            'value'                     =>  '+977-9813346784',
        ],
        [
            'key'                       =>  'logo',
            'value'                     =>  'img/logo.png',
        ],
        [
            'key'                       =>  'favicon',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'footer_text',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'seo_meta_title',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'seo_meta_description',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'facebook',
            'value'                     =>  'www.facebook.com/merooffering',
        ],
        [
            'key'                       =>  'twitter',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'instagram',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'linkedin',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'google_analytics',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'facebook_pixels',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'stripe_payment_method',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'stripe_key',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'stripe_secret_key',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'paypal_payment_method',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'paypal_client_id',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'paypal_secret_id',
            'value'                     =>  '',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->settings as $index => $setting)
        {
            $result = Setting::create($setting);
            if (!$result) {
                $this->command->info("Insert failed at record $index.");
                return;
            }
        }
        $this->command->info('Inserted '.count($this->settings). ' records');
    }
}
