@extends('site.partials.main_index')
@section('content')
	
<body class="page-template page-template-template-follow page-template-template-follow-php page page-id-24 logged-in">
	@include('site.partials.navbar')
	<section class="user-pages section-gray-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-4">
      @include('site.partials.sidebar')
			</div><!--col-lg-3-->
			<div class="col-lg-9 col-md-8 user-content-height">
				<div class="user-detail-section section-bg-white">
					<!-- followers -->
					<div class="user-ads follower">
						<h4 class="user-detail-section-heading text-uppercase">
							Followers						</h4>
						<div class="row loop-content">
							@forelse($followers as $value)
							<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
										<div class="media">
											<div class="media-left">
												<img class="media-object" src="{{asset('frontend/images/user.png')}}" alt="{{$value->follower->name}}">
											</div>
											<div class="media-body">
												<h5 class="media-heading">
													<a href="#">
														{{$value->follower->name}}												</a>
												</h5>
												<p>
													Member Since&nbsp;
													{{$value->follower->created_at}}												</p>
						 <form method="get" class="fav-form clearfix" >
                                                   <?php if(Auth::check()){    
                                                   $auth_id = Auth::user()->id;
                                                   } ?>
                      @if(!Auth::check())<a href="{{url('login')}}">@endif          
            <button class="classiera_follow_user" id="{{'btn____'.$value->follower->id}}" data-auth_id="@if(Auth::check()) {{ Auth::user()->id }} @endif" data-user_id="{{ $value->follower->id }}" type="button" onclick="addToFollow(this)" 
                                    {{$value->follower->isAddedToFollowedList($value->follower->id)?"disabled":""}}>
                                    
                {{$value->follower->isAddedToFollowedList($value->follower->id)?"Followed Back":"Follow Back"}} </button>
            @if(!Auth::check())</a>@endif
        </form>
        <script type="text/javascript">
function addToFollow(el){
  var btn=jQuery(el);
  var auth_id=btn.attr('data-auth_id');
  var user_id=btn.attr('data-user_id');
  jQuery.ajax({
    url:'{{route("addSeller")}}',
    method:"get",
    data:{'user_id':user_id,'auth_id':auth_id,'_token':'{{csrf_token()}}'},
    success:function(data){
      if(data.status=="1"){
        console.log("added");
          jQuery("#btn____"+user_id).attr("disabled",true).html("Added");
      }else{
        console.log("else");
      }
    },
    error:function(data){
      console.log("error");
    }
  });
  // alert("Hi");
}
</script>
														
							<div class="clearfix"></div>
													</div>
										</div>
									</div>
									@empty
									<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
										You do not have any followers !
									</div>
									@endforelse
												</div>
					</div>
					<!-- followers -->
					<!-- following -->
					<div class="user-ads follower">
						<h4 class="user-detail-section-heading text-uppercase">
						Following						</h4>
						<div class="row loop-content">
							@forelse($following as $value)
																<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
										<div class="media">
											<div class="media-left">
												<img class="media-object" src="{{asset('frontend/images/user.png')}}" alt="{{$value->following->name}}">
											</div>
											<div class="media-body">
												<h5 class="media-heading">
													<a href="#">
														{{$value->following->name}}													</a>
												</h5>
												<p>
													Member Since&nbsp;
													{{$value->following->created_at }}												</p>
		<form method="post" action="{{ route('follow.destroy', ['id'=>$value->id]) }}" class="classiera_follow_user">
			{{ csrf_field() }}
            {{ method_field('DELETE')}}
			<input type="submit" name="unfollow" value="Unfollow" />
		</form>
		<div class="clearfix"></div>
													</div>
										</div>
									</div>
									@empty
									<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6"> You are not following anyone !</div>
									@endforelse
																		
						</div>
					</div>
					<!-- following -->
					<!-- followers -->
					<div class="user-ads follower">
						<h4 class="user-detail-section-heading text-uppercase">
							Company Followers						</h4>
						<div class="row loop-content">
							@forelse($com_followers as $value)
							<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
										<div class="media">
											<div class="media-left">
												<img class="media-object" src="{{asset('frontend/images/user.png')}}" alt="{{$value->follower->name}}">
											</div>
											<div class="media-body">
												<h5 class="media-heading">
													<a href="#">
														{{$value->follower->name}}												</a>
												</h5>
												<p>
													Member Since&nbsp;
													{{$value->follower->created_at}}												</p>
														
							<div class="clearfix"></div>
													</div>
										</div>
									</div>
									@empty
									<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
										Noone is following your company !
									</div>
									@endforelse
												</div>
					</div>
					<!-- followers -->
					<!-- following -->
					<div class="user-ads follower">
						<h4 class="user-detail-section-heading text-uppercase">
						Company Following						</h4>
						<div class="row loop-content">
							@forelse($com_following as $value)
									<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
										<div class="media">
											<div class="media-left">
												<img class="media-object" src="{{asset('frontend/images/user.png')}}" alt="{{$value->following->name}}">
											</div>
											<div class="media-body">
												<h5 class="media-heading">
													<a href="#">
														{{@$value->following->firmuser->name}}													</a>
												</h5>
												<p>
													Member Since&nbsp;
													{{$value->following->created_at }}												</p>
		<form method="post" action="{{ route('follow.destroy', ['id'=>$value->id]) }}" class="classiera_follow_user">
			{{ csrf_field() }}
            {{ method_field('DELETE')}}
			<input type="submit" name="unfollow" value="Unfollow" />
		</form>
		<div class="clearfix"></div>
													</div>
										</div>
									</div>
									@empty
									<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6"> You are not following any company !</div>
									@endforelse
																		
						</div>
					</div>
					<!-- following -->
				</div><!--user-detail-->
			</div><!--col-lg-9-->
		</div><!--row-->
	</div><!--container-->
</section><!--user-pages-->
 
@include('site.partials.footer')
@endsection