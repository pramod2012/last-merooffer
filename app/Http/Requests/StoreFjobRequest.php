<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFjobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'body' => 'required',
            'fcategory_id' => 'required|numeric',          
            'endate' => 'required',
            'budget' => 'required|numeric',
            'budget_to' => 'nullable|numeric',
            'address' => 'nullable',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'title',
            'body' => 'work description',
            'fcategory_id' => 'work category',
            'endate' => 'last date of apply',
            'address' => 'location',
        ];
    }
}
