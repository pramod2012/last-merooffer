<?php

use Illuminate\Support\Facades\Route;

Auth::routes(['verify' => true]);
require 'admin.php';
//frontend routes
Route::get('/', 'Site\FrontendController@index')->name('index');
Route::get('category/{category}', 'Site\FrontendController@menu')->name('menu');
Route::get('city/{slug}', 'Site\FrontendController@findByCity')->name('findByCity');
Route::get('user/{slug}', 'Site\FrontendController@user')->name('user');
Route::get('ad/{slug}', 'Site\AdshowController@product_details')->name('product_details');

Route::get('search-product', 'Site\AdshowController@searchProduct')->name('search-product');
Route::get('all-categories', 'Site\FrontendController@allCategories');
//blogshow
Route::get('blog/{slug}', 'Site\BlogController@show')->name('blog');
Route::get('blog', 'Site\BlogController@all')->name('blog.all');
Route::get('blog/category/{slug}', 'Site\BlogController@findByBlogCategory')->name('blog.category');
Route::get('blog/tag/{slug}', 'Site\BlogController@findByTag')->name('blog.tag');

Route::get('sold_action', 'Site\AdminController@sold_action')->name('sold_action');

//AdminPanel
Route::group(['middleware' => ['auth', 'verified']], function () {

    Route::get('addSeller', 'Site\FollowerController@addSeller')->name('addSeller');
    /*Route::get('companyFollow', 'Site\FollowerController@companyFollow')->name('companyFollow');
    Route::get('follow', 'Site\FollowerController@index')->name('follow');
    Route::delete('follow/destroy', 'Site\FollowerController@destroy')->name('follow.destroy');*/
    Route::get('submit-ad', 'Site\AdminController@submitAd')->name('submit-ad');
    // Route::get('submit-ad', 'Site\SearchController@storeAd')->name('storeAd');
    Route::get('submit-ad-photo/{id}', 'Site\AdminController@submitAdPhoto')->name('submit-ad-photo');
    Route::get('submit-ad-edit/{id}', 'Site\AdminController@submitAdEdit')->name('submit-ad-edit');
    Route::post('product-store', 'Site\AdminController@productStore')->name('product-store');
    Route::patch('update-product/{id}', 'Site\AdminController@updateProduct')->name('update-product');
    Route::delete('product-delete/{id}', 'Site\AdminController@deleteProduct')->name('product-delete');
    Route::get('inbox', 'Site\AdminController@inbox')->name('inbox');
    Route::get('profile', 'Site\AdminController@profile')->name('profile');
    Route::get('user-all-ads', 'Site\AdminController@userAllAds')->name('user-all-ads');

    Route::get('favourite-ads', 'Site\AdminController@favouriteAds')->name('favourite-ads');
    Route::delete('wishlist/{id}', 'Site\WishlistController@destroy')->name('wishlist.destroy');
    Route::get('edit-profile', 'Site\AdminController@editProfile')->name('edit-profile');
    Route::patch('updateProfile', 'Site\AdminController@updateProfile')->name('updateProfile');
    Route::get('user-resumes', 'Site\AdminController@userResumes')->name('user-resumes');

    Route::post('/reply/store', 'Site\CommentController@replyStore')->name('reply.add');
    Route::post('comment-store', 'Site\CommentController@storeComment')->name('comment-store');
    Route::post('bid-store', 'Site\BidController@storeBid')->name('bid-store');
});

Route::post('contact-store', 'Site\ContactController@storeContact')->name('contact-store');
Route::post('report-store', 'Site\ReportController@storeReport')->name('report-store');
//jobs
Route::get('jobs', 'Site\JobController@index')->name('jobs');
Route::get('all-jobcategories', 'Site\JobController@allJobCategories')->name('all-jobcategories');
Route::get('all-jobs', 'Site\JobController@allJobs')->name('all-jobs');
Route::get('jobs/category/{slug}', 'Site\JobController@jobCat')->name('jobcat');
Route::get('jobs/city/{slug}', 'Site\JobController@jobCity')->name('jobcity');
Route::get('jobs/user/{id}', 'Site\JobController@findByCompany')->name('jobuser');
Route::get('jobs/type/{slug}', 'Site\JobController@findByJobType')->name('jobtype');
Route::get('jobs/skill/{slug}', 'Site\JobController@jobSkill')->name('jobskill');
Route::get('search-job', 'Site\JobController@searchJob')->name('search-job');
Route::post('create-resume', 'Site\AdminController@createResume')->name('create-resume');
// Job Controller
Route::resource('job-user', 'Site\JobuserController')->except('show');

Route::get('addToLove', 'Site\WishlistController@post_wishlist')->name('addToLove');
// Freelancer Controller
Route::get('jobs/{slug}', 'Site\JobController@jobShow')->name('job_post');

Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::post('/profile/store', 'Site\FreelancerController@storeProfile');
    Route::post('/profile/edit', 'Site\FreelancerController@updateProfile');
    Route::post('/profile/uploadphoto', 'Site\FreelancerController@uploadPhoto');
    Route::post('/profile/updatephoto', 'Site\FreelancerController@updatePhoto');
    Route::get('/profile/{name}', 'Site\FreelancerController@profile');
    Route::get('/my-jobs', 'Site\FreelancerController@myJobs');

    Route::get('/job/application/{id}', 'Site\ApplicantController@show');
    Route::post('/job/application/{id}/store', 'Site\ApplicantController@store');
});

Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::get('/applicant/profile/{id}', 'Site\ApplicantController@view');
});
// Client Controller
Route::get('/dashboard', 'Site\ClientController@dashboard');
Route::get('/shortlist/{slug}', 'Site\ClientController@shortlist');
Route::get('/proposal/{slug}/{user_id}', 'Site\ClientController@proposal');
Route::get('/proposal/{id}/{user}/hire', 'Site\ClientController@hire');
Route::get('/proposal/{id}/{user}/reject', 'Site\ClientController@reject');
Route::get('/editfirmuser', 'Site\ClientController@editfirmuser')->name('editfirmuser');
Route::post('/firmuser/store1', 'Site\ClientController@storeFirmuser')->name('firmuser.store1');
Route::patch('/firmuser/{id}/update1', 'Site\ClientController@updateFirmuser')->name('firmuser.update1');

// Skill Controller
Route::post('/profile/skills/store', 'Site\SkillController@storeSkill');
Route::post('/profile/skills/edit', 'Site\SkillController@editSkill');
// Education Controller
Route::post('/profile/education/store', 'Site\EducationController@storeEducation');
Route::post('/profile/education/update', 'Site\EducationController@updateEducation');
Route::post('/profile/education/delete', 'Site\EducationController@deleteEducation');

//training
Route::post('/profile/training/store', 'Site\TrainingController@storeTraining');
Route::post('/profile/training/update', 'Site\TrainingController@updateTraining');
Route::post('/profile/training/delete', 'Site\TrainingController@deleteTraining');
// Work Controller
Route::post('/profile/work/store', 'Site\WorkController@storeWork');
Route::post('/profile/work/update', 'Site\WorkController@updateWork');
Route::post('/profile/work/delete', 'Site\WorkController@deleteWork');
// Applicant Controller
Route::delete('/panel/jobs/delete/{id}', 'Site\AdminController@deleteJob');

Route::get('contact-us', 'Site\AdshowController@contactUs')->name('contact-us');
Route::post('mcontact/store', 'Site\McontactController@store')->name('mcontact.store');

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('freelancers', 'Site\FreelancersController@index')->name('freelancers.index');
Route::get('freelancers/category/{slug}', 'Site\FreelancersController@category')->name('freelancers.category');
Route::get('projects/{slug}', 'Site\FreelancersController@show')->name('freelancers.show');
Route::get('freelancers-search', 'Site\FreelancersController@search')->name('freelancers.search');
Route::get('works', 'Site\FreelancersController@allWorks')->name('works.index');
Route::get('/freelancers/all-categories', 'Site\FreelancersController@allCategories');
Route::get('freelancers/skill/{slug}', 'Site\FreelancersController@skill')->name('freelancers.skill');
Route::get('freelancers/user/{slug}', 'Site\FreelancersController@user')->name('freelancers.user');

Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::resource('project', 'Site\ProjectController')->except('show');
    Route::post('freelancers/applicant/store', 'Site\FreelancersController@applicantStore')->name('freelancers.applicant.store');
    Route::get('/fshortlist/{slug}', 'Site\FreelancersController@shortlist');
    Route::get('/fproposal/{slug}/{user_id}', 'Site\FreelancersController@proposal');
    Route::get('/fproposal/{id}/{user}/hire', 'Site\FreelancersController@hire');
    Route::get('/fproposal/{id}/{user}/reject', 'Site\FreelancersController@reject');
});

Route::resource('cart', 'Site\CartController')->except('show');

Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::get('checkout', 'Site\CheckoutController@index');
    Route::get('bank-transfer', 'Site\BankTransferController@index')->name('bank-transfer.index');
    Route::post('bank-transfer', 'Site\BankTransferController@store')->name('bank-transfer.store');
    Route::get('addresses', 'Site\AddressController@index');
    Route::post('addresses/store', 'Site\AddressController@store')->name('addresses.store');
    Route::delete('addresses/delete/{id}', 'Site\AddressController@destroy')->name('addresses.destroy');

    Route::get('order', 'Site\OrderController@index')->name('order');
});

Route::get('orderAdmin', 'Site\AdminController@orderAdmin')->name('orderAdmin');
Route::get('cancelOrder/{id}', 'Site\AdminController@cancelOrder')->name('cancelOrder');
Route::post('cancelOrderRequest/{id}', 'Site\AdminController@cancelOrderRequest')->name('cancelOrderRequest');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['auth', 'verified']], function () {

    Route::post('images/upload', 'Admin\ProductImageController@upload')->name('admin.products.images.upload');
    Route::delete('images/{id}/delete', 'Admin\ProductImageController@delete')->name('admin.products.images.delete');
});

Route::get('product-comments/{id}', 'Site\ProductController@getCommentsForProduct');
Route::get('bid-stats/{product_id}', 'Site\BidController@getStatsOfProduct')->name('bid-stats');
Route::get('categories-for-search', 'Site\SearchController@getCategoriesForSearch')->name('categories-for-search');
Route::get('get-search-attributes-for-category', 'Site\SearchController@getAttributesOfCategory')->name('get-search-attributes-for-category');
Route::get('get-search-attributes-for-job-category', 'Site\SearchController@getAttributesOfJobCategory')->name('get-search-attributes-for-job-category');
Route::get('filter-products-based-on-criteria', 'Site\SearchController@filterProductsBasedOnSearchCriteria')->name('filterProductsBasedOnSearchCriteria');
Route::get('filter-jobs-based-on-criteria', 'Site\SearchController@filterJobsBasedOnSearchCriteria')->name('filterJobsBasedOnSearchCriteria');

Route::get('filter-products-based-on-location', 'Site\SearchController@filterProductsBasedOnLatitudeAndLongitude')->name('filterProductsBasedOnLatitudeAndLongitude');

Route::get('search-products-for-drop-down', 'Site\SearchController@searchProductsForDropDown')->name('searchProductsForDropDown');
Route::get('get-basic-ad-posting-data', 'Site\SearchController@getBasicAdPostingData')->name('getBasicAdPostingData');
Route::post('store-ad', 'Site\SearchController@storeAd')->name('storeAd')->middleware(['auth']);

Route::get('get-meta-data-for-navbar-job-search', 'Site\SearchController@getMetaDataForNavbarJobSearch')->name('getMetaDataForNavbarJobSearch');
Route::post('verify-mobile-number', 'Site\SearchController@verifyMobile')->middleware(['auth', 'throttle:1,1'])->name('verify-mobile-number');
Route::post('verify-mobile-otp', 'Site\SearchController@verifyMobileOtp')->middleware(['auth'])->name('verify-mobile-otp');

Route::get('map', 'Site\AdshowController@map');
Route::get('chat', function () {
    return view('chat');
});
