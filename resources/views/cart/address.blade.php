@extends('site.partials.main_index')
@section('title','Address')
@section('content')
<body class="page-template page-template-template-submit-ads page-template-template-submit-ads-php page page-id-44 logged-in">
@include('site.partials.navbar')
<section class="user-pages section-gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4">
                @include('site.partials.sidebar')
            </div><!--col-lg-3 col-md-4-->
            <div class="col-lg-9 col-md-8 user-content-height">
                <div class="user-detail-section section-bg-white">
                    @include('partials.alert')

                    <div class="user-ads user-profile-settings">
                        <h4 class="user-detail-section-heading text-uppercase">
                            Address                       </h4>                       
                        <form role="form" method="POST" action="{{route('addresses.store')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            
                            <!-- user basic information -->
                            <div class="form-inline row form-inline-margin">
                                <div class="form-group col-sm-5">
                                    <label for="first-name">Alias</label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="alias" class="form-control form-control-sm" placeholder="" value="{{old('alias')}}">
                                        <strong>{{ $errors->first('alias') }}</strong>
                                    </div>
                                </div><!--Firstname-->
                                
                                <div class="form-group col-sm-5">
                                    <label for="first-name">Address 1 <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="address_1" class="form-control form-control-sm" placeholder="" value="{{old('address_1')}}">
                                        <strong>{{ $errors->first('address_1') }}</strong>
                                    </div>
                                </div><!--Firstname-->
                                <div class="form-group col-sm-5">
                                    <label for="first-name">Address 2</label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="address_2" class="form-control form-control-sm" placeholder="" value="{{old('address_2')}}">
                                        <strong>{{ $errors->first('address_2') }}</strong>
                                    </div>
                                </div><!--Firstname-->
                                <div class="form-group col-sm-5">
                                    <label for="first-name">City <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="city" class="form-control form-control-sm" placeholder="" value="{{old('city')}}">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </div>
                                </div><!--Firstname-->
                                <div class="form-group col-sm-5">
                                    <label for="first-name">Province <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="province" class="form-control form-control-sm" placeholder="" value="{{old('province')}}">
                                        <strong>{{ $errors->first('province') }}</strong>
                                    </div>
                                </div><!--Firstname-->
                                <div class="form-group col-sm-5">
                                    <label for="first-name">Country<span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="country" class="form-control form-control-sm" placeholder="" value="{{old('country')}}">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </div>
                                </div><!--Firstname-->
                                <div class="form-group col-sm-5">
                                    <label for="first-name">State Code</label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="state_code" class="form-control form-control-sm" placeholder="" value="{{old('state_code')}}">
                                        <strong>{{ $errors->first('state_code') }}</strong>
                                    </div>
                                </div><!--Firstname-->
                                <div class="form-group col-sm-5">
                                    <label for="last-name">Phone<span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="last-name" name="phone" class="form-control form-control-sm" placeholder="" value="{{old('phone')}}">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </div>
                                </div><!--Last name-->
                                <div class="form-group col-sm-5">
                                    <label for="last-name">Zip</label>
                                    <div class="inner-addon">
                                        <input type="text" id="last-name" name="zip" class="form-control form-control-sm" placeholder="" value="{{old('zip')}}">
                                        <strong>{{ $errors->first('zip') }}</strong>
                                    </div>
                                </div><!--Last name-->
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            </div>
                            <!-- user basic information -->
                            <button type="submit" name="op" value="submit" class="btn btn-primary sharp btn-sm btn-style-one">Create</button>
                            <!-- Update your Password -->
                        </form>
                    </div><!--user-ads user-profile-settings-->
                </div><!--user-detail-section-->
            </div><!--col-lg-9-->
        </div><!--row-->
    </div><!--container-->  
</section><!--user-pages section-gray-bg-->

@include('site.partials.footer')
@endsection