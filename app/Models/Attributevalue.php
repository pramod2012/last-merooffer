<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Attribute;
use App\Models\Product;
use App\Models\Productattribute;

class Attributevalue extends Model
{
    protected $fillable = [
        'value',
        'attribute_id',
    ];

    public function attribute(){
    	return $this->belongsTo(Attribute::class);
    }

    public function products(){
        return $this->belongsToMany(Product::class,'product_attributevalues');
    }

    public function productAttributes()
    {
        return $this->belongsToMany(Productattribute::class);
    }
}
