@extends('admin.applicant.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('applicant.update', $data->id) }}" enctype="multipart/form-data">
                       {{csrf_field()}}
                       {{method_field('PATCH')}}
                        <div class="form-group">
                            <label for="application_letter" class="col-md-4 control-label">Application Letter</label>

                            <div class="col-md-6">
                                <textarea rows="4" id="application_letter" type="text" class="form-control" name="application_letter">{{ old('application_letter', $data->application_letter) }}</textarea>
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('application_letter') }}
                                        </strong>
                                    </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-md-4 control-label">Status</label>
                            <div class="col-md-6">
                                <input id="status" type="text" class="form-control" name="status" value="{{ old('status', $data->status) }}">
                            </div>
                        </div>
                        

                    <div class="form-group">
                      <label class="col-md-4 control-label">User ID</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="user_id">
                              <option value="{{$data->user_id}}">{{$data->user->name}}</option>
                            </select>
                                <span class="text-danger">{{ $errors->first('user_id') }}</span>
                            </div>
                        </div>

                    <div class="form-group">
                      <label class="col-md-4 control-label">Job ID</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="job_id">
                              <option value="{{$data->job_id}}">{{$data->job->name}}</option>
                            </select>
                                <span class="text-danger">{{ $errors->first('job_id') }}</span>
                            </div>
                        </div>

    <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="mail_status" value="0">
                      <input name="mail_status" value="1" type="checkbox" {{$data->mail_status?"checked":""}}>
                      Mail Status 
                    </label>
                  </div>
              </div>
          </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
