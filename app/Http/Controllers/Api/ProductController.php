<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Contracts\ProductContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests as RulesOf;
use App\Utils as Utils;
use Illuminate\Support\Facades\Auth;

class ProductController extends BaseController
{
    protected $productRepo;

    public function __construct(
        ProductContract $productRepo
    ) {
        $this->productRepo = $productRepo;
    }

    public function index(Request $request)
    {
        try {
            $datas = $this->productRepo->searchProduct($request);
            $data = new Utils\SuccessJsonResponse($datas, "success");
            return response()->json($data->json());
        } catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function show($id)
    {
        try {
            $data1 = $this->productRepo->findById($id);
            $data = new Utils\SuccessJsonResponse($data1, "success");
            return response()->json($data->json());
        } catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function store(Request $request)
    {
        try {
            app()->make(RulesOf\StoreProductFormRequest::class);
            $data1 = $this->productRepo->storeProduct($request);
            $data = new Utils\SuccessJsonResponse($data1, "Record added Successfully");
            return response()->json($data->json());
        } catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function update(Request $request, $id)
    {
        try {
            app()->make(RulesOf\UpdateProductRequest::class);
            $data1 = $this->productRepo->editProduct($request, $id);
            $data = new Utils\SuccessJsonResponse($data1, "Record updated successfully");
            return response()->json($data->json());
        } catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function destroy($id)
    {
        try {
            $data1 = $this->productRepo->destroy($id);
            $data = new Utils\SuccessJsonResponse($data1, "Record deleted Successfully");
            return response()->json($data->json());
        } catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function myAds()
    {
        try {
            $datas = $this->productRepo->myAds();
            $data = new Utils\SuccessJsonResponse($datas, "success");
            return response()->json($data->json());
        } catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }
    
}
