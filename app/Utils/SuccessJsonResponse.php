<?php

namespace App\Utils;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Validation\ValidationException;

class SuccessJsonResponse
{
    public $data;
    public $message;

    public function __construct($data, $message)
    {
        $this->data = $data;
        $this->message = $message;
    }

    public function json()
    {
        return ['status' => 1, 'message' => $this->message, 'data' => $this->data];
    }
}
