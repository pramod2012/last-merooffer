@extends('site.partials.main_index')
@section('title',"Submit Ads")
@section('content')

<body
    class="page-template page-template-template-submit-ads page-template-template-submit-ads-php page page-id-44 logged-in">
    @include('site.partials.navbar')
    <section class="user-pages section-gray-bg" id="app">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-8 user-content-height">
                    <div class="submit-post section-bg-white">
                        <ad-submit-form />
                    </div>
                </div>
            </div>
        </div>
    </section>



    @include('site.partials.footer')

    @section('page-script')
    {{-- <script type='text/javascript' src="{{ asset('frontend/js/validator.min.js') }}"></script> --}}
    @stop
    @endsection
