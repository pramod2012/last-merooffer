@extends('admin.post.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">Update Post</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{route('post.update',$post->id)}}" enctype="multipart/form-data">
                       {{csrf_field()}}
                       {{ method_field('PATCH') }}
                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Name *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="name" value="{{ $post->name }}">
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            </div>
                        </div>
                        
                  <div class="form-group">
                      <label class="col-md-2 control-label">Post Category</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="mcategory_id">
                              <option value="{{$post->mcategory->id}}">{{$post->mcategory->name}}</option>
                                  @foreach($mcategories as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('mcategory_id') }}</span>
                            </div>
                        </div>


                         <div class="form-group">
                            <label class="col-md-2 control-label">Date *</label>
                            <div class="col-md-10">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" value="{{ $post->date }}" name="date" class="form-control pull-right" id="birthDate">
                                    <span class="text-danger">{{ $errors->first('date') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description" class="col-md-2 control-label">Description / Summary *</label>

                            <div class="col-md-10">
                                <textarea rows="5" type="text" class="form-control" name="desc" value="{{ $post->desc }}">{{ $post->desc }}</textarea>
                                <span class="text-danger">{{ $errors->first('desc') }}</span>
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="post" class="col-md-2 control-label">Long Desctiption*</label>

                            <div class="col-md-10">
                                <textarea rows="10" id="news" type="text" class="ckeditor" name="body" value="{{ $post->body }}">{{ $post->body }}</textarea>

                                    <span class="text-danger">{{ $errors->first('body') }}</span>
                            </div>
                        </div>

                    <div class="form-group">
                            <label for="keywords" class="col-md-2 control-label">Keywords</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="keyword" value="{{$post->keyword}}">
                            </div>
                        </div>
  <div class="form-group">
        <label class="col-md-2 control-label">Tags</label>
              <div class="col-md-10">
                <select class="form-control select2" multiple="multiple" data-placeholder="Tags" style="width: 100%;" name="tag_id[]">
                  @foreach($tags as $value)
                  <option value="{{$value->id}}">{{$value->name}}</option>
                  @endforeach
                  @foreach($post->tags as $value)
                  <option value="{{$value->id}}" selected="">{{$value->name}}</option>
                  @endforeach
                  
                </select>
              </div>
  </div>
                  <div class="form-group">
                      <label class="col-md-2 control-label">Posed By</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="author_id">
                              <option value="{{$post->author->id}}">{{$post->author->name}}</option>
                                  @foreach($authors as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('author_id') }}</span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="txtname" class="col-md-2 control-label">Image *</label>

                            <div class="col-md-10">
                                <input id="imgInp" type="file" class="form-control" name="image" multiple="">
                                <span class="text-danger">{{ $errors->first('image') }}</span>
                                
                                <img src="{{asset('storage/'.$post->image)}}" width="150px" height="150px"/>
                                <img id="blah" width="350" />
                            </div>
                        </div>

          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                  <input name="feature" type="hidden" value="0">
                  <input name="feature" type="checkbox" value="1" {{$post->feature?"checked":""}}>
                      Feature
                    </label>
                  </div>
              </div>
          </div>


          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input name="status" type="hidden" value="0">
                      <input name="status" type="checkbox" value="1" {{$post->status?"checked":""}}>
                      Status 
                    </label>
                  </div>
              </div>
          </div>

          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                  <input name="is_blog" type="hidden" value="0">
                  <input name="is_blog" type="checkbox" value="1" {{$post->is_blog?"checked":""}}>
                      Is Blog / Is page( 1 for blog, 0 for page)
                    </label>
                  </div>
              </div>
          </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
