<?php

namespace App\Repo;

use App\Applicant;
use App\Contracts\ApplicantContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubmitApplicantEmail;
use App\Mail\SubmitApplicantEmailReceipient;

/**
 * Class ApplicantRepository
 *
 * @package \App\Repo
 */
class ApplicantRepo extends BaseRepo implements ApplicantContract
{
    /**
     * ApplicantRepository constructor.
     * @param Applicant $model
     */
    public function __construct(Applicant $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function store($request)
    {
        try {
            $applicant = new Applicant;
            $applicant->application_letter = $request->input('application_letter');
            $applicant->job_id = $request->input('job_id');
            $applicant->astatus = 'pending';
            $applicant->user_id = auth()->user()->id;
            $applicant->mail_status = $request->input('mail_status');   
            $applicant->save();

        $applicant->jobs()->attach($id);

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
        try
        {
        $applicant = $this->findById($id);
        $applicant->application_letter = $request->input('application_letter');
        $applicant->job_id = $request->input('job_id');
        $applicant->astatus = $request->input('astatus');
        $applicant->user_id = $request->input('user_id');
        $applicant->mail_status = $request->input('mail_status');
        $applicant->save();

        if($request->mail_status == '1'){
        Mail::to($applicant->user->email)->send(new SubmitApplicantEmail($applicant));
        Mail::to($applicant->job->user->email)->send(new SubmitApplicantEmailReceipient($applicant));
        }

        return $applicant;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);
        $data->delete();
        return $data;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $data = Applicant::where('slug', $slug)->first();
        return $data;
    }
}
