@extends('admin.comment.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">City</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('comment.store') }}" enctype="multipart/form-data">
                       {{csrf_field()}}
                        <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Body</label>

                            <div class="col-md-6">
                                <textarea  rows="4" id="name" type="text" class="form-control" name="body">{{old('body')}}</textarea>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
