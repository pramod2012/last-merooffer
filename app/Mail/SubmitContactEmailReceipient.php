<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Contact;

class SubmitContactEmailReceipient extends Mailable
{
    use Queueable, SerializesModels;
    protected $contact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@merooffer.com')
                    ->subject('Submit Contact Email Receipient')
                    ->view('emails.submitContactEmailReceipient')
                    ->with([
                        'name' => $this->contact->name,
                        'email' => $this->contact->email,
                        'subject' => $this->contact->subject,
                        'body' => $this->contact->desc,
                        'product_of' => $this->contact->user->name,
                        'product_of_email' => $this->contact->user->email,
                        'slug' => $this->contact->product->slug,
                    ]);
    }
}
