<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class OrderStatus extends Model
{
    protected $fillable = [
        'name',
        'color',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
