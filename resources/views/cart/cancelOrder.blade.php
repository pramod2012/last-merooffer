@extends('site.partials.main_index')
@section('title','Cancel Reasons')
@section('content')
<body class="page-template page-template-template-submit-ads page-template-template-submit-ads-php page page-id-44 logged-in">
@include('site.partials.navbar')
<section class="user-pages section-gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4">
                @include('site.partials.sidebar')
            </div><!--col-lg-3 col-md-4-->
            <div class="col-lg-9 col-md-8 user-content-height">
                <div class="user-detail-section section-bg-white">
                    @include('partials.alert')

                    <div class="user-ads user-profile-settings">
                        <h4 class="user-detail-section-heading text-uppercase">
                            Cancellation Request                     </h4>                       
                        <form role="form" method="POST" action="{{route('cancelOrderRequest', $order->id)}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            
                            <!-- user basic information -->
                            <div class="form-inline row form-inline-margin">
                                <div class="form-group col-sm-10">
                                    <label for="first-name">Reasons for cancellation</label>
                                    <div class="inner-addon">
                                        <textarea type="text" id="cancel_reason" name="cancel_reason" class="form-control form-control-sm" placeholder="" value="{{old('cancel_reason')}}"></textarea>
                                        <strong>{{ $errors->first('cancel_reason') }}</strong>
                                    </div>
                                </div><!--Firstname-->
                            </div>
                            <!-- user basic information -->
                            <button type="submit" name="op" value="submit" class="btn btn-primary sharp btn-sm btn-style-one">Submit</button>
                            <!-- Update your Password -->
                        </form>
                    </div><!--user-ads user-profile-settings-->
                </div><!--user-detail-section-->
            </div><!--col-lg-9-->
        </div><!--row-->
    </div><!--container-->  
</section><!--user-pages section-gray-bg-->

@include('site.partials.footer')
@endsection