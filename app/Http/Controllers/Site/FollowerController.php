<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Follower;
use Carbon\Carbon;

class FollowerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $com_following = Follower::where('is_company', 1)->where('followed_by', auth()->user()->id)->get();
        $com_followers = Follower::where('is_company', 1)->where('leader_id', auth()->user()->id)->get();
        $following = Follower::where('is_company', 0)->where('followed_by', auth()->user()->id)->get();
        $followers = Follower::where('is_company', 0)->where('leader_id', auth()->user()->id)->get();
        return view('site.admin.follow', compact("following", "followers", "com_following", "com_followers"));
    }

    public function addSeller(Request $request)
    {
        // return " jk";

        $data['leader_id'] = $request->user_id;
        $data['followed_by'] = $request->auth_id;
        $follower = Follower::where('leader_id', $request->user_id)->where('followed_by', $request->auth_id)->first();
        if ($follower) {
            $follower->updated_at = Carbon::now();
            $follower->save();

            // return redirect()->back()->with('success', 'Added to your wish list');
        } else {
            Follower::create([
                'leader_id' => $request->user_id, 'followed_by' => $request->auth_id
            ]);
            Follower::create([
                'followed_by' => $request->user_id, 'leader_id' => $request->auth_id
            ]);
        }
        return array('status' => '1');
        // return array('status' => '0');
        // return redirect()->back();
    }

    public function companyFollow(Request $request)
    {
        // return " jk";

        $data['leader_id'] = $request->user_id;
        $data['followed_by'] = $request->auth_id;
        $data['is_company'] = '1';

        if (Follower::create($data)) {
            return array('status' => '1');
            // return redirect()->back()->with('success', 'Added to your wish list');
        }
        return array('status' => '0');
        // return redirect()->back();
    }

    public function destroy(Request $request)
    {
        $id = (int) $request->id;
        Follower::find($request->id)->delete();
        return redirect()->back();
    }
}
