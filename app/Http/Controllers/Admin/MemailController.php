<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\MemailContract;
use App\Http\Controllers\BaseController;

class MemailController extends BaseController
{
    private $page="admin.memail.";

    protected $memailRepo;

    public function __construct(
        MemailContract $memailRepo
    )
    {
        $this->memailRepo = $memailRepo;
    }

    public function index()
    {
        $list = $this->memailRepo->list('created_at', 'desc');
        $datas = $this->memailRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('memail', 'List of memail');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('memail', 'Create memail');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->memailRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('memail.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->memailRepo->findById($id);
        $this->setPageTitle('memail', 'Edit memail');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->memailRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('memail.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->memailRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('memail.index', 'Record deleted successfully' ,'success',false, false);
    }
}
