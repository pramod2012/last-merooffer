@extends('admin.comment.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Comment</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('comment.update',$comment->id) }}" enctype="multipart/form-data">
                       {{csrf_field()}}
                       {{method_field('PATCH')}}
                       <input type="hidden" name="product_id" value="{{$comment->commentable_id}}">
                        <div class="form-group">
                            <label for="body" class="col-md-4 control-label">Body</label>

                            <div class="col-md-6">
                                <textarea id="body" type="text" class="form-control" name="body">{{$comment->body }}</textarea>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <level for="bid" class="col-md-4 control-label">Bid</level>
                                <div class="col-md-6">
                                    <input value="{{$comment->bid}}" type="text" name="bid" class="form-control">
                                </div>
                        </div>
                        <div class="form-group">
                            <level for="bid" class="col-md-4 control-label">URL</level>
                                <div class="col-md-6">
                                    <input value="{{$comment->url}}" type="text" name="url" class="form-control">
                                </div>
                        </div>
                        <div class="form-group">
                            <level for="bid" class="col-md-4 control-label">Commented By</level>
                                <div class="col-md-6">
                                    <select name="user_id" class="form-control">
                                        <option value="{{$comment->user_id}}">{{$comment->user->name}}</option>
                                    </select>
                                </div>
                        </div>

    <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                  <input name="status" type="hidden" value="0">
                  <input name="status" value="1" type="checkbox" {{$comment->status?"checked":""}}>
                      Status
                    </label>
                  </div>
              </div>
          </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                 Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
