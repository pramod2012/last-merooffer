<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Memail extends Model
{
    protected $fillable =
    [
    	'from','greet','body','end_greet','status','subject','to'
    ];
}
