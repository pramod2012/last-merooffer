-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 09, 2021 at 01:04 AM
-- Server version: 10.3.24-MariaDB-cll-lve
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nepsalc1_meroofferv3`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `alias`, `address_1`, `address_2`, `zip`, `state_code`, `city`, `province`, `country`, `user_id`, `status`, `phone`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Narephant', 'Kathmandu', 'jadibuti', NULL, '00977', 'Khandbari, Sankhuwasabha', 'Sankhuwasabha', 'Nepal', 28, 1, '9843661518', '2021-03-06 23:29:28', '2021-02-22 22:11:56', '2021-03-06 23:29:28'),
(2, 'Lalitpur', 'Lalitpur', 'Hattiban', NULL, NULL, 'Kathmandu', 'Province No 3', 'Nepal', 28, 1, '9849551992', NULL, '2021-03-06 23:30:06', '2021-03-06 23:30:06'),
(3, 'Kathmandu', 'sitapaila', 'sitapaila', '44600', '44600', 'Kathmandu', 'Bagmati', 'Nepal', 65, 1, '9843806920', NULL, '2021-08-07 20:10:17', '2021-08-07 20:10:17');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mandeep Dev Rana', 'hr@merooffer.com', NULL, '$2y$10$6Nq97DNd38Y3efzjjFQAGefo/quAprq7vU6uewMyJIeolgth1H9ke', 'juAPzZdFaxecVt5mT0RgqYts1Sq2zeQrDtgiKk16hN01GYULtvw3TD7ZDix3', '2021-02-06 05:34:36', '2021-02-06 05:34:36');

-- --------------------------------------------------------

--
-- Table structure for table `adtypes`
--

CREATE TABLE `adtypes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `adtypes`
--

INSERT INTO `adtypes` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'For Sale', 1, '2021-02-06 05:35:13', '2021-02-06 05:35:13'),
(2, 'Want to Buy', 1, '2021-02-06 05:35:13', '2021-02-06 05:35:13'),
(3, 'Want to lease', 1, '2021-02-06 05:35:13', '2021-02-06 05:35:13'),
(4, 'For Rent', 1, '2021-02-06 05:35:15', '2021-02-06 05:35:15'),
(5, 'For Hire', 1, '2021-02-06 05:35:15', '2021-02-06 05:35:15'),
(6, 'Lost and found', 1, '2021-02-06 05:35:16', '2021-02-06 05:35:16'),
(7, 'An event', 1, '2021-02-06 05:35:16', '2021-02-06 05:35:16'),
(8, 'Professional Service', 1, '2021-02-06 05:35:16', '2021-02-06 05:35:16'),
(9, 'Free', 1, '2021-02-06 05:35:17', '2021-02-06 05:35:17'),
(10, 'Exchange', 1, '2021-02-06 05:35:17', '2021-02-06 05:35:17');

-- --------------------------------------------------------

--
-- Table structure for table `amenities`
--

CREATE TABLE `amenities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `font` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `amenities`
--

INSERT INTO `amenities` (`id`, `value`, `image`, `font`, `attribute_id`, `created_at`, `updated_at`) VALUES
(1, 'Lawn', NULL, NULL, 25, '2021-02-06 05:39:23', '2021-02-06 05:39:23'),
(2, 'Parking Space', NULL, NULL, 25, '2021-02-06 05:39:23', '2021-02-06 05:39:23'),
(3, 'Garage', NULL, NULL, 25, '2021-02-06 05:39:24', '2021-02-06 05:39:24'),
(4, 'Security Staff', NULL, NULL, 25, '2021-02-06 05:39:25', '2021-02-06 05:39:25'),
(5, 'Drainage', NULL, NULL, 25, '2021-02-06 05:39:25', '2021-02-06 05:39:25'),
(6, 'Balcony', NULL, NULL, 25, '2021-02-06 05:39:25', '2021-02-06 05:39:25'),
(7, 'Fencing', NULL, NULL, 25, '2021-02-06 05:39:27', '2021-02-06 05:39:27'),
(8, 'Backyard', NULL, NULL, 25, '2021-02-06 05:39:27', '2021-02-06 05:39:27'),
(9, 'Frontyard', NULL, NULL, 25, '2021-02-06 05:39:27', '2021-02-06 05:39:27'),
(10, 'Water Supply', NULL, NULL, 25, '2021-02-06 05:39:28', '2021-02-06 05:39:28'),
(11, 'Modular Kitchen', NULL, NULL, 25, '2021-02-06 05:39:28', '2021-02-06 05:39:28'),
(12, 'Solar Water', NULL, NULL, 25, '2021-02-06 05:39:29', '2021-02-06 05:39:29'),
(13, 'Wifi', NULL, NULL, 25, '2021-02-06 05:39:29', '2021-02-06 05:39:29'),
(14, 'Water Well', NULL, NULL, 25, '2021-02-06 05:39:30', '2021-02-06 05:39:30'),
(15, 'Water Tank', NULL, NULL, 25, '2021-02-06 05:39:30', '2021-02-06 05:39:30'),
(16, 'Garden', NULL, NULL, 25, '2021-02-06 05:39:30', '2021-02-06 05:39:30'),
(17, 'TV Cable', NULL, NULL, 25, '2021-02-06 05:39:31', '2021-02-06 05:39:31'),
(18, 'Air Condition', NULL, NULL, 25, '2021-02-06 05:39:31', '2021-02-06 05:39:31'),
(19, 'Electricity Backup', NULL, NULL, 25, '2021-02-06 05:39:32', '2021-02-06 05:39:32'),
(20, 'Store Room', NULL, NULL, 25, '2021-02-06 05:39:32', '2021-02-06 05:39:32'),
(21, 'Lift', NULL, NULL, 25, '2021-02-06 05:39:32', '2021-02-06 05:39:32'),
(22, 'Microwave', NULL, NULL, 25, '2021-02-06 05:39:33', '2021-02-06 05:39:33'),
(23, 'CCTV', NULL, NULL, 25, '2021-02-06 05:39:33', '2021-02-06 05:39:33'),
(24, 'Gym', NULL, NULL, 25, '2021-02-06 05:39:33', '2021-02-06 05:39:33'),
(25, 'Jacuzzi', NULL, NULL, 25, '2021-02-06 05:39:33', '2021-02-06 05:39:33'),
(26, 'Deck', NULL, NULL, 25, '2021-02-06 05:39:34', '2021-02-06 05:39:34'),
(27, 'Swimming Pool', NULL, NULL, 25, '2021-02-06 05:39:34', '2021-02-06 05:39:34'),
(28, 'Washing Machine', NULL, NULL, 25, '2021-02-06 05:39:34', '2021-02-06 05:39:34'),
(29, 'Kids Playground', NULL, NULL, 25, '2021-02-06 05:39:34', '2021-02-06 05:39:34'),
(30, 'Cafeteria', NULL, NULL, 25, '2021-02-06 05:39:36', '2021-02-06 05:39:36'),
(31, 'LCD Touchscreen', NULL, NULL, 26, '2021-02-06 05:39:36', '2021-02-06 05:39:36'),
(32, 'Power Window', NULL, NULL, 26, '2021-02-06 05:39:37', '2021-02-06 05:39:37'),
(33, 'Electric Side Mirror (ORVM)', NULL, NULL, 26, '2021-02-06 05:39:37', '2021-02-06 05:39:37'),
(34, 'Tubeless Tyres', NULL, NULL, 26, '2021-02-06 05:39:38', '2021-02-06 05:39:38'),
(35, 'Air Conditioner - Automatic', NULL, NULL, 26, '2021-02-06 05:39:39', '2021-02-06 05:39:39'),
(36, 'Air Bags', NULL, NULL, 26, '2021-02-06 05:39:39', '2021-02-06 05:39:39'),
(37, 'Anti-lock Braking (ABS)', NULL, NULL, 26, '2021-02-06 05:39:39', '2021-02-06 05:39:39'),
(38, 'Keyless Remote Entry', NULL, NULL, 26, '2021-02-06 05:39:39', '2021-02-06 05:39:39'),
(39, 'Air Conditioner - Manual', NULL, NULL, 26, '2021-02-06 05:39:40', '2021-02-06 05:39:40'),
(40, 'Power Steering', NULL, NULL, 26, '2021-02-06 05:39:41', '2021-02-06 05:39:41'),
(41, 'Steering Mounted Controls', NULL, NULL, 26, '2021-02-06 05:39:41', '2021-02-06 05:39:41'),
(42, 'Projected Headlight', NULL, NULL, 26, '2021-02-06 05:39:41', '2021-02-06 05:39:41'),
(43, 'Alloy Wheels', NULL, NULL, 26, '2021-02-06 05:39:42', '2021-02-06 05:39:42'),
(44, 'Rear AC Vent', NULL, NULL, 26, '2021-02-06 05:39:42', '2021-02-06 05:39:42'),
(45, 'Central Lock', NULL, NULL, 26, '2021-02-06 05:39:42', '2021-02-06 05:39:42'),
(46, 'Fog Lights', NULL, NULL, 26, '2021-02-06 05:39:42', '2021-02-06 05:39:42'),
(47, 'Push Engine Start Button', NULL, NULL, 26, '2021-02-06 05:39:42', '2021-02-06 05:39:42'),
(48, 'Front Disc Brake', NULL, NULL, 27, '2021-02-06 05:39:42', '2021-02-06 05:39:42'),
(49, 'Electric Start', NULL, NULL, 27, '2021-02-06 05:39:43', '2021-02-06 05:39:43'),
(50, 'Projected Headlight', NULL, NULL, 27, '2021-02-06 05:39:43', '2021-02-06 05:39:43'),
(51, 'Alloy Wheels', NULL, NULL, 27, '2021-02-06 05:39:43', '2021-02-06 05:39:43'),
(52, 'Tubeless Tyres', NULL, NULL, 27, '2021-02-06 05:39:43', '2021-02-06 05:39:43'),
(53, 'LED Tail Light', NULL, NULL, 27, '2021-02-06 05:39:43', '2021-02-06 05:39:43'),
(54, 'Split Seat', NULL, NULL, 27, '2021-02-06 05:39:43', '2021-02-06 05:39:43'),
(55, 'Rear Disc Brake', NULL, NULL, 27, '2021-02-06 05:39:43', '2021-02-06 05:39:43'),
(56, 'Low Fuel Indicator', NULL, NULL, 27, '2021-02-06 05:39:44', '2021-02-06 05:39:44'),
(57, 'Mono Suspension', NULL, NULL, 27, '2021-02-06 05:39:44', '2021-02-06 05:39:44'),
(58, 'Tripmeter', NULL, NULL, 27, '2021-02-06 05:39:45', '2021-02-06 05:39:45'),
(59, 'Anti-lock Braking (ABS)', NULL, NULL, 27, '2021-02-06 05:39:46', '2021-02-06 05:39:46'),
(60, 'Digital Display Panel', NULL, NULL, 27, '2021-02-06 05:39:46', '2021-02-06 05:39:46'),
(61, 'Sales bill', NULL, NULL, 40, '2021-02-06 05:39:46', '2021-02-06 05:39:46'),
(62, 'Stamped warranty card', NULL, NULL, 40, '2021-02-06 05:39:46', '2021-02-06 05:39:46'),
(63, 'IMEI matching box', NULL, NULL, 40, '2021-02-06 05:39:47', '2021-02-06 05:39:47'),
(64, 'Original purchase bill (for used phone)', NULL, NULL, 40, '2021-02-06 05:39:47', '2021-02-06 05:39:47'),
(65, 'Memory Card Slot', NULL, NULL, 40, '2021-02-06 05:39:47', '2021-02-06 05:39:47'),
(66, 'WiFi', NULL, NULL, 40, '2021-02-06 05:39:47', '2021-02-06 05:39:47'),
(67, 'Fingerprint Sensor', NULL, NULL, 40, '2021-02-06 05:39:47', '2021-02-06 05:39:47'),
(68, 'Gorilla Glass Screen', NULL, NULL, 40, '2021-02-06 05:39:47', '2021-02-06 05:39:47'),
(69, 'Water & Dust Proof (IP)', NULL, NULL, 40, '2021-02-06 05:39:48', '2021-02-06 05:39:48'),
(70, 'Compass Sensor', NULL, NULL, 40, '2021-02-06 05:39:48', '2021-02-06 05:39:48'),
(71, 'Heart Rate Sensor', NULL, NULL, 40, '2021-02-06 05:39:49', '2021-02-06 05:39:49'),
(72, 'Front LED Flash', NULL, NULL, 40, '2021-02-06 05:39:49', '2021-02-06 05:39:49'),
(73, 'Proximity Sensor', NULL, NULL, 40, '2021-02-06 05:39:49', '2021-02-06 05:39:49'),
(74, 'GPS', NULL, NULL, 40, '2021-02-06 05:39:49', '2021-02-06 05:39:49'),
(75, 'Dual Camera - Back', NULL, NULL, 40, '2021-02-06 05:39:50', '2021-02-06 05:39:50'),
(76, 'Dual Camera - Front', NULL, NULL, 40, '2021-02-06 05:39:50', '2021-02-06 05:39:50'),
(77, 'NFC', NULL, NULL, 40, '2021-02-06 05:39:50', '2021-02-06 05:39:50'),
(78, 'SSD Storage', NULL, NULL, 49, '2021-02-06 05:39:51', '2021-02-06 05:39:51'),
(79, 'HDMI', NULL, NULL, 49, '2021-02-06 05:39:51', '2021-02-06 05:39:51'),
(80, 'DVD-CD Combo', NULL, NULL, 49, '2021-02-06 05:39:51', '2021-02-06 05:39:51'),
(81, 'Webcam', NULL, NULL, 49, '2021-02-06 05:39:51', '2021-02-06 05:39:51'),
(82, 'Blu-ray Drive', NULL, NULL, 49, '2021-02-06 05:39:51', '2021-02-06 05:39:51'),
(83, 'SD Card Slot', NULL, NULL, 49, '2021-02-06 05:39:52', '2021-02-06 05:39:52'),
(84, 'WiFi', NULL, NULL, 49, '2021-02-06 05:39:52', '2021-02-06 05:39:52'),
(85, 'Thunderbolt Port', NULL, NULL, 49, '2021-02-06 05:39:53', '2021-02-06 05:39:53'),
(86, 'DVD-CD Rewritable', NULL, NULL, 49, '2021-02-06 05:39:53', '2021-02-06 05:39:53'),
(87, 'USB Type-C Port', NULL, NULL, 49, '2021-02-06 05:39:54', '2021-02-06 05:39:54'),
(88, 'Touch Screen', NULL, NULL, 49, '2021-02-06 05:39:54', '2021-02-06 05:39:54'),
(89, 'Bluetooth', NULL, NULL, 49, '2021-02-06 05:39:54', '2021-02-06 05:39:54'),
(90, 'Fingerprint Recognition', NULL, NULL, 49, '2021-02-06 05:39:55', '2021-02-06 05:39:55'),
(91, 'Aluminium Metal Body', NULL, NULL, 49, '2021-02-06 05:39:55', '2021-02-06 05:39:55'),
(92, 'Surround Speaker', NULL, NULL, 49, '2021-02-06 05:39:56', '2021-02-06 05:39:56'),
(93, 'Backlit Keyboard', NULL, NULL, 49, '2021-02-06 05:39:56', '2021-02-06 05:39:56');

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE `applicants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `application_letter` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `astatus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `job_id` bigint(20) UNSIGNED DEFAULT NULL,
  `mail_status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`id`, `application_letter`, `astatus`, `user_id`, `job_id`, `mail_status`, `created_at`, `updated_at`) VALUES
(2, 'this is cover letter', 'pending', 28, 11, 0, '2021-03-24 11:14:40', '2021-03-24 11:14:40');

-- --------------------------------------------------------

--
-- Table structure for table `applicant_jobs`
--

CREATE TABLE `applicant_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `applicant_user_id` bigint(20) UNSIGNED NOT NULL,
  `job_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `applicant_jobs`
--

INSERT INTO `applicant_jobs` (`id`, `applicant_user_id`, `job_id`, `created_at`, `updated_at`) VALUES
(2, 28, 11, '2021-03-24 11:14:40', '2021-03-24 11:14:40');

-- --------------------------------------------------------

--
-- Table structure for table `assureds`
--

CREATE TABLE `assureds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assureds`
--

INSERT INTO `assureds` (`id`, `name`, `summary`, `desc`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Not Assured', 'Not assured Item. This might be second hand product.', 'At Mero Offer we are 100% committed to making sure that your experience on our site is as safe as possible. Mero Offer is not responsible for transaction of any goods, services listed in this website. It is just platform to share information. \r\n\r\nIf you feel that you have been the victim of a scam, please report your situation to us immediately. If you have been cheated, we also recommend that you contact your local police department.\r\n\r\nMero Offer is committed to ensuring the privacy of its users and therefore does not share information about its users publicly. However, we are committed to the safety of our users and will cooperate with the police department should we receive any requests for information in connection with fraudulent or other criminal activity.', 'not-assured', 1, '2021-02-06 05:34:59', '2021-03-30 10:21:45'),
(2, 'Food Assurance', '- Quality Food\r\n- Registered Company', 'The given post is operated itself by its own owner. Our services are limited to share information regarding food to their customers and owners. Mero Offer is no way responsible for the quality of the foods and their packaging as offered by the home chefs or restaurant.', 'food-assurance', 1, '2021-02-06 05:34:59', '2021-02-26 02:40:17'),
(3, 'Cake Assurance', '- Quality Food - Registered Company', 'The given post is operated itself by its own owner. Our services are limited to share information regarding food to their customers and owners. Mero Offer is no way responsible for the quality of the cakes.\r\n\r\nSafety Instructions for Cake:\r\nStore the cake into freezer as soon as you receive it. The cake will taste best only when it is chilled.\r\nWe delivery freshly made cakes. You can safely store the cake for days if you refrigerate it in deep freezer without any other food.\r\nPlease check for any non-edible cake support that might be used in the cake before cutting.\r\nCake can contain wire supports or toothpicks or straws for support. DO NOT SMASH THE CAKE ON FACE without confirming.\r\nWater soluble food color can transfer into tongue easily. That is normal behavior.\r\nFondant cakes might not be cut by plastic knife. Use a serated metal knife instead.\r\nWe do not encourage use of plastic bags. Cakes are delivered in cardboard box. Enjoy the cake!', 'cake-assurance', 1, '2021-03-07 04:45:15', '2021-03-07 04:54:35'),
(4, 'Travel Package Assured', '- Registered Company', 'The given post is operated itself by its own owner. Our services are limited to share information regarding  professional service to their customers. Mero Offer is no way responsible for the misinformation and their professional service as offered by the company.\r\n\r\nAt Mero Offer we are 100% committed to making sure that your experience on our site is as safe as possible. Mero Offer is not responsible for the services listed in this website. It is just platform to share information. As it is operated by the owner itself.\r\n\r\nIf you feel that you have been the victim of a scam, please report your situation to us immediately. If you have been cheated, we also recommend that you contact your local police department.\r\n\r\nMero Offer is committed to ensuring the privacy of its users and therefore does not share information about its users publicly. However, we are committed to the safety of our users and will cooperate with the police department should we receive any requests for information in connection with fraudulent or other criminal activity.', '2-travel-package-assured', 1, '2021-04-08 08:55:29', '2021-04-08 08:59:06');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `font` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `filterable` tinyint(1) NOT NULL DEFAULT 0,
  `has_combin` tinyint(1) NOT NULL DEFAULT 0,
  `data_type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `interval` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `code`, `name`, `image`, `font`, `type`, `required`, `filterable`, `has_combin`, `data_type`, `order`, `created_at`, `updated_at`, `interval`) VALUES
(1, 'property_type', 'Property Type', NULL, NULL, 'radio', 1, 1, 0, 'required|numeric', 1, '2021-02-06 05:37:13', '2021-02-06 05:37:13', 0),
(2, 'property_face', 'Property Face', NULL, NULL, 'dropdown', 0, 1, 0, 'nullable|numeric', 2, '2021-02-06 05:37:18', '2021-02-06 05:37:18', 0),
(3, 'furnishing', 'Furnishing', NULL, NULL, 'radio', 0, 1, 0, 'nullable|numeric', 3, '2021-02-06 05:37:23', '2021-02-06 05:37:23', 0),
(4, 'road_access', 'Road Access', NULL, NULL, 'dropdown', 0, 1, 0, 'nullable|numeric', 4, '2021-02-06 05:37:26', '2021-02-06 05:37:26', 0),
(5, 'road_type', 'Road Type', NULL, NULL, 'radio', 0, 1, 0, 'nullable|numeric', 5, '2021-02-06 05:37:29', '2021-02-06 05:37:29', 0),
(6, 'total_area', 'Total Area(in aana/dhur)', NULL, NULL, 'text', 0, 1, 0, 'nullable|numeric', 6, '2021-02-06 05:37:31', '2021-03-14 10:40:35', 1),
(7, 'built_up', 'Built Up Area (sq.ft)', NULL, NULL, 'text', 0, 1, 0, 'nullable|numeric', 7, '2021-02-06 05:37:32', '2021-03-14 10:40:26', 1),
(8, 'floors', 'Floors', NULL, NULL, 'text', 0, 1, 0, 'nullable|numeric', 8, '2021-02-06 05:37:35', '2021-03-14 10:41:20', 1),
(9, 'bedroom', 'Bedroom', NULL, NULL, 'text', 0, 1, 0, 'nullable|numeric', 9, '2021-02-06 05:37:37', '2021-03-14 10:41:42', 1),
(10, 'bathroom', 'Bathroom', NULL, NULL, 'text', 0, 1, 0, 'nullable|numeric', 10, '2021-02-06 05:37:40', '2021-03-14 10:41:31', 1),
(11, 'living_room', 'Living Room', NULL, NULL, 'text', 0, 1, 0, 'nullable|numeric', 11, '2021-02-06 05:37:41', '2021-03-08 06:52:54', 0),
(12, 'car_brand', 'Brand', NULL, NULL, 'dropdown', 1, 1, 0, 'nullable|numeric', 12, '2021-02-06 05:37:43', '2021-03-08 06:52:54', 0),
(13, 'car_type', 'Type', NULL, NULL, 'dropdown', 0, 1, 0, 'nullable|numeric', 13, '2021-02-06 05:37:49', '2021-03-08 06:52:54', 0),
(14, 'car_make_year', 'Make Year', NULL, NULL, 'text', 1, 1, 0, 'required|numeric', 14, '2021-02-06 05:37:51', '2021-03-08 06:52:54', 0),
(15, 'motorbike_brand', 'Brand', NULL, NULL, 'dropdown', 1, 1, 0, 'required|numeric', 15, '2021-02-06 05:37:52', '2021-03-08 06:52:54', 0),
(16, 'motorbike_make_year', 'Make Year', NULL, NULL, 'text', 0, 1, 0, 'nullable|numeric', 16, '2021-02-06 05:37:55', '2021-03-08 06:52:54', 0),
(17, 'motorbike_type', 'Type', NULL, NULL, 'radio', 1, 1, 0, 'required|numeric', 17, '2021-02-06 05:37:56', '2021-03-08 06:52:54', 0),
(18, 'engine', 'Engine (CC)', NULL, NULL, 'text', 0, 1, 0, 'nullable|numeric', 18, '2021-02-06 05:37:59', '2021-03-08 06:52:54', 0),
(19, 'kilometers', 'Kilometers', NULL, NULL, 'text', 0, 1, 0, 'nullable|numeric', 19, '2021-02-06 05:38:01', '2021-03-08 06:52:54', 0),
(20, 'colour', 'Colour', NULL, NULL, 'text', 0, 1, 0, 'nullable|string', 20, '2021-02-06 05:38:04', '2021-03-08 06:52:54', 0),
(21, 'fuel', 'Fuel', NULL, NULL, 'radio', 0, 1, 0, 'nullable|string', 21, '2021-02-06 05:38:06', '2021-03-08 06:52:54', 0),
(22, 'transmission', 'Transmission', NULL, NULL, 'radio', 0, 1, 0, 'nullable|numeric', 22, '2021-02-06 05:38:14', '2021-03-08 06:52:54', 0),
(23, 'lot_number', 'Lot Number', NULL, NULL, 'text', 1, 1, 0, 'required|numeric', 23, '2021-02-06 05:38:19', '2021-03-08 06:52:54', 0),
(24, 'mileage', 'Mileage (km / l)', NULL, NULL, 'text', 0, 1, 0, 'nullable|numeric', 24, '2021-02-06 05:38:20', '2021-03-08 06:52:54', 0),
(25, 'real_estate_feature', 'Real Estate Amnities', NULL, NULL, 'checkbox', 0, 1, 0, 'nullable|numeric', 25, '2021-02-06 05:38:21', '2021-03-08 06:52:54', 0),
(26, 'car_feature', 'Car Amnities', NULL, NULL, 'checkbox', 0, 1, 0, 'nullable|numeric', 26, '2021-02-06 05:38:24', '2021-03-08 06:52:54', 0),
(27, 'bike_feature', 'Bike Amnities', NULL, NULL, 'checkbox', 0, 1, 0, 'nullable|numeric', 27, '2021-02-06 05:38:27', '2021-03-08 06:52:54', 0),
(28, 'cloth_type', 'Type', NULL, NULL, 'dropdown', 1, 1, 0, 'required|numeric', 28, '2021-02-06 05:38:28', '2021-03-08 06:52:54', 0),
(29, 'shoes_type', 'Type', NULL, NULL, 'dropdown', 1, 1, 0, 'required|numeric', 29, '2021-02-06 05:38:33', '2021-03-08 06:52:54', 0),
(30, 'watches_type', 'Type', NULL, NULL, 'radio', 1, 1, 0, 'required|numeric', 30, '2021-02-06 05:38:36', '2021-03-08 06:52:54', 0),
(31, 'mobile_brand', 'Brand', NULL, NULL, 'dropdown', 1, 1, 0, 'required|numeric', 31, '2021-02-06 05:38:39', '2021-03-08 06:52:54', 0),
(32, 'mobile_ram', 'RAM', NULL, NULL, 'dropdown', 0, 1, 0, 'nullable|numeric', 32, '2021-02-06 05:38:47', '2021-03-08 06:52:54', 0),
(33, 'mobile_internal_storage', 'Internal Storage', NULL, NULL, 'dropdown', 0, 1, 0, 'nullable|numeric', 33, '2021-02-06 05:38:50', '2021-03-08 06:52:54', 0),
(34, 'mobile_cpu_core', 'CPU Core', NULL, NULL, 'dropdown', 0, 1, 0, 'nullable|numeric', 34, '2021-02-06 05:38:53', '2021-03-08 06:52:54', 0),
(35, 'mobile_os', 'Operating System', NULL, NULL, 'dropdown', 0, 1, 0, 'nullable|numeric', 35, '2021-02-06 05:38:55', '2021-03-08 06:52:54', 0),
(36, 'mobile_sim_slot', 'Sim Slot', NULL, NULL, 'dropdown', 0, 1, 0, 'nullable|numeric', 36, '2021-02-06 05:38:59', '2021-03-08 06:52:54', 0),
(37, 'mobile_screen_size', 'Screen Size', NULL, NULL, 'dropdown', 0, 1, 0, 'nullable|numeric', 37, '2021-02-06 05:39:01', '2021-03-08 06:52:54', 0),
(38, 'mobile_front_camera', 'Front Camera', NULL, NULL, 'dropdown', 0, 1, 0, 'nullable|numeric', 38, '2021-02-06 05:39:04', '2021-03-08 06:52:54', 0),
(39, 'mobile_back_camera', 'Back Camera', NULL, NULL, 'dropdown', 0, 1, 0, 'nullable|numeric', 39, '2021-02-06 05:39:06', '2021-03-08 06:52:54', 0),
(40, 'mobile_amnities', 'Mobile Features', NULL, NULL, 'checkbox', 0, 0, 0, 'nullable|numeric', 40, '2021-02-06 05:39:08', '2021-03-08 06:52:54', 0),
(41, 'laptop_ram', 'RAM(GB)', NULL, NULL, 'text', 1, 1, 0, 'required|numeric', 41, '2021-02-06 05:39:09', '2021-03-08 06:52:54', 0),
(42, 'laptop_hdd', 'HDD (GB/TB)', NULL, NULL, 'text', 1, 1, 0, 'required|numeric', 42, '2021-02-06 05:39:09', '2021-03-08 06:52:54', 0),
(43, 'laptop_video_card', 'Video Card', NULL, NULL, 'text', 0, 1, 0, 'nullable|numeric', 43, '2021-02-06 05:39:10', '2021-03-08 06:52:54', 0),
(44, 'laptop_screen_size', 'Screen Size (inch)', NULL, NULL, 'text', 0, 1, 0, 'nullable|numeric', 44, '2021-02-06 05:39:10', '2021-03-08 06:52:54', 0),
(45, 'laptop_processor', 'Processor', NULL, NULL, 'dropdown', 1, 1, 0, 'required|numeric', 45, '2021-02-06 05:39:10', '2021-03-08 06:52:54', 0),
(46, 'laptop_processor_generation', 'Processor Generation', NULL, NULL, 'dropdown', 0, 1, 0, 'nullable|numeric', 46, '2021-02-06 05:39:15', '2021-03-08 06:52:54', 0),
(47, 'laptop_screen_type', 'Screen Type', NULL, NULL, 'radio', 0, 1, 0, 'nullable|numeric', 47, '2021-02-06 05:39:18', '2021-03-08 06:52:54', 0),
(48, 'laptop_battery', 'Battery', NULL, NULL, 'dropdown', 0, 1, 0, 'nullable|numeric', 48, '2021-02-06 05:39:19', '2021-03-08 06:52:54', 0),
(49, 'laptop_amnities', 'Laptop Features', NULL, NULL, 'checkbox', 0, 0, 0, 'nullable|numeric', 49, '2021-02-06 05:39:23', '2021-03-08 06:52:54', 0),
(50, 'chicken_sadeko', 'Chicken Sandheko', NULL, NULL, 'dropdown', 0, 0, 1, 'nullable|numeric', 50, '2021-02-22 03:22:40', '2021-03-08 06:52:54', 0),
(52, 'sunday_menu', 'Sunday', NULL, NULL, 'dropdown', 0, 0, 1, 'nullable|numeric', 51, '2021-02-23 05:43:24', '2021-03-08 06:52:54', 0),
(53, 'monday_menu', 'Monday', NULL, NULL, 'dropdown', 0, 0, 1, 'nullable|numeric', 53, '2021-02-23 05:49:12', '2021-03-08 06:30:46', 0),
(54, 'tuesday_menu', 'Tuesday', NULL, NULL, 'dropdown', 0, 0, 1, 'nullable|numeric', 54, '2021-02-23 05:49:31', '2021-03-08 06:30:46', 0),
(55, 'wednesday_menu', 'Wednesday', NULL, NULL, 'dropdown', 0, 0, 1, 'nullable|numeric', 55, '2021-02-23 05:49:54', '2021-03-08 06:30:46', 0),
(56, 'thursday', 'Thursday', NULL, NULL, 'dropdown', 0, 0, 1, 'nullable|numeric', 56, '2021-02-23 05:50:21', '2021-03-08 06:30:46', 0),
(57, 'friday', 'Friday', NULL, NULL, 'dropdown', 0, 0, 1, 'nullable|numeric', 57, '2021-02-23 05:50:49', '2021-03-08 06:30:46', 0);

-- --------------------------------------------------------

--
-- Table structure for table `attributevalues`
--

CREATE TABLE `attributevalues` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `font` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributevalues`
--

INSERT INTO `attributevalues` (`id`, `value`, `image`, `font`, `attribute_id`, `created_at`, `updated_at`) VALUES
(1, 'Residential', NULL, NULL, 1, '2021-02-06 05:37:17', '2021-02-06 05:37:17'),
(2, 'Commercial', NULL, NULL, 1, '2021-02-06 05:37:17', '2021-02-06 05:37:17'),
(3, 'Agricultural', NULL, NULL, 1, '2021-02-06 05:37:17', '2021-02-06 05:37:17'),
(4, 'East', NULL, NULL, 2, '2021-02-06 05:37:21', '2021-02-06 05:37:21'),
(5, 'West', NULL, NULL, 2, '2021-02-06 05:37:21', '2021-02-06 05:37:21'),
(6, 'North', NULL, NULL, 2, '2021-02-06 05:37:21', '2021-02-06 05:37:21'),
(7, 'South', NULL, NULL, 2, '2021-02-06 05:37:22', '2021-02-06 05:37:22'),
(8, 'South-East', NULL, NULL, 2, '2021-02-06 05:37:22', '2021-02-06 05:37:22'),
(9, 'South-West', NULL, NULL, 2, '2021-02-06 05:37:23', '2021-02-06 05:37:23'),
(10, 'North-East', NULL, NULL, 2, '2021-02-06 05:37:23', '2021-02-06 05:37:23'),
(11, 'North-West', NULL, NULL, 2, '2021-02-06 05:37:23', '2021-02-06 05:37:23'),
(12, 'Semi', NULL, NULL, 3, '2021-02-06 05:37:25', '2021-02-06 05:37:25'),
(13, 'Non', NULL, NULL, 3, '2021-02-06 05:37:25', '2021-02-06 05:37:25'),
(14, 'Full', NULL, NULL, 3, '2021-02-06 05:37:26', '2021-02-06 05:37:26'),
(15, 'Less than 5 feet', NULL, NULL, 4, '2021-02-06 05:37:28', '2021-02-06 05:37:28'),
(16, '5 to 8 Feet', NULL, NULL, 4, '2021-02-06 05:37:28', '2021-02-06 05:37:28'),
(17, '9 to 12 Feet', NULL, NULL, 4, '2021-02-06 05:37:28', '2021-02-06 05:37:28'),
(18, '13 to 20 Feet', NULL, NULL, 4, '2021-02-06 05:37:28', '2021-02-06 05:37:28'),
(19, 'Above 20 Feet', NULL, NULL, 4, '2021-02-06 05:37:28', '2021-02-06 05:37:28'),
(20, 'Goreto Bato', NULL, NULL, 4, '2021-02-06 05:37:29', '2021-02-06 05:37:29'),
(21, 'No Road Access', NULL, NULL, 4, '2021-02-06 05:37:29', '2021-02-06 05:37:29'),
(22, 'Soil Stabilized', NULL, NULL, 5, '2021-02-06 05:37:31', '2021-02-06 05:37:31'),
(23, 'Paved', NULL, NULL, 5, '2021-02-06 05:37:31', '2021-02-06 05:37:31'),
(24, 'Gavelled', NULL, NULL, 5, '2021-02-06 05:37:31', '2021-02-06 05:37:31'),
(25, 'Blacktopped', NULL, NULL, 5, '2021-02-06 05:37:31', '2021-02-06 05:37:31'),
(26, 'Alley', NULL, NULL, 5, '2021-02-06 05:37:31', '2021-02-06 05:37:31'),
(27, 'Chevrolet', NULL, NULL, 12, '2021-02-06 05:37:44', '2021-02-06 05:37:44'),
(28, 'Daihatsu', NULL, NULL, 12, '2021-02-06 05:37:44', '2021-02-06 05:37:44'),
(29, 'Datsun', NULL, NULL, 12, '2021-02-06 05:37:45', '2021-02-06 05:37:45'),
(30, 'Eicher', NULL, NULL, 12, '2021-02-06 05:37:45', '2021-02-06 05:37:45'),
(31, 'Fiat', NULL, NULL, 12, '2021-02-06 05:37:45', '2021-02-06 05:37:45'),
(32, 'Ford', NULL, NULL, 12, '2021-02-06 05:37:45', '2021-02-06 05:37:45'),
(33, 'Geely', NULL, NULL, 12, '2021-02-06 05:37:46', '2021-02-06 05:37:46'),
(34, 'Honda', NULL, NULL, 12, '2021-02-06 05:37:46', '2021-02-06 05:37:46'),
(35, 'Hyundai', NULL, NULL, 12, '2021-02-06 05:37:46', '2021-02-06 05:37:46'),
(36, 'Kia', NULL, NULL, 12, '2021-02-06 05:37:46', '2021-02-06 05:37:46'),
(37, 'Land Rover', NULL, NULL, 12, '2021-02-06 05:37:47', '2021-02-06 05:37:47'),
(38, 'Mahindra', NULL, NULL, 12, '2021-02-06 05:37:47', '2021-02-06 05:37:47'),
(39, 'Maruti Suzuki', NULL, NULL, 12, '2021-02-06 05:37:47', '2021-02-06 05:37:47'),
(40, 'Mazda', NULL, NULL, 12, '2021-02-06 05:37:47', '2021-02-06 05:37:47'),
(41, 'Mitsubishi', NULL, NULL, 12, '2021-02-06 05:37:47', '2021-02-06 05:37:47'),
(42, 'Nissan', NULL, NULL, 12, '2021-02-06 05:37:47', '2021-02-06 05:37:47'),
(43, 'Other Brands', NULL, NULL, 12, '2021-02-06 05:37:48', '2021-02-06 05:37:48'),
(44, 'Other Chinese Brands', NULL, NULL, 12, '2021-02-06 05:37:48', '2021-02-06 05:37:48'),
(45, 'Perodua', NULL, NULL, 12, '2021-02-06 05:37:48', '2021-02-06 05:37:48'),
(46, 'Renault', NULL, NULL, 12, '2021-02-06 05:37:48', '2021-02-06 05:37:48'),
(47, 'Skoda', NULL, NULL, 12, '2021-02-06 05:37:48', '2021-02-06 05:37:48'),
(48, 'Ssangyong', NULL, NULL, 12, '2021-02-06 05:37:48', '2021-02-06 05:37:48'),
(49, 'Tata', NULL, NULL, 12, '2021-02-06 05:37:48', '2021-02-06 05:37:48'),
(50, 'Toyota', NULL, NULL, 12, '2021-02-06 05:37:49', '2021-02-06 05:37:49'),
(51, 'Volkswagen', NULL, NULL, 12, '2021-02-06 05:37:49', '2021-02-06 05:37:49'),
(52, 'Small Hatchback', NULL, NULL, 13, '2021-02-06 05:37:50', '2021-02-06 05:37:50'),
(53, 'Mid Size Hatchback', NULL, NULL, 13, '2021-02-06 05:37:50', '2021-02-06 05:37:50'),
(54, 'Sedan', NULL, NULL, 13, '2021-02-06 05:37:50', '2021-02-06 05:37:50'),
(55, 'CUV / Compact SUV', NULL, NULL, 13, '2021-02-06 05:37:50', '2021-02-06 05:37:50'),
(56, 'Jeep / SUV', NULL, NULL, 13, '2021-02-06 05:37:51', '2021-02-06 05:37:51'),
(57, 'PickUp', NULL, NULL, 13, '2021-02-06 05:37:51', '2021-02-06 05:37:51'),
(58, 'Others', NULL, NULL, 13, '2021-02-06 05:37:51', '2021-02-06 05:37:51'),
(59, 'Vespa', NULL, NULL, 15, '2021-02-06 05:37:52', '2021-02-06 05:37:52'),
(60, 'Yamaha', NULL, NULL, 15, '2021-02-06 05:37:52', '2021-02-06 05:37:52'),
(61, 'TVS', NULL, NULL, 15, '2021-02-06 05:37:53', '2021-02-06 05:37:53'),
(62, 'Honda', NULL, NULL, 15, '2021-02-06 05:37:53', '2021-02-06 05:37:53'),
(63, 'Hero', NULL, NULL, 15, '2021-02-06 05:37:53', '2021-02-06 05:37:53'),
(64, 'Bajaj', NULL, NULL, 15, '2021-02-06 05:37:53', '2021-02-06 05:37:53'),
(65, 'Apollo - Rieju', NULL, NULL, 15, '2021-02-06 05:37:53', '2021-02-06 05:37:53'),
(66, 'Aprilia', NULL, NULL, 15, '2021-02-06 05:37:54', '2021-02-06 05:37:54'),
(67, 'Um', NULL, NULL, 15, '2021-02-06 05:37:54', '2021-02-06 05:37:54'),
(68, 'Royal Enfield', NULL, NULL, 15, '2021-02-06 05:37:54', '2021-02-06 05:37:54'),
(69, 'Benelli', NULL, NULL, 15, '2021-02-06 05:37:54', '2021-02-06 05:37:54'),
(70, 'Crossfire', NULL, NULL, 15, '2021-02-06 05:37:54', '2021-02-06 05:37:54'),
(71, 'Hartford', NULL, NULL, 15, '2021-02-06 05:37:54', '2021-02-06 05:37:54'),
(72, 'KTM', NULL, NULL, 15, '2021-02-06 05:37:55', '2021-02-06 05:37:55'),
(73, 'Suzuki', NULL, NULL, 15, '2021-02-06 05:37:55', '2021-02-06 05:37:55'),
(74, 'Mahindra', NULL, NULL, 15, '2021-02-06 05:37:55', '2021-02-06 05:37:55'),
(75, 'Other Electric Bike', NULL, NULL, 15, '2021-02-06 05:37:55', '2021-02-06 05:37:55'),
(76, 'Other Motorcycle Brands', NULL, NULL, 15, '2021-02-06 05:37:55', '2021-02-06 05:37:55'),
(77, 'Standard', NULL, NULL, 17, '2021-02-06 05:37:57', '2021-02-06 05:37:57'),
(78, 'Cruiser', NULL, NULL, 17, '2021-02-06 05:37:57', '2021-02-06 05:37:57'),
(79, 'Sports', NULL, NULL, 17, '2021-02-06 05:37:57', '2021-02-06 05:37:57'),
(80, 'Dirt', NULL, NULL, 17, '2021-02-06 05:37:58', '2021-02-06 05:37:58'),
(81, 'Scooty', NULL, NULL, 17, '2021-02-06 05:37:58', '2021-02-06 05:37:58'),
(82, 'Petrol', NULL, NULL, 21, '2021-02-06 05:38:09', '2021-02-06 05:38:09'),
(83, 'Diesel', NULL, NULL, 21, '2021-02-06 05:38:11', '2021-02-06 05:38:11'),
(84, 'Electric', NULL, NULL, 21, '2021-02-06 05:38:12', '2021-02-06 05:38:12'),
(85, 'Hybrid', NULL, NULL, 21, '2021-02-06 05:38:13', '2021-02-06 05:38:13'),
(86, 'Manual Gear-2WD', NULL, NULL, 22, '2021-02-06 05:38:18', '2021-02-06 05:38:18'),
(87, 'Manual Gear-4WD', NULL, NULL, 22, '2021-02-06 05:38:18', '2021-02-06 05:38:18'),
(88, 'Automatic Gear-2WD', NULL, NULL, 22, '2021-02-06 05:38:19', '2021-02-06 05:38:19'),
(89, 'Automatic Gear-4WD', NULL, NULL, 22, '2021-02-06 05:38:19', '2021-02-06 05:38:19'),
(90, 'Jacket & Coats', NULL, NULL, 28, '2021-02-06 05:38:30', '2021-02-06 05:38:30'),
(91, 'Hoodie & Sweat Shirts', NULL, NULL, 28, '2021-02-06 05:38:30', '2021-02-06 05:38:30'),
(92, 'T-shirt', NULL, NULL, 28, '2021-02-06 05:38:31', '2021-02-06 05:38:31'),
(93, 'Shirt', NULL, NULL, 28, '2021-02-06 05:38:31', '2021-02-06 05:38:31'),
(94, 'Jeans', NULL, NULL, 28, '2021-02-06 05:38:32', '2021-02-06 05:38:32'),
(95, 'Polo', NULL, NULL, 28, '2021-02-06 05:38:32', '2021-02-06 05:38:32'),
(96, 'Pants', NULL, NULL, 28, '2021-02-06 05:38:32', '2021-02-06 05:38:32'),
(97, 'Leggings', NULL, NULL, 28, '2021-02-06 05:38:32', '2021-02-06 05:38:32'),
(98, 'Party Wear', NULL, NULL, 28, '2021-02-06 05:38:32', '2021-02-06 05:38:32'),
(99, 'Sweaters & Cardigans', NULL, NULL, 28, '2021-02-06 05:38:33', '2021-02-06 05:38:33'),
(100, 'Tops', NULL, NULL, 28, '2021-02-06 05:38:33', '2021-02-06 05:38:33'),
(101, 'Shorts', NULL, NULL, 28, '2021-02-06 05:38:33', '2021-02-06 05:38:33'),
(102, 'Dress & Skirts', NULL, NULL, 28, '2021-02-06 05:38:33', '2021-02-06 05:38:33'),
(103, 'Undergarments', NULL, NULL, 28, '2021-02-06 05:38:33', '2021-02-06 05:38:33'),
(104, 'Sports Wear', NULL, NULL, 28, '2021-02-06 05:38:33', '2021-02-06 05:38:33'),
(105, 'Others', NULL, NULL, 28, '2021-02-06 05:38:33', '2021-02-06 05:38:33'),
(106, 'Sports', NULL, NULL, 29, '2021-02-06 05:38:34', '2021-02-06 05:38:34'),
(107, 'Casual', NULL, NULL, 29, '2021-02-06 05:38:34', '2021-02-06 05:38:34'),
(108, 'Boot', NULL, NULL, 29, '2021-02-06 05:38:34', '2021-02-06 05:38:34'),
(109, 'Formal', NULL, NULL, 29, '2021-02-06 05:38:34', '2021-02-06 05:38:34'),
(110, 'Canvas', NULL, NULL, 29, '2021-02-06 05:38:34', '2021-02-06 05:38:34'),
(111, 'Flat', NULL, NULL, 29, '2021-02-06 05:38:34', '2021-02-06 05:38:34'),
(112, 'Heel', NULL, NULL, 29, '2021-02-06 05:38:35', '2021-02-06 05:38:35'),
(113, 'Sandal', NULL, NULL, 29, '2021-02-06 05:38:35', '2021-02-06 05:38:35'),
(114, 'Wedges', NULL, NULL, 29, '2021-02-06 05:38:35', '2021-02-06 05:38:35'),
(115, 'Others', NULL, NULL, 29, '2021-02-06 05:38:36', '2021-02-06 05:38:36'),
(116, 'Ladies', NULL, NULL, 30, '2021-02-06 05:38:38', '2021-02-06 05:38:38'),
(117, 'Gents', NULL, NULL, 30, '2021-02-06 05:38:38', '2021-02-06 05:38:38'),
(118, 'Both', NULL, NULL, 30, '2021-02-06 05:38:39', '2021-02-06 05:38:39'),
(119, 'Oppo', NULL, NULL, 31, '2021-02-06 05:38:40', '2021-02-06 05:38:40'),
(120, 'OnePlus', NULL, NULL, 31, '2021-02-06 05:38:41', '2021-02-06 05:38:41'),
(121, 'Vivo', NULL, NULL, 31, '2021-02-06 05:38:41', '2021-02-06 05:38:41'),
(122, 'Samsung', NULL, NULL, 31, '2021-02-06 05:38:41', '2021-02-06 05:38:41'),
(123, 'Mi(Xiaomi)', NULL, NULL, 31, '2021-02-06 05:38:41', '2021-02-06 05:38:41'),
(124, 'Huawei - Honor', NULL, NULL, 31, '2021-02-06 05:38:42', '2021-02-06 05:38:42'),
(125, 'HTC', NULL, NULL, 31, '2021-02-06 05:38:43', '2021-02-06 05:38:43'),
(126, 'Sony', NULL, NULL, 31, '2021-02-06 05:38:43', '2021-02-06 05:38:43'),
(127, 'Apple', NULL, NULL, 31, '2021-02-06 05:38:43', '2021-02-06 05:38:43'),
(128, 'Colors', NULL, NULL, 31, '2021-02-06 05:38:43', '2021-02-06 05:38:43'),
(129, 'Blackberry', NULL, NULL, 31, '2021-02-06 05:38:44', '2021-02-06 05:38:44'),
(130, 'Gionee', NULL, NULL, 31, '2021-02-06 05:38:44', '2021-02-06 05:38:44'),
(131, 'Asus', NULL, NULL, 31, '2021-02-06 05:38:44', '2021-02-06 05:38:44'),
(132, 'Lava-Xolo', NULL, NULL, 31, '2021-02-06 05:38:44', '2021-02-06 05:38:44'),
(133, 'Google', NULL, NULL, 31, '2021-02-06 05:38:44', '2021-02-06 05:38:44'),
(134, 'Micromax', NULL, NULL, 31, '2021-02-06 05:38:45', '2021-02-06 05:38:45'),
(135, 'Nokia', NULL, NULL, 31, '2021-02-06 05:38:45', '2021-02-06 05:38:45'),
(136, 'Motorola', NULL, NULL, 31, '2021-02-06 05:38:45', '2021-02-06 05:38:45'),
(137, 'LG', NULL, NULL, 31, '2021-02-06 05:38:46', '2021-02-06 05:38:46'),
(138, 'Lenovo', NULL, NULL, 31, '2021-02-06 05:38:46', '2021-02-06 05:38:46'),
(139, 'Other Brands', NULL, NULL, 31, '2021-02-06 05:38:46', '2021-02-06 05:38:46'),
(140, 'Other Copy-Clone Phones', NULL, NULL, 31, '2021-02-06 05:38:46', '2021-02-06 05:38:46'),
(141, '512 MB Or Less', NULL, NULL, 32, '2021-02-06 05:38:48', '2021-02-06 05:38:48'),
(142, '1 GB', NULL, NULL, 32, '2021-02-06 05:38:48', '2021-02-06 05:38:48'),
(143, '1.5 GB', NULL, NULL, 32, '2021-02-06 05:38:48', '2021-02-06 05:38:48'),
(144, '2 GB', NULL, NULL, 32, '2021-02-06 05:38:48', '2021-02-06 05:38:48'),
(145, '3 GB', NULL, NULL, 32, '2021-02-06 05:38:48', '2021-02-06 05:38:48'),
(146, '4 GB', NULL, NULL, 32, '2021-02-06 05:38:49', '2021-02-06 05:38:49'),
(147, '6 GB', NULL, NULL, 32, '2021-02-06 05:38:49', '2021-02-06 05:38:49'),
(148, '8 GB or More', NULL, NULL, 32, '2021-02-06 05:38:49', '2021-02-06 05:38:49'),
(149, '16 GB', NULL, NULL, 32, '2021-02-06 05:38:49', '2021-02-06 05:38:49'),
(150, 'Less than 512 MB', NULL, NULL, 33, '2021-02-06 05:38:50', '2021-02-06 05:38:50'),
(151, '512 MB', NULL, NULL, 33, '2021-02-06 05:38:51', '2021-02-06 05:38:51'),
(152, '1 GB', NULL, NULL, 33, '2021-02-06 05:38:51', '2021-02-06 05:38:51'),
(153, '2 GB', NULL, NULL, 33, '2021-02-06 05:38:51', '2021-02-06 05:38:51'),
(154, '4 GB', NULL, NULL, 33, '2021-02-06 05:38:51', '2021-02-06 05:38:51'),
(155, '8 GB', NULL, NULL, 33, '2021-02-06 05:38:51', '2021-02-06 05:38:51'),
(156, '16 GB', NULL, NULL, 33, '2021-02-06 05:38:51', '2021-02-06 05:38:51'),
(157, '32 GB', NULL, NULL, 33, '2021-02-06 05:38:52', '2021-02-06 05:38:52'),
(158, '64 GB', NULL, NULL, 33, '2021-02-06 05:38:52', '2021-02-06 05:38:52'),
(159, '128 GB', NULL, NULL, 33, '2021-02-06 05:38:52', '2021-02-06 05:38:52'),
(160, '256 GB or more', NULL, NULL, 33, '2021-02-06 05:38:53', '2021-02-06 05:38:53'),
(161, '512 GB', NULL, NULL, 33, '2021-02-06 05:38:53', '2021-02-06 05:38:53'),
(162, 'Single', NULL, NULL, 34, '2021-02-06 05:38:53', '2021-02-06 05:38:53'),
(163, 'Dual - 2', NULL, NULL, 34, '2021-02-06 05:38:53', '2021-02-06 05:38:53'),
(164, 'Quad - 4', NULL, NULL, 34, '2021-02-06 05:38:53', '2021-02-06 05:38:53'),
(165, 'Hexa - 6', NULL, NULL, 34, '2021-02-06 05:38:53', '2021-02-06 05:38:53'),
(166, 'Octa - 8', NULL, NULL, 34, '2021-02-06 05:38:54', '2021-02-06 05:38:54'),
(167, 'Deca - 10', NULL, NULL, 34, '2021-02-06 05:38:54', '2021-02-06 05:38:54'),
(168, 'Android 5.0 (Lollipop) or below', NULL, NULL, 35, '2021-02-06 05:38:55', '2021-02-06 05:38:55'),
(169, 'Android 6.0 (Marshmarllow)', NULL, NULL, 35, '2021-02-06 05:38:55', '2021-02-06 05:38:55'),
(170, 'Android 7.0 (Nougat)', NULL, NULL, 35, '2021-02-06 05:38:56', '2021-02-06 05:38:56'),
(171, 'Android 8.0 (Oreo)', NULL, NULL, 35, '2021-02-06 05:38:56', '2021-02-06 05:38:56'),
(172, 'Android 9.0 (Pie)', NULL, NULL, 35, '2021-02-06 05:38:56', '2021-02-06 05:38:56'),
(173, 'Android 10 (Q)', NULL, NULL, 35, '2021-02-06 05:38:56', '2021-02-06 05:38:56'),
(174, 'Apple iOS 9 or below', NULL, NULL, 35, '2021-02-06 05:38:56', '2021-02-06 05:38:56'),
(175, 'Apple iOS 10', NULL, NULL, 35, '2021-02-06 05:38:57', '2021-02-06 05:38:57'),
(176, 'Apple iOS 11', NULL, NULL, 35, '2021-02-06 05:38:57', '2021-02-06 05:38:57'),
(177, 'Apple iOS 12', NULL, NULL, 35, '2021-02-06 05:38:58', '2021-02-06 05:38:58'),
(178, 'Apple iOS 13', NULL, NULL, 35, '2021-02-06 05:38:58', '2021-02-06 05:38:58'),
(179, 'Windows 10', NULL, NULL, 35, '2021-02-06 05:38:58', '2021-02-06 05:38:58'),
(180, 'Windows 8.x or below', NULL, NULL, 35, '2021-02-06 05:38:58', '2021-02-06 05:38:58'),
(181, 'Symbian', NULL, NULL, 35, '2021-02-06 05:38:58', '2021-02-06 05:38:58'),
(182, 'RIM Blackberry', NULL, NULL, 35, '2021-02-06 05:38:58', '2021-02-06 05:38:58'),
(183, 'Firefox', NULL, NULL, 35, '2021-02-06 05:38:59', '2021-02-06 05:38:59'),
(184, 'Tizen', NULL, NULL, 35, '2021-02-06 05:38:59', '2021-02-06 05:38:59'),
(185, 'Other OS', NULL, NULL, 35, '2021-02-06 05:38:59', '2021-02-06 05:38:59'),
(186, 'Single Sim - 2G', NULL, NULL, 36, '2021-02-06 05:39:00', '2021-02-06 05:39:00'),
(187, 'Single Sim - 3G', NULL, NULL, 36, '2021-02-06 05:39:00', '2021-02-06 05:39:00'),
(188, 'Single Sim - 4G (LTE)', NULL, NULL, 36, '2021-02-06 05:39:00', '2021-02-06 05:39:00'),
(189, 'Single Sim - CDMA', NULL, NULL, 36, '2021-02-06 05:39:00', '2021-02-06 05:39:00'),
(190, 'Dual Sim - 2G + 2G', NULL, NULL, 36, '2021-02-06 05:39:01', '2021-02-06 05:39:01'),
(191, 'Dual Sim - 3G + 2G', NULL, NULL, 36, '2021-02-06 05:39:01', '2021-02-06 05:39:01'),
(192, 'Dual Sim - 4G + 3G', NULL, NULL, 36, '2021-02-06 05:39:01', '2021-02-06 05:39:01'),
(193, 'Dual Sim - 4G + 4G', NULL, NULL, 36, '2021-02-06 05:39:01', '2021-02-06 05:39:01'),
(194, 'Dual Sim - GSM + CDMA', NULL, NULL, 36, '2021-02-06 05:39:01', '2021-02-06 05:39:01'),
(195, 'Triple Sim', NULL, NULL, 36, '2021-02-06 05:39:01', '2021-02-06 05:39:01'),
(196, 'Less than 4 inch', NULL, NULL, 37, '2021-02-06 05:39:02', '2021-02-06 05:39:02'),
(197, '4.0 to 4.4 inch', NULL, NULL, 37, '2021-02-06 05:39:03', '2021-02-06 05:39:03'),
(198, '4.5 to 4.9 inch', NULL, NULL, 37, '2021-02-06 05:39:03', '2021-02-06 05:39:03'),
(199, '5.0 to 5.4 inch', NULL, NULL, 37, '2021-02-06 05:39:03', '2021-02-06 05:39:03'),
(200, '5.5 to 5.9 inch', NULL, NULL, 37, '2021-02-06 05:39:04', '2021-02-06 05:39:04'),
(201, '6.0 to 6.4 inch', NULL, NULL, 37, '2021-02-06 05:39:04', '2021-02-06 05:39:04'),
(202, '6.5 inch or More', NULL, NULL, 37, '2021-02-06 05:39:04', '2021-02-06 05:39:04'),
(203, 'No', NULL, NULL, 38, '2021-02-06 05:39:05', '2021-02-06 05:39:05'),
(204, 'VGA', NULL, NULL, 38, '2021-02-06 05:39:05', '2021-02-06 05:39:05'),
(205, '1 MP', NULL, NULL, 38, '2021-02-06 05:39:05', '2021-02-06 05:39:05'),
(206, '2 MP', NULL, NULL, 38, '2021-02-06 05:39:05', '2021-02-06 05:39:05'),
(207, '3 MP', NULL, NULL, 38, '2021-02-06 05:39:06', '2021-02-06 05:39:06'),
(208, '5 MP', NULL, NULL, 38, '2021-02-06 05:39:06', '2021-02-06 05:39:06'),
(209, '8 MP', NULL, NULL, 38, '2021-02-06 05:39:06', '2021-02-06 05:39:06'),
(210, '13 MP or More', NULL, NULL, 38, '2021-02-06 05:39:06', '2021-02-06 05:39:06'),
(211, 'No', NULL, NULL, 39, '2021-02-06 05:39:07', '2021-02-06 05:39:07'),
(212, '1 MP or Less', NULL, NULL, 39, '2021-02-06 05:39:07', '2021-02-06 05:39:07'),
(213, '2 MP - 2.9 MP', NULL, NULL, 39, '2021-02-06 05:39:07', '2021-02-06 05:39:07'),
(214, '3 MP - 4.9 MP', NULL, NULL, 39, '2021-02-06 05:39:07', '2021-02-06 05:39:07'),
(215, '5 MP - 7.9 MP', NULL, NULL, 39, '2021-02-06 05:39:08', '2021-02-06 05:39:08'),
(216, '8 MP - 12.9 MP', NULL, NULL, 39, '2021-02-06 05:39:08', '2021-02-06 05:39:08'),
(217, '13 MP - 19.9 MP', NULL, NULL, 39, '2021-02-06 05:39:08', '2021-02-06 05:39:08'),
(218, '20 MP or More', NULL, NULL, 39, '2021-02-06 05:39:08', '2021-02-06 05:39:08'),
(219, 'Intel Pentium 4 or Lower', NULL, NULL, 45, '2021-02-06 05:39:11', '2021-02-06 05:39:11'),
(220, 'Intel Pentium M', NULL, NULL, 45, '2021-02-06 05:39:11', '2021-02-06 05:39:11'),
(221, 'Intel Pentium Dual/Quad', NULL, NULL, 45, '2021-02-06 05:39:11', '2021-02-06 05:39:11'),
(222, 'Intel Celeron', NULL, NULL, 45, '2021-02-06 05:39:11', '2021-02-06 05:39:11'),
(223, 'Intel Celeron Dual/Quad', NULL, NULL, 45, '2021-02-06 05:39:12', '2021-02-06 05:39:12'),
(224, 'Intel Core M', NULL, NULL, 45, '2021-02-06 05:39:12', '2021-02-06 05:39:12'),
(225, 'Intel Core 2', NULL, NULL, 45, '2021-02-06 05:39:12', '2021-02-06 05:39:12'),
(226, 'Intel Atom', NULL, NULL, 45, '2021-02-06 05:39:12', '2021-02-06 05:39:12'),
(227, 'Intel Core i3', NULL, NULL, 45, '2021-02-06 05:39:12', '2021-02-06 05:39:12'),
(228, 'Intel Core i5', NULL, NULL, 45, '2021-02-06 05:39:12', '2021-02-06 05:39:12'),
(229, 'Intel COre i7', NULL, NULL, 45, '2021-02-06 05:39:13', '2021-02-06 05:39:13'),
(230, 'Intel Core i9', NULL, NULL, 45, '2021-02-06 05:39:13', '2021-02-06 05:39:13'),
(231, 'Intel - Others', NULL, NULL, 45, '2021-02-06 05:39:13', '2021-02-06 05:39:13'),
(232, 'AMD Ryzen 3', NULL, NULL, 45, '2021-02-06 05:39:13', '2021-02-06 05:39:13'),
(233, 'AMD Ryzen 5', NULL, NULL, 45, '2021-02-06 05:39:13', '2021-02-06 05:39:13'),
(234, 'AMD Ryzen 7', NULL, NULL, 45, '2021-02-06 05:39:13', '2021-02-06 05:39:13'),
(235, 'AMD A-Series APU', NULL, NULL, 45, '2021-02-06 05:39:14', '2021-02-06 05:39:14'),
(236, 'AMD E-Series APU', NULL, NULL, 45, '2021-02-06 05:39:14', '2021-02-06 05:39:14'),
(237, 'AMD - Others', NULL, NULL, 45, '2021-02-06 05:39:14', '2021-02-06 05:39:14'),
(238, 'Others', NULL, NULL, 45, '2021-02-06 05:39:14', '2021-02-06 05:39:14'),
(239, '2009 before', NULL, NULL, 46, '2021-02-06 05:39:15', '2021-02-06 05:39:15'),
(240, '1st Gen (2010)', NULL, NULL, 46, '2021-02-06 05:39:16', '2021-02-06 05:39:16'),
(241, '2st Gen (2011)', NULL, NULL, 46, '2021-02-06 05:39:16', '2021-02-06 05:39:16'),
(242, '3st Gen (2012)', NULL, NULL, 46, '2021-02-06 05:39:16', '2021-02-06 05:39:16'),
(243, '4st Gen (2013 - 2014)', NULL, NULL, 46, '2021-02-06 05:39:16', '2021-02-06 05:39:16'),
(244, '5st Gen (2015)', NULL, NULL, 46, '2021-02-06 05:39:16', '2021-02-06 05:39:16'),
(245, '6st Gen (2016)', NULL, NULL, 46, '2021-02-06 05:39:17', '2021-02-06 05:39:17'),
(246, '7st Gen (2017)', NULL, NULL, 46, '2021-02-06 05:39:17', '2021-02-06 05:39:17'),
(247, '8st Gen (2018)', NULL, NULL, 46, '2021-02-06 05:39:17', '2021-02-06 05:39:17'),
(248, '9st Gen (2019)', NULL, NULL, 46, '2021-02-06 05:39:17', '2021-02-06 05:39:17'),
(249, '10st Gen (2019-2020)', NULL, NULL, 46, '2021-02-06 05:39:17', '2021-02-06 05:39:17'),
(250, 'Other Gen', NULL, NULL, 46, '2021-02-06 05:39:17', '2021-02-06 05:39:17'),
(251, 'CRT', NULL, NULL, 47, '2021-02-06 05:39:18', '2021-02-06 05:39:18'),
(252, 'LCD', NULL, NULL, 47, '2021-02-06 05:39:19', '2021-02-06 05:39:19'),
(253, 'LED', NULL, NULL, 47, '2021-02-06 05:39:19', '2021-02-06 05:39:19'),
(254, 'None', NULL, NULL, 47, '2021-02-06 05:39:19', '2021-02-06 05:39:19'),
(255, 'No Battery', NULL, NULL, 48, '2021-02-06 05:39:20', '2021-02-06 05:39:20'),
(256, 'Less than 30 minutes', NULL, NULL, 48, '2021-02-06 05:39:20', '2021-02-06 05:39:20'),
(257, '30 - 60 minutes', NULL, NULL, 48, '2021-02-06 05:39:21', '2021-02-06 05:39:21'),
(258, '1 - 2 hour', NULL, NULL, 48, '2021-02-06 05:39:21', '2021-02-06 05:39:21'),
(259, '2 - 3 hour', NULL, NULL, 48, '2021-02-06 05:39:21', '2021-02-06 05:39:21'),
(260, '3 - 4 hour', NULL, NULL, 48, '2021-02-06 05:39:21', '2021-02-06 05:39:21'),
(261, '4 - 5 hour', NULL, NULL, 48, '2021-02-06 05:39:21', '2021-02-06 05:39:21'),
(262, '5 - 6 hour', NULL, NULL, 48, '2021-02-06 05:39:22', '2021-02-06 05:39:22'),
(263, '6 - 7 hour', NULL, NULL, 48, '2021-02-06 05:39:22', '2021-02-06 05:39:22'),
(264, '7 - 8 hour', NULL, NULL, 48, '2021-02-06 05:39:22', '2021-02-06 05:39:22'),
(265, '8 - 10 hour', NULL, NULL, 48, '2021-02-06 05:39:22', '2021-02-06 05:39:22'),
(266, 'Above 10 hours', NULL, NULL, 48, '2021-02-06 05:39:22', '2021-02-06 05:39:22'),
(267, '4', NULL, NULL, 41, '2021-02-06 08:18:20', '2021-02-06 08:18:20'),
(268, '500', NULL, NULL, 42, '2021-02-06 08:18:20', '2021-02-06 08:18:20'),
(269, '2004', NULL, NULL, 14, '2021-02-06 20:57:59', '2021-02-06 20:57:59'),
(270, '2400', NULL, NULL, 18, '2021-02-06 20:57:59', '2021-02-06 20:57:59'),
(271, '36000', NULL, NULL, 19, '2021-02-06 20:57:59', '2021-02-06 20:57:59'),
(272, 'Green', NULL, NULL, 20, '2021-02-06 20:57:59', '2021-02-06 20:57:59'),
(273, '4', NULL, NULL, 6, '2021-02-06 22:39:31', '2021-02-06 22:39:31'),
(274, '4', NULL, NULL, 6, '2021-02-06 22:40:59', '2021-02-06 22:40:59'),
(275, '4', NULL, NULL, 7, '2021-02-06 22:40:59', '2021-02-06 22:40:59'),
(276, '4', NULL, NULL, 6, '2021-02-07 00:53:17', '2021-02-07 00:53:17'),
(277, '4', NULL, NULL, 7, '2021-02-07 00:53:17', '2021-02-07 00:53:17'),
(278, '4', NULL, NULL, 6, '2021-02-07 08:05:12', '2021-02-07 08:05:12'),
(279, '4', NULL, NULL, 7, '2021-02-07 08:05:12', '2021-02-07 08:05:12'),
(280, 'chicken', NULL, NULL, 50, '2021-02-22 03:26:19', '2021-02-22 03:26:19'),
(281, 'Fried Rice Veg Manchurian', NULL, NULL, 52, '2021-02-23 05:52:18', '2021-02-23 05:53:38'),
(282, 'Thali Set - Veg', NULL, NULL, 53, '2021-02-23 05:53:25', '2021-02-23 05:53:25'),
(283, 'Fried Rice Chicken Manchurian', NULL, NULL, 52, '2021-02-23 05:54:01', '2021-02-23 05:54:01'),
(284, 'Thali Set - Chicken', NULL, NULL, 53, '2021-02-23 05:54:40', '2021-02-23 05:54:40'),
(285, 'Roti + Dal Makhani (Add.Curry Rs.50)', NULL, NULL, 54, '2021-02-23 05:56:03', '2021-02-23 05:56:03'),
(286, 'Mo:Mo - Veg', NULL, NULL, 55, '2021-02-23 05:56:58', '2021-02-23 05:56:58'),
(287, 'Mo:Mo - Buff', NULL, NULL, 55, '2021-02-23 05:57:27', '2021-02-23 05:57:27'),
(288, 'Mo:Mo - Chicken', NULL, NULL, 55, '2021-02-23 05:57:54', '2021-02-23 05:57:54'),
(289, 'Rajma Chawal + Raita', NULL, NULL, 56, '2021-02-23 05:58:18', '2021-02-23 06:48:00'),
(290, 'Paneer Wrap / Chicken Wrap', NULL, NULL, 57, '2021-02-23 05:59:02', '2021-02-23 05:59:02'),
(291, '4', NULL, NULL, 6, '2021-03-10 21:46:18', '2021-03-10 21:46:18'),
(292, '4', NULL, NULL, 7, '2021-03-10 21:46:18', '2021-03-10 21:46:18'),
(293, '2016', NULL, NULL, 16, '2021-04-07 22:09:01', '2021-04-07 22:09:01'),
(294, '149', NULL, NULL, 18, '2021-04-07 22:09:01', '2021-04-07 22:09:01'),
(295, '63000', NULL, NULL, 19, '2021-04-07 22:09:01', '2021-04-07 22:09:01'),
(296, 'ARMY BATTLE', NULL, NULL, 20, '2021-04-07 22:09:01', '2021-04-07 22:09:01'),
(297, '73', NULL, NULL, 23, '2021-04-07 22:09:01', '2021-04-07 22:09:01'),
(298, '45', NULL, NULL, 24, '2021-04-07 22:09:01', '2021-04-07 22:09:01'),
(299, '2018', NULL, NULL, 16, '2021-04-07 22:38:58', '2021-04-07 22:38:58'),
(300, '220', NULL, NULL, 18, '2021-04-07 22:38:58', '2021-04-07 22:38:58'),
(301, '20000', NULL, NULL, 19, '2021-04-07 22:38:58', '2021-04-07 22:38:58'),
(302, 'black', NULL, NULL, 20, '2021-04-07 22:38:58', '2021-04-07 22:38:58'),
(303, '97', NULL, NULL, 23, '2021-04-07 22:38:58', '2021-04-07 22:38:58'),
(304, '35', NULL, NULL, 24, '2021-04-07 22:38:58', '2021-04-07 22:38:58'),
(305, '4', NULL, NULL, 6, '2021-04-20 21:57:58', '2021-04-20 21:57:58'),
(306, '1870', NULL, NULL, 7, '2021-04-20 21:57:58', '2021-04-20 21:57:58'),
(307, '3', NULL, NULL, 8, '2021-04-20 21:57:58', '2021-04-20 21:57:58'),
(308, '5', NULL, NULL, 9, '2021-04-20 21:57:58', '2021-04-20 21:57:58'),
(309, '4', NULL, NULL, 10, '2021-04-20 21:57:58', '2021-04-20 21:57:58'),
(310, '2', NULL, NULL, 11, '2021-04-20 21:57:58', '2021-04-20 21:57:58'),
(311, '2014', NULL, NULL, 14, '2021-08-07 20:15:34', '2021-08-07 20:15:34'),
(312, '800', NULL, NULL, 18, '2021-08-07 20:15:34', '2021-08-07 20:15:34'),
(313, '45532', NULL, NULL, 19, '2021-08-07 20:15:34', '2021-08-07 20:15:34'),
(314, 'red', NULL, NULL, 20, '2021-08-07 20:15:34', '2021-08-07 20:15:34');

-- --------------------------------------------------------

--
-- Table structure for table `attributevalue_productattribute`
--

CREATE TABLE `attributevalue_productattribute` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attributevalue_id` bigint(20) UNSIGNED NOT NULL,
  `productattribute_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributevalue_productattribute`
--

INSERT INTO `attributevalue_productattribute` (`id`, `attributevalue_id`, `productattribute_id`, `created_at`, `updated_at`) VALUES
(8, 281, 3, NULL, NULL),
(10, 283, 5, NULL, NULL),
(11, 282, 6, NULL, NULL),
(12, 284, 7, NULL, NULL),
(13, 285, 8, NULL, NULL),
(14, 286, 9, NULL, NULL),
(15, 287, 10, NULL, NULL),
(16, 288, 11, NULL, NULL),
(17, 289, 12, NULL, NULL),
(18, 290, 13, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_categories`
--

CREATE TABLE `attribute_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_categories`
--

INSERT INTO `attribute_categories` (`id`, `attribute_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 2, NULL, NULL),
(3, 1, 3, NULL, NULL),
(4, 1, 4, NULL, NULL),
(5, 1, 5, NULL, NULL),
(6, 1, 6, NULL, NULL),
(7, 1, 7, NULL, NULL),
(8, 1, 8, NULL, NULL),
(9, 1, 9, NULL, NULL),
(10, 2, 1, NULL, NULL),
(11, 2, 2, NULL, NULL),
(12, 2, 3, NULL, NULL),
(13, 2, 4, NULL, NULL),
(14, 2, 5, NULL, NULL),
(15, 2, 6, NULL, NULL),
(16, 2, 7, NULL, NULL),
(17, 2, 8, NULL, NULL),
(18, 2, 9, NULL, NULL),
(19, 3, 1, NULL, NULL),
(20, 3, 3, NULL, NULL),
(21, 3, 4, NULL, NULL),
(22, 3, 5, NULL, NULL),
(23, 3, 6, NULL, NULL),
(24, 3, 7, NULL, NULL),
(25, 3, 8, NULL, NULL),
(26, 3, 9, NULL, NULL),
(27, 4, 1, NULL, NULL),
(28, 4, 2, NULL, NULL),
(29, 4, 3, NULL, NULL),
(30, 4, 4, NULL, NULL),
(31, 4, 5, NULL, NULL),
(32, 4, 6, NULL, NULL),
(33, 4, 7, NULL, NULL),
(34, 4, 8, NULL, NULL),
(35, 4, 9, NULL, NULL),
(36, 5, 1, NULL, NULL),
(37, 5, 2, NULL, NULL),
(38, 5, 3, NULL, NULL),
(39, 5, 4, NULL, NULL),
(40, 5, 5, NULL, NULL),
(41, 5, 6, NULL, NULL),
(42, 5, 7, NULL, NULL),
(43, 5, 8, NULL, NULL),
(44, 5, 9, NULL, NULL),
(45, 6, 1, NULL, NULL),
(46, 6, 2, NULL, NULL),
(47, 6, 3, NULL, NULL),
(48, 6, 5, NULL, NULL),
(49, 6, 6, NULL, NULL),
(50, 6, 7, NULL, NULL),
(51, 6, 8, NULL, NULL),
(52, 6, 9, NULL, NULL),
(53, 7, 1, NULL, NULL),
(54, 7, 2, NULL, NULL),
(55, 7, 3, NULL, NULL),
(56, 7, 4, NULL, NULL),
(57, 7, 5, NULL, NULL),
(58, 7, 6, NULL, NULL),
(59, 7, 7, NULL, NULL),
(60, 7, 8, NULL, NULL),
(61, 7, 9, NULL, NULL),
(62, 8, 1, NULL, NULL),
(63, 8, 3, NULL, NULL),
(64, 8, 5, NULL, NULL),
(65, 8, 6, NULL, NULL),
(66, 8, 7, NULL, NULL),
(67, 8, 8, NULL, NULL),
(68, 8, 9, NULL, NULL),
(69, 9, 1, NULL, NULL),
(70, 9, 3, NULL, NULL),
(71, 9, 5, NULL, NULL),
(72, 9, 6, NULL, NULL),
(73, 9, 7, NULL, NULL),
(74, 9, 8, NULL, NULL),
(75, 9, 9, NULL, NULL),
(76, 10, 1, NULL, NULL),
(77, 10, 3, NULL, NULL),
(78, 10, 5, NULL, NULL),
(79, 10, 6, NULL, NULL),
(80, 10, 7, NULL, NULL),
(81, 10, 8, NULL, NULL),
(82, 10, 9, NULL, NULL),
(83, 11, 1, NULL, NULL),
(84, 11, 3, NULL, NULL),
(85, 11, 5, NULL, NULL),
(86, 11, 6, NULL, NULL),
(87, 11, 7, NULL, NULL),
(88, 11, 8, NULL, NULL),
(89, 11, 9, NULL, NULL),
(90, 12, 11, NULL, NULL),
(91, 12, 13, NULL, NULL),
(92, 12, 14, NULL, NULL),
(93, 12, 15, NULL, NULL),
(94, 13, 11, NULL, NULL),
(95, 13, 13, NULL, NULL),
(96, 13, 14, NULL, NULL),
(97, 13, 15, NULL, NULL),
(98, 14, 11, NULL, NULL),
(99, 14, 13, NULL, NULL),
(100, 14, 14, NULL, NULL),
(101, 14, 15, NULL, NULL),
(102, 15, 12, NULL, NULL),
(103, 16, 12, NULL, NULL),
(104, 17, 12, NULL, NULL),
(105, 18, 11, NULL, NULL),
(106, 18, 12, NULL, NULL),
(107, 18, 13, NULL, NULL),
(108, 18, 14, NULL, NULL),
(109, 18, 15, NULL, NULL),
(110, 19, 11, NULL, NULL),
(111, 19, 12, NULL, NULL),
(112, 19, 13, NULL, NULL),
(113, 19, 14, NULL, NULL),
(114, 19, 15, NULL, NULL),
(115, 20, 11, NULL, NULL),
(116, 20, 12, NULL, NULL),
(117, 20, 13, NULL, NULL),
(118, 20, 14, NULL, NULL),
(119, 20, 15, NULL, NULL),
(120, 21, 11, NULL, NULL),
(121, 21, 12, NULL, NULL),
(122, 21, 13, NULL, NULL),
(123, 21, 14, NULL, NULL),
(124, 21, 15, NULL, NULL),
(125, 22, 11, NULL, NULL),
(126, 22, 13, NULL, NULL),
(127, 22, 14, NULL, NULL),
(128, 22, 15, NULL, NULL),
(129, 23, 12, NULL, NULL),
(130, 24, 12, NULL, NULL),
(131, 25, 1, NULL, NULL),
(132, 25, 2, NULL, NULL),
(133, 25, 3, NULL, NULL),
(134, 25, 4, NULL, NULL),
(135, 25, 5, NULL, NULL),
(136, 25, 6, NULL, NULL),
(137, 25, 7, NULL, NULL),
(138, 25, 8, NULL, NULL),
(139, 25, 9, NULL, NULL),
(140, 26, 11, NULL, NULL),
(141, 26, 13, NULL, NULL),
(142, 26, 14, NULL, NULL),
(143, 27, 12, NULL, NULL),
(144, 28, 19, NULL, NULL),
(145, 28, 20, NULL, NULL),
(146, 28, 22, NULL, NULL),
(147, 29, 21, NULL, NULL),
(148, 29, 23, NULL, NULL),
(149, 30, 27, NULL, NULL),
(150, 31, 35, NULL, NULL),
(151, 32, 35, NULL, NULL),
(152, 33, 35, NULL, NULL),
(153, 34, 35, NULL, NULL),
(154, 35, 35, NULL, NULL),
(155, 36, 35, NULL, NULL),
(156, 37, 35, NULL, NULL),
(157, 38, 35, NULL, NULL),
(158, 39, 35, NULL, NULL),
(159, 40, 35, NULL, NULL),
(160, 41, 37, NULL, NULL),
(161, 42, 37, NULL, NULL),
(162, 43, 37, NULL, NULL),
(163, 44, 37, NULL, NULL),
(164, 45, 37, NULL, NULL),
(165, 46, 37, NULL, NULL),
(166, 47, 37, NULL, NULL),
(167, 47, 44, NULL, NULL),
(168, 48, 37, NULL, NULL),
(169, 49, 37, NULL, NULL),
(170, 50, 126, NULL, NULL),
(171, 52, 126, NULL, NULL),
(172, 53, 126, NULL, NULL),
(173, 54, 126, NULL, NULL),
(174, 55, 126, NULL, NULL),
(175, 56, 126, NULL, NULL),
(176, 57, 126, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `image`, `name`, `username`, `phone`, `remarks`, `status`, `email`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Admin', 'pramodpb', 9813999208, 'dummy', 1, 'pramodbhandari68@gmail.com', '2021-02-06 05:34:55', '2021-02-06 05:34:55');

-- --------------------------------------------------------

--
-- Table structure for table `bids`
--

CREATE TABLE `bids` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bid` bigint(20) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `bidable_id` bigint(20) UNSIGNED NOT NULL,
  `bidable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bids`
--

INSERT INTO `bids` (`id`, `body`, `bid`, `url`, `user_id`, `parent_id`, `bidable_id`, `bidable_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'test', 30000, 'https://merooffer.com/ad/samsung-galaxy-m31', 71, NULL, 30, 'App\\Models\\Product', 0, '2021-09-04 00:18:40', '2021-09-04 00:18:40');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `font` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` bigint(20) NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `menu` tinyint(1) NOT NULL DEFAULT 1,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `status`, `font`, `color`, `order`, `desc`, `featured`, `menu`, `image`, `parent_id`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Real Estate', 1, 'zmdi zmdi-city-alt', '#86267e', 7, NULL, 1, 0, NULL, NULL, 'real-estate', '2021-02-06 05:35:46', '2021-07-04 07:44:23'),
(2, 'Land', 1, NULL, NULL, 8, NULL, 0, 1, NULL, 1, 'land', '2021-02-06 05:35:46', '2021-07-04 07:44:23'),
(3, 'House', 1, NULL, NULL, 9, NULL, 0, 1, NULL, 1, 'house', '2021-02-06 05:35:46', '2021-07-04 07:44:23'),
(4, 'Shutter', 1, NULL, NULL, 6, NULL, 0, 1, NULL, 1, 'shutter', '2021-02-06 05:35:47', '2021-07-04 07:44:23'),
(5, 'Property', 1, NULL, NULL, 4, NULL, 0, 1, NULL, 1, 'property', '2021-02-06 05:35:47', '2021-07-04 07:44:23'),
(6, 'Office Space', 1, NULL, NULL, 5, NULL, 0, 1, NULL, 1, 'office-space', '2021-02-06 05:35:47', '2021-07-04 07:44:23'),
(7, 'Flat', 1, NULL, NULL, 3, NULL, 0, 1, NULL, 1, 'flat', '2021-02-06 05:35:48', '2021-07-04 07:44:23'),
(8, 'Apartment', 1, NULL, NULL, 2, NULL, 0, 1, NULL, 1, 'apartment', '2021-02-06 05:35:48', '2021-07-04 07:44:23'),
(9, 'Hostel', 1, NULL, NULL, 1, NULL, 0, 1, NULL, 1, 'hostel', '2021-02-06 05:35:48', '2021-07-04 07:44:23'),
(10, 'Automotive', 1, 'zmdi zmdi-car', '#46b75d', 10, NULL, 1, 1, NULL, NULL, 'automotive', '2021-02-06 05:35:49', '2021-07-04 07:44:23'),
(11, 'Cars', 1, NULL, NULL, 11, NULL, 0, 1, NULL, 10, 'cars', '2021-02-06 05:35:49', '2021-07-04 07:44:44'),
(12, 'Motorbikes', 1, NULL, NULL, 12, NULL, 0, 1, NULL, 10, 'motorbikes', '2021-02-06 05:35:49', '2021-07-04 07:44:44'),
(13, 'Heavy Vehicles', 1, NULL, NULL, 15, NULL, 0, 1, NULL, 10, 'heavy-vehicles', '2021-02-06 05:35:49', '2021-07-04 07:44:44'),
(14, 'Van & Trucks', 1, NULL, NULL, 13, NULL, 0, 1, NULL, 10, 'van-trucks', '2021-02-06 05:35:50', '2021-07-04 07:44:44'),
(15, 'Mechanics & Garages', 1, NULL, NULL, 16, NULL, 0, 1, NULL, 10, 'mechanics-garages', '2021-02-06 05:35:50', '2021-04-10 09:43:22'),
(16, 'Parts & accessories For Car', 1, NULL, NULL, 17, NULL, 0, 1, NULL, 10, 'parts-accessories-for-car', '2021-02-06 05:35:50', '2021-04-10 09:43:22'),
(17, 'Parts & Accessories for Motorcycles', 1, NULL, NULL, 18, NULL, 0, 1, NULL, 10, 'parts-accessories-for-motorcycles', '2021-02-06 05:35:51', '2021-04-10 09:43:22'),
(18, 'Lifestyle', 1, 'zmdi zmdi-female', '#86267e', 19, NULL, 1, 1, NULL, NULL, 'lifestyle', '2021-02-06 05:35:51', '2021-04-10 09:43:22'),
(19, 'Kid\'s Clothing', 1, NULL, NULL, 20, NULL, 0, 1, NULL, 18, 'kids-clothing', '2021-02-06 05:35:51', '2021-04-10 09:43:22'),
(20, 'Women\'s Clothing', 1, NULL, NULL, 21, NULL, 0, 1, NULL, 18, 'womens-clothing', '2021-02-06 05:35:52', '2021-04-10 09:43:22'),
(21, 'Women\'s Shoes', 1, NULL, NULL, 22, NULL, 0, 1, NULL, 18, 'womens-shoes', '2021-02-06 05:35:52', '2021-04-10 09:43:22'),
(22, 'Men\'s Clothing', 1, NULL, NULL, 23, NULL, 0, 1, NULL, 18, 'mens-clothing', '2021-02-06 05:35:52', '2021-04-10 09:43:22'),
(23, 'Men\'s Shoes', 1, NULL, NULL, 24, NULL, 0, 1, NULL, 18, 'mens-shoes', '2021-02-06 05:35:52', '2021-04-10 09:43:22'),
(24, 'Kid\'s Accessories', 1, NULL, NULL, 25, NULL, 0, 1, NULL, 18, 'kids-accessories', '2021-02-06 05:35:52', '2021-04-10 09:43:22'),
(25, 'Men\'s Accessories', 1, NULL, NULL, 26, NULL, 0, 1, NULL, 18, 'mens-accessories', '2021-02-06 05:35:52', '2021-04-10 09:43:22'),
(26, 'Women\'s Accessories', 1, NULL, NULL, 27, NULL, 0, 1, NULL, 18, 'womens-accessories', '2021-02-06 05:35:53', '2021-04-10 09:43:22'),
(27, 'Watches', 1, NULL, NULL, 28, NULL, 0, 1, NULL, 18, 'watches', '2021-02-06 05:35:53', '2021-04-10 09:43:22'),
(28, 'Jewellery', 1, NULL, NULL, 29, NULL, 0, 1, NULL, 18, 'jewellery', '2021-02-06 05:35:53', '2021-04-10 09:43:22'),
(29, 'Bags/Luggage', 1, NULL, NULL, 30, NULL, 0, 1, NULL, 18, 'bagsluggage', '2021-02-06 05:35:53', '2021-04-10 09:43:23'),
(30, 'Sunglasses', 1, NULL, NULL, 31, NULL, 0, 1, NULL, 18, 'sunglasses', '2021-02-06 05:35:54', '2021-04-10 09:43:23'),
(31, 'Other Accessories', 1, NULL, NULL, 32, NULL, 0, 1, NULL, 18, 'other-accessories', '2021-02-06 05:35:54', '2021-04-10 09:43:23'),
(32, 'Men Grooming Tools', 1, NULL, NULL, 33, NULL, 0, 1, NULL, 18, 'men-grooming-tools', '2021-02-06 05:35:54', '2021-04-10 09:43:23'),
(33, 'Women Grooming Tools', 1, NULL, NULL, 34, NULL, 0, 1, NULL, 18, 'women-grooming-tools', '2021-02-06 05:35:54', '2021-04-10 09:43:23'),
(34, 'Electronics', 1, 'zmdi zmdi-radio', '#39444c', 35, NULL, 1, 1, NULL, NULL, 'electronics', '2021-02-06 05:35:54', '2021-04-10 09:43:23'),
(35, 'Mobile Phone Sets', 1, NULL, NULL, 36, NULL, 0, 1, NULL, 34, 'mobile-phone-sets', '2021-02-06 05:35:55', '2021-04-10 09:43:23'),
(36, 'Mobile Accessories', 1, NULL, NULL, 37, NULL, 0, 1, NULL, 34, 'mobile-accessories', '2021-02-06 05:35:55', '2021-04-10 09:43:23'),
(37, 'Laptops & Computer', 1, NULL, NULL, 38, NULL, 0, 1, NULL, 34, 'laptops-computer', '2021-02-06 05:35:55', '2021-04-10 09:43:23'),
(38, 'Laptop Accessories', 1, NULL, NULL, 39, NULL, 0, 1, NULL, 34, 'laptop-accessories', '2021-02-06 05:35:55', '2021-04-10 09:43:23'),
(39, 'Camera Lens & Accessories', 1, NULL, NULL, 40, NULL, 0, 1, NULL, 34, 'camera-lens-accessories', '2021-02-06 05:35:55', '2021-04-10 09:43:23'),
(40, 'Drone & Quadcopter', 1, NULL, NULL, 41, NULL, 0, 1, NULL, 34, 'drone-quadcopter', '2021-02-06 05:35:56', '2021-04-10 09:43:23'),
(41, 'Projectors', 1, NULL, NULL, 42, NULL, 0, 1, NULL, 34, 'projectors', '2021-02-06 05:35:56', '2021-04-10 09:43:23'),
(42, 'Storage & Optical Devices', 1, NULL, NULL, 43, NULL, 0, 1, NULL, 34, 'storage-optical-devices', '2021-02-06 05:35:56', '2021-04-10 09:43:23'),
(43, 'Networking Equipments', 1, NULL, NULL, 44, NULL, 0, 1, NULL, 34, 'networking-equipments', '2021-02-06 05:35:56', '2021-04-10 09:43:23'),
(44, 'Monitors', 1, NULL, NULL, 45, NULL, 0, 1, NULL, 34, 'monitors', '2021-02-06 05:35:57', '2021-04-10 09:43:23'),
(45, 'Desktops & Accessories', 1, NULL, NULL, 46, NULL, 0, 1, NULL, 34, 'desktops-accessories', '2021-02-06 05:35:57', '2021-04-10 09:43:23'),
(46, 'Tablets & Accessories', 1, NULL, NULL, 47, NULL, 0, 1, NULL, 34, 'tablets-accessories', '2021-02-06 05:35:57', '2021-04-10 09:43:23'),
(47, 'TV & DVDs', 1, NULL, NULL, 48, NULL, 0, 1, NULL, 34, 'tv-dvds', '2021-02-06 05:35:58', '2021-04-10 09:43:23'),
(48, 'Home & Furnitures', 1, 'zmdi zmdi-seat', '#c25762', 49, NULL, 0, 1, NULL, NULL, 'home-furnitures', '2021-02-06 05:35:58', '2021-04-10 09:43:23'),
(49, 'Home Furnitures', 1, NULL, NULL, 50, NULL, 0, 1, NULL, 48, '2-home-furnitures', '2021-02-06 05:35:59', '2021-04-10 09:43:23'),
(50, 'Construction', 1, NULL, NULL, 51, NULL, 0, 1, NULL, 48, 'construction', '2021-02-06 05:35:59', '2021-04-10 09:43:23'),
(51, 'Household & Tools', 1, NULL, NULL, 52, NULL, 0, 1, NULL, 48, 'household-tools', '2021-02-06 05:35:59', '2021-04-10 09:43:23'),
(52, 'Lighting', 1, NULL, NULL, 53, NULL, 0, 1, NULL, 48, 'lighting', '2021-02-06 05:35:59', '2021-04-10 09:43:23'),
(53, 'Appliances', 1, NULL, NULL, 54, NULL, 0, 1, NULL, 48, 'appliances', '2021-02-06 05:36:00', '2021-04-10 09:43:23'),
(54, 'Garden Supplies', 1, NULL, NULL, 55, NULL, 0, 1, NULL, 48, 'garden-supplies', '2021-02-06 05:36:00', '2021-04-10 09:43:23'),
(55, 'Home Decor and Interiors', 1, NULL, NULL, 56, NULL, 0, 1, NULL, 48, 'home-decor-and-interiors', '2021-02-06 05:36:00', '2021-04-10 09:43:23'),
(56, 'Kitchenware & Utensils', 1, NULL, NULL, 57, NULL, 0, 1, NULL, 48, 'kitchenware-utensils', '2021-02-06 05:36:00', '2021-04-10 09:43:23'),
(57, 'Others', 1, NULL, NULL, 58, NULL, 0, 1, NULL, 48, 'others', '2021-02-06 05:36:00', '2021-04-10 09:43:23'),
(58, 'Services', 1, 'zmdi zmdi-wrench', '#44baff', 59, NULL, 1, 0, NULL, NULL, 'services', '2021-02-06 05:36:01', '2021-04-15 09:06:33'),
(59, 'Software & Games', 1, NULL, NULL, 60, NULL, 0, 1, NULL, 58, 'software-games', '2021-02-06 05:36:01', '2021-04-10 09:43:23'),
(60, 'Web Development', 1, NULL, NULL, 61, NULL, 0, 1, NULL, 58, 'web-development', '2021-02-06 05:36:01', '2021-04-10 09:43:23'),
(61, 'Education & Classes', 1, NULL, NULL, 62, NULL, 0, 1, NULL, 58, 'education-classes', '2021-02-06 05:36:01', '2021-04-10 09:43:23'),
(62, 'Cleaning Services', 1, NULL, NULL, 63, NULL, 0, 1, NULL, 58, 'cleaning-services', '2021-02-06 05:36:01', '2021-04-10 09:43:23'),
(63, 'Photography', 1, NULL, NULL, 64, NULL, 0, 1, NULL, 58, 'photography', '2021-02-06 05:36:01', '2021-04-10 09:43:23'),
(64, 'Technical Maintenance', 1, NULL, NULL, 65, NULL, 0, 1, NULL, 58, 'technical-maintenance', '2021-02-06 05:36:02', '2021-04-10 09:43:23'),
(65, 'Others', 1, NULL, NULL, 66, NULL, 0, 1, NULL, 58, '2-others', '2021-02-06 05:36:02', '2021-04-10 09:43:23'),
(66, 'Hotel Management', 1, NULL, NULL, 67, NULL, 0, 1, NULL, 58, 'hotel-management', '2021-02-06 05:36:02', '2021-04-10 09:43:23'),
(67, 'Musical Instrument & Services', 0, 'zmdi zmdi-audio', '#9bc2a4', 68, NULL, 0, 0, NULL, NULL, '2-musical-instrument-services', '2021-02-06 05:36:02', '2021-04-10 09:43:23'),
(68, 'Guitars', 1, NULL, NULL, 69, NULL, 0, 1, NULL, 67, 'guitars', '2021-02-06 05:36:03', '2021-04-10 09:43:23'),
(69, 'Drums', 1, NULL, NULL, 70, NULL, 0, 1, NULL, 67, 'drums', '2021-02-06 05:36:03', '2021-04-10 09:43:23'),
(70, 'Keyboard / Piano', 1, NULL, NULL, 71, NULL, 0, 1, NULL, 67, 'keyboard-piano', '2021-02-06 05:36:03', '2021-04-10 09:43:23'),
(71, 'Dj and lightening', 1, NULL, NULL, 72, NULL, 0, 1, NULL, 67, 'dj-and-lightening', '2021-02-06 05:36:03', '2021-04-10 09:43:23'),
(72, 'Amp and Speakers', 1, NULL, NULL, 73, NULL, 0, 1, NULL, 67, 'amp-and-speakers', '2021-02-06 05:36:04', '2021-04-10 09:43:23'),
(73, 'Band Baja Services', 1, NULL, NULL, 74, NULL, 0, 1, NULL, 67, 'band-baja-services', '2021-02-06 05:36:04', '2021-04-10 09:43:23'),
(74, 'Musical Instruments for Hire', 1, NULL, NULL, 75, NULL, 0, 1, NULL, 67, 'musical-instruments-for-hire', '2021-02-06 05:36:04', '2021-04-10 09:43:23'),
(75, 'Other Musical instrument', 1, NULL, NULL, 76, NULL, 0, 1, NULL, 67, 'other-musical-instrument', '2021-02-06 05:36:05', '2021-04-10 09:43:23'),
(76, 'Video Games & Toys', 0, 'zmdi zmdi-play', '#86267e', 77, NULL, 0, 0, NULL, NULL, '2-video-games-toys', '2021-02-06 05:36:05', '2021-04-10 09:43:23'),
(77, 'Gaming Accessories', 1, NULL, NULL, 78, NULL, 0, 1, NULL, 76, 'gaming-accessories', '2021-02-06 05:36:05', '2021-04-10 09:43:23'),
(78, 'Gaming Console', 1, NULL, NULL, 79, NULL, 0, 1, NULL, 76, 'gaming-console', '2021-02-06 05:36:05', '2021-04-10 09:43:23'),
(79, 'Puzzle and Educational Toys', 1, NULL, NULL, 80, NULL, 0, 1, NULL, 76, 'puzzle-and-educational-toys', '2021-02-06 05:36:05', '2021-04-10 09:43:23'),
(80, 'General Toys', 1, NULL, NULL, 81, NULL, 0, 1, NULL, 76, 'general-toys', '2021-02-06 05:36:05', '2021-04-10 09:43:23'),
(81, 'Kid\'s Pretend Play', 1, NULL, NULL, 82, NULL, 0, 1, NULL, 76, 'kids-pretend-play', '2021-02-06 05:36:06', '2021-04-10 09:43:23'),
(82, 'Toys - Remote Control', 1, NULL, NULL, 83, NULL, 0, 1, NULL, 76, 'toys-remote-control', '2021-02-06 05:36:06', '2021-04-10 09:43:23'),
(83, 'Gaming Disc and Cartiges', 1, NULL, NULL, 84, NULL, 0, 1, NULL, 76, 'gaming-disc-and-cartiges', '2021-02-06 05:36:06', '2021-04-10 09:43:23'),
(84, 'Toys - Stuffed, Dolls & Figures', 1, NULL, NULL, 85, NULL, 0, 1, NULL, 76, 'toys-stuffed-dolls-figures', '2021-02-06 05:36:06', '2021-04-10 09:43:23'),
(85, 'Sports and Outdoor Play', 1, NULL, NULL, 86, NULL, 0, 1, NULL, 76, 'sports-and-outdoor-play', '2021-02-06 05:36:07', '2021-04-10 09:42:43'),
(86, 'Blocks and Building Toys', 1, NULL, NULL, 87, NULL, 0, 1, NULL, 76, 'blocks-and-building-toys', '2021-02-06 05:36:07', '2021-04-10 09:42:43'),
(87, 'Others', 1, NULL, NULL, 88, NULL, 0, 1, NULL, 76, '3-others', '2021-02-06 05:36:07', '2021-04-10 09:42:43'),
(88, 'Sports,books & Hobbies', 0, 'zmdi zmdi-book', '#d8690c', 89, NULL, 0, 0, NULL, NULL, '2-sportsbooks-hobbies', '2021-02-06 05:36:07', '2021-04-10 09:42:43'),
(89, 'Bicycles', 1, NULL, NULL, 90, NULL, 0, 1, NULL, 88, 'bicycles', '2021-02-06 05:36:08', '2021-04-10 09:42:43'),
(90, 'Fitness and Gyms Equipment', 1, NULL, NULL, 91, NULL, 0, 1, NULL, 88, 'fitness-and-gyms-equipment', '2021-02-06 05:36:08', '2021-04-10 09:42:43'),
(91, 'Other Sports', 1, NULL, NULL, 92, NULL, 0, 1, NULL, 88, 'other-sports', '2021-02-06 05:36:08', '2021-04-10 09:42:43'),
(92, 'Bicycle Parts and Accessories', 1, NULL, NULL, 93, NULL, 0, 1, NULL, 88, 'bicycle-parts-and-accessories', '2021-02-06 05:36:08', '2021-04-10 09:42:43'),
(93, 'Outdoor and Hiking', 1, NULL, NULL, 94, NULL, 0, 1, NULL, 88, 'outdoor-and-hiking', '2021-02-06 05:36:08', '2021-04-10 09:42:43'),
(94, 'Novels', 1, NULL, NULL, 95, NULL, 0, 1, NULL, 88, 'novels', '2021-02-06 05:36:09', '2021-04-10 09:42:43'),
(95, 'Course Books', 1, NULL, NULL, 96, NULL, 0, 1, NULL, 88, 'course-books', '2021-02-06 05:36:09', '2021-04-10 09:42:43'),
(96, 'Other Books', 1, NULL, NULL, 97, NULL, 0, 1, NULL, 88, 'other-books', '2021-02-06 05:36:09', '2021-04-10 09:42:43'),
(97, 'Travel & Tourism', 0, 'zmdi zmdi-flight-takeoff', '#509fa6', 98, NULL, 0, 0, NULL, NULL, '2-travel-tourism', '2021-02-06 05:36:09', '2021-04-10 09:42:43'),
(98, 'Air Ticket', 1, NULL, NULL, 99, NULL, 0, 1, NULL, 97, 'air-ticket', '2021-02-06 05:36:10', '2021-04-10 09:42:43'),
(99, 'Bus Ticket', 1, NULL, NULL, 100, NULL, 0, 1, NULL, 97, 'bus-ticket', '2021-02-06 05:36:10', '2021-04-10 09:42:43'),
(100, 'Hotel', 1, NULL, NULL, 101, NULL, 0, 1, NULL, 97, 'hotel', '2021-02-06 05:36:10', '2021-04-10 09:42:43'),
(101, 'Tour Package-Domestic', 1, NULL, NULL, 102, NULL, 0, 1, NULL, 97, 'tour-package-domestic', '2021-02-06 05:36:10', '2021-04-10 09:42:43'),
(102, 'Tour Package-International', 1, NULL, NULL, 103, NULL, 0, 1, NULL, 97, 'tour-package-international', '2021-02-06 05:36:10', '2021-04-10 09:42:43'),
(103, 'Treeking Package', 1, NULL, NULL, 104, NULL, 0, 1, NULL, 97, 'treeking-package', '2021-02-06 05:36:11', '2021-04-10 09:42:43'),
(104, 'Other Tour Packages', 1, NULL, NULL, 105, NULL, 0, 1, NULL, 97, 'other-tour-packages', '2021-02-06 05:36:11', '2021-04-10 09:42:43'),
(105, 'Pets & Animals', 0, 'fa fa-paw', '#adb337', 106, NULL, 0, 0, NULL, NULL, 'pets-animals', '2021-02-06 05:36:12', '2021-04-15 09:07:54'),
(106, 'Dogs', 1, NULL, NULL, 107, NULL, 0, 1, NULL, 105, 'dogs', '2021-02-06 05:36:12', '2021-04-10 09:42:43'),
(107, 'Cats', 1, NULL, NULL, 108, NULL, 0, 1, NULL, 105, 'cats', '2021-02-06 05:36:12', '2021-04-10 09:42:43'),
(108, 'Fish and Aquarium', 1, NULL, NULL, 109, NULL, 0, 1, NULL, 105, 'fish-and-aquarium', '2021-02-06 05:36:13', '2021-04-10 09:42:43'),
(109, 'Rabbits', 1, NULL, NULL, 110, NULL, 0, 1, NULL, 105, 'rabbits', '2021-02-06 05:36:13', '2021-04-10 09:42:43'),
(110, 'Others', 1, NULL, NULL, 111, NULL, 0, 1, NULL, 105, '4-others', '2021-02-06 05:36:13', '2021-04-10 09:42:43'),
(111, 'Pet Services', 1, NULL, NULL, 112, NULL, 0, 1, NULL, 105, 'pet-services', '2021-02-06 05:36:14', '2021-04-10 09:42:43'),
(112, 'Food', 0, 'zmdi zmdi-cutlery', '#46b75d', 113, NULL, 0, 0, NULL, NULL, 'food', '2021-02-06 05:36:14', '2021-04-10 09:42:43'),
(113, 'Cake Services', 1, NULL, NULL, 114, NULL, 0, 1, NULL, 112, 'cake-services', '2021-02-06 05:36:14', '2021-04-10 09:42:43'),
(114, 'Celebrity', 0, 'zmdi zmdi-male-female', '#e76b77', 116, NULL, 1, 0, NULL, NULL, '2-celebrity', '2021-02-06 05:36:14', '2021-03-21 03:43:21'),
(115, 'Actors', 1, NULL, NULL, 117, NULL, 0, 1, NULL, 114, 'actors', '2021-02-06 05:36:14', '2021-03-21 03:43:21'),
(116, 'Actress', 1, NULL, NULL, 118, NULL, 0, 1, NULL, 114, 'actress', '2021-02-06 05:36:14', '2021-03-21 03:43:21'),
(117, 'Comedians', 1, NULL, NULL, 119, NULL, 0, 1, NULL, 114, 'comedians', '2021-02-06 05:36:15', '2021-03-21 03:43:15'),
(118, 'Singers', 1, NULL, NULL, 120, NULL, 0, 1, NULL, 114, 'singers', '2021-02-06 05:36:15', '2021-03-21 03:43:15'),
(119, 'Anchor', 1, NULL, NULL, 121, NULL, 0, 1, NULL, 114, 'anchor', '2021-02-06 05:36:16', '2021-03-21 03:43:15'),
(120, 'Artists', 1, NULL, NULL, 122, NULL, 0, 1, NULL, 114, 'artists', '2021-02-06 05:36:16', '2021-03-21 03:43:15'),
(121, 'Youtuber', 1, NULL, NULL, 123, NULL, 0, 1, NULL, 114, 'youtuber', '2021-02-06 05:36:16', '2021-03-21 03:43:15'),
(122, 'Director', 1, NULL, NULL, 124, NULL, 0, 1, NULL, 114, 'director', '2021-02-06 05:36:16', '2021-03-21 03:43:15'),
(123, 'Musicians', 1, NULL, NULL, 125, NULL, 0, 1, NULL, 114, 'musicians', '2021-02-06 05:36:16', '2021-03-21 03:43:15'),
(124, 'Story Tellers', 1, NULL, NULL, 126, NULL, 0, 1, NULL, 114, 'story-tellers', '2021-02-06 05:36:17', '2021-03-21 03:43:15'),
(125, 'Bike Services', 1, NULL, NULL, 127, NULL, 0, 0, NULL, 10, 'bike-services', '2021-02-07 09:20:14', '2021-03-21 03:43:15'),
(126, 'Restaurant & Cafe', 1, NULL, NULL, 115, NULL, 0, 0, NULL, 112, 'restaurant-cafe', '2021-02-22 03:11:48', '2021-04-10 09:42:43');

-- --------------------------------------------------------

--
-- Table structure for table `category_pricelabel`
--

CREATE TABLE `category_pricelabel` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `pricelabel_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_pricelabel`
--

INSERT INTO `category_pricelabel` (`id`, `category_id`, `pricelabel_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 1, NULL, NULL),
(3, 3, 1, NULL, NULL),
(4, 4, 1, NULL, NULL),
(5, 5, 1, NULL, NULL),
(6, 6, 1, NULL, NULL),
(7, 7, 1, NULL, NULL),
(8, 8, 1, NULL, NULL),
(9, 9, 1, NULL, NULL),
(10, 1, 2, NULL, NULL),
(11, 2, 2, NULL, NULL),
(12, 3, 2, NULL, NULL),
(13, 4, 2, NULL, NULL),
(14, 5, 2, NULL, NULL),
(15, 6, 2, NULL, NULL),
(16, 7, 2, NULL, NULL),
(17, 8, 2, NULL, NULL),
(18, 9, 2, NULL, NULL),
(19, 1, 3, NULL, NULL),
(20, 2, 3, NULL, NULL),
(21, 3, 3, NULL, NULL),
(22, 4, 3, NULL, NULL),
(23, 5, 3, NULL, NULL),
(24, 6, 3, NULL, NULL),
(25, 7, 3, NULL, NULL),
(26, 8, 3, NULL, NULL),
(27, 9, 3, NULL, NULL),
(28, 1, 4, NULL, NULL),
(29, 2, 4, NULL, NULL),
(30, 3, 4, NULL, NULL),
(31, 4, 4, NULL, NULL),
(32, 5, 4, NULL, NULL),
(33, 6, 4, NULL, NULL),
(34, 7, 4, NULL, NULL),
(35, 8, 4, NULL, NULL),
(36, 9, 4, NULL, NULL),
(37, 1, 5, NULL, NULL),
(38, 2, 5, NULL, NULL),
(39, 3, 5, NULL, NULL),
(40, 4, 5, NULL, NULL),
(41, 5, 5, NULL, NULL),
(42, 6, 5, NULL, NULL),
(43, 7, 5, NULL, NULL),
(44, 8, 5, NULL, NULL),
(45, 9, 5, NULL, NULL),
(46, 1, 6, NULL, NULL),
(47, 2, 6, NULL, NULL),
(48, 3, 6, NULL, NULL),
(49, 4, 6, NULL, NULL),
(50, 5, 6, NULL, NULL),
(51, 6, 6, NULL, NULL),
(52, 7, 6, NULL, NULL),
(53, 8, 6, NULL, NULL),
(54, 9, 6, NULL, NULL),
(55, 1, 7, NULL, NULL),
(56, 2, 7, NULL, NULL),
(57, 3, 7, NULL, NULL),
(58, 4, 7, NULL, NULL),
(59, 5, 7, NULL, NULL),
(60, 6, 7, NULL, NULL),
(61, 7, 7, NULL, NULL),
(62, 8, 7, NULL, NULL),
(63, 9, 7, NULL, NULL),
(64, 1, 8, NULL, NULL),
(65, 2, 8, NULL, NULL),
(66, 3, 8, NULL, NULL),
(67, 4, 8, NULL, NULL),
(68, 5, 8, NULL, NULL),
(69, 6, 8, NULL, NULL),
(70, 7, 8, NULL, NULL),
(71, 8, 8, NULL, NULL),
(72, 9, 8, NULL, NULL),
(73, 1, 9, NULL, NULL),
(74, 2, 9, NULL, NULL),
(75, 3, 9, NULL, NULL),
(76, 4, 9, NULL, NULL),
(77, 5, 9, NULL, NULL),
(78, 6, 9, NULL, NULL),
(79, 7, 9, NULL, NULL),
(80, 8, 9, NULL, NULL),
(81, 9, 9, NULL, NULL),
(82, 1, 10, NULL, NULL),
(83, 2, 10, NULL, NULL),
(84, 3, 10, NULL, NULL),
(85, 4, 10, NULL, NULL),
(86, 5, 10, NULL, NULL),
(87, 6, 10, NULL, NULL),
(88, 7, 10, NULL, NULL),
(89, 8, 10, NULL, NULL),
(90, 9, 10, NULL, NULL),
(91, 1, 11, NULL, NULL),
(92, 2, 11, NULL, NULL),
(93, 3, 11, NULL, NULL),
(94, 4, 11, NULL, NULL),
(95, 5, 11, NULL, NULL),
(96, 6, 11, NULL, NULL),
(97, 7, 11, NULL, NULL),
(98, 8, 11, NULL, NULL),
(99, 9, 11, NULL, NULL),
(100, 1, 12, NULL, NULL),
(101, 2, 12, NULL, NULL),
(102, 3, 12, NULL, NULL),
(103, 4, 12, NULL, NULL),
(104, 5, 12, NULL, NULL),
(105, 6, 12, NULL, NULL),
(106, 7, 12, NULL, NULL),
(107, 8, 12, NULL, NULL),
(108, 9, 12, NULL, NULL),
(109, 113, 13, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` bigint(20) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `order`, `slug`, `status`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Kathmandu', 1, 'kathmandu', 1, NULL, '2021-02-06 05:35:21', '2021-02-06 05:35:21'),
(2, 'Bhaktapur', 2, 'bhaktapur', 1, NULL, '2021-02-06 05:35:21', '2021-02-06 05:35:21'),
(3, 'Lalitpur', 3, 'lalitpur', 1, NULL, '2021-02-06 05:35:21', '2021-02-06 05:35:21'),
(4, 'Chitwan', 4, 'chitwan', 1, NULL, '2021-02-06 05:35:22', '2021-02-06 05:35:22'),
(5, 'Biratnagar', 5, 'biratnagar', 1, NULL, '2021-02-06 05:35:22', '2021-02-06 05:35:22'),
(6, 'Pokhara', 6, 'pokhara', 1, NULL, '2021-02-06 05:35:22', '2021-02-06 05:35:22'),
(7, 'Dharan', 7, 'dharan', 1, NULL, '2021-02-06 05:35:23', '2021-02-06 05:35:23'),
(8, 'Butwal', 8, 'butwal', 1, NULL, '2021-02-06 05:35:23', '2021-02-06 05:35:23'),
(9, 'Others', 9, 'others', 1, NULL, '2021-02-06 05:35:23', '2021-02-06 05:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `commentable_id` bigint(20) UNSIGNED NOT NULL,
  `commentable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `body`, `bid`, `url`, `user_id`, `parent_id`, `commentable_id`, `commentable_type`, `status`, `created_at`, `updated_at`) VALUES
(1, '..', NULL, 'https://merooffer.com/ad/the-lunch-delight', 68, NULL, 14, 'App\\Models\\Product', 0, '2021-08-26 03:22:04', '2021-08-26 03:22:04');

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE `conditions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Unboxed', 1, '2021-02-06 05:35:19', '2021-02-06 05:35:19'),
(2, 'Brand New', 1, '2021-02-06 05:35:19', '2021-02-06 05:35:19'),
(3, 'Almost Like New', 1, '2021-02-06 05:35:20', '2021-02-06 05:35:20'),
(4, 'Excellent', 1, '2021-02-06 05:35:20', '2021-02-06 05:35:20'),
(5, 'Good / Fair', 1, '2021-02-06 05:35:20', '2021-02-06 05:35:20'),
(6, 'Not Working', 1, '2021-02-06 05:35:20', '2021-02-06 05:35:20');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `subject`, `desc`, `product_id`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Ramesh', 'ramesh@gmail.com', 'ramesh', 'Hi, I\'m interested in \"Lunch Delight\". Is this still available? Cheers.', 14, 22, 0, '2021-07-12 21:35:32', '2021-07-12 21:35:32'),
(2, 'Pramod Bhandari', 'pramodbhandari68@gmail.com', 'New Product Launces', 'Hi, I\'m interested in \"PIneapple 1 pound\". Is this still available? Cheers.', 24, 28, 0, '2021-07-12 21:46:03', '2021-07-12 21:46:03');

-- --------------------------------------------------------

--
-- Table structure for table `couriers`
--

CREATE TABLE `couriers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_free` int(11) NOT NULL,
  `cost` decimal(8,2) DEFAULT 0.00,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `couriers`
--

INSERT INTO `couriers` (`id`, `name`, `description`, `url`, `is_free`, `cost`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Courier Service', 'this is a courier service', NULL, 0, 100.00, 1, '2021-02-06 05:34:55', '2021-02-06 05:34:55');

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `title`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, '15 Days', 1, '2021-02-06 05:35:41', '2021-02-06 05:35:41'),
(2, NULL, '30 Days', 1, '2021-02-06 05:35:42', '2021-02-06 05:35:42'),
(3, NULL, '45 Days', 1, '2021-02-06 05:35:43', '2021-02-06 05:35:43'),
(4, NULL, '60 Days', 1, '2021-02-06 05:35:43', '2021-02-06 05:35:43');

-- --------------------------------------------------------

--
-- Table structure for table `durations`
--

CREATE TABLE `durations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `durations`
--

INSERT INTO `durations` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Less than 1 week', 'less-than-1-week', 1, '2021-02-06 05:35:00', '2021-02-06 05:35:00'),
(2, '1 week to 2 weeks', '1-week-to-2-weeks', 1, '2021-02-06 05:35:00', '2021-02-06 05:35:00'),
(3, '2 weeks to 4 weeks', '2-weeks-to-4-weeks', 1, '2021-02-06 05:35:00', '2021-02-06 05:35:00'),
(4, '1 months to 2 months', '1-months-to-2-months', 1, '2021-02-06 05:35:01', '2021-02-06 05:35:01'),
(5, '2 months to 3 months', '2-months-to-3-months', 1, '2021-02-06 05:35:01', '2021-02-06 05:35:01'),
(6, '3 months to 4 months', '3-months-to-4-months', 1, '2021-02-06 05:35:01', '2021-02-06 05:35:01'),
(7, '4 months to 5 months', '4-months-to-5-months', 1, '2021-02-06 05:35:01', '2021-02-06 05:35:01'),
(8, '5 months to 6 months', '5-months-to-6-months', 1, '2021-02-06 05:35:02', '2021-02-06 05:35:02'),
(9, 'Over 6 months / Ongoing', 'over-6-months-ongoing', 1, '2021-02-06 05:35:02', '2021-02-06 05:35:02'),
(10, 'Unspecified', 'unspecified', 1, '2021-02-06 05:35:02', '2021-02-06 05:35:02');

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE `education` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `course` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `achievement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `greet` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_greet` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `from`, `greet`, `body`, `subject`, `end_greet`, `status`, `created_at`, `updated_at`) VALUES
(1, 'noreply@merooffer.com', 'Greetings from Mero Offer', '<p>We Nepalese also celebrate the monsoon season as a festival in the most beautiful way, Ropain in Ashad 15. A day that symbolizes the end of the rice planting period, a day when all farmers enjoy the completion of the plantation and wishes for good production. A party is thrown at the end of the completion of the plantation and like any normal event, it is incomplete without guest, food, drinks and music; Asadh 15 event is no different, only the dress code should be as informal as you can make it. The reason for such dress code is you got to show your moves and grooves in the muddy dance floor.</p>\r\n\r\n<p><img alt=\"\" src=\"https://merooffer.com/ckfinder/userfiles/files/asar15.jpg\" style=\"height:338px; width:600px\" /></p>\r\n\r\n<p>Regards,</p>\r\n\r\n<p>Mero Offer Team</p>', 'Happy Ashar 15', 'Skyfall Technologies Pvt. Ltd.', 1, '2021-03-17 02:36:35', '2021-06-29 07:55:27'),
(2, 'alert@merooffer.com', 'Greetings from Mero Offer', '<p>Eid al-Adha (Arabic: عید الاضحیٰ&lrm;, romanized: Eid &#39;al &#39;Adha, lit.&thinsp;&#39;Festival of the Sacrifice&#39;) is the latter of the two official holidays celebrated within Islam (the other being Eid al-Fitr). It honors the willingness of Ibrahim (Abraham) to sacrifice his son Ishmael (in Judaism, Isaac) as an act of obedience to God&#39;s command. Before Abraham could sacrifice his son, however, God provided a lamb to sacrifice instead. In commemoration of this intervention, animals are sacrificed ritually. One third of their meat is consumed by the family offering the sacrifice, while the rest is distributed to the poor and needy. Sweets and gifts are given, and extended family are typically visited and welcomed.&nbsp;The day is also sometimes called Big Eid or the Greater Eid.</p>\r\n\r\n<p>In the Islamic lunar calendar, Eid al-Adha falls on the 10th day of Dhu al-Hijjah, and lasts for four days. In the international (Gregorian) calendar, the dates vary from year to year, shifting approximately 11 days earlier each year.</p>\r\n\r\n<p><img alt=\"\" src=\"https://merooffer.com/ckfinder/userfiles/files/Eid2_1626753550385.jpg\" style=\"height:675px; width:1200px\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:22px\"><strong>Bakra Eid Mubarak</strong></span></p>', 'Bakra Eid Mubarak', 'Skyfall Technologies Pvt. Ltd.', 1, '2021-03-22 01:49:34', '2021-07-20 19:08:55');

-- --------------------------------------------------------

--
-- Table structure for table `experiences`
--

CREATE TABLE `experiences` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `experiences`
--

INSERT INTO `experiences` (`id`, `name`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'No Experience', 1, 'no-experience', '2021-02-06 05:34:56', '2021-02-06 05:34:56'),
(2, '1/2 year', 1, '12-year', '2021-02-06 05:34:56', '2021-02-06 05:34:56'),
(3, '0-1 year', 1, '0-1-year', '2021-02-06 05:34:57', '2021-02-06 05:34:57'),
(4, '1-2 years', 1, '1-2-years', '2021-02-06 05:34:57', '2021-02-06 05:34:57'),
(5, '2-3 years', 1, '2-3-years', '2021-02-06 05:34:57', '2021-02-06 05:34:57'),
(6, '3-4 years', 1, '3-4-years', '2021-02-06 05:34:58', '2021-02-06 05:34:58'),
(7, '4-5 years', 1, '4-5-years', '2021-02-06 05:34:58', '2021-02-06 05:34:58'),
(8, '5-6 years', 1, '5-6-years', '2021-02-06 05:34:58', '2021-02-06 05:34:58'),
(9, '6-7 years', 1, '6-7-years', '2021-02-06 05:34:58', '2021-02-06 05:34:58'),
(10, '7-10 years', 1, '7-10-years', '2021-02-06 05:34:59', '2021-02-06 05:34:59'),
(11, '10+ years', 1, '10-years', '2021-02-06 05:34:59', '2021-02-06 05:34:59');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fapplicants`
--

CREATE TABLE `fapplicants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `application_letter` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `astatus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `fjob_id` bigint(20) UNSIGNED NOT NULL,
  `mail_status` tinyint(1) NOT NULL DEFAULT 0,
  `bid` bigint(20) NOT NULL,
  `day` bigint(20) DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fapplicants`
--

INSERT INTO `fapplicants` (`id`, `application_letter`, `astatus`, `user_id`, `fjob_id`, `mail_status`, `bid`, `day`, `attachment`, `created_at`, `updated_at`) VALUES
(1, 'this is cover leter', 'rejected', 9, 1, 0, 40000, 20, NULL, '2021-02-22 22:18:29', '2021-02-22 22:18:29'),
(2, 'jjkkkk', 'hired', 28, 1, 0, 56, 28, NULL, '2021-03-20 22:31:53', '2021-03-20 22:31:53');

-- --------------------------------------------------------

--
-- Table structure for table `fapplicant_fjobs`
--

CREATE TABLE `fapplicant_fjobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fapplicant_user_id` bigint(20) UNSIGNED NOT NULL,
  `fjob_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fapplicant_fjobs`
--

INSERT INTO `fapplicant_fjobs` (`id`, `fapplicant_user_id`, `fjob_id`, `created_at`, `updated_at`) VALUES
(1, 9, 1, '2021-02-22 22:18:29', '2021-02-22 22:18:29'),
(2, 28, 1, '2021-03-20 22:31:53', '2021-03-20 22:31:53');

-- --------------------------------------------------------

--
-- Table structure for table `fcategories`
--

CREATE TABLE `fcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `font` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` bigint(20) NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `menu` tinyint(1) NOT NULL DEFAULT 1,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fcategories`
--

INSERT INTO `fcategories` (`id`, `name`, `status`, `font`, `color`, `order`, `desc`, `featured`, `menu`, `image`, `parent_id`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Graphics & Design', 1, 'fas fa-paint-brush', '#86267e', 1, NULL, 1, 1, NULL, NULL, 'graphics-design', '2021-02-06 05:36:27', '2021-02-06 05:36:33'),
(2, 'Programming & Tech', 1, 'fas fa-code', '#469990', 2, NULL, 1, 1, NULL, NULL, 'programming-tech', '2021-02-06 05:36:27', '2021-02-06 05:36:33'),
(3, 'Websites and apps', 1, 'fas fa-globe', '#c25762', 3, NULL, 0, 1, NULL, NULL, 'websites-and-apps', '2021-02-06 05:36:27', '2021-02-06 05:36:33'),
(4, 'Digital Marketing', 1, 'fas fa-chart-bar', '#86267e', 4, NULL, 1, 1, NULL, NULL, 'digital-marketing', '2021-02-06 05:36:28', '2021-02-06 05:36:33'),
(5, 'Writing & Content', 1, 'fas fa-pencil-alt', '#46b75d', 5, NULL, 0, 1, NULL, NULL, 'writing-content', '2021-02-06 05:36:29', '2021-02-06 05:36:34'),
(6, 'Data Entry & Admin', 1, 'fas fa-keyboard', '#39444c', 6, NULL, 1, 1, NULL, NULL, 'data-entry-admin', '2021-02-06 05:36:29', '2021-02-06 05:36:34'),
(7, 'Marketing & Customer Service', 1, 'fas fa-bullhorn', '#86267e', 7, NULL, 1, 1, NULL, NULL, 'marketing-customer-service', '2021-02-06 05:36:29', '2021-02-06 05:36:34'),
(8, 'Video & Animation', 1, 'fas fa-video', '#39444c', 8, NULL, 0, 1, NULL, NULL, 'video-animation', '2021-02-06 05:36:30', '2021-02-06 05:36:35'),
(9, 'Agriculture', 1, 'fas fa-leaf', '#46b75d', 9, NULL, 0, 1, NULL, NULL, 'agriculture', '2021-02-06 05:36:30', '2021-02-06 05:36:35'),
(10, 'Business & Accounting', 1, 'fas fa-chart-bar', '#964000', 10, NULL, 0, 1, NULL, NULL, 'business-accounting', '2021-02-06 05:36:30', '2021-02-06 05:36:35'),
(11, 'Local Jobs & Services', 1, 'fas fa-fire', '#4B3A26', 11, NULL, 1, 1, NULL, NULL, 'local-jobs-services', '2021-02-06 05:36:30', '2021-02-06 05:36:36'),
(12, 'Protective & Security Services', 1, 'fas fa-star', '#0B6623', 12, NULL, 0, 1, NULL, NULL, 'protective-security-services', '2021-02-06 05:36:30', '2021-02-06 05:36:36'),
(13, 'Teaching/Education', 1, 'fas fa-bookmark', '#0E4C92', 13, NULL, 0, 1, NULL, NULL, 'teachingeducation', '2021-02-06 05:36:31', '2021-02-06 05:36:36'),
(14, 'Human Resource & ORG Development', 1, 'fas fa-users', '#784B84', 14, NULL, 0, 1, NULL, NULL, 'human-resource-org-development', '2021-02-06 05:36:31', '2021-02-06 05:36:36'),
(15, 'Commercial/Logistics & Supply Chain', 1, 'fas fa-ambulance', '#DE3163', 15, NULL, 0, 1, NULL, NULL, 'commerciallogistics-supply-chain', '2021-02-06 05:36:31', '2021-02-06 05:36:36'),
(16, 'Research & Development', 1, 'fas fa-cogs', '#dcbeff', 16, NULL, 0, 1, NULL, NULL, 'research-development', '2021-02-06 05:36:32', '2021-02-06 05:36:37'),
(17, 'Journalism & Media/Editor', 1, 'fas fa-edit', '#964000', 17, NULL, 0, 1, NULL, NULL, 'journalism-mediaeditor', '2021-02-06 05:36:32', '2021-02-06 05:36:37'),
(18, 'Product Sourcing & Manufacturing', 1, 'fas fa-chart-pie', '#F8E473', 18, NULL, 0, 1, NULL, NULL, 'product-sourcing-manufacturing', '2021-02-06 05:36:32', '2021-02-06 05:36:37'),
(19, 'Others', 1, 'fas fa-tint', '#88807B', 19, NULL, 0, 1, NULL, NULL, 'others', '2021-02-06 05:36:32', '2021-02-06 05:36:37');

-- --------------------------------------------------------

--
-- Table structure for table `firmusers`
--

CREATE TABLE `firmusers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_phone` tinyint(1) NOT NULL DEFAULT 0,
  `show_email` tinyint(1) NOT NULL DEFAULT 0,
  `fb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `firmusers`
--

INSERT INTO `firmusers` (`id`, `name`, `logo`, `img1`, `img2`, `url`, `desc`, `phone`, `email`, `show_phone`, `show_email`, `fb`, `insta`, `twitter`, `youtube`, `linkedin`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'i-creation', 'firmusers/idAqPGLcwJcd9P8zLm0240NwA.jpg', NULL, NULL, 'http://icreationtech.com', 'We believe in evolution, expansion, and extension. Our team works for your professional success, dedicating our time and energy to designing innovative ideas for business houses to drive growth and stand out in a virtual world. Our main objective is to provide our clients with leading ideas and leverage the use of technology to soothe the process of achieving business success. We are a research-based technology company with advanced data analytics using the latest technology and digital marketing for the client’s organizational growth.', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 7, '2021-02-06 05:34:54', '2021-02-07 07:42:57'),
(2, 'Incube Technologies', 'firmusers/a0j5PdQQRPmCo8kA1Tkp3sszj.png', NULL, NULL, 'www.incubeweb.com', '', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 6, '2021-02-06 05:34:54', '2021-02-06 05:34:54'),
(3, 'Delta Tech', 'firmusers/ydhCYZDAynnqPOvVq5zycwPHw.png', NULL, NULL, 'http://dtechnepal.com/', 'Delta Tech is a Software Development Company based in Nepal. Established in 2016, we have a brilliant team of software developers, designers and technical support members, and over 1500 happy clients globally. We are a member of RK Golchha Group and Golchha Organization, one of the oldest and most esteemed business houses in Nepal. Golchha Organization, which was established in 1934, employs over 20,000 people across Nepal and has an annual turnover of over 400 million USD\r\n\r\nDelta Tech started as a web and mobile app development company. Initially, we entered the tech business with a motive to become a top service-based technology company. After a few months of continuous market research, we realized that many organizations need user-friendly business software to enhance their productivity, performance, and ROI. Thus, we started focusing on building SaaS applications that can solve common business problems.\r\n\r\nOur current suite of products include:\r\n\r\n1. Delta Sales App\r\n\r\nAn app to track field sales employees and manage sales of business\r\n\r\n\r\n2. Delta Inventory\r\n\r\nA cloud-based inventory management solution with CRM, smart stock alerts and analytics\r\n\r\n3. Delta Sales CRM\r\n\r\nSales CRM software to manage prospective customers and convert more leads into sales\r\n\r\n\r\n4. Delta Retailer App\r\n\r\nDelta Retailer App is an easy-to-use mobile app that connects Retailers, Distributors and Brands, by automating the ordering process for Retail Chains, Kiranas and Mom-and-Pop Stores.\r\n\r\nThese products are designed to make operations smoother and hasslefree for entrepreneurs and enterprises. Our innovative and user-friendly software can help in transforming the way businesses work and improve productivity. At Delta Tech, we believe customer satisfaction is a crucial factor that determines a company’s success. Considering customer convenience and usability, our software applications ensure easy configuration and setup. Our highly responsive support team will always be at your service to resolve any difficulties while using any of our products. We also provide tutorials, webinars, demo, and other resources so that you can make the best use of our product.\r\n\r\nWe also provide IT services which include:\r\n\r\nWeb Designing\r\nE-commerce Solutions\r\nWeb Application Development\r\nMobile App Development\r\nCustomized Software Development\r\nSearch Engine Optimization (SEO)\r\nGraphics Designing', '9804067116', 'career@deltatechnepal.com', 0, 0, 'https://www.facebook.com/deltatechnepal/', 'https://www.instagram.com/deltatechnepal', 'https://twitter.com/deltatechnepal', 'https://www.youtube.com/channel/UCDH4jR0cUgoTHV1pDrDoeHg', 'https://www.linkedin.com/company/13199442/admin/', 2, '2021-02-06 05:34:54', '2021-02-07 07:43:46'),
(4, 'PASCHIMANCHAL SOLUTION PVT. LTD.', 'firmusers/pRIUv7Mx5G0PeOCK8GrFOihaB.png', NULL, NULL, 'www.asalnepal.com', NULL, '9851155371', 'jobsatpspl@gmail.com', 0, 0, NULL, NULL, NULL, NULL, NULL, 27, '2021-02-06 05:34:55', '2021-02-07 07:49:35'),
(6, 'Pacific Engineering Co. Pvt. Ltd.', 'firmusers/7qzzoDtf0jkiZJTKhRNUcfWSw.jpg', 'firmusers/pfYr4rZeqJEo9PCOGeH0hARCi.jpg', NULL, 'www.pacificeng.com.np', 'Pacific Engineering Co. Pvt. Ltd is an “A” Class construction company specializing in the construction of major infrastructure projects and has served the nation as a dynamic Nepali construction firm after our Chairman Mr. Thakur Prasad Kandel entered the construction sector as a young entrepreneur in 1981 A.D. In the present day, the company has proven itself to be one of the largest physical infrastructure construction companies in Nepal with a track record of having completed 200+ projects of different nature, complexity, and volume. The company possesses a strong passion for construction which is reflected by the wealth of national road, bridge, irrigation, hydropower, bus park and airport projects that the company has successfully completed in the past 40+ years of operation.', '014377009', 'info@pacificeng.com.np', 1, 1, NULL, NULL, NULL, NULL, 'https://www.linkedin.com/company/pacific-engineering-pvt-ltd?trk=public_profile_topcard-current-company', 36, '2021-03-08 22:44:30', '2021-03-08 22:56:20'),
(7, 'Purple Haze Rock Bar', 'firmusers/KTNRm519rwbzcEcOixm4FoZ9e.jpg', 'firmusers/9enjQsj2CRWoReVU5RadYK4d4.jpg', NULL, NULL, 'Purple Haze Rock Bar A large stage for live, American-style rock shows anchors this buzzy nightspot for drinks & dancing,a true  iconic landmark of Thamel, Kathmandu.\r\n A central hub for all rock music lovers, this night spot boasts a large stage, stadium seating, amazing drinks ,food and dance till late night .\r\n\r\nFamous artist like - Eric Martin,  nitro, bumblefoot of GNR , Chris Adler of Lamb of God and many more international artists have graced the stage  here. It\'s also a popular venue for underground gigs &  numerous live band have recorded here over the years.\r\nThe pub  has been a top destination for visitors as well as locals  for a decade now.', '9803719781', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 38, '2021-03-16 01:32:58', '2021-03-16 01:49:50'),
(8, 'Executive Search - Mero Offer', 'firmusers/yOIbtQN2G2MkjSCkKGGZy2208.png', 'firmusers/9LxYSoOvtYao9FWWDQDkWciCT.jpg', NULL, 'https://merooffer.com/', 'Merooffer.com is the next generation of free online classifieds. It provides a simple solution to the complications involved in selling, buying, trading, discussing and meeting people near you.', '9849551992', 'hr@merooffer.com', 0, 0, NULL, NULL, NULL, NULL, NULL, 28, '2021-04-07 00:58:07', '2021-04-07 00:58:07'),
(9, 'JSS FURNITURE & DECOR', 'firmusers/jrV3oWlle0aNryLaS6TbK87Vc.jpg', 'firmusers/IFdHFCMXoIuIm3m2auVz0OtYs.JPG', 'firmusers/EzaBIBXq563YTjzWEEztmqmGF.JPG', 'WWW.JSSDECOR.COM', 'We provide wicker furniture that are comfortable stylish and long lasting. We make wicker furniture with high quality rattan material it come out with strong and sturdy natural wicker to create a unique design.', '9840006419', 'decorjss@gmail.com', 1, 0, 'https://www.facebook.com/113781500467782/posts/256741092838488/?app=fbl', NULL, NULL, NULL, NULL, 47, '2021-04-09 00:21:51', '2021-04-09 00:21:51'),
(10, 'Outlines Research and Development', 'firmusers/hT1s74NjyE5CZlAJSD2EhWpTI.png', NULL, NULL, 'www.outlines-rnd.com', 'Outlines Research & Development is a research and consulting knowledge-based company working in various sectors like Government, Finance, Cooperative, Education, Tourism, Development, and Private sectors. We expertise in the area of Research, Information Technology, Management, Capacity Building, and Project Development & Implementation. We have recently started exploring in the area of Conservation, Cultural Tourism Development, and Disaster Management.\r\n\r\nOur mission is to inspire better Nepali living standards through knowledge, technology,  skills, and management.\r\n\r\nOur Goal is to be a Partner to develop business solutions for market growth, economic impact, and sustainability in diverse areas.', '5260469', 'info@outlines.com', 0, 0, NULL, NULL, NULL, NULL, NULL, 39, '2021-04-11 01:28:33', '2021-04-11 01:28:33');

-- --------------------------------------------------------

--
-- Table structure for table `fjobs`
--

CREATE TABLE `fjobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `feature` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `budget` double(8,2) DEFAULT NULL,
  `budget_to` double(8,2) DEFAULT NULL,
  `budget_avg` double(8,2) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 0,
  `order` bigint(20) NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` int(11) DEFAULT NULL,
  `longitude` int(11) DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL,
  `jobtype_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `fcategory_id` bigint(20) UNSIGNED NOT NULL,
  `endate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `duration_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fjobs`
--

INSERT INTO `fjobs` (`id`, `feature`, `name`, `body`, `budget`, `budget_to`, `budget_avg`, `views`, `status`, `order`, `address`, `latitude`, `longitude`, `expired_at`, `jobtype_id`, `fcategory_id`, `endate`, `user_id`, `duration_id`, `slug`, `created_at`, `updated_at`) VALUES
(1, 1, 'Javascript Developer for a minor project', 'Vue js developer for a project --- mobile app development and website development', 30000.00, 50000.00, 40000.00, 230, 1, 1, 'Kathmandu', NULL, NULL, '2021-03-09 06:17:43', 1, 2, '2021/02/30', 28, 5, 'javascript-developer-for-a-minor-project', '2021-02-06 22:17:43', '2021-02-07 09:26:22');

-- --------------------------------------------------------

--
-- Table structure for table `fjob_skill`
--

CREATE TABLE `fjob_skill` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `skill_id` bigint(20) UNSIGNED NOT NULL,
  `fjob_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fjob_skill`
--

INSERT INTO `fjob_skill` (`id`, `skill_id`, `fjob_id`, `created_at`, `updated_at`) VALUES
(6, 19, 1, '2021-02-07 09:26:22', '2021-02-07 09:26:22'),
(7, 20, 1, '2021-02-07 09:26:22', '2021-02-07 09:26:22'),
(8, 9, 1, '2021-02-07 09:26:22', '2021-02-07 09:26:22'),
(9, 9, 1, '2021-02-07 09:26:22', '2021-02-07 09:26:22');

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `leader_id` bigint(20) UNSIGNED NOT NULL,
  `followed_by` bigint(20) UNSIGNED NOT NULL,
  `is_company` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `status`, `leader_id`, `followed_by`, `is_company`, `created_at`, `updated_at`) VALUES
(10, 0, 28, 50, 0, '2021-04-10 00:07:00', '2021-04-10 00:07:00'),
(12, 0, 42, 51, 0, '2021-04-11 23:10:11', '2021-04-11 23:10:11'),
(17, 0, 50, 28, 0, '2021-07-12 23:39:03', '2021-07-12 23:39:03'),
(18, 0, 22, 28, 0, '2021-07-22 23:09:39', '2021-07-22 23:09:39'),
(19, 0, 22, 64, 0, '2021-08-06 02:17:10', '2021-08-06 02:17:10'),
(20, 0, 22, 67, 0, '2021-08-21 05:24:20', '2021-08-21 05:24:20'),
(21, 0, 22, 68, 0, '2021-08-26 03:21:01', '2021-08-26 03:21:01');

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

CREATE TABLE `industries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` bigint(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`id`, `name`, `order`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Banks', 1, 1, 'banks', '2021-02-06 05:35:35', '2021-02-06 05:35:35'),
(2, 'Construction / Real Estate', 2, 1, 'construction-real-estate', '2021-02-06 05:35:35', '2021-02-06 05:35:35'),
(3, 'Information / Computer / Tech', 3, 1, 'information-computer-tech', '2021-02-06 05:35:36', '2021-02-06 05:35:36'),
(4, 'Software Companies', 4, 1, 'software-companies', '2021-02-06 05:35:37', '2021-02-06 05:35:37'),
(5, 'Education- School & Colleges', 5, 1, 'education-school-colleges', '2021-02-06 05:35:37', '2021-02-06 05:35:37'),
(6, 'Finance Companies', 6, 1, 'finance-companies', '2021-02-06 05:35:37', '2021-02-06 05:35:37'),
(7, 'ISP', 7, 1, 'isp', '2021-02-06 05:35:37', '2021-02-06 05:35:37'),
(8, 'NGO / INGO / Development Projects', 8, 1, 'ngo-ingo-development-projects', '2021-02-06 05:35:37', '2021-02-06 05:35:37'),
(9, 'Mandufacturing / Engineering', 9, 1, 'mandufacturing-engineering', '2021-02-06 05:35:38', '2021-02-06 05:35:38'),
(10, 'Travel Agents / Tour Operators', 10, 1, 'travel-agents-tour-operators', '2021-02-06 05:35:38', '2021-02-06 05:35:38'),
(11, 'Advertising Agency', 11, 1, 'advertising-agency', '2021-02-06 05:35:38', '2021-02-06 05:35:38'),
(12, 'Distribution Companies', 12, 1, 'distribution-companies', '2021-02-06 05:35:38', '2021-02-06 05:35:38'),
(13, 'Poultry / Dairy / Veterinary', 13, 1, 'poultry-dairy-veterinary', '2021-02-06 05:35:38', '2021-02-06 05:35:38'),
(14, 'Designing / Printing / Publication', 14, 1, 'designing-printing-publication', '2021-02-06 05:35:39', '2021-02-06 05:35:39'),
(15, 'BPO / Call Center / ITES', 15, 1, 'bpo-call-center-ites', '2021-02-06 05:35:39', '2021-02-06 05:35:39'),
(16, 'Training Institutes', 16, 1, 'training-institutes', '2021-02-06 05:35:39', '2021-02-06 05:35:39'),
(17, 'Architecture / Interior Designing', 17, 1, 'architecture-interior-designing', '2021-02-06 05:35:39', '2021-02-06 05:35:39'),
(18, 'Security Service Company', 18, 1, 'security-service-company', '2021-02-06 05:35:40', '2021-02-06 05:35:40'),
(19, 'Garments / Carpet Industries', 19, 1, 'garments-carpet-industries', '2021-02-06 05:35:40', '2021-02-06 05:35:40'),
(20, 'Retail / Shops', 20, 1, 'retail-shops', '2021-02-06 05:35:40', '2021-02-06 05:35:40'),
(21, 'Logistics / Courier / Air Express Companies', 21, 1, 'logistics-courier-air-express-companies', '2021-02-06 05:35:41', '2021-02-06 05:35:41'),
(22, 'Research Firms', 22, 1, 'research-firms', '2021-02-06 05:35:41', '2021-02-06 05:35:41'),
(23, 'Hotels / Restaurants / Resort', 23, 1, 'hotels-restaurants-resort', '2021-03-16 01:39:05', '2021-03-16 01:39:05');

-- --------------------------------------------------------

--
-- Table structure for table `jattributes`
--

CREATE TABLE `jattributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `font` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `filterable` tinyint(1) NOT NULL DEFAULT 0,
  `data_type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jattributevalues`
--

CREATE TABLE `jattributevalues` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `font` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jattribute_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jattribute_job_category`
--

CREATE TABLE `jattribute_job_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `jattribute_id` bigint(20) UNSIGNED NOT NULL,
  `jobcategory_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `feature` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` double(8,2) DEFAULT NULL,
  `salary_to` double(8,2) DEFAULT NULL,
  `salary_avg` double(8,2) DEFAULT NULL,
  `education` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vac_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 0,
  `order` bigint(20) NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` int(11) DEFAULT NULL,
  `longitude` int(11) DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL,
  `experience_id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `day_id` bigint(20) UNSIGNED NOT NULL,
  `jobtype_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `salarytype_id` bigint(20) UNSIGNED DEFAULT NULL,
  `level_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `positiontype_id` bigint(20) UNSIGNED NOT NULL,
  `endate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `industry_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `feature`, `name`, `body`, `salary`, `salary_to`, `salary_avg`, `education`, `vac_no`, `views`, `status`, `order`, `address`, `latitude`, `longitude`, `gender`, `age`, `expired_at`, `experience_id`, `city_id`, `day_id`, `jobtype_id`, `salarytype_id`, `level_id`, `category_id`, `positiontype_id`, `endate`, `industry_id`, `user_id`, `slug`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'PHP Developer (Remote)', 'Job Description\r\nDelta Tech is seeking a PHP Developer: Remote who is an in-depth understanding of advanced PHP, MCV frameworks, and also creative and innovative. You will be working on various internal products (Our products: DeltaSalesApp, DeltaSalesCRM, DeltaRetailerApp & DeltaInventory) and external projects with different levels of complexities and challenges.\r\n\r\nRole and Responsibilities:\r\n\r\nYour responsibilities will include (but are not limited to):\r\n\r\nBuild server-side applications with the latest technologies for delivering the best experience for users\r\nBuild platforms, frameworks, APIs, libraries & automated tools\r\nKeeping up-to-date with technology\r\nMentoring and assigning tasks to junior web developers\r\nClient Communication\r\nWhat We Offer:\r\n\r\nDelta Tech is looking for candidates who are loyal and plan to make a long-term commitment. In return for your loyalty we offer:\r\n\r\nPosition Benefits:\r\n\r\nCompetitive/ Attractive Salary (No bar for right and deserving candidate)\r\nTimely performance appraisal and salary reviews\r\nPaid holiday & paid leaves\r\nFestive bonus\r\nGreat work culture\r\nFlexible timings against performance', NULL, NULL, NULL, 'Bachelor', '2', 186, 1, 3, NULL, NULL, NULL, NULL, NULL, '2021-10-08 15:26:06', 5, 5, 2, 7, 1, 2, 7, 1, '2021/02/30', 3, 2, 'php-developer-remote', NULL, '2021-02-06 07:26:06', '2021-08-08 19:55:09'),
(2, 1, 'Operation Officer', 'Our expanding company is seeking to hire an Operations Manager to join our leadership team. You will be in charge of providing inspired leadership for the operation for one of our organization\'s lines of business, which involves making important policy and strategic decisions, as well as the development and implementation of operational policies and procedures. You will also be assisting our Human Resources with recruiting, when necessary, and help promote a company culture that encourages morale and performance.\r\n\r\nSuccessful candidates will have a Bachelor\'s degree in operations management (or a related field) and have prior experience in a management or leadership position. A deep understanding of financial management is also a plus.\r\n\r\nOperations Manager Responsibilities:\r\n• Perform all the administrative tasks.\r\n• Provide inspired leadership for the organization.\r\n• Make important policy, planning, and strategy decisions.\r\n• Develop, implement, and review operational policies and procedures.\r\n• Assist HR with recruiting when necessary.\r\n• Help promote a company culture that encourages top performance and high morale.\r\n• Oversee budgeting, reporting, planning, and auditing.\r\n• Work with Management.\r\n• Ensure all legal and regulatory documents are filed and monitor compliance with laws and regulations.\r\n• Work with the Management Team to determine values and mission, and plan for short and long-term goals.\r\n• Identify and address problems and opportunities for the company.\r\n• Build alliances and partnerships with other organizations.\r\n• Support worker communication with the management team.\r\nOperations Manager Requirements:\r\n• Bachelor’s degree in operations management or related field.\r\n• Experience in management, operations, and leadership for more than a year.\r\n• Maintain a solid accounting system for the organization.\r\n• Understanding of general finance and budgeting, including profit and loss, balance sheet, and cash-flow management.\r\n• Ability to build consensus and relationships among managers, partners, and employees.\r\n• Excellent communication skills.\r\n• Solid understanding of financial management.\r\nFor Email apply@icreationtech.com', NULL, NULL, NULL, 'Bachelor', '1', 136, 1, 1, NULL, NULL, NULL, NULL, NULL, '2021-10-07 15:27:33', 4, 1, 4, 7, 1, 2, 7, 6, '2021/02/30', 3, 7, 'operation-officer', NULL, '2021-02-06 07:27:33', '2021-08-08 19:55:09'),
(3, 1, 'Messenger / Store keeper', 'Job Description\r\n\r\n• Experience in stock management and distribution.\r\n• Stores supplies and equipment in storerooms\r\n• Maintains accurate records of stocks received and issued\r\n• Receive the materials from the suppliers, check the receipt\r\n• Coordinate and execute the import processing\r\n• Maintain stock and keep up-to-date record and reporting\r\n• Responsible for arranging, preservation, recording & issue of the items\r\n\r\nApply Instructions:\r\nKindly submit your CV and cover letter with the expected salary to jobsatpspl@gmail.com. We may close the vacancy after we got the qualified candidates before the end of the deadline.', NULL, NULL, NULL, 'Bachelor', '1', 190, 1, 4, NULL, NULL, NULL, NULL, NULL, '2021-10-07 15:29:23', 4, 1, 4, 7, 1, 2, 7, 1, '2021/02/06', 3, 27, 'messenger-store-keeper', NULL, '2021-02-06 07:29:23', '2021-08-08 19:55:09'),
(4, 1, 'Junior Accountant', 'Job Description:\r\n• Post and process journal entries to ensure all business transactions are recorded\r\n• Update accounts receivable and issue invoices\r\n• Update accounts payable and perform reconciliations\r\n• Assist in the processing of balance sheets, income statements and other financial statements\r\n• Prepare and submit weekly/monthly reports\r\n• Assist senior accountants in the preparation of monthly/yearly closings\r\n• Assist with other accounting/ financial projects\r\n\r\nApply Instructions\r\nKindly submit your CV and cover letter with the expected salary on jobsatpspl@gmail.com We may close the vacancy after we got the qualified candidates before the end of the deadline.\r\n\r\nNote: The employee will have to travel frequently to the project sites, inside and outside the valley, according to the demands of the project.', NULL, NULL, NULL, 'Bachelor', '1', 74, 1, 5, NULL, NULL, NULL, NULL, NULL, '2021-10-07 15:32:00', 3, 1, 4, 5, 1, 2, 9, 1, '2021/02/06', 6, 27, 'junior-accountant', NULL, '2021-02-06 07:32:00', '2021-08-08 19:55:09'),
(5, 1, 'Engineer', 'Job Specification\r\nRequired Qualifications\r\n\r\nBE in Civil/Electronics/Computer/ Electrical or any related field.\r\nAbility to work proactively, and in efficient manner.\r\n**The employee will have to travel frequently to the project sites, inside and outside the valley, according to the demands of the project.\r\n\r\nApply Instructions\r\nKindly submit your CV and cover letter with expected salary on jobsatpspl@gmail.com\r\n\r\nWe may close the vacancy after we got the qualified candidates before the end of the deadline.', NULL, NULL, NULL, 'Bachelor', '1', 149, 1, 6, NULL, NULL, NULL, NULL, NULL, '2021-10-23 15:34:36', 4, 1, 3, 6, 1, 2, 8, 1, '2021/02/30', 22, 27, 'engineer', NULL, '2021-02-06 07:34:36', '2021-08-08 19:55:09'),
(6, 1, 'Account Officer', 'Job Description:\r\n\r\nCandidates should have a Master\'s degree in any field of Management /Economics Stream or semi-qualified CA.\r\nHaving working experience of more than 2 years in a similar position.\r\n\r\n• Knowledge of Nepal’s taxation regulations.\r\n• Handling Project Accounting.\r\n• Co-ordination with client, Joint Venture Partners and sub consultants.\r\n• Computation and Payment of Personal Income tax of Expats.\r\n• Verifying/Filling tax returns of PIT.\r\n• Liaison with Tax officials for collecting tax clearance, Audit Clearance, etc.\r\n• Reconciliation of Tax ledgers, Debtors, Creditors and Bank reconciliation.\r\n• Liaison with Internal and external auditors and completion of Audit.\r\n\r\nApply Instructions:\r\nKindly submit your CV and cover letter with the expected salary to jobsatpspl@gmail.com. We may close the vacancy after we got the qualified candidates before the end of the deadline.', NULL, NULL, NULL, 'Bachelors', '1', 128, 1, 7, NULL, NULL, NULL, NULL, NULL, '2021-10-07 16:04:15', 4, 1, 4, 6, 1, 2, 9, 1, '2021/02/06', 4, 27, 'account-officer', NULL, '2021-02-06 08:04:15', '2021-08-08 19:55:09'),
(7, 1, 'Overseer', 'Job Description:\r\n\r\n• Survey\r\n• Implementation of the survey data in coordination with Engineer\r\n• Report Preparation\r\n• Team Mobilization\r\n\r\nAny other as assigned by the Company\r\n\r\n\r\nJob Specification\r\nRequired qualification:\r\n\r\nOverseer in Civil, Electronic and Communication or any other related field\r\n\r\n**The employee will have to travel frequently to the project sites, inside and outside the valley, according to the demands of the project.\r\n\r\nApply Instructions\r\nKindly submit your CV and cover letter with expected salary on jobsatpspl@gmail.com\r\n\r\nWe may close the vacancy after we got the qualified candidates before the end of the deadline.', NULL, NULL, NULL, 'Bachelors', '2', 160, 1, 8, NULL, NULL, NULL, NULL, NULL, '2021-10-23 16:05:49', 4, 1, 3, 4, 1, 2, 8, 1, '2021/02/06', 9, 27, 'overseer', NULL, '2021-02-06 08:05:49', '2021-07-27 06:09:09'),
(8, 1, 'Sales Manager', 'Job Description:\r\nDelta Tech Pvt. Ltd. is seeking a Sales Manager who has good analytical as well as communication skills, has a good knowledge of sales, business development and also creative and innovative. You will be working on various internal products (Our products: Delta Sales App, Delta Sales CRM, Delta Retailer App & Delta Inventory) and external projects with different levels of complexities and challenges.\r\n\r\nRole and Responsibilities:\r\n\r\nYour responsibilities will include (but are not limited to) :\r\nMust have more than or equal to 3-years experience in business development, sales \r\nKnowledge of the IT industry\r\nClient relationship building and social skills\r\nGood knowledge of IT, web browsing, and MS Office.\r\nOutstanding written and verbal communication\r\nFluency in English and Nepali (spoken & written).\r\nExcellent time management skill\r\nBe independent and self-motivated\r\nAbility to work in a team with peers\r\nGood presentation and negotiation skills\r\nCapable in dealing with customers\r\nGood documentation and proposal writing skills\r\n\r\nWhat We Offer:\r\n\r\nDelta Tech is looking for candidates who are loyal and plan to make a long-term commitment. In return for your loyalty we offer:\r\n\r\nPositional Benefits:\r\n\r\nCompetitive/ Attractive Salary (No bar for right and deserving candidate)\r\nTimely performance appraisal and salary reviews\r\nPaid holiday & paid leaves\r\nFestive bonus\r\nGreat work culture\r\nFlexible timings against performance', NULL, NULL, NULL, 'Bachelor', '1', 139, 1, 9, 'Morang', NULL, NULL, NULL, NULL, '2021-10-24 13:03:08', 5, 5, 4, 3, 1, 3, 7, 1, '2021/03/07', 3, 2, 'sales-manager', NULL, '2021-02-23 05:03:08', '2021-08-08 19:55:09'),
(9, 1, 'Site Engineer', '- Managing Projects on Site.\r\n- Undertaking Surveys.\r\n- Setting out Sites.\r\n- Checking Technical Designs & Drawings to Ensure that they are followed Correctly.\r\n- Supervising Contracted Staff\r\n- Ensuring Project Packages Meet Agreed Specifications, Budgets and/or Timescales.\r\n- Liaising with Clients, Subcontractors and Other Professional Staff, Especially Quantity \r\n  Surveyors and the Overall Project Manager\r\n- Providing Technical Advice and Solving Problems on Site\r\n- Preparing Site Reports and Filling in Other Paperwork\r\n- Liaising with Quantity Surveyors about the Ordering and the Pricing of Materials', NULL, NULL, NULL, 'Bachelor\'s', '1', 61, 1, 10, NULL, NULL, NULL, 'Male', NULL, '2021-10-08 06:55:00', 7, 9, 4, 1, 1, 3, 21, 3, '2021/03/19', 2, 36, 'site-engineer', NULL, '2021-03-08 22:55:00', '2021-08-08 19:55:09'),
(10, 1, 'Senior Civil Engineer', '- Managing the planning and design stages of civil engineering projects.\r\n- Performing due diligence on the impact and feasibility of new construction sites.\r\n- Overseeing all project stages from preliminary layouts to final engineering designs.\r\n- Surveying new construction sites and assessing existing structures for upgrades.\r\n- Designing and recommending improvements to computer-aided design (CAD) software \r\n  drawings and schematics.\r\n- Performing cost estimations and preparing project budgets.\r\n- Preparing work schedules and allocating resources, as well as supervising junior \r\n  engineers, construction managers, workers, technicians, and contractors.\r\n- Ensuring that civil engineering projects are completed on time and within budget.\r\n- Inspecting completed projects for compliance with industry codes, specifications, and \r\n  safety standards.\r\n- Documenting processes and presenting project progress updates to senior managers \r\n  and clients.', NULL, NULL, NULL, 'Bachelor\'s', '1', 105, 1, 11, 'Dhumbarahi, Kathmandu', NULL, NULL, 'Male', NULL, '2021-10-08 07:02:52', 11, 1, 2, 4, 2, 3, 21, 1, '2021/03/29', 2, 36, 'senior-civil-engineer', NULL, '2021-03-08 23:02:52', '2021-08-08 19:55:09'),
(11, 1, 'Head Chef / Sous Chef', '-Chef should only apply if he is skilled to make New York style pizza.\r\n-At least 3 years experience in pizza mastery\r\n-NYC pizza making is very different and no way similar to Italian pizza that we find around in Kathmandu\r\n\r\n-Construct menus with new or existing culinary creations ensuring the variety and quality of the servings\r\n-Plan orders of equipment or ingredients according to identified shortages\r\n-Arrange for repairs when necessary\r\n-Oversee the work of subordinates\r\n-The person will be an individual who does not compromise on standards and maintains excellence at all times\r\n-A positive and learning culture is essential to the success of Purple Haze and the Head Chef/ Sous Chef will be an individual who not only naturally embraces this environment but also upholds and maintains this throughout their team\r\n-The individual we are seeking will already have a track record of success in their culinary career, will already know what success looks like and has the ambition to establish themselves as one of the leading chefs', 30000.00, 75000.00, 52500.00, 'Bachelor', '2', 187, 1, 12, NULL, NULL, NULL, NULL, NULL, '2021-10-15 09:44:56', 5, 1, 4, 3, 2, 3, 21, 1, '2021/03/16', 23, 38, 'head-chef-sous-chef', NULL, '2021-03-16 01:44:56', '2021-08-08 19:55:09'),
(12, 1, 'Dental Hygienist / Dental Assistant', 'Dental Assistant Job Responsibilities:\r\n\r\n-Supports dental care delivery by preparing treatment room, patient, instruments, and materials.\r\n-Performs procedures in compliance with the Dental Practice Act.\r\n-Prepares treatment room for patient by following prescribed procedures and protocols.\r\n-Readies patient for dental treatment by welcoming, comforting, seating, and draping patient.\r\n-Provides information to patients and employees by answering questions and requests.\r\n-Assembles instrumentation by sterilizing and delivering instruments to treatment area, positioning instruments for dentist’s access, suctioning, and passing instruments.\r\n-Provides materials by selecting, mixing, and placing materials on instruments and in the patient’s mouth.\r\n-Provides diagnostic information by exposing and developing radiographic studies and pouring, trimming, and polishing study casts.\r\n-Maintains patient appearance and ability to masticate by fabricating temporary restorations and cleaning and polishing removable appliances.\r\n-Helps dentist manage dental and medical emergencies by maintaining CPR certification, emergency drug and oxygen supply, and emergency telephone directory.\r\n\r\nDental Assistant Qualifications / Skills:\r\n\r\n-Dental health maintenance\r\n-Use of dental technology\r\n-Infection control\r\n-Patient services\r\n\r\nWorking Hours: 10am - 6pm\r\nplease drop your cv at darpandentalhome@gmail.com Or apply via Mero Offer Services', NULL, NULL, NULL, 'Bachelor', '1', 161, 1, 13, NULL, NULL, NULL, NULL, NULL, '2021-10-06 09:05:27', 3, 1, 4, 3, 1, 2, 12, 1, '2021/06/07', 15, 28, 'dental-hygienist-dental-assistant', NULL, '2021-04-07 01:05:27', '2021-08-08 19:55:09'),
(13, 1, 'Accountant Officer', 'Job Specification\r\nEducation Level: Bachelor\r\nExperience Required: More than 1 year\r\n\r\nOther Specification\r\n\r\nMinimum Bachelor or Equivalent in Business\r\nKnowledge of VAT, TDS, Income tax, taxation \r\nAdept computer skills and proficiency using programs like google docs/spreadsheet\r\nAdvanced math skills to keep accurate records and supervise the bookkeeping of an organization\r\nStrong verbal and written communication skills to interact regularly with clients regarding sensitive topics like billing and payments\r\nExcellent organization skills for maintaining clear, accurate and meticulous financial records for a company\r\nAttention to detail for ensuring the accuracy of a company\'s records and invoices\r\nMultitasking in order to successfully handle multiple accounts, invoices and payments at various stages of execution\r\nExcellent efficiency for handling any accounting issues quickly with minimal interference\r\n\r\nJob Description\r\nCreating and processing invoices\r\nCross-checking invoices with payments and expenses to ensure accuracy\r\nManaging a company\'s accounts payable and receivable\r\nSending bills and invoices to clients\r\nTracking organization expenses\r\nProcessing refunds\r\nWorking with collection agencies on overdue payments\r\nCommunicating with clients regarding billing and payments', NULL, NULL, NULL, 'BBS,BBA', '1', 170, 1, 14, 'Chakupat,Lalitpur', NULL, NULL, 'BOTH', '24-29', '2021-10-21 07:11:04', 4, 3, 4, 3, 1, 2, 9, 1, '2021/04/22', 3, 39, 'accountant-officer', NULL, '2021-04-21 23:11:04', '2021-08-08 19:55:09');

-- --------------------------------------------------------

--
-- Table structure for table `jobtypes`
--

CREATE TABLE `jobtypes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobtypes`
--

INSERT INTO `jobtypes` (`id`, `name`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'General', 1, 'general', '2021-02-06 05:35:10', '2021-02-06 05:35:10'),
(2, 'Featured', 1, 'featured', '2021-02-06 05:35:10', '2021-02-06 05:35:10'),
(3, 'Top', 1, 'top', '2021-02-06 05:35:10', '2021-02-06 05:35:10'),
(4, 'Hot', 1, 'hot', '2021-02-06 05:35:11', '2021-02-06 05:35:11'),
(5, 'Standard', 1, 'standard', '2021-02-06 05:35:12', '2021-02-06 05:35:12'),
(6, 'Platinum', 1, 'platinum', '2021-02-06 05:35:12', '2021-02-06 05:35:12'),
(7, 'Premium', 1, 'premium', '2021-02-06 05:35:12', '2021-02-06 05:35:12');

-- --------------------------------------------------------

--
-- Table structure for table `job_categories`
--

CREATE TABLE `job_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `font` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` bigint(20) NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_categories`
--

INSERT INTO `job_categories` (`id`, `name`, `font`, `color`, `order`, `desc`, `featured`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Graphics & Design', 'fas fa-paint-brush', '#86267e', 1, NULL, 0, 'graphics-design', 1, '2021-02-06 05:35:28', '2021-02-06 05:36:20'),
(2, 'Programming & Tech', 'fas fa-code', '#46b75d', 2, NULL, 1, 'programming-tech', 1, '2021-02-06 05:35:29', '2021-02-06 05:36:20'),
(3, 'Digital Marketing', 'fas fa-chart-bar', '#86267e', 3, NULL, 0, 'digital-marketing', 1, '2021-02-06 05:35:29', '2021-02-06 05:36:21'),
(4, 'Writing & Translation', 'fas fa-pencil-alt', '#39444c', 4, NULL, 0, 'writing-translation', 1, '2021-02-06 05:35:30', '2021-02-06 05:36:21'),
(5, 'Marketing & Customer Service', 'fas fa-bell', '#c25762', 5, NULL, 0, 'marketing-customer-service', 1, '2021-02-06 05:35:30', '2021-02-06 05:36:21'),
(6, 'Video & Animation', 'fas fa-video', '#44baff', 6, NULL, 0, 'video-animation', 1, '2021-02-06 05:35:30', '2021-02-06 05:36:22'),
(7, 'IT & Telecommunication', 'fas fa-globe', '#c29bc2', 7, NULL, 1, 'it-telecommunication', 1, '2021-02-06 05:35:31', '2021-02-06 05:36:22'),
(8, 'Agriculture & Engineering', 'fas fa-cog', '#86267e', 8, NULL, 0, 'agriculture-engineering', 1, '2021-02-06 05:35:31', '2021-02-06 05:36:22'),
(9, 'Banking, Management & Finanace', 'fas fa-chart-pie', '#86267e', 9, NULL, 1, 'banking-management-finanace', 1, '2021-02-06 05:35:31', '2021-02-06 05:36:22'),
(10, 'Protective & Security Services', 'fas fa-star', '#509fa6', 10, NULL, 0, 'protective-security-services', 1, '2021-02-06 05:35:31', '2021-02-06 05:36:22'),
(11, 'NGO/INGO & Social Work', 'fas fa-certificate', '#adb337', 11, NULL, 0, 'ngoingo-social-work', 1, '2021-02-06 05:35:32', '2021-02-06 05:36:23'),
(12, 'Healthcare/Pharma & Biotech', 'fas fa-plus', '#86267e', 12, NULL, 1, 'healthcarepharma-biotech', 1, '2021-02-06 05:35:32', '2021-02-06 05:36:24'),
(13, 'Teaching/Education', 'fas fa-bookmark', '#46b75d', 13, NULL, 0, 'teachingeducation', 1, '2021-02-06 05:35:32', '2021-02-06 05:36:24'),
(14, 'Secretarial/Frontoffice & Data Entry', 'fas fa-keyboard', '#86267e', 14, NULL, 0, 'secretarialfrontoffice-data-entry', 1, '2021-02-06 05:35:33', '2021-02-06 05:36:24'),
(15, 'Human Resource & ORG Development', 'fas fa-users', '#39444c', 15, NULL, 1, 'human-resource-org-development', 1, '2021-02-06 05:35:33', '2021-02-06 05:36:24'),
(16, 'Production & Quality Maintenance', 'fas fa-tint', '#c25762', 16, NULL, 0, 'production-quality-maintenance', 1, '2021-02-06 05:35:33', '2021-02-06 05:36:25'),
(17, 'Commercial/Logistics & Supply Chain', 'fas fa-ambulance', '#44baff', 17, NULL, 0, 'commerciallogistics-supply-chain', 1, '2021-02-06 05:35:34', '2021-02-06 05:36:25'),
(18, 'Research & Development', 'fas fa-cogs', '#86267e', 18, NULL, 0, 'research-development', 1, '2021-02-06 05:35:34', '2021-02-06 05:36:25'),
(19, 'Journalism & Media/Editor', 'fas fa-edit', '#86267e', 19, NULL, 1, 'journalism-mediaeditor', 1, '2021-02-06 05:35:34', '2021-02-06 05:36:26'),
(20, 'Sales & Public Relation', 'fas fa-chart-pie', '#509fa6', 20, NULL, 0, 'sales-public-relation', 1, '2021-02-06 05:35:34', '2021-02-06 05:36:26'),
(21, 'Others', 'fas fa-certificate', '#adb337', 21, NULL, 0, 'others', 1, '2021-02-06 05:35:35', '2021-02-06 05:36:26');

-- --------------------------------------------------------

--
-- Table structure for table `job_jattributevalue`
--

CREATE TABLE `job_jattributevalue` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `jattributevalue_id` bigint(20) UNSIGNED NOT NULL,
  `job_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job_skill`
--

CREATE TABLE `job_skill` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `skill_id` bigint(20) UNSIGNED NOT NULL,
  `job_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_skill`
--

INSERT INTO `job_skill` (`id`, `skill_id`, `job_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 2),
(5, 5, 2),
(6, 6, 2),
(7, 7, 3),
(8, 8, 3),
(9, 9, 3),
(10, 10, 4),
(11, 11, 4),
(12, 12, 4),
(13, 13, 4),
(14, 14, 5),
(15, 10, 6),
(16, 15, 6),
(17, 16, 6),
(18, 17, 6),
(19, 18, 6),
(20, 21, 8),
(21, 22, 8),
(22, 23, 8),
(23, 17, 8),
(24, 24, 8),
(25, 26, 11),
(26, 23, 11),
(27, 27, 11),
(30, 28, 12),
(31, 29, 12),
(32, 9, 12);

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Junior Level', 1, '2021-02-06 05:35:23', '2021-02-06 05:35:23'),
(2, 'Mid Level', 1, '2021-02-06 05:35:24', '2021-02-06 05:35:24'),
(3, 'Senior Level', 1, '2021-02-06 05:35:24', '2021-02-06 05:35:24');

-- --------------------------------------------------------

--
-- Table structure for table `mcategories`
--

CREATE TABLE `mcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `order` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mcategories`
--

INSERT INTO `mcategories` (`id`, `name`, `slug`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, 'Legal Policy', 'legal-policy', 1, 1, '2021-02-06 05:35:03', '2021-02-06 05:35:03'),
(2, 'Job News', 'job-news', 1, 2, '2021-02-06 05:35:03', '2021-02-06 05:35:03'),
(3, 'Tech News', 'tech-news', 1, 3, '2021-02-06 05:35:05', '2021-02-06 05:35:05');

-- --------------------------------------------------------

--
-- Table structure for table `mcontacts`
--

CREATE TABLE `mcontacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `memails`
--

CREATE TABLE `memails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `greet` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_greet` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `memails`
--

INSERT INTO `memails` (`id`, `from`, `to`, `greet`, `body`, `subject`, `end_greet`, `status`, `created_at`, `updated_at`) VALUES
(1, 'noreply@merooffer.com', 'doctest321@gmail.com', 'Greetings from Mero Offer', '<p><strong>Dear Sir/Madam,</strong></p>\r\n\r\n<p>Thank you for registering the product.<strong>&nbsp;https://merooffer.com</strong></p>\r\n\r\n<p><strong>What is merooffer ?</strong></p>\r\n\r\n<p>Merooffer.com is the trusted next generation of free online classifieds. It provides a simple solution to the complications involved in selling, buying, trading, discussing and meeting people near you.</p>\r\n\r\n<p>Merooffer provides you to announce job vacancy and find your employee you&#39;re looking for in and around your local community for free.&nbsp;</p>\r\n\r\n<p>You can search for freelancers and hire the best freelancers&nbsp;for any job, online.</p>\r\n\r\n<p>What are the features of Mero Offer?</p>\r\n\r\n<p>Buying and selling of the classifieds.<br />\r\nJob seeker can upload CV for free and make a Resume Profile in Mero Offer.&nbsp;Mero Offer provides advanced job search for the jobseeker.</p>\r\n\r\n<p>Classified ads are made easy to find sellers with google map and Advanced Search Engine placed at the top&nbsp;of the board. It also searches advanced keywords with specific input based on particular category.</p>\r\n\r\n<p><strong>How to post ad on Merooffer ?</strong></p>\r\n\r\n<ul>\r\n	<li>Signup using facebook or google services or simply register&nbsp;-&nbsp;<a href=\"https://merooffer.com/register\">https://merooffer.com/register</a></li>\r\n	<li>Start posting your classified --&nbsp;<a href=\"https://merooffer.com/ads-list\">https://merooffer.com/ads-list</a></li>\r\n	<li>Start posting your project --<a href=\"https://merooffer.com/project/create\">https://merooffer.com/project/create</a></li>\r\n</ul>\r\n\r\n<p>We hope to hear from you soon.<br />\r\nIf you have any queries please feel free to contact us.</p>\r\n\r\n<p>Thank You</p>\r\n\r\n<p><strong>Sending you good vibes.&nbsp;</strong></p>\r\n\r\n<p><strong>Best Regards,</strong></p>\r\n\r\n<p><strong><img alt=\"\" src=\"https://merooffer.com/storage/img/logo.png\" style=\"height:42px; width:161px\" /><br />\r\n&nbsp;Mero Offer Team</strong><br />\r\n<strong>Skyfall Technologies Pvt. Ltd.</strong></p>', 'Welcome to Mero Offer', 'Skyfall Technologies Pvt. Ltd.', 1, '2021-02-06 07:09:29', '2021-09-04 06:57:13'),
(2, 'noreply@merooffer.com', 'kapiltmagar50@gmail.com', 'Greetings from Mero Offer', '<p style=\"text-align:justify\"><strong>Dear </strong>Sir/Mam<strong>,</strong></p>\r\n\r\n<p style=\"text-align:justify\">We are sending this email to remind you&nbsp;to&nbsp;<strong>verify your mobile number.</strong> With respect to the information, you have&nbsp;signed up&nbsp;https://www.merooffer.com.&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Why to verify my mobile number ?</strong></p>\r\n\r\n<p style=\"text-align:justify\">You are not a activated user unless you verify your mobile number in Mero Offer. You will not be able to use any features of mero offer unless you verify your mobile number.&nbsp;&nbsp;As part of our efforts to protect publisher accounts and provide account-specific support, we require you to provide a valid mobile number. To make sure that your information is accurate and up-to-date, we may require that you verify&nbsp;your mobile number via SMS text or phone call.&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">How to verify ?</p>\r\n\r\n<p style=\"text-align:justify\">Please login your account.</p>\r\n\r\n<p style=\"text-align:justify\">Open the link&nbsp;<a href=\"https://merooffer.com/edit-profile\">https://merooffer.com/edit-profile</a></p>\r\n\r\n<p style=\"text-align:justify\">Enter your mobile number and send us the activation key and your mobile will be verified.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>What is merooffer ?</strong></p>\r\n\r\n<p style=\"text-align:justify\">Merooffer provides you to announce job vacancy, project and classifieds&nbsp;you&#39;re looking for in and around your local community for free.&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">Merooffer.com is the trusted next generation of free online classifieds. It provides a simple solution to the complications involved in selling, buying, trading, discussing and meeting people near you.</p>\r\n\r\n<p style=\"text-align:justify\">So what are the features ?</p>\r\n\r\n<p style=\"text-align:justify\">Buying and selling of the classifieds.<br />\r\nJob seeker can upload CV for free and make a Resume Profile in Mero Offer.&nbsp;Mero Offer provides advanced job search for the jobseeker.</p>\r\n\r\n<p style=\"text-align:justify\">You can post project and find freelancers online and get proposals.</p>\r\n\r\n<p style=\"text-align:justify\">Classified ads are made easy to find sellers with google map and Advanced Search Engine placed at the top&nbsp;of the board. It also searches advanced keywords with specific input based on particular category.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>How to post ad on Merooffer ?</strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Signup using facebook or google services or simply register&nbsp;-&nbsp;<a href=\"https://merooffer.com/register\">https://merooffer.com/register</a></li>\r\n	<li style=\"text-align:justify\">Start posting your classified --&nbsp;<a href=\"https://merooffer.com/ads-list\">https://merooffer.com/ads-list</a></li>\r\n	<li style=\"text-align:justify\">Start posting your job -&nbsp;<a href=\"https://merooffer.com/job-user/create\">https://merooffer.com/job-user/create</a></li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">We hope to hear from you soon.<br />\r\nIf you have any queries please feel free to contact us.</p>\r\n\r\n<p style=\"text-align:justify\">Thank You</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Sending you good vibes.&nbsp;</strong></p>\r\n\r\n<p style=\"text-align:justify\"><strong>Best Regards,</strong></p>\r\n\r\n<p style=\"text-align:justify\"><strong><img alt=\"\" src=\"https://merooffer.com/storage/img/logo.png\" style=\"height:42px; width:161px\" /><br />\r\nMero Offer Team&nbsp;</strong><br />\r\n<strong>Skyfall Technologies Pvt. Ltd.</strong></p>', 'Please verify your phone number', 'Skyfall Technologies Pvt. Ltd.', 1, '2021-02-06 07:10:19', '2021-09-04 07:00:27'),
(3, 'noreply@merooffer.com', 'kapiltmagar50@gmail.com', 'Greetings from Mero Offer', '<p style=\"text-align:justify\">Lock down is almost over in Nepal . Though, the number of positive cases of COVID 19 is increasing day by day.&nbsp;We care you, and your family. So, we are sending you self-quarantine guideline to ease your life and ensure your safety during self-quarantine.</p>\r\n\r\n<p style=\"text-align:justify\">Protect yourself and others around you by knowing the facts and taking appropriate precautions. Follow advice provided by your local public health agency.</p>\r\n\r\n<hr />\r\n<p style=\"text-align:justify\">To prevent the spread of COVID-19:-&nbsp;</p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Clean your hands often. Use soap and water, or an alcohol-based hand rub.</li>\r\n	<li style=\"text-align:justify\">Maintain a safe distance from anyone who is coughing or sneezing.</li>\r\n	<li style=\"text-align:justify\">Don&rsquo;t touch your eyes, nose or mouth.</li>\r\n	<li style=\"text-align:justify\">Cover your nose and mouth with your bent elbow or a tissue when you cough or sneeze.</li>\r\n	<li style=\"text-align:justify\">Stay home if you feel unwell.</li>\r\n	<li style=\"text-align:justify\">If you have a fever, cough and difficulty breathing, seek medical attention. Call in advance.</li>\r\n	<li style=\"text-align:justify\">Follow the directions of your local health authority.</li>\r\n	<li style=\"text-align:justify\">Avoiding unneeded visits to medical facilities allows healthcare systems to operate more effectively, therefore protecting you and others.</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">Regards,</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Mero Offer Team</strong></p>', 'Precaution of COVID 19', 'Skyfall Technologies Pvt. Ltd.', 1, '2021-02-06 07:11:08', '2021-09-04 07:00:11'),
(4, 'noreply@merooffer.com', 'coast_anv@hotmail.com', 'Greetings from Mero Offer', '<p><strong>Dear Sir/Madam,</strong></p>\r\n\r\n<p>Your mobille phone has been verified.</p>\r\n\r\n<p><strong>What is merooffer ?</strong></p>\r\n\r\n<p>Merooffer.com is the trusted next generation of free online classifieds. It provides a simple solution to the complications involved in selling, buying, trading, discussing and meeting people near you.</p>\r\n\r\n<p>You can search for freelancers and hire the best freelancers&nbsp;for any job, online.</p>\r\n\r\n<p>So what are the features ?</p>\r\n\r\n<p>Classified ads are made easy to find sellers with google map and Advanced Search Engine placed at the top&nbsp;of the board. It also searches advanced keywords with specific input based on particular category.</p>\r\n\r\n<p><strong>How to post ad on Merooffer ?</strong></p>\r\n\r\n<ul>\r\n	<li>Signup using facebook or google services or simply register&nbsp;-&nbsp;<a href=\"https://merooffer.com/register\">https://merooffer.com/register</a></li>\r\n	<li>Start posting your job -&nbsp;<a href=\"https://merooffer.com/job-user/create\">https://merooffer.com/job-user/create</a></li>\r\n	<li>Start posting your classified --&nbsp;<a href=\"https://merooffer.com/ads-list\">https://merooffer.com/ads-list</a></li>\r\n	<li>Start posting your project --<a href=\"https://merooffer.com/project/create\">https://merooffer.com/project/create</a></li>\r\n</ul>\r\n\r\n<p>We hope to hear from you soon.<br />\r\nIf you have any queries please feel free to contact us.</p>\r\n\r\n<p>Thank You</p>\r\n\r\n<p><strong>Sending you good vibes.&nbsp;</strong></p>\r\n\r\n<p><strong>Best Regards,</strong></p>\r\n\r\n<p><strong><img alt=\"\" src=\"https://merooffer.com/storage/img/logo.png\" style=\"height:42px; width:161px\" /><br />\r\nMero Offer Team</strong></p>', 'Thank you for verifiying your mobile number', 'Skyfall Technologies Pvt. Ltd.', 1, '2021-02-06 07:11:58', '2021-09-04 06:56:41'),
(6, 'barsa.dahal@merooffer.com', 'career@deltatechnepal.com', 'Greetings from Mero Offer', '<p><strong>Dear Sir/Madam,&nbsp;</strong></p>\r\n\r\n<p style=\"text-align:justify\">We have quite long conversation and we beg a pardon that we could not follow up your company in the past days.&nbsp;We are a startup company and we are new in the market. That may be the reason we could not provide&nbsp;resumes for your company in the past though your company has always supported us by posting in our website.</p>\r\n\r\n<p style=\"text-align:justify\">We will always try our best to find the best candidate for your company.&nbsp;It was just a followup phone call and we are glad you reacted about our problems that we take it as a complement and&nbsp;try to fix in the coming days.</p>\r\n\r\n<p style=\"text-align:justify\">Please let you know that Merooffer is just a local community which helps&nbsp;to find&nbsp;the best people near you.&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">We will try to send the resumes for Sales Manager as soon as possible.</p>\r\n\r\n<p><strong>Sending you good vibes.&nbsp;</strong></p>\r\n\r\n<p><strong>Best Regards,</strong></p>\r\n\r\n<p><strong><img alt=\"\" src=\"https://merooffer.com/storage/img/logo.png\" style=\"height:42px; width:161px\" /><br />\r\nBarsa Dahal (&nbsp;</strong>977-9813346784 )&nbsp;<br />\r\n<strong>Sales Associate | Client Relation</strong></p>', 'Resume of candidates for the Sales Manager', 'Skyfall Technologies Pvt. Ltd.', 1, '2021-02-23 01:35:20', '2021-02-23 01:40:53'),
(7, 'noreply@merooffer.com', 'hemrajhg7@gmail.com', 'Greetings from Mero Offer', '<p><img alt=\"\" src=\"https://merooffer.com/final.jpg\" /></p>\r\n\r\n<p>This offer is valid till 20 April, 2021 . The post should follow all the terms and conditions of Mero Offer Services.</p>\r\n\r\n<p>Regards,</p>\r\n\r\n<p>Mero Offer Team</p>', 'Limited Time Offer', 'Skyfall Technologies Pvt. Ltd.', 1, '2021-03-19 04:35:04', '2021-04-09 03:26:29'),
(8, 'noreply@merooffer.com', 'amit2010mahto@yahoo.com', 'Greetings from Mero Offer', '<p style=\"text-align:justify\"><strong>Dear </strong>Amit Kumar Mahato<strong>,</strong></p>\r\n\r\n<p style=\"text-align:justify\">Congratulations you have been awared with Rs.10&nbsp;topup successfully to the mobile number&nbsp; +977 - 9801622351.</p>\r\n\r\n<p style=\"text-align:justify\">Rs.10 has been awarded as per the offer which is valid till April 20,2021.<br />\r\nIf you have any queries please feel free to contact us.</p>\r\n\r\n<p style=\"text-align:justify\">Thank You</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Sending you good vibes.&nbsp;</strong></p>\r\n\r\n<p style=\"text-align:justify\"><strong>Best Regards,</strong></p>\r\n\r\n<p style=\"text-align:justify\"><strong><img alt=\"\" src=\"https://merooffer.com/storage/img/logo.png\" style=\"height:42px; width:161px\" /></strong></p>', 'Top Up Rs.20 inclusive of taxes Successful', 'Skyfall Technologies Pvt. Ltd.', 1, '2021-03-20 03:45:59', '2021-03-26 21:51:37');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from_user_id` bigint(20) NOT NULL,
  `to_user_id` bigint(20) NOT NULL,
  `type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_format` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `from_user_id`, `to_user_id`, `type`, `file_format`, `file_path`, `message`, `date`, `time`, `ip`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 28, 25, 'text', '', '', 'hello', '2021-03-10', '06:11 PM', '127.0.0.1', NULL, NULL, NULL),
(2, 28, 25, 'text', '', '', 'k cha', '2021-03-10', '06:12 PM', '127.0.0.1', NULL, NULL, NULL),
(3, 28, 25, 'text', '', '', 'how are you', '2021-03-10', '06:12 PM', '127.0.0.1', NULL, NULL, NULL),
(4, 28, 25, 'text', '', '', 'hello', '2021-03-10', '11:02 PM', '127.0.0.1', NULL, NULL, NULL),
(5, 28, 30, 'text', '', '', 'hello', '2021-03-11', '02:04 AM', '127.0.0.1', NULL, NULL, NULL),
(6, 28, 28, 'text', '', '', 'hii', '2021-03-12', '12:50 AM', '127.0.0.1', NULL, NULL, NULL),
(7, 28, 32, 'text', '', '', 'ok', '2021-03-12', '01:26 AM', '127.0.0.1', NULL, NULL, NULL),
(8, 28, 27, 'text', '', '', 'hello', '2021-03-19', '12:45 AM', '127.0.0.1', NULL, NULL, NULL),
(9, 28, 32, 'text', '', '', 'ok', '2021-03-23', '12:37 AM', '127.0.0.1', NULL, NULL, NULL),
(10, 28, 27, 'text', '', '', 'i like your ad', '2021-03-23', '12:38 AM', '127.0.0.1', NULL, NULL, NULL),
(11, 28, 20, 'text', '', '', 'how are you', '2021-03-26', '11:00 PM', '127.0.0.1', NULL, NULL, NULL),
(12, 50, 28, 'text', '', '', 'oijko', '2021-04-10', '04:07 PM', '127.0.0.1', NULL, NULL, NULL),
(13, 51, 42, 'text', '', '', 'hello', '2021-04-12', '03:10 PM', '127.0.0.1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_09_140619_create_assureds_table', 1),
(4, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(5, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(7, '2016_06_01_000004_create_oauth_clients_table', 1),
(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(9, '2017_07_14_041924_create_categories', 1),
(10, '2017_07_15_142933_create_cities_table', 1),
(11, '2017_07_17_143408_create_adtypes_table', 1),
(12, '2017_07_18_144519_create_days_table', 1),
(13, '2017_07_18_144654_create_pricelabels_table', 1),
(14, '2017_07_18_144655_create_pricetypes_table', 1),
(15, '2017_07_18_165353_create_category_pricelabel_table', 1),
(16, '2017_07_19_181216_create_products', 1),
(17, '2017_07_20_073514_create_bids_table', 1),
(18, '2017_08_08_165531_create_comments_table', 1),
(19, '2018_01_10_081859_create_experiences_table', 1),
(20, '2018_01_17_110558_create_tags_table', 1),
(21, '2018_01_19_045216_create_authors_table', 1),
(22, '2018_01_22_093849_create_industries_table', 1),
(23, '2018_01_24_093930_create_partners_table', 1),
(24, '2018_01_30_050258_create_mcategories_table', 1),
(25, '2018_02_19_121230_create_jobtypes_table', 1),
(26, '2018_02_19_161923_create_levels_table', 1),
(27, '2018_02_19_192818_create_job_categories_table', 1),
(28, '2018_02_20_154649_create_salarytypes_table', 1),
(29, '2018_02_20_162941_create_positiontypes_table', 1),
(30, '2018_02_20_192613_create_jobs_table', 1),
(31, '2018_02_25_082407_create_profiles_table', 1),
(32, '2018_02_25_104251_create_skills_table', 1),
(33, '2018_02_25_104449_create_skill_user_table', 1),
(34, '2018_02_25_112259_create_education_table', 1),
(35, '2018_02_25_121854_create_works_table', 1),
(36, '2018_03_01_010626_create_applicants_table', 1),
(37, '2018_03_01_011919_create_applicant_jobs_table', 1),
(38, '2018_03_30_144813_create_conditions_table', 1),
(39, '2018_05_29_162757_create_wishlists_table', 1),
(40, '2018_05_30_115417_create_ratings_table', 1),
(41, '2018_07_27_092819_create_messages_table', 1),
(42, '2018_08_28_093723_create_posts_table', 1),
(43, '2018_08_28_110327_create_post_tags_table', 1),
(44, '2018_09_04_105604_create_contacts_table', 1),
(45, '2018_11_08_105326_create_reviews_table', 1),
(46, '2018_11_16_043616_create_followers_table', 1),
(47, '2018_12_23_120000_create_shoppingcart_table', 1),
(48, '2019_05_16_084727_create_admins_table', 1),
(49, '2019_05_27_114658_create_settings_table', 1),
(50, '2019_06_06_033852_create_attributes_table', 1),
(51, '2019_07_24_083527_create_product_images_table', 1),
(52, '2019_08_19_000000_create_failed_jobs_table', 1),
(53, '2019_10_16_160233_create_reports_table', 1),
(54, '2019_10_29_132256_create_reportnames_table', 1),
(55, '2020_01_02_150530_create_product_categories_table', 1),
(56, '2020_01_07_093013_create_trainings_table', 1),
(57, '2020_01_11_062032_create_firmusers_table', 1),
(58, '2020_01_12_102706_create_attribute_categories_table', 1),
(59, '2020_01_12_103231_create_attributevalues_table', 1),
(60, '2020_01_12_103635_create_product_attributevalues_table', 1),
(61, '2020_03_28_122204_create_resumes_table', 1),
(62, '2020_05_04_080950_create_emails_table', 1),
(63, '2020_05_05_112659_create_mcontacts_table', 1),
(64, '2020_05_07_102735_create_amenities_table', 1),
(65, '2020_05_07_103047_create_product_amenities_table', 1),
(66, '2020_05_14_160933_create_memails_table', 1),
(67, '2020_06_09_051821_create_social_identities_table', 1),
(68, '2020_07_03_063926_create_fcategories_table', 1),
(69, '2020_07_03_064358_create_durations_table', 1),
(70, '2020_07_03_064750_create_fjobs_table', 1),
(71, '2020_07_07_180611_create_fapplicants_table', 1),
(72, '2020_07_08_084024_create_fapplicant_fjobs_table', 1),
(73, '2020_07_11_134650_create_fjob_skill_table', 1),
(74, '2020_07_12_034908_create_job_skill_table', 1),
(75, '2020_10_12_132046_create_couriers_table', 1),
(76, '2020_10_12_132538_create_order_statuses_table', 1),
(77, '2020_10_12_132613_create_addresses_table', 1),
(78, '2020_10_12_132953_create_orders_table', 1),
(79, '2020_10_12_133243_create_order_products_table', 1),
(80, '2020_12_21_071944_create_permission_tables', 1),
(81, '2021_01_06_043802_create_jattributes_table', 1),
(82, '2021_01_06_043844_create_jattributevalues_table', 1),
(83, '2021_01_06_044039_create_job_jattributevalue_table', 1),
(84, '2021_01_06_065903_create_jattribute_job_category_table', 1),
(85, '2021_01_15_125712_create_productattributes_table', 1),
(86, '2021_01_15_130415_create_attributevalue_productattribute_table', 1),
(87, '2021_03_14_181542_add_intervals_to_attributes_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('1f8465c8f334e1fec39ce79b8cca9bacce3946463572c793c8a9b07c83b0b7c17315c8b59edd4d8d', 28, 2, 'MyApp', '[]', 0, '2021-07-12 04:07:52', '2021-07-12 04:07:52', '2022-07-12 09:52:52'),
('227b21fec7146d4594a83428f2f65fa39187e04c1e69660737e84027ea724f574c4587c794eda2cb', 28, 2, 'MyApp', '[]', 0, '2021-07-12 01:23:49', '2021-07-12 01:23:49', '2022-07-12 07:08:49'),
('330ded46875aec5e76d2b96eefebd9a68755db5cf5bf194742ce3fa9d0f3c7eb882d9cc8895aa7c9', 28, 2, 'MyApp', '[]', 0, '2021-07-12 01:31:53', '2021-07-12 01:31:53', '2022-07-12 07:16:53'),
('36099caef5f30fbf10cfb408a1677b60492a34e22915032158144a29e16657f862486c44e5c909ad', 28, 2, 'MyApp', '[]', 0, '2021-07-12 04:53:22', '2021-07-12 04:53:22', '2022-07-12 10:38:22'),
('44c60a4b7aa9a19f3352309f744130869bd3c88893d8fcb7a6c38acfb6869dde8b009fce4c7a7a8d', 28, 2, 'authToken', '[]', 0, '2021-07-12 00:25:16', '2021-07-12 00:25:16', '2022-07-12 06:10:16'),
('4ceff19c3f63981690199e677c7579be69c9c803a13abadf68bac0092b309a1664079b9dd1a5e60e', 28, 2, 'MyAppToken', '[]', 0, '2021-07-29 23:59:53', '2021-07-29 23:59:53', '2022-07-30 07:59:53'),
('5407892fc1e2e150125ad743c5301753e829fffac0d28d9933ea26b691f8c316ff76eaf99af37e7a', 28, 2, 'MyApp', '[]', 0, '2021-07-12 00:36:50', '2021-07-12 00:36:50', '2022-07-12 06:21:50'),
('5d6f6bf47fe2813aa01b9fc0551e44cdd215bb887c89a15c87a8152b47a34e277948e6a283eee5f6', 28, 2, 'authToken', '[]', 0, '2021-07-12 00:33:20', '2021-07-12 00:33:20', '2022-07-12 06:18:20'),
('63ceb39edfeb59612f021caa116719b7cf76ff56dd1565a72379fe529545f12b9e3e57f852ec30ff', 28, 2, 'MyApp', '[]', 0, '2021-07-12 01:39:46', '2021-07-12 01:39:46', '2022-07-12 07:24:46'),
('6e872dd659e53cd4340b48549ff4085cfe843c2684c1d66b202cf6e2d802e99a402a6af623dc84f4', 28, 2, 'authToken', '[]', 0, '2021-07-12 00:24:19', '2021-07-12 00:24:19', '2022-07-12 06:09:19'),
('7ada8d9e13ec71f06660710d61a3d52389f3a165effe3c762c979d57b0bdbeb1f5893ca23b63bc39', 28, 2, 'MyApp', '[]', 0, '2021-07-12 01:23:20', '2021-07-12 01:23:20', '2022-07-12 07:08:20'),
('a679d683c764607d734f604d4d5a851f07e002099bf68ecbf1de662e3d2b78ff083a9806f7fc38ef', 28, 2, 'MyApp', '[]', 0, '2021-07-12 01:23:56', '2021-07-12 01:23:56', '2022-07-12 07:08:56'),
('a7b389017d4e92e3c492a1eb8628c5751a0b95c25317bcd2ec3fcc83e551835cebec5dc6851e8c20', 28, 2, 'MyApp', '[]', 0, '2021-07-12 03:19:00', '2021-07-12 03:19:00', '2022-07-12 09:04:00'),
('b015a9c90a03cb6ae0a3e0c543e6426c8ca242ea702c98caa90a92eb7ac2173bca4538375b19ba01', 28, 2, 'MyAppToken', '[]', 0, '2021-07-12 20:46:08', '2021-07-12 20:46:08', '2022-07-13 02:31:08'),
('b3305c2c519ce8bfdc839a9101f090b21d901bd623859517df2ab463cd1d4ebe1784576da0777370', 28, 2, 'MyAppToken', '[]', 0, '2021-07-12 23:25:35', '2021-07-12 23:25:35', '2022-07-13 07:25:35'),
('bcaa75cd7b0caacebc44f2fab347b735461fd375386d4de2b08f4ca893eec0e48a16b0d7f4756bdc', 28, 2, 'MyApp', '[]', 0, '2021-07-12 04:05:45', '2021-07-12 04:05:45', '2022-07-12 09:50:45'),
('ca90828188d1ad81ccfe0b6cfc95511824cd060eb86c3e8d0930758b343a08fa2f702219838bb96c', 28, 2, 'MyApp', '[]', 0, '2021-07-12 04:10:34', '2021-07-12 04:10:34', '2022-07-12 09:55:34'),
('cc1bb84e68acb0e77f36d8ffafdefece9c154ba789c36c8e6820bc594c6bea6c6e1e4bbdf9f77c21', 28, 2, 'MyApp', '[]', 0, '2021-07-12 04:09:11', '2021-07-12 04:09:11', '2022-07-12 09:54:11'),
('cff06591002c9cbb24bd2ba153d99358578b239a15a9c4445caa330d6b034bebf89aa56c9abacbf9', 28, 2, 'authToken', '[]', 0, '2021-07-12 00:32:55', '2021-07-12 00:32:55', '2022-07-12 06:17:55'),
('d13433a31097939012230fab9aab1f1f1aec1764a111828a853e81c36d8501dcbe01c27bfe4afb8e', 28, 2, 'MyApp', '[]', 0, '2021-07-12 00:34:06', '2021-07-12 00:34:06', '2022-07-12 06:19:06'),
('d2bbcd800ec32319563a03f4cf9209afcec47e7dfcf621c4a31780199c706e47ea4792ff373a75f6', 28, 2, 'MyAppToken', '[]', 0, '2021-07-12 23:01:06', '2021-07-12 23:01:06', '2022-07-13 07:01:06'),
('ff46a8f5fa955efb5f3e31d4dd899ad986ad09634ebff3a0a898fcd6ca30e7783d2f6bd4a77487ae', 28, 2, 'MyApp', '[]', 0, '2021-07-12 04:49:35', '2021-07-12 04:49:35', '2022-07-12 10:34:35');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'MyApp', 'ubd3cYCEnWeIobGsQhqjyfc2JxFDtA22DOEy0byB', NULL, 'http://localhost', 1, 0, 0, '2021-07-12 00:20:41', '2021-07-12 00:20:41'),
(2, NULL, 'Mero Offer Personal Access Client', 'BICIKgVjoXFwXrI4bZHSoHazr6Uz8X0xHcWDLQgM', NULL, 'http://localhost', 1, 0, 0, '2021-07-12 00:23:26', '2021-07-12 00:23:26'),
(3, NULL, 'Mero Offer Password Grant Client', 'wThqsBrszJAMaTypgmVO8znWoOgK6S9bg1MjVlOe', 'users', 'http://localhost', 0, 1, 0, '2021-07-12 00:23:26', '2021-07-12 00:23:26');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-07-12 00:20:41', '2021-07-12 00:20:41'),
(2, 2, '2021-07-12 00:23:26', '2021-07-12 00:23:26');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `courier_id` bigint(20) UNSIGNED NOT NULL,
  `courier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `address_id` bigint(20) UNSIGNED NOT NULL,
  `order_status_id` bigint(20) UNSIGNED NOT NULL,
  `payment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discounts` decimal(18,2) NOT NULL DEFAULT 0.00,
  `total_products` decimal(18,2) NOT NULL,
  `total_shipping` decimal(18,2) NOT NULL DEFAULT 0.00,
  `tax` decimal(18,2) NOT NULL DEFAULT 0.00,
  `total` decimal(18,2) NOT NULL,
  `total_paid` decimal(18,2) NOT NULL DEFAULT 0.00,
  `invoice` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tracking_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancel_reason` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `reference`, `courier_id`, `courier`, `user_id`, `address_id`, `order_status_id`, `payment`, `discounts`, `total_products`, `total_shipping`, `tax`, `total`, `total_paid`, `invoice`, `label_url`, `tracking_number`, `cancel_reason`, `note`, `created_at`, `updated_at`) VALUES
(57, '871912d9-da6b-4e75-a7d4-f7a7aa138189', 1, NULL, 28, 1, 1, 'Cash By hand', 0.00, 250.00, 100.00, 0.00, 350.00, 350.00, NULL, NULL, NULL, NULL, NULL, '2021-02-24 10:36:58', '2021-02-24 10:36:58'),
(58, 'c5d164b4-f9d2-4d15-bc4b-484b8ee2d5db', 1, NULL, 28, 1, 1, 'Cash By hand', 0.00, 1700250.00, 100.00, 0.00, 1700350.00, 1700350.00, NULL, NULL, NULL, NULL, NULL, '2021-02-24 10:39:29', '2021-02-24 10:39:29'),
(59, 'a1f98a90-dd59-4981-95b9-9d57c225d996', 1, NULL, 28, 1, 1, 'Cash By hand', 0.00, 1700250.00, 100.00, 0.00, 1700350.00, 1700350.00, NULL, NULL, NULL, NULL, NULL, '2021-02-24 10:42:31', '2021-03-27 07:43:55'),
(60, '6d822a72-bd77-44e5-b0ad-80b879d9e090', 1, NULL, 28, 1, 3, 'Cash By hand', 0.00, 400.00, 100.00, 0.00, 500.00, 500.00, NULL, NULL, NULL, NULL, NULL, '2021-03-06 01:12:25', '2021-07-07 07:48:34'),
(61, '62e2efec-75e3-4c05-a53f-386951e06fc8', 1, NULL, 28, 1, 3, 'Cash By hand', 0.00, 2500.00, 100.00, 0.00, 2600.00, 2600.00, NULL, NULL, NULL, NULL, NULL, '2021-04-15 09:54:39', '2021-04-18 09:46:48'),
(62, 'c28ff9e4-0aff-4d62-85c1-39b36c9394ff', 1, NULL, 28, 1, 3, 'Cash By hand', 0.00, 200.00, 100.00, 0.00, 300.00, 300.00, NULL, NULL, NULL, NULL, NULL, '2021-04-15 09:57:45', '2021-06-25 08:08:14'),
(63, '136f7324-6cbe-479e-bc89-44878d2c2c75', 1, NULL, 28, 1, 1, 'Cash By hand', 0.00, 450.00, 100.00, 0.00, 550.00, 550.00, NULL, NULL, NULL, NULL, NULL, '2021-07-10 00:47:53', '2021-07-10 00:47:53'),
(64, '5fcaa503-f382-4802-a92f-35670a3b1117', 1, NULL, 28, 1, 1, 'Cash By hand', 0.00, 800.00, 100.00, 0.00, 900.00, 900.00, NULL, NULL, NULL, NULL, NULL, '2021-07-11 00:59:02', '2021-07-11 00:59:02'),
(65, '0eadfa50-7ba3-4455-9823-4fb2f3b5a7b5', 1, NULL, 28, 1, 1, 'Cash By hand', 0.00, 800.00, 100.00, 0.00, 900.00, 900.00, NULL, NULL, NULL, NULL, NULL, '2021-07-11 01:00:39', '2021-07-11 01:00:39'),
(66, 'af0def59-13a4-4931-aac9-d10f29e98fe4', 1, NULL, 28, 1, 1, 'Cash By hand', 0.00, 800.00, 100.00, 0.00, 900.00, 900.00, NULL, NULL, NULL, NULL, NULL, '2021-07-11 01:03:27', '2021-07-11 01:03:27'),
(67, 'b7526f34-94e5-4911-8050-fa071f94acea', 1, NULL, 28, 1, 1, 'Cash By hand', 0.00, 800.00, 100.00, 0.00, 900.00, 900.00, NULL, NULL, NULL, NULL, NULL, '2021-07-11 01:07:22', '2021-07-11 01:07:22'),
(68, 'b9e6b234-9125-416d-b963-42455bb502f4', 1, NULL, 28, 1, 1, 'Cash By hand', 0.00, 800.00, 100.00, 0.00, 900.00, 900.00, NULL, NULL, NULL, NULL, NULL, '2021-07-11 01:09:46', '2021-07-11 01:09:46'),
(69, '12529822-3610-476c-84fc-130b9a9cd763', 1, NULL, 28, 1, 1, 'Cash By hand', 0.00, 800.00, 100.00, 0.00, 900.00, 900.00, NULL, NULL, NULL, NULL, NULL, '2021-07-11 01:11:44', '2021-07-11 01:11:44'),
(70, 'deea8870-a148-46ef-be85-83f93ea46e64', 1, NULL, 28, 1, 1, 'Cash By hand', 0.00, 800.00, 100.00, 0.00, 900.00, 900.00, NULL, NULL, NULL, NULL, NULL, '2021-07-11 01:16:15', '2021-07-11 01:16:15'),
(71, 'cfa6c114-062a-430b-9a42-a89c37d9c36c', 1, NULL, 28, 1, 1, 'Cash By hand', 0.00, 800.00, 100.00, 0.00, 900.00, 900.00, NULL, NULL, NULL, NULL, NULL, '2021-07-11 01:17:45', '2021-07-11 01:17:45'),
(72, 'cb3d4865-a3a7-44e6-acda-bc5213337941', 1, NULL, 28, 1, 1, 'Cash By hand', 0.00, 800.00, 100.00, 0.00, 900.00, 900.00, NULL, NULL, NULL, NULL, NULL, '2021-07-11 01:18:36', '2021-07-11 01:18:36'),
(73, '658d0802-086c-4637-bf45-52ab90f34544', 1, NULL, 28, 1, 1, 'Cash By hand', 0.00, 800.00, 100.00, 0.00, 900.00, 900.00, NULL, NULL, NULL, NULL, NULL, '2021-07-11 01:23:35', '2021-07-11 01:23:35');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `productattribute_id` bigint(20) UNSIGNED DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_sku` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_price` decimal(18,2) DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`id`, `order_id`, `product_id`, `productattribute_id`, `quantity`, `product_name`, `product_sku`, `product_description`, `product_price`, `note`, `created_at`, `updated_at`) VALUES
(73, 61, 38, NULL, 1, 'Rafting Package', NULL, 'Summer is here\r\n\r\nThe first river you paddle runs through the rest of your life.\r\n\r\nONE DAY TRISHULI/BHOTEKOSHI RAFTING @ Rs 2500/-\r\nPrice Includes:-\r\n✔️ Two-way transportation\r\n✔️ Professional Rafting Guide, Safety Drum for Camera & Mobiles\r\n✔️ Rafting 3+ Hour\r\n✔️ All necessary Safety Equipment like Life Jacket, Paddle, Helmet\r\n✔️ Buffet Veg or Non-Veg Lunch at the resort\r\n✔️ Photos & Videos\r\n------------------------------------------\r\nOvernight Stay and Rafting at Trishuli River Side @ Rs 3500/-\r\nPrice Includes:-\r\n✔️ Two-way Transportation\r\n✔️ Professional Rafting Guide, Safety Drum for Camera & Mobiles\r\n✔️ All necessary Safety Equipment like Life Jacket, Paddle, Helmet\r\n✔️ Rafting 3+ Hour\r\n✔️ 1 Veg or Non-Veg Lunch\r\n✔️ Photo & Videos\r\n✔️ Snacks\r\n✔️ Campfire\r\n✔️ Music\r\n✔️ Veg / Non-Veg Dinner\r\n✔️ Night Stay (Tent)\r\n✔️ Breakfast', 2500.00, NULL, NULL, NULL),
(74, 62, 14, 6, 1, 'Lunch Delight', NULL, 'Find here the best of lunch delight.  Love your hunger giving your hunger a new option.. Location : Baneshwor Kathmandu', 33.00, NULL, NULL, NULL),
(75, 63, 27, NULL, 1, 'Red Velvet 2 pound', NULL, 'All cakes are baked with hygiene and quality in the top of our priority at our own state-of-the-art baking facility and quality checked twice by our expert team before delivery. You can send cakes or gifts to your loved ones in Nepal or shop online for yourself in Nepal with Mero Offer, a pioneer in online shopping in Nepal.\r\n\r\nAfter ordering the product please call 9818238939 .', 450.00, NULL, NULL, NULL),
(76, 64, 14, 6, 1, 'Lunch Delight', NULL, 'Find here the best of lunch delight.  Love your hunger giving your hunger a new option.. Location : Baneshwor Kathmandu', 33.00, NULL, NULL, NULL),
(77, 65, 14, 6, 1, 'Lunch Delight', NULL, 'Find here the best of lunch delight.  Love your hunger giving your hunger a new option.. Location : Baneshwor Kathmandu', 33.00, NULL, NULL, NULL),
(78, 66, 14, 6, 1, 'Lunch Delight', NULL, 'Find here the best of lunch delight.  Love your hunger giving your hunger a new option.. Location : Baneshwor Kathmandu', 33.00, NULL, NULL, NULL),
(79, 67, 14, 6, 1, 'Lunch Delight', NULL, 'Find here the best of lunch delight.  Love your hunger giving your hunger a new option.. Location : Baneshwor Kathmandu', 33.00, NULL, NULL, NULL),
(80, 68, 14, 6, 1, 'Lunch Delight', NULL, 'Find here the best of lunch delight.  Love your hunger giving your hunger a new option.. Location : Baneshwor Kathmandu', 33.00, NULL, NULL, NULL),
(81, 69, 14, 6, 1, 'Lunch Delight', NULL, 'Find here the best of lunch delight.  Love your hunger giving your hunger a new option.. Location : Baneshwor Kathmandu', 33.00, NULL, NULL, NULL),
(82, 70, 14, 6, 1, 'Lunch Delight', NULL, 'Find here the best of lunch delight.  Love your hunger giving your hunger a new option.. Location : Baneshwor Kathmandu', 33.00, NULL, NULL, NULL),
(83, 71, 14, 6, 1, 'Lunch Delight', NULL, 'Find here the best of lunch delight.  Love your hunger giving your hunger a new option.. Location : Baneshwor Kathmandu', 33.00, NULL, NULL, NULL),
(84, 72, 14, 6, 1, 'Lunch Delight', NULL, 'Find here the best of lunch delight.  Love your hunger giving your hunger a new option.. Location : Baneshwor Kathmandu', 33.00, NULL, NULL, NULL),
(85, 73, 14, 6, 1, 'Lunch Delight', NULL, 'Find here the best of lunch delight.  Love your hunger giving your hunger a new option.. Location : Baneshwor Kathmandu', 33.00, NULL, NULL, NULL),
(86, 73, 14, 9, 1, 'Lunch Delight', NULL, 'Find here the best of lunch delight.  Love your hunger giving your hunger a new option.. Location : Baneshwor Kathmandu', 33.00, NULL, NULL, NULL),
(87, 73, 22, NULL, 1, 'Vanella 1 pound', NULL, 'All cakes are baked with hygiene and quality in the top of our priority at our own state-of-the-art baking facility and quality checked twice by our expert team before delivery. You can send cakes or gifts to your loved ones in Nepal or shop online for yourself in Nepal with Mero Offer, a pioneer in online shopping in Nepal.\r\n\r\nPlease call 9818238939 after ordering the cake.', 500.00, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `name`, `color`, `created_at`, `updated_at`) VALUES
(1, 'Ordered', 'green', '2021-02-06 05:34:47', '2021-02-06 05:34:47'),
(2, 'Delivery In Progress', 'yellow', '2021-03-27 07:27:27', '2021-03-27 07:27:48'),
(3, 'Cancelled', 'red', '2021-03-27 07:28:02', '2021-03-27 07:28:02');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positiontypes`
--

CREATE TABLE `positiontypes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `positiontypes`
--

INSERT INTO `positiontypes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Full Time', '2021-02-06 05:35:24', '2021-02-06 05:35:24'),
(2, 'Part Time', '2021-02-06 05:35:25', '2021-02-06 05:35:25'),
(3, 'Contract', '2021-02-06 05:35:26', '2021-02-06 05:35:26'),
(4, 'Temporary', '2021-02-06 05:35:26', '2021-02-06 05:35:26'),
(5, 'Internship', '2021-02-06 05:35:26', '2021-02-06 05:35:26'),
(6, 'Commission', '2021-02-06 05:35:27', '2021-02-06 05:35:27');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` int(11) NOT NULL DEFAULT 0,
  `summary` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_blog` tinyint(1) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `feature` tinyint(1) NOT NULL DEFAULT 0,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author_id` bigint(20) UNSIGNED NOT NULL,
  `mcategory_id` bigint(20) UNSIGNED NOT NULL,
  `order` bigint(20) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `name`, `views`, `summary`, `desc`, `keyword`, `body`, `is_blog`, `status`, `feature`, `image`, `date`, `author_id`, `mcategory_id`, `order`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Terms of Use', 0, NULL, 'Terms of Use', NULL, '<p style=\"text-align:justify\"><span style=\"color:#000000\"><strong>TERMS OF USE</strong>:&nbsp;Effective 12 May 2020, the Services (as defined below) will be provided to you by Skyfall Technologies Pvt. Ltd, the company currently providing the site to you.&nbsp;<br />\nMero Offer Terms of Use<br />\n&nbsp; &nbsp; <strong>1. Introduction</strong>. Welcome to <strong>www.merooffer.com</strong>. Thanks for stopping by. These <strong>Terms of Use</strong>, the <strong>Privacy Policy</strong>, and all policies posted on our site set out the terms on which we offer you access to and use of our site, services, applications and tools (collectively &ldquo;Services&rdquo;). You agree to comply with the full Terms of Use when accessing or using our Services.<br />\nThe Services are currently provided to you by <strong>Skyfall Technologies Pvt. Ltd</strong>.</span></p>\n\n<p style=\"text-align:justify\"><br />\n<span style=\"color:#000000\">&nbsp; &nbsp; <strong>2. Your Account</strong>. To access and use some of the Services, you may need to register with us and set up an account with your email address and a password (your &ldquo;Account&rdquo;). The email address you register with will be your email address, and you are solely responsible for maintaining the confidentiality of your password. You are solely responsible for all activities that happen under your Account.<br />\nIf you believe your Account has been compromised or misused, <strong>contact us</strong> immediately at Mero Offer Customer Support.</span></p>\n\n<p style=\"text-align:justify\"><br />\n<span style=\"color:#000000\">&nbsp; &nbsp; <strong>3. Using Mero Offer</strong>.&nbsp;To use the Services, you must be over 18 years old. You agree that you will only post in relation to goods or services in Nepal in the appropriate category or area and you agree that you will not do any of the following bad things:<br />\n&nbsp; &nbsp; &bull; violate any laws or the <strong>Posting Rules</strong>;<br />\n&nbsp; &nbsp; &bull; post any threatening, abusive, defamatory, obscene or indecent material;<br />\n&nbsp; &nbsp; &bull; be false or misleading;<br />\n&nbsp; &nbsp; &bull; infringe any third-party right;<br />\n&nbsp; &nbsp; &bull; distribute or send communications that contain spam, chain letters, or pyramid schemes;<br />\n&nbsp; &nbsp; &bull; distribute viruses or any other technologies that may harm Mero Offer, the Services or the interests or property of Mero Offer users;<br />\n&nbsp; &nbsp; &bull; impose an unreasonable load on our infrastructure or interfere with the proper working of the Services;<br />\n&nbsp; &nbsp; &bull; copy, modify, or distribute any other person&rsquo;s content without their consent;<br />\n&nbsp; &nbsp; &bull; use any robot spider, scraper or other automated means to access the Services and collect content for any purpose without our express written permission;<br />\n&nbsp; &nbsp; &bull; harvest or otherwise collect information about others, including email addresses, without their consent<br />\n&nbsp; &nbsp; &bull; bypass measures used to prevent or restrict access to the Services; and/or<br />\n&nbsp; &nbsp; &bull; post an ad with the intention to profit from a natural disaster, health or public safety concern, or tragic event.</span></p>\n\n<p style=\"text-align:justify\"><br />\n<span style=\"color:#000000\">&nbsp; &nbsp; <strong>4. Abusing Mero Offer Services</strong>. Mero Offer and the Mero Offer community work together to keep the Services working properly and the community safe. Please report problems, offensive content and policy breaches to us using the reporting system. You are solely responsible for all information that you give to Mero Offer and any consequences that may result from your posts. We can at our discretion refuse, delete or take down content that we think is inappropriate or breaching these Terms of Use. We also can at our discretion restrict a user&rsquo;s usage of the Services either temporarily or permanently, or refuse a user&rsquo;s registration. Without limiting other remedies, we may issue warnings, limit or terminate our Services, remove hosted content and take technical and legal steps to keep users off the Services if we think that they are creating problems or acting inconsistently with the letter or spirit of our policies. However, whether we take any of these steps, we don&rsquo;t accept any liability for monitoring the Services or for unauthorized or unlawful content on the Services or use of the Services by users. You also accept that Mero Offer is not under any obligation to monitor any data or content which is submitted to or available on the Services.&nbsp;</span></p>\n\n<p style=\"text-align:justify\"><br />\n<span style=\"color:#000000\">&nbsp; &nbsp;<strong> 5. Fees and Services</strong>.&nbsp;Using the Services is generally free. We may sometimes charge a fee for certain features or Services. If the feature you use incurs a fee, you will be able to review and accept that charge before purchase. Our fees are quoted in Nepali Rupees, and we may sometimes change them. We&rsquo;ll notify you of changes to our fees by posting the changes on the site. We may sometimes temporarily change our fees for testing purposes, promotional events or new features, these changes take effect from the time the price change is posted to the site. Our fees are non-refundable after the feature is supplied, and you are responsible for paying them when they are due. If you don&rsquo;t, we may limit your ability to use the Services.</span></p>\n\n<p style=\"text-align:justify\"><br />\n<span style=\"color:#000000\">&nbsp; &nbsp; <strong>6. Content</strong>.&nbsp;Mero Offer&rsquo;s Services contain content from us, you, and other users. Mero Offer is protected by copyright laws of Nepal. You agree not to copy, distribute the Services or modify content from the Services, our trademarks or copyrights without our express written consent. You may not disassemble or decompile, reverse engineer or otherwise attempt to discover any source code contained in the Services. Without limiting the foregoing, you agree not to reproduce, copy, sell, resell, or exploit for any purposes any aspect of the Services (other than your own content). When you give us content, including pictures, you grant us and represent that you have the right to grant us, a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, sub-licensable (through multiple tiers) right to exercise any and all copyright, publicity, trademarks, design, database and intellectual property rights to that content, in any media whether now known or to be discovered in the future, including third party sites and applications. You also waive all moral rights you have in the content to the fullest extent permitted by law. We reserve the right to remove content where we have grounds for suspecting the violation of these terms or the rights of any other party.</span></p>\n\n<p style=\"text-align:justify\"><br />\n<span style=\"color:#000000\">&nbsp; &nbsp; <strong>7. Intellectual Property Infringements .</strong>&nbsp;Do not post content that infringes the rights of third parties. This includes, but is not limited to, content that infringes on intellectual property rights such as copyright and trademark (e.g. offering counterfeit items for sale). We can remove content where we have grounds for suspecting the violation of these terms, our policies or of any party&rsquo;s rights.&nbsp;</span></p>\n\n<p style=\"text-align:justify\"><br />\n<span style=\"color:#000000\">&nbsp; &nbsp; <strong>8. Limitation of Liability.</strong> &nbsp;Nothing in these Terms of Use excludes, restricts or modifies any rights or statutory guarantees that you may have under applicable laws that cannot be excluded, restricted or modified, including any such rights or statutory guarantees under the Nepal Consumer Law and Act. To the extent that these Terms of Use are found to exclude, restrict or modify any such rights or statutory guarantees, those rights and/or statutory guarantees prevail to the extent of the inconsistency. &nbsp;Services are provided &ldquo;as is&rdquo; and &ldquo;as available&rdquo;. You agree not to hold us responsible for things other users post or do. As most of the content on the Services comes from other users, we do not guarantee the accuracy of postings or user communications or the quality, safety, or legality of what is offered. We also cannot guarantee continuous or secure access to our Services. While we will use reasonable efforts to maintain an uninterrupted service, we cannot guarantee this and, to the extent permitted by law, we do not give any promises or warranties (whether express or implied) about the availability of our Services or that the Services will be uninterrupted or error-free. Notification functionality in the Services may not occur in real time. That functionality is subject to delays beyond our control, including without limitation, delays or latency due to your physical location or your wireless data service provider&rsquo;s network. &nbsp;To the extent permitted by law, we are not liable for the posting of any unlawful, threatening, abusive, defamatory, obscene or indecent information, or material of any kind by a user of the Service which violates or infringes upon your rights, including without limitation any transmissions constituting or encouraging conduct that would constitute a criminal offense, give rise to civil liability or otherwise violate any applicable law. To the extent permitted by law, and without limiting any rights that you may have under the Nepal Law, Mero Offer&rsquo;s liability to you for any failure by Mero Offer to comply with any statutory guarantee under the Nepal&rsquo;s Law is limited to Mero Offer supplying the Services again or paying you the cost of having the Services supplied again.Mero Offer excludes any liability to you for any loss or damage suffered by you as a result of Mero Offer failing to comply with an applicable statutory guarantee under the Nepal&rsquo;s Law if you suffering such loss or damage was not reasonably foreseeable and was not directly caused by Mero Offer.<br />\n&nbsp;</span></p>\n\n<p style=\"text-align:justify\"><span style=\"color:#000000\">&nbsp; <strong>9. Indemnification.</strong>&nbsp;You will indemnify and hold harmless Mero Offer and our affiliates and our and their respective officers, directors, agents and employees (each an &ldquo;Indemnified Party&rdquo;), from any claim made by any third party, together with any amounts payable to the third party whether in settlement or as may otherwise be awarded, and reasonable legal costs incurred by any of the Indemnified Parties, arising from or relating to your use of the Services, any alleged violation by you of the applicable terms, and any alleged violation by you of any applicable law or regulation. We reserve the right, at our own expense, to assume the exclusive defence and control of any matter subject to indemnification by you, but doing so will not excuse your indemnity obligations. &nbsp;</span></p>\n\n<p style=\"text-align:justify\"><br />\n<span style=\"color:#000000\">&nbsp; &nbsp; <strong>10. Release.</strong>&nbsp;If you have a dispute with one or more Mero Offer users, you release us (and our officers, directors, agents, subsidiaries, joint ventures and employees) from any and all claims, demands and damages (actual and consequential) of every kind and nature, known or unknown, arising out of or in any way connected with such disputes. &nbsp;</span></p>\n\n<p style=\"text-align:justify\"><br />\n<span style=\"color:#000000\">&nbsp; &nbsp; <strong>11. Personal Information.</strong>&nbsp;By using the Services, you agree to the collection, transfer, storage and use of your personal information by us (the &ldquo;data controller&rdquo;) on servers located in the United States and in the European Union as further described in our privacy policy. &nbsp;</span></p>\n\n<p style=\"text-align:justify\"><br />\n<span style=\"color:#000000\">&nbsp; &nbsp; <strong>12. Severability.</strong>&nbsp;&nbsp;If a provision of these Terms of Use is illegal or unenforceable in any relevant jurisdiction, it may be severed for the purposes of that jurisdiction without affecting the enforceability of the other provisions of these Terms of Use. &nbsp;</span></p>\n\n<p style=\"text-align:justify\"><br />\n<span style=\"color:#000000\">&nbsp; &nbsp; <strong>13. General.</strong>&nbsp;These Terms of Use and the other policies posted on the Services set out the entire agreement between Mero Offer and you, overriding any prior agreements.</span><br />\n&nbsp;</p>', 0, 1, 1, 'posts/terms.jpg', '2020/05/11', 1, 1, 6, 'terms-of-use', '2021-02-06 05:39:56', '2021-03-19 07:35:59'),
(2, 'Posting Policy', 0, NULL, 'Posting Policy', NULL, '<p style=\"text-align:justify\"><strong>General Mero Offer Posting Policies</strong><br />\nAs a condition of use of <strong>Mero Offer</strong> you agree that you are at least 18 years of age.<br />\nAt Mero Offer we want to make sure that the site is as clean, friendly and usable as possible for everyone. Ads that fall outside the posting rules stated in our Help sections or our <strong>Terms of Use</strong> may be removed from the site.<br />\nYou are solely responsible for all information that you submit to Mero Offer and any consequences that may result from your post. We reserve the right at our discretion to refuse or delete content that we believe is inappropriate or breaching our <strong>Terms of Use</strong>. We also reserve the right at our discretion to restrict a user&#39;s usage of the site either temporarily or permanently, or refuse a user&#39;s registration.</p>\n\n<p style=\"text-align:justify\"><strong>General reasons for ads being deleted are:</strong><br />\n&nbsp; &nbsp; 1. Ad breaches Mero Offer <strong>Posting Policies</strong><br />\n&nbsp; &nbsp; 2. Breaches of Nepal&rsquo;s law. It is the responsibility of the advertiser before posting an ad on Mero Offer to ensure that content advertised adheres to Mero Offer <strong>posting policies</strong> as well as Nepal&rsquo;s applicable laws. As a condition of your use of Mero Offer specified under our <strong>Terms of Use</strong>, you agree that you will not violate any laws<br />\n&nbsp; &nbsp; 3. &nbsp;All the item being offered for sale is not allowed to be sold on Mero Offer.&nbsp;&nbsp;See <strong>&ldquo;What is not allowed in Mero Offer? &ldquo;</strong> page to see what can&#39;t be posted for sale on Mero Offer.<br />\n&nbsp; &nbsp; 4. Including information in your ad which Mero Offer believes is designed to manipulate search, including keyword stuffing and adding tags to your ad.&nbsp;<br />\n&nbsp; &nbsp; 5. The ad is a duplicate of another ad previously posted.<br />\n&nbsp; &nbsp; 6. Posted under wrong category (You must choose the single most relevant category for your ad)<br />\n&nbsp; &nbsp; 7. Ads posted in a language other than English or Nepali. We only accept ads in English or Nepali. It is better to post in english language for better search results.<br />\n&nbsp; &nbsp; 8. Ads posted from overseas or from behind a VPN unless the ad is posted in anticipation of you being in Nepali (eg. to find a place to live or a job while you are here). Mero Offer is for Nepali based individuals and businesses only.<br />\n&nbsp; &nbsp; 9. Ad contains external links: No external website links are allowed within your ad to other property / job / classified or auction sites<br />\n&nbsp; &nbsp; 10. Not descriptive enough: Ads that do not provide enough detail will be placed on hold or removed as this makes for a bad browsing experience<br />\n&nbsp; &nbsp; 11. Inappropriate language<br />\n&nbsp; &nbsp; 12. Inappropriate photo / image<br />\n&nbsp; &nbsp; 13. Discriminatory on race / religion / nationality / gender / etc<br />\n&nbsp; &nbsp; 14. Ads that report other fraudulent ads. Please report potentially fraudulent ads via the &quot;report ad&quot; option located within each ad or Contact Us with ad details (ad id, email address) and reasons why these ads should be reviewed<br />\n&nbsp; &nbsp; 15. Ads that are intended to profit off natural disasters, health or public safety concerns, or tragic events<br />\nMero Offer reserves the right to remove any ad that we feel is not relevant, or of value to the Mero Offer community, with or without notice to the ad poster.<br />\nThere are several ways that your ad may be found to be in breach of policy and removed from the site including:</p>\n\n<p style=\"text-align:justify\">&nbsp; &nbsp; <strong>&bull; Your ad has been reported to us.</strong><br />\nWhen this happens your ad may be temporarily suspended until we review it.<br />\nWe check reported ads as quickly as we can. If we conclude that the ad hasn&#39;t broken any <strong>Posting Policies</strong> or <strong>Terms of Use</strong> we will activate the ad again promptly. Check back after a few hours to see if this is the case.</p>\n\n<p style=\"text-align:justify\">&nbsp; &nbsp;<strong> &bull; Your ad has been removed by our moderation tools</strong><br />\nAds identified by our automated tools as inappropriate, that we then find do break our <strong>Posting Policies</strong> or <strong>Terms of Use</strong> will be removed from the site.<br />\nIn most cases we email you to let you know when we have had to remove your ad. These emails sometimes get queued and not delivered or sometimes directed to junk folders so please look out for Mero Offer emails. You may wish to add Mero Offer to your safe senders list if you have one.<br />\nIf you have checked out all of these possibilities and none of them apply to your ad then please let us know and we&#39;ll be happy to help you out.<br />\n&nbsp;</p>', 0, 1, 1, 'posts/posting_policy.jpg', '2020/05/11', 1, 1, 1, 'posting-policy', '2021-02-06 05:39:57', '2021-03-19 07:09:09'),
(3, 'Privacy Policy', 0, NULL, 'Privacy Policy', NULL, '<p style=\"text-align:justify\"><strong>Privacy Policy</strong></p>\n\n<p style=\"text-align:justify\"><strong>Skyfall Technologies Pvt. Ltd.</strong> is the operator of merooffer.com (&ldquo;the Site&rdquo;). This Privacy Notice describes:<br />\n&nbsp; &nbsp; &bull; the personal information we collect and how we use that information<br />\n&nbsp; &nbsp; &bull; when we might disclose your personal information; and<br />\n&nbsp; &nbsp; &bull; how we keep and protect your personal information.</p>\n\n<p style=\"text-align:justify\">The Privacy Notice applies to this Site and to any applications, services or tools (collectively &ldquo;Services&rdquo;) where this Privacy Notice is referenced. By using our Services and/or registering for an account, you are accepting the terms of this Privacy Notice and our Terms of Use</p>\n\n<p style=\"text-align:justify\"><strong>We collect information you give us including:</strong></p>\n\n<p style=\"text-align:justify\">&nbsp; &nbsp; <strong>&bull; When you register for an account:</strong> Information such as your name, addresses, telephone numbers, email addresses or user ID (where applicable) when you register for an account with us<br />\n&nbsp; &nbsp; <strong>&bull; When we verify you or your account:</strong> we may collect and process information (as permitted by law) to authenticate you or your account, or to verify the information that you provided to us<br />\n&nbsp; &nbsp;&bull;<strong> When you transact on or use our Services:</strong> such as when you post an ad, reply to an ad, communicate with us or other users, information you provide for the Services that you use or during a transaction or other transaction-based content. We may also collect your financial information (such as credit card or bank account numbers) if you buy a feature from us or are required to pay fees to us<br />\n&nbsp; &nbsp; <strong>&bull; When you engage with our community:</strong> such as when you submit a web form or participate in community discussions or chats<br />\n&nbsp; &nbsp; <strong>&bull; When you interact with your account:</strong> such as updating or adding information to your account, adding items to alerts lists and saving searches. Sometimes you may also give us your age, gender, interests and favourites<br />\n&nbsp; &nbsp;<strong> &bull; When you contact us:</strong> such as through a web form, chat or dispute resolution or when we otherwise communicate with each other. We may also record our calls with you (if we have your consent to do so)</p>\n\n<p style=\"text-align:justify\"><br />\nYour <strong>resume</strong> if you choose to submit it to advertisers on our site for consideration We collect information automatically including:<br />\n&nbsp; &nbsp; &bull; Information from the devices you use when interacting with us or our Services such as device ID or unique user ID, device type, ID for advertising and unique device token<br />\n&nbsp; &nbsp; &bull; Information about your location such as geo-location<br />\n&nbsp; &nbsp; &bull; Computer and connection information such as statistics on your page views, traffic to and from the sites, referral URL, ad data, your IP address, your browsing history and your web log information</p>\n\n<p style=\"text-align:justify\"><strong>We collect information using cookies and similar technologies including:</strong><br />\n&nbsp; &nbsp; &bull; Information about the pages you view, the links you click and other actions you take on our Services, or within our advertising or email content.&nbsp;</p>\n\n<p style=\"text-align:justify\"><strong>We use your personal information to provide, improve and personalise our Services. </strong></p>\n\n<p style=\"text-align:justify\">Your personal information allows us to:<br />\n&nbsp; &nbsp; &bull; Provide you with access to and use of our Services as well as access to your history, internal messages and other features we may provide<br />\n&nbsp; &nbsp; &bull; Offer you site content that includes items and services that you may like<br />\n&nbsp; &nbsp; &bull; Provide you with credit offers and opportunities on behalf of other members of our corporate family and their financial institution partners. However, we don&rsquo;t share financial information without your explicit consent<br />\n&nbsp; &nbsp; &bull; Customise, measure and improve our Services<br />\n&nbsp; &nbsp; &bull; Provide other services requested by you as described when we collect the information<br />\n&nbsp; &nbsp; &bull; To provide you with location-based services (such as advertising, search results and other personalised content)</p>\n\n<p style=\"text-align:justify\"><strong>Cookie Policy:</strong></p>\n\n<p style=\"text-align:justify\">When you visit or interact with our sites, services, applications, tools or messaging, we or our authorized service providers may use cookies, web beacons, and other similar technologies for storing information to help provide you with a better, faster, and safer experience and for advertising purposes.<br />\nThis page is designed to help you understand more about these technologies and our use of them on our sites and in our services, applications, and tools. Below is a summary of a few key things you should know about our use of such technologies.&nbsp;<br />\nOur cookies and similar technologies have different functions. They are either necessary for the functioning of our services, help us improve our performance, give you extra functionalities, or help us to serve you relevant and targeted ads. We use cookies and similar technologies that only remain on your device for as long as you keep your browser active (session) and cookies and similar technologies that remain on your device for a longer period (persistent).You are free to block, delete, or disable these technologies if your device permits so. You can manage your cookies and your cookie preferences in your browser or device settings.</p>\n\n<p style=\"text-align:justify\"><strong>Skyfall Technologies Pvt. Ltd.</strong>, is responsible for the collection, use, disclosure, retention and protection of your personal information and third party services you use under the Nepal&rsquo;s National laws. We acknowledge that we do not sell or rent your personal information which is against the Nepal&rsquo;s Law.</p>', 0, 1, 1, 'posts/privacy-policy.jpg', '2020/05/11', 1, 1, 2, 'privacy-policy', '2021-02-06 05:39:57', '2021-03-19 07:09:09'),
(4, 'Safety Tips', 0, NULL, 'Safety Tips', NULL, '<p><strong>Mero Offer&rsquo;s Safety Guidelines</strong><br />\nAt&nbsp;Mero Offer&nbsp;we want to make sure all our users have a safe, successful and hassle-free experience. Whether you&rsquo;re buying or selling, make sure you follow our safety guidelines.</p>\n\n<p><br />\n1.&nbsp;When buying or selling, you should always meet in-person to see the item and exchange money. If possible, take a friend or family member with you, or make sure there is someone else at your home or workplace.</p>\n\n<p><br />\n2.&nbsp;Never send money to anyone you don&rsquo;t know. Mero Offer does not allow users to transfer money via the website, and advocates face-to-face transactions. This includes mailing a cheque or using payment services like E-sewa, Western Union or Money Gram to pay for items found on Mero offer.</p>\n\n<p><br />\n3.&nbsp;If you receive an SMS message requesting you reply via email please ignore it! This is most likely an attempted fraud. You should only trade with local buyers you can meet in person.</p>\n\n<p><br />\n4.Mero Offer doesn&rsquo;t offer any sort of buyer protection or payment programs. Any emails you receive that talk about such systems should be ignored, even if they may have the Mero Offer logo. If you receive any emails promoting these services, please report it to us.</p>\n\n<p><br />\n5.&nbsp;Always use common sense &ndash; If it looks too good to be true it probably is!</p>\n\n<p><br />\nIf you see any ad you are concerned about please report it to us by using the &lsquo;Report Ad&rsquo; function, which is clearly displayed on every ad published on the website.<br />\n&nbsp;</p>', 0, 1, 1, 'posts/safety_tips.jpg', '2020/05/11', 1, 1, 3, 'safety-tips', '2021-02-06 05:39:58', '2021-03-19 07:35:59'),
(5, 'What is not allowed in Mero Offer', 0, NULL, 'Not allowed in Mero Offer', NULL, '<p><strong>The list below details what can&#39;t be posted on Mero Offer in either offering or wanted ads:</strong></p>\n\n<p>&nbsp; &nbsp; 1. Material that infringes copyright, including but not limited to software or other digital goods which you are not authorized to sell<br />\n&nbsp; &nbsp; 2. Escort services that offer or indicate sexual services<br />\n&nbsp; &nbsp; 3. Alcohol, E-cigarette and Tobacco Products<br />\n&nbsp; &nbsp; 4. Identity Documents, Personal Financial Records &amp; Personal Information<br />\n&nbsp; &nbsp; 5. Blood, Bodily Fluids and Body Parts<br />\n&nbsp; &nbsp; 6. Burglary Tools<br />\n&nbsp; &nbsp; 7. Counterfeit Products, replicas or knock-off brand name goods<br />\n&nbsp; &nbsp; 8. Government and Transit Badges, Uniforms, IDs, Documents and Licenses<br />\n&nbsp; &nbsp; 9. Illegal Goods/Embargoed Goods/Contraband goods<br />\n&nbsp; &nbsp; 10. Endangered or protected species, or any part of any endangered or protected species<br />\n&nbsp; &nbsp; 11. Fireworks, Destructive Devices and Explosives<br />\n&nbsp; &nbsp; 12. Hazardous Materials including but not limited to radioactive, toxic and explosive materials<br />\n&nbsp; &nbsp; 13. Illegal Drugs, controlled substances, substances and items used to manufacture controlled substances and drugs, &amp; drug paraphernalia<br />\n&nbsp; &nbsp; 14. Illegal items and services<br />\n&nbsp; &nbsp; 15. Illegal telecommunication and electronics equipment such as access cards, password sniffers, radar scanners, traffic signal control devices or cable descrambler<br />\n&nbsp; &nbsp; 16. Items issued to any Armed Force that have not been disposed of in accordance with that country&#39;s demilitarization policies<br />\n&nbsp; &nbsp; 17. Items which encourage or facilitate illegal activity<br />\n&nbsp; &nbsp; 18. Lottery Tickets, Sweepstakes Entries and Slot Machines<br />\n&nbsp; &nbsp; 19. Material that is obscene, pornographic, adult in nature, or harmful to minors<br />\n&nbsp; &nbsp; 20. New merchandise or services from network marketing companies, work-from-home, independent franchisees or distributors, or similar representatives<br />\n&nbsp; &nbsp; 21. Personal information or mailing lists. We do not accept the sale of bulk email, Internet Protocol (IP), Instant Messenger (IM), or mailing lists that contain names, addresses, phone numbers, or other personal identifying information. Any tools or software designed predominantly to send unsolicited commercial messages (UCE or &quot;spam&quot;) are also not permitted.<br />\n&nbsp; &nbsp; 22. Selling or offering services for supplements/medicine general or pharmaceutical<br />\n&nbsp; &nbsp; 23. Nonprescription drugs, drugs that make false or misleading treatment claims, or treatment claims that require therapeutic goods administration Nepal&rsquo;s Government approval<br />\n&nbsp; &nbsp; 24. Commercial tanning units or commercial tanning services wanted or offered<br />\n&nbsp; &nbsp; 25. Offensive material- examples include ethnically or racially offensive material<br />\n&nbsp; &nbsp; 26. Pesticides or hazardous materials<br />\n&nbsp; &nbsp; 27. Pictures or images that contain nudity<br />\n&nbsp; &nbsp; 28. Plants and insects that are restricted or regulated<br />\n&nbsp; &nbsp; 29. Prescription drugs and devices<br />\n&nbsp; &nbsp; 30. Prostitution or ads that offer sex, sexual favors or sexual actions in exchange for anything<br />\n&nbsp; &nbsp; 31. Recalled items, banned products or products&nbsp;<br />\n&nbsp; &nbsp; 32. Stocks and other securities including Bitcoins and related mining equipment<br />\n&nbsp; &nbsp; 33. Stolen property<br />\n&nbsp; &nbsp; 34. Tobacco products and related items, including e-cigarettes<br />\n&nbsp; &nbsp; 35. Used cosmetics<br />\n&nbsp; &nbsp; 36. Used or rebuilt batteries or batteries containing mercury<br />\n&nbsp; &nbsp; 37. We do not accept ads selling body parts/bodily fluids, adoption or surrogacy anywhere on the site<br />\n&nbsp; &nbsp; 38. Weapons and related items (including, but not limited to firearms, firearm accessories, parts and magazines, ammunition, paintball guns, gel blaster guns, BB and pellet guns, spearguns, tear gas, tasers, stun guns, switchblade knives, martial arts weapons, archery and/or bow and arrow equipment)<br />\n&nbsp; &nbsp; 39. Ivory, rhino horn or any animal parts or&nbsp;hunting trophies&nbsp;<br />\n&nbsp; &nbsp; 40. Votes in elections administered&nbsp;<br />\n&nbsp; &nbsp; 41. Census or other survey papers issued by the Nepal&rsquo;s Bureau of Statistic<br />\n&nbsp; &nbsp; 42. Education certificates(fake) for High school diplomas, university, medals, etc.<br />\n&nbsp; &nbsp; 43. All ads for COVID-19 associated items, including:<br />\n&nbsp; &nbsp; &nbsp; &nbsp; a. Surgical and respiratory masks<br />\n&nbsp; &nbsp; &nbsp; &nbsp; b. Hand sanitisers and gels<br />\n&nbsp; &nbsp; &nbsp; &nbsp; c. Toilet paper<br />\n&nbsp; &nbsp; &nbsp; &nbsp; d. Disinfectant wipes&nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; e. Medical protection disposable gowns and gloves</p>\n\n<p>It is the responsibility of the advertiser before posting an ad on Mero Offer that content advertised adheres to Mero Offer Posting Policies as well as applicable laws. As a condition of your use of Mero Offer specified under our Terms of Use, you agree that you will not violate any laws of Nepal&rsquo;s Government.</p>', 0, 1, 1, 'posts/not_allowed.jpg', '2020/05/11', 1, 1, 5, 'what-is-not-allowed-in-mero-offer', '2021-02-06 05:39:58', '2021-03-19 07:35:59');

-- --------------------------------------------------------

--
-- Table structure for table `post_tags`
--

CREATE TABLE `post_tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pricelabels`
--

CREATE TABLE `pricelabels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pricelabels`
--

INSERT INTO `pricelabels` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Per Month', 1, '2021-02-06 05:36:38', '2021-02-06 05:36:38'),
(2, 'Onwards', 1, '2021-02-06 05:36:42', '2021-02-06 05:36:42'),
(3, 'Per Sq. Feet', 1, '2021-02-06 05:36:45', '2021-02-06 05:36:45'),
(4, 'Per Ropani', 1, '2021-02-06 05:36:47', '2021-02-06 05:36:47'),
(5, 'Per Paisa', 1, '2021-02-06 05:36:51', '2021-02-06 05:36:51'),
(6, 'Per Aana', 1, '2021-02-06 05:36:53', '2021-02-06 05:36:53'),
(7, 'Per Dhur', 1, '2021-02-06 05:36:56', '2021-02-06 05:36:56'),
(8, 'Per Kattha', 1, '2021-02-06 05:36:58', '2021-02-06 05:36:58'),
(9, 'Per Bigha', 1, '2021-02-06 05:37:00', '2021-02-06 05:37:00'),
(10, 'Per Daam', 1, '2021-02-06 05:37:01', '2021-02-06 05:37:01'),
(11, 'Per Acres', 1, '2021-02-06 05:37:03', '2021-02-06 05:37:03'),
(12, 'Per Haat', 1, '2021-02-06 05:37:05', '2021-02-06 05:37:05'),
(13, 'Per Pound', 1, '2021-02-06 05:37:06', '2021-02-06 05:37:06');

-- --------------------------------------------------------

--
-- Table structure for table `pricetypes`
--

CREATE TABLE `pricetypes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pricetypes`
--

INSERT INTO `pricetypes` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Price Negotiable', 1, '2021-02-06 05:35:44', '2021-02-06 05:35:44'),
(2, 'Fixed Price', 1, '2021-02-06 05:35:44', '2021-02-06 05:35:44'),
(3, 'Price On Call', 1, '2021-02-06 05:35:45', '2021-02-06 05:35:45'),
(4, 'Price On Email', 1, '2021-02-06 05:35:45', '2021-02-06 05:35:45');

-- --------------------------------------------------------

--
-- Table structure for table `productattributes`
--

CREATE TABLE `productattributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(18,2) DEFAULT NULL,
  `sale_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default` tinyint(4) NOT NULL DEFAULT 0,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productattributes`
--

INSERT INTO `productattributes` (`id`, `quantity`, `price`, `sale_price`, `default`, `product_id`, `created_at`, `updated_at`) VALUES
(3, 100, 200.00, NULL, 0, 14, '2021-02-23 06:42:11', '2021-02-23 06:42:11'),
(5, 100, 250.00, NULL, 0, 14, '2021-02-23 06:45:03', '2021-02-23 06:45:03'),
(6, 100, 200.00, NULL, 0, 14, '2021-02-23 06:45:25', '2021-02-23 06:45:25'),
(7, 100, 250.00, NULL, 0, 14, '2021-02-23 06:45:39', '2021-02-23 06:45:39'),
(8, 100, 200.00, NULL, 0, 14, '2021-02-23 06:46:09', '2021-02-23 06:46:09'),
(9, 100, 100.00, NULL, 0, 14, '2021-02-23 06:46:32', '2021-02-23 06:46:32'),
(10, 100, 120.00, NULL, 0, 14, '2021-02-23 06:47:00', '2021-02-23 06:47:00'),
(11, 100, 150.00, NULL, 0, 14, '2021-02-23 06:47:18', '2021-02-23 06:47:18'),
(12, 100, 200.00, NULL, 0, 14, '2021-02-23 06:48:28', '2021-02-23 06:48:28'),
(13, 100, 200.00, NULL, 0, 14, '2021-02-23 06:48:47', '2021-02-23 06:48:47');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sold` tinyint(1) NOT NULL DEFAULT 0,
  `order` bigint(20) NOT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `views` bigint(20) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `bid` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `weight` decimal(8,2) DEFAULT 0.00,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cell` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(18,2) DEFAULT NULL,
  `rgr_price` decimal(18,2) DEFAULT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expired_at` datetime DEFAULT NULL,
  `condition_id` bigint(20) UNSIGNED DEFAULT NULL,
  `pricelabel_id` bigint(20) UNSIGNED DEFAULT NULL,
  `assured_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `pricetype_id` bigint(20) UNSIGNED NOT NULL,
  `day_id` bigint(20) UNSIGNED NOT NULL,
  `adtype_id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sold`, `order`, `featured`, `views`, `status`, `bid`, `slug`, `quantity`, `weight`, `address`, `latitude`, `longitude`, `cell`, `url`, `name`, `price`, `rgr_price`, `desc`, `expired_at`, `condition_id`, `pricelabel_id`, `assured_id`, `pricetype_id`, `day_id`, `adtype_id`, `city_id`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 0, 2, 0, 88, 1, 1, 'available-one-room-for-rent', 1, 0.00, 'Machhapokhari Chowk, Ring Road, Kathmandu, Nepal', '27.7353111', '85.3058395', '9813976454', NULL, 'Available One room for rent', 4500.00, NULL, '14\'x13\' size\'s room available for rent in Naya pool shesmati ( Saraswoti tole)\r\nWater 24 hours, parking available for bike WiFi too\r\nGround floor Rent 4500\r\nCall for more information at 9813976454\r\nplease don\'t comment... direct call.. if you r serious...', '2021-10-08 16:41:21', NULL, 1, 1, 1, 2, 4, 1, 8, '2021-02-06 08:41:21', '2021-02-06 08:41:21'),
(3, 0, 3, 1, 270, 1, 1, 'allo-fabric-for-mens-coat', 1, 0.00, 'New Baneshwor, Kathmandu, Nepal', '27.6915196', '85.3420486', '9808223910', NULL, 'Allo Fabric For Men\'s Coat', 3600.00, NULL, 'Online shop of variety of Allo Products such as coats, caps, waist coats, etc.', '2021-10-08 16:47:25', 1, NULL, 1, 2, 2, 1, 1, 20, '2021-02-06 08:47:25', '2021-02-06 08:47:25'),
(4, 0, 4, 1, 227, 1, 1, 'couples-t-shirt-love-heart-o-neck-casual-t-shirt-tops-for-couple-lovers', 10, 0.00, 'Machhapokhari Chowk, Ring Road, Kathmandu, Nepal', '27.7353111', '85.3058395', '9849824945', NULL, 'Couples T-Shirt Love Heart O-Neck Casual T Shirt Tops For Couple Lovers', 850.00, NULL, 'Embroidery Love Heart Couples T-shirt Plus Size Casual T shirt Tops\r\nMaterial: cotton blends\r\n\r\nIf you are interested send me an inbox 📩\r\nor, please contact +977-9849824952 | 9880695377 | 9810043268\r\n📩 mcare967@gmail.com\r\n🚚Home Delivery available\r\nNo delivery charges', '2021-10-24 04:53:53', 1, NULL, 1, 1, 3, 1, 1, 11, '2021-02-06 20:53:53', '2021-02-06 20:53:53'),
(5, 0, 5, 1, 266, 1, 1, 'mahendra-tharthe-name-is-enough', 1, 0.00, 'Balkumari Bridge, Koteshwor, Kathmandu, Nepal', '27.6730537', '85.34189479999999', '9803308893', NULL, 'Mahendra Thar...The Name Is Enough...', 1575000.00, NULL, 'Full modified...just painted full body....no any fault on engine...', '2021-10-24 04:57:59', 3, NULL, 1, 1, 3, 1, 1, 25, '2021-02-06 20:57:59', '2021-02-06 20:57:59'),
(6, 0, 6, 1, 218, 1, 1, 'quality-designer-cakes-at-reasonable-price', 50, 0.00, 'Mandikhatar liquor store, Budanilkantha, Nepal', '27.7402891', '85.34896909999999', '9841082584', 'https://www.youtube.com/watch?v=RMfPJcdowJ0&feature=emb_logo', 'Quality Designer Cakes @ Reasonable Price', 1000.00, NULL, 'S Confectioner & Patisserie is the answer to your search for show-stopping cakes, high-quality pastries, and bread. We just don’t make cakes… we create excellent products with exacting standards making sure that our cakes leave a ‘Wow!’ on our customers’ lips.\r\nWe are focused on things that make a lasting culinary impression… product innovation; quality; consistency; improving our customer experience. We create our offerings using only high-quality base ingredients. All our products are made fresh and from scratch at our facility in Kathmandu.\r\nOur philosophy is simple: We believe in offering fresh, quality, healthy, crafted goodness that is kind to your wallet. Add into that a little bit of creativity, innovative touches to traditional recipes, and a dedicated kitchen team, and you are sure to and that we’ve managed to create something magical to your taste.\r\nWe enjoy going that extra mile to create cakes that are tailor-made for you, no matter what the occasion is because we genuinely love what we are doing. And we make sure you not only have unforgettable desserts but that you are just as impressed with the level of service that is served up with it.\r\nGo ahead and indulge yourself with these amazing products, don’t forget to share it with your loved ones.', '2021-10-09 05:02:47', NULL, 13, 1, 3, 2, 1, 1, 24, '2021-02-06 21:02:47', '2021-02-06 21:04:37'),
(7, 0, 7, 0, 110, 1, 1, 'naruto-cos-couple-t-shirt-yc21591', 50, 0.00, 'Machhapokhari Chowk, Ring Road, Kathmandu, Nepal', '27.7353111', '85.3058395', '9849824947', NULL, 'NARUTO COS COUPLE T-SHIRT YC21591', 897.00, NULL, 'Material: Cotton\r\nSize: S, M, L, XL\r\nS: Bust 102cm Length 64cm Shoulder 48cm Sleeve 19cm\r\nM: Bust 106cm Length 66cm Shoulder 49cm Sleeve 20cm\r\nL: Bust 110cm Length 68cm Shoulder 50cm Sleeve 21cm\r\nXL: Bust 114cm Length 70cm Shoulder 51cm Sleeve 21cm\r\n\r\n\r\nAbout color&size difference: According to the light and different computer monitor, the color may be slightly different as pictures. Besides, please allow 1-3cm measurement differ due to manual making. Your understanding and support are highly appreciated.\r\n\r\nPlease contact +977-9849824952 | 9880695377 | 9810043268\r\n📩 mcare967@gmail.com\r\n🚚Home Delivery available.', '2021-10-08 05:55:08', 1, NULL, 1, 2, 4, 1, 1, 11, '2021-02-06 21:55:08', '2021-02-06 21:55:08'),
(8, 0, 8, 0, 127, 1, 1, 'couple-round-neck-t-shirt-combo', 1, 0.00, 'Machhapokhari Chowk, Ring Road, Kathmandu, Nepal', '27.7353111', '85.3058395', '9849824952', NULL, 'Couple Round Neck T-Shirt Combo', 800.00, NULL, 'Type T-Shirt\r\nPrimary Color White\r\nFit Regular Fit\r\nFabric Cotton\r\nPattern Printed\r\nSleeve Type Wide Half Sleeve\r\n\r\nPlease contact +977-9849824952 | 9880695377 | 9810043268\r\n📩 mcare967@gmail.com\r\n🚚Home Delivery available.', '2021-10-08 05:57:09', 1, NULL, 1, 2, 4, 1, 1, 11, '2021-02-06 21:57:09', '2021-02-06 21:57:09'),
(9, 0, 9, 0, 115, 1, 1, 'anime-t-shirt-aesthetic-anime-shirt-aesthetic-clothing', 50, 0.00, 'Machhapokhari Chowk, Ring Road, Kathmandu, Nepal', '27.7353111', '85.3058395', '9849824947', NULL, 'Anime T-Shirt, Aesthetic Anime Shirt, Aesthetic Clothing', 700.00, NULL, 'The Nice And Unique Gift For Holidays And Special Occasions. We Offer Shirts To Buy For Christmas, Xmas, Halloween, Father\'S Day, Mothers Day, Birthday, Party, Back To School.\r\nOur products are officially licensed, designed, and printed in Nepal.\r\n100% SATISFACTION GUARANTEE: If you are not happy with our product, please feel free to contact us, we will give the best solution to you within 24 hours.\r\n\r\nSIZE: Please see more details in our size picture chart for accurate sizes.\r\nCUSTOM DESIGN: Please contact us to request changing anything in the design with no more extra fee.\r\nCARE INSTRUCTIONS: Machine washes cold inside-out. Tumble dry low inside-out. Do not iron overprint. Do not dry clean. Do not bleach\r\n\r\nPlease contact +977-9849824952 | 9880695377 | 9810043268\r\n📩 mcare967@gmail.com\r\n🚚Home Delivery available.', '2021-10-08 05:59:15', 1, NULL, 1, 2, 4, 1, 1, 11, '2021-02-06 21:59:15', '2021-02-06 21:59:15'),
(10, 0, 10, 0, 122, 1, 1, 'biku-son-bike-zone', 1, 0.00, 'Koteshwor Party Palace, Kathmandu, Nepal', '27.6712169', '85.3502024', NULL, NULL, 'Biku Son Bike Zone', 33.00, NULL, 'this is bike service where we sell second hand bike .. if anyone who is interested please contact', '2021-10-08 06:01:37', 1, NULL, 1, 3, 4, 8, 1, 28, '2021-02-06 22:01:37', '2021-02-07 09:20:59'),
(11, 0, 11, 1, 145, 1, 0, 'land-for-sale-in-thecho', 1, 0.00, 'Thecho, Nepal', '27.6175711', '85.3187914', '9849551992', NULL, 'Land for sale in thecho', 1600000.00, NULL, '4 ana jagga for sale in plotting area ... 8 feet ko road bhako ...', '2021-10-08 06:39:31', NULL, 6, 1, 2, 4, 1, 3, 28, '2021-02-06 22:39:31', '2021-03-14 11:07:30'),
(14, 0, 14, 1, 529, 1, 0, 'the-lunch-delight', 40, 0.00, 'New Baneshwor, Kathmandu, Nepal', '27.6915196', '85.3420486', NULL, NULL, 'Lunch Delight', 33.00, NULL, 'Find here the best of lunch delight.  Love your hunger giving your hunger a new option.. Location : Baneshwor Kathmandu', '2021-10-24 09:27:47', 1, NULL, 2, 2, 4, 8, 1, 22, '2021-02-23 05:47:17', '2021-07-11 01:23:36'),
(16, 0, 15, 0, 219, 1, 1, '2-pc-daraj', 1, 0.00, 'Imadol, Mahalaxmi, Nepal', '27.6564024', '85.3420486', '9851114354', NULL, '2 pc daraj', 21500.00, NULL, 'Brand new daraz for sale at an affortable price.. serious buyer please call 985114354 available here for anytime', '2021-10-18 06:47:58', 2, NULL, 1, 1, 1, 1, 3, 32, '2021-03-02 22:47:58', '2021-03-02 23:11:06'),
(18, 0, 16, 1, 127, 1, 0, 'heavenly-white-forest-3-pound', 1, 0.00, 'Sumeru City Hospital, Lalitpur, Nepal', '27.676005', '85.314724', NULL, NULL, 'Heavenly White Forest 3 pound', 835.00, NULL, 'This cake is 3 pound. All cakes are baked with hygiene and quality in the top of our priority at our own state-of-the-art baking facility and quality checked twice by our expert team before delivery. You can send cakes or gifts to your loved ones in Nepal or shop online for yourself in Nepal with Mero Offer, a pioneer in online shopping in Nepal.', '2021-10-06 12:37:30', 2, 13, 3, 2, 4, 1, 3, 28, '2021-03-07 04:37:30', '2021-03-07 04:37:30'),
(19, 0, 17, 0, 90, 1, 0, 'chocoloate-cake-1-pound', 20, 0.00, 'GEMS School, Dhapakhel Road, Lalitpur, Nepal', '27.64314869999999', '85.3262662', '9818238939', NULL, 'Chocoloate Cake 1 pound', 650.00, NULL, 'Please call 9818238939 after ordering the cake.\r\nThe cake can be delivered online, which is the best online cake delivery service in Kathmandu.\r\n\r\nAll cakes are baked with hygiene and quality at the top of our priority at our own state-of-the-art baking facility and quality checked twice by our expert team before delivery.', '2021-10-06 12:58:58', 2, 13, 3, 2, 4, 1, 3, 28, '2021-03-07 04:58:58', '2021-03-07 06:14:37'),
(20, 0, 18, 0, 122, 1, 0, 'butter-scous-1-pound', 20, 0.00, 'GEMS School, Dhapakhel Road, Lalitpur, Nepal', '27.64314869999999', '85.3262662', '9818238939', NULL, 'Butter scous 1 pound', 800.00, NULL, 'All cakes are baked with hygiene and quality in the top of our priority at our own state-of-the-art baking facility and quality checked twice by our expert team before delivery. You can send cakes or gifts to your loved ones in Nepal or shop online for yourself in Nepal with Mero Offer, a pioneer in online shopping in Nepal.\r\n\r\nPlease call 9818238939 after ordering the cake.', '2021-10-06 13:01:10', 2, 13, 3, 2, 4, 1, 3, 28, '2021-03-07 05:01:10', '2021-03-07 06:14:08'),
(21, 0, 19, 0, 103, 1, 0, 'stwarry-cake-1-pound', 20, 0.00, 'GEMS School, Dhapakhel Road, Lalitpur, Nepal', '27.64314869999999', '85.3262662', '9818238939', NULL, 'Stwarry Cake 1 pound', 750.00, NULL, 'All cakes are baked with hygiene and quality in the top of our priority at our own state-of-the-art baking facility and quality checked twice by our expert team before delivery. You can send cakes or gifts to your loved ones in Nepal or shop online for yourself in Nepal with Mero Offer, a pioneer in online shopping in Nepal.\r\n\r\nIf you have any queries you can directly call to Mero Offer.\r\nPlease call 9818238939 after ordering the cake.', '2021-10-06 13:03:37', 2, 13, 1, 2, 4, 1, 3, 28, '2021-03-07 05:03:37', '2021-03-07 06:13:28'),
(22, 0, 20, 0, 141, 1, 0, 'vanella-1-pound', 0, 0.00, 'Dhapakhel, Lalitpur, Nepal', '27.6267867', '85.33171329999999', '9818238939', NULL, 'Vanella 1 pound', 500.00, NULL, 'All cakes are baked with hygiene and quality in the top of our priority at our own state-of-the-art baking facility and quality checked twice by our expert team before delivery. You can send cakes or gifts to your loved ones in Nepal or shop online for yourself in Nepal with Mero Offer, a pioneer in online shopping in Nepal.\r\n\r\nPlease call 9818238939 after ordering the cake.', '2021-10-06 13:04:48', 2, 13, 3, 2, 4, 1, 3, 28, '2021-03-07 05:04:48', '2021-07-11 01:23:36'),
(23, 0, 21, 0, 106, 1, 0, 'white-forest-1-pound', 30, 0.00, 'GEMS School, Dhapakhel Road, Lalitpur, Nepal', '27.64314869999999', '85.3262662', '9818238939', NULL, 'White Forest 1 pound', 550.00, NULL, 'All cakes are baked with hygiene and quality in the top of our priority at our own state-of-the-art baking facility and quality checked twice by our expert team before delivery. You can send cakes or gifts to your loved ones in Nepal or shop online for yourself in Nepal with Mero Offer, a pioneer in online shopping in Nepal.\r\n\r\nPlease call 9818238939 after ordering the cake.', '2021-10-06 13:06:21', 2, 13, 3, 2, 4, 1, 3, 28, '2021-03-07 05:06:21', '2021-03-07 06:12:26'),
(24, 1, 22, 0, 107, 1, 0, 'pineapple-1-pound', 10, 0.00, 'GEMS School, Dhapakhel Road, Lalitpur, Nepal', '27.64314869999999', '85.3262662', '9818238939', NULL, 'PIneapple 1 pound', 750.00, NULL, 'All cakes are baked with hygiene and quality in the top of our priority at our own state-of-the-art baking facility and quality checked twice by our expert team before delivery. You can send cakes or gifts to your loved ones in Nepal or shop online for yourself in Nepal with Mero Offer, a pioneer in online shopping in Nepal.\r\n\r\nPlease call 9818238939 after ordering the cake.', '2021-10-06 13:08:20', 2, 13, 3, 2, 4, 1, 3, 28, '2021-03-07 05:08:20', '2021-03-24 05:16:27'),
(25, 0, 23, 0, 83, 1, 0, 'black-forest-1-pound', 20, 0.00, 'GEMS School, Dhapakhel Road, Lalitpur, Nepal', '27.64314869999999', '85.3262662', '9818238939', NULL, 'Black Forest 1 pound', 550.00, NULL, 'All cakes are baked with hygiene and quality in the top of our priority at our own state-of-the-art baking facility and quality checked twice by our expert team before delivery. You can send cakes or gifts to your loved ones in Nepal or shop online for yourself in Nepal with Mero Offer, a pioneer in online shopping in Nepal.\r\n\r\nPlease call 9818238939 after ordering the cake.', '2021-05-06 13:10:54', 2, 13, 3, 2, 4, 1, 3, 28, '2021-03-07 05:10:54', '2021-03-07 06:11:06'),
(26, 0, 24, 0, 58, 1, 0, 'blue-berry-1-pound', 10, 0.00, 'GEMS School, Dhapakhel Road, Lalitpur, Nepal', '27.64314869999999', '85.3262662', '9818238939', NULL, 'Blue Berry 1 pound', 750.00, NULL, 'All cakes are baked with hygiene and quality in the top of our priority at our own state-of-the-art baking facility and quality checked twice by our expert team before delivery. You can send cakes or gifts to your loved ones in Nepal or shop online for yourself in Nepal with Mero Offer, a pioneer in online shopping in Nepal.\r\n\r\nPlease call 9818238939 after ordering the cake.', '2021-05-06 13:14:47', 2, 13, 3, 2, 4, 1, 3, 28, '2021-03-07 05:14:46', '2021-03-07 06:10:41'),
(27, 0, 25, 0, 134, 1, 0, 'red-velvet-2-pound', 0, 0.00, 'GEMS School, Dhapakhel Road, Lalitpur, Nepal', '27.64314869999999', '85.3262662', '9818238939', NULL, 'Red Velvet 2 pound', 450.00, NULL, 'All cakes are baked with hygiene and quality in the top of our priority at our own state-of-the-art baking facility and quality checked twice by our expert team before delivery. You can send cakes or gifts to your loved ones in Nepal or shop online for yourself in Nepal with Mero Offer, a pioneer in online shopping in Nepal.\r\n\r\nAfter ordering the product please call 9818238939 .', '2021-10-06 13:16:28', 2, 13, 3, 2, 4, 1, 3, 28, '2021-03-07 05:16:28', '2021-07-10 00:47:53'),
(28, 0, 26, 1, 182, 1, 1, 'limited-edition-summer-hoodies', 18, 0.00, 'Jamal, Rastriya Naach Ghar, Kathmandu, Nepal', '27.7092711', '85.3152373', '9884069209', NULL, 'Limited Edition Summer Hoodies', 2800.00, 2500.00, 'Unique edition summer hoodie\r\nFashion Brand \r\nLimited orders!!\r\nNew drop!!!!\r\nNEW ARRIVAL!!!\r\nGet this epic printed hoodies only at @theregalave_ (insta)\r\nFor a better you♡\r\nFollow us and get 5% off on each purchase!!\r\nLimited piece only!!\r\nRs2500/- \r\nSize =L,XL,XXL,XXXL\r\nColour = black ,white\r\nFree delivery service all over nepal', '2021-10-17 08:56:53', 2, NULL, 1, 2, 4, 1, 1, 41, '2021-03-18 00:56:53', '2021-03-20 03:35:38'),
(29, 0, 27, 1, 118, 1, 1, 'samsung-m40', 1, 0.00, 'Koteshwor, Kathmandu, Nepal', '27.6755549', '85.3459238', '9801622351', NULL, 'Samsung M40', 25000.00, 25000.00, 'The Samsung Galaxy M40 boasts of many segment-firsts such as a hole-punch display and a vibrating screen for a earpiece. It features a bright and vibrant 6.3-inch full-HD+ PLS TFT LCD display, which has good viewing angles but there\'s also a mild vignetting around the edges, which can be distracting.', '2021-10-10 16:22:30', 3, NULL, 1, 1, 1, 1, 1, 42, '2021-03-26 08:22:30', '2021-03-26 21:42:36'),
(30, 0, 28, 1, 107, 1, 1, 'samsung-galaxy-m31', 1, 0.00, 'Koteshwar Mahadev Temple, Koteshwar, Ahmedabad, Gujarat, India', '23.0941395', '72.6122042', NULL, NULL, 'Samsung Galaxy M31', 34999.00, 34984.00, 'Samsung’s Galaxy A series and M series are getting overcrowded and the company isn’t stopping anytime soon. Samsung Galaxy M31- the company’s first phone of 2020 in the Galaxy M-series was launched in Nepal amidst the COVID-19 lockdown. The M31 phone is still going strong and Samsung has dropped the price of the phone recently. The handset is an improved and redesigned variant of last year’s M30 and M30s with a much-updated camera.', '2021-10-16 17:03:51', 2, NULL, 1, 2, 1, 1, 1, 42, '2021-04-01 09:03:51', '2021-04-02 04:09:16'),
(31, 0, 29, 1, 26, 1, 0, 'samsung-galaxy-m12', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'Samsung Galaxy M12', 22499.00, 22482.00, 'The Samsung Galaxy M12 features a 6.5-inch HD+ PLS TFT Infinity-V display. Infinity-V refers to a water-drop notch and its resolution also is 720x1600p.\r\n\r\nFurthermore, you can experience more content with this display. Similarly, its HD+ technology provides clear as well as sharp disaplay quality which enables the enjoyment of enjoying the ultimate daily contents.', '2021-04-16 17:13:09', 2, NULL, 1, 2, 1, 1, 1, 42, '2021-04-01 09:13:09', '2021-04-02 04:08:51'),
(32, 0, 30, 0, 16, 1, 1, 'portable-wireless-speakers', 1, 0.00, 'Pepsi Ground, Samabhav Society, Gorai 2, Borivali West, Mumbai, Maharashtra, India', '19.2305046', '72.8248192', '9840329488', NULL, 'Portable Wireless Speakers', 2000.00, 2300.00, 'Speaker with the best material and every speaker is the same quality. And it is CE certificated. Besides, we packed by air bags to prevent it from damaging on the way.Any problems pls contact us freely at any time, we will offer you the best customer service. Thank you!\r\n\r\nFeature:\r\n\r\n1. Brand new and high quality.\r\n\r\n2. Mesh cover for protect speaker from water, dust, shock.\r\n\r\n3. IPX5 waterproof, dustproof, shockproof.\r\n\r\n4. 5W*2 big power with two bass diaphragms on each side, vibrate when playing music.\r\n\r\n5. Cylindrical shape and built-in left and right channel offer 360° stereo.\r\n\r\n6. Call function, noise canceling microphone makes you answer calls without reaching your phone.\r\n\r\n7. FM receiver makes you listen to the radio freely.\r\n\r\n8. Support MP3, WMA, WAV, FLAC, APE format music files.\r\n\r\n9. Support TF Card or misro SD Card up to 32G, support USB 2.0 Disk.\r\n\r\n10. Support USB flash disk plug and play.\r\n\r\n11. Support external MP3 / MP4 / computer and other audio input function (AUX）\r\n\r\n12. Easy button control, you can press buttons to turn on/off, play/pause, switch mode, skip songs, adjust volume etc.\r\n\r\n13. Bluetooth 5.0, fast connection and stable signal, compatible with all Bluetooth devices.\r\n\r\n14. 1200 mAh battery for up to 10 hours of playtime at 2/3 volume on a full charg', '2021-04-19 04:34:52', 2, NULL, 1, 2, 1, 1, 1, 43, '2021-04-03 20:34:52', '2021-04-04 00:00:49'),
(33, 0, 31, 0, 17, 1, 1, 'yamaha-fzs', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'YAMAHA FZs', 200000.00, 200000.00, 'The Yamaha FZ Series set a new benchmark for biking. The FZS-FI takes it to the next level. It brings the most advanced Yamaha Blue Core concept to offer bikers an unmatched experience of performance and efficiency. At its heart is the all new Yamaha Fuel Injection(FI) engine.', '2021-04-23 06:09:01', 5, NULL, 1, 1, 1, 1, 3, 42, '2021-04-07 22:09:00', '2021-04-09 00:00:10'),
(34, 0, 32, 0, 14, 1, 1, 'avenger-cruise-220', 1, 0.00, 'Koteshwor, Kathmandu, Nepal', '27.6755549', '85.3459238', NULL, NULL, 'Avenger Cruise 220', 299962.00, NULL, 'Bajaj Avenger 220 is all about cruising in style. While Bajaj continues to send mixed signals for the Avenger lineup, it is still one of the most popular cruise motorcycles in Nepal. Interestingly, it is only available in a single variant: Avenger Street 220 ABS.', '2021-04-23 06:38:58', 5, NULL, 1, 1, 1, 1, 3, 42, '2021-04-07 22:38:58', '2021-04-08 23:59:16'),
(35, 0, 33, 0, 22, 1, 1, 'redmi-6', 1, 0.00, 'Koteshwor, Kathmandu, Nepal', '27.6755549', '85.3459238', NULL, NULL, 'Redmi 6', 10000.00, NULL, 'Xiaomi recently launched the Redmi 6 in India, along with the Redmi 6A (Review) and the Redmi 6 Pro (Review). As its name suggests, it is the successor of the Redmi 5 (Review), which has been fairly popular for offering good hardware at affordable prices. The Redmi 6 gets a new processor, has interesting specifications on paper, and is positioned between the Redmi 6A and the Redmi 6 Pro.', '2021-05-08 06:52:52', 5, NULL, 1, 1, 2, 1, 1, 42, '2021-04-07 22:52:52', '2021-04-08 23:59:03'),
(36, 0, 34, 0, 20, 1, 0, 'samsung-galaxy-m01-core', 1, 0.00, 'New Baneshwor, Kathmandu, Nepal', '27.6915196', '85.3420486', NULL, NULL, 'Samsung Galaxy M01 Core', 8399.00, 8399.00, '8MP (F2.2) rear camera | 5MP (F2.4) front camera\r\n5.3-inch (13.41 centimeters) PLS TFT LCD, HD+ capacitive multi-touch touchscreen with 1480 x 720 pixels resolution, 310 ppi pixel density and 16 M color support\r\nMemory, Storage & SIM: 1GB RAM | 16GB internal memory expandable up to 512GB | Dual SIM (nano+nano) dual-standby (4G+4G)\r\nAndroid GO | v10.0 operating system with 1.5GHz+2GHz MediaTek | MT6739WW quad core processor\r\n3000mAH lithium-ion battery', '2021-05-08 14:29:49', 2, NULL, 1, 2, 2, 1, 1, 42, '2021-04-08 06:29:49', '2021-04-08 23:58:52'),
(37, 0, 35, 0, 28, 1, 0, 'samsung-galaxy-a32', 1, 0.00, 'Koteshwar Mahadev Mandir, Old walled City, Rajkot, Gujarat, India', '22.300938', '70.808033', NULL, NULL, 'Samsung Galaxy A32', 31999.00, 31999.00, 'There are 2 variants of A32 based on cellular technology. There’s not much of a visual overhaul between the 4G and 5G models of the Galaxy A32. So, they’re visibly the same. But one can notice how the notch for the selfie camera is different between the two. While the 5G model had an Infinity-V cutout for the selfie camera, that’s substituted by a sharper Infinity-U notch on the Galaxy A32 4G.', '2021-05-08 14:44:33', 2, NULL, 1, 2, 2, 1, 1, 42, '2021-04-08 06:44:33', '2021-04-08 08:52:56'),
(38, 0, 36, 1, 136, 1, 0, 'rafting-package', 39, 0.00, 'New Baneshwor, Kathmandu, Nepal', '27.6915196', '85.3420486', NULL, NULL, 'Rafting Package', 2500.00, NULL, 'Summer is here\r\n\r\nThe first river you paddle runs through the rest of your life.\r\n\r\nONE DAY TRISHULI/BHOTEKOSHI RAFTING @ Rs 2500/-\r\nPrice Includes:-\r\n✔️ Two-way transportation\r\n✔️ Professional Rafting Guide, Safety Drum for Camera & Mobiles\r\n✔️ Rafting 3+ Hour\r\n✔️ All necessary Safety Equipment like Life Jacket, Paddle, Helmet\r\n✔️ Buffet Veg or Non-Veg Lunch at the resort\r\n✔️ Photos & Videos\r\n------------------------------------------\r\nOvernight Stay and Rafting at Trishuli River Side @ Rs 3500/-\r\nPrice Includes:-\r\n✔️ Two-way Transportation\r\n✔️ Professional Rafting Guide, Safety Drum for Camera & Mobiles\r\n✔️ All necessary Safety Equipment like Life Jacket, Paddle, Helmet\r\n✔️ Rafting 3+ Hour\r\n✔️ 1 Veg or Non-Veg Lunch\r\n✔️ Photo & Videos\r\n✔️ Snacks\r\n✔️ Campfire\r\n✔️ Music\r\n✔️ Veg / Non-Veg Dinner\r\n✔️ Night Stay (Tent)\r\n✔️ Breakfast', '2021-05-08 16:50:56', NULL, NULL, 4, 3, 2, 8, 1, 28, '2021-04-08 08:50:56', '2021-04-15 09:54:40'),
(39, 0, 37, 0, 12, 1, 0, 'akg-type-c-earphone', 1, 0.00, 'New Baneshwor, Kathmandu, Nepal', '27.6915196', '85.3420486', NULL, NULL, 'AKG TYPE C EARPHONE', 425.00, 425.00, 'Wired earphones tuned by AKG with tangle-free, fabric cable\r\n\r\n8mm and 11mm speaker units for clear, balanced sound', '2021-04-23 17:18:38', 2, NULL, 1, 2, 1, 1, 1, 42, '2021-04-08 09:18:38', '2021-04-08 21:48:52'),
(40, 0, 38, 0, 9, 1, 0, 'jbl-tune-110-bt', 1, 0.00, 'New Baneshwor, Kathmandu, Nepal', '27.6915196', '85.3420486', NULL, NULL, 'JBL TUNE 110 BT', 4900.00, 4900.00, 'A pair of 9mm drivers punch out serious bass resulting in the JBL pure bass sound You have heard in concert halls and arenas around the world\r\nA single button remote on a no-tangle cable lets you control music playback as well as answer calls on the fly with a built-in microphone', '2021-04-23 17:21:03', 2, NULL, 1, 2, 1, 1, 1, 42, '2021-04-08 09:21:03', '2021-04-08 21:49:06'),
(41, 0, 39, 0, 19, 1, 0, 'oppo-watch-46mm', 1, 0.00, 'New Road Gate, नयाँ सडक, Kathmandu, Nepal', '27.7032447', '85.3131068', NULL, NULL, 'Oppo Watch (46mm)', 34990.00, 34990.00, 'Compatible with all Android Smartphones\r\n46MM (1.91-inch) AMOLED Flexible Dual-Curved Display with 402x476 screen resolution\r\nGoogle Android based Wear OS operating system with Qualcomm Snapdragon Wear 3100 & Ambiq Micro Apollo3 Wireless SoC processor', '2021-04-23 17:23:46', 2, NULL, 1, 2, 1, 1, 1, 42, '2021-04-08 09:23:46', '2021-04-08 21:49:35'),
(42, 0, 40, 0, 20, 1, 0, 'redmi-earbuds-s', 1, 0.00, 'New Road Gate, नयाँ सडक, Kathmandu, Nepal', '27.7032447', '85.3131068', NULL, NULL, 'Redmi Earbuds S', 3200.00, 3200.00, 'Bluetooth 5.0, IPX4, Music playback time up to 12 hours, Charging time 1.5 hours, Seamless Pairing, Punchier sound and DSP Environmental Noise cancellation, light weight, sleek', '2021-05-08 17:26:09', 2, NULL, 1, 2, 2, 1, 1, 42, '2021-04-08 09:26:09', '2021-04-08 21:49:47'),
(43, 0, 41, 0, 31, 1, 0, 'skullcandy-sesh-true-wireless-in-ear-earbud', 1, 0.00, 'Bhaktapur Durbar Square, Durbar square, Bhaktapur, Nepal', '27.6720744', '85.42810229999999', NULL, NULL, 'Skullcandy Sesh True Wireless In-Ear Earbud', 8500.00, 8500.00, 'Bluetooth wireless technology\r\n10 hours of total battery\r\nIP55 sweat, water, and dust resistant\r\nCharging case', '2021-05-08 17:28:20', 2, NULL, 1, 2, 2, 1, 1, 42, '2021-04-08 09:28:20', '2021-04-08 21:49:58'),
(44, 0, 42, 0, 25, 1, 0, 'oppo-watch-41mm', 1, 0.00, 'Sanepa, Lalitpur, Nepal', '27.6844496', '85.3058664', NULL, NULL, 'Oppo Watch (41mm)', 24990.00, 24990.00, 'Dual-curved Edge AMOLED Screen\r\nVOOC Flash Charge-15 min charge all day use--Up to 2 weeks of battery life\r\nDual-curved Edge AMOLED Screen VOOC Flash Charge-15 min charge all day use--Up to 2 weeks of battery life Dual Processor - Qualcomm Snapdragon 3100 and Apollo 3 Ultra-Lightweight Body\r\nNotifications from WhatsApp received and replied through a shortcut.\r\nWith Call Function', '2021-04-23 17:30:19', 2, NULL, 1, 2, 1, 1, 1, 42, '2021-04-08 09:30:19', '2021-04-08 21:50:07'),
(45, 0, 43, 0, 0, 0, 0, '20000mah-mi-power-bank-2i', 1, 0.00, 'Thimi, Nepal', '27.6781812', '85.3807886', NULL, NULL, '20000mAh Mi Power Bank 2i', 3199.00, 3199.00, 'It will charge a 3000mAh phone battery 4 times And It will charge a 4000mAh phone battery 3 times\r\nTime to Recharge - Approx 6.7 hours(18W- 9V/2A charger, standard USB cable) OR Approx 10 hours(10W- 5V/2A charger, standard USB cable)', '2021-04-24 08:58:28', 2, NULL, 1, 2, 1, 1, 2, 42, '2021-04-09 00:58:28', '2021-04-09 00:58:28'),
(46, 0, 44, 0, 20, 1, 0, '4-pieces-100-cotton-baby-blankets-newborn-bedding-set-infant-cot-crib-sheet-swaddle-towel-multi-functions-baby-wrap', 1, 0.00, 'Thimi, Nepal', '27.6781812', '85.3807886', NULL, NULL, '4 Pieces 100% Cotton Baby Blankets Newborn Bedding Set Infant Cot Crib Sheet Swaddle Towel Multi Functions Baby Wrap', 1290.00, 1290.00, 'Made from soft and warm 100% cotton flannel\r\n • Includes 4 receiving blankets • Machine wash cold, tumble dry low Receiving blankets have fun prints to add sweet style', '2021-04-24 09:01:33', 1, NULL, 1, 2, 1, 1, 1, 42, '2021-04-09 01:01:33', '2021-04-10 08:18:12'),
(47, 0, 45, 0, 2, 0, 0, 'newborn-baby-clothes-set-with-blanket', 1, 0.00, 'Bhaktapur, Nepal', '27.6710221', '85.4298197', NULL, NULL, 'Newborn Baby clothes Set With Blanket', 1190.00, 1190.00, 'Metrials: Velvet\r\nVery Soft\r\n10Pcs of 1 Set\r\nNewborn To 3 Months', '2021-04-24 09:04:54', 2, NULL, 1, 2, 1, 1, 1, 42, '2021-04-09 01:04:54', '2021-04-09 01:04:54'),
(48, 0, 46, 0, 0, 0, 0, 'nillkin-camshield-pro-case-for-samsung-galaxy-s21-ultra', 1, 0.00, 'Koteshwor, Kathmandu, Nepal', '27.6755549', '85.3459238', NULL, NULL, 'Nillkin CamShield Pro Case For Samsung Galaxy S21 Ultra', 2399.00, 2399.00, 'CamShied Armor case is made for outdoor sports lovers\r\nImpact resistant & camera protection & kickstand.\r\nMade of environmentally friendly TPU and PC material.\r\nUpgraded impact resistant function.\r\nImpact resistant air space on four corners.\r\nProvides your device reliable protection.', '2021-04-24 09:09:48', 1, NULL, 1, 2, 1, 1, 1, 48, '2021-04-09 01:09:48', '2021-04-09 01:09:48'),
(49, 0, 47, 0, 0, 0, 0, 'metal-ring-camera-lens-protector-for-iphone-12-pro-max', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'Metal Ring Camera Lens Protector For iPhone 12 Pro Max', 785.00, 779.00, '1 Aluminum alloy protection sticker ,2. Easy fit , Effective dispersion of impact power when surface is strongly impacted 2 9d arc surface full protection lens ,3. Avoid force concentration after grinding4. Hydrophobic oil treatment , Not easily contaminated with fingerprints5 Nami adsorption , Simple and easy fit , Super adsorption ! Alignment and deposition can automatically adsorb and exhaust the air', '2021-05-09 09:11:42', 1, NULL, 1, 2, 2, 1, 3, 48, '2021-04-09 01:11:42', '2021-04-09 01:11:42'),
(50, 0, 48, 0, 0, 0, 0, 'clear-case-colorful-crome-finish-border-with-camera-protection-for-iphone-12-pro-max', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'Clear Case Colorful Crome Finish Border With Camera Protection For iPhone 12 pro max', 1799.00, 1799.00, 'COMPATIBILITY: Designed specifically for the 6.1” iPhone 12 and iPhone 12 Pro (2020). SLIM & FLEXIBLE: Made with a lightweight tough polymer that’s easy to put on or take off your iPhone 12/iPhone 12 Pro. SCREEN & CAMERA PROTECTION: Raised bezels protect your iPhone 12’s/iPhone 12 Pro’s screen & camera from scratches. WATERMARK-FREE: Microdots in the back of this iPhone 12/iPhone 12 Pro case keep it from clinging to your phone. CLEAR BUT COLORFUL: The crystal-clear back showcases your iPhone while the colored frame makes it really pop.', '2021-05-09 09:14:37', 2, NULL, 1, 2, 2, 1, 3, 48, '2021-04-09 01:14:37', '2021-04-09 01:14:37'),
(51, 0, 49, 0, 0, 0, 0, 'liquid-silicone-case-for-galaxy-s21-ultra-68-inches-jelly-rubber-bumper-case-cover-with-soft-microfiber-lining', 1, 0.00, 'Koteshwor, Kathmandu, Nepal', '27.6755549', '85.3459238', NULL, NULL, 'Liquid Silicone Case for Galaxy S21 Ultra (6.8 inches) Jelly Rubber Bumper Case Cover with Soft Microfiber Lining', 950.00, 950.00, 'Compatibility – Liquid silicone case designed for Samsung Galaxy S21 Ultra 5G, 6.8 inches, release 2020; not fit Samsung S21 5G (6.2 inches), S21+/S21 Plus 5G (6.7 inches), please double check your mobile phone model before you place order.\r\n\r\nLiquid Silicone Material – S21 Ultra case is made of real liquid silicone, anti-dust better than over 95% silicone cases, for reach a better hand feeling and anti-dust, we have used 2 times extra polish process and oil coating, anti-fingerprint, anti-scratches, comfortable touch feeling just like baby skin. Any stain that gets on the case wipes off easily with a damp rag, will not fade or discoloring.', '2021-05-09 09:16:39', 2, NULL, 1, 2, 2, 1, 1, 48, '2021-04-09 01:16:39', '2021-04-09 01:16:39'),
(52, 0, 50, 0, 1, 0, 0, 'iphone-11-pro-max-case-clear-bumper-hard-pc-soft-tpu-lightweight-slim-fit-thin-transparent-anti-scratch-protective-cover-for-iphone-11-pro-max-case-65-inch-2019', 1, 0.00, 'Koteshwor, Kathmandu, Nepal', '27.6755549', '85.3459238', NULL, NULL, 'iPhone 11 Pro Max Case, Clear Bumper Hard PC+ Soft TPU Lightweight Slim Fit Thin Transparent Anti-Scratch Protective Cover for iPhone 11 Pro Max Case 6.5 inch 2019', 199.00, 199.00, 'Precisely designed for iPhone 11 Pro Max 6.5 inch. Easy access to all the controls and features. Perfect cutouts for speakers, camera and other ports (Compatible with wireless charger).', '2021-05-24 09:18:38', 2, NULL, 1, 2, 3, 1, 1, 48, '2021-04-09 01:18:38', '2021-04-09 09:03:03'),
(53, 0, 51, 0, 3, 0, 0, 'polychromatic-camshield-bumper-protective-case-for-xiaomi-redmi-note-9-propro-max9s', 1, 0.00, 'New Baneshwor, Kathmandu, Nepal', '27.6915196', '85.3420486', NULL, NULL, 'Polychromatic CamShield Bumper Protective Case for Xiaomi Redmi Note 9 Pro/Pro Max/9s', 899.00, 899.00, 'Material: Soft TPU+Acrylic Composite\r\nStyle:Fashion/Simple/Ultra-thin\r\nFunction:Dirt-resistant', '2021-04-24 09:20:27', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-09 01:20:27', '2021-04-09 01:20:27'),
(54, 0, 52, 0, 0, 0, 0, 'nillkin-super-frosted-shield-matte-cover-case-for-samsung-galaxy-s20-fe-2020-fan-edition-2020', 1, 0.00, 'New Baneshwor, Kathmandu, Nepal', '27.6915196', '85.3420486', NULL, NULL, 'Nillkin Super Frosted Shield Matte cover case for Samsung Galaxy S20 FE 2020 (Fan edition 2020)', 1199.00, 1199.00, 'Nillkin Super Frosted Shield Matte cover case for Samsung Galaxy S20 FE 2020 (Fan edition 2020)\r\nThe matte cover of this Nillkin case was made by high precision injection with imported environmentally friendly PC materials. The case is coated on both sides with dustless matte UV painting technology which looks and feels great. Nillkin frosted is rigid enough to offer great protection, yet flexible enough not to break off. The surface is water-resistant and dustproof. Also, it does not collect fingerprint smudges or skid marks. The case feels great in your hands and you can hold it freely and comfortably.\r\nSimple, Stylish, Extraordinary', '2021-04-24 09:23:39', 1, NULL, 1, 2, 1, 1, 1, 48, '2021-04-09 01:23:39', '2021-04-09 01:23:39'),
(55, 0, 53, 0, 0, 0, 0, 'silicone-samsung-galaxy-s21-ultra', 1, 0.00, 'Satdobato, Lalitpur, Nepal', '27.6515356', '85.327837', NULL, NULL, 'Silicone Samsung Galaxy S21 Ultra', 950.00, 950.00, 'Compatibility: Samsung Galaxy S21 Ultra\r\nA soft microfiber lining on the inside helps protect your iPhone.\r\nOn the outside, the silky, soft-touch finish of the silicone exterior feels great in your hand.\r\nAnd you can keep it on all the time, even when you’re charging wirelessly.', '2021-04-24 09:25:45', 2, NULL, 1, 1, 1, 1, 3, 48, '2021-04-09 01:25:45', '2021-04-09 01:25:45'),
(56, 0, 54, 0, 4, 0, 0, 'liquid-silicone-case-xiaomi-redmi-note-9-pro-max-redimi-note-9-pro-luxury-soft-case-mi-note-9-pronote-9-pro-max', 1, 0.00, 'Satdobato, Lalitpur, Nepal', '27.6515356', '85.327837', NULL, NULL, 'Liquid Silicone Case Xiaomi Redmi Note 9 Pro Max & Redimi Note 9 Pro Luxury Soft Case MI Note 9 Pro/Note 9 Pro Max', 750.00, 750.00, '【Liquid Silicone Case ] Applying high-quality liquid Silicpne material, Soft & Silky\r\n【Full Body Design & Durable Silicone】 Increase bottom protection, the thicker silicone material to protect enhanced. Effectively buffer the impact of falling the phone. Can better protect your phone.\r\n【Screen and Camera Protection】 This case\'s edge lip to protect your screen and Camera. Don\'t worry about your phone\'s camera will be scratching when put on the table.', '2021-04-24 09:28:24', 1, NULL, 1, 2, 1, 1, 3, 48, '2021-04-09 01:28:24', '2021-04-09 01:28:24'),
(57, 0, 55, 0, 3, 0, 0, 'nillkin-camshield-armor-case-for-samsung-galaxy-s21-ultra-s21-ultra-5g', 1, 0.00, 'Satdobato, Lalitpur, Nepal', '27.6515356', '85.327837', NULL, NULL, 'Nillkin CamShield Armor Case For Samsung Galaxy S21 Ultra (S21 Ultra 5G)', 3099.00, 3099.00, 'CamShied Armor case is made for outdoor sports lovers.\r\nImpact resistant & camera protection & kickstand. Made of environmentally friendly TPU and PC material.', '2021-04-24 09:30:33', 1, NULL, 1, 2, 1, 1, 3, 48, '2021-04-09 01:30:33', '2021-04-09 01:30:33'),
(58, 0, 56, 0, 1, 0, 0, 'kanti-herbal-lemongrass-essential-oil-8-ml', 1, 0.00, 'Satdobato Swimming Pool, Lalitpur, Nepal', '27.6597325', '85.3277683', NULL, NULL, 'Kanti Herbal Lemongrass Essential Oil - 8 ml', 250.00, 250.00, 'Has beneficial properties as antidepressant, antimicrobial, antiseptic, astringent, fungicidal etc\r\nHelpful to relieve pain and heal wounds', '2021-04-24 09:34:03', 1, NULL, 1, 2, 1, 1, 3, 48, '2021-04-09 01:34:03', '2021-04-09 01:34:03'),
(59, 0, 57, 0, 1, 0, 0, 'kanti-herbal-eucalyptus-essential-oil-8-ml', 1, 0.00, 'New Baneshwor, Kathmandu, Nepal', '27.6915196', '85.3420486', NULL, NULL, 'Kanti Herbal Eucalyptus Essential Oil - 8 ml', 250.00, 250.00, 'Its health benefits include treating colds and flu, hair care, relieving muscle pain, and preventing cavities\r\nWidely used in wounds, respiratory problems, muscle pain, dental care, skin care etc', '2021-04-24 09:35:59', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-09 01:35:59', '2021-04-09 01:35:59'),
(60, 0, 58, 0, 0, 0, 0, 'natures-essence-jojoba-oil-100-ml', 1, 0.00, 'New Baneshwor, Kathmandu, Nepal', '27.6915196', '85.3420486', NULL, NULL, 'Nature\'s Essence Jojoba Oil 100 ml.', 750.00, 750.00, 'Jojoba Oil 100 ml\r\nJojoba Oil 100 ml\r\nNormal Skin\r\nSKU104102375_NP-1024926892', '2021-04-24 09:38:44', 2, NULL, 1, 1, 1, 1, 1, 48, '2021-04-09 01:38:44', '2021-04-09 01:38:44'),
(61, 0, 59, 0, 1, 0, 0, 'rose-essential-oil-8ml', 1, 0.00, 'Kalanki, Kathmandu, Nepal', '27.6931052', '85.28065389999999', NULL, NULL, 'Rose Essential Oil -8ml', 200.00, 200.00, 'Rose fragrance oil* Useful in aromatherapy for its rejuvenating qualities*Helpful to improve your circulation*Helps to rejuvenate mind and body', '2021-04-24 09:40:48', 1, NULL, 1, 2, 1, 1, 1, 48, '2021-04-09 01:40:48', '2021-04-09 01:40:48'),
(62, 0, 60, 0, 1, 0, 0, 'kanti-herbal-clove-essential-oil-8ml', 1, 0.00, 'Thimi, Nepal', '27.6781812', '85.3807886', NULL, NULL, 'Kanti Herbal Clove Essential Oil- 8ml', 250.00, 250.00, 'Consists of several health benefits\r\nActs as an antiseptic, depurative, astringent, antispasmodic etc\r\nHelpful to purify blood', '2021-04-24 09:42:54', 2, NULL, 1, 2, 1, 1, 2, 48, '2021-04-09 01:42:54', '2021-04-09 01:42:54'),
(63, 0, 61, 0, 0, 0, 0, 'palsonic-australia-40-full-hd-android-smart-led-tv', 1, 0.00, 'Kalanki, Kathmandu, Nepal', '27.6931052', '85.28065389999999', NULL, NULL, 'Palsonic Australia 40\" Full Hd Android Smart Led Tv', 34960.00, 34960.00, 'Brand: Palsonic Australia\r\nModel: 40S2100\r\nScreen Size: 40\"\r\nScreen Resolution: Full HD (1920×1080)\r\nAndroid Smart 8.0', '2021-04-24 09:59:09', 1, NULL, 1, 1, 1, 1, 1, 48, '2021-04-09 01:59:09', '2021-04-09 01:59:09'),
(64, 0, 62, 0, 0, 0, 0, 'palsonic-australia-32-full-hd-led-tv-black', 1, 0.00, 'Koteshwor, Kathmandu, Nepal', NULL, NULL, NULL, NULL, 'Palsonic Australia 32\" Full Hd Led Tv - Black', 20990.00, 20990.00, 'Brand Name: Palsonic Australia\r\nModel No: 32N1100\r\nDisplay Size: 32-inches\r\nPower Consumption: 100-240 V / 50-60 Hz\r\nResolution: 1920 x 1080 Pixels\r\nConnectivity: USB / HDMI / VGA In / Audio In / Video In / Earphone Out\r\nFeatures: A+ Grade Panel / USB Playback For Movies, Music &', '2021-04-24 10:01:20', 1, NULL, 1, 1, 1, 1, 3, 48, '2021-04-09 02:01:20', '2021-04-09 02:01:20'),
(65, 0, 63, 0, 0, 0, 0, 'videocon-32-90-android-smart-led-tv', 1, 0.00, 'Gatthaghar, Madhyapur Thimi, Nepal', '27.6765033', '85.3743339', NULL, NULL, 'Videocon 32\" 9.0 Android Smart Led Tv', 30500.00, 30500.00, 'features Smart TV: yes\r\nconnectivity 2 USB Ports, 2 HDMI Ports\r\ndisplay 32 Inch, LED, HD-Ready, 1366x768\r\ndesign Colour: Black\r\nWarranty: 1 year', '2021-04-24 10:03:05', 2, NULL, 1, 2, 1, 1, 2, 48, '2021-04-09 02:03:05', '2021-04-09 02:03:05'),
(66, 0, 64, 0, 0, 0, 0, 'konka-32-inch-hd-led-tv-ke32ed311', 1, 0.00, 'Gatthaghar, Madhyapur Thimi, Nepal', '27.6765033', '85.3743339', NULL, NULL, 'Konka 32 Inch HD LED TV (KE32ED311)', 23520.00, 23520.00, 'The Magnificent Konka Series is designed to be \"A Remarkably Better TV\". With LED technology and Full HD resolution the display will deliver an immersive entertainment experience with sharp colour and contrast. KONKA TV supports 3D Surround Sound, ensuring extraordinary audio quality. \r\nAt its core is the advanced XC3™ UHD Engine that includes CrystalView™ Clarity Enhancer, DeepBlack™ Contrast Enhancer and PurePalette™ Color Enhancer for picture quality that is a step above. To enhance the picture quality, Konka has the HiBright™ Pro Backlight for a 25% brighter picture and ColorWave™ Wide Color Gamut for deeper, more lifelike colors. The Series also includes DynaBright™, AccuMotion™120.', '2021-04-24 10:04:44', 1, NULL, 1, 2, 1, 8, 2, 48, '2021-04-09 02:04:44', '2021-04-09 02:04:44'),
(67, 0, 65, 0, 0, 0, 0, 'samsung-led-tv-49-full-hd-smart-ua49n5300', 1, 0.00, 'Gatthaghar, Madhyapur Thimi, Nepal', NULL, NULL, NULL, NULL, 'Samsung Led Tv 49 Full Hd Smart – Ua49N5300', 69900.00, 69900.00, 'Model: UA49N5300\r\nBrand: Samsung\r\nScreen Size In Inches: 49\r\nScreen Technology: LED\r\nHD Type: Full HD\r\nScreen Resolution: 1920 x 1080\r\nCurved Design: No\r\n4K Technology: No\r\nBuilt-In Receiver: Yes\r\nSmart Connection: Yes\r\nWi-Fi Connection: Yes\r\nNumber Of HDMI Inputs:2\r\nNumber Of USB Ports: 1', '2021-04-24 10:06:53', 1, NULL, 1, 1, 1, 1, 2, 48, '2021-04-09 02:06:53', '2021-04-09 02:06:53'),
(68, 0, 66, 0, 0, 0, 0, 'samsung-ua43t5500-43-smart-full-hd-led-tv-2020', 1, 0.00, 'Tinkune, Kathmandu, Nepal', NULL, NULL, NULL, NULL, 'SAMSUNG UA43T5500 - 43\" Smart Full HD LED TV 2020', 50900.00, 50900.00, 'Smart TV with Tizen OS\r\nPurColor, Tune Station, & Auto Brightness Detection\r\nQuad-Core Processor\r\nAccessibility ( Voice Guide, High Contrast, Multi-output audio)\r\nAuto Channel Search, auto power-off\r\nSmart Hub & One Remote Control\r\n2 Years Brand Warranty\r\nVoice Assistant with One Remote Control', '2021-04-24 10:09:17', 2, NULL, 1, 1, 1, 1, 1, 48, '2021-04-09 02:09:17', '2021-04-09 02:09:17'),
(69, 0, 67, 0, 0, 0, 0, 'samsung-43inch-full-hd-smart-led', 1, 0.00, 'Tinkune, Kathmandu, Nepal', NULL, NULL, NULL, NULL, 'Samsung 43Inch Full HD Smart LED', 74090.00, 74090.00, 'Brand: Samsung Model:UA43N5300ARSHE Color: Black Screen Type: LED HD Technology & Resolution: FULL HD HDMI / USB / Built In Wi-Fi 3G Dongle Plug and Play Supported App:Netflix Screen Mirroring TM1240A Remote Controller Brand: Samsung Colour: Black Screen Size: 43 Inches Item model number: UA43N5300ARSHE Resolution: 1920 x 1080 Pixels Display Technology: LED Number of Speakers: 4 Speaker Output RMS: 40W Screen Mirroring HDMI / USB Built In Wi-Fi / 3G Dongle Plug and Play Supported App: Netflix/Youtube Width x Height x Depth (without stand) - 979.9 mm x 571.9 mm x 77.3 mm', '2021-04-24 10:11:20', 2, NULL, 1, 1, 1, 1, 1, 48, '2021-04-09 02:11:20', '2021-04-09 02:11:20'),
(70, 0, 68, 0, 0, 0, 0, 'sony-bravia-43-full-hd-smart-tv-kdl-43w660g', 1, 0.00, 'Tinkune, Kathmandu, Nepal', NULL, NULL, NULL, NULL, 'SONY Bravia 43\" Full Hd Smart Tv [Kdl-43W660G]', 71000.00, 71000.00, 'High Dynamic Range\r\nFull HD Resolution (1920 x 1080)\r\nX-Reality™ PRO\r\nYouTube™', '2021-04-24 10:14:01', 1, NULL, 1, 1, 1, 1, 3, 48, '2021-04-09 02:14:01', '2021-04-09 02:14:01'),
(71, 0, 69, 0, 1, 0, 0, 'sony-bravia-klv-32r302e-32-hd-led-tv', 1, 0.00, 'Tinkune, Kathmandu, Nepal', '27.6915087', '85.29590329999999', NULL, NULL, 'SONY Bravia Klv-32R302E 32 Hd Led Tv', 33600.00, 33600.00, 'Resolution : HD Ready (1366x768p)\r\nRefresh Rate: 50 hertz\r\nDisplay: HD Ready, Clear Resolution Enhancer\r\nConnectivity: 2 HDMI ports to connect set top box, Blu Ray players, gaming console\r\n1 USB port to connect hard drives and other USB devices\r\nSound : 10 Watts Output, Clear Phase\r\nWarranty: 1 year standard manufacturer warranty from Sony, 2 Years Service Warranty', '2021-04-24 10:17:17', 1, NULL, 1, 2, 1, 1, 3, 48, '2021-04-09 02:17:17', '2021-04-09 02:17:17'),
(72, 0, 70, 0, 1, 0, 0, 'sony-bravia-65-4k-android-led-tv-kd-65x8000h', 1, 0.00, 'Kalanki, Kathmandu, Nepal', '27.6931052', '85.28065389999999', NULL, NULL, 'SONY BRAVIA 65\" 4K ANDROID LED TV KD-65X8000H', 292000.00, NULL, 'See the beauty with more colour, contrast and clarityClear multidimensional soundRefined design matches your spaceTransform your viewing with Dolby Vision™', '2021-05-09 10:19:11', 2, NULL, 1, 2, 2, 1, 1, 48, '2021-04-09 02:19:11', '2021-04-09 02:19:11'),
(73, 0, 71, 0, 12, 1, 0, 'mug-with-long-handle-1225ml', 1, 0.00, 'Koteshwor, Kathmandu, Nepal', '27.6755549', '85.3459238', NULL, NULL, 'Mug With Long Handle 1225ml', 200.00, 200.00, 'Brand : Bengal Plastics\r\nCapacity : 1225 ml\r\nProduct Dimension : 265 × 147 × 109 mm\r\n100 % Original Product', '2021-05-01 17:31:22', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-16 09:31:22', '2021-04-17 03:40:12'),
(74, 0, 72, 0, 8, 1, 0, 'gem-plastic-bucket-drum-with-lid-50-l-505', 1, 0.00, 'Koteshwor, Kathmandu, Nepal', '27.6755549', '85.3459238', NULL, NULL, 'Gem Plastic Bucket (Drum) With Lid - 50 L (505)', 1500.00, NULL, 'Material: Plastic\r\nCapacity: 50L\r\nDimension: (LxB): 20\" x 17.5\"\r\nUsed To Keep Drinking Water And Rice Or Other Useful Materials\r\nHas Full Cover Lid to protect from Dust and insect.\r\nCan Be Used In Bathrooms, Kitchens, Garages, Offices Etc.', '2021-05-01 17:36:23', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-16 09:36:23', '2021-04-17 03:39:59'),
(75, 0, 73, 0, 0, 0, 0, 'collapsible-wash-tub-space-saving-plastic-washtub-155-x-155-x-35cm', 1, 0.00, 'Koteshwor, Kathmandu, Nepal', '27.6755549', '85.3459238', NULL, NULL, 'Collapsible Wash Tub Space Saving Plastic Washtub 15.5 X 15.5 X 3.5cm', 1200.00, NULL, '1:Made with premium BPA-free PP material, sturdy and safe to use.2:Collapsible washtub to make it convenient to store and help you save space.3:Hanging hole for easy storage.', '2021-05-01 17:39:44', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-16 09:39:44', '2021-04-16 09:39:44'),
(76, 0, 74, 0, 0, 0, 0, 'blue-kitchen-dish-basket', 1, 0.00, 'Koteshwor, Kathmandu, Nepal', '27.6755549', '85.3459238', NULL, NULL, 'Blue Kitchen Dish basket', 1000.00, NULL, 'Durable and strong\r\nUseful for house, offices\r\nKeeps your home managed\r\nQuality ensured', '2021-05-01 17:42:09', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-16 09:42:09', '2021-04-16 09:42:09'),
(77, 0, 75, 0, 13, 1, 0, 'reusable-latex-hand-gloves-for-kitchen', 1, 0.00, 'Koteshwor, Kathmandu, Nepal', '27.6755549', '85.3459238', NULL, NULL, 'Reusable Latex Hand Gloves For Kitchen', 300.00, NULL, 'Take Care of Your Hands While Doing Daily Household Chores with This Pair of Velveteen Rubber Protected Full Size Gloves Made of 100% Natural Latex.\r\nSuitable for Dishwashing, Cleaning Kitchen, Cleaning Bathroom, Cleaning Toilet, Washing Clothes and Doing Other Household Chores.', '2021-05-01 17:47:50', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-16 09:47:50', '2021-04-17 03:55:16'),
(78, 0, 76, 0, 15, 1, 0, 'green-plastic-tub', 1, 0.00, 'Koteshwor, Kathmandu, Nepal', '27.6755549', '85.3459238', NULL, NULL, 'Green Plastic Tub', 500.00, NULL, 'Material: Plastic\r\nDiameter: 7\"\r\nUse it as a Dish Pan, laundry Pan, Cleaning Pail & More.', '2021-05-01 17:51:06', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-16 09:51:06', '2021-04-17 03:40:29'),
(79, 0, 77, 0, 9, 1, 0, 'washing-machine-cover-for-7-to-9-kg-front-load-water-proof', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'Washing Machine Cover For 7 to 9 kg Front Load Water Proof', 1000.00, NULL, 'Hurry up limited stock Happy Shopping. Slide 2 is just the sample rest you can get same as the pic .just let us know if u need a specific design through chat.', '2021-05-01 17:54:01', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-16 09:54:01', '2021-04-17 03:42:24'),
(80, 0, 78, 0, 11, 1, 0, 'microtel-caller-id-phone', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'Microtel Caller Id Phone', 2000.00, NULL, 'FSK/DTMF Dual System Caller ID Phone\r\n2 Ways Hands Free Speakerphone\r\nRinger Selectable\r\nOne Touch Redial\r\nMusic On Hold\r\nLCD Contrast Adjustable', '2021-05-01 17:56:11', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-16 09:56:11', '2021-04-17 03:42:09'),
(81, 0, 79, 0, 11, 1, 0, 'divya-motion-and-light-sensor-led-bulb-9w', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'Divya Motion And Light Sensor Led Bulb-9W', 1000.00, NULL, 'Light Sensor: Automatic switch on and off\r\nDay night sensor\r\n9w Led', '2021-05-01 17:57:44', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-16 09:57:44', '2021-04-17 03:41:58'),
(82, 0, 80, 0, 14, 1, 0, 'remote-controlled-automatic-rechargeable-emergency-ceiling-wall-light-led-bulb', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'Remote Controlled Automatic Rechargeable Emergency Ceiling Wall Light Led Bulb', 1200.00, NULL, 'POWER OUT, LIGHT ON! LED Bulb integrates a rechargeable built in Lithium Ion battery, bulb will automatically activate in the event of a sudden power outage(Hurricane,Snowstorm) and will light for up to 3.5 hours\r\nNOT ONLY BULB The Everbright light bulb can be used as flashlight for home or outdoor', '2021-05-01 18:00:47', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-16 10:00:47', '2021-04-17 03:39:28'),
(83, 0, 81, 0, 14, 1, 0, 'shangrila-magic-buddha-printed-t-shirt-for-men', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'Shangrila Magic Buddha Printed T-Shirt For Men', 800.00, NULL, 'Brand: Shangri-La\r\nFabric: 100% Cotton\r\nNeck: Round neck\r\nFit: Regular\r\nWash Care:Hand/Machine wash', '2021-05-02 17:48:28', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-17 09:48:28', '2021-04-21 07:18:31'),
(84, 0, 82, 0, 2, 0, 0, 'combo-of-5-half-sleeves-t-shirt-for-men', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'Combo Of 5 Half Sleeves T-Shirt For Men', 1800.00, NULL, 'Material : Cotton\r\nNeck : Round\r\nSleeves : Short Sleeves\r\nStyle :Plane\r\nFit : Regular\r\nWash Care : Hand/Machine Wash', '2021-05-02 17:51:14', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-17 09:51:14', '2021-04-17 09:51:14');
INSERT INTO `products` (`id`, `sold`, `order`, `featured`, `views`, `status`, `bid`, `slug`, `quantity`, `weight`, `address`, `latitude`, `longitude`, `cell`, `url`, `name`, `price`, `rgr_price`, `desc`, `expired_at`, `condition_id`, `pricelabel_id`, `assured_id`, `pricetype_id`, `day_id`, `adtype_id`, `city_id`, `user_id`, `created_at`, `updated_at`) VALUES
(85, 0, 83, 0, 1, 0, 0, 'shangrila-pack-of-3-full-sleeve-t-shirt-with-button-for-men', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'Shangrila Pack Of 3 Full Sleeve T-Shirt With Button For Men', 1881.00, NULL, 'Brand: Shangri-La\r\nFabric: 100% Cotton\r\nNeck: Round neck\r\nFit: Regular\r\nWash Care:Hand/Machine wash', '2021-05-02 17:53:00', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-17 09:53:00', '2021-04-17 09:53:00'),
(86, 0, 84, 0, 12, 1, 0, 'shangrila-combo-of-4-stone-wash-t-shirt-for-men', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'Shangrila Combo Of 4 Stone Wash T-Shirt For Men', 2100.00, NULL, 'Brand: Shangri-La\r\nFabric: 100% Cotton\r\nNeck: Round neck\r\nFit: Regular\r\nWash Care:Hand/Machine wash', '2021-05-02 17:55:00', 2, NULL, 1, 1, 1, 1, 1, 48, '2021-04-17 09:55:00', '2021-04-21 07:19:54'),
(87, 0, 85, 0, 8, 1, 0, 'shangrila-combo-of-3-cotton-heavy-metal-printed-t-shirts-for-men', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'Shangrila Combo Of 3 Cotton Heavy-Metal Printed T-Shirts For Men', 1150.00, NULL, 'Fabric : 100% Cotton\r\nSleeves : Half Sleeves\r\nNeck : Round Neck\r\nFit : Regular\r\nStyle : Printed\r\nDifferent sizes available\r\nWash/Care : Machine / Hand Wash, Dry in Shade', '2021-05-02 17:57:56', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-17 09:57:56', '2021-04-21 07:14:24'),
(88, 0, 86, 0, 2, 0, 0, 'navy-blue-solid-windcheater-for-men', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'Navy Blue Solid Windcheater For Men', 1226.00, NULL, 'Navy Blue Solid Thick (Baklo) Windcheater (NEW)\r\nMaterial : Polyester\r\nStyle : Solid\r\nClosing : Zip\r\nNeck : Hooded and Removable Hood\r\nSleeves : Full\r\n2 layer : inner is net\r\nWash: Machine Care/ Cold Wash', '2021-05-02 18:00:04', 2, NULL, 1, 2, 1, 1, 1, 48, '2021-04-17 10:00:04', '2021-04-21 07:20:24'),
(94, 0, 87, 0, 20, 1, 1, 'modern-bungalow-13-a-on-sale-at-balkhu', 1, 0.00, 'Balkhu, Kathmandu, Nepal', '27.6862726', '85.2948778', NULL, NULL, 'Modern Bungalow 13 A On Sale at Balkhu', 29079000.00, NULL, 'Property Description\r\nIf you are looking for a bungalow inside a colony within Ringroad in Kathmandu, we have just a house for you. The house is inside a housing colony developed by reputed real estate organization named Brihat Group. \r\n\r\n\r\n\r\nIt is located inside Brihat Cluster - Balkhu, which is just 1.3 km from Tribhuvan University, 600m from Vayodha Hospital, 2.2 km from Kalanki Chowk and 4.5 km from New Road. \r\n\r\n\r\n\r\nWe build the house following new bylaws and with plenty of required spaces, front yard, back yard, parking spaces. The community itself has plenty of open space, greenery, wide access road. It can be reached by 3 entrances connecting to newly constructed Ringroad.\r\n\r\nPrice is Negotiable\r\n\r\nBrand New (not used) house In a Colony\r\n\r\nAbove 20 feet road\r\n\r\nBuilt up 1870 (sq.ft)\r\n\r\nNon Furnishing\r\n\r\nGarden , Parking Space , Security Guards', '2021-05-06 05:57:58', 2, 2, 1, 1, 1, 1, 1, 53, '2021-04-20 21:57:58', '2021-04-21 07:10:49'),
(95, 0, 88, 0, 12, 1, 0, 'samsung-galaxy-a12-4gb-128gb-blue', 1, 0.00, 'Kalanki, Kathmandu, Nepal', '27.6931052', '85.28065389999999', NULL, NULL, 'Samsung Galaxy A12 4GB 128GB Blue', 19999.00, NULL, 'The phone comes with a 6.50-inch touchscreen display with a resolution of 720x1600 pixels and an aspect ratio of 20:9. Samsung Galaxy A12 is powered by a 1.8GHz octa-core MediaTek Helio P35 (MT6765) processor that features 4 cores clocked at 1.8GHz and 4 cores clocked at 23GHz. It comes with 4GB of RAM. The Samsung Galaxy A12 runs Android 10 and is powered by a 5000mAh battery. The Samsung Galaxy A12 supports proprietary fast charging.', '2021-05-06 06:09:57', 1, NULL, 1, 2, 1, 1, 1, 53, '2021-04-20 22:09:57', '2021-04-21 07:10:37'),
(96, 0, 89, 0, 10, 1, 0, 'oppo-a12-3gb-32gb-blue', 1, 0.00, 'Kalanki, Kathmandu, Nepal', '27.6931052', '85.28065389999999', NULL, NULL, 'Oppo A12 3GB 32GB Blue', 16590.00, NULL, 'OPPO A12 smartphone has a IPS LCD display. It measures 155.9 mm x 75.5 mm x 8.3 mm and weighs 165 grams. The screen has a resolution of 720 x 1520 pixels and 270 ppi pixel density. It has an aspect ratio of 19:9 and screen-to-body ratio of 81.63 %. On camera front, the buyers get a 5 MP f/2.4 Primary Camera(5.0\" sensor size, 1.12µm pixel size) and on the rear, there\'s an 13 MP + 2 MP camera with features like Digital Zoom, Auto Flash, Touch to focus. It is backed by a 4230 mAh battery. Connectivity features in the smartphone include WiFi, Bluetooth, GPS, Volte, and more.', '2021-05-06 06:13:37', 1, NULL, 1, 2, 1, 1, 1, 53, '2021-04-20 22:13:36', '2021-04-21 07:10:12'),
(97, 0, 90, 0, 13, 1, 0, 'samsung-galaxy-m01s-3gb-32gb-blue', 1, 0.00, 'Balkhu, Kathmandu, Nepal', '27.6862726', '85.2948778', NULL, NULL, 'Samsung Galaxy M01s 3GB 32GB Blue', 14499.00, NULL, 'The phone comes with a 6.20-inch touchscreen display with a resolution of 720x1280 pixels at a pixel density of 270 pixels per inch (ppi). Samsung\r\nGalaxy M01s is powered by an octa-core MediaTek Helio P22 (MT6762) processor. It comes with 3GB of RAM. The Samsung Galaxy M01s runs Android 9 Pie and is powered by a 4000mAh battery. As far as the cameras are concerned, the Samsung Galaxy M01s on the rear packs a 13-megapixel primary camera and a second 2-megapixel camera. It sports a 8-megapixel camera on the front for selfies. The Samsung Galaxy M01s runs One UI based on Android 9 Pie and packs 32GB of inbuilt storage (up to 512GB) with a dedicated slot.\r\n\r\nThe phone comes with a 6.20-inch touchscreen display with a resolution of 720x1280 pixels at a pixel density of 270 pixels per inch (ppi). Samsung\r\nGalaxy M01s is powered by an octa-core MediaTek Helio P22 (MT6762) processor. It comes with 3GB of RAM. The Samsung Galaxy M01s runs Android 9 Pie and is powered by a 4000mAh battery. As far as the cameras are concerned, the Samsung Galaxy M01s on the rear packs a 13-megapixel primary camera and a second 2-megapixel camera. It sports a 8-megapixel camera on the front for selfies. The Samsung Galaxy M01s runs One UI based on Android 9 Pie and packs 32GB of inbuilt storage (up to 512GB) with a dedicated slot.\r\n\r\nNETWORK\r\nTECHNOLOGY	GSM / HSPA / LTE\r\nLAUNCH\r\nAnnounced	GSM / HSPA / LTE\r\nSTATUS	Available. Released 2020, July 16\r\nBODY\r\nDIMENSIONS	156.9 x 75.8 x 7.8 mm (6.18 x 2.98 x 0.31 in)\r\nWEIGHT	168 g (5.93 oz)\r\nBUILD	Glass front, plastic back, plastic frame\r\nSIM	Dual SIM (Nano-SIM, dual stand-by)\r\nDISPLAY\r\nTYPE	PLS TFT capacitive touchscreen, 16M colors\r\nSIZE	6.2 inches, 95.9 cm2 (~80.7% screen-to-body ratio)\r\nRESOLUTION	720 x 1520 pixels, 19:9 ratio (~271 ppi density)\r\nPROTECTION	Glass front, plastic back, plastic frame\r\nPLATFORM\r\nOS	Android 9.0 (Pie), upgradable to Android 10, One UI 2.1\r\nCHIPSET	Mediatek MT6762 Helio P22 (12 nm)\r\nCPU	Octa-core (4x2.0 GHz & 4x1.5 GHz)\r\nGPU	PowerVR GE8320\r\nMEMORY\r\nCARD SLOT	microSDXC (dedicated slot)\r\nINTERNAL	32GB 3GB RAM\r\nMAIN CAMERA\r\nDual	13 MP, f/1.8, 28mm (wide), 1/3.1\", 1.12µm, AF 2 MP, f/2.4, (depth)\r\nFEATURES	LED flash\r\nVIDEO	1080p@30fps\r\nSELFIE CAMERA\r\nSINGLE	8 MP, f/2.0\r\nFEATURES	HDR\r\nVIDEO	1080p@30fps\r\nSOUND\r\nLOUDSPEAKER	Yes\r\n3.5MM JACK	Yes\r\nCOMMS\r\nWLAN	Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot\r\nBLUETOOTH	4.2, A2DP, LE\r\nGPS	Yes, with A-GPS, GLONASS, GALILEO, BDS\r\nRADIO	FM radio\r\nUSB	microUSB 2.0, USB On-The-Go\r\nFEATURES\r\nSENSORS	Fingerprint (rear-mounted), accelerometer, proximity\r\nBATTERY\r\nCAPACITY	Li-Ion 4000 mAh, non-removable\r\nMISC\r\nCOLORS	Light Blue, Gray\r\nMODELS	SM-M017F, SM-M017F/DS\r\nThere could be certain circumstances beyond our control', '2021-05-06 06:17:48', 1, NULL, 1, 1, 1, 1, 1, 53, '2021-04-20 22:17:48', '2021-04-21 07:09:38'),
(98, 0, 91, 0, 19, 1, 0, 'realme-c12-3gb-32gb-power-blue', 1, 0.00, 'Balkumari, Lalitpur, Nepal', '27.6695288', '85.3407568', NULL, NULL, 'Realme C12 3GB 32GB Power Blue', 16990.00, NULL, 'The Realme C12 has a 6.5-inch HD+ LCD screen, with a resolution of 1600x720 pixels and an unspecified version of Corning\'s Gorilla Glass. Colours looked decent to me, but the quality of the panel is pretty average. The main selling point here is the large 6,000mAh battery. Battery life is excellent, but the C12 doesn\'t support fast charging. \r\nThe C12 has a 2-megapixel monochrome camera and a 2-megapixel macro camera, in addition to the main 13-megapixel primary camera. There\'s a 5-megapixel selfie camera. Image quality was stritcly average when shooting during the day and quite poor in low light', '2021-05-06 06:20:47', 1, NULL, 1, 2, 1, 1, 3, 53, '2021-04-20 22:20:47', '2021-04-21 07:09:17'),
(99, 0, 92, 1, 48, 1, 1, 'realme-7', 5, 0.00, 'Bhaktapur, Nepal', '27.6710221', '85.4298197', '9849865934', 'https://youtu.be/e8b2kWNltac', 'Realme 7', 35990.00, 32990.00, 'NETWORK\r\nTechnology	GSM / HSPA / LTE\r\n2G bands	GSM 850 / 900 / 1800 / 1900 - SIM 1 & SIM 2\r\n3G bands	HSDPA 850 / 900 / 2100\r\n4G bands	1, 3, 5, 8, 38, 40, 41\r\nSpeed	HSPA 42.2/5.76 Mbps, LTE-A\r\nLAUNCH\r\nAnnounced	2020, September 03\r\nStatus	Available. Released 2020, September 10\r\nBODY\r\nDimensions	162.3 x 75.4 x 9.4 mm (6.39 x 2.97 x 0.37 in)\r\nWeight	196.5 g (6.95 oz)\r\nBuild	Glass front (Gorilla Glass 3), plastic back, plastic frame\r\nSIM	Dual SIM (Nano-SIM, dual stand-by)\r\nDISPLAY\r\nType	IPS LCD, 90Hz, 480 nits (typ)\r\nSize	6.5 inches, 102.0 cm2 (~83.4% screen-to-body ratio)\r\nResolution	1080 x 2400 pixels, 20:9 ratio (~405 ppi density)\r\nProtection	Corning Gorilla Glass 3\r\nPLATFORM\r\nOS	Android 10, upgradable to Android 11, Realme UI 2.0\r\nChipset	Mediatek Helio G95 (12 nm)\r\nCPU	Octa-core (2x2.05 GHz Cortex-A76 & 6x2.0 GHz Cortex-A55)\r\nGPU	Mali-G76 MC4\r\nMEMORY\r\nCard slot	microSDXC (dedicated slot)\r\nInternal	64GB 6GB RAM, 128GB 8GB RAM\r\n 	UFS 2.1\r\nMAIN CAMERA\r\nQuad	64 MP, f/1.8, 26mm (wide), 1/1.73\", 0.8µm, PDAF\r\n8 MP, f/2.3, 119˚, 16mm (ultrawide), 1/4.0\", 1.12µm\r\n2 MP, f/2.4, (macro)\r\n2 MP, f/2.4, (depth)\r\nFeatures	LED flash, HDR, panorama\r\nVideo	4K@30fps, 1080p@30/60/120fps, gyro-EIS\r\nSELFIE CAMERA\r\nSingle	16 MP, f/2.1, 26mm (wide), 1/3.1\", 1.0µm\r\nFeatures	Panorama\r\nVideo	1080p@30/120fps\r\nSOUND\r\nLoudspeaker	Yes\r\n3.5mm jack	Yes\r\nCOMMS\r\nWLAN	Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot\r\nBluetooth	5.0, A2DP, LE\r\nGPS	Yes, with A-GPS, GLONASS, BDS\r\nNFC	Yes (Indonesia only)\r\nRadio	No\r\nUSB	USB Type-C 2.0, USB On-The-Go\r\nFEATURES\r\nSensors	Fingerprint (side-mounted), accelerometer, gyro, proximity, compass\r\nMessaging	SMS(threaded view), MMS, Email, Push Email, IM\r\nBrowser	HTML5\r\nBATTERY\r\nType	Li-Po 5000 mAh, non-removable\r\nCharging	Fast charging 30W, 50% in 26 min, 100% in 65 min (advertised)\r\nMISC\r\nColors	Mist Blue, Mist White\r\nModels	RMX2151, RMX2163\r\nSAR	0.99 W/kg (head)     0.93 W/kg (body', '2021-08-18 02:01:23', 2, NULL, 1, 3, 2, 1, 2, 63, '2021-07-18 18:01:23', '2021-07-18 19:39:09'),
(100, 0, 93, 0, 28, 1, 1, 'alto-800-semi-option', 1, 0.00, 'sitapaila', NULL, NULL, '9843806920', NULL, 'alto 800 semi option', 2700000.00, 2700000.00, 'brand new car one hand ali ali scratch xa testo xina arko gadi linu parda sell gar na khojya', '2021-09-07 04:15:34', 2, NULL, 1, 1, 2, 1, 1, 65, '2021-08-07 20:15:33', '2021-08-08 19:54:43');

-- --------------------------------------------------------

--
-- Table structure for table `product_amenities`
--

CREATE TABLE `product_amenities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `amenity_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_amenities`
--

INSERT INTO `product_amenities` (`id`, `amenity_id`, `product_id`) VALUES
(4, 2, 2),
(5, 10, 2),
(6, 13, 2),
(7, 34, 5),
(8, 40, 5),
(9, 42, 5),
(10, 43, 5),
(11, 45, 5),
(12, 46, 5),
(13, 63, 29),
(14, 65, 29),
(15, 66, 29),
(16, 67, 29),
(17, 68, 29),
(18, 70, 29),
(19, 75, 29),
(20, 66, 31),
(21, 75, 31),
(22, 48, 33),
(23, 49, 33),
(24, 52, 33),
(25, 55, 33),
(26, 56, 33),
(27, 60, 33),
(28, 48, 34),
(29, 52, 34),
(30, 56, 34),
(31, 60, 34),
(32, 61, 35),
(33, 65, 35),
(34, 66, 35),
(35, 67, 35),
(36, 68, 35),
(37, 73, 35),
(38, 75, 35),
(39, 76, 35),
(48, 69, 37),
(49, 74, 37),
(50, 75, 37),
(51, 77, 37),
(52, 65, 36),
(53, 66, 36),
(54, 67, 36),
(55, 68, 36),
(56, 2, 94),
(57, 3, 94),
(58, 6, 94),
(59, 12, 94),
(60, 13, 94),
(61, 15, 94),
(62, 16, 94),
(63, 17, 94),
(64, 19, 94),
(65, 61, 95),
(66, 62, 95),
(67, 63, 95),
(68, 65, 95),
(69, 66, 95),
(70, 67, 95),
(71, 68, 95),
(72, 69, 95),
(73, 70, 95),
(74, 71, 95),
(75, 72, 95),
(76, 75, 95),
(77, 76, 95),
(86, 61, 96),
(87, 62, 96),
(88, 63, 96),
(89, 64, 96),
(90, 66, 96),
(91, 67, 96),
(92, 68, 96),
(93, 76, 96),
(94, 61, 97),
(95, 62, 97),
(96, 63, 97),
(97, 64, 97),
(98, 66, 97),
(99, 67, 97),
(100, 68, 97),
(101, 70, 97),
(102, 72, 97),
(103, 73, 97),
(104, 75, 97),
(105, 76, 97),
(106, 61, 98),
(107, 62, 98),
(108, 63, 98),
(109, 64, 98),
(110, 66, 98),
(111, 67, 98),
(112, 68, 98),
(113, 70, 98),
(114, 72, 98),
(115, 75, 98),
(116, 61, 99),
(117, 62, 99),
(118, 63, 99),
(119, 65, 99),
(120, 66, 99),
(121, 67, 99),
(122, 68, 99),
(123, 69, 99),
(124, 70, 99),
(125, 74, 99),
(126, 75, 99),
(127, 76, 99),
(128, 77, 99),
(129, 34, 100),
(130, 40, 100),
(131, 43, 100),
(132, 45, 100);

-- --------------------------------------------------------

--
-- Table structure for table `product_attributevalues`
--

CREATE TABLE `product_attributevalues` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attributevalue_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_attributevalues`
--

INSERT INTO `product_attributevalues` (`id`, `attributevalue_id`, `product_id`) VALUES
(7, 5, 2),
(8, 17, 2),
(9, 1, 2),
(10, 22, 2),
(11, 38, 5),
(12, 56, 5),
(13, 83, 5),
(14, 86, 5),
(15, 269, 5),
(16, 270, 5),
(17, 271, 5),
(18, 272, 5),
(53, 16, 11),
(54, 1, 11),
(55, 22, 11),
(56, 99, 28),
(57, 122, 29),
(58, 148, 29),
(59, 159, 29),
(60, 166, 29),
(61, 172, 29),
(62, 193, 29),
(63, 201, 29),
(64, 210, 29),
(65, 218, 29),
(66, 122, 31),
(67, 147, 31),
(68, 159, 31),
(69, 193, 31),
(70, 202, 31),
(71, 209, 31),
(72, 218, 31),
(73, 60, 33),
(74, 77, 33),
(75, 82, 33),
(76, 293, 33),
(77, 294, 33),
(78, 295, 33),
(79, 296, 33),
(80, 297, 33),
(81, 298, 33),
(82, 64, 34),
(83, 78, 34),
(84, 82, 34),
(85, 299, 34),
(86, 300, 34),
(87, 301, 34),
(88, 302, 34),
(89, 303, 34),
(90, 304, 34),
(91, 123, 35),
(92, 144, 35),
(93, 157, 35),
(94, 164, 35),
(95, 169, 35),
(96, 188, 35),
(97, 200, 35),
(98, 210, 35),
(99, 218, 35),
(118, 122, 37),
(119, 147, 37),
(120, 159, 37),
(121, 166, 37),
(122, 173, 37),
(123, 193, 37),
(124, 201, 37),
(125, 210, 37),
(126, 218, 37),
(127, 122, 36),
(128, 142, 36),
(129, 156, 36),
(130, 164, 36),
(131, 171, 36),
(132, 193, 36),
(133, 199, 36),
(134, 208, 36),
(135, 217, 36),
(136, 92, 83),
(137, 92, 84),
(138, 92, 85),
(139, 92, 86),
(140, 92, 87),
(141, 92, 88),
(142, 8, 94),
(143, 19, 94),
(144, 1, 94),
(145, 14, 94),
(146, 22, 94),
(147, 305, 94),
(148, 306, 94),
(149, 307, 94),
(150, 308, 94),
(151, 309, 94),
(152, 310, 94),
(153, 122, 95),
(154, 148, 95),
(155, 158, 95),
(156, 164, 95),
(157, 171, 95),
(158, 193, 95),
(159, 202, 95),
(160, 210, 95),
(161, 218, 95),
(171, 119, 96),
(172, 145, 96),
(173, 157, 96),
(174, 164, 96),
(175, 170, 96),
(176, 194, 96),
(177, 201, 96),
(178, 210, 96),
(179, 217, 96),
(180, 122, 97),
(181, 145, 97),
(182, 157, 97),
(183, 163, 97),
(184, 169, 97),
(185, 193, 97),
(186, 201, 97),
(187, 210, 97),
(188, 217, 97),
(189, 140, 98),
(190, 145, 98),
(191, 157, 98),
(192, 164, 98),
(193, 170, 98),
(194, 193, 98),
(195, 201, 98),
(196, 210, 98),
(197, 217, 98),
(198, 139, 99),
(199, 148, 99),
(200, 159, 99),
(201, 166, 99),
(202, 193, 99),
(203, 202, 99),
(204, 210, 99),
(205, 218, 99),
(206, 39, 100),
(207, 52, 100),
(208, 82, 100),
(209, 87, 100),
(210, 311, 100),
(211, 312, 100),
(212, 313, 100),
(213, 314, 100);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `product_id`, `category_id`) VALUES
(3, 2, 7),
(4, 2, 1),
(5, 3, 31),
(6, 3, 18),
(7, 4, 31),
(8, 4, 18),
(9, 5, 11),
(10, 5, 10),
(11, 6, 113),
(12, 6, 112),
(13, 7, 31),
(14, 7, 18),
(15, 8, 31),
(16, 8, 18),
(17, 9, 31),
(18, 9, 18),
(20, 10, 10),
(21, 11, 2),
(22, 11, 1),
(23, 10, 125),
(28, 14, 126),
(29, 14, 112),
(32, 16, 49),
(33, 16, 48),
(36, 18, 113),
(37, 18, 112),
(38, 19, 113),
(39, 19, 112),
(40, 20, 113),
(41, 20, 112),
(42, 21, 113),
(43, 21, 112),
(44, 22, 113),
(45, 22, 112),
(46, 23, 113),
(47, 23, 112),
(48, 24, 113),
(49, 24, 112),
(50, 25, 113),
(51, 25, 112),
(52, 26, 113),
(53, 26, 112),
(54, 27, 113),
(55, 27, 112),
(56, 28, 20),
(57, 28, 18),
(58, 29, 35),
(59, 29, 34),
(60, 30, 43),
(61, 30, 34),
(62, 31, 35),
(63, 31, 34),
(64, 32, 36),
(65, 32, 34),
(66, 33, 12),
(67, 33, 10),
(68, 34, 12),
(69, 34, 10),
(70, 35, 35),
(71, 35, 34),
(72, 36, 35),
(73, 36, 34),
(74, 37, 35),
(75, 37, 34),
(76, 38, 101),
(77, 38, 97),
(78, 39, 36),
(79, 39, 34),
(80, 40, 36),
(81, 40, 34),
(82, 41, 36),
(83, 41, 34),
(84, 42, 36),
(85, 42, 34),
(86, 43, 36),
(87, 43, 34),
(88, 44, 36),
(89, 44, 34),
(90, 45, 36),
(91, 45, 34),
(92, 46, 84),
(93, 46, 76),
(94, 47, 84),
(95, 47, 76),
(96, 48, 36),
(97, 48, 34),
(98, 49, 36),
(99, 49, 34),
(100, 50, 36),
(101, 50, 34),
(102, 51, 36),
(103, 51, 34),
(104, 52, 36),
(105, 52, 34),
(106, 53, 36),
(107, 53, 34),
(108, 54, 36),
(109, 54, 34),
(110, 55, 36),
(111, 55, 34),
(112, 56, 36),
(113, 56, 34),
(114, 57, 36),
(115, 57, 34),
(116, 58, 90),
(117, 58, 88),
(118, 59, 90),
(119, 59, 88),
(120, 60, 90),
(121, 60, 88),
(122, 61, 90),
(123, 61, 88),
(124, 62, 90),
(125, 62, 88),
(126, 63, 47),
(127, 63, 34),
(128, 64, 47),
(129, 64, 34),
(130, 65, 47),
(131, 65, 34),
(132, 66, 47),
(133, 66, 34),
(134, 67, 47),
(135, 67, 34),
(136, 68, 47),
(137, 68, 34),
(138, 69, 47),
(139, 69, 34),
(140, 70, 47),
(141, 70, 34),
(142, 71, 47),
(143, 71, 34),
(144, 72, 47),
(145, 72, 34),
(146, 73, 51),
(147, 73, 48),
(148, 74, 51),
(149, 74, 48),
(150, 75, 51),
(151, 75, 48),
(152, 76, 51),
(153, 76, 48),
(154, 77, 51),
(155, 77, 48),
(156, 78, 51),
(157, 78, 48),
(158, 79, 51),
(159, 79, 48),
(160, 80, 51),
(161, 80, 48),
(162, 81, 52),
(163, 81, 48),
(164, 82, 52),
(165, 82, 48),
(166, 83, 22),
(167, 83, 18),
(168, 84, 22),
(169, 84, 18),
(170, 85, 22),
(171, 85, 18),
(172, 86, 22),
(173, 86, 18),
(174, 87, 22),
(175, 87, 18),
(176, 88, 22),
(177, 88, 18),
(188, 94, 3),
(189, 94, 1),
(190, 95, 35),
(191, 95, 34),
(192, 96, 35),
(193, 96, 34),
(194, 97, 35),
(195, 97, 34),
(196, 98, 35),
(197, 98, 34),
(198, 99, 35),
(199, 99, 34),
(200, 100, 11),
(201, 100, 10);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `full`, `product_id`, `created_at`, `updated_at`) VALUES
(18, 'products/IT9LOyr91aG3EqhRYS2a2UGsu.jpg', 2, '2021-02-07 08:33:59', '2021-02-07 08:33:59'),
(19, 'products/zvdXRPZ0v3gkIhllHaJATb6l1.jpg', 10, '2021-02-07 08:36:16', '2021-02-07 08:36:16'),
(20, 'products/XYQh4V51tYktlI182G3KKKq2d.jpeg', 6, '2021-02-07 08:39:58', '2021-02-07 08:39:58'),
(21, 'products/rB9DUDUMleFvPxmYGFv1vqexe.jpg', 7, '2021-02-07 08:46:58', '2021-02-07 08:46:58'),
(22, 'products/Omw0tSetGG1UqIbZEOy58NNVZ.jpg', 4, '2021-02-07 08:48:51', '2021-02-07 08:48:51'),
(23, 'products/ALgFrxcTCiAn6j2jq2g48hckQ.jpg', 5, '2021-02-07 09:07:21', '2021-02-07 09:07:21'),
(27, 'products/VjQbdXT1ifJCtIuFoEPWW1l1W.jpg', 14, '2021-02-23 05:48:04', '2021-02-23 05:48:04'),
(28, 'products/px2AduPXkekWADP7Go4pj0BsZ.jpg', 14, '2021-02-23 05:48:09', '2021-02-23 05:48:09'),
(29, 'products/TWuWXPB1c5yS224u1nrXqbvgn.jpg', 9, '2021-02-25 08:45:01', '2021-02-25 08:45:01'),
(30, 'products/cPu52in7DSim4HNTPi6Tq3h82.jpg', 8, '2021-02-25 08:46:35', '2021-02-25 08:46:35'),
(31, 'products/cUO99EMDks4OPSJ1Cz037yeTn.jpg', 3, '2021-02-25 08:52:17', '2021-02-25 08:52:17'),
(33, 'products/u6awGLaCgpUUTBhCIhGmtF53Y.jpg', 16, '2021-03-02 22:50:00', '2021-03-02 22:50:00'),
(34, 'products/FZnESS7JvomlmriVrB5fKK2gq.jpg', 18, '2021-03-07 04:38:27', '2021-03-07 04:38:27'),
(35, 'products/jWMoGsWNY8AH1rzFUNsfCvFki.jpg', 18, '2021-03-07 04:38:27', '2021-03-07 04:38:27'),
(36, 'products/sN2QKRP6vGGYDa8KEYIDE7WsV.jpg', 19, '2021-03-07 04:59:20', '2021-03-07 04:59:20'),
(37, 'products/zhOqq07t52owGKjsuNlAnj9je.jpg', 20, '2021-03-07 05:01:26', '2021-03-07 05:01:26'),
(38, 'products/SPdfPaMSjNWtfe2vSA1q2YKxG.jpg', 21, '2021-03-07 05:03:50', '2021-03-07 05:03:50'),
(39, 'products/cHsFjJzXby2INoTvt7BmJHZEZ.jpg', 22, '2021-03-07 05:05:06', '2021-03-07 05:05:06'),
(40, 'products/Qi6ccOJjdFNPE4iMBDWdYaHEI.jpg', 23, '2021-03-07 05:06:34', '2021-03-07 05:06:34'),
(41, 'products/jTS0MKL3odOYx7pyCNEVlXhyo.jpg', 24, '2021-03-07 05:08:40', '2021-03-07 05:08:40'),
(42, 'products/yB3TAZpjA7pdkahHF5zXDzqyW.jpg', 25, '2021-03-07 05:11:11', '2021-03-07 05:11:11'),
(43, 'products/iMRvLVo1UNNa4sPE3rIWkdnY9.jpg', 26, '2021-03-07 05:15:00', '2021-03-07 05:15:00'),
(44, 'products/0UVpxvgA5Hm8hC1eM47jlOCnx.jpg', 27, '2021-03-07 05:16:35', '2021-03-07 05:16:35'),
(46, 'products/9j9KjsBgEOP7sXlMHZHK4BGEh.jpg', 28, '2021-03-18 01:04:54', '2021-03-18 01:04:54'),
(47, 'products/keJ5B29ZoWeO8NK3HxuB0ceQq.jpg', 28, '2021-03-18 01:04:54', '2021-03-18 01:04:54'),
(48, 'products/YzU8upn7HGUbr1YgMZARbkRCU.jpg', 29, '2021-03-26 08:24:28', '2021-03-26 08:24:28'),
(49, 'products/oEzPXE7uMjlfcgTQZn1OXJUkJ.jpg', 30, '2021-04-01 09:04:03', '2021-04-01 09:04:03'),
(50, 'products/dz9lRngH8VxViChLr7xyZwyIo.jpg', 31, '2021-04-01 09:13:21', '2021-04-01 09:13:21'),
(51, 'products/CyIIqtOaedXdmfGABof6SAUyo.jpg', 33, '2021-04-07 22:12:08', '2021-04-07 22:12:08'),
(52, 'products/rY1b963LE5Hm0vQBuk4x94ndA.jpg', 34, '2021-04-07 22:41:14', '2021-04-07 22:41:14'),
(53, 'products/U5ePWGucMOeNbPVSZePsIaTVX.', 35, '2021-04-07 22:57:22', '2021-04-07 22:57:22'),
(54, 'products/1Mu319oQxV6npCSsXkOHZg15u.jpg', 37, '2021-04-08 06:56:31', '2021-04-08 06:56:31'),
(55, 'products/Ku99ArB6BIr5w4RllmpVBNWGD.jpg', 36, '2021-04-08 06:57:20', '2021-04-08 06:57:20'),
(56, 'products/qGFjPu74VeiFRTtm63KaJMyTG.jpg', 38, '2021-04-08 08:51:58', '2021-04-08 08:51:58'),
(57, 'products/h7sj0BJIX9cBcV470JjSJYQAm.jpg', 39, '2021-04-08 09:19:03', '2021-04-08 09:19:03'),
(58, 'products/d7zn3rVNhQiDMfcaWejXPYLDS.jpg', 40, '2021-04-08 09:21:10', '2021-04-08 09:21:10'),
(59, 'products/Nkx1ozivY98AHr22OM35roBOO.jpg', 41, '2021-04-08 09:24:05', '2021-04-08 09:24:05'),
(60, 'products/3VLONGdnEShXNOsEVzahsZbHA.jpg', 42, '2021-04-08 09:26:32', '2021-04-08 09:26:32'),
(61, 'products/uD3NLbjMOMUcoJhTZn1OQGOCt.jpg', 43, '2021-04-08 09:28:35', '2021-04-08 09:28:35'),
(62, 'products/RzmoTojmCCr8F96N0K93gerVZ.jpg', 44, '2021-04-08 09:30:51', '2021-04-08 09:30:51'),
(63, 'products/yPsBm0i2GbDW0qc4xnZ2CaihX.jpg', 45, '2021-04-09 00:59:08', '2021-04-09 00:59:08'),
(64, 'products/5iITYiUYYsVwmCS81uoUqQJek.jpg', 46, '2021-04-09 01:02:22', '2021-04-09 01:02:22'),
(65, 'products/OlvKigsKS6ARsKvHxNEIC4Oaa.jpg', 47, '2021-04-09 01:05:01', '2021-04-09 01:05:01'),
(66, 'products/yaGpN8Hf3Oj7bEXoQiyR2z59o.jpg', 48, '2021-04-09 01:10:19', '2021-04-09 01:10:19'),
(67, 'products/xr8VRhsL3NMNFw79oQMtEFZrO.jpg', 49, '2021-04-09 01:12:22', '2021-04-09 01:12:22'),
(68, 'products/Tmr250PPaVdOYd103iKdZP3HF.jpg', 50, '2021-04-09 01:14:42', '2021-04-09 01:14:42'),
(69, 'products/7iYn1P48HQcFxgglIVQdyBMVg.jpg', 51, '2021-04-09 01:16:56', '2021-04-09 01:16:56'),
(70, 'products/Y0fcLLiGwWbDi2z2MlH8WCoRF.jpg', 52, '2021-04-09 01:18:52', '2021-04-09 01:18:52'),
(72, 'products/e6wk3BbPieXXuEj5P5wUUnYIY.jpg', 53, '2021-04-09 01:21:46', '2021-04-09 01:21:46'),
(73, 'products/b3b1vpP4r6dQVooOmxWPzvU2o.jpg', 54, '2021-04-09 01:23:55', '2021-04-09 01:23:55'),
(74, 'products/Hqkm8VRJ5ta2qgpb6qKaI4Hrw.jpg', 55, '2021-04-09 01:26:21', '2021-04-09 01:26:21'),
(75, 'products/BnQ0VebCe2GmMBObGfMOxNrmr.jpg', 56, '2021-04-09 01:28:41', '2021-04-09 01:28:41'),
(76, 'products/6VguNVl6RD13RGGw9V7zNHoq2.jpg', 57, '2021-04-09 01:30:53', '2021-04-09 01:30:53'),
(77, 'products/pvac6pzWbSqvcmEWl6nFOPa5M.jpg', 58, '2021-04-09 01:34:31', '2021-04-09 01:34:31'),
(78, 'products/JhectSALqDPm5plaaFscqw0fU.jpg', 59, '2021-04-09 01:36:15', '2021-04-09 01:36:15'),
(79, 'products/3mApuCasr4oLHYlMiY3BNt2wR.jpg', 60, '2021-04-09 01:38:51', '2021-04-09 01:38:51'),
(80, 'products/PMCMjVTEwDjHJrelPZOkrhGvH.jpg', 61, '2021-04-09 01:40:55', '2021-04-09 01:40:55'),
(81, 'products/m9Xn4fqqMj8wuLAnS7DM6lOHA.jpg', 62, '2021-04-09 01:43:01', '2021-04-09 01:43:01'),
(82, 'products/nMGBmq7GzmqEfoqkWRJHOYMI5.jpg', 63, '2021-04-09 01:59:16', '2021-04-09 01:59:16'),
(83, 'products/gpQcx8yCzAowin1WM1ydPu7sb.jpg', 64, '2021-04-09 02:01:26', '2021-04-09 02:01:26'),
(84, 'products/joVUXpi1SWl1VOnYXyKe8AGUE.jpg', 65, '2021-04-09 02:03:11', '2021-04-09 02:03:11'),
(85, 'products/iAcEU044ZZ3gcHTYLkYkiLVT2.jpg', 66, '2021-04-09 02:04:51', '2021-04-09 02:04:51'),
(86, 'products/n6Ob9TRiA5Os7Lj8FaI3qdBCG.jpg', 67, '2021-04-09 02:07:01', '2021-04-09 02:07:01'),
(87, 'products/bNRhf9pDWbVggGeTgz5uIHsKK.jpg', 68, '2021-04-09 02:09:22', '2021-04-09 02:09:22'),
(88, 'products/pdXZZ56wu4KknWLDvrUt1n3Lf.jpg', 69, '2021-04-09 02:11:26', '2021-04-09 02:11:26'),
(89, 'products/GuKgrPZ0ZCutbpIIf7J8cczK8.jpg', 70, '2021-04-09 02:14:06', '2021-04-09 02:14:06'),
(90, 'products/vZWX6Smr84fT2ATNg4bxvMqf9.jpg', 71, '2021-04-09 02:17:23', '2021-04-09 02:17:23'),
(91, 'products/6gvMW6VzYgsGAeWjBx0uYSC1l.jpg', 72, '2021-04-09 02:19:18', '2021-04-09 02:19:18'),
(92, 'products/czoLQcazXiD8plIxLVLN0AI5m.jpg', 73, '2021-04-16 09:33:39', '2021-04-16 09:33:39'),
(93, 'products/nxbLQ2JFkqIOzIvw1MbTmrwsy.jpg', 74, '2021-04-16 09:36:37', '2021-04-16 09:36:37'),
(94, 'products/bMn7uomXf54wW35rDsfVw72yQ.jpg', 75, '2021-04-16 09:39:57', '2021-04-16 09:39:57'),
(96, 'products/tihHQqr01gUAxVd7juuMpKHYH.jpeg', 76, '2021-04-16 09:43:23', '2021-04-16 09:43:23'),
(97, 'products/VmNsll9PKovcdLpcFuZlDhf3J.jpg', 77, '2021-04-16 09:47:58', '2021-04-16 09:47:58'),
(98, 'products/jM1JjErH5Ar766F0U7WOIbbKK.jpg', 78, '2021-04-16 09:51:13', '2021-04-16 09:51:13'),
(99, 'products/vdieL8plRZFQY2JXlms0QMd4J.jpg', 79, '2021-04-16 09:54:13', '2021-04-16 09:54:13'),
(100, 'products/9w8U04T6Zk51Ddhj5hQu2Czwa.jpg', 80, '2021-04-16 09:56:17', '2021-04-16 09:56:17'),
(101, 'products/gFYNto0NqIatbrcToDjAKYHen.jpeg', 81, '2021-04-16 09:57:50', '2021-04-16 09:57:50'),
(102, 'products/u7oSoQPQmNBLAbtmpwRVokHza.jpg', 82, '2021-04-16 10:00:52', '2021-04-16 10:00:52'),
(103, 'products/dRFRRvoxV4pDxjiRKtFElsWXm.jpg', 83, '2021-04-17 09:48:40', '2021-04-17 09:48:40'),
(104, 'products/3UExi8oKL4gmHm8HXDcqfdeJc.jpg', 84, '2021-04-17 09:51:23', '2021-04-17 09:51:23'),
(105, 'products/XTiVfpENDdlz2CkvSvclKqjJz.jpg', 85, '2021-04-17 09:53:10', '2021-04-17 09:53:10'),
(106, 'products/qFwy6QvDDztCJRiefJoBo9POW.jpg', 86, '2021-04-17 09:55:37', '2021-04-17 09:55:37'),
(107, 'products/uyumLjMJZ6CRKSQCmOOJ1vFrw.jpg', 87, '2021-04-17 09:58:02', '2021-04-17 09:58:02'),
(108, 'products/9U64D0d3MSAQUOd1qEVEusbBr.jpg', 88, '2021-04-17 10:00:11', '2021-04-17 10:00:11'),
(115, 'products/y8tvspN59aLBU3zkOi8NKLsZC.png', 94, '2021-04-20 21:59:02', '2021-04-20 21:59:02'),
(116, 'products/P7qMDTliv7kLfnsmY3BnafhQW.jpg', 95, '2021-04-20 22:10:27', '2021-04-20 22:10:27'),
(117, 'products/I99hMV0URx97pR5bSmeL3nAsn.jpg', 96, '2021-04-20 22:14:27', '2021-04-20 22:14:27'),
(118, 'products/18Xi8uCdX9aGxp8sCT17PAGQM.jpg', 96, '2021-04-20 22:14:27', '2021-04-20 22:14:27'),
(119, 'products/eWlhQwbV5NRhRWUzxkwho4Sio.jpg', 97, '2021-04-20 22:18:17', '2021-04-20 22:18:17'),
(120, 'products/EtOEtEyFfMfK05W0PWy8vdhT4.jpg', 97, '2021-04-20 22:18:17', '2021-04-20 22:18:17'),
(121, 'products/nbCj6QaWM8WXyd9JLYAhq8uct.jpg', 98, '2021-04-20 22:21:30', '2021-04-20 22:21:30'),
(122, 'products/C1fIlzL3Lj0EH1cwVrJfiF6od.jpg', 98, '2021-04-20 22:21:30', '2021-04-20 22:21:30'),
(123, 'products/dLe0XAou0j1uEc0rOTJGQ8ecb.jpg', 99, '2021-07-20 19:32:30', '2021-07-20 19:32:30'),
(124, 'products/r0sbnDtgnwWX01Y2wqz6kUPBZ.jpg', 99, '2021-07-20 19:32:30', '2021-07-20 19:32:30');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `overview` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `photo`, `job_title`, `overview`, `city`, `province`, `country`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '0.78793700 1615096411.jpg', 'Pramod', 'TO be a part of an orgg', 'Kathmandu', '1', '9849551992', 28, '2021-02-22 22:00:43', '2021-03-06 21:53:31'),
(2, '0.20132800 1614061097.jpg', 'Pradhan', 'tis is overvie', 'nepal', '1', '984577555', 9, '2021-02-22 22:18:05', '2021-02-22 22:18:17');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `signup_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reportnames`
--

CREATE TABLE `reportnames` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reportnames`
--

INSERT INTO `reportnames` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'This is illegal / fraudulent', '2021-02-06 05:35:17', '2021-02-06 05:35:17'),
(2, 'This ad is spam', '2021-02-06 05:35:18', '2021-02-06 05:35:18'),
(3, 'This ad is a duplicate', '2021-02-06 05:35:18', '2021-02-06 05:35:18'),
(4, 'This ad is in wrong category', '2021-02-06 05:35:18', '2021-02-06 05:35:18'),
(5, 'This ad goes againt posting rules', '2021-02-06 05:35:19', '2021-02-06 05:35:19');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reportname_id` int(10) UNSIGNED DEFAULT NULL,
  `auth_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `body`, `url`, `reportname_id`, `auth_id`, `user_id`, `product_id`, `status`, `created_at`, `updated_at`) VALUES
(3, NULL, 'https://merooffer.com/ad/the-lunch-delight', NULL, 28, 30, 14, 0, '2021-06-08 23:21:31', '2021-06-08 23:21:31'),
(4, NULL, 'https://merooffer.com/ad/the-lunch-delight', 2, 28, 30, 14, 0, '2021-06-08 23:33:04', '2021-06-08 23:33:04'),
(5, NULL, 'https://merooffer.com/ad/the-lunch-delight', 1, 28, 30, 14, 0, '2021-07-06 08:11:51', '2021-07-06 08:11:51'),
(6, NULL, 'https://merooffer.com/ad/limited-edition-summer-hoodies', 2, 28, 41, 28, 0, '2021-07-18 07:32:52', '2021-07-18 07:32:52'),
(7, NULL, 'https://merooffer.com/ad/the-lunch-delight', 2, 68, 22, 14, 0, '2021-08-26 03:21:33', '2021-08-26 03:21:33');

-- --------------------------------------------------------

--
-- Table structure for table `resumes`
--

CREATE TABLE `resumes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cv` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `job_id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seller_id` bigint(20) UNSIGNED NOT NULL,
  `commentator_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salarytypes`
--

CREATE TABLE `salarytypes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `salarytypes`
--

INSERT INTO `salarytypes` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Negotiable', 1, '2021-02-06 05:35:06', '2021-02-06 05:35:06'),
(2, 'Monthly', 1, '2021-02-06 05:35:07', '2021-02-06 05:35:07'),
(3, 'Daily', 1, '2021-02-06 05:35:07', '2021-02-06 05:35:07'),
(4, 'Weekly', 1, '2021-02-06 05:35:08', '2021-02-06 05:35:08'),
(5, 'Hourly', 1, '2021-02-06 05:35:08', '2021-02-06 05:35:08'),
(6, 'Yearly', 1, '2021-02-06 05:35:09', '2021-02-06 05:35:09');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 'site_name', 'E-Commerce Application', '2021-02-06 05:37:07', '2021-02-06 05:37:07'),
(2, 'site_title', 'Mero Offer: Nepal\'s Free Marketplace. Search project,job,realestate & more.', '2021-02-06 05:37:08', '2021-02-06 05:37:08'),
(3, 'email', 'hr@merooffer.com', '2021-02-06 05:37:08', '2021-02-06 05:37:08'),
(4, 'currency_code', 'GBP', '2021-02-06 05:37:08', '2021-02-06 05:37:08'),
(5, 'currency_symbol', '£', '2021-02-06 05:37:08', '2021-02-06 05:37:08'),
(6, 'location', 'Gwarko, Lalitpur', '2021-02-06 05:37:08', '2021-02-21 08:26:57'),
(7, 'company_name', 'Skyfall Technologies Pvt. Ltd.', '2021-02-06 05:37:09', '2021-02-06 05:37:09'),
(8, 'error_image', 'img/dummy.jpg', '2021-02-06 05:37:09', '2021-02-06 05:37:09'),
(9, 'description', 'Merooffer.com is the next generation of free online classifieds. It provides a simple solution to the complications involved in selling, buying, trading, discussing and meeting people near you.', '2021-02-06 05:37:09', '2021-02-06 05:37:09'),
(10, 'phone', '+977-9849551992', '2021-02-06 05:37:09', '2021-09-02 05:03:59'),
(11, 'logo', 'img/logo.png', '2021-02-06 05:37:09', '2021-02-06 05:37:09'),
(12, 'favicon', '', '2021-02-06 05:37:09', '2021-02-06 05:37:09'),
(13, 'footer_text', '', '2021-02-06 05:37:09', '2021-02-06 05:37:09'),
(14, 'seo_meta_title', '', '2021-02-06 05:37:10', '2021-02-06 05:37:10'),
(15, 'seo_meta_description', '', '2021-02-06 05:37:10', '2021-02-06 05:37:10'),
(16, 'facebook', 'www.facebook.com/merooffer1', '2021-02-06 05:37:10', '2021-07-20 19:22:53'),
(17, 'twitter', NULL, '2021-02-06 05:37:10', '2021-07-20 19:22:53'),
(18, 'instagram', NULL, '2021-02-06 05:37:11', '2021-07-20 19:22:53'),
(19, 'linkedin', NULL, '2021-02-06 05:37:11', '2021-07-20 19:22:53'),
(20, 'google_analytics', '', '2021-02-06 05:37:11', '2021-02-06 05:37:11'),
(21, 'facebook_pixels', '', '2021-02-06 05:37:11', '2021-02-06 05:37:11'),
(22, 'stripe_payment_method', '', '2021-02-06 05:37:12', '2021-02-06 05:37:12'),
(23, 'stripe_key', '', '2021-02-06 05:37:12', '2021-02-06 05:37:12'),
(24, 'stripe_secret_key', '', '2021-02-06 05:37:13', '2021-02-06 05:37:13'),
(25, 'paypal_payment_method', '', '2021-02-06 05:37:13', '2021-02-06 05:37:13'),
(26, 'paypal_client_id', '', '2021-02-06 05:37:13', '2021-02-06 05:37:13'),
(27, 'paypal_secret_id', '', '2021-02-06 05:37:13', '2021-02-06 05:37:13');

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `skill` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `skill`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'php', 'php', '2021-02-06 07:26:06', '2021-02-06 07:26:06'),
(2, ' laravel', 'laravel', '2021-02-06 07:26:06', '2021-02-06 07:26:06'),
(3, ' javascript', 'javascript', '2021-02-06 07:26:06', '2021-02-06 07:26:06'),
(4, 'finance', 'finance', '2021-02-06 07:27:33', '2021-02-06 07:27:33'),
(5, ' budgeting', 'budgeting', '2021-02-06 07:27:33', '2021-02-06 07:27:33'),
(6, ' manage cash-flow', 'manage-cash-flow', '2021-02-06 07:27:33', '2021-02-06 07:27:33'),
(7, 'stock mgmt', 'stock-mgmt', '2021-02-06 07:29:23', '2021-02-06 07:29:23'),
(8, ' distribution', 'distribution', '2021-02-06 07:29:23', '2021-02-06 07:29:23'),
(9, '', '', '2021-02-06 07:29:23', '2021-02-06 07:29:23'),
(10, 'communication', 'communication', '2021-02-06 07:32:00', '2021-02-06 07:32:00'),
(11, ' reporting', 'reporting', '2021-02-06 07:32:00', '2021-02-06 07:32:00'),
(12, ' financial forcasting', 'financial-forcasting', '2021-02-06 07:32:00', '2021-02-06 07:32:00'),
(13, ' account management', 'account-management', '2021-02-06 07:32:00', '2021-02-06 07:32:00'),
(14, 'survey', 'survey', '2021-02-06 07:34:36', '2021-02-06 07:34:36'),
(15, ' accounting', 'accounting', '2021-02-06 08:04:15', '2021-02-06 08:04:15'),
(16, ' taxation', 'taxation', '2021-02-06 08:04:15', '2021-02-06 08:04:15'),
(17, ' analytical', 'analytical', '2021-02-06 08:04:15', '2021-02-06 08:04:15'),
(18, ' microsoft office suite', 'microsoft-office-suite', '2021-02-06 08:04:15', '2021-02-06 08:04:15'),
(19, 'javascript', 'javascript-1', '2021-02-06 22:17:43', '2021-02-06 22:17:43'),
(20, ' php', 'php-1', '2021-02-06 22:17:43', '2021-02-06 22:17:43'),
(21, 'Marketing', 'marketing', '2021-02-23 05:03:08', '2021-02-23 05:03:08'),
(22, ' management', 'management', '2021-02-23 05:03:08', '2021-02-23 05:03:08'),
(23, ' communication', 'communication-1', '2021-02-23 05:03:08', '2021-02-23 05:03:08'),
(24, ' customer relationship management', 'customer-relationship-management', '2021-02-23 05:03:08', '2021-02-23 05:03:08'),
(25, 'construction', 'construction', '2021-03-06 22:25:08', '2021-03-06 22:25:08'),
(26, 'bakery', 'bakery', '2021-03-16 01:44:56', '2021-03-16 01:44:56'),
(27, ' teamwork', 'teamwork', '2021-03-16 01:44:56', '2021-03-16 01:44:56'),
(28, 'infection control', 'infection-control', '2021-04-07 01:05:27', '2021-04-07 01:05:27'),
(29, ' patient services', 'patient-services', '2021-04-07 01:05:27', '2021-04-07 01:05:27');

-- --------------------------------------------------------

--
-- Table structure for table `skill_user`
--

CREATE TABLE `skill_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `skill_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `skill_user`
--

INSERT INTO `skill_user` (`id`, `user_id`, `skill_id`, `created_at`, `updated_at`) VALUES
(1, 28, 13, '2021-02-06 10:21:51', '2021-02-06 10:21:51'),
(3, 28, 15, '2021-03-06 22:50:38', '2021-03-06 22:50:38');

-- --------------------------------------------------------

--
-- Table structure for table `social_identities`
--

CREATE TABLE `social_identities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `provider_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `social_identities`
--

INSERT INTO `social_identities` (`id`, `user_id`, `provider_name`, `provider_id`, `created_at`, `updated_at`) VALUES
(3, 8, 'facebook', '3634179079971709', '2021-02-06 05:34:48', '2021-02-06 05:34:48'),
(4, 9, 'google', '106110588256003720257', '2021-02-06 05:34:49', '2021-02-06 05:34:49'),
(5, 10, 'google', '113400656993196451881', '2021-02-06 05:34:49', '2021-02-06 05:34:49'),
(6, 11, 'google', '115601249912182821437', '2021-02-06 05:34:49', '2021-02-06 05:34:49'),
(7, 12, 'google', '103203790273154611143', '2021-02-06 05:34:49', '2021-02-06 05:34:49'),
(8, 13, 'google', '116423431068988074889', '2021-02-06 05:34:50', '2021-02-06 05:34:50'),
(9, 14, 'google', '108559602430157403814', '2021-02-06 05:34:50', '2021-02-06 05:34:50'),
(10, 15, 'google', '108977045803289557691', '2021-02-06 05:34:50', '2021-02-06 05:34:50'),
(11, 16, 'google', '110463831025115387920', '2021-02-06 05:34:51', '2021-02-06 05:34:51'),
(12, 18, 'google', '117751946393128085798', '2021-02-06 05:34:51', '2021-02-06 05:34:51'),
(13, 20, 'google', '107716984682218308664', '2021-02-06 05:34:51', '2021-02-06 05:34:51'),
(14, 21, 'google', '102338328564744304768', '2021-02-06 05:34:51', '2021-02-06 05:34:51'),
(15, 23, 'facebook', '1040680776447804', '2021-02-06 05:34:52', '2021-02-06 05:34:52'),
(16, 24, 'facebook', '10223350192958437', '2021-02-06 05:34:52', '2021-02-06 05:34:52'),
(17, 25, 'facebook', '3531032020315467', '2021-02-06 05:34:53', '2021-02-06 05:34:53'),
(18, 26, 'facebook', '4070431252984119', '2021-02-06 05:34:53', '2021-02-06 05:34:53'),
(19, 28, 'facebook', '3016258721744330', '2021-02-06 03:54:46', '2021-02-06 03:54:46'),
(20, 28, 'google', '101716233686111298599', '2021-02-06 04:14:35', '2021-02-06 04:14:35'),
(21, 29, 'google', '113168116143884837311', '2021-02-12 09:20:35', '2021-02-12 09:20:35'),
(22, 31, 'google', '107422100803353758650', '2021-02-24 10:01:54', '2021-02-24 10:01:54'),
(23, 32, 'google', '102081220985397986974', '2021-02-27 22:12:20', '2021-02-27 22:12:20'),
(24, 33, 'google', '113886561941337192583', '2021-02-28 22:21:09', '2021-02-28 22:21:09'),
(25, 34, 'google', '114451017628170168573', '2021-02-28 22:40:32', '2021-02-28 22:40:32'),
(26, 35, 'google', '113967980971856737600', '2021-03-01 21:43:03', '2021-03-01 21:43:03'),
(27, 36, 'google', '112296382365536389652', '2021-03-08 22:35:58', '2021-03-08 22:35:58'),
(28, 37, 'facebook', '10218978995082221', '2021-03-10 01:12:33', '2021-03-10 01:12:33'),
(29, 40, 'facebook', '1404359139917112', '2021-03-17 03:52:45', '2021-03-17 03:52:45'),
(30, 42, 'facebook', '5217814391622396', '2021-03-26 08:04:00', '2021-03-26 08:04:00'),
(31, 44, 'facebook', '926436281451924', '2021-04-07 19:00:49', '2021-04-07 19:00:49'),
(32, 45, 'google', '110138272355094583545', '2021-04-07 19:01:22', '2021-04-07 19:01:22'),
(34, 47, 'google', '114475823859445972733', '2021-04-09 00:06:31', '2021-04-09 00:06:31'),
(35, 48, 'google', '116555309592570406828', '2021-04-09 01:05:56', '2021-04-09 01:05:56'),
(36, 50, 'facebook', '2560808370891941', '2021-04-10 00:04:58', '2021-04-10 00:04:58'),
(37, 51, 'google', '108626844866329447315', '2021-04-11 23:05:59', '2021-04-11 23:05:59'),
(38, 53, 'google', '102134834652749603071', '2021-04-17 10:00:42', '2021-04-17 10:00:42'),
(39, 54, 'google', '117605238209619785648', '2021-04-20 02:33:00', '2021-04-20 02:33:00'),
(40, 55, 'facebook', '3051483298260572', '2021-04-21 07:14:14', '2021-04-21 07:14:14'),
(41, 56, 'google', '111951994423028902463', '2021-05-31 04:59:34', '2021-05-31 04:59:34'),
(42, 57, 'google', '104430329829339646942', '2021-06-21 01:30:11', '2021-06-21 01:30:11'),
(43, 58, 'google', '100951654245719109122', '2021-07-06 18:44:49', '2021-07-06 18:44:49'),
(44, 59, 'google', '115376388650458997849', '2021-07-09 22:11:03', '2021-07-09 22:11:03'),
(45, 60, 'facebook', '4253181304720897', '2021-07-09 22:27:35', '2021-07-09 22:27:35'),
(46, 61, 'facebook', '4058867487522185', '2021-07-11 03:07:50', '2021-07-11 03:07:50'),
(47, 62, 'google', '105438965259324677980', '2021-07-11 03:09:10', '2021-07-11 03:09:10'),
(48, 64, 'facebook', '4528894240509999', '2021-07-27 20:30:29', '2021-07-27 20:30:29'),
(49, 66, 'facebook', '151312320403149', '2021-08-11 07:33:26', '2021-08-11 07:33:26'),
(50, 67, 'google', '104506669197478562984', '2021-08-21 05:21:45', '2021-08-21 05:21:45'),
(51, 68, 'google', '116235887977929381092', '2021-08-26 03:18:31', '2021-08-26 03:18:31'),
(52, 69, 'google', '100763478918979951770', '2021-08-28 23:31:31', '2021-08-28 23:31:31'),
(53, 70, 'google', '101501250803356222791', '2021-09-03 23:53:52', '2021-09-03 23:53:52'),
(54, 71, 'google', '112306680514546279030', '2021-09-04 00:00:08', '2021-09-04 00:00:08');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE `trainings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `org_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completion_year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `socket_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `online` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `activation_key` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cell_activated` tinyint(1) NOT NULL DEFAULT 0,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_email` tinyint(1) NOT NULL DEFAULT 0,
  `show_mobile` tinyint(1) NOT NULL DEFAULT 0,
  `show_phone` tinyint(1) NOT NULL DEFAULT 0,
  `role` tinyint(4) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login_at` datetime DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `mobile`, `phone`, `email`, `email_verified_at`, `password`, `address`, `socket_id`, `online`, `activation_key`, `cell_activated`, `image`, `show_email`, `show_mobile`, `show_phone`, `role`, `slug`, `ip`, `last_login_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Delta Tech Pvt. Ltd.', '9804067116', '021531317', 'career@deltatechnepal.com', '2020-10-30 09:42:20', '$2y$10$1myhvxKNr9sFEgRULv1wge6Eb/I1LTUYxa33lGQcs6f9Na/2rk7b6', NULL, NULL, 'N', 'DMb1bI', 1, NULL, 0, 0, 0, NULL, 'delta-tech-pvt-ltd', '127.0.0.1', NULL, NULL, '2021-02-06 05:34:38', '2021-02-06 05:34:38'),
(3, 'Joseph Karki', '9869374448', NULL, 'josephkarki8@gmail.com', '2020-10-30 09:42:20', '$2y$10$/JJtxvlmEUNmJnSVdecEz.Kb.EN25VPRHYIkHZNzJxLJj0vtSdHe.', NULL, NULL, 'N', 'dENw47', 1, NULL, 0, 0, 0, NULL, 'joseph-karki', '127.0.0.1', NULL, NULL, '2021-02-06 05:34:38', '2021-02-06 05:34:38'),
(4, 'Anish Raj', '+9779849441738', NULL, 'anish9849@gmail.com', '2020-10-30 09:42:20', '$2y$10$colxYcaViqdJ9ZJit8PKaObrueB47lja16a2FJ6k2B6IFcjtERbNq', NULL, NULL, 'N', 'eshklD', 1, NULL, 0, 0, 0, NULL, 'anish-raj', '127.0.0.1', NULL, NULL, '2021-02-06 05:34:40', '2021-02-06 05:34:40'),
(5, 'Kanchen Kattel', '9804058545', NULL, 'kanchan_kattel@yahoo.com', '2020-10-30 09:42:20', '$2y$10$RTXDRgolqfZMvQtmxbSlwuIEauG4dVX7DNoBSZe95xvbRbIuxmi4K', NULL, NULL, 'N', 'UdxY2a', 1, NULL, 0, 0, 0, NULL, 'kanchen-kattel', '127.0.0.1', NULL, NULL, '2021-02-06 05:34:40', '2021-02-06 05:34:40'),
(6, 'Saurav Shrestha', '9860305485', NULL, 'sauravshrestha13@gmail.com', '2020-10-30 09:42:20', '$2y$10$QRFQ7WCBSRmc1.CiBxQWFuPdhy.syLC0dlMOAQjDpDmSTtf.cxx5e', NULL, NULL, 'N', 'vzoRXz', 1, NULL, 0, 0, 0, NULL, 'saurav-shrestha', '127.0.0.1', NULL, NULL, '2021-02-06 05:34:41', '2021-02-06 05:34:41'),
(7, 'i-creation', '9843274641', NULL, 'apply@icreationtech.com', '2020-10-30 09:42:20', '$2y$10$A8VRUmU6qgMimGfkBvqx9eG009/PT/qk.FjN6R08EG/j3dQL8VtUa', NULL, NULL, 'N', 'lrthLA', 1, NULL, 0, 0, 0, NULL, 'i-creation', '127.0.0.1', NULL, NULL, '2021-02-06 05:34:41', '2021-02-06 05:34:41'),
(8, 'Pujan Shrestha', '9849824952', NULL, 'world_history18@yahoo.com', '2020-10-30 09:42:20', '$2y$10$Bbpkx6iL2It9n/Hp7UObhOKjNdZ6VkJeJIycjzOcr5eKQRijmHyA6', NULL, NULL, 'N', 'wUj26a', 1, NULL, 0, 0, 0, NULL, 'pujan-shrestha', '113.199.242.52', NULL, 'TY5IaEMaTGZ55L2khbgu3nyHYkbQP9iXh0x3LJTguA9YEHvLV9OqNsVRzgMt', '2021-02-06 05:34:42', '2021-02-06 05:34:42'),
(9, 'Pradhan Papers', '9857044022', NULL, 'pradhanpapers2020@gmail.com', '2020-10-30 09:42:20', '$2y$10$nKulyhSTv.DL7QCGKtYs0u5LgGsfDtdOn5T10VRbCm/z04sFOO.H.', NULL, NULL, 'N', 'YkeSbj', 1, NULL, 0, 0, 0, NULL, 'pradhan-papers', '113.199.242.52', NULL, 'zBKeipmU9Kokw0g3IPivh639ZKzbgG3UmghjmTolirU65dBXwN09dDQBQRrv', '2021-02-06 05:34:42', '2021-02-06 05:34:42'),
(10, 'Kshitiz Pradhan', NULL, NULL, 'kshitizpradhan10@gmail.com', '2020-10-30 09:42:20', '$2y$10$4atCVDdroULBQ2Pd5nt5D.xY5RWgObkWXWreOAPpVwpLdw2.R8W2a', NULL, NULL, 'N', 'Kp51Gz', 1, NULL, 0, 0, 0, NULL, 'kshitiz-pradhan', '27.34.48.98', NULL, 'GFbk4i6Mj4nw7QnHTnDAwiNXnAEs6NWjKUofAtKf4VCDykhFyrEdh7c7DmLh', '2021-02-06 05:34:42', '2021-02-06 05:34:42'),
(11, 'mother care', NULL, NULL, 'mcare967@gmail.com', '2020-10-30 09:42:20', '$2y$10$16gGT88zWrht/BSCU38HxeuHQkqU/Bm7rUr5O8bQ0hIDAbZJaKJl.', NULL, NULL, 'N', 'dpJ0ck', 1, NULL, 0, 0, 0, NULL, 'mother-care', '110.34.15.137', NULL, 'YbBgNbLIHAurCU4ps1lwHREQqhwiPwl7XvoA5TQWXvSq3ZNO15zpyytiwXt5', '2021-02-06 05:34:42', '2021-02-06 05:34:42'),
(12, 'Raju Lamichhane', '9861977022', NULL, 'rajulamichhane204@gmail.com', '2020-10-30 09:42:20', '$2y$10$8M/NMZis6r3S5A/mbkIZ.es3lcqk/rojiyLcX4jdESWJ3deMsJWki', NULL, NULL, 'N', 'TgdbMs', 1, 'users/rf0NdQXPT3LvRqKcS16Xka5pt.JPG', 0, 0, 0, NULL, 'raju-lamichhane', '27.34.20.76', NULL, 'XFWAGAy7eOriZSVnNDh3p50QqatW24rR5jEoGKQOz971LhgH8FRlAxudMX9A', '2021-02-06 05:34:43', '2021-02-06 05:34:43'),
(13, 'Buenostore.np', '9849575165', NULL, 'bidhankhawas23@gmail.com', '2020-10-30 09:42:20', '$2y$10$AWHJxWvsuG.374dIzf5lSOZHsD7oP2P5wFt1soaPzCkDrrWbEEYVO', NULL, NULL, 'N', 'dPt30h', 1, 'users/k83GVfEDOWib61DwNbfl6urbA.jpeg', 0, 0, 0, NULL, 'buenostorenp', '27.34.20.76', NULL, 'rl8Ub0NeEDILHFopVs6oKaZukAzrBqrSCWArmw2WXBol5fHFAg6wKj8f8ahN', '2021-02-06 05:34:43', '2021-02-06 05:34:43'),
(14, 'Ram Chandra', '9849494516', NULL, 'cram87351@gmail.com', '2020-10-30 09:42:20', '$2y$10$rW3NL60I34OUuTG4cgI3k.WdqCQZmCubXelGeUq37nJJm/SG/WBdG', NULL, NULL, 'N', 'ZlzZ7p', 1, NULL, 0, 0, 0, NULL, 'ram-chandra', '120.89.104.45', NULL, 'j8kd17rmC04VPkaZ7Ovxpgx6AKIzBDuJFLMDcHHan7817V6pnhNSGJT37AZC', '2021-02-06 05:34:44', '2021-02-06 05:34:44'),
(15, 'Gambir Lama', NULL, NULL, 'nirvanagambir123@gmail.com', '2020-10-30 09:42:20', '$2y$10$mh2y8ebNXJDEnmlOqZJmj.LiDaqo6QLjpoYFlwmpKcMY/HZphnaPe', NULL, NULL, 'N', 'AT3usp', 1, NULL, 0, 0, 0, NULL, 'gambir-lama', '27.34.68.78', NULL, 'OMNneg6LicmAzo4Kl89m0MzXTQO4285MWBr5Gibforc8u60AtGpGEFPNxDr0', '2021-02-06 05:34:44', '2021-02-06 05:34:44'),
(16, 'pujan shrestha', NULL, NULL, 'spujan573@gmail.com', '2020-10-30 09:42:20', '$2y$10$EhraQi.xgeSfIJKZyCvPleZtHy2/IOJ5gBAVBo6rDOmCnnoTVI6U.', NULL, NULL, 'N', 'mFtop2', 1, NULL, 0, 0, 0, NULL, '2-pujan-shrestha', '103.94.220.72', NULL, '2MKCJAAkexzy3PC16KQzBzn9HHj3bBME1d0sN5d9Rlr3K01uZ5VEfCVK20GI', '2021-02-06 05:34:44', '2021-02-06 05:34:44'),
(17, 'Sandip tamang', '9866658865', NULL, 'sandiptamang65886450@gmail.com', '2020-10-30 09:42:20', '$2y$10$pctnaDH6mfZqJmZ/Y86Wz.PWmylohwhUzjPFcaTKUd1FfREhlMMqS', NULL, NULL, 'N', 'L43uYU', 1, NULL, 0, 0, 0, NULL, 'sandip-tamang', '27.34.20.205', NULL, NULL, '2021-02-06 05:34:44', '2021-02-06 05:34:44'),
(18, 'GTS S', '9808091906', NULL, 'gtmobileconcern@gmail.com', '2020-10-30 09:42:20', '$2y$10$ZbiooRodsckpRXEhmCFqDemeeSUI.KAUjYjZubQIlFBJiSCy1CCJG', NULL, NULL, 'N', 'le9VXa', 1, NULL, 0, 0, 0, NULL, 'gts-s', '27.34.20.129', NULL, 'IHHwQ7ByOyVzU4tAO54t8sGc27JIpoJImy81dsZ0exHkjgYTIVlnaBjY87Il', '2021-02-06 05:34:45', '2021-02-06 05:34:45'),
(19, 'S Confectioner', '9869212429', NULL, 'sconfectionerandpatisserie@gmail.com', '2020-10-30 09:42:20', '$2y$10$7sS3f3OHfRws6HuSLE2XGO0XKX6XUqPe5v8edq0j.FkhgiaKbEj22', NULL, NULL, 'N', 'JVFosF', 1, NULL, 0, 0, 0, NULL, 's-confectioner', '182.93.79.67', NULL, NULL, '2021-02-06 05:34:45', '2021-02-06 05:34:45'),
(20, 'Aman Shrestha', NULL, NULL, 'aman.shrestha471@gmail.com', '2020-10-30 09:42:20', '$2y$10$YCsvce6bgpSwEmUt.XAf8.dBM2deUNVzq06Q/kuk84.9rANGN1Bfi', NULL, NULL, 'N', 'SVwHtu', 1, NULL, 0, 0, 0, NULL, 'aman-shrestha', '27.34.17.8', NULL, 'd7aWIHRg3Ddk1nI3ZrphQKv2VgotWGRtmXXFSU6mz1NBDfTFf96wQqPrSjKE', '2021-02-06 05:34:46', '2021-02-06 05:34:46'),
(21, 'aakrish Mainali', NULL, NULL, 'aakrishmainali@gmail.com', '2020-10-30 09:42:20', '$2y$10$Ni8y.flEH9i7Ro4tPhTN6OD.I/tYaQXHxWREVJ5gZ/TiGZGYQHeRq', NULL, NULL, 'N', 'ZNNn4M', 1, NULL, 0, 0, 0, NULL, 'aakrish-mainali', '27.34.68.40', NULL, '3QvBzr5rqGIUZipMRukQVnSZCvXZHImQTPxIHSeIAi7KNmAqEofpRU16ayoV', '2021-02-06 05:34:46', '2021-02-06 05:34:46'),
(22, 'pramod', '9813999208', NULL, 'pramodbhandari069@gmail.com', '2020-10-30 09:42:20', '$2y$10$Y4gyUFe24rjHf65svpPUXe7A7rtQX.hyEhqdpNOupcr8vS3d9k7lO', NULL, NULL, 'N', 'pymOuv', 0, NULL, 0, 0, 0, NULL, 'pramod', '27.34.20.93', NULL, NULL, '2021-02-06 05:34:46', '2021-02-06 05:34:46'),
(23, 'लोकसेवाको राजाहुने चाहाना', NULL, NULL, 'khardarbaje@gmail.com', '2020-10-30 09:42:20', '$2y$10$.1pElUK26R.3Kw6dVasXFupvr6qQjkMdfBygKZ94em52KzAySX2r2', NULL, NULL, 'N', 'HplfqT', 1, NULL, 0, 0, 0, NULL, 'lkasavaka-rajahana-cahana', '27.34.20.93', NULL, 'IN2a0wb5ed0h2Ehtcyeh7Og9LwipcVhCArRdnPhagwW5eVVFt9AoRWCQ3Qer', '2021-02-06 05:34:46', '2021-02-06 05:34:46'),
(24, 'Kaisav Bhattarai', NULL, NULL, 'coolkeshab222@yahoo.com', '2020-10-30 09:42:20', '$2y$10$WBY/ucB0uCbkzheSazOEhOU3oRXQSC0ZM/4zNkkmQas4/y.wAkBWm', NULL, NULL, 'N', 'TJ8eBq', 1, NULL, 0, 0, 0, NULL, 'kaisav-bhattarai', '27.34.12.31', NULL, 'S49GuJGxw0iHaGlsyZPOo9XtFjMMPDHyTESrKxPWM7ZYPaYm26kTQL6GldDx', '2021-02-06 05:34:47', '2021-02-06 05:34:47'),
(25, 'Manish Jung Thapa', NULL, NULL, 'manishjung980@gmail.com', '2020-10-30 09:42:20', '$2y$10$4.zVLqldf1Hm/FnfaK5VpO7fW.gLRMVjfhyjW8uasPn78dpRpVEkG', NULL, NULL, 'N', 'drN4C0', 1, NULL, 0, 0, 0, NULL, 'manish-jung-thapa', '27.34.68.151', NULL, 'iO1ayjK4TSpWxLneXaegtqTqUacLGkKUapxeZFj8pNmIoYECbZC0D21ewJvi', '2021-02-06 05:34:47', '2021-02-06 05:34:47'),
(26, 'शिृ कृष्ण भट्टराई', NULL, NULL, 'krishnabhattarai14@yahoo.com', '2020-10-30 09:42:20', '$2y$10$iMPq49FBvP9JfTVuBXF5Z.lWfTFub9oDasNepzVW0bNf/c24BTPwu', NULL, NULL, 'N', 'VBsw4L', 1, NULL, 0, 0, 0, NULL, 'sha-kashhanae-bhatataraii', '27.34.12.20', NULL, 'R2RLlJSEdmTyHw99w3TFEUJ7NygRPNGeIPObXOvQzwNpinpUMgExT3x7Pj9Y', '2021-02-06 05:34:47', '2021-02-06 05:34:47'),
(27, 'Fiber Jobs', NULL, NULL, 'jobsatpspl@gmail.com', '2020-10-30 09:42:20', '$2y$10$cHICg.RVkQjKazi.ClW7oOIfuCU5f/MgXWbUcaBxIK0NMChmlhqSu', NULL, NULL, 'N', 'Re6N19', 1, NULL, 0, 0, 0, NULL, 'fiber-jobs', '27.34.12.26', NULL, 'R2RLlJSEdmTyHw99w3TFEUJ7NygRPNGeIPObXOvQzwNpinpUMgExT3x7Pj9Y', '2021-02-06 05:34:47', '2021-02-06 05:34:47'),
(28, 'Mero Offer', '9849551992', NULL, 'pramodbhandari68@gmail.com', '2021-02-06 03:54:46', '$2y$10$tZ3a6e9nKFEUXwkMyhO5n.Cy0QsjNWH2QBznWloWIcO77g0bANX6a', NULL, '', 'N', 'KcaD4i', 1, NULL, 0, 0, 0, NULL, 'mero-offer', '27.34.12.255', NULL, 'hQJ3IjJI2jJVDYXOET8tGYzYG9F4ltwlqTf36KfAKE4JZR3pTwwZcl4Gh64K', '2021-02-06 03:54:46', '2021-06-25 08:07:50'),
(29, 'Rahul Niraula', NULL, NULL, 'rahul.niraula@gmail.com', '2021-02-12 09:20:35', '$2y$10$PkkftgCm4vMhsr29ZtaswOf8ujLC8iX1Q5OpsxwGwtso1aySQrqoG', NULL, NULL, 'N', 'WPFExu', 0, NULL, 0, 0, 0, NULL, 'rahul-niraula', '103.10.28.141', NULL, 'VL8Y0URmofrsGYWd9E710pAv88hVQEkMBp5fWePS0yMpkMTPQFqcu8zYvurG', '2021-02-12 09:20:35', '2021-02-12 09:20:35'),
(30, 'Lunch Delight', '9866335883', NULL, 'lunchdelight18@gmail.com', NULL, '$2y$10$UiE3CyIM3CJhTuY8yYXzOOioqBR/jgUIOtnVjmMVBU1PVPH6qXnQS', NULL, NULL, 'N', 'YVE2d0', 0, NULL, 0, 0, 0, NULL, 'lunch-delight', '27.34.12.131', NULL, NULL, '2021-02-22 09:31:15', '2021-02-22 09:31:15'),
(31, 'Ashish Niraula', NULL, NULL, 'ashishniraula7@gmail.com', '2021-02-24 10:01:54', '$2y$10$QshJXZz0jJE0Vgjb4BEXjOy4NZ0gkldoJs70iF2CGoqNrbitrZL1O', NULL, NULL, 'N', 'nbrSbY', 0, NULL, 0, 0, 0, NULL, 'ashish-niraula', '27.34.12.39', NULL, 'gVkWEg2DIhHePFlcQn4iYIjCHJxInY5hmOhKVzlLLmxRLSLEW3PzDLtZRNKb', '2021-02-24 10:01:54', '2021-02-24 10:01:54'),
(32, 'Sujita Shrestha', '9851114354', NULL, 'rbnstha88@gmail.com', '2021-02-27 22:12:19', '$2y$10$vwnyqNYMR7JcadFoBw18Vu/iL.fxbESp8lzjO/JO6Gbj2Hli/6/za', NULL, NULL, 'N', 'k1MlPI', 1, NULL, 0, 0, 0, NULL, 'sujita-shrestha', '27.34.12.48', NULL, 'QUxr9OcLLEbvo3D6DhfZhtmK0QEdwfqi4kjOrCgLDUFFokN2wqJPXNrmS2zo', '2021-02-27 22:12:19', '2021-03-01 01:38:11'),
(33, 'Madan Majhi', '9810018547', NULL, 'madansuju2046@gmail.com', '2021-02-28 22:21:09', '$2y$10$iyLvube/Rb9yBCNPtY46N.BAbE93dLI.hkp9qPcRKlF18fp2XA/zu', NULL, NULL, 'N', 'OedJAx', 1, NULL, 0, 0, 0, NULL, 'madan-majhi', '27.34.12.48', NULL, 'LuVEIKf0zU1Ufxzmyx1Hg5Y3UCXJBjyXEE2LgNgvmbQao9fjn2PRsfA4jx0s', '2021-02-28 22:21:09', '2021-03-01 01:38:31'),
(34, 'Raju Pangeni', '9841823871', '9846325017', 'rajspangeni73@gmail.com', '2021-02-28 22:40:32', '$2y$10$zBcu2JJVXfQIVJ8a2Z1r9eN5/WMjbdXtKvneaQt0WQF3wpgtovJ6S', NULL, NULL, 'N', '25o5l2', 1, NULL, 0, 1, 1, NULL, 'raju-pangeni', '27.34.12.48', NULL, '1G4507vEymYu3Qp7cTocZEb9PPVvu0UTBTWb1sJm65vX1n711wbdCKMr5Iaf', '2021-02-28 22:40:32', '2021-03-01 01:38:41'),
(35, 'Barsha Dahal', '9818658990', NULL, 'dahalbarsha288@gmail.com', '2021-03-01 21:43:03', '$2y$10$bsOEVIpjOUvScAQKB8DSj.i75r4MLmGv7fS.hGkLzkzjTjdy9lLYy', NULL, NULL, 'N', 'VU1QSr', 1, NULL, 0, 0, 0, NULL, 'barsha-dahal', '27.34.68.75', NULL, 'hlbYxfSvEg24wNiswiWwZmGUYszRbz1ksSpjQLGEGC5dnaDk1AI12mwQ8qEJ', '2021-03-01 21:43:03', '2021-03-01 22:01:18'),
(36, 'Pacific Engineering Company Pvt. Ltd.', NULL, NULL, 'ktmpacific@gmail.com', '2021-03-08 22:35:57', '$2y$10$mSM3XjSNBcGMjygoH8JIe.U4chacI/56gs87HB4hk0PmSgf.EbK/m', NULL, NULL, 'N', 'uIEHcU', 0, 'users/DRWVEs9n5ooHiGw9ge2dPyfD0.jpg', 0, 0, 0, NULL, '2-pacific-engineering-company-pvt-ltd', '182.93.77.102', NULL, 'hJdbOO51w7LyWyl3VZ6q5LWP7k9ZNCDXAHYUd0S84iyhuUYoP8E7HxmDW0Mm', '2021-03-08 22:35:58', '2021-03-08 23:03:48'),
(37, 'G Opal Raj Giri', '9869151953', NULL, 'gopalrg@hotmail.com', '2021-03-10 01:12:33', '$2y$10$PbbxUsSkjk0I52mcOR2ki.R0nhBikrJfE0KtYMfNMzNrawX1NiaNG', NULL, NULL, 'N', 'K250Gf', 1, NULL, 0, 0, 0, NULL, '2-g-opal-raj-giri', '27.34.13.136', NULL, 'iWup9oq49B973Wgt0Xp56Ibowcyr16jYQWNK906LtnIibGFLtIyJ4nQV8NLy', '2021-03-10 01:12:33', '2021-03-10 05:50:46'),
(38, 'Purple Haze Rock Bar', '9803719781', NULL, 'hr@merooffer.com', '2021-03-16 01:27:34', '$2y$10$osS/L3AzUiiMMzyDmnHCCu74fOC58hVXvRIPzlDMwa.6OjIbHqhz2', NULL, NULL, 'N', 'yzrKqG', 1, NULL, 0, 0, 0, NULL, 'purple-haze-rock-bar', '27.34.13.122', NULL, NULL, '2021-03-16 01:30:22', '2021-03-16 01:30:22'),
(39, 'Samata Dhakhwa', '9841404083', NULL, 'samata.dhk@gmail.com', '2021-03-17 01:00:28', '$2y$10$AETu9XqI2umhKsSijh3ny.DfbpXUPW3.3/Y8gNtRxza/Z4b5t4g0q', NULL, '', 'N', 'SzlQfm', 1, NULL, 0, 0, 0, NULL, '2-samata-dhakhwa', '27.34.69.20', NULL, 'iekuJQpUOr0niHBtNzroPqZYzG4mIXgTXNc1asxQL2w76Wi1OGajoUayQVWt', '2021-03-17 00:58:31', '2021-04-21 22:55:12'),
(40, 'सुप्रिया छेत्री', NULL, NULL, 'supriya.chhetri7@gmail.com', '2021-03-17 03:52:45', '$2y$10$gAr1DVJ4G3r.e9lon8FZcegBE5gPzIw7atp87be66A1fYabUEEIte', NULL, NULL, 'N', 'zux1aG', 0, NULL, 0, 0, 0, NULL, 'saparaya-chhatara', '27.34.68.38', NULL, 'HuZgN8OQU8TLgYfUtyyETpoSSa0kub1OMPf4utOOUCjmEqnv7x0Qvsn263bJ', '2021-03-17 03:52:45', '2021-03-17 03:52:45'),
(41, 'Selina', '9884069209', NULL, 'selina.kapali@gmail.com', '2021-03-18 00:52:13', '$2y$10$slSq/bZ92GXIxjA.LwIiI.4encLiE5S99NtQ7spb1TLLJpNpZqOd6', NULL, NULL, 'N', 'lZl0it', 1, NULL, 0, 0, 0, NULL, '2-selina', '202.51.88.86', NULL, NULL, '2021-03-18 00:50:46', '2021-03-19 04:36:19'),
(42, 'Amit Kumar Mahato', '9801622351', NULL, 'amit2010mahto@yahoo.com', '2021-03-26 08:04:00', '$2y$10$kF/YLTFTsAsQXQBTNjVA9eNuanmNe9Iv6BvPIXcyLeXg7Gv3qSLWC', NULL, NULL, 'N', 'F0xF3L', 1, NULL, 1, 1, 0, NULL, '2-amit-kumar-mahato', '202.51.76.77', NULL, 'ryqeAlLDITYQpkv078PB07F4XFmH1t7sjrcQa0EgzTakQeNikfTXbvq3Ej7u', '2021-03-26 08:04:00', '2021-03-26 21:44:18'),
(43, 'Sanket Adhikari', '9840329488', NULL, 'sanketadhikari199@gmail.com', '2021-04-03 20:27:56', '$2y$10$b3KycC9MMq11nD/Wvjia/.Sxh/fClRyLQtE8/8QRP922PUEkf1VMG', NULL, NULL, 'N', 'A3tY8x', 1, NULL, 0, 0, 0, NULL, '2-sanket-adhikari', '45.64.160.39', NULL, NULL, '2021-04-03 20:23:36', '2021-04-04 02:05:08'),
(44, 'Gyalchen Dhokpya', NULL, NULL, 'gyalchenemail@gmail.com', '2021-04-07 19:00:49', '$2y$10$oelzM9X4aqJ2DUEXw5uUUuc6KEvncAfnz4zzOMqN8bfXVAaSPzCsa', NULL, NULL, 'N', 'gCagNv', 0, NULL, 0, 0, 0, NULL, 'gyalchen-dhokpya', '24.102.100.175', NULL, 'SmzDDO50VWq5kEJLjgsr1j1nPlr17htzbDm0O5GgATa7yEmLIEm1Qp5WIn8G', '2021-04-07 19:00:49', '2021-04-07 19:00:49'),
(45, 'Gyalchen Sherpa', NULL, NULL, 'glcnsrpa@gmail.com', '2021-04-07 19:01:21', '$2y$10$p5TwRMUd2FCKMm7D.5DfMOSqSAmBoTADsLXmOzZIWS/ic6mQirJ.q', NULL, NULL, 'N', 'syQf2x', 0, NULL, 0, 0, 0, NULL, 'gyalchen-sherpa', '24.102.100.175', NULL, 'fIDxKVip2SKzGvzGfNoTlbFx7n4Ok4frWhU0KsdwAHoVkXPCdGbXaZ4Q1cTE', '2021-04-07 19:01:22', '2021-04-07 19:01:22'),
(47, 'JSS DECOR', '9840006419', '+977144131482', 'decorjss@gmail.com', '2021-04-09 00:06:31', '$2y$10$JQ.piEwTF8patQBt69ja5uJMKn3RAwdzJSuF4IF.6WtXgnmuKushS', NULL, NULL, 'N', 'm8UisK', 1, 'users/das8Rijg6miaZduks9lgPXTGL.JPG', 0, 1, 0, NULL, 'jss-decor', '202.51.92.196', NULL, 'w2CLbUdXPtd5MIjURv09sZ5ZnBYt1chynJzgdtZn4OASFqsV1y3J16Ft3RVh', '2021-04-09 00:06:31', '2021-04-09 00:27:48'),
(48, 'subhadra sah', '9817671055', '9817671055', 'subhadrasah8@gmail.com', '2021-04-09 01:05:56', '$2y$10$dy5mkPvmWHWuhVXuPaiHgOnTmnVLgfEMQCVoHKpL3lDqwACgcs1Ny', NULL, NULL, 'N', 'khvnPK', 1, NULL, 1, 1, 0, NULL, 'subhadra-sah', '110.44.121.7', NULL, 'wksfwXa8UURSxniK7ooIR2DIOUY96WXA24WV4QzIKh0yitSu60L8x5JhYSwD', '2021-04-09 01:05:56', '2021-04-09 02:31:47'),
(49, 'Hz thapa', '9848879140', NULL, 'hemrajhg7@gmail.com', '2021-04-09 02:35:07', '$2y$10$dJeU.3KJdt4IjHH8OB0XC.o/8vwoJrrW4oriHOn13JGxVdoiobgx6', NULL, NULL, 'N', '4uKweJ', 1, NULL, 0, 0, 0, NULL, '2-hz-thapa', '27.34.12.140', NULL, 'G7pPaaz0Unmb15lMIkDciKo1yB5W03zvsZnUlG91ACWNhfBEFP36xJm6SjZK', '2021-04-09 02:32:33', '2021-04-09 03:24:40'),
(50, 'Sunil Yadav', NULL, NULL, 'raajsunil571@gmail.com', '2021-04-10 00:04:58', '$2y$10$yhUKo9l2za.7NH7UkiruSeKw3k50eRB9Ns8oFDrCSaZoVUpsxnPMq', NULL, '', 'N', 'Zg6rHm', 0, NULL, 0, 0, 0, NULL, 'sunil-yadav', '27.34.24.0', NULL, 'pzNCCrz9sH7CFLXjpWu3isKxoGfaz8L5qkNC6SSjlo2QzDH5XvewgPw2pwOz', '2021-04-10 00:04:58', '2021-04-10 00:04:58'),
(51, 'Sandip Shrestha', NULL, NULL, 'sandipshresthag@gmail.com', '2021-04-11 23:05:58', '$2y$10$mjr1FUaWaskvvMZwQEpExOKGCX3gVZI4CiDbu7fgc9/4jd4kbh5tC', NULL, '', 'N', 'hlE9Vs', 0, NULL, 0, 0, 0, NULL, 'sandip-shrestha', '27.34.12.134', NULL, 'zVWwkkE67ZdhXpzyc1bOgukUGCHz3iIesBM3JFiKguWDqYFqX3UoPzWf6pGw', '2021-04-11 23:05:59', '2021-04-11 23:05:59'),
(52, 'Imadol furniture house', '9849699006', NULL, 'neelamnk09@gmail.com', NULL, '$2y$10$7aU/q9A1sCTy4HcD3A6PtuS2t5gfl4oMrcMPp3jqcSNQGIpqj6NZC', NULL, NULL, 'N', 'ggznMw', 1, NULL, 0, 0, 0, NULL, '2-imadol-furniture-house', '103.137.201.3', NULL, NULL, '2021-04-16 09:11:51', '2021-04-17 18:31:24'),
(53, 'Amit Mahato', '9815828230', NULL, 'spinzamit1992@gmail.com', '2021-04-17 10:00:42', '$2y$10$./kinxsEUIliHNVIbrWnJ.hTTYVs7nvUf60QarT0Ok215nW3mVp9u', NULL, NULL, 'N', 'iEmp9e', 1, NULL, 0, 0, 0, NULL, 'amit-mahato', '103.10.31.30', NULL, 'vqkdkT0ZhNrpLvGJI4qCRyWJkD2EoTyThl9gQX9Nq80LulmDuPEDsWYBAR14', '2021-04-17 10:00:42', '2021-04-17 18:31:13'),
(54, 'Exploring beings', NULL, NULL, 'exploringbeings2000@gmail.com', '2021-04-20 02:33:00', '$2y$10$VKq.lPmZyM2uBG7J9FOiCuadBD8/jzUAghml3qCV3XuttWobyx2X2', NULL, NULL, 'N', 'mV7lFk', 0, NULL, 0, 0, 0, NULL, 'exploring-beings', '42.111.13.19', NULL, 'ipwzhOV1yfSgoGmn9ueFbX5v9tFydj5ephWT4GuT9Zx723q49KVq6wUqHWke', '2021-04-20 02:33:00', '2021-04-20 02:33:00'),
(55, 'Niraula Rahul', NULL, NULL, 'rahul.niraula@yahoo.com', '2021-04-21 07:14:14', '$2y$10$2asp6Q2XL1tBVPiAz55JkO0.DYV5isp2aXjYgd3Ip1EekycMmdJxm', NULL, '', 'N', 'ntzHAF', 0, NULL, 0, 0, 0, NULL, 'niraula-rahul', '120.89.104.2', NULL, '6nLw4JYhm1ZpSb7S1pceMOESL1HwUI3eEI4VVMW2MTcmC6toURq8u79vyFKZ', '2021-04-21 07:14:14', '2021-04-21 07:14:14'),
(56, 'Padam Prasad Bhandari', NULL, NULL, 'ppbhandari77@gmail.com', '2021-05-31 04:59:34', '$2y$10$7k8vnS0Ilbo9pxHw.91IbuuTmWmSLVrqVZmzY/3aTNgJ9Zp8WAP.W', NULL, NULL, 'N', 'pH0ZLh', 0, NULL, 0, 0, 0, NULL, 'padam-prasad-bhandari', '113.199.243.96', NULL, 'zPAjQjE8OE7Ldma7qadRTogyBUOkeC29cFdtn7kKe8P4JE9y2pEm6HCHGI4V', '2021-05-31 04:59:34', '2021-05-31 04:59:34'),
(57, 'rjk bishowkarma', NULL, NULL, 'rjkbishowkarma45@gmail.com', '2021-06-21 01:30:10', '$2y$10$x2wQDZHC8ADCLkSsoZNCFeoKLaJeEPzRq1X/NNvCpe3KmyfSbiCEq', NULL, NULL, 'N', 'XKpafe', 0, NULL, 0, 0, 0, NULL, 'rjk-bishowkarma', '45.64.162.107', NULL, 'cO3TiYoBtIvoRRTDOhpoNk1T9Dbgtn4Wc5pLJTTVfpEbE2manTW5JGBPzsVR', '2021-06-21 01:30:11', '2021-06-21 01:30:11'),
(58, 'Nrn Gajurel', NULL, NULL, 'nrngajurel@gmail.com', '2021-07-06 18:44:49', '$2y$10$C8HUvTEdewptwGjtNcrH6.iBK6sNtBZkz6USVPWbLYTKmqGYDbu02', NULL, NULL, 'N', 'TLtgBp', 0, NULL, 0, 0, 0, NULL, 'nrn-gajurel', '27.34.16.252', NULL, 'vBed7TdszaRWQTLQsWpjuUIEyZRHBla7WOAGUVo97z3IKBmNCDRHcDbMzmA9', '2021-07-06 18:44:49', '2021-07-06 18:44:49'),
(59, 'test test', NULL, NULL, 'systemerror977@gmail.com', '2021-07-09 22:11:03', '$2y$10$0it8oOV5/ZjQ0gxXa6YTzOaNqWWcmfX9ed3Af/YeK7UOPcbSmU6EW', NULL, NULL, 'N', 'PYen24', 0, NULL, 0, 0, 0, NULL, 'test-test', '27.34.12.213', NULL, 'jzaHer6MfdEPAWpYZfk2vvf0Aqm0sIxcgHqg4goW6WzReGGmr8l3nskvhu4G', '2021-07-09 22:11:03', '2021-07-09 22:11:03'),
(60, 'Umesh Aakar', NULL, NULL, 'ur.umesh159@gmail.com', '2021-07-09 22:27:35', '$2y$10$BWI0CiVyq0L0ispbxlLsMOxd9hxIyncvqWU9ZQybJIu3cyngCV86.', NULL, NULL, 'N', '8ydJSk', 0, NULL, 0, 0, 0, NULL, 'umesh-aakar', '103.95.18.107', NULL, 'eFlzOg7fsqzRTObiOxLN1cofJffUGXAq4VuID5WtXSw31xMVldva6ZFp0a8k', '2021-07-09 22:27:35', '2021-07-09 22:27:35'),
(61, 'Krishna Bhandari', NULL, NULL, 'krishnabhandari88@yahoo.com', '2021-07-11 03:07:49', '$2y$10$ZLxhiOmImbcMcmRE6WcMz.Uep7Iqu2MMZ.UMemVxLwF1pB6G1099C', NULL, NULL, 'N', 'csIx8x', 0, NULL, 0, 0, 0, NULL, 'krishna-bhandari', '113.199.245.149', NULL, 'zOgVRPkd7ofEOxgKE4pzLEMgNJyDX0kvZUjgk09aius6hnsuh75PlA1JEOJf', '2021-07-11 03:07:50', '2021-07-11 03:07:50'),
(62, 'Krishna Bhandari', NULL, NULL, 'kbhandari53@gmail.com', '2021-07-11 03:09:10', '$2y$10$F/ZCjKgOCly3ttQWEli5QuvfO9d3fcCVFjtaWWXNT1l7idYBbEHkG', NULL, NULL, 'N', 'fHltn1', 0, NULL, 0, 0, 0, NULL, '2-krishna-bhandari', '113.199.245.149', NULL, 'J06xkOUyplxX3tqTvagqSqvBdtsQC7KDiq0HSgkfYwiRImCVmTorxOAGjogc', '2021-07-11 03:09:10', '2021-07-11 03:09:10'),
(63, 'Gadget Hub', '9849865934', NULL, 'giriavinavko@gmail.com', '2021-07-18 17:54:05', '$2y$10$8yn0IEE3WTHO4WxFDYBgEemaP5ISl/2HQ.9MBAn9SJPZIY.nqvFJC', NULL, NULL, 'N', 'QQy6cl', 1, NULL, 0, 0, 0, NULL, '2-gadget-hub', '103.225.244.8', NULL, 'kEap0vZXrx06DnRJ3mg4WStIUzITXU7VVh0FucqWOwwdYhWaW7q60YOSB7FB', '2021-07-18 17:53:04', '2021-07-18 19:39:44'),
(64, 'Padam Prasad Bhandari', NULL, NULL, 'padambhandari34@yahoo.com', '2021-07-27 20:30:29', '$2y$10$LojtsNdJ/mn0b1R6F4AsCuFUx5Xj4wGR/akdp5KJX3qIpSLdN5HQK', NULL, '', 'N', 'uzgonS', 0, NULL, 0, 0, 0, NULL, '2-padam-prasad-bhandari', '113.199.241.88', NULL, 'H8cSdBDGBzKrazqYSOjLMeu0VTlSohxAlmb73dUB10DZfkR8YwmUIm220ncX', '2021-07-27 20:30:29', '2021-07-27 20:30:29'),
(65, 'seshan poudel', '9843806920', NULL, 'seshan682@gmail.com', '2021-08-07 20:08:24', '$2y$10$tLiz8/XeWoZ6XhOwr8vubebe0ajQR8hLsxsSdnRdpF3lZ1Vc/.S32', NULL, NULL, 'N', 'XEOCUi', 1, NULL, 0, 0, 0, NULL, '2-seshan-poudel', '27.34.30.118', NULL, NULL, '2021-08-07 20:05:48', '2021-08-08 07:29:17'),
(66, 'Mero Offer', NULL, NULL, 'smartepasal@gmail.com', '2021-08-11 07:33:26', '$2y$10$r/JzceOvGSB76WiH9uWjDeVa7MYyjq50cxKL92n1Dt4LkQ/UdEtW2', NULL, NULL, 'N', 'ZhPPzg', 0, NULL, 0, 0, 0, NULL, '2-mero-offer', '202.70.77.5', NULL, 'pcEBeegI71NrJ2JGspikmTFWIjm26qS0CYthGPZB5ga5VwRoToYuPxgb2eya', '2021-08-11 07:33:26', '2021-08-11 07:33:26'),
(67, 'rohit mainali', NULL, NULL, 'rohitmainali222@gmail.com', '2021-08-21 05:21:45', '$2y$10$U.EYKq71QozXO2OKkiyA6e8gYLVxKXbgy0GMj/tQvwFJPB0cfwkXe', NULL, '', 'N', 'lj2iCy', 0, NULL, 0, 0, 0, NULL, 'rohit-mainali', '27.34.13.217', NULL, 'fpFZBF6lnEVjehkjZNJuGx63YfJv57KObaIdBQhMg3nvzYOB3QsjuQpQOA0b', '2021-08-21 05:21:45', '2021-08-21 05:21:45'),
(68, 'Nivaj Shakya', '9841980800', NULL, 'nivaj.shakya@gmail.com', '2021-08-26 03:18:30', '$2y$10$vljx9xRlVHcHjK6glQhvlexmHiTuBmrauZ0ycg6yG5MClR0Md3khC', NULL, NULL, 'N', 'jc0kkf', 1, NULL, 0, 0, 0, NULL, 'nivaj-shakya', '27.34.12.7', NULL, '9reEQ5q4Pt7hnEq4faY4FaaTlHsc6f9Mt2qV96nSqpKndcigqPOZyxbCuQ4U', '2021-08-26 03:18:31', '2021-08-26 03:58:22'),
(69, 'Al Develloper', NULL, NULL, 'amritnepcom@gmail.com', '2021-08-28 23:31:31', '$2y$10$yYolLYrEsuZwJmHToeJPw.EwFoHObUlCvEA03aEK0ndYUbV0hc146', NULL, NULL, 'N', '7lPXWc', 0, NULL, 0, 0, 0, NULL, 'al-develloper', '103.163.182.211', NULL, 'QbhLuAJg3sm88lhYOifNbtkqtq2k9jtSL7n1ec75NbECFxiIah2NHZ1uSHrQ', '2021-08-28 23:31:31', '2021-08-28 23:31:31'),
(70, 'Kapil Thapa Magar', NULL, NULL, 'kapiltmagar50@gmail.com', '2021-09-03 23:53:52', '$2y$10$h1FytcrU9gxHkrkI0/bKFOrMtnRFolDkKIQhY87PKcHUmHRBrEq4u', NULL, NULL, 'N', 'uENcn9', 0, NULL, 0, 0, 0, NULL, 'kapil-thapa-magar', '103.10.29.119', NULL, 'yMm2y4CavJlvMg7f33vxYiRxAEDS4v4VhNIXvKovMnN7iiQGRDrO5df8WnyP', '2021-09-03 23:53:52', '2021-09-03 23:53:52'),
(71, 'Doc Test', NULL, NULL, 'doctest321@gmail.com', '2021-09-04 00:00:08', '$2y$10$9EMcRDAXix6SSxS4m51gAOM/BfjbokujRilr/1EOdCZ7jv8h5syyO', NULL, NULL, 'N', 'S41vSv', 0, NULL, 0, 0, 0, NULL, 'doc-test', '103.10.29.119', NULL, 'UqBZmKoRaF1zRVG4K19NFCfBVm9EuKfY74YVd5a8k7n15gvpDXVIBTlHqmAt', '2021-09-04 00:00:08', '2021-09-04 00:00:08'),
(72, 'Mohan B', '9808099591', NULL, 'coast_anv@hotmail.com', '2021-09-04 00:18:17', '$2y$10$A3KSNS.T.9rj4fYYA8UCBujFat3coXMxUu6ZoNW7tk3KJnssfMs6C', NULL, NULL, 'N', 'iJtWkf', 0, NULL, 0, 0, 0, NULL, 'mohan-b', '27.34.27.128', NULL, NULL, '2021-09-04 00:16:32', '2021-09-04 00:18:17');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `auth_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 is not added to wish list, 1 is added to wish list',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `works`
--

CREATE TABLE `works` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `addresses_user_id_index` (`user_id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `adtypes`
--
ALTER TABLE `adtypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `amenities`
--
ALTER TABLE `amenities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `amenities_attribute_id_foreign` (`attribute_id`);

--
-- Indexes for table `applicants`
--
ALTER TABLE `applicants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `applicants_user_id_foreign` (`user_id`),
  ADD KEY `applicants_job_id_foreign` (`job_id`);

--
-- Indexes for table `applicant_jobs`
--
ALTER TABLE `applicant_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `applicant_jobs_applicant_user_id_foreign` (`applicant_user_id`),
  ADD KEY `applicant_jobs_job_id_foreign` (`job_id`);

--
-- Indexes for table `assureds`
--
ALTER TABLE `assureds`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `assureds_slug_unique` (`slug`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attributes_code_unique` (`code`);

--
-- Indexes for table `attributevalues`
--
ALTER TABLE `attributevalues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attributevalues_attribute_id_foreign` (`attribute_id`);

--
-- Indexes for table `attributevalue_productattribute`
--
ALTER TABLE `attributevalue_productattribute`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attributevalue_productattribute_attributevalue_id_foreign` (`attributevalue_id`),
  ADD KEY `attributevalue_productattribute_productattribute_id_foreign` (`productattribute_id`);

--
-- Indexes for table `attribute_categories`
--
ALTER TABLE `attribute_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attribute_categories_attribute_id_foreign` (`attribute_id`),
  ADD KEY `attribute_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `authors_username_unique` (`username`),
  ADD UNIQUE KEY `authors_email_unique` (`email`);

--
-- Indexes for table `bids`
--
ALTER TABLE `bids`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bids_user_id_foreign` (`user_id`),
  ADD KEY `bids_bidable_id_foreign` (`bidable_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `category_pricelabel`
--
ALTER TABLE `category_pricelabel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_pricelabel_category_id_foreign` (`category_id`),
  ADD KEY `category_pricelabel_pricelabel_id_foreign` (`pricelabel_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cities_slug_unique` (`slug`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_commentable_id_foreign` (`commentable_id`);

--
-- Indexes for table `conditions`
--
ALTER TABLE `conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contacts_product_id_foreign` (`product_id`),
  ADD KEY `contacts_user_id_foreign` (`user_id`);

--
-- Indexes for table `couriers`
--
ALTER TABLE `couriers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `durations`
--
ALTER TABLE `durations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `durations_slug_unique` (`slug`);

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD PRIMARY KEY (`id`),
  ADD KEY `education_user_id_foreign` (`user_id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `experiences`
--
ALTER TABLE `experiences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `experiences_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `fapplicants`
--
ALTER TABLE `fapplicants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fapplicants_user_id_foreign` (`user_id`),
  ADD KEY `fapplicants_fjob_id_foreign` (`fjob_id`);

--
-- Indexes for table `fapplicant_fjobs`
--
ALTER TABLE `fapplicant_fjobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fapplicant_fjobs_fapplicant_user_id_foreign` (`fapplicant_user_id`),
  ADD KEY `fapplicant_fjobs_fjob_id_foreign` (`fjob_id`);

--
-- Indexes for table `fcategories`
--
ALTER TABLE `fcategories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fcategories_slug_unique` (`slug`);

--
-- Indexes for table `firmusers`
--
ALTER TABLE `firmusers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `firmusers_user_id_foreign` (`user_id`);

--
-- Indexes for table `fjobs`
--
ALTER TABLE `fjobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fjobs_slug_unique` (`slug`),
  ADD KEY `fjobs_jobtype_id_foreign` (`jobtype_id`),
  ADD KEY `fjobs_fcategory_id_foreign` (`fcategory_id`),
  ADD KEY `fjobs_user_id_foreign` (`user_id`),
  ADD KEY `fjobs_duration_id_foreign` (`duration_id`);

--
-- Indexes for table `fjob_skill`
--
ALTER TABLE `fjob_skill`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fjob_skill_skill_id_foreign` (`skill_id`),
  ADD KEY `fjob_skill_fjob_id_foreign` (`fjob_id`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `followers_leader_id_foreign` (`leader_id`),
  ADD KEY `followers_followed_by_foreign` (`followed_by`);

--
-- Indexes for table `industries`
--
ALTER TABLE `industries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `industries_slug_unique` (`slug`);

--
-- Indexes for table `jattributes`
--
ALTER TABLE `jattributes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `jattributes_code_unique` (`code`);

--
-- Indexes for table `jattributevalues`
--
ALTER TABLE `jattributevalues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jattributevalues_jattribute_id_foreign` (`jattribute_id`);

--
-- Indexes for table `jattribute_job_category`
--
ALTER TABLE `jattribute_job_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jattribute_job_category_jattribute_id_foreign` (`jattribute_id`),
  ADD KEY `jattribute_job_category_jobcategory_id_foreign` (`jobcategory_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `jobs_slug_unique` (`slug`),
  ADD KEY `jobs_experience_id_foreign` (`experience_id`),
  ADD KEY `jobs_city_id_foreign` (`city_id`),
  ADD KEY `jobs_day_id_foreign` (`day_id`),
  ADD KEY `jobs_jobtype_id_foreign` (`jobtype_id`),
  ADD KEY `jobs_level_id_foreign` (`level_id`),
  ADD KEY `jobs_category_id_foreign` (`category_id`),
  ADD KEY `jobs_positiontype_id_foreign` (`positiontype_id`),
  ADD KEY `jobs_industry_id_foreign` (`industry_id`),
  ADD KEY `jobs_user_id_foreign` (`user_id`);

--
-- Indexes for table `jobtypes`
--
ALTER TABLE `jobtypes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `jobtypes_slug_unique` (`slug`);

--
-- Indexes for table `job_categories`
--
ALTER TABLE `job_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `job_categories_slug_unique` (`slug`);

--
-- Indexes for table `job_jattributevalue`
--
ALTER TABLE `job_jattributevalue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_jattributevalue_jattributevalue_id_foreign` (`jattributevalue_id`),
  ADD KEY `job_jattributevalue_job_id_foreign` (`job_id`);

--
-- Indexes for table `job_skill`
--
ALTER TABLE `job_skill`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_skill_skill_id_foreign` (`skill_id`),
  ADD KEY `job_skill_job_id_foreign` (`job_id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mcategories`
--
ALTER TABLE `mcategories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mcategories_slug_unique` (`slug`);

--
-- Indexes for table `mcontacts`
--
ALTER TABLE `mcontacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `memails`
--
ALTER TABLE `memails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `orders_reference_unique` (`reference`),
  ADD KEY `orders_courier_id_index` (`courier_id`),
  ADD KEY `orders_user_id_index` (`user_id`),
  ADD KEY `orders_address_id_index` (`address_id`),
  ADD KEY `orders_order_status_id_index` (`order_status_id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_product_order_id_index` (`order_id`),
  ADD KEY `order_product_product_id_index` (`product_id`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positiontypes`
--
ALTER TABLE `positiontypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`),
  ADD KEY `posts_author_id_foreign` (`author_id`),
  ADD KEY `posts_mcategory_id_foreign` (`mcategory_id`);

--
-- Indexes for table `post_tags`
--
ALTER TABLE `post_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_tags_post_id_foreign` (`post_id`),
  ADD KEY `post_tags_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `pricelabels`
--
ALTER TABLE `pricelabels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricetypes`
--
ALTER TABLE `pricetypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productattributes`
--
ALTER TABLE `productattributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productattributes_product_id_foreign` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`),
  ADD KEY `products_assured_id_foreign` (`assured_id`),
  ADD KEY `products_pricetype_id_foreign` (`pricetype_id`),
  ADD KEY `products_day_id_foreign` (`day_id`),
  ADD KEY `products_adtype_id_foreign` (`adtype_id`),
  ADD KEY `products_city_id_foreign` (`city_id`),
  ADD KEY `products_user_id_foreign` (`user_id`);

--
-- Indexes for table `product_amenities`
--
ALTER TABLE `product_amenities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_amenities_amenity_id_foreign` (`amenity_id`),
  ADD KEY `product_amenities_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_attributevalues`
--
ALTER TABLE `product_attributevalues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_attributevalues_attributevalue_id_foreign` (`attributevalue_id`),
  ADD KEY `product_attributevalues_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_categories_product_id_foreign` (`product_id`),
  ADD KEY `product_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profiles_user_id_foreign` (`user_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_signup_id_foreign` (`signup_id`),
  ADD KEY `ratings_product_id_foreign` (`product_id`);

--
-- Indexes for table `reportnames`
--
ALTER TABLE `reportnames`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_auth_id_foreign` (`auth_id`),
  ADD KEY `reports_user_id_foreign` (`user_id`),
  ADD KEY `reports_product_id_foreign` (`product_id`);

--
-- Indexes for table `resumes`
--
ALTER TABLE `resumes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `resumes_user_id_foreign` (`user_id`),
  ADD KEY `resumes_job_id_foreign` (`job_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_seller_id_foreign` (`seller_id`),
  ADD KEY `reviews_commentator_id_foreign` (`commentator_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `salarytypes`
--
ALTER TABLE `salarytypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`identifier`,`instance`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `skills_slug_unique` (`slug`);

--
-- Indexes for table `skill_user`
--
ALTER TABLE `skill_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `skill_user_user_id_foreign` (`user_id`),
  ADD KEY `skill_user_skill_id_foreign` (`skill_id`);

--
-- Indexes for table `social_identities`
--
ALTER TABLE `social_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `social_identities_provider_id_unique` (`provider_id`),
  ADD KEY `social_identities_user_id_foreign` (`user_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_slug_unique` (`slug`);

--
-- Indexes for table `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trainings_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_slug_unique` (`slug`),
  ADD UNIQUE KEY `users_mobile_unique` (`mobile`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wishlists_auth_id_foreign` (`auth_id`),
  ADD KEY `wishlists_product_id_foreign` (`product_id`);

--
-- Indexes for table `works`
--
ALTER TABLE `works`
  ADD PRIMARY KEY (`id`),
  ADD KEY `works_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `adtypes`
--
ALTER TABLE `adtypes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `amenities`
--
ALTER TABLE `amenities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `applicants`
--
ALTER TABLE `applicants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `applicant_jobs`
--
ALTER TABLE `applicant_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `assureds`
--
ALTER TABLE `assureds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `attributevalues`
--
ALTER TABLE `attributevalues`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=315;

--
-- AUTO_INCREMENT for table `attributevalue_productattribute`
--
ALTER TABLE `attributevalue_productattribute`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `attribute_categories`
--
ALTER TABLE `attribute_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bids`
--
ALTER TABLE `bids`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `category_pricelabel`
--
ALTER TABLE `category_pricelabel`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `conditions`
--
ALTER TABLE `conditions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `couriers`
--
ALTER TABLE `couriers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `durations`
--
ALTER TABLE `durations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `education`
--
ALTER TABLE `education`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `experiences`
--
ALTER TABLE `experiences`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fapplicants`
--
ALTER TABLE `fapplicants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fapplicant_fjobs`
--
ALTER TABLE `fapplicant_fjobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fcategories`
--
ALTER TABLE `fcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `firmusers`
--
ALTER TABLE `firmusers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `fjobs`
--
ALTER TABLE `fjobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fjob_skill`
--
ALTER TABLE `fjob_skill`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `industries`
--
ALTER TABLE `industries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `jattributes`
--
ALTER TABLE `jattributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jattributevalues`
--
ALTER TABLE `jattributevalues`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jattribute_job_category`
--
ALTER TABLE `jattribute_job_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `jobtypes`
--
ALTER TABLE `jobtypes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `job_categories`
--
ALTER TABLE `job_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `job_jattributevalue`
--
ALTER TABLE `job_jattributevalue`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_skill`
--
ALTER TABLE `job_skill`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mcategories`
--
ALTER TABLE `mcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mcontacts`
--
ALTER TABLE `mcontacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `memails`
--
ALTER TABLE `memails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `positiontypes`
--
ALTER TABLE `positiontypes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `post_tags`
--
ALTER TABLE `post_tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pricelabels`
--
ALTER TABLE `pricelabels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pricetypes`
--
ALTER TABLE `pricetypes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `productattributes`
--
ALTER TABLE `productattributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `product_amenities`
--
ALTER TABLE `product_amenities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `product_attributevalues`
--
ALTER TABLE `product_attributevalues`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reportnames`
--
ALTER TABLE `reportnames`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `resumes`
--
ALTER TABLE `resumes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salarytypes`
--
ALTER TABLE `salarytypes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `skill_user`
--
ALTER TABLE `skill_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `social_identities`
--
ALTER TABLE `social_identities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trainings`
--
ALTER TABLE `trainings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `works`
--
ALTER TABLE `works`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `amenities`
--
ALTER TABLE `amenities`
  ADD CONSTRAINT `amenities_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `applicants`
--
ALTER TABLE `applicants`
  ADD CONSTRAINT `applicants_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicants_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `applicant_jobs`
--
ALTER TABLE `applicant_jobs`
  ADD CONSTRAINT `applicant_jobs_applicant_user_id_foreign` FOREIGN KEY (`applicant_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicant_jobs_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attributevalues`
--
ALTER TABLE `attributevalues`
  ADD CONSTRAINT `attributevalues_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attributevalue_productattribute`
--
ALTER TABLE `attributevalue_productattribute`
  ADD CONSTRAINT `attributevalue_productattribute_attributevalue_id_foreign` FOREIGN KEY (`attributevalue_id`) REFERENCES `attributevalues` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `attributevalue_productattribute_productattribute_id_foreign` FOREIGN KEY (`productattribute_id`) REFERENCES `productattributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_categories`
--
ALTER TABLE `attribute_categories`
  ADD CONSTRAINT `attribute_categories_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `attribute_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `bids`
--
ALTER TABLE `bids`
  ADD CONSTRAINT `bids_bidable_id_foreign` FOREIGN KEY (`bidable_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bids_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_pricelabel`
--
ALTER TABLE `category_pricelabel`
  ADD CONSTRAINT `category_pricelabel_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_pricelabel_pricelabel_id_foreign` FOREIGN KEY (`pricelabel_id`) REFERENCES `pricelabels` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_commentable_id_foreign` FOREIGN KEY (`commentable_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contacts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `education`
--
ALTER TABLE `education`
  ADD CONSTRAINT `education_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fapplicants`
--
ALTER TABLE `fapplicants`
  ADD CONSTRAINT `fapplicants_fjob_id_foreign` FOREIGN KEY (`fjob_id`) REFERENCES `fjobs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fapplicants_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fapplicant_fjobs`
--
ALTER TABLE `fapplicant_fjobs`
  ADD CONSTRAINT `fapplicant_fjobs_fapplicant_user_id_foreign` FOREIGN KEY (`fapplicant_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fapplicant_fjobs_fjob_id_foreign` FOREIGN KEY (`fjob_id`) REFERENCES `fjobs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `firmusers`
--
ALTER TABLE `firmusers`
  ADD CONSTRAINT `firmusers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fjobs`
--
ALTER TABLE `fjobs`
  ADD CONSTRAINT `fjobs_duration_id_foreign` FOREIGN KEY (`duration_id`) REFERENCES `durations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fjobs_fcategory_id_foreign` FOREIGN KEY (`fcategory_id`) REFERENCES `fcategories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fjobs_jobtype_id_foreign` FOREIGN KEY (`jobtype_id`) REFERENCES `jobtypes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fjobs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fjob_skill`
--
ALTER TABLE `fjob_skill`
  ADD CONSTRAINT `fjob_skill_fjob_id_foreign` FOREIGN KEY (`fjob_id`) REFERENCES `fjobs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fjob_skill_skill_id_foreign` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `followers`
--
ALTER TABLE `followers`
  ADD CONSTRAINT `followers_followed_by_foreign` FOREIGN KEY (`followed_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `followers_leader_id_foreign` FOREIGN KEY (`leader_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `jattributevalues`
--
ALTER TABLE `jattributevalues`
  ADD CONSTRAINT `jattributevalues_jattribute_id_foreign` FOREIGN KEY (`jattribute_id`) REFERENCES `jattributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `jattribute_job_category`
--
ALTER TABLE `jattribute_job_category`
  ADD CONSTRAINT `jattribute_job_category_jattribute_id_foreign` FOREIGN KEY (`jattribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jattribute_job_category_jobcategory_id_foreign` FOREIGN KEY (`jobcategory_id`) REFERENCES `job_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `jobs_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `job_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jobs_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jobs_day_id_foreign` FOREIGN KEY (`day_id`) REFERENCES `days` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jobs_experience_id_foreign` FOREIGN KEY (`experience_id`) REFERENCES `experiences` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jobs_industry_id_foreign` FOREIGN KEY (`industry_id`) REFERENCES `industries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jobs_jobtype_id_foreign` FOREIGN KEY (`jobtype_id`) REFERENCES `jobtypes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jobs_level_id_foreign` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jobs_positiontype_id_foreign` FOREIGN KEY (`positiontype_id`) REFERENCES `positiontypes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jobs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `job_jattributevalue`
--
ALTER TABLE `job_jattributevalue`
  ADD CONSTRAINT `job_jattributevalue_jattributevalue_id_foreign` FOREIGN KEY (`jattributevalue_id`) REFERENCES `jattributevalues` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `job_jattributevalue_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `job_skill`
--
ALTER TABLE `job_skill`
  ADD CONSTRAINT `job_skill_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `job_skill_skill_id_foreign` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_address_id_foreign` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`),
  ADD CONSTRAINT `orders_courier_id_foreign` FOREIGN KEY (`courier_id`) REFERENCES `couriers` (`id`),
  ADD CONSTRAINT `orders_order_status_id_foreign` FOREIGN KEY (`order_status_id`) REFERENCES `order_statuses` (`id`),
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `order_product_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_mcategory_id_foreign` FOREIGN KEY (`mcategory_id`) REFERENCES `mcategories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_tags`
--
ALTER TABLE `post_tags`
  ADD CONSTRAINT `post_tags_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `productattributes`
--
ALTER TABLE `productattributes`
  ADD CONSTRAINT `productattributes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_adtype_id_foreign` FOREIGN KEY (`adtype_id`) REFERENCES `adtypes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_assured_id_foreign` FOREIGN KEY (`assured_id`) REFERENCES `assureds` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_day_id_foreign` FOREIGN KEY (`day_id`) REFERENCES `days` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_pricetype_id_foreign` FOREIGN KEY (`pricetype_id`) REFERENCES `pricetypes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_amenities`
--
ALTER TABLE `product_amenities`
  ADD CONSTRAINT `product_amenities_amenity_id_foreign` FOREIGN KEY (`amenity_id`) REFERENCES `amenities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_amenities_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_attributevalues`
--
ALTER TABLE `product_attributevalues`
  ADD CONSTRAINT `product_attributevalues_attributevalue_id_foreign` FOREIGN KEY (`attributevalue_id`) REFERENCES `attributevalues` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_attributevalues_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD CONSTRAINT `product_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_categories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ratings_signup_id_foreign` FOREIGN KEY (`signup_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reports`
--
ALTER TABLE `reports`
  ADD CONSTRAINT `reports_auth_id_foreign` FOREIGN KEY (`auth_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reports_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `resumes`
--
ALTER TABLE `resumes`
  ADD CONSTRAINT `resumes_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_commentator_id_foreign` FOREIGN KEY (`commentator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reviews_seller_id_foreign` FOREIGN KEY (`seller_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `skill_user`
--
ALTER TABLE `skill_user`
  ADD CONSTRAINT `skill_user_skill_id_foreign` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `skill_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_identities`
--
ALTER TABLE `social_identities`
  ADD CONSTRAINT `social_identities_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `trainings`
--
ALTER TABLE `trainings`
  ADD CONSTRAINT `trainings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD CONSTRAINT `wishlists_auth_id_foreign` FOREIGN KEY (`auth_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wishlists_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `works`
--
ALTER TABLE `works`
  ADD CONSTRAINT `works_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
