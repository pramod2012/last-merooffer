<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use App\Job;
Use App\JobCategory;
Use App\Models\User;
use App\Industry;
use App\Positiontype;
use App\Level;
use App\Firmuser;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreJobRequest;
use App\Contracts\JobContract;

class JobuserController extends Controller
{
    protected $jobRepo;
    public function __construct(JobContract $jobRepo)
    {
        $this->middleware(['auth','verified']);
        $this->jobRepo = $jobRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()  { 
       $user_id = Auth()->user()->id;
       $user = User::findOrFail($user_id);
       $jobs = Job::where('user_id', $user->id)
               ->orderBy('created_at', 'desc')
               ->paginate(5); 
       return view('client.jobs', compact('jobs', 'user'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Firmuser::where('user_id',auth()->user()->id)->count() == null ){
            return redirect()->route('editfirmuser')->with('success','Please complete your company details first to post job!');
        }
        else{
        $categories = JobCategory::all();
        $industries = Industry::all();
        $positiontypes = Positiontype::all();
        $levels = Level::all();
        return view('jobpost.create',compact("categories","industries","positiontypes","levels"));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreJobRequest $request)
    {
        $job = $this->jobRepo->storeJob($request);

        return redirect()->route('job-user.index')->with('success', "Job Posting Created");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $job = Job::where('user_id',auth()->user()->id)->findOrFail($id);
       $categories = JobCategory::all();
       $industries = Industry::all();
       $levels = Level::all();
       $positiontypes = Positiontype::all();
       if($job->user_id != auth()->user()->id) {
          return redirect('/')->with('error', 'Unauthorize Page');
       } 
       return view('jobpost.edit', compact('job','categories',"industries",'levels','positiontypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreJobRequest $request, $id)
    {
        $job = Job::where('user_id',auth()->user()->id)->findOrFail($id);
        if(Auth::user()->id == !$job->user_id){
            return redirect('/job')->with('success', "Error");
        }
        $this->jobRepo->editJob($request, $id);

        return redirect('/job-user')->with('success', "Job Posting Updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $job = Job::where('user_id',auth()->user()->id)->findOrFail($id);
        $job->delete();

        return back()->with('success', "Job Posting Deleted");
    }
}
