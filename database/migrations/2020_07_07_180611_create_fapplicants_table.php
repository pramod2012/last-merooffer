<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFapplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fapplicants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('application_letter');
            $table->string('astatus');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('fjob_id')->unsigned();
            $table->foreign('fjob_id')->references('id')->on('fjobs')->onDelete('cascade');
            $table->boolean('mail_status')->default(0);
            $table->bigInteger('bid');
            $table->bigInteger('day')->nullable();
            $table->string('attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fapplicants');
    }
}
