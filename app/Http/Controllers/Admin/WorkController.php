<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\WorkContract;
use App\Http\Controllers\BaseController;

class WorkController extends BaseController
{
    private $page="admin.work.";

    protected $workRepo;

    public function __construct(
        WorkContract $workRepo
    )
    {
        $this->workRepo = $workRepo;
    }

    public function index()
    {
        $list = $this->workRepo->list('created_at', 'desc');
        $datas = $this->workRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('work', 'List of work');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('work', 'Create work');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->workRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('work.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->workRepo->findById($id);
        $this->setPageTitle('work', 'Edit work');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->workRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('work.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->workRepo->destroy($id);
        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('work.index', 'Record deleted successfully' ,'success',false, false);
    }
}