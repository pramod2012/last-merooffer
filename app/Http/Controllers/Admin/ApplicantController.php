<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\ApplicantContract;
use App\Http\Controllers\BaseController;

class ApplicantController extends BaseController
{
    private $page="admin.applicant.";

    protected $applicantRepo;

    public function __construct(
        ApplicantContract $applicantRepo
    )
    {
        $this->applicantRepo = $applicantRepo;
    }

    public function index()
    {
        $list = $this->applicantRepo->list('created_at', 'desc');
        $datas = $this->applicantRepo->paginateArrayResults($list->all(), 5);
        $this->setPageTitle('applicant', 'List of applicant');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('applicant', 'Create applicant');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->applicantRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('applicant.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->applicantRepo->findById($id);
        $this->setPageTitle('applicant', 'Edit applicant');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->applicantRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('applicant.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->applicantRepo->destroy($id);
        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('applicant.index', 'Record deleted successfully' ,'success',false, false);
    }
}
