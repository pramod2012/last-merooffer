@extends('site.partials.main_index')
@section('title', 'Page Not Found')
@section('content')
<body class="archive category category-uncategorized category-1 logged-in">
    @include('site.partials.navbar')
    @include('site.partials.searchjob')
<section class="page-content-404">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div id="background-wrap">
                    <div class="x1">
                        <div class="cloud-inner"></div>
                        <div class="cloud"></div>
                    </div>

                    <div class="x2">
                        <div class="cloud-inner"></div>
                        <div class="cloud"></div>
                    </div>

                    <div class="x3">
                        <div class="cloud-inner"></div>
                        <div class="cloud"></div>
                    </div>

                    <div class="x4">
                        <div class="cloud-inner"></div>
                        <div class="cloud"></div>
                    </div>

                    <div class="x5">
                        <div class="cloud-inner"></div>
                        <div class="cloud"></div>
                    </div>
                </div>
                <div class="img-404 text-center">
                    <img class="img-responsive" src="{{asset('frontend/images/404.png')}}" alt="404">
                </div>
                <div class="text-404 text-center">
                    <h1 class="text-uppercase">Oops ! </h1>
                    <h2 class="text-uppercase">Sorry We Cant Find That Page!</h2>
                    <p>Either something went wrong or the page doesn't exist anymore.</p>
                    <a href="{{route('index')}}" class="btn btn-primary btn-sm sharp btn-style-one">Go back HomePage</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="grass-404"></section>
@include('site.partials.footer')
@endsection
