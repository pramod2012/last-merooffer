<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\AddressContract;
use App\Http\Controllers\BaseController;

class AddressController extends BaseController
{
    private $page="admin.address.";

    protected $addressRepo;

    public function __construct(
        AddressContract $addressRepo
    )
    {
        $this->addressRepo = $addressRepo;
    }

    public function index()
    {
        $list = $this->addressRepo->list('created_at', 'desc');
        $datas = $this->addressRepo->paginateArrayResults($list->all(), 5);
        $this->setPageTitle('address', 'List of addresss');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('address', 'Create address');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->addressRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('address.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->addressRepo->findById($id);
        $this->setPageTitle('address', 'Edit address');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->addressRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('address.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->addressRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('address.index', 'Record deleted successfully' ,'success',false, false);
    }
}
