<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProfileResumeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'title' => 'required',
            'city' => 'required',
            'province' => 'required|numeric',
            'country' => 'nullable|regex:/9[78][\d]{7}\d$/',
            'overview' => 'required',
        ];
    }

    public function attributes()
    {
        return [
        'title' => 'job title',
        'city' => 'city',
        'province' => 'province',
        'country' => 'mobile number',
        'overview' => 'career objective'
        ];
    }
}
