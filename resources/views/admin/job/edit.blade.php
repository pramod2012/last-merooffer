@extends('admin.job.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{route('job.update',$job->id)}}" enctype="multipart/form-data">
                       {{csrf_field()}}
                       {{method_field('PATCH')}}
                       
                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Title / name *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="name" value="{{ old('name',$job->name) }}">
                                <span class="text-danger">{{ $errors->first('name') }}</span>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Expired At *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="expired_at" value="{{ old('expired_at',$job->expired_at) }}">
                                <span class="text-danger">{{ $errors->first('expired_at') }}</span>

                            </div>
                        </div>                                    

                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Salary *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="salary" value="{{ old('salary',$job->salary) }}">
                                <span class="text-danger">{{ $errors->first('salary') }}</span>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Salary To*</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="salary_to" value="{{ old('salary_to',$job->salary_to) }}">
                                <span class="text-danger">{{ $errors->first('salary_to') }}</span>

                            </div>
                        </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Salary Type</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="salarytype_id">
                                  @foreach(\App\Salarytype::all() as $value)
                                  @if ($job->salarytype_id == $value->id)
                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                      @else
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                  @endif
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('salarytype_id') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Education *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="education" value="{{ old('education', $job->education) }}">
                                <span class="text-danger">{{ $errors->first('education') }}</span>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Vacancy Number *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="vac_no" value="{{ old('vac_no', $job->vac_no) }}">
                                <span class="text-danger">{{ $errors->first('vac_no') }}</span>

                            </div>
                        </div>

                        <div class="form-group">
                      <label class="col-md-2 control-label">Experience</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="experience_id">
                                  @foreach(\App\Experience::all() as $value)
                                  @if ($job->experience_id == $value->id)
                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                      @else
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                  @endif
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('experience_id') }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Age</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="age" value="{{ old('age',$job->age) }}">
                                <span class="text-danger">{{ $errors->first('age') }}</span>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Gender</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="gender" value="{{ old('gender',$job->gender) }}">
                              <span class="text-danger">{{ $errors->first('gender') }}</span>

                            </div>
                        </div>

                        <div class="form-group">
                      <label class="col-md-2 control-label">City</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="city_id">
                              @foreach(\App\Models\City::all() as $city)
                                @if ($job->city_id == $city->id)
                                    <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                                      @else
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endif
                              @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('city_id') }}</span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Address *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="address" value="{{ old('address',$job->address) }}">
                                <span class="text-danger">{{ $errors->first('address') }}</span>

                            </div>
                        </div>
                        
                        
                  <div class="form-group">
                      <label class="col-md-2 control-label">Job Category</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="category_id">
                              @foreach($jobcategories as $jobcategory)
                                @if ($job->category_id == $jobcategory->id)
                                    <option value="{{ $jobcategory->id }}" selected>{{ $jobcategory->name }}</option>
                                      @else
                                    <option value="{{ $jobcategory->id }}">{{ $jobcategory->name }}</option>
                                @endif
                              @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('category_id') }}</span>
                            </div>
                        </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label">Industries</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="industry_id">
                                  @foreach($industries as $value)
                                  @if ($job->industry_id == $value->id)
                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                      @else
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                  @endif
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('industry_id') }}</span>
                            </div>
                        </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label">Position Type</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="positiontype_id">
                                  @foreach($positiontypes as $value)
                                  @if ($job->positiontype_id == $value->id)
                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                      @else
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                  @endif
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('positiontype_id') }}</span>
                            </div>
                        </div>



                    <div class="form-group">
                      <label class="col-md-2 control-label">Level</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="level_id">
                                  @foreach($levels as $value)
                                  @if ($job->level_id == $value->id)
                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                      @else
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                  @endif
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('level_id') }}</span>
                            </div>
                        </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label">User</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="user_id">
                                  <option value="{{ $job->user->id }}" selected>{{ $job->user->name }}</option>
                                     
                                </select>
                                <span class="text-danger">{{ $errors->first('user_id') }}</span>
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="col-md-2 control-label">Last Date *</label>
                            <div class="col-md-10">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" value="{{ $job->endate }}" name="endate" class="form-control pull-right" id="birthDate">
                                    <span class="text-danger">{{ $errors->first('endate') }}</span>
                                </div>
                            </div>
                        </div>


                         <div class="form-group">
                            <label for="post" class="col-md-2 control-label">Body*</label>

                            <div class="col-md-10">
                                <textarea class="form-control" rows="7" id="news" type="text" name="body" value="{{old('body')}}">{{old('body', $job->body)}}</textarea>

                                    <span class="text-danger">{{ $errors->first('body') }}</span>
                            </div>
                        </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label">Day</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="day_id">
                                  @foreach($days as $value)
                                  @if ($job->day_id == $value->id)
                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                      @else
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                  @endif
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('day_id') }}</span>
                            </div>
                        </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label">Job Type</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="jobtype_id">
                                  @foreach($jobtypes as $value)
                                  @if ($job->jobtype_id == $value->id)
                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                      @else
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                  @endif
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('jobtype_id') }}</span>
                            </div>
                        </div>

                <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Skills</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="skills" value="@foreach($job->skillss as $skill){{$skill->skill}},@endforeach">
                                <span class="text-danger">{{ $errors->first('skills') }}</span>

                            </div>
                        </div>

          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="feature" value="0">
                  <input name="feature" type="checkbox" value="1" {{ $job->feature == 1 ? 'checked' : '' }}>
                      Feature
                    </label>
                  </div>
              </div>
          </div>


          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="status" value="0">
                  <input name="status" type="checkbox" value="1" {{ $job->status == 1 ? 'checked' : '' }}>
                      Status
                    </label>
                  </div>
              </div>
          </div>

                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
