@extends('site.partials.main_index')
@section('title','Edit a Project')
@section('content')
<body class="page-template page-template-template-submit-ads page-template-template-submit-ads-php page page-id-44 logged-in">
@include('freelancers.partials.navbarwork')
<section class="user-pages section-gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4">
               @include('site.partials.sidebar') 
            </div><!--col-lg-3 col-md-4-->
                <div class="col-lg-9 col-md-8 user-content-height">
					<div class="user-detail-section section-bg-white">
					<div class="user-ads user-profile-settings">
						<h4 class="user-detail-section-heading text-uppercase">
							Edit a Work					</h4>						
						<form data-toggle="validator" role="form" method="POST" id="primaryPostForm" action="{{route('project.update',$fjob->id)}}" enctype="multipart/form-data">
							{{ csrf_field() }}
							{{method_field('PATCH')}}
							<!-- user basic information -->
							<div class="form-inline row form-inline-margin">
								<div class="form-group col-sm-10">
                                    <label for="first-name">Name of your project <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="first-name" name="name" class="form-control form-control-sm" placeholder="Example: Web Developer with Ecommerce Experience.." value="{{old('name',$fjob->name)}}">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                </div><!--Firstname-->
								<div class="form-group col-sm-12">
                                    <label for="bio"> Description <span>*</span></label>
                                    <div class="inner-addon">
                                        <textarea name="body" id="breakText" placeholder="enter your job details..">{{old('body',$fjob->body)}}</textarea>
                                        <p>{{$errors->first('body')}}</p>
                                    </div>
                                </div><!--biography-->

                                                   <script type="text/javascript">
  //Some text string that has line breaks in it that need to be removed
  var someText = "{{old('body',$fjob->body)}}";
  //This javascript code replaces all 3 types of line breaks with a single space
  someText = someText.replace(/(\r\n|\n|\r)/gm," ");
  //Replace all double white spaces with single spaces
  someText = someText.replace(/\s+/g," ");
  
  //Put the newly treated text string into the textarea to show the result
  document.getElementById("breakText").value = someText;
</script>
							
							<!-- user basic information -->
							<!-- Contact Details -->
								<div class="form-group col-sm-2">
                                    <label for="phone">Budget From *</label>
                                    <div class="inner-addon">
                                        <input type="number" id="budget" class="form-control form-control-sm" placeholder="Budget" name="budget" value="{{old('budget',$fjob->budget)}}">
                                        {{ $errors->first('budget') }}
                                    </div>
                                </div>
                                <div class="form-group col-sm-3">
                                    <label for="phone">To </label>
                                    <div class="inner-addon">
                                        <input type="number" id="budget_to" class="form-control form-control-sm" placeholder="budget" name="budget_to" value="{{old('budget_to',$fjob->budget_to)}}">
                                        {{ $errors->first('budget_to') }}
                                    </div>
                                </div>

                                <!--Phone Number-->
                                <div class="form-group col-sm-5">
                                    <label for="mobile">Duration <span>*</span></label>
                                    <div class="inner-addon">
                                        
                                        <select class="form-control form-control-sm" id="duration_id" name="duration_id">
                                            @foreach (\App\Models\Freelancers\Duration::all() as $duration)
                                              @if($fjob->duration_id == $duration->id ||old('duration_id') == $duration->id)
                                              <option value="{{$duration->id}}" selected>{{$duration->name}}</option>
                                              @else
                                            <option value="{{$duration->id}}">{{$duration->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        {{$errors->first('duration_id')}}
                                    </div>
                                </div><!--Mobile Number-->
                                
                                
                                <!--Phone Number-->
								<div class="form-group col-sm-5">
                                    <label for="mobile">Work Category <span>*</span></label>
                                    <div class="inner-addon">
                                        
                                    	<select class="form-control form-control-sm" id="fcategory_id" name="fcategory_id">

						    				<option selected disabled>...Select Job Category</option>
			                        		@foreach (\App\Models\Freelancers\Fcategory::all() as $category)
                                              @if($fjob->fcategory_id == $category->id ||old('fcategory_id') == $category->id)
                                              <option value="{{$category->id}}" selected>{{$category->name}}</option>
                                              @else
			                            	<option value="{{$category->id}}">{{$category->name}}</option>
                                                @endif
			                        		@endforeach
						    			</select>
                                        {{$errors->first('fcategory_id')}}
                                    </div>
                                </div><!--Mobile Number-->
								
								<div class="form-group col-sm-5">
                                    <label for="email">Last Date of Apply <span>*</span></label>
                                    <div class="inner-addon">
                                        <input type="text" id="endate" name="endate" class="form-control form-control-sm" placeholder="{{ date('Y/m/d') }}" value="{{ date('Y/m/d') }}">
                                        {{$errors->first('endate')}}
                                    </div>
                                </div><!--Your Email-->
                                <div class="form-group col-sm-5">
                                    <label for="email">Skills</label>
                                    <div class="inner-addon">
                                        <input type="text" id="skills" name="skills" class="form-control form-control-sm" placeholder="skills" value="@foreach($fjob->skillss as $value){{$value->skill}},@endforeach">
                                        {{$errors->first('skills')}}
                                    </div>
                                </div>
								
                                <div class="form-group col-sm-5">
                                    <label for="email">Address</label>
                                    <div class="inner-addon">
                                        <input type="text" id="address" name="address" class="form-control form-control-sm" placeholder="address" value="{{old('address',$fjob->address)}}">
                                        {{$errors->first('address')}}
                                    </div>
                                </div>

                        <div class="form-group col-sm-10">
                                    <div class="inner-addon">
                                        <div class="checkbox pull-left flip">
                                            <label for="remember">By submitting I agree to <a href="{{url('blog/terms-of-use')}}" target="_blank">Merooffer's Terms of Use</a>  and <a href="{{url('blog/privacy-policy')}}" target="_blank">Privacy Policy</a>, <a href="{{url('blog/posting-policy')}}" target="_blank">Posting Policy</a> and I consent to receiving marketing from Merooffer.</label>
                                        </div>
                            <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
							<button type="submit" name="op" value="submit" class="btn btn-primary sharp btn-sm btn-style-one">Submit Now</button>
                        </div>
                    </div>
            </div>
            <!-- Update your Password -->
						</form>
					</div><!--user-ads user-profile-settings-->
				</div><!--user-detail-section-->
			</div><!--col-lg-9-->
		</div><!--row-->
	</div><!--container-->	
</section><!--user-pages section-gray-bg-->

@include('site.partials.footer')
@endsection