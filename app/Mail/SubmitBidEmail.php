<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Bid;

class SubmitBidEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $bid;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Bid $bid)
    {
        $this->bid = $bid;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@merooffer.com')
                    ->subject('Submit Offer Email')
                    ->view('emails.submitBidEmail')
                    ->with([
                        'user' => $this->bid->user->name,
                        'body' => $this->bid->body,
                        'url' => $this->bid->url,
                        'bid' => $this->bid->bid,
                    ]);
    }
}
