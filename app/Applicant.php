<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
  protected $fillable = [
       'application_letter','status','mail_status'
    ];
	
    function jobs() {
        return $this->belongsToMany('App\Job')->withTimeStamps();
    }

    function job() {
        return $this->belongsTo('App\Job');
    }

    function user() {
    	return $this->belongsTo('App\Models\User');
   }

   function profile() {
    	return $this->hasOne('App\Profile', 'profile');
   }
}
