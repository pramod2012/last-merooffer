<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\ProfileContract;
use App\Http\Controllers\BaseController;

class ProfileController extends BaseController
{
    private $page="admin.profile.";

    protected $profileRepo;

    public function __construct(
        ProfileContract $profileRepo
    )
    {
        $this->profileRepo = $profileRepo;
    }

    public function index()
    {
        $list = $this->profileRepo->list('created_at', 'desc');
        $datas = $this->profileRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('profile', 'List of profile');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $users = \App\Models\User::all();
        $this->setPageTitle('profile', 'Create profile');
        return view($this->page.'create',compact('users'));
    }

    public function store(Request $request)
    {
        $data = $this->profileRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('profiles.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->profileRepo->findById($id);
        $this->setPageTitle('profile', 'Edit profile');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->profileRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('profiles.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->profileRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('profile.index', 'Record deleted successfully' ,'success',false, false);
    }
}
