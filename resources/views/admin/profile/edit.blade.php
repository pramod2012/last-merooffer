@extends('admin.profile.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('profiles.update', $data->id) }}" enctype="multipart/form-data">
                       {{csrf_field()}}
                       {{method_field('PATCH')}}
                        <div class="form-group{{ $errors->has('job_title') ? ' has-error' : '' }}">
                            <label for="job_title" class="col-md-4 control-label">Job Title</label>

                            <div class="col-md-6">
                                <input id="job_title" type="text" class="form-control" name="job_title" value="{{ old('job_title',$data->job_title) }}">

                                @if ($errors->has('job_title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('job_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="overview" class="col-md-4 control-label">Overview*</label>

                            <div class="col-md-6">
                                <textarea rows="4" id="overview" type="text" class="form-control" name="overview">{{ old('overview', $data->overview) }}</textarea>
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('overview') }}
                                        </strong>
                                    </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">City</label>
                            <div class="col-md-6">
                                <input id="city" type="text" class="form-control" name="city" value="{{ old('city', $data->city) }}">
                                <span class="text-danger">{{ $errors->first('city') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="province" class="col-md-4 control-label">Province</label>
                            <div class="col-md-6">
                                <input id="province" type="text" class="form-control" name="province" value="{{ old('province', $data->province) }}">
                                <span class="text-danger">{{ $errors->first('province') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="country" class="col-md-4 control-label">Mobile</label>

                            <div class="col-md-6">
                                <input id="country" type="text" class="form-control" name="country" value="{{ old('country', $data->country) }}">
                                <span class="text-danger">{{ $errors->first('country') }}</span>
                            </div>
                        </div>

                        

                    <div class="form-group">
                      <label class="col-md-4 control-label">User ID</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="user_id">
                              <option value="{{$data->user_id}}">{{$data->user->name}}</option>
                            </select>
                                <span class="text-danger">{{ $errors->first('user_id') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
