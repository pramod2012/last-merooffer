<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Socialite;
Use App\Models\User;
use App\SocialIdentity;
use Carbon\Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider($provider)
   {
       if($provider == 'facebook'){
           return Socialite::driver($provider)->redirect();
       }
       else{
           return Socialite::driver($provider)->stateless()->redirect();
       }
   }

   public function handleProviderCallback($provider)
   {
       try {
           if($provider == 'facebook'){
               $user = Socialite::driver($provider)->user();
           }
           else{
               $user = Socialite::driver($provider)->stateless()->user();
           }
       } catch (Exception $e) {
           return redirect('/login');
       }

       $authUser = $this->findOrCreateUser($user, $provider);
       Auth::login($authUser, true);
       return redirect($this->redirectTo);
   }


   public function findOrCreateUser($providerUser, $provider)
   {
       $account = SocialIdentity::whereProviderName($provider)
                  ->whereProviderId($providerUser->getId())
                  ->first();

       if ($account) {
           return $account->user;
       } else {
           $user = User::whereEmail($providerUser->getEmail())->first();

           if (! $user) {
               $user = new User();
               $user->email = $providerUser->getEmail();
               $user->name = $providerUser->getName();
               $user->email_verified_at = Carbon::now();
               $user->password = bcrypt('5TMzNGP$s6*528');
               $user->activation_key = rand(100000, 999999);
               $user->ip = request()->ip();
               $user->save();
           }

           $user->identities()->create([
               'provider_id'   => $providerUser->getId(),
               'provider_name' => $provider,
           ]);

           return $user;
       }
   }
}
