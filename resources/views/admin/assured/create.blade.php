@extends('admin.assured.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form method="POST" action="{{ route('assured.store') }}" aria-label="category" enctype="multipart/form-data">
                        {{csrf_field()}}
                        

                        
                    <div class="form-group">
                        <label for="alias">Name<span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label for="address_1">Summary <span class="text-danger">*</span></label>
                        <input type="text" name="summary" id="summary" placeholder="summary" class="form-control" value="{{ old('summary') }}">
                    </div>
                    <div class="form-group">
                        <label for="desc">Desc</label>
                        <input type="text" name="desc" id="desc" placeholder="Description" class="form-control" value="{{ old('desc') }}">
                    </div>
                    <div class="form-group">
                        <label for="status">Status </label>
                        <select name="status" id="status" class="form-control">
                            <option value="0">Disable</option>
                            <option value="1">Enable</option>
                        </select>
                    </div>
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
