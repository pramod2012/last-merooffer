<?php

namespace App\Models\Freelancers;

use Illuminate\Database\Eloquent\Model;
use App\Models\Freelancers\Fjob;

class Fcategory extends Model
{

    /**
     * @var string
     */
    protected $table = 'fcategories';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'status',
        'order',
        'image',
        'parent_id',
        'desc',
        'featured',
        'slug',
        'menu',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'feature'  =>  'boolean'
    ];

    /**
     * @param $value
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        if (static::whereSlug($slug = str_slug($value))->exists())
        {
            $slug = $this->incrementSlug($slug);
        }

        $this->attributes['slug'] = $slug;
    }

    public function incrementSlug($slug)
    {
        $original = $slug;
        $count = 2;
        while (static::whereSlug($slug)->exists()) 
        {
           $slug = $count++ . "-{$original}";
        }
        return $slug;
    }

    public function fcategory()
    {
        return $this->belongsTo('App\Models\Freelancers\Fcategory', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Freelancers\Fcategory', 'parent_id');
    }

    public function fjobs(){
        return $this->hasMany(Fjob::class,'fcategory_id');
    }

    public function getFjobCount(){
        $count=0;
        $subs=$this->children()->get();
        // dd($subs);
        foreach($subs as $sub){
                $i=$sub->fjobs->count();
                $count+=$i;
        }
        return $count;
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->order = $model->max('order') + 1;
        });
    }
}
