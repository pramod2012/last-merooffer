@extends('site.partials.main_index')
@foreach(\App\Models\Category::where('status',1)->get() as $category)
@if(Request::get('category_id') == $category->id)
@section('title')
    {{$category->name}} - buy or sell {{$category->name}} in Nepal - merooffer.com
@endsection
@section('meta-tags')
<meta name="description" content="{{$category->meta_body}}" />
<meta name="keywords" content="{{$category->meta_keywords}}" />
<meta name="author" content="merooffer.com" />
@endsection
@section('og-tags')
<meta property='og:title' content="{{$category->name}}" />
@if($category->desc == !null)
<meta property='og:description' content='{{$category->desc}}' />@endif
<meta property='og:url' content="{{app('request')->url()}}">
<meta property=" og:type" content="website" />
<meta property="og:site_name" content="{{app('request')->url()}}" />
@endsection

@endif
@endforeach

@section('content')

<body class="archive category category-uncategorized category-1 logged-in">
    <div>
        @include('site.partials.navbar')
        
        @include('site.partials.search')
        <section class="inner-page-content border-bottom top-pad-50" id="scrollbar">
            <div class="container" id="app">

                <search-product />

            </div>

        </section>
        @include('site.partials.footer')
    </div>
    @section('page-script')
    <script type='text/javascript' src="{{ asset('frontend/js/validator.min.js') }}"></script>
    @stop
    @section('extra-script')
    <script type='text/javascript'>
        //     jQuery(document).ready(function() {

    //     jQuery(document).on('click', '.pagination a', function(event) {
    //         event.preventDefault();
    //         jQuery('html, body').animate({
    //     scrollTop: jQuery("#scrollbar").offset().top
    // }, 200);
    //         var page = jQuery(this).attr('href').split('page=')[1];
    //         fetch_data(page);
    //         var l = window.location;
    //         window.history.pushState("", "", l.origin + l.pathname + "?page=" + page);
    //     });

    //     function fetch_data(page) {
    //         jQuery.ajax({
    //             url: "/welcome/pagination?page=" + page,
    //             success: function(products) {
    //                 jQuery('#table_data').html(products);
    //             }
    //         });
    //     }

    // });
    </script>
    @stop
    @endsection
