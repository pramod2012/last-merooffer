<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('mobile')->unique()->nullable();
            $table->string('phone')->unique()->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('address', 255)->nullable();
            $table->string('socket_id')->nullable();
            $table->string('online', 1)->default('N');
            $table->string('activation_key', 100)->nullable();
            $table->boolean('cell_activated')->default(0);
            $table->string('image')->nullable();
            $table->boolean('show_email')->default(0);
            $table->boolean('show_mobile')->default(0);
            $table->boolean('show_phone')->default(0);
            $table->tinyInteger('role')->nullable();
            $table->string('slug')->unique();
            $table->ipAddress('ip');
            $table->datetime('last_login_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
