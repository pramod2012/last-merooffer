@extends('site.partials.main_jobpost')
@section('title',"Mero Offer - $job->name")
@section('content') 
<body class="post-template-default single single-post postid-445 single-format-standard logged-in">
@include('site.partials.navbarjob')
@include('site.partials.searchjob')
<section class="inner-page-content single-post-page">
    <div class="container">
        <!-- breadcrumb -->
        <div class="row"><div class="col-lg-12"><ul class="breadcrumb"><li><a rel="v:url" property="v:title" href="{{route('index')}}"><i class="fas fa-home"></i></a></li>&nbsp;<li class="cAt"><a rel="v:url" href="{{ route('jobcat', $job->category->slug) }}">{{ ucwords($job->category->name) }}</a></li>&nbsp;<li class="active">{{$job->name}}</li></ul></div></div>     <!-- breadcrumb -->
        <!--Google Section-->
                        <!--Google Section-->
                <div class="row">
            <div class="col-md-8">
                <!-- single post -->
                <div class="single-post">
                    <!-- post title-->
<div class="single-post-title">
            <div class="post-price visible-xs visible-sm">      
        <h4>
            @if($job->salary==!null) Rs. {{ number_format($job->salary) }}@endif @if($job->salary_to == !null) - {{number_format($job->salary_to)}} @endif {{$job->salarytype->name}}
        </h4>
    </div>
        <h4 class="text-uppercase">
        <a href="#">{{$job->name}}</a>
        <!--Edit Ads Button-->
    </h4>
    <p class="post-category">
            <i class="far fa-folder-open"></i>:
        <span>
        <a href="{{ route('jobcat', $job->category->slug) }}" title="View all posts in {{ ucwords($job->category->name) }}" >{{ ucwords($job->category->name) }}</a>    </span>
                <i class="fas fa-map-marker-alt"></i>:<span><a href="{{route('jobcity',$job->city->slug)}}">{{$job->city->name}}</a></span>
    </p>
</div><br>
<!-- post title-->
<!-- single post carousel-->
@if(@$firmuser->img1== !null || @$firmuser->img2== !null)
<div id="single-post-carousel" class="carousel slide single-carousel" data-ride="carousel" data-interval="3000">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        @if(@$firmuser->img1== !null)
        <div class="item active">
            <a href="{{asset('storage/'. @$firmuser->img1)}}" target="_blank">
            <img class="img-responsive" src="{{url('storage/'. @$firmuser->img1)}}" alt="{{$job->name}}"></a>
        </div>@endif
        @if(@$firmuser->img2== !null)
        <div class="item">
            <a href="{{asset('storage/'. @$firmuser->img2)}}" target="_blank">
            <img class="img-responsive" src="{{url('storage/'. @$firmuser->img2)}}" alt="{{$job->name}}"></a>
        </div>@endif
    </div>
    <!-- slides number -->
    <div class="num">
        <i class="fas fa-camera"></i>
        <span class="init-num">1</span>
        <span>of</span>
        <span class="total-num"></span>
    </div>
    <!-- Left and right controls -->
    <div class="single-post-carousel-controls">
        <a class="left carousel-control" href="#single-post-carousel" role="button" data-slide="prev">
            <span class="fas fa-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#single-post-carousel" role="button" data-slide="next">
            <span class="fas fa-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- Left and right controls -->
</div>@endif
<!-- single post carousel-->                                        <!-- ad deails -->
                    <div class="border-section border details">
                        <h4 class="border-section-heading text-uppercase"><i class="far fa-file-alt"></i>General Details</h4>
                        <div class="post-details">
                            <ul class="list-unstyled clearfix">
                                <li>
                                    <p>Job ID: 
                                    <span class="pull-right flip">
                                        <i class="fas fa-hashtag IDIcon"></i>
                                        {{$job->id}}</span>
                                    </p>
                                </li><!--PostDate-->
                                <li>
                                    <p>Added: 
                                    <span class="pull-right flip">{{$job->created_at->diffForHumans()}}
                                    </span>
                                    </p>
                                </li><!--PostDate-->                                
                                <!--Price Section -->
                                <li>
                                    <p>Views: 
                                    <span class="pull-right flip">
                                        {{$job->views}}
                                    </span>
                                    </p>
                                </li><!--Views-->
                                <li>
                                    <p>Last Date to Apply: 
                                    <span class="pull-right flip">
                                        {{ ucwords($job->endate) }}</span>
                                    </p>
                                </li><!--Sale Price-->
                                <li>
                                    <p>Education
                                    <span class="pull-right flip">{{$job->education}}</span>
                                    </p>
                                </li>
                                <li>
                                    <p>Level
                                    <span class="pull-right flip">{{$job->level->name}}</span></p>
                                </li>
                                <li>
                                    <p>Number of Vacancy
                                    <span class="pull-right flip">[ {{$job->vac_no}} ]</span></p>
                                </li>
                                <li>
                                    <p>Industry
                                    <span class="pull-right flip">{{$job->industry->name}}</span></p>
                                </li>
                                <li>
                                    <p>
                                        Salary <span class="pull-right flip">
                                            @if($job->salary==!null) Rs. {{ number_format($job->salary) }}@endif @if($job->salary_to == !null) - {{number_format($job->salary_to)}} @endif {{$job->salarytype->name}}
                                        </span>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Position Type <span class="pull-right flip">{{$job->positiontype->name}}</span>
                                    </p>
                               </li>
                                <li>
                                    <p>
                                        Experience  <span class="pull-right flip">
                                    {{$job->experience->name}}</span>
                                    </p>
                                </li>
                                @if($job->address ==!null)
                                <li>
                                    <p>
                                        Address  <span class="pull-right flip">
                                    {{$job->address}}</span>
                                    </p>
                                </li>
                                @endif
                            </ul>
                        </div><!--post-details-->
                    </div>
                    <!-- ad details -->                 
                    <!--PostVideo-->
                                        <!--PostVideo-->
                    <!-- post description -->
                    <div class="border-section border description">
                        <h4 class="border-section-heading text-uppercase">
                        Description
                    </h4>
                        <p><?php $str = $job->body;
                        echo nl2br($str);  ?>
                            </p>
    @if($job->skillss->count() > 0)
                <div class="tags">
                            <span>
                                <i class="fas fa-tags"></i>
                                Skills Required :
                            </span>
                            @foreach($job->skillss as $skill)
                            <a href="{{route('jobskill',$skill->slug)}}" rel="tag">{{$skill->skill}}</a>
                            @endforeach

                           </div>
    @endif
                    </div>
                    <!-- post description -->
                </div>
                <!-- single post -->
            </div><!--col-md-8-->
            <div class="col-md-4">
                <aside class="sidebar">
                    <div class="row">
                                                <!--Widget for style 1-->
                        <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                            <div class="widget-box ">
                                                                <div class="widget-title price">
                                    <h3 class="post-price">                                     
                                   @if($job->salary==!null) Rs. {{ number_format($job->salary) }}@endif @if($job->salary_to == !null) - {{number_format($job->salary_to)}} @endif {{$job->salarytype->name}}
                                        <span class="ad_type_display">
                                              {{$job->positiontype->name}}                                           </span>
                                          </h3>
                                    
                                </div><!--price-->
                                    
                                <div class="widget-content widget-content-post">
                                    <div class="author-info border-bottom widget-content-post-area">
                                        <div class="media">
                                            <div class="media-left">
                                                <img class="media-object" src="{{asset('frontend/images/user.png')}}" alt="{{@$firmuser->name}}">
                                            </div><!--media-left-->
                                            <div class="media-body">
                                                <h5 class="media-heading text-uppercase">
                                                    {{@$firmuser->name}}
                                                </h5>
                                                <p>Member Since: &nbsp;{{$job->user->updated_at->format('F j, Y')}}</p>
                                                <a href="{{route('jobuser',$job->user->id)}}">See all Jobs</a>


                                        <div class="user-make-offer-message widget-content-post-area">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="btnWatch"> 
                                               {{-- <form method="get" class="fav-form clearfix">
        @if(!Auth::check())<a href="{{url('login')}}">@endif          
            <button class="watch-later text-uppercase" id="{{'btn____'.$job->user->id}}" data-auth_id="@if(Auth::check()) {{ Auth::user()->id }} @endif" data-user_id="{{ $job->user->id }}" type="button" onclick="addToFollow(this)" 
                                    {{$job->user->isAddedToFollowedCompany($job->user->id)?"disabled":""}}>
                                    
                {{$job->user->isAddedToFollowedCompany($job->user->id)?"Followed":"Follow"}} </button>
            @if(!Auth::check())</a>@endif

            </form> --}}
        </li>
    </ul>
</div>
{{--
            <script type="text/javascript">
function addToFollow(el){
  var btn=jQuery(el);
  var auth_id=btn.attr('data-auth_id');
  var user_id=btn.attr('data-user_id');
  jQuery.ajax({
    url:'{{route("companyFollow")}}',
    method:"get",
    data:{'user_id':user_id,'auth_id':auth_id,'_token':'{{csrf_token()}}'},
    success:function(data){
      if(data.status=="1"){
        console.log("added");
          jQuery("#btn____"+user_id).attr("disabled",true).html("Added");
      }else{
        console.log("else");
      }
    },
    error:function(data){
      console.log("error");
    }
  });
  // alert("Hi");
}
</script>--}}
        <div class="clearfix"></div>
                                                        
                                            </div><!--media-body-->
                                        </div><!--media-->
                                    </div><!--author-info-->
                                </div><!--widget-content-->
                                <div class="widget-content widget-content-post">
                                    <div class="contact-details widget-content-post-area">
                                        <h5 class="text-uppercase">Contact Details :</h5>
                                        <ul class="list-unstyled fa-ul c-detail">
                                            @if(@$firmuser->show_email == 1)
                                            <li><i class="fas fa-li fa-envelope"></i> 
                                                <a href="mailto:{{$firmuser->email}}">
                                                    {{$firmuser->email}}
                                                </a>
                                            </li>@endif
                                            <li><i class="fas fa-li fa-eye"></i>{{$jobcount}} jobs posted</li>
                                            @if(@$firmuser->show_phone == 1)
                                            <li><i class="fas fa-li fa-phone"></i>
                                                {{$firmuser->phone}}</li>@endif
                                                @if(@$firmuser->logo == !null)
                                            <li>
                                                <span class="text-primary">
                                                    <img src="{{asset('upsize_images/'.$firmuser->logo)}}" height="50px"/>
                                                </span>
                                            </li>@endif
                                        </ul>
                                    </div><!--contact-details-->
                                </div><!--widget-content-->
                                                            </div><!--widget-box-->
                        </div><!--col-lg-12 col-md-12 col-sm-6 match-height-->
                                                <!--Widget for style 1-->
                        <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                            <div class="widget-box ">
                                <div class="widget-content widget-content-post">
                                    <div class="user-make-offer-message widget-content-post-area">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="btnWatch">
                                                        @guest
            @else
                @if(Auth::user())
                <?php 
                $id = $job->id;
                $user_id = Auth()->user()->id;
                $exist = DB::table('applicants')
            ->join('jobs', 'applicants.job_id', '=', 'jobs.id')
            ->when($id, function ($query) use ($id) {
                    return $query->where('applicants.job_id', $id);
                })
            ->when($user_id, function ($query) use ($user_id) {
                    return $query->where('applicants.user_id', $user_id);
                })
            ->first();  
        if ($exist == null) {
            $result = 'not exist';          
        } else {
             $result = 'exist';
        } 
                ?>
                    @if ($result == 'exist')
                       <button class="watch-later text-uppercase"><i class="fas fa-check"></i>Applied</button>
                    @else
                    @if($job->user_id !== auth()->user()->id)
                    <a href="{{url("/job/application/$job->id")}}">Apply to this Job</a>
                    @endif
                    @endif
                @endif
            @endguest
         @if(!Auth::check())<a href="{{url("/login")}}"> Apply to this Job</a>@endif
                                                    </li>
                                            <li role="presentation" class="active">
                                                <a href="#message" aria-controls="message" role="tab" data-toggle="tab"><i class="fas fa-envelope"></i>Send Email</a>
                                            </li>
                                        </ul><!--nav nav-tabs-->
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="message">
                                            @if(Session::has('success'))
                                            <div class="alert alert-success">
                                                {{Session::get('success')}}
                                            </div>
                                            @endif
                                            @if($errors->any())
                                            <div class="alert alert-danger">
                                                {{ implode('', $errors->all(':message')) }}
                                            </div>
                                            @endif

                                            <!--ShownMessage-->
                                            <form method="POST" class="form-horizontal"  name="contactForm" action="{{route('create-resume')}}" enctype="multipart/form-data">
                                                @csrf
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="name">Name :</label>
                                                        <div class="col-sm-9">
                                                            <input id="name" data-minlength="5" type="text" class="form-control form-control-xs" name="name" placeholder="Type your name" required>
                                                        </div>
                                                    </div><!--name-->
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="email">Email :</label>
                                                        <div class="col-sm-9">
                                                            <input id="email" type="email" class="form-control form-control-xs" name="email" placeholder="Type your email" required>
                                                        </div>
                                                    </div><!--Email-->
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="subject">CV/Resume:</label>
                                                        <div class="col-sm-9">
                                                            <input id="subject" type="file" class="form-control" name="cv" placeholder="Type your subject" required>
                                                        </div>
                                                    </div><!--Subject-->
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="msg">Message:</label>
                                                        <div class="col-sm-9">
                                                            <textarea id="msg" name="message" class="form-control" placeholder="Type Message" required>Hi, I'm interested in the "{{$job->name}}" job you've listed on Mero Offer. I look forward to hearing from you. Thanks.</textarea>
                                                        </div>
                                                    </div><!--Message-->
                                                    <input type="hidden" name="user_id" value="{{$job->user_id}}">
                                                    <input type="hidden" name="job_id" value="{{$job->id}}">
                                                    <input type="hidden" name="url" value="<?php echo url()->full(); ?>">
                                                    <input type="hidden" name="submit" value="send_message" />
                                                    <button class="btn btn-primary btn-block btn-sm sharp btn-style-one" name="send_message" value="send_message" type="submit">Send Message</button>
                                                </form>
                                            </div><!--message-->
                                                                                                                                    
                                        </div><!--tab-content-->
                                        <!-- Tab panes -->
                                    </div><!--user-make-offer-message-->
                                </div><!--widget-content-->
                            </div><!--widget-box-->
                        </div><!--col-lg-12 col-md-12 col-sm-6 match-height-->
                                                <!--Social Widget-->

                    </div><!--row-->
                </aside><!--sidebar-->
            </div><!--col-md-4-->
        </div><!--row-->
    </div><!--container-->
</section>
<!-- related post section -->
<!-- related blog post section -->
<section class="blog-post-section related-blog-post-section border-bottom">
    <div class="container" style="overflow: hidden;">
        <div class="row">
            <div class="col-sm-6 related-blog-post-head">
                <h4 class="text-uppercase">Related Jobs</h4>
            </div><!--col-sm-6-->
            <div class="col-sm-6">
                <div class="navText text-right flip hidden-xs">
                    <a class="prev btn btn-primary sharp btn-style-one btn-sm"><i class="fas fa-chevron-left"></i></a>
                    <a class="next btn btn-primary sharp btn-style-one btn-sm"><i class="fas fa-chevron-right"></i></a>
                </div>
            </div><!--col-sm-6-->
        </div><!--row-->
        <div class="row">
            <div class="col-lg-12">
                <div class="owl-carousel premium-carousel-v1" data-car-length="4" data-items="4" data-loop="true" data-nav="false" data-autoplay="true" data-autoplay-timeout="3000" data-dots="false" data-auto-width="false" data-auto-height="true" data-right="false" data-responsive-small="1" data-autoplay-hover="true" data-responsive-medium="2" data-responsive-xlarge="4" data-margin="30">
                                    <!--SingleItem-->
                                    @foreach($jobs as $job)
                    <div class="classiera-box-div-v1 item match-height">
                        <figure>
                            <div class="premium-img">@if($job->jobtype->id !== 1)
                                                                <div class="featured-tag">
                                    <span class="left-corner"></span>
                                    <span class="right-corner"></span>
                                    <div class="featured">
                                        <p>{{$job->jobtype->name}}</p>
                                    </div>
                                </div>@endif
                                @if (!empty($job->user->firmuser->logo))
                    <img class="image-responsive" src="{{ asset('storage/'.$job->user->firmuser->logo) }}" alt="{{$job->name}}">
                    @else
                    <img class="image-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$job->name}}">
                    @endif
                                                                        <span class="hover-posts">
                                    <a href="{{route('job_post',$job->slug)}}" class="btn btn-primary sharp btn-sm active">View Ad</a>
                                </span>
                            </div>
                                                        <span class="classiera-price-tag" style="background-color:#00beae; color:#00beae;">
                                <span class="price-text">
                                    @if($job->salary==!null) Rs. {{ number_format($job->salary) }}@endif @if($job->salary_to == !null) - {{number_format($job->salary_to)}} @endif {{$job->salarytype->name}}
                                </span>
                            </span>
                                                        <figcaption>
                                <h5><a href="{{route('job_post',$job->slug)}}">{{$job->name}}</a></h5>
                                <p>
                                    Category :
                    <a href="{{route('jobcat',$job->category->slug)}}" title="View all posts in {{$job->category->name}}">{{$job->category->name}}</a>
                    
                                </p>
                                                                <span class="category-icon-box" style=" background:#00beae; color:#00beae; ">
                                                                            <i class="fas fa-briefcase"></i>
                                                                        </span>
                                                            </figcaption>
                        </figure>
                    </div><!--item-->
                                        <!--SingleItem-->
                    @endforeach
                </div><!--owl-carousel-->
            </div><!--col-lg-12-->
        </div><!--row-->
    </div>
</section><!-- /.related blog post -->
@include('site.partials.footer')
    @endsection