@extends('admin.skill.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Skill</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('skill.store') }}" enctype="multipart/form-data">
                       {{csrf_field()}}

                        <div class="form-group{{ $errors->has('skill') ? ' has-error' : '' }}">
                            <label for="firstname" class="col-md-4 control-label">Skill</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="skill" value="{{ old('skill') }}">

                                @if ($errors->has('skill'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('skill') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        

                  

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
