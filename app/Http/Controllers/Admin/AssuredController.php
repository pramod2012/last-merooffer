<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\AssuredContract;
use App\Http\Controllers\BaseController;

class AssuredController extends BaseController
{
    private $page="admin.assured.";

    protected $assuredRepo;

    public function __construct(
        AssuredContract $assuredRepo
    )
    {
        $this->assuredRepo = $assuredRepo;
    }

    public function index()
    {
        $list = $this->assuredRepo->list('created_at', 'desc');
        $datas = $this->assuredRepo->paginateArrayResults($list->all(), 5);
        $this->setPageTitle('assured', 'List of assureds');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('assured', 'Create assured');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->assuredRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('assured.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->assuredRepo->findById($id);
        $this->setPageTitle('assured', 'Edit assured');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->assuredRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('assured.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->assuredRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('assured.index', 'Record deleted successfully' ,'success',false, false);
    }
}
