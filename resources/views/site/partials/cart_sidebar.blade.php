<aside class="section-bg-white">
  <div class="author-info border-bottom">
    <div class="media">
      <div class="media-left">
        <img class="media-object" src="{{asset('frontend/images/user.png')}}" alt="demo">
      </div><!--media-left-->
      <div class="media-body">
        <h5 class="media-heading text-uppercase">
          {{@Auth::user()->name}}         
                  </h5>
        <p>Member Since :&nbsp;{{ @Auth::user()->created_at}}</p>
                <span><i class="fas fa-circle"></i>Online</span>
              </div><!--media-body-->
    </div><!--media-->
  </div><!--author-info-->

<?php $r = Request::path(); ?>
<ul class="user-page-list list-unstyled">
        <li @if($r == 'profile' || $r == 'edit-profile') class="active" @endif>
            <a href="{{route('profile')}}">
                <span>
                    <i class="fas fa-user"></i>
                    About Me
                </span>
            </a>
        </li><!--About-->
        
        <li @if($r == 'editfirmuser') class="active" @endif>
            <a href="{{url('/editfirmuser')}}">
                <span><i class="fas fa-user"></i>Employer Profile</span>
            </a>
        </li>
        
        
        <li @if($r == 'my-jobs') class="active" @endif>
            <a href="{{url('/my-jobs')}}">
                <span><i class="fas fa-briefcase"></i>My Applied Jobs</span>
            </a>
        </li>
        
        
        <li @if($r == 'user-resumes') class="active" @endif>
            <a href="{{route('user-resumes')}}">
                <span><i class="fas fa-file"></i>Resumes</span>
            </a>
        </li>
        
        
        <li @if($r == 'cart') class="active" @endif>
      <a href="#">
        <span><i class="fas fa-shopping-cart"></i>Carts</span>
        <span class="in-count pull-right flip"></span>
      </a>
    </li>
        <li @if($r == 'follow') class="active" @endif>
            <a href="{{route('follow')}}">
                <span><i class="fas fa-users"></i>Follower</span>
            </a>
        </li><!--follower-->

        <li @if($r == 'inbox') class="active" @endif>
            <a href="{{route('inbox')}}">
                <span><i class="fas fa-envelope"></i>Emails</span>
            </a>
        </li><!--follower-->
</ul>
<div class="user-submit-ad">
        <a href="{{route('job-user.create')}}" class="btn btn-primary sharp btn-block btn-sm @if($r == 'job-user/create') active @else btn-user-submit-ad @endif">
            <i class="icon-left fas fa-plus-circle"></i>
            POST NEW JOB     </a>
    </div><!--user-submit-ad-->
</aside><!--sideBarAffix-->