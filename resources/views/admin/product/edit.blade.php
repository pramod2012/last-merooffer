@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/js/plugins/dropzone/dist/min/dropzone.min.css') }}"/>
@endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-shopping-bag"></i> {{ $pageTitle }} - {{ $subTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row user">
        <div class="col-md-3">
            <div class="tile p-0">
                <ul class="nav flex-column nav-tabs user-tabs">
                    <li class="nav-item"><a class="nav-link active" href="#general" data-toggle="tab">General</a></li>
                    <li class="nav-item"><a class="nav-link" href="#images" data-toggle="tab">Images</a></li>
                    <li class="nav-item"><a class="nav-link" href="#attributes" data-toggle="tab">Attributes</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-content">
                <div class="tab-pane active" id="general">
                    <div class="tile">
                        <form action="{{ route('product.update', $product->id) }}" method="POST" role="form">
                            @csrf
                            {{method_field('PATCH')}}
                            <h3 class="tile-title">Product Information</h3>
                            <hr>
                            <div class="tile-body">
                                <div class="form-group">
                                    <label class="control-label" for="name">Name</label>
                                    <input
                                        class="form-control @error('name') is-invalid @enderror"
                                        type="text"
                                        placeholder="Enter attribute name"
                                        id="name"
                                        name="name"
                                        value="{{ old('name', $product->name) }}"
                                    />
                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                    <div class="invalid-feedback active">
                                        <i class="fa fa-exclamation-circle fa-fw"></i> @error('name') <span>{{ $message }}</span> @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="cell">Mobile Number</label>
                                            <input
                                                class="form-control @error('cell') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter product cell"
                                                id="cell"
                                                name="cell"
                                                value="{{ old('cell', $product->cell) }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('cell') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="brand_id">Price Type</label>
                                            <select name="pricetype_id" id="pricetype_id" class="form-control @error('pricetype_id') is-invalid @enderror">
                                                
                                                @foreach($pricetypes as $value)
                                                    @if ($product->pricetype_id == $value->id)
                                                        <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                                    @else
                                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                                    @endif
                                                @endforeach

                                                
                                            </select>
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('pricetype_id') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="categories">Categories</label>
                                            <select name="category_id[]" id="categories" class="form-control" multiple>
                                                @foreach($categories as $cat)
                                                    @php $check = in_array($cat->id, $product->categories->pluck('id')->toArray()) ? 'selected' : ''@endphp
                                                    <option value="{{ $cat->id }}" {{ $check }}>{{ $cat->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="price">Price</label>
                                            <input
                                                class="form-control @error('price') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter product price"
                                                id="price"
                                                name="price"
                                                value="{{ old('price', $product->price) }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('price') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="brand_id">Price Level</label>
                                            <select name="pricelabel_id" id="pricelabel_id" class="form-control @error('pricelabel_id') is-invalid @enderror">

                                                <option value="">Chose one pricelabel</option>
                                                
                                                @foreach(\App\Models\Pricelabel::all() as $value)
                                                    @if ($product->pricelabel_id == $value->id)
                                                        <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                                    @else
                                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                                    @endif
                                                @endforeach

                                                
                                            </select>
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('pricelabel_id') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="sale_price">Regular Price</label>
                                            <input
                                                class="form-control"
                                                type="text"
                                                placeholder="Enter product regulat price"
                                                id="rgr_price"
                                                name="rgr_price"
                                                value="{{ old('rgr_price', $product->rgr_price) }}"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="url">Youtube URL</label>
                                            <input
                                                class="form-control @error('url') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter product url"
                                                id="url"
                                                name="url"
                                                value="{{ old('url', $product->url) }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('url') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="expired_at">Expired At</label>
                                            <input
                                                class="form-control"
                                                type="text"
                                                placeholder="Enter product expired_at"
                                                id="expired_at"
                                                name="expired_at"
                                                value="{{ old('expired_at', $product->expired_at) }}"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="brand_id">Ad Type</label>
                                            <select name="adtype_id" id="adtype_id" class="form-control @error('adtype_id') is-invalid @enderror">
                                                
                                                @foreach($adtypes as $adtype)
                                                    @if ($product->adtype_id == $adtype->id)
                                                        <option value="{{ $adtype->id }}" selected>{{ $adtype->name }}</option>
                                                    @else
                                                        <option value="{{ $adtype->id }}">{{ $adtype->name }}</option>
                                                    @endif
                                                @endforeach

                                                
                                            </select>
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('adtype_id') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="day_id">Days</label>
                                            <select name="day_id" id="day_id" class="form-control @error('day_id') is-invalid @enderror">
                                                
                                                @foreach($days as $day)
                                                    @if ($product->day_id == $day->id)
                                                        <option value="{{ $day->id }}" selected>{{ $day->name }}</option>
                                                    @else
                                                        <option value="{{ $day->id }}">{{ $day->name }}</option>
                                                    @endif
                                                @endforeach

                                                
                                            </select>
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('day_id') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="brand_id">city</label>
                                            <select name="city_id" id="city_id" class="form-control @error('city_id') is-invalid @enderror">
                                                
                                                @foreach($cities as $city)
                                                    @if ($product->city_id == $city->id)
                                                        <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                                                    @else
                                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                                    @endif
                                                @endforeach

                                                
                                            </select>
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('adtype_id') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="user_id">Users</label>
                                            <select name="user_id" id="user_id" class="form-control @error('user_id') is-invalid @enderror">
                                                <option value="{{ $product->user->id }}" selected>{{ $product->user->name }}</option>

                                                
                                            </select>
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('user_id') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="address">Address</label>
                                            <input
                                                class="form-control @error('url') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter product address"
                                                id="address"
                                                name="address"
                                                value="{{ old('address', $product->address) }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('address') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="brand_id">Condition</label>
                                            <select name="condition_id" id="condition_id" class="form-control @error('condition_id') is-invalid @enderror">
                                                <option value="">Chose one pricelabel</option>
                                                @foreach(\App\Models\Condition::all() as $value)
                                                    @if ($product->condition_id == $value->id)
                                                        <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                                    @else
                                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                                    @endif
                                                @endforeach
                                                    

                                                
                                            </select>
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('condition_id') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="expired_at">Latitude</label>
                                            <input
                                                class="form-control"
                                                type="text"
                                                placeholder="latitude"
                                                id="latitude"
                                                name="latitude"
                                                value="{{ old('latitude', $product->latitude) }}"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="expired_at">Longitude</label>
                                            <input
                                                class="form-control"
                                                type="text"
                                                placeholder="Longitude"
                                                id="longitude"
                                                name="longitude"
                                                value="{{ old('longitude', $product->longitude) }}"
                                            />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="brand_id">Assured type</label>
                                            <select name="assured_id" id="pricetype_id" class="form-control @error('assured_id') is-invalid @enderror">
                                                
                                    @foreach(\App\Models\Assured::all() as $value)
                                                    @if ($product->assured_id == $value->id)
                                                        <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                                    @else
                                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                                    @endif
                                                @endforeach

                                                
                                            </select>
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('assured_id') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label" for="description">Description</label>
                                    <textarea name="desc" id="desc" rows="8" class="form-control">{{ old('desc', $product->desc) }}</textarea>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="hidden" name="status" value="0">
                                            <input class="form-check-input"
                                                   type="checkbox" value="1" 
                                                   id="status"
                                                   name="status"
                                                   {{ $product->status == 1 ? 'checked' : '' }}
                                                />Status
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="hidden" name="featured" value="0">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   id="featured" value="1" 
                                                   name="featured"
                                                   {{ $product->featured == 1 ? 'checked' : '' }}
                                                />Featured
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="hidden" name="sold" value="0">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   id="sold"
                                                   name="sold" value="1" 
                                                   {{ $product->sold == 1 ? 'checked' : '' }}
                                                />sold
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="hidden" name="bid" value="0">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   id="bid" value="1" 
                                                   name="bid"
                                                   {{ $product->bid == 1 ? 'checked' : '' }}
                                                />bid
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="tile-footer">
                                <div class="row d-print-none mt-2">
                                    <div class="col-12 text-right">
                                        <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Product</button>
                                        <a class="btn btn-danger" href="{{ route('product.index') }}"><i class="fa fa-fw fa-lg fa-arrow-left"></i>Go Back</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="images">
                    <div class="tile">
                        <h3 class="tile-title">Upload Image</h3>
                        <hr>
                        <div class="tile-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="" class="dropzone" id="dropzone" style="border: 2px dashed rgba(0,0,0,0.3)">
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        {{ csrf_field() }}

                                    </form>
                                </div>
                            </div>
                            <div class="row d-print-none mt-2">
                                <div class="col-12 text-right">
                                    <button class="btn btn-success" type="button" id="uploadButton">
                                        <i class="fa fa-fw fa-lg fa-upload"></i>Upload Images
                                    </button>
                                </div>
                            </div>
                            @if ($product->images)
                                <hr>
                                <div class="row">
                                    @foreach($product->images as $image)
                                        <div class="col-md-3">
                                            <div class="card">
                                                <div class="card-body">
                                                    <img src="{{ asset('storage/'.$image->full) }}" id="brandLogo" class="img-fluid" alt="img">
                                                    <form action="{{ route('admin.products.images.delete', $image->id) }}" method="POST" role="form">
                                                        @csrf
                                                        {{method_field("DELETE")}}
                                                        <button type="submit">
                                                            <i class="fa fa-fw fa-lg fa-trash"></i>
                                                        </button>
                                                </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="attributes">

                    <div class="row">


<div class="col-md-4">
    <form class="form-horizontal" role="form" method="POST" action="{{route('productattribute.store')}}" enctype="multipart/form-data">
        @csrf

<h2>Make combinations</h2>

<div class="form-group">
    <label class="control-label" for="attributes">Attributes combinations</label>
        <select name="attributeValue[]" id="attributes" class="form-control" multiple>
            @foreach($category->combinations as $attribute)
                @foreach($attribute->attributevalues as $attr)
                    <option value="{{ $attr->id }}">{{ $attribute->name }}: {{ $attr->value }}</option>
                @endforeach
            @endforeach
        </select>
</div>
<div class="form-group">
    <label for="productAttributeQuantity">Quantity <span class="text text-danger">*</span></label>
    <input type="text" name="productAttributeQuantity" id="productAttributeQuantity" class="form-control" placeholder="Set quantity">
</div>
<div class="form-group">
    <label for="productAttributePrice">Price</label>
    <div class="input-group">
        <span class="input-group-addon">{{ config('cart.currency') }}</span>
        <input type="text" name="productAttributePrice" id="productAttributePrice" class="form-control" placeholder="Price">
    </div>
</div>
<div class="form-group">
    <label for="salePrice">Sale Price</label>
    <div class="input-group">
        <span class="input-group-addon">{{ config('cart.currency') }}</span>
        <input type="text" name="salePrice" id="salePrice" class="form-control" placeholder="Sale Price">
    </div>
</div>
<div class="form-group">
    <label for="default">Show as default price?</label> <br />
    <select name="default" id="default" class="form-control select2">
        <option value="0" selected="selected">No</option>
        <option value="1">Yes</option>
    </select>
</div>
<div class="box-footer">
    <div class="btn-group">
        <input type="hidden" name="product_id" value="{{$product->id}}">
        <button type="button" class="btn btn-sm btn-default" onclick="backToInfoTab()">Back</button>
        <button id="createCombinationBtn" type="submit" class="btn btn-sm btn-primary">Create combination</button>
    </div>
</div>

</form>
                    
</div>

<div class="col-md-6">


@if(!$productAttributes->isEmpty())
    <p class="alert alert-info">You can only set 1 default combination</p>
    <ul class="list-unstyled">
        <li>
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Sale Price</th>
                    <th>Attributes</th>
                    <th>Is default?</th>
                    <th>Remove</th>
                </tr>
                </thead>
                <tbody>
                @foreach($productAttributes as $pa)
                    <tr>
                        <td>{{ $pa->id }}</td>
                        <td>{{ $pa->quantity }}</td>
                        <td>{{ $pa->price }}</td>
                        <td>{{ $pa->sale_price }}</td>
                        <td>
                            <ul class="list-unstyled">
                                @foreach($pa->attributesValues as $item)
                                    <li>{{ $item->attribute->name }} : {{ $item->value }}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>
                            @if($pa->default == 1)
                                <button class="btn btn-success"><i class="fa fa-check"></i></button>
                            @else
                                <button class="btn btn-danger"><i class="fa fa-remove"></i></button>
                            @endif
                        </td>
                        <td class="btn-group">
                            <form method="POST" action="{{route('productattribute.destroy',$pa->id)}}">
                                @csrf
                                {{method_field('DELETE')}}
                                <button type="submit"
                                    onclick="return confirm('Are you sure?')"
                                    href=""
                                    class="btn btn-sm btn-danger">
                                <i class="fa fa-trash"></i> </button>
                            </a>
                                
                            </form>
                            
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </li>
    </ul>
@else
    <p class="alert alert-warning">No combination yet.</p>
@endif

</div>



                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('backend/js/plugins/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/dropzone/dist/min/dropzone.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/bootstrap-notify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/app.js') }}"></script>
    <script>
        Dropzone.autoDiscover = false;

        $( document ).ready(function() {
            $('#categories').select2();

            let myDropzone = new Dropzone("#dropzone", {
                paramName: "image",
                addRemoveLinks: false,
                maxFilesize: 4,
                parallelUploads: 2,
                uploadMultiple: false,
                url: "{{ route('admin.products.images.upload') }}",
                autoProcessQueue: false,
            });
            myDropzone.on("queuecomplete", function (file) {
                window.location.reload();
                showNotification('Completed', 'All product images uploaded', 'success', 'fa-check');
            });
            $('#uploadButton').click(function(){
                if (myDropzone.files.length === 0) {
                    showNotification('Error', 'Please select files to upload.', 'danger', 'fa-close');
                } else {
                    myDropzone.processQueue();
                }
            });
            function showNotification(title, message, type, icon)
            {
                $.notify({
                    title: title + ' : ',
                    message: message,
                    icon: 'fa ' + icon
                },{
                    type: type,
                    allow_dismiss: true,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                });
            }
        });
    </script>

    
@endpush
