<?php

namespace App\Repo;

use App\Product;  //product expirable not used
use App\Traits\UploadAble;
use Illuminate\Http\UploadedFile;
use App\Contracts\ProductContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubmitAdEmail;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Attributevalue;
use App\Models\Productattribute;
use App\Models\Amenity;
use App\Models\Product as ModelsProduct;
use Carbon\Carbon;
use App\Shop\Products\Transformations\ProductTransformable;

/**
 * Class ProductRepository
 *
 * @package \App\Repositories
 */
class ProductRepo extends BaseRepo implements ProductContract
{
    use ProductTransformable, UploadAble;

    /**
     * ProductRepository constructor.
     * @param Product $model
     */
    public function __construct(Product $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }
    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function store($request)
    {
        try {
            $product = new Product();

            $product->name = $request->name;
            $product->desc = $request->desc;
            $product->adtype_id = $request->adtype_id;
            $product->pricetype_id = $request->pricetype_id;
            $product->condition_id = $request->condition_id;
            $product->pricelabel_id = $request->pricelabel_id;
            $product->price = $request->price;
            $product->rgr_price = $request->rgr_price;

            $product->cell = $request->cell;
            $product->url = $request->url;
            $product->city_id = $request->city_id;
            $product->address = $request->address;
            $product->longitude = $request->longitude;
            $product->latitude = $request->latitude;
            $product->day_id = $request->day_id;
            $product->user_id = $request->user_id;
            $product->slug = $this->createSlug($request->name);

            $product->featured = $request->featured;
            $product->status = $request->status;
            $product->sold = $request->sold;
            $product->bid = $request->bid;

            $product->save();

            if ($request->has('category_id')) {
                $product->categories()->sync($request->category_id);
            }

            if ($request->day_id == 1) {
                $new = Product::find($product->id);
                $new->expired_at = Carbon::now()->addDays(15);
                $new->save();
            }
            if ($request->day_id == 2) {
                $new = Product::find($product->id);
                $new->expired_at = Carbon::now()->addDays(30);
                $new->save();
            }
            if ($request->day_id == 3) {
                $new = Product::find($product->id);
                $new->expired_at = Carbon::now()->addDays(45);
                $new->save();
            }
            if ($request->day_id == 4) {
                $new = Product::find($product->id);
                $new->expired_at = Carbon::now()->addDays(60);
                $new->save();
            }
            return $product;
        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
        $product = $this->findById($id);

        $product->name = $request->name;
        $product->desc = $request->desc;
        $product->adtype_id = $request->adtype_id;
        $product->pricetype_id = $request->pricetype_id;
        $product->condition_id = $request->condition_id;
        $product->pricelabel_id = $request->pricelabel_id;
        $product->assured_id = $request->assured_id;
        $product->price = $request->price;
        $product->rgr_price = $request->rgr_price;
        $product->bid = $request->bid;
        $product->cell = $request->cell;
        $product->url = $request->url;
        $product->city_id = $request->city_id;
        $product->address = $request->address;
        $product->longitude = $request->longitude;
        $product->latitude = $request->latitude;
        $product->day_id = $request->day_id;
        $product->user_id = $request->user_id;

        $product->featured = $request->featured;
        $product->status = $request->status;
        $product->sold = $request->sold;
        $product->bid = $request->bid;
        $product->update();

        if ($request->has('category_id')) {
            $product->categories()->sync($request->category_id);
        }

        if ($request->status == 1) {
            Mail::to($product->user->email)->send(new SubmitAdEmail($product));
        }

        return $product;
    }

    public function storeProduct($request)
    {
        try {
            $product = new Product();

            $product->name = $request->name;
            $product->desc = $request->desc;
            $product->adtype_id = $request->adtype_id;
            $product->pricetype_id = $request->pricetype_id;
            $product->condition_id = $request->condition_id;
            $product->pricelabel_id = $request->pricelabel_id;
            $product->price = $request->price;
            $product->rgr_price = $request->rgr_price;
            $product->quantity = $request->quantity;
            $product->bid = $request->bid;
            $product->cell = $request->cell;
            $product->url = $request->url;
            $product->city_id = $request->city_id;
            $product->address = $request->address;
            $product->longitude = $request->longitude;
            $product->latitude = $request->latitude;
            $product->day_id = $request->day_id;
            $product->user_id = auth()->user()->id;
            $product->slug = $this->createSlug($request->name);

            $product->save();

            if ($request->has('category_id')) {
                $product->categories()->sync($request->category_id);
            }

            $attributes_checkbox = Attribute::where('type', 'checkbox')->get();

            foreach ($attributes_checkbox as $value) {
                if ($request->has("attribute_id$value->code")) {
                    $code = $request->input("attribute_id$value->code");
                    $product->amenities()->sync($code);
                }
            }

            $attributes_dropdown = Attribute::where('type', 'dropdown')->get();

            foreach ($attributes_dropdown as $value) {
                if ($request->has("$value->code")) {
                    $code = $request->input("$value->code");
                    $product->attributevalues()->attach($code);
                }
            }
            $attributes_radio = Attribute::where('type', 'radio')->get();

            foreach ($attributes_radio as $value) {
                if ($request->has("$value->code")) {
                    $code = $request->input("$value->code");
                    $product->attributevalues()->attach($code);
                }
            }

            $attributes = Attribute::where('type', 'text')->get();

            foreach ($attributes as $key => $value) {
                if ($request->input("$value->code")) {
                    $day = new Attributevalue();
                    $day['value'] = $request->input("$value->code");
                    $day['attribute_id'] = $value->id;
                    $day->save();
                    $product->attributevalues()->attach($day->id);
                }
            }

            if ($request->day_id == 1) {
                $new = Product::find($product->id);
                $new->expired_at = Carbon::now()->addDays(15);
                $new->save();
            }
            if ($request->day_id == 2) {
                $new = Product::find($product->id);
                $new->expired_at = Carbon::now()->addDays(30);
                $new->save();
            }
            if ($request->day_id == 3) {
                $new = Product::find($product->id);
                $new->expired_at = Carbon::now()->addDays(45);
                $new->save();
            }
            if ($request->day_id == 4) {
                $new = Product::find($product->id);
                $new->expired_at = Carbon::now()->addDays(60);
                $new->save();
            }
            return $product;
        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    public function updateProduct($request, $id)
    {
        $product = $this->findById($id);

        $product->name = $request->name;
        $product->desc = $request->desc;
        $product->adtype_id = $request->adtype_id;
        $product->pricetype_id = $request->pricetype_id;
        $product->condition_id = $request->condition_id;
        $product->pricelabel_id = $request->pricelabel_id;
        $product->price = $request->price;
        $product->rgr_price = $request->rgr_price;
        $product->quantity = $request->quantity;
        $product->bid = $request->bid;
        $product->cell = $request->cell;
        $product->url = $request->url;
        $product->city_id = $request->city_id;
        $product->address = $request->address;
        $product->longitude = $request->longitude;
        $product->latitude = $request->latitude;
        $product->day_id = $request->day_id;
        $product->user_id = auth()->user()->id;

        $product->update();

        if ($request->has('category_id')) {
            $product->categories()->sync($request->category_id);
        }

        $product->attributevalues()->detach();
        $product->amenities()->detach();

        $attributes_checkbox = Attribute::where('type', 'checkbox')->get();

        foreach ($attributes_checkbox as $value) {
            if ($request->has("attribute_id$value->code")) {
                $code = $request->input("attribute_id$value->code");
                $product->amenities()->sync($code);
            }
        }

        $attributes_dropdown = Attribute::where('type', 'dropdown')->get();

        foreach ($attributes_dropdown as $value) {
            if ($request->has("$value->code")) {
                $code = $request->input("$value->code");
                $product->attributevalues()->attach($code);
            }
        }

        $attributes_radio = Attribute::where('type', 'radio')->get();

        foreach ($attributes_radio as $value) {
            if ($request->has("$value->code")) {
                $code = $request->input("$value->code");
                $product->attributevalues()->attach($code);
            }
        }

        $attributes = Attribute::where('type', 'text')->get();

        foreach ($attributes as $key => $value) {
            if ($request->input("$value->code")) {
                $day = new Attributevalue();
                $day['value'] = $request->input("$value->code");
                $day['attribute_id'] = $value->id;
                $day->save();
                $product->attributevalues()->attach($day->id);
            }
        }

        return $product;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $product = $this->findById($id);

        foreach ($product->images as $image) {
            $this->deleteOne($image->full);
        }

        $product->delete();
        return $product;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $product = Product::where('slug', $slug)->firstOrFail();
        return $product;
    }

    public function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (!$allSlugs->contains('slug', $slug)) {
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Product::select('slug')->where('slug', 'like', $slug . '%')
            ->where('id', '<>', $id)
            ->get();
    }

    public function searchProduct($request)
    {
        $products = Product::where('status', 1)->with(['images','attributevalues','attributevalues.attribute'])->where(function ($query) use ($request) {
            $title = $request->input('name');
            $address = $request->input('address');
            $city = $request->input('city_id');
            $category = $request->input('category_id');
            $adtype = $request->input('adtype_id');
            $min_price = $request->input('min_price');
            $max_price = $request->input('max_price');
            $condition = $request->input('condition_id');
            $pricetype = $request->input('pricetype_id');

            if (isset($title)) {
                $query->where('name', 'like', "%$title%");
            }
            if (isset($city)) {
                $query->where('city_id', '=', $city);
            }
            if (isset($condition)) {
                $query->where('condition_id', '=', $condition);
            }
            if (isset($category)) {
                $query->whereHas('categories', function ($q) use ($category) {
                    $q->whereCategoryId($category);
                });
            }
            if (isset($address)) {
                $query->where('address', 'like', "%$address%");
            }
            if (isset($adtype)) {
                $query->where('adtype_id', '=', $adtype);
            }
            if (isset($pricetype)) {
                $query->where('pricetype_id', '=', $pricetype);
            }
            if (isset($min_price) && isset($max_price)) {
                $query->where('price', '>=', $min_price)
                    ->where('price', '<=', $max_price);
            }
            $attributes_dropdown = Attribute::where('type', 'dropdown')->get();

            foreach ($attributes_dropdown as $key => $value) {
                $id = $request->input("$value->code");
                if (isset($id)) {
                    $query->whereHas('attributevalues', function ($q) use ($id, $value) {
                        $q->where('attribute_id', $value->id)->whereAttributevalueId($id);
                    });
                }
            }
            $attributes_radio = Attribute::where('type', 'radio')->get();

            foreach ($attributes_radio as $key => $value) {
                $id = $request->input("$value->code");
                if (isset($id)) {
                    $query->whereHas('attributevalues', function ($q) use ($id, $value) {
                        $q->where('attribute_id', $value->id)->whereAttributevalueId($id);
                    });
                }
            }

            $attributes_text = Attribute::where('type', 'text')->get();

            foreach ($attributes_text as $key => $value) {
                $id = $request->input("$value->code");
                if (isset($id)) {
                    $query->whereHas('attributevalues', function ($q) use ($id, $value) {
                        $q->where('attribute_id', $value->id)->where('value', 'LIKE', "%$id%");
                    });
                }
            }

            $attributes_number = Attribute::where('type', 'number')->get();
            foreach ($attributes_number as $key => $value) {
                $id = $request->input("$value->code");
                if (isset($id)) {
                    $query->whereHas('attributevalues', function ($q) use ($id, $value) {
                        $q->where('attribute_id', $value->id)->where('value', $id);
                    });
                }
            }
        })->paginate(12)->appends($request->all());
        return $products;
    }

    public function myAds()
    {
        $user = auth()->user()->id;
        $products = Product::orderBy('created_at', 'desc')->where('user_id', '=', $user)->with('images')->paginate(4);
        return $products;
    }

    public function getCommentsForProduct($id)
    {
        $product = ModelsProduct::with(['comments.user', 'comments.replies.user'])->find($id);
        return $product;
    }
}
