<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\{Comment,Product,User,Category};
use App\Models\Partner;
use App\Models\Wishlist;
use App\Models\City;
use App\Models\Post;
use App\Job;
use App\Models\Reportname;
use App\Models\Categoryattribute;
use App\Models\Attribute;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Contracts\CategoryContract;
use App\Contracts\UserContract;

class FrontendController extends Controller
{
  protected $categoryRepo;
  protected $userRepo;

    public function __construct(CategoryContract $categoryRepo, UserContract $userRepo){
        $this->categoryRepo = $categoryRepo;
        $this->userRepo = $userRepo;
    }

	private $page="site.";

  public function welcome(){
    return view('welcome');
  }

   public function index(){
    $posts = Post::where('is_blog',1)->where('feature',1)->where('status',1)->orderBy('order','asc')->orderBy('updated_at', 'desc')->limit(4)->get();
    $jobs = Job::where('status',1)->get();
    $featured = Product::where('status',1)->where('featured', 1)->limit(12)->get();
      $latest = Product::orderBy('created_at', 'desc')->where('status',1)->limit(18)->get();
      $popular = Product::orderBy('views', 'desc')->where('status',1)->limit(18)->get();
      $old = Product::orderBy('views', 'asc')->where('status',1)->limit(18)->get();
      $partners = Partner::where('status', 1)->orderBy('order', 'asc')->get();
      return view($this->page.'index',compact("latest","partners","featured","popular","jobs","posts",'old'));
   }

   public function user($slug){
    $user = $this->userRepo->findBySlug($slug);
      return view($this->page.'user',compact("user"));
   }
   
  public function menu($category){
    $category = Category::where('slug', $category)->firstOrFail();
    return view($this->page.'menu',['category' => $category]);
   }

   public function findByCity($slug){
    $city = City::all();
    $cit = City::where('slug', $slug)->firstOrFail();
    $id = $cit->id;
    $products = new Product;
    $featured = Product::where('status',1)->where('featured',1)->limit(5)->get();
    $popular = Product::where('status',1)->orderBy('views', 'asc')->limit(5)->get();
    $queries = [];
    if (request()->has('price')){
      $products = $products->orderBy('price', request('price'));
      $queries['price'] = request('price');
    }
    if (request()->has('view')){
      $products = $products->orderBy('views', request('view'));
      $queries['view'] = request('view');
    }

      $products = $products->where('status',1)->orderBy('created_at', 'desc')
                          ->where('city_id',$id)
                          ->paginate(12)
                          ->appends($queries);
      $count = $products->count();
    return view($this->page.'findbycity',compact("cit","city","products","count","featured","popular"));
   }
   //.......... 
   
   public function allCategories(){
    $featured = Product::where('status',1)->where('featured', 1)->limit(6)->get();
    $partners = Partner::where('status', 1)->orderBy('order', 'asc')->get();
    return view($this->page.'all-categories', compact("featured","partners"));
   }
}
