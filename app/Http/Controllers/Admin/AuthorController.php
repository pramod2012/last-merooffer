<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\AuthorContract;
use App\Http\Controllers\BaseController;

class AuthorController extends BaseController
{
    private $page="admin.author.";

    protected $authorRepo;

    public function __construct(
        AuthorContract $authorRepo
    )
    {
        $this->authorRepo = $authorRepo;
    }

    public function index()
    {
        $list = $this->authorRepo->list('created_at', 'desc');
        $authors = $this->authorRepo->paginateArrayResults($list->all(), 5);
        $this->setPageTitle('Authors', 'List of Authors');
        return view($this->page.'index',compact('authors'));
    }

    public function create()
    {
        $categories = $this->authorRepo->list();
        $this->setPageTitle('Authors', 'Create author');
        return view($this->page.'create',compact('categories'));
    }

    public function store(Request $request)
    {
        $data = $this->authorRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('author.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $author = $this->authorRepo->findById($id);
        $this->setPageTitle('Cities', 'Edit author');
        return view($this->page.'edit', compact('author'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->authorRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('author.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->authorRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('author.index', 'Record deleted successfully' ,'success',false, false);
    }
}
