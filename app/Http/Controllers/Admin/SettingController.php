<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use App\Traits\UploadAble;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\BaseController;

/**
 * Class SettingController
 * @package App\Http\Controllers\Admin
 */
class SettingController extends BaseController
{
    use UploadAble;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->setPageTitle('Settings', 'Manage Settings');
        return view('admin.settings.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        if ($request->has('logo') && ($request->file('logo') instanceof UploadedFile)) {

            if (config('settings.logo') != null) {
                $this->deleteOne(config('settings.logo'));
            }
            $logo = $this->uploadOne($request->file('logo'), 'img');
            Setting::set('logo', $logo);

        } elseif ($request->has('favicon') && ($request->file('favicon') instanceof UploadedFile)) {

            if (config('settings.favicon') != null) {
                $this->deleteOne(config('settings.favicon'));
            }
            $favicon = $this->uploadOne($request->file('favicon'), 'img');
            Setting::set('favicon', $favicon);

        } elseif ($request->has('error_image') && ($request->file('error_image') instanceof UploadedFile)) {

            if (config('settings.error_image') != null) {
                $this->deleteOne(config('settings.error_image'));
            }
            $error_image = $this->uploadOne($request->file('error_image'), 'img');
            Setting::set('error_image', $error_image);
        }

        else {

            $keys = $request->except('_token');

            foreach ($keys as $key => $value)
            {
                Setting::set($key, $value);
            }
        }
        return $this->responseRedirectBack('Settings updated successfully.', 'success');
    }
}
