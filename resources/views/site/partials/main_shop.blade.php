<!DOCTYPE html>
<html lang="en-US">
    <head>      
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mero Offer Pasal : Online Shopping In Nepal</title>
    <meta name="description" content="Online Shopping In Nepal"/>
    <meta name="keywords" content="merooffer.com, popular classified in Nepal, real estate buy and rent, next generation of free online classifieds"/>
    <link rel="canonical" href="Mero Offer Pasal: Online Shopping in Nepal"/>
    <link rel="shortcut icon" href="{{asset('frontend/images/icon.png')}}" />
    <meta property='og:title' content='Mero Offer Pasal: Online Shopping In Nepal'/>
    <meta property='og:image' content="{{asset('frontend/images/merooffer.jpg')}}"/>
    <meta property='og:description' content='Online shopping in Nepal'/>
    <meta property='og:url' content='{{app('request')->url()}}'>
    <meta property="og:type" content="website"/>
    <meta property="og:site_name" content="{{app('request')->url()}}"/>
<link rel='stylesheet' id='jquery-ui-css'  href='{{ asset('frontend/css/jquery-ui.min.css') }}' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-css'  href='{{ asset('frontend/css/bootstrap.css') }}' type='text/css' media='all' />
<link rel='stylesheet' id='classiera-components-css'  href="{{ asset('frontend/css/classiera-components.css') }}" type='text/css' media='all' />
<link rel='stylesheet' id='classiera-css' href="{{ asset('frontend/css/classiera.css') }}" type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css' href="{{ asset('frontend/css/fontawesome.css') }}" type='text/css' media='all'/>
<link rel='stylesheet' id='material-design-iconic-font-css'  href="{{ asset('frontend/css/material-design-iconic-font.css') }}" type='text/css' media='all' />
<link rel='stylesheet' id='owl.carousel.min-css'  href="{{ asset('frontend/css/owl.carousel.min.css') }}" type='text/css' media='all' />
<link rel='stylesheet' id='owl.theme.default.min-css'  href="{{ asset('frontend/css/owl.theme.default.min.css') }}" type='text/css' media='all' />
<link rel='stylesheet' id='responsive-css' href="{{ asset('frontend/css/responsive.css') }}" type='text/css' media='all' />
<link rel='stylesheet' id='redux-google-fonts-redux_demo-css' href="{{ asset('frontend/css/css.css') }}" type='text/css' media='all' />
<link rel='stylesheet' id='main-style-css' href="{{ asset('frontend/css/main_style.css') }}" type='text/css' media='all' />
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<style type="text/css" id="wp-custom-css">
            footer .widget-box .widget-title h4 {
    color: #f2fbfe !important;
}       </style>
        <style type="text/css" title="dynamic-css" class="options-output">h1, h1 a{font-family:ubuntu;line-height:36px;font-weight:700;font-style:normal;color:#232323;font-size:36px;}h2, h2 a, h2 span{font-family:ubuntu;line-height:30px;font-weight:700;font-style:normal;color:#232323;font-size:30px;}h3, h3 a, h3 span{font-family:ubuntu;line-height:24px;font-weight:700;font-style:normal;color:#232323;font-size:24px;}h4, h4 a, h4 span{font-family:ubuntu;line-height:18px;font-weight:700;font-style:normal;color:#232323;font-size:18px;}h5, h5 a, h5 span{font-family:ubuntu;line-height:24px;font-weight:normal;font-style:normal;color:#232323;font-size:14px;}h6, h6 a, h6 span{font-family:ubuntu;line-height:24px;font-weight:normal;font-style:normal;color:#232323;font-size:12px;}html, body, div, applet, object, iframe p, blockquote, a, abbr, acronym, address, big, cite, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video{font-family:"Open Sans";line-height:24px;font-weight:normal;font-style:normal;color:#6c6c6c;font-size:14px;}</style>
<style>
/* width */
::-webkit-scrollbar {
  width: 12px;
}

/* Track */
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey; 
  border-radius: 10px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #F5F5F5; 
  border-radius: 10px;
}

/* Handle on hover */
::-webkit-scrollbar-thumb {
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
  background: #009996; 
}
</style>
         </head>
@yield('content')

<script type='text/javascript' src="{{ asset('frontend/js/jquery.js') }}"></script>
<script type='text/javascript' src="{{ asset('frontend/js/mediaelement-and-player.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('frontend/wp-includes/js/media-audiovideo.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('frontend/js/validator.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('frontend/js/bootstrap-dropdownhover.js') }}"></script>
<script type='text/javascript' src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('frontend/js/jquery.matchHeight.js') }}"></script>
<script type='text/javascript' src="{{ asset('frontend/js/infinitescroll.js') }}"></script>
<script type='text/javascript' src="{{ asset('frontend/js/select2.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('frontend/js/classiera.js') }}"></script>
<script type='text/javascript' src="{{ asset('frontend/js/jquery-ui.min.js') }}"></script>

</body>
</html>