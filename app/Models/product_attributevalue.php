<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class product_attributevalue extends Model
{
    protected $fillable = [
        'product_id',
        'attributevalue_id',
    ];
}
