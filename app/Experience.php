<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $table = 'experiences';

    protected $fillable = [
        'name',
        'status',
        'slug'
    ];

    function jobs() {
    	return $this->hasMany('App\Job');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        if (static::whereSlug($slug = str_slug($value))->exists())
        {
            $slug = $this->incrementSlug($slug);
        }

        $this->attributes['slug'] = $slug;
    }

    public function incrementSlug($slug)
    {
        $original = $slug;
        $count = 2;
        while (static::whereSlug($slug)->exists()) 
        {
           $slug = $count++ . "-{$original}";
        }
        return $slug;
    }
}
