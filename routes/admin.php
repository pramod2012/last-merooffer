<?php

Route::group(['prefix'  =>  'admin'], function () {
    Route::get('login', 'Admin\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'Admin\LoginController@login')->name('admin.login.post');
    Route::get('logout', 'Admin\LoginController@logout')->name('admin.logout');
    Route::group(['middleware' => ['auth:admin']], function () {
        Route::get('/', function () {
            return view('admin.dashboard.index');
        })->name('admin.dashboard');
        Route::get('/settings', 'Admin\SettingController@index')->name('admin.settings');
        Route::post('/settings', 'Admin\SettingController@update')->name('admin.settings.update');
    });
});

Route::group(['middleware' => ['auth:admin'], 'prefix'  =>  'admin'], function () {

Route::resource('user','Admin\UserController')->except('show');
Route::resource('email','Admin\EmailController')->except('show');
Route::resource('memail','Admin\MemailController')->except('show');
Route::resource('attribute','Admin\AttributeController')->except('show');
Route::resource('attributevalue', 'Admin\AttributevalueController')->except('show');
Route::resource('amenity', 'Admin\AmenityController')->except('show');
Route::resource('productattribute', 'Admin\ProductattributeController')->except('show');

//Product Admin Controller
Route::resource('orders', 'Admin\OrderController');
Route::resource('product', 'Admin\ProductController')->except('show');
Route::resource('category', 'Admin\CategoryController')->except('show');
Route::resource('contact', 'Admin\ContactController')->except('show');
Route::resource('cities', 'Admin\CityController')->except('show');
Route::resource('adtype', 'Admin\AdtypeController')->except('show');
Route::resource('day', 'Admin\DayController')->except('show');
Route::resource('report', 'Admin\ReportController')->except('show');
Route::resource('pricetype', 'Admin\PricetypeController')->except('show');
Route::resource('bid','Admin\BidController')->except('show');
Route::resource('reportname', 'Admin\ReportnameController')->except('show');
Route::resource('condition', 'Admin\ConditionController')->except('show');
Route::resource('experience', 'Admin\ExperienceController');
//blog Controller
Route::resource('post', 'Admin\PostController')->except('show');
Route::resource('author', 'Admin\AuthorController')->except('show');
Route::resource('mcategory', 'Admin\McategoryController')->except('show');
Route::resource('tag', 'Admin\TagController')->except('show');
// Job Admin Controller
Route::resource('job', 'Admin\JobController')->except('show');
Route::resource('jobtype', 'Admin\JobtypeController')->except('show');
Route::resource('salarytype', 'Admin\SalarytypeController')->except('show');
Route::resource('jobcategory', 'Admin\JobcategoryController')->except('show');
Route::resource('industry', 'Admin\IndustryController')->except('show');
Route::resource('skill', 'Admin\SkillController')->except('show');
Route::resource('level', 'Admin\LevelController')->except('show');
Route::resource('positiontype', 'Admin\PositiontypeController')->except('show');
Route::resource('comment','Admin\CommentController')->except('show');
Route::resource('bid','Admin\BidController')->except('show', 'update');
Route::resource('resume', 'Admin\ResumeController')->except('show');

Route::resource('address', 'Admin\AddressController')->except('show');
Route::resource('courier', 'Admin\CourierController')->except('show');
Route::resource('orderstatus', 'Admin\OrderStatusController')->except('show');
Route::resource('pricelabel', 'Admin\PricelabelController')->except('show');

Route::resource('follower', 'Admin\FollowerController')->except('show','store');
Route::resource('wishlist', 'Admin\WishlistController')->except('show','destroy','store');

Route::resource('profiles', 'Admin\ProfileController')->except('show','store');
Route::resource('education', 'Admin\EducationController')->except('show','store');
Route::resource('applicant', 'Admin\ApplicantController')->except('show','store','create');
Route::resource('work', 'Admin\WorkController')->except('show','store','create');
Route::resource('training', 'Admin\TrainingController')->except('show','store','create');
Route::resource('partner', 'Admin\PartnerController')->except('show');
Route::resource('firmusers','Admin\FirmuserController')->except('show');
Route::resource('mcontact', 'Admin\McontactController')->except('show','store');

Route::resource('fcategory', 'Admin\FcategoryController')->except('show');
Route::resource('fjob', 'Admin\FjobController')->except('show');
Route::resource('assured', 'Admin\AssuredController')->except('show');
});

Route::group(['middleware' => ['auth:admin'],'prefix' => 'api'], function() {
    Route::post('category/save_orders', 'Admin\CategoryController@saveOrders');
    Route::post('partner/save_orders', 'Admin\PartnerController@saveOrders');
    Route::post('product/save_orders', 'Admin\ProductController@saveOrders');
    Route::post('post/save_orders', 'Admin\PostController@saveOrders');
    Route::post('jobcategory/save_orders', 'Admin\JobcategoryController@saveOrders');
    Route::post('job/save_orders', 'Admin\JobController@saveOrders');
    Route::post('attribute/save_orders', 'Admin\AttributeController@saveOrders');
});

