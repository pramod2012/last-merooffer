<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Contracts\ConditionContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests as RulesOf;
use App\Utils as Utils;

class ConditionController extends BaseController
{
    protected $conditionRepo;

    public function __construct(
        ConditionContract $conditionRepo
    )
    {
        $this->conditionRepo = $conditionRepo;
    }

    public function index()
    {
        try{
            $list = $this->conditionRepo->list('created_at', 'desc');
            $datas = $this->conditionRepo->paginateArrayResults($list->all(), 5);
            $data = new Utils\SuccessJsonResponse($datas,"success");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function show($id)
    {
        try{
            $data1 = $this->conditionRepo->findById($id);
            $data = new Utils\SuccessJsonResponse($data1,"success");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function store(Request $request)
    {
        try{
            $data1 = $this->conditionRepo->store($request);
            $data = new Utils\SuccessJsonResponse($data1, "Record added Successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $data1 = $this->conditionRepo->edit($request, $id);
            $data = new Utils\SuccessJsonResponse($data1, "Record updated successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function destroy($id)
    {
        try{
            $data1 = $this->conditionRepo->destroy($id);
            $data = new Utils\SuccessJsonResponse($data1,"Record deleted Successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }
}
