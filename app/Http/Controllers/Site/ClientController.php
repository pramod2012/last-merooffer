<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Job;
use App\Models\User;
use App\Applicant;
use App\Firmuser;
use Illuminate\Support\Facades\DB;
use App\Contracts\FirmuserContract;
use App\Http\Requests\StoreFirmuserRequest;
use App\Http\Requests\UpdateFirmuserRequest;

class ClientController extends Controller
{
    protected $firmuserRepo;
    /**
     * Create a new controller instance.
     *
     * @return vpopmail_del_domain(domain)
     */
    public function __construct(FirmuserContract $firmuserRepo)
    {
        $this->firmuserRepo = $firmuserRepo;
        $this->middleware(['auth','verified']);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    
    public function dashboard() {
        $user_id = Auth()->user()->id;
        $user = User::find($user_id);
        $jobs = Job::where('user_id', $user->id)->orderBy('created_at', 'desc')->paginate(5); 
        return view('client.dashboard')->with('jobs', $jobs);
    }   

    public function shortlist($slug) {
        $job = Job::where('user_id',auth()->user()->id)->where('slug',$slug)->firstOrFail();
        $id = $job->id;
        $applicants = DB::table('applicants')
            ->join('profiles', 'applicants.user_id', '=', 'profiles.user_id')
            ->join('jobs', 'applicants.job_id', '=', 'jobs.id')
            ->join('users', 'applicants.user_id', '=', 'users.id')
            ->when($id, function ($query) use ($id) {
                       return $query->where('applicants.job_id', $id);
                 })  
            ->orderBy('applicants.created_at', 'desc')
            ->get();
        return view('client.shortlist', compact('job', 'applicants'));
    }   

    public function proposal($slug, $user_id) {
         $job = Job::where('user_id',auth()->user()->id)->where('slug',$slug)->firstOrFail();
         $id = $job->id;
         $applicant = DB::table('users')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->join('applicants', 'users.id', '=', 'applicants.user_id')
            ->when($id, function ($query) use ($id) {
                    return $query->where('applicants.job_id', $id);
                })
            ->when($user_id, function ($query) use ($user_id) {
                    return $query->where('applicants.user_id', $user_id);
                })
            ->first();
            
        return view('client.proposal', compact('job', 'applicant'));
    }   

    public function hire($id, $user) {
        $job= Job::where('user_id', auth()->user()->id)->where('id',$id)->firstOrFail();
        $applicant = DB::table('applicants')
            ->when($id, function ($query) use ($id) {
                    return $query->where('job_id', $id);
                })
            ->when($user, function ($query) use ($user) {
                    return $query->where('user_id', $user);
                })
            ->update(['astatus' => 'hired']);   
        return back();
    }  

    public function reject($id, $user) {
        $job= Job::where('user_id', auth()->user()->id)->where('id',$id)->firstOrFail();
         $applicant = DB::table('applicants')
            ->when($id, function ($query) use ($id) {
                    return $query->where('job_id', $id);
                })
            ->when($user, function ($query) use ($user) {
                    return $query->where('user_id', $user);
                })
            ->update(['astatus' => 'rejected']);

        return back();
    }

    public function editfirmuser() {
        $auth_id = auth()->user()->id;
        $firmuser = Firmuser::where('user_id', $auth_id)->first();

        return view('client.editfirmuser',compact("firmuser"));
    }

    public function storeFirmuser(StoreFirmuserRequest $request) { 
        $this->firmuserRepo->storeFirmuser($request);
        return redirect()->back();
    }

    public function updateFirmuser(UpdateFirmuserRequest $request, $id)
    {
        $this->firmuserRepo->editFirmuser($request, $id);
        
        return redirect()->back()->with('success', 'Data Updated');
    }
}
