<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use App\Models\User;

class Contact extends Model
{
    protected $fillable = [
        'name',
        'email',
        'subject',
        'desc',
        'product_id',
        'user_id',
        'status'
        
    ];

    public function product(){
    	return $this->belongsTo(Product::class,'product_id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
