<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Comment;

class SubmitCommentEmailReceipient extends Mailable
{
    use Queueable, SerializesModels;
    protected $comment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@merooffer.com')
                    ->view('emails.submitCommentEmailReceipient')
                    ->with([
                        'id' => $this->comment->product->id,
                        'price' => $this->comment->product->price,
                        'name' =>$this->comment->product->user->name,
                        'email' => $this->comment->product->user->email,
                        'posted_by' =>$this->comment->user->name,
                        'posted_by_email'=>$this->comment->user->email,
                        'body' => $this->comment->body,
                        'slug' => $this->comment->url,
                    ]);
    }
}
