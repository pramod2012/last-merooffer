<?php

namespace App\Repo;

use App\Models\Freelancers\Fjob;
use App\Skill;
use App\Traits\UploadAble;
use Illuminate\Http\UploadedFile;
use App\Contracts\FjobContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubmitFjobEmail;
use Carbon\Carbon;

/**
 * Class jobRepository
 *
 * @package \App\Repositories
 */
class FjobRepo extends BaseRepo implements FjobContract
{
    use UploadAble;

    /**
     * jobRepository constructor.
     * @param job $model
     */
    public function __construct(Fjob $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return job|mixed
     */
    public function store($request)
    {
        try {
            $job = new Fjob();
            $job->name = $request->name;
            $job->fcategory_id = $request->fcategory_id;
            $job->body = $request->body;
            $job->address = $request->address;
            $job->budget = $request->budget;
            $job->budget_to = $request->budget_to;
            $job->duration_id = $request->duration_id;
            $job->endate = $request->endate;
            $job->user_id = $request->user_id;
            $job->expired_at = Carbon::now()->addDays(30);
            $job->jobtype_id = $request->jobtype_id;
            $job->slug = $this->createSlug($request->name);
            $job->status = $request->status;
            $job->feature = $request->feature;
            $job->budget_avg = collect([$request->budget, $request->budget_to])->avg();
            $job->save();

            $this->handleTags($request, $job);

            return $job;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
        $job = $this->findById($id);
            $job->name = $request->name;
            $job->fcategory_id = $request->fcategory_id;
            $job->body = $request->body;
            $job->address = $request->address;
            $job->budget = $request->budget;
            $job->budget_to = $request->budget_to;
            $job->duration_id = $request->duration_id;
            $job->endate = $request->endate;
            $job->user_id = $request->user_id;
            $job->jobtype_id = $request->jobtype_id;
            $job->status = $request->status;
            $job->feature = $request->feature;
            $job->budget_avg = collect([$request->budget, $request->budget_to])->avg();
        $job->update();

        $job->skillss()->detach();

        $this->handleTags($request, $job);

        if($request->has('status') == 1)
        {
            $when = now()->addMinutes(10);
            Mail::to($job->user->email)->later($when, new SubmitFjobEmail($job));
        }

        return $job;
    }

    public function storeFjob($request)
    {
        try {
            $job = new Fjob();
            $job->name = $request->name;
            $job->fcategory_id = $request->fcategory_id;
            $job->body = $request->body;
            $job->address = $request->address;
            $job->budget = $request->budget;
            $job->budget_to = $request->budget_to;
            $job->duration_id = $request->duration_id;
            $job->endate = $request->endate;
            $job->user_id = $request->user_id;
            $job->expired_at = Carbon::now()->addDays(30);
            $job->jobtype_id = 1;
            $job->slug = $this->createSlug($request->name);
            $job->budget_avg = collect([$request->budget, $request->budget_to])->avg();
            $job->save();

            $this->handleTags($request, $job);

            return $job;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function editFjob($request, $id)
    {
        $job = $this->findById($id);
        $job->name = $request->name;
            $job->fcategory_id = $request->fcategory_id;
            $job->body = $request->body;
            $job->address = $request->address;
            $job->budget = $request->budget;
            $job->budget_to = $request->budget_to;
            $job->duration_id = $request->duration_id;
            $job->endate = $request->endate;
            $job->user_id = $request->user_id;
            $job->budget_avg = collect([$request->budget, $request->budget_to])->avg();
        $job->update();

        $job->skillss()->detach();

        $this->handleTags($request, $job);

        return $job;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $job = $this->findById($id);
        $job->delete();
        return $job;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $job = Fjob::where('slug', $slug)->firstOrFail();
        return $job;
    }

    public function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }

    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Fjob::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }

    public function handleTags($request, $job){
    if( ! empty($tags = $request->get('skills'))){

        $tagList = explode(",", $tags);
        // Loop through the tag array that we just created
        foreach ($tagList as $tags) {
        // Get any existing tags
            $tag = Skill::where('skill', '=', $tags)->first();
            // If the tag exists, sync it, otherwise create it
            if ($tag != null) {
                $job->skillss()->attach($tag->id);
            } else {
                $tag = new Skill();
                $tag->skill = $tags;
                $tag->slug = $this->createSlugSkill($tags);
                $tag->save();
                $job->skillss()->attach($tag->id);
            }
        }
      }
        
    }


    public function createSlugSkill($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getSkillRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    
    protected function getSkillRelatedSlugs($slug, $id = 0)
    {
        return Skill::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
}
