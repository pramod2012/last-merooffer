<div id="load" class="row">
    @foreach($products as $key => $value)
    <div class="col-lg-4 col-md-4 col-sm-6 match-height item item-list">
        <div class="classiera-box-div classiera-box-div-v5">
            <figure class="clearfix">
                <div class="premium-img">
                    @if($value->featured == 1)
                    <div class="featured-tag">
                        <span class="left-corner"></span>
                        <span class="right-corner"></span>
                        <div class="featured">
                            <p>Featured</p>
                        </div>
                    </div>
                    @endif
                    @if ($value->images->count() > 0)
                    <img class="img-responsive" src="{{ asset('images_small/'.$value->images->first()->full) }}"
                        alt="{{$value->name}}" style="max-height: 100%">
                    @else
                    <img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}"
                        alt="{{$value->name}}">
                    @endif
                    <span class="hover-posts">
                        <a href="{{ route('product_details',$value->slug) }}">View Ad</a>
                    </span>
                    <span class="price">
                        PRICE: Rs.{{ number_format($value->price) }}</span>
                    <span class="classiera-buy-sel">
                        {{@$value->adtype->name}} </span>
                </div>
                <div class="detail text-center">
                    <span class="price">
                        Rs. {{ number_format($value->price) }} </span>
                    <div class="box-icon">
                        @if($value->user->show_email)
                        <a href="mailto:{{ $value->user->email }}?subject">
                            <i class="fas fa-envelope"></i>
                        </a>@endif
                        @if($value->user->show_mobile)
                        <a href="tel:{{ $value->user->mobile}}"><i class="fas fa-phone"></i></a>@endif
                    </div>
                    <a href="{{ route('product_details',$value->slug) }}"
                        class="btn btn-primary outline btn-style-five">View Ad</a>
                </div>
                <figcaption>
                    <span class="price visible-xs">Rs.{{ number_format($value->price) }}
                    </span>
                    <h5><a href="{{ route('product_details',$value->slug) }}">{{$value->name}}</a></h5>
                    <div class="category">
                        <span>
                            Category :
                            @foreach($value->categories->where('parent_id',!null) as $cat)
                            <a href="{{route('menu', $cat->category->slug)}}"
                                title="View all posts in {{$cat->category->name}}">{{$cat->category->name}}</a> /
                            <a href="{{route('menu', $cat->slug)}}"
                                title="View all posts in {{$cat->name}}">{{$cat->name}}</a>
                            @endforeach </span>
                        <span>Location :
                            <a href="{{route('findByCity',$value->city->slug)}}"
                                title="View all posts in {{$value->city->name}}">{{$value->city->name}}</a>
                        </span>
                    </div>
                    <p class="description">{{substr($value->desc, 0, 340)}}</p>
                </figcaption>
            </figure>
        </div>
    </div>
    @endforeach

</div>
<div class="classiera-pagination">
    <nav aria-label="Page navigation">
        <div id="pagination">
            {!!$products->links()!!}
        </div>
    </nav>
</div>
