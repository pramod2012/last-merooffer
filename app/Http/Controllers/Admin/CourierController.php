<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\CourierContract;
use App\Http\Controllers\BaseController;

class CourierController extends BaseController
{
    private $page="admin.courier.";

    protected $courierRepo;

    public function __construct(
        CourierContract $courierRepo
    )
    {
        $this->courierRepo = $courierRepo;
    }

    public function index()
    {
        $list = $this->courierRepo->list('created_at', 'desc');
        $datas = $this->courierRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('courier', 'List of couriers');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('courier', 'Create courier');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->courierRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('courier.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $courier = $this->courierRepo->findById($id);
        $this->setPageTitle('courier', 'Edit courier');
        return view($this->page.'edit', compact('courier'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->courierRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('courier.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->courierRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('courier.index', 'Record deleted successfully' ,'success',false, false);
    }
}
