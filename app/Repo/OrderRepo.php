<?php

namespace App\Repo;

use App\Models\Order;
use App\Models\User;
use App\Contracts\OrderContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use App\Events\OrderCreateEvent;
use App\Repo\CartRepo;
use App\Shop\Carts\ShoppingCart;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Collection;
use App\Repo\ProductRepo;
use App\Repo\UserRepo;
use App\Product;
use App\Shop\Orders\Transformers\OrderTransformable;
use App\Jsdecena\Baserepo\BaseRepository;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendOrderToCustomerMailable;
use App\Mail\SendOrderToProductOwner;

/**
 * Class EducationRepository
 *
 * @package \App\Repo
 */
class OrderRepo extends BaseRepository implements OrderContract
{
    use OrderTransformable;
    /**
     * EducationRepository constructor.
     * @param Education $model
     */
    public function __construct(Order $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    public function createOrder(array $params): Order
    {
        $order = $this->create($params);

        $orderRepo = new OrderRepo($order);
        $orderRepo->buildOrderDetails(Cart::content());

        $orderRepo->sendEmailToCustomer();
        $user_ids = array_unique($order->products->pluck('user_id')->toArray());
        foreach ($user_ids as $user_id) {
            $user = User::find($user_id);
            Mail::to($user->email)
                ->send(new SendOrderToProductOwner($user, $this->findOrderById($order->id)));
        }

        return $order;
    }

    public function updateOrder(array $params): bool
    {
        return $this->update($params);
    }

    public function findOrderById(int $id): Order
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException($e);
        }
    }

    public function listOrders(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
    {
        return $this->all($columns, $order, $sort);
    }

    public function findProducts(Order $order): Collection
    {
        return $order->products;
    }

    public function associateProduct(Product $product, int $quantity = 1, array $data = [])
    {

        $this->model->products()->attach($product, [
            'quantity' => $quantity,
            'product_name' => $product->name,
            'product_sku' => $product->sku,
            'product_description' => $product->desc,
            'product_price' => $product->price,
            'productattribute_id' => isset($data['productattribute_id']) ? $data['productattribute_id'] : null,
        ]);
        $product->quantity = ($product->quantity - $quantity);

        $product->save();
    }

    public function sendEmailToCustomer()
    {
        Mail::to($this->model->user)
            ->send(new SendOrderToCustomerMailable($this->findOrderById($this->model->id)));
    }

    public function transform()
    {
        return $this->transformOrder($this->model);
    }

    public function listOrderedProducts(): Collection
    {
        return $this->model->products->map(function (\App\Product $product) {
            $product->name = $product->pivot->product_name;
            $product->sku = $product->pivot->product_sku;
            $product->desc = $product->pivot->product_description;
            $product->price = $product->pivot->product_price;
            $product->quantity = $product->pivot->quantity;
            $product->productattribute_id = $product->pivot->productattribute_id;
            return $product;
        });
    }

    public function buildOrderDetails(Collection $items)
    {
        $items->each(function ($item) {
            $productRepo = new ProductRepo(new Product);
            $product = $productRepo->find($item->id);
            if ($item->options->has('productattribute_id')) {
                $this->associateProduct($product, $item->qty, [
                    'productattribute_id' => $item->options->productattribute_id
                ]);
            } else {
                $this->associateProduct($product, $item->qty);
            }
        });
    }

    public function getAddresses(): Collection
    {
        return $this->model->address()->get();
    }

    public function getCouriers(): Collection
    {
        return $this->model->courier()->get();
    }
}
