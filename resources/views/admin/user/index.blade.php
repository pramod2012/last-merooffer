@extends('admin.user.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          <h3 class="box-title">List of users</h3>
        </div>
        <div class="col-sm-4">
          @include('admin.partials.flash')
          <a class="btn btn-primary" href="{{route('user.create')}}">Add new user</a>
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>
     <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                <th width="2%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">ID</th>
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending">Image</th>
                <th width="10%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending">Email</th>
                <th width="5%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending">Name</th>
                <th width="5%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Mobile</th>
                <th width="5%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Phone</th>
                
                <th width="5%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Email Verified</th>
                <th width="5%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Show Email</th>
                <th width="5%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Show Mobile</th>
                <th width="5%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Show Phone</th>
                <th width="5%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Activation Key</th>
                
                <th width="3%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Mobile Activation</th>
                <th width="3%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Ip</th>
                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="2" aria-label="Action: activate to sort column ascending">Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach($users as $data)
                <tr role="row" class="odd">
                  <td class="sorting_1">{{ $data->id }}</td>
                  <td>
                    <img src="{{asset('images_small/'.$data->image)}}" height="40" width="40">
                  </td>
                  <td>{{ $data->email }}</td>
                  <td class="hidden-xs">{{ $data->name }}</td>
                  <td class="hidden-xs">{{ $data->mobile }}</td>
                  <td>{{$data->phone}}</td>
                  
                  <td class="hidden-xs">
                    {{$data->email_verified_at}}
                  </td>
                  <td class="hidden-xs">
                    <i class="{{$data->show_email == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                  </td>
                  <td class="hidden-xs">
                    <i class="{{$data->show_mobile == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                  </td>
                  <td class="hidden-xs">
                    <i class="{{$data->show_phone == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                  </td>
                  <td class="hidden-xs">{{ $data->activation_key }}</td>
                  <td class="hidden-xs">
                    <i class="{{$data->cell_activated == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                  </td>
                  <td>{{$data->ip}}</td>
                  <td>
                    
                    <form class="row" method="POST" action="{{route('user.destroy',$data->id) }}" onsubmit = "return confirm('Are you sure?')">
                        {{ csrf_field() }}
                        {{method_field('DELETE')}}
                        <a href="{{route('user.edit', $data->id)}}" class="btn btn-warning col-sm-3 col-xs-5 btn-margin"><i class="fa fa-pencil-square-o large"></i></a>
                         <button type="submit" class="btn btn-danger col-sm-3 col-xs-5 btn-margin">
                          <i class="fa fa-trash large"></i>
                        </button>
                      
                    </form>
                  </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      {{ $users->links() }}
    </div>
  </div>
  <!-- /.box-body -->
</div>
    </section>
    <!-- /.content -->
  </div>
@endsection