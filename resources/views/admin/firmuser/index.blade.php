@extends('admin.firmuser.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          <h3 class="box-title">{{$subTitle}}</h3><br>
        </div>
        <div class="col-sm-4">

          @include('admin.partials.flash')
          <a class="btn btn-primary" href="{{route('firmusers.create')}}">Add new Firmuser</a>
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>
      
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <table id="example9" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">ID</th>
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Name</th>
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Logo</th>
                <th width="10%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">URL</th>
                <th width="10%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Desc</th>
                
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Phone</th>
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Email</th>
                
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Show Email</th>
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Show Phone</th>
                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="2" aria-label="Action: activate to sort column ascending">Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach($datas as $value)  
                <tr data-category-id="{{ $value->id }}" role="row" class="odd">
                  <td class="sorting_1"> {{$value->id}}</td>
                  
                  <td class="hidden-xs">{{$value->name}}</td>
                  <td class="sorting_1">
                    <img src="{{@asset('storage/'.$value->logo)}}" height="70" width="70">
                  </td>
                  <td class="hidden-xs">{{$value->url}}</td>
                  <td class="hidden-xs">{{substr($value->desc,0,40)}}</td>
                  <td class="hidden-xs">{{$value->phone}}</td>
                  <td class="sorting_1">{{$value->email}}</td>
                   <td>
                     <i class="{{$value->show_email == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                   </td>
                   <td>
                     <i class="{{$value->show_phone == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                   </td>
                  <td> 
                        <a href="{{ route('firmusers.edit',$value->id)}}" class="btn btn-warning col-sm-3 col-xs-5 btn-margin">
                        <i class="fa fa-pencil-square-o large"></i>
                        </a>
                    <form class="row" method="POST" action="{{ route('firmusers.destroy',$value->id) }}" onsubmit = "return confirm('Are you sure?')">
                      {{csrf_field()}}
                      {{method_field('DELETE')}}
                      <button type="submit" class="btn btn-danger col-sm-3 col-xs-5 btn-margin">
                        <i class="fa fa-trash large"></i>
                      </button>
                      </form>
                  </td>
              </tr>
        @endforeach
            </tbody>
          </table>
          
        </div>
      </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
</div>
    </section>
    <!-- /.content -->
  </div>
@endsection