@extends('site.partials.main_index')
@section('title', 'Welcome to Mero freelancers - Mero Offer')
@section('content')
<body class="home page-template page-template-template-homepage-v5 page-template-template-homepage-v5-php page page-id-6 logged-in">
@include('freelancers.partials.navbarwork')
@include('freelancers.partials.searchwork')
<!--search-section-->
<section class="classiera-simple-bg-slider">
    <div class="classiera-simple-bg-slider-content text-center">
        <h1>Hire the best <span class="color">Freelancers</span> for any job, online. </h1>
        <h4>Browse works by Popular Work Categories</h4>
        <div class="category-slider-small-box  text-center">
            <ul class="list-inline list-unstyled">
                @foreach(\App\Models\Freelancers\Fcategory::where('featured',1)->limit(6)->get() as $value)
                    <li>
                        <a class="match-height" href="{{route('freelancers.category',$value->slug)}}">
                            <i class="{{$value->font}}"></i>
                            <p>{{$value->name}}</p>
                        </a>
                    </li>
                @endforeach
            </ul><!--list-inline list-unstyled-->
        </div><!--category-slider-small-box-->
    </div><!--classiera-simple-bg-slider-content-->
</section><!--classiera-simple-bg-slider-->
<section class="section-pad-80 category-v5">
    <div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-9 center-block">
                    <h1 class="text-capitalize">Get work done in different categories</h1>
                    <p></p>
                </div><!--col-lg-7-->
            </div><!--row-->
        </div><!--container-->
    </div><!--section-heading-v1-->
    <div class="container">
        <ul class="list-inline list-unstyled categories">
            @foreach(\App\Models\Freelancers\Fcategory::where('parent_id',null)->orderBy('order','asc')->limit(12)->get() as $value)
            <li class="match-height">
                <div class="category-content">                  
                        <i style="color:{{$value->color}};" class="{{$value->font}}"></i>
                                            <h4>
                        <a href="{{ route('freelancers.category',$value->slug) }}">
                            {{$value->name}}                        </a>
                    </h4>
                </div><!--category-box-->
            </li><!--col-lg-4-->
            @endforeach
                    </ul>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="view-all text-center">
                <a href="{{url('freelancers/all-categories')}}" class="btn btn-primary outline btn-style-five">View All Work Categories</a>
            </div>
        </div>
    </div>
</section>
<section class="classiera-premium-ads-v5 border-bottom section-pad-80">
    <div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 center-block">
                    <h3 class="text-capitalize">PREMIUM WORKS</h3>
                    <p>Top Preminum Works of Nepal</p>
                </div>
            </div>
        </div>
    </div>
    <div style="overflow: hidden; padding-top:10px;">
        <div style="margin-bottom: 40px;">          
            <div id="owl-demo" class="owl-carousel premium-carousel-v5" data-car-length="5" data-items="5" data-loop="true" data-nav="false" data-autoplay="true" data-autoplay-timeout="3000" data-dots="false" data-auto-width="false" data-auto-height="true" data-right="false" data-responsive-small="1" data-autoplay-hover="true" data-responsive-medium="2" data-responsive-large="5" data-responsive-xlarge="7" data-margin="10">
                @foreach(\App\Models\Freelancers\Fjob::where('feature',1)->limit(10)->get() as $value)
                <div class="classiera-box-div-v5 item match-height">
                    <figure>
                        <div class="premium-img">@if($value->jobtype->id !== 1)
                                    <div class="featured-tag">
                                        <span class="left-corner"></span>
                                        <span class="right-corner"></span>
                                        <div class="featured">
                                            <p>{{$value->jobtype->name}}</p>
                                        </div>
                                    </div>@endif
                           
                    <img class="media-object" src="{{ asset('frontend/images/empty.png') }}" alt="{{$value->name}}">
                    
                                                        <span class="hover-posts">
                                <a href="{{route('freelancers.show',$value->slug)}}">view project</a>
                            </span>
                        <span class="price">
                            Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif
                        </span>
                                <span class="classiera-buy-sel">
                            {{ ucwords($value->duration->name) }}</span>
                                                    </div><!--premium-img-->
                        <figcaption>
                            <h5><a href="{{route('freelancers.show',$value->slug)}}">{{$value->name}}</a></h5>
                            <div class="category">
                                <span>
                    Category : 
                    <a href="{{route('freelancers.category',$value->fcategory->slug)}}" title="View all posts in {{ ucwords($value->fcategory->name) }}">{{ ucwords($value->fcategory->name) }}</a></span>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                @endforeach
                
                                                            </div>
        </div>
        <div class="navText">
            <a class="prev btn btn-primary radius outline btn-style-five">
                <i class="icon-left zmdi zmdi-arrow-back"></i>
                Previous            </a>
            <a class="next btn btn-primary radius outline btn-style-five">
                Next                <i class="icon-right zmdi zmdi-arrow-forward"></i>
            </a>
        </div>
    </div>
</section>
<section class="classiera-advertisement advertisement-v5 section-pad-80 border-bottom">
    <div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 center-block">
                    <h3 class="text-capitalize">WORKS</h3>
                    <p>A job section where we have three types of jobs listing one with grid, medium grid and the other one with list view latest jobs, popular ads &amp; random jobs also featured ads will show below.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-divs">
        <div class="view-head">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-sm-7 col-xs-8">
                        <div class="tab-button">
                            <ul class="nav nav-tabs" role="tablist">                                
                                <li role="presentation" class="active">
                                    <a href="#allworks" aria-controls="all" role="tab" data-toggle="tab">
                                        Latest Works                                     <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation">                                   
                                    <a href="#popularworks" aria-controls="popular" role="tab" data-toggle="tab">
                                        Popular Works                                     <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation">                                    
                                    <a href="#highbudgetworks" aria-controls="jobs" role="tab" data-toggle="tab">
                                        High Budget Works                                <span class="arrow-down"></span>
                                    </a>
                                </li>
                            </ul><!--nav nav-tabs-->
                        </div><!--tab-button-->
                    </div><!--col-lg-6 col-sm-8-->
                    <div class="col-lg-6 col-sm-5 col-xs-4">
                        <div class="view-as text-right flip">
                            <a id="grid" class="grid active" href="#">
                                <i class="fas fa-th-large"></i>
                            </a>
                            <a id="grid" class="grid-medium" href="#">
                                <i class="fa fa-th"></i>
                            </a>
                            <a id="list" class="list " href="#">
                                <i class="fas fa-th-list"></i>
                            </a>                            
                        </div><!--view-as tab-button-->
                    </div><!--col-lg-6 col-sm-4 col-xs-12-->
                </div><!--row-->
            </div><!--container-->
        </div><!--view-head-->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="allworks">
                <div class="container">
                    <div class="row">
                    <!--FeaturedAds-->
@foreach(\App\Models\Freelancers\Fjob::where('status',1)->orderBy('created_at','desc')->limit(10)->get() as $value)
                    <div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
    <div class="classiera-box-div classiera-box-div-v5">
        <figure class="clearfix">
            <div class="premium-img">
                @if($value->jobtype->id !== 1)
                                    <div class="featured-tag">
                                        <span class="left-corner"></span>
                                        <span class="right-corner"></span>
                                        <div class="featured">
                                            <p>{{$value->jobtype->name}}</p>
                                        </div>
                                    </div>@endif
                       
                    <img class="media-object" src="{{ asset('frontend/images/empty.png') }}" alt="{{$value->name}}">
                   
                                    <span class="hover-posts">                                          
                    <a href="{{route('freelancers.show',$value->slug)}}">View Project</a>
                </span>
                <span class="price">
                    Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif 
                </span>
                        <span class="classiera-buy-sel">{{ ucwords($value->duration->name) }}</span>
                            </div><!--premium-img-->
            <div class="detail text-center">
                                <span class="price">
                                    Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif 
                                </span>
                                <div class="box-icon">
                    @if($value->user->show_email == 1)
                    <a href="mailto:{{ $job->user->email }}?subject">
                        <i class="fas fa-envelope"></i>
                    </a>@endif
                    @if($value->user->show_phone == 1)
                    <a href="tel:{{$job->user->phone}}">
                        <i class="fas fa-phone"></i>
                    </a>@endif
                                    </div>
                <a href="{{route('freelancers.show',$value->slug)}}" class="btn btn-primary outline btn-style-five">View</a>
            </div><!--detail text-center-->
            <figcaption>
                                <span class="price visible-xs">
                                    Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif 
                                </span>
                                <h5><a href="{{route('freelancers.show',$value->slug)}}">{{$value->name}}</a></h5>
                <div class="category">
                    <span>
                    Category : 
                    <a href="{{route('freelancers.category',$value->fcategory->slug)}}" title="View all posts in {{ ucwords($value->fcategory->name) }}">{{ ucwords($value->fcategory->name) }}</a></span>
                                        <span>Location : 
                        <a href="#" title="View all ads in {{$value->address}}">{{$value->address}}</a>
                    </span>
                </div>
                <p class="description">{{substr($value->body, 0, 340)}}</p>
            </figcaption>
        </figure>
    </div><!--row-->
</div><!--col-lg-4-->
@endforeach
                    </div><!--row-->
                </div><!--container-->
            </div><!--tabpanel-->
            <div role="tabpanel" class="tab-pane fade" id="highbudgetworks">
                <div class="container">
                    <div class="row">
@foreach(\App\Models\Freelancers\Fjob::where('status',1)->orderBy('budget_avg','desc')->limit(10)->get() as $value)
                    <div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
    <div class="classiera-box-div classiera-box-div-v5">
        <figure class="clearfix">
            <div class="premium-img">
                @if($value->jobtype->id !== 1)
                                    <div class="featured-tag">
                                        <span class="left-corner"></span>
                                        <span class="right-corner"></span>
                                        <div class="featured">
                                            <p>{{$value->jobtype->name}}</p>
                                        </div>
                                    </div>@endif
                       
                    <img class="media-object" src="{{ asset('frontend/images/empty.png') }}" alt="{{$value->name}}">
                   
                                    <span class="hover-posts">                                          
                    <a href="{{route('freelancers.show',$value->slug)}}">View Project</a>
                </span>
                <span class="price">
                    Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif 
                </span>
                        <span class="classiera-buy-sel">{{ ucwords($value->duration->name) }}</span>
                            </div><!--premium-img-->
            <div class="detail text-center">
                                <span class="price">
                                    Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif 
                                </span>
                                <div class="box-icon">
                    @if($value->user->show_email == 1)
                    <a href="mailto:{{ $job->user->email }}?subject">
                        <i class="fas fa-envelope"></i>
                    </a>@endif
                    @if($value->user->show_phone == 1)
                    <a href="tel:{{$job->user->phone}}">
                        <i class="fas fa-phone"></i>
                    </a>@endif
                                    </div>
                <a href="{{route('freelancers.show',$value->slug)}}" class="btn btn-primary outline btn-style-five">View</a>
            </div><!--detail text-center-->
            <figcaption>
                                <span class="price visible-xs">
                                    Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif 
                                </span>
                                <h5><a href="{{route('freelancers.show',$value->slug)}}">{{$value->name}}</a></h5>
                <div class="category">
                    <span>
                    Category : 
                    <a href="{{route('freelancers.category',$value->fcategory->slug)}}" title="View all posts in {{ ucwords($value->fcategory->name) }}">{{ ucwords($value->fcategory->name) }}</a></span>
                                        <span>Location : 
                        <a href="#" title="View all ads in {{$value->address}}">{{$value->address}}</a>
                    </span>
                </div>
                <p class="description">{{substr($value->body, 0, 340)}}</p>
            </figcaption>
        </figure>
    </div><!--row-->
</div><!--col-lg-4-->
@endforeach
</div><!--row-->
                </div><!--container-->
            </div><!--tabpanel Random-->
            <div role="tabpanel" class="tab-pane fade" id="popularworks">
                <div class="container">
                    <div class="row">
            @foreach(\App\Models\Freelancers\Fjob::where('status',1)->orderBy('views','desc')->limit(10)->get() as $value)
                    <div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
    <div class="classiera-box-div classiera-box-div-v5">
        <figure class="clearfix">
            <div class="premium-img">
                @if($value->jobtype->id !== 1)
                                    <div class="featured-tag">
                                        <span class="left-corner"></span>
                                        <span class="right-corner"></span>
                                        <div class="featured">
                                            <p>{{$value->jobtype->name}}</p>
                                        </div>
                                    </div>@endif
                       
                    <img class="media-object" src="{{ asset('frontend/images/empty.png') }}" alt="{{$value->name}}">
                   
                                    <span class="hover-posts">                                          
                    <a href="{{route('freelancers.show',$value->slug)}}">View Project</a>
                </span>
                <span class="price">
                    Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif 
                </span>
                        <span class="classiera-buy-sel">{{ ucwords($value->duration->name) }}</span>
                            </div><!--premium-img-->
            <div class="detail text-center">
                                <span class="price">
                                    Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif 
                                </span>
                                <div class="box-icon">
                    @if($value->user->show_email == 1)
                    <a href="mailto:{{ $job->user->email }}?subject">
                        <i class="fas fa-envelope"></i>
                    </a>@endif
                    @if($value->user->show_phone == 1)
                    <a href="tel:{{$job->user->phone}}">
                        <i class="fas fa-phone"></i>
                    </a>@endif
                                    </div>
                <a href="{{route('freelancers.show',$value->slug)}}" class="btn btn-primary outline btn-style-five">View</a>
            </div><!--detail text-center-->
            <figcaption>
                                <span class="price visible-xs">
                                    Budget:@if($value->budget==!null) NRs. {{ number_format($value->budget) }}@endif @if($value->budget_to == !null) - {{number_format($value->budget_to)}} @endif 
                                </span>
                                <h5><a href="{{route('freelancers.show',$value->slug)}}">{{$value->name}}</a></h5>
                <div class="category">
                    <span>
                    Category : 
                    <a href="{{route('freelancers.category',$value->fcategory->slug)}}" title="View all posts in {{ ucwords($value->fcategory->name) }}">{{ ucwords($value->fcategory->name) }}</a></span>
                                        <span>Location : 
                        <a href="#" title="View all ads in {{$value->address}}">{{$value->address}}</a>
                    </span>
                </div>
                <p class="description">{{substr($value->body, 0, 340)}}</p>
            </figcaption>
        </figure>
    </div><!--row-->
</div><!--col-lg-4-->
@endforeach
                </div><!--row-->
                </div><!--container-->
            </div><!--tabpanel popular-->
                        <div class="view-all text-center">               
                <a href="{{url('works')}}" class="btn btn-primary outline btn-md btn-style-five">
                    View All Works                </a>
            </div>
        </div><!--tab-content-->
    </div><!--tab-divs-->
</section>

@if($posts->count() > 0)
<section class="blog-post-section blog-post-section-bg section-pad-80 border-bottom">
    <!-- section heading with icon -->
        <div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 center-block">
                    <h1 class="text-capitalize">Latest Blog Posts</h1>
                                    </div>
            </div>
        </div>
    </div><!-- section heading with icon -->
        <div class="container">
        <div class="row">
            @foreach($posts as $post)
            <div class="col-lg-3 col-sm-4 col-md-3">
                <div class="blog-post blog-post-v2 match-height">
                    <div class="blog-post-img-sec">
                        <div class="blog-img">
                            
                            <img width="800" height="500" src="{{@asset('images_small/'. $post->image)}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="blog post" srcset="{{@asset('images_small/'. $post->image)}}" sizes="(max-width: 800px) 100vw, 800px" />
                            <span class="hover-posts">
                                <a href="{{route('blog',$post->slug)}}" class="btn btn-primary radius btn-md active">
                                    View Post
                                </a>
                            </span>
                        </div>
                    </div>
                    <div class="blog-post-content">
                        <h4><a href="{{route('blog',$post->slug)}}">{{$post->name}}</a></h4>
                        <p>
                            <span>
                                <i class="fas fa-user"></i>
                                <a href="#">{{$post->author->name}}</a>
                            </span>
                            <span><i class="far fa-clock"></i>{{$value->created_at->format('M j, Y')}}</span>
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
        <div class="view-all text-center">
        <a href="{{route('blog.all')}}" class="btn btn-primary btn-style-five">View All</a>
    </div>
</section><!-- /.blog post -->
@endif
<section class="call-to-action parallax__400" style="background-image:url()">
    <div class="container">
        <div class="row gutter-15">
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="call-to-action-box match-height">
                    <div class="action-box-heading">
                        <div class="heading-content">
                            <img src="{{ asset('frontend/images/apply-png.png') }}" alt="Sell Safely">
                        </div>
                        <div class="heading-content">
                            <h3>Post a project</h3>
                        </div>
                    </div>
                    <div class="action-box-content">
                        <p>
                            It's easy. Simply post a project you need completed and receive competitive offfers from freelancers within minutes.                     </p>
                    </div>
                </div>
            </div><!--End About Section-->
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="call-to-action-box match-height">
                    <div class="action-box-heading">
                        <div class="heading-content">
                            <img src="{{ asset('frontend/images/choose-freelancers.svg') }}" alt="Sell Safely">
                        </div>
                        <div class="heading-content">
                            <h3>Choose Freelancers</h3>
                        </div>
                    </div>
                    <div class="action-box-content">
                        <p>
                           Whatever your needs, there will be a freelancer to get it done: from web design, mobile app development, virtual assistants, product manufacturing, and graphic design (and a whole lot more).
                        </p>
                    </div>
                </div>
            </div><!--End Sell Safely Section-->
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="call-to-action-box match-height">
                    <div class="action-box-heading">
                        <div class="heading-content">
                            <img src="{{ asset('frontend/images/hired.png') }}" alt="Buy Safely">
                        </div>
                        <div class="heading-content">
                            <h3>Hire Freelancers</h3>
                        </div>
                    </div>
                    <div class="action-box-content">
                        <p>
                            You might get shortlisted for an interview. If you have completed the profile you might get notifications from the website.  Always be prepared for interview and get hired.
                        </p>
                    </div>
                </div>
            </div><!--End Buy Safely Section-->
        </div><!--row gutter-15-->
    </div><!--container-->
</section><!--call-to-action-->

@if($partners->count() > 0)

<section class="partners-v3 section-gray-bg">   
    <div class="container" style="overflow: hidden;">
        <div class="row">   
            <div class="col-lg-12">
                <div class="owl-carousel partner-carousel-v3 partner-carousel-v4" data-car-length="6" data-items="6" data-loop="true" data-nav="false" data-autoplay="true" data-autoplay-timeout="2000" data-dots="false" data-auto-width="false" data-auto-height="true" data-right="false" data-responsive-small="2" data-autoplay-hover="true" data-responsive-medium="4" data-responsive-xlarge="6" data-margin="30">
                    @foreach($partners as $value)
                        <div class="item partner-img match-height">
                            <span class="helper"></span>
                            <a href="//{{$value->link}}" target="_blank">
                                <img src="{{@asset('images_small/'.$value->image)}}" alt="partner">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div><!--col-lg-12-->  
        </div><!--row-->
    </div><!--container-->
</section>
@endif

@include('site.partials.footer')
@endsection