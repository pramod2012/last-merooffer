<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

class Pricelabel extends Model
{
    use HasFactory;
    
    protected $fillable = [
       'name',
       'status',
    ];

    public function categories(){
    	return $this->belongsToMany(Category::class,'category_pricelabel');
    }
}
