<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Job;
use App\JobCategory;
use App\Jobtype;
use App\Models\User;
use App\Profile;
use App\Skill;
use App\Education;
use App\Work;
use App\Industry;
use App\Training;
use App\Firmuser;
use App\Models\Partner;
use Image;
use DB;
use App\Applicant;
use App\Models\Post;
use App\Models\City;

class JobController extends Controller
{
    public function index()
    {
        $posts = Post::where('is_blog', 1)->where('feature', 1)->where('status', 1)->orderBy('order', 'asc')->orderBy('updated_at', 'desc')->limit(4)->get();
        $partners = Partner::where('status', 1)->orderBy('order', 'asc')->get();
        return view('site.jobs', compact('posts', 'partners'));
    }

    public function allJobCategories()
    {
        return view('site.all-jobcategories');
    }

    public function allJobs()
    {
        $jobs = new Job;
        $queries = [];

        if (request()->has('salary_avg')) {
            $jobs = $jobs->orderBy('salary_avg', request('salary_avg'));
            $queries['salary_avg'] = request('salary_avg');
        }
        if (request()->has('views')) {
            $jobs = $jobs->orderBy('views', request('views'));
            $queries['views'] = request('views');
        }
        $jobs = $jobs->where('status', 1)->orderBy('created_at', 'desc')
            ->paginate(6)
            ->appends($queries);
        return view('site.all-jobs', compact("jobs"));
    }

    public function jobCat($slug)
    {
        $job_category = JobCategory::where('slug', $slug)->firstOrFail();
        $id = $job_category->id;
        $jobs = new Job;
        $queries = [];
        if (request()->has('salary_avg')) {
            $jobs = $jobs->orderBy('salary_avg', request('salary_avg'));
            $queries['salary_avg'] = request('salary_avg');
        }
        if (request()->has('views')) {
            $jobs = $jobs->orderBy('views', request('views'));
            $queries['views'] = request('views');
        }
        $jobs = $jobs->where('status', 1)->orderBy('created_at', 'desc')
            ->where('category_id', $id)
            ->paginate(6)
            ->appends($queries);
        return view('site.jobcat', compact("jobs", "job_category"));
    }

    public function jobCity($slug)
    {
        $city = City::where('slug', $slug)->firstOrFail();
        $id = $city->id;
        $jobs = new Job;
        $queries = [];
        if (request()->has('salary_avg')) {
            $jobs = $jobs->orderBy('salary_avg', request('salary_avg'));
            $queries['salary_avg'] = request('salary_avg');
        }
        if (request()->has('views')) {
            $jobs = $jobs->orderBy('views', request('views'));
            $queries['views'] = request('views');
        }
        $jobs = $jobs->where('status', 1)->orderBy('created_at', 'desc')
            ->where('city_id', $id)
            ->paginate(6)
            ->appends($queries);
        return view('site.jobcity', compact("jobs", "city"));
    }

    public function jobSkill($slug)
    {
        $skill = Skill::where('slug', $slug)->firstOrFail();
        $jobs = new Job;
        $queries = [];
        if (request()->has('salary_avg')) {
            $jobs = $jobs->orderBy('salary_avg', request('salary_avg'));
            $queries['salary_avg'] = request('salary_avg');
        }
        if (request()->has('views')) {
            $jobs = $jobs->orderBy('views', request('views'));
            $queries['views'] = request('views');
        }
        $id = $skill->id;
        $jobs = $jobs->orderBy('created_at', 'desc')
            ->whereHas('skillss', function ($query) use ($id) {
                $query->whereSkillId($id);
            })->paginate(6)
            ->appends($queries);
        $count = $jobs->count();
        return view('site.jobskill', compact("skill", "jobs", "count"));
    }

    public function jobUser()
    {
        return view('site.jobuser');
    }

    public function jobShow($slug)
    {
        $job = Job::where('slug', $slug)->firstOrFail();
        $id = $job->id;
        $jobs = Job::where('slug', $slug)->orderBy('views', 'asc')->limit(8)->get();
        $job_categories = JobCategory::all();
        $industries = Industry::all();
        app('db')->table('jobs')->where('id', $id)->increment('views');
        $firmuser = Firmuser::where('user_id', $job->user_id)->first();
        $jobcount = Job::where('user_id', $job->user_id)->count();

        return view('jobpost.show', compact('job', 'jobcount', 'job_categories', 'industries', 'firmuser', 'jobs'));
    }

    public function findByJobType($slug)
    {
        $jobtype = Jobtype::where('slug', $slug)->firstOrFail();
        $id = $jobtype->id;
        $jobs = new Job;
        $queries = [];
        if (request()->has('salary_avg')) {
            $jobs = $jobs->orderBy('salary_avg', request('salary_avg'));
            $queries['salary_avg'] = request('salary_avg');
        }
        if (request()->has('views')) {
            $jobs = $jobs->orderBy('views', request('views'));
            $queries['views'] = request('views');
        }
        $jobs = $jobs->where('status', 1)->orderBy('created_at', 'desc')
            ->where('jobtype_id', $id)
            ->paginate(6)
            ->appends($queries);
        return view('site.findbyjobtype', compact('jobs', 'jobtype'));
    }

    public function findByCompany($id)
    {
        $user = User::find($id);
        $jobs = new Job;
        $queries = [];
        if (request()->has('salary_avg')) {
            $jobs = $jobs->orderBy('salary_avg', request('salary_avg'));
            $queries['salary_avg'] = request('salary_avg');
        }
        if (request()->has('views')) {
            $jobs = $jobs->orderBy('views', request('views'));
            $queries['views'] = request('views');
        }
        $jobs = $jobs->where('status', 1)->orderBy('created_at', 'desc')
            ->where('user_id', $id)
            ->paginate(6)
            ->appends($queries);
        return view('site.findbycompany', compact('jobs', 'user'));
    }

    public function searchJob(Request $request)
    {
        $jobs = $this->filterJobsBasedOnSearchCriteria($request);
        return view('site.search-job', compact('jobs'));
    }
    public function filterJobsBasedOnSearchCriteria(Request $request)
    {
        $jobs = Job::sortSearchableJobs()->where('status', 1)
            ->with(['jobtype', 'user.firmuser', 'salarytype', 'city', 'category'])
            ->where(function ($query) use ($request) {
                $city_id = $request->input('city_id');
                $positiontype = $request->input('positiontype_id');
                $min_salary = $request->input('min_salary');
                $max_salary = $request->input('max_salary');
                $level = $request->input('level_id');
                $industry = $request->input('industry_id');
                $jobcategory = $request->input('category_id');
                $jobtype = $request->input('jobtype_id');
                $experience = $request->input('experience_id');
                if (isset($city_id)) {
                    $query->where('city_id', '=', $city_id);
                }
                if (isset($positiontype)) {
                    $query->where('positiontype_id', '=', $positiontype);
                }
                if (isset($experience)) {
                    $query->where('experience_id', '=', $experience);
                }
                if (($request->has('salarytype_id') && $request->get('salarytype_id') != 1) && isset($min_salary) && isset($max_salary)) {
                    $query->where('salary', '>=', $min_salary)
                        ->where('salary', '<=', $max_salary);
                }
                if ($request->has('salarytype_id') && $request->get('salarytype_id') != '') {
                    $query->where('salarytype_id', $request->get('salarytype_id'));
                }
                if (isset($level)) {
                    $query->where('level_id', '=', $level);
                }
                if (isset($industry)) {
                    $query->where('industry_id', '=', $industry);
                }
                if (isset($jobcategory)) {
                    $query->where('category_id', '=', $jobcategory);
                }
                if (isset($jobtype)) {
                    $query->where('jobtype_id', '=', $jobtype);
                }
            })
            ->paginate(4)->appends($request->all());
        // logger($jobs->toSql());
        return $jobs;
    }
}
