<!--SearchForm-->
<form method="get" action="{{route('freelancers.search')}}">
    <div class="search-form border">
        <div class="search-form-main-heading">
            <a href="#innerSearch" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="innerSearch">
                <i class="fas fa-sync-alt"></i>
                Category Search         </a>
        </div><!--search-form-main-heading-->
        <div id="innerSearch" class="collapse in classiera__inner">
            <!--Price Range-->
            
            <div class="inner-search-box">
                <h5>
                    <span class="currency__symbol">
                    Rs. 
                    </span>
                Budget Range             </h5>
                    
                    <div class="classiera_price_slider">
                        <p>
                          <input data-cursign="Rs." type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
                        </p>                     
                    <div id="slider-range"></div>
                        <input type="hidden" id="classieraMaxPrice" value="1000000">
                        <input type="hidden" id="range-first-val" name="min_budget" value="{{Request::get('min_budget')}}">
                        <input type="hidden" id="range-second-val" name="max_budget" value="{{Request::get('max_budget')}}">
                    </div>                  
                </div>


                <div class="inner-search-box">
                <h5 class="inner-search-heading"><i class="fas fa-list"></i>
                Category               </h5>
                <div class="inner-addon right-addon">
                    <i class="right-addon form-icon fas fa-sort"></i>
                    <select class="form-control form-control-sm" name="fcategory_id">
                        <option value="" selected disabled>Select Category</option>
                        @foreach(\App\Models\Freelancers\Fcategory::where('status',1)->orderBy('order','asc')->where('parent_id',null)->get() as $value)
                        @if(Request::get('fcategory_id') == $value->id)
                        <option value="{{$value->id}}" selected>{{$value->name}}</option>
                        @else
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endif
                        @foreach($value->children as $subcat)
                        @if(Request::get('fcategory_id') == $subcat->id)
                        <option value="{{$subcat->id}}" selected>---{{$subcat->name}}</option>
                        @else
                        <option value="{{$subcat->id}}">---{{$subcat->name}}</option>
                        @endif
                        @endforeach
                        @endforeach
                        
                    </select>
                </div>
            </div>

            <div class="inner-search-box">
                <h5 class="inner-search-heading"><i class="fas fa-clock"></i>
                Project Duration              </h5>
                <div class="inner-addon right-addon">
                    <i class="right-addon form-icon fas fa-sort"></i>
                    <select class="form-control form-control-sm" name="duration_id">
                        <option value="" selected disabled>Select Duration</option>
                        @foreach(\App\Models\Freelancers\Duration::where('status',1)->get() as $value)
                        @if(Request::get('duration_id') == $value->id)
                        <option value="{{$value->id}}" selected>{{$value->name}}</option>
                        @else
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endif
                        @endforeach
                        
                    </select>
                </div>
            </div>

            <div class="inner-search-box">
                <h5 class="inner-search-heading"><i class="fas fa-map-marker-alt"></i>
                Address            </h5>
                <!--SelectCategory-->
                <div class="inner-addon right-addon">    
                <input type="text" name='address' value="{{Request::get('address')}}" placeholder="address" class='form-control form-control-sm'>
            </div>
        </div>


            <div class="inner-search-box-child">
                <p>Work Type</p>
                <div class="radio">
                   @foreach(\App\Jobtype::all() as $value)
                   @if(Request::get('jobtype_id') == $value->id)
                    <input value="{{$value->id}}" id="{{$value->name}}" type="radio" name="jobtype_id" checked>
                    <label for="{{$value->name}}">{{$value->name}}</label>
                    @else
                    <input value="{{$value->id}}" id="{{$value->name}}" type="radio" name="jobtype_id">
                    <label for="{{$value->name}}">{{$value->name}}</label>
                    @endif
                   @endforeach
                </div>
            </div><hr>
            
            <button type="submit" class="btn btn-primary sharp btn-sm btn-style-one btn-block" value="Search">Search</button>
        </div><!--innerSearch-->
    </div><!--search-form-->
</form>
<!--SearchForm-->
