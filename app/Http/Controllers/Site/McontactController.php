<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\McontactContract;
use App\Http\Requests\StoreMcontactRequest;

class McontactController extends Controller
{
    protected $mcontactRepo;

    public function __construct(
        McontactContract $mcontactRepo
    )
    {
        $this->mcontactRepo = $mcontactRepo;
    }

    public function store(StoreMcontactRequest $request)
    {
    	$mcontact = $this->mcontactRepo->store($request);
    	return back()->with('success', 'Thank you! We will get back to you as soon as possible.');
    }
}
