<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Contracts\AdtypeContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests as RulesOf;
use App\Utils as Utils;

class AdtypeController extends BaseController
{
    protected $adtypeRepo;

    public function __construct(
        AdtypeContract $adtypeRepo
    )
    {
        $this->adtypeRepo = $adtypeRepo;
    }

    public function index()
    {
        try{
            $list = $this->adtypeRepo->list('created_at', 'desc');
            $datas = $this->adtypeRepo->paginateArrayResults($list->all(), 5);
            $data = new Utils\SuccessJsonResponse($datas,"success");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function show($id)
    {
        try{
            $data1 = $this->adtypeRepo->findById($id);
            $data = new Utils\SuccessJsonResponse($data1,"success");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function store(Request $request)
    {
        try{
            $data1 = $this->adtypeRepo->store($request);
            $data = new Utils\SuccessJsonResponse($data1,"Record added Successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $data1 = $this->adtypeRepo->edit($request, $id);
            $data = new Utils\SuccessJsonResponse($data1, "Record updated Successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function destroy($id)
    {
        try{
            $data1 = $this->adtypeRepo->destroy($id);
            $data = new Utils\SuccessJsonResponse($data1,"Record deleted Successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }
}
