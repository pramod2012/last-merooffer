<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class product_amenity extends Model
{
    protected $fillable = [
        'product_id',
        'amenity_id',
    ];
}
