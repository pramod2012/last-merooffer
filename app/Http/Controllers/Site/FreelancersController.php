<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Freelancers\Fjob;
use App\Models\Freelancers\Fcategory;
use App\Models\Freelancers\Fapplicant;
use App\Models\Freelancers\fapplicant_fjob;
use App\Http\Requests\StoreFjobRequest;
use App\Http\Requests\StoreFapplicantRequest;
use App\Models\Post;
use App\Models\User;
use App\Models\Partner;
use App\Profile;
use App\Skill;
use DB;

class FreelancersController extends Controller
{
    public function index(){
        $posts = Post::where('is_blog',1)->where('feature',1)->where('status',1)->orderBy('order','asc')->orderBy('updated_at', 'desc')->limit(4)->get();
        $partners = Partner::where('status', 1)->orderBy('order', 'asc')->get();
        return view('freelancers.index',compact('posts','partners'));
    }

    public function allWorks(){
    	$works = Fjob::where('status',1)->orderBy('created_at','desc')->paginate(4);
    	return view('freelancers.allworks',compact('works'));
    }

    public function category($slug){
        $fcategory = Fcategory::where('slug',$slug)->firstOrFail();
        $id = $fcategory->id;
        $jobs = new Fjob;
        $queries = [];
        if (request()->has('budget_avg')){
          $jobs = $jobs->orderBy('budget_avg', request('budget_avg'));
          $queries['budget_avg'] = request('budget_avg');
        }
        if (request()->has('views')){
          $jobs = $jobs->orderBy('views', request('views'));
          $queries['views'] = request('views');
        }
          $works = $jobs->where('status',1)->orderBy('created_at', 'desc')
                              ->where('fcategory_id',$id)
                              ->paginate(6)
                              ->appends($queries);
        return view('freelancers.category',compact("works","fcategory"));
    }

    public function skill($slug){
        $skill = Skill::where('slug',$slug)->firstOrFail();
        $id = $skill->id;
        $jobs = new Fjob;
        $queries = [];
        if (request()->has('budget_avg')){
          $jobs = $jobs->orderBy('budget_avg', request('budget_avg'));
          $queries['budget_avg'] = request('budget_avg');
        }
        if (request()->has('views')){
          $jobs = $jobs->orderBy('views', request('views'));
          $queries['views'] = request('views');
        }
          $works = $jobs->orderBy('created_at', 'desc')
                              ->whereHas('skillss',function($query) use ($id){
                    $query->whereSkillId($id);
                  })->paginate(6)
                  ->appends($queries);
        return view('freelancers.workskill',compact("works","skill"));
    }

    public function user($slug){
        $user = User::where('slug',$slug)->firstOrFail();
        $id = $user->id;
        $jobs = new Fjob;
        $queries = [];
        if (request()->has('budget_avg')){
          $jobs = $jobs->orderBy('budget_avg', request('budget_avg'));
          $queries['budget_avg'] = request('budget_avg');
        }
        if (request()->has('views')){
          $jobs = $jobs->orderBy('views', request('views'));
          $queries['views'] = request('views');
        }
          $works = $jobs->orderBy('created_at', 'desc')
                        ->where('user_id',$id)
                        ->paginate(12)
                  ->appends($queries);
        return view('freelancers.workuser',compact("works","user"));
    }

    public function show($slug){
      $work = Fjob::where('slug',$slug)->firstOrFail();
      app('db')->table('fjobs')->where('slug',$slug)->increment('views');
      $related = Fjob::orderBy('views','asc')->limit(6)->get();
      return view('freelancers.show',compact('work','related'));
    }

    public function allCategories(){
      $featured = Fjob::where('feature',1)->limit(12)->get();
      return view('freelancers.all-categories',compact('featured'));
    }

    public function search(Request $request){
      $fjobs = Fjob::where('status',1)->where(function($query) use ($request){

        $name = $request->input('name');
        $address = $request->input('address');
            
            $min_budget = $request->input('min_budget');
            $max_budget = $request->input('max_budget');
            
            $fcategory = $request->input('fcategory_id');
            $duration = $request->input('duration_id');
            $jobtype = $request->input('jobtype_id');
            if(isset($name)){
                $query->where('name', 'like', "%$name%");
            }
            if(isset($name)){
                $query->where('address', 'like', "%$address%");
            }
            if(isset($min_budget) && isset($max_budget)){
            $query->where('budget_avg' , '>=', $min_budget)
                ->where('budget_avg', '<=', $max_budget);
            }
            
            if(isset($fcategory)){
                $query->where('fcategory_id', '=', $fcategory);
            }
            if(isset($jobtype)){
                $query->where('jobtype_id', '=', $jobtype);
            }
            if(isset($duration)){
                $query->where('duration_id', '=', $duration);
            }
        })->paginate(12);
      return view('freelancers.search',compact('fjobs'));
    }

    public function applicantStore(StoreFapplicantRequest $request){

        if(Profile::where('user_id', auth()->user()->id)->count() == null){
            $user = str_slug(strtolower(auth()->user()->name), '-');
            return redirect("/profile/$user")->with('success','Please complete profile to send offer !');
        }
        else{
        $applicant = new Fapplicant;
        $applicant->application_letter = $request->input('application_letter');
        $applicant->fjob_id = $request->input('fjob_id');
        $applicant->bid = $request->input('bid');
        $applicant->astatus = 'pending';
        $applicant->user_id = auth()->user()->id;
        $applicant->day = $request->day;
        if ($request->hasFile('attachment')){
              $attachment = time() . '.' . $request->attachment->getClientOriginalExtension();
              $request->attachment->move(public_path('storage/attachments'), $attachment);
              $applicant->attachment = $attachment;
            }   
        $applicant->save();

        $applicant_job = new fapplicant_fjob;
        $applicant_job->fapplicant_user_id = auth()->user()->id;
        $applicant_job->fjob_id = $request->input('fjob_id');
        $applicant_job->save();

        return back();
       }
    }

    public function shortlist($slug) {
        $job = Fjob::where('user_id',auth()->user()->id)->where('slug',$slug)->firstOrFail();
        $id = $job->id;
        $applicants = DB::table('fapplicants')
            ->join('profiles', 'fapplicants.user_id', '=', 'profiles.user_id')
            ->join('fjobs', 'fapplicants.fjob_id', '=', 'fjobs.id')
            ->join('users', 'fapplicants.user_id', '=', 'users.id')
            ->when($id, function ($query) use ($id) {
                       return $query->where('fapplicants.fjob_id', $id);
                 })  
            ->orderBy('fapplicants.created_at', 'desc')
            ->get();
        return view('freelancers.shortlist', compact('job', 'applicants'));
    }   

    public function proposal($slug, $user_id) {
         $job = Fjob::where('user_id',auth()->user()->id)->where('slug',$slug)->firstOrFail();
         $id = $job->id;
         $applicant = DB::table('users')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->join('fapplicants', 'users.id', '=', 'fapplicants.user_id')
            ->when($id, function ($query) use ($id) {
                    return $query->where('fapplicants.fjob_id', $id);
                })
            ->when($user_id, function ($query) use ($user_id) {
                    return $query->where('fapplicants.user_id', $user_id);
                })
            ->first();
            
        return view('freelancers.proposal', compact('job', 'applicant'));
    }   

    public function hire($id, $user) {
        $job= Fjob::where('user_id', auth()->user()->id)->where('id',$id)->firstOrFail();
        $applicant = DB::table('fapplicants')
            ->when($id, function ($query) use ($id) {
                    return $query->where('fjob_id', $id);
                })
            ->when($user, function ($query) use ($user) {
                    return $query->where('user_id', $user);
                })
            ->update(['astatus' => 'hired']);   
        return back();
    }  

    public function reject($id, $user) {
        $job= Fjob::where('user_id', auth()->user()->id)->where('id',$id)->firstOrFail();
         $applicant = DB::table('fapplicants')
            ->when($id, function ($query) use ($id) {
                    return $query->where('fjob_id', $id);
                })
            ->when($user, function ($query) use ($user) {
                    return $query->where('user_id', $user);
                })
            ->update(['astatus' => 'rejected']);

        return back();
    }

}
