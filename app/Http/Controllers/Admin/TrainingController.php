<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\TrainingContract;
use App\Http\Controllers\BaseController;

class TrainingController extends BaseController
{
    private $page="admin.training.";

    protected $trainingRepo;

    public function __construct(
        TrainingContract $trainingRepo
    )
    {
        $this->trainingRepo = $trainingRepo;
    }

    public function index()
    {
        $list = $this->trainingRepo->list('created_at', 'desc');
        $datas = $this->trainingRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('training', 'List of training');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('training', 'Create training');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->trainingRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('training.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->trainingRepo->findById($id);
        $this->setPageTitle('training', 'Edit training');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->trainingRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('training.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->trainingRepo->destroy($id);
        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('training.index', 'Record deleted successfully' ,'success',false, false);
    }
}
