<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateJobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'body' => 'required',
            'category_id' => 'required|numeric',
            'industry_id' => 'required|numeric',
            'positiontype_id' => 'required|numeric',            
            'endate' => 'required',
            'level_id' => 'required|numeric',
            'day_id' => 'required|numeric',
            'city_id' => 'required',
            'vac_no' => 'required',
            'education' => 'required',
            'experience_id' => 'required',
            'salary' => 'nullable|numeric',
            'salary_to' => 'nullable|numeric',
            'salarytype_id' => 'required|numeric',
        ];
    }
    
    public function attributes()
    {
        return [
            'name' => 'title',
        'body' => 'job description',
        'day_id' => 'number of days to run',
        'city_id' => 'location or city',
        'category_id' => 'job category',
        'industry_id' => 'industry',
        'experience_id' => 'experience',
        'positiontype_id' => 'position type',
        'level_id' => 'level',
        'endate' => 'last date of apply',
        'vac_no' => 'Number of Vacancy',
        ];
    }
}
