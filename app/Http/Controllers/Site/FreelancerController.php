<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Job;
use App\JobCategory;
use App\Models\User;
use App\Profile;
use App\Skill;
use App\Education;
Use App\Work;
use App\Industry;
use App\Training;
use App\Firmuser;
use Image;
use DB;
use App\Applicant;
use App\Http\Requests\ProfileImageRequest;
use App\Http\Requests\StoreProfileResumeRequest;

class FreelancerController extends Controller
{
    public function index(Request $request) {
        $industries = Industry::all();
        $job_categories = JobCategory::all();
        $cat = $request->input('cat');
        $search = $request->input('search');

        if($request->has('cat') && $cat != 'all') {
            if($request->has('search') && $search != null) {
                $jobs = Job::where('category_id', $cat)
                ->where(function ($query) use ($search) {
                        $query->where('title', 'like', '%'.$search.'%')
                              ->orWhere('body', 'like', '%'.$search.'%');
                })->orderBy('created_at', 'desc')
                ->paginate(5)
                ->appends([
                    'cat' => request('cat'),
                    'search' => request('search')
                ]);
            } else {
                $jobs = Job::where('category_id', $cat)
                ->orderBy('created_at', 'desc')
                ->paginate(5)
                ->appends([
                    'cat' => request('cat')
                ]);
            } 
        } else {
            $jobs = Job::where('title', 'like', '%'.$search.'%')
            ->orWhere('body', 'like', '%'.$search.'%')
            ->orderby('created_at', 'desc')
            ->paginate(5)
            ->appends([
                'search' => request('search')
            ]);
        }
        
        return view('freelancer.userdashboard', compact('job_categories', 'jobs', 'cat', 'search','industries'));
    }

    public function profile() {
        $user_id = Auth()->user()->id;
        $user = User::find($user_id);
        $skills = Skill::orderBy('skill', 'asc')->get();     
        $profile = Profile::where('user_id', $user->id)->first();
        $educations = Education::where('user_id', $user->id)
                    ->orderBy('created_at', 'desc')
                    ->get();
        $works = Work::where('user_id', $user->id)
                    ->orderBy('created_at', 'desc')
                    ->get();
        $trainings = Training::where('user_id', $user->id)
                    ->orderBy('created_at','desc')
                    ->get();
        return view('freelancer.profile', compact('user', 'profile', 'skills', 'educations', 'works','trainings'));
    }

    public function storeProfile(Request $request) { 

        $profile = new Profile;
        $profile->job_title = $request->title;
        $profile->city = $request->city;
        $profile->province = $request->province;
        $profile->country = $request->country;
        $profile->user_id = Auth()->user()->id;
        $profile->overview = $request->overview;        
        $profile->save();
    }

    public function updateProfile(Request $request) { 
        $id = auth()->user()->id;
        $profile = Profile::where('user_id', $id)->first();
        $profile->job_title = $request->title;
        $profile->city = $request->city;
        $profile->province = $request->province;
        $profile->country = $request->country;
        $profile->overview = $request->overview;        
        $profile->save();
    }

     public function uploadPhoto(ProfileImageRequest $request) {
        $id = Auth()->user()->id;
        $user = User::find($id);
        $profile = Profile::where('user_id', $id)->first();
        if($request->hasFile('profilepicture')) {
            $imageName = microtime().'.'.$request->profilepicture->getClientOriginalExtension();
            Image::make($request->profilepicture)->resize(128, 128, function ($constraint) {
                $constraint->aspectRatio(); //to preserve the aspect ratio
                $constraint->upsize();
                    })->save(public_path('photo_small/' . $imageName ));
                $destinationPath = public_path('photo');
                $request->profilepicture->move($destinationPath, $imageName);
                $profile->photo = $imageName;
        }
        $profile->save();
        return back();
    }
     public function updatePhoto(ProfileImageRequest $request) { 
        $id = Auth()->user()->id;
        $profile = Profile::where('user_id', $id)->first();
        if($request->hasFile('profilepicture')) {
            $imageName = microtime().'.'.$request->profilepicture->getClientOriginalExtension();
            Image::make($request->profilepicture)->resize(128, 128, function ($constraint) {
                $constraint->aspectRatio(); //to preserve the aspect ratio
                $constraint->upsize();
                    })->save(public_path('photo_small/' . $imageName ));
                $destinationPath = public_path('photo');
                $request->profilepicture->move($destinationPath, $imageName);
                $profile->photo = $imageName;
        }
         
        $profile->save();
        return back();
    }

    public function myJobs() {
        $id = Auth()->user()->id;
        $user = User::find($id);
        $app = Applicant::where('user_id', $id)->orderBy('created_at', 'desc')->paginate(10);
        $jobs = DB::table('applicants')
            ->join('jobs', 'applicants.job_id', '=', 'jobs.id')
            ->when($id, function ($query) use ($id) {
                    return $query->where('applicants.user_id', $id);
                })
            ->orderBy('applicants.created_at', 'desc')
            ->paginate(10);
        return view('freelancer.my_jobs',compact("app"))->withUser($user)->withJobs($jobs);
    }
}
