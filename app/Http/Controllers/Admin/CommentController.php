<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\CommentContract;
use App\Http\Controllers\BaseController;

class CommentController extends BaseController
{
    private $page="admin.comment.";

    protected $commentRepo;

    public function __construct(
        CommentContract $commentRepo
    )
    {
        $this->commentRepo = $commentRepo;
    }

    public function index()
    {
        $list = $this->commentRepo->list('created_at', 'desc');
        $comments = $this->commentRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('comments', 'List of comments');
        return view($this->page.'index',compact('comments'));
    }

    public function create()
    {
        $this->setPageTitle('comments', 'Create comment');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->commentRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('comment.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $comment = $this->commentRepo->findById($id);
        $this->setPageTitle('comment', 'Edit comment');
        return view($this->page.'edit', compact('comment'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->commentRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('comment.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->commentRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('comment.index', 'Record deleted successfully' ,'success',false, false);
    }
}
