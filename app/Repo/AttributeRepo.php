<?php

namespace App\Repo;

use App\Models\Attribute;
use App\Contracts\AttributeContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

class AttributeRepo extends BaseRepo implements AttributeContract
{
    /**
     * AttributeRepository constructor.
     * @param Attribute $model
     */
    public function __construct(Attribute $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    public function listAttribute()
    {
        return Attribute::where('has_com',0)->get();
    }

    public function listCombination()
    {
        return Attribute::where('has_com',1)->get();
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Category|mixed
     */
    public function store($request)
    {
        try {
            $params = $request->all();
            $collection = collect($params);

            $filterable = $collection->has('filterable') ? 1 : 0;
            $required = $collection->has('required') ? 1 : 0;

            $merge = $collection->merge(compact('filterable', 'required'));

            $attribute = new Attribute($merge->all());

            $attribute->save();

            if ($collection->has('category_id')) {
                $attribute->categories()->sync($params['category_id']);
            }

            return $attribute;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
        $params = $request->all();

        $attribute = $this->findById($id);

        $collection = collect($params);

        $filterable = $collection->has('filterable') ? 1 : 0;
        $required = $collection->has('required') ? 1 : 0;

        $merge = $collection->merge(compact('filterable', 'required'));

        $attribute->update($merge->all());

         if ($collection->has('category_id')) {
                $attribute->categories()->sync($params['category_id']);
            }

        return $attribute;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $attribute = $this->findById($id);
        $attribute->delete();
        return $attribute;
    }
}
