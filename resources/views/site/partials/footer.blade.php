<footer class="section-pad section-bg-black four-columns-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-6 match-height">
                <div class="widget-box">
                    <div class="widget-title">
                        <img class="img-responsive" src="{{ asset('storage/'.config('settings.logo')) }}"
                            alt="{{ config('settings.logo') }}">
                    </div>
                    <div class="widget-content">
                        <div class="textwidget"> <i>{{ config('settings.description') }}</i> </div>
                        <div class="contact-info">
                            <div class="contact-info-box">
                                <i class="fas fa-map-marker-alt"></i>
                            </div>
                            <div class="contact-info-box">
                                <p>{{ config('settings.location') }}</p>
                            </div>
                        </div>
                        <div class="contact-info">
                            <div class="contact-info-box">
                                <i class="fas fa-envelope"></i>
                            </div>
                            <div class="contact-info-box">
                                <p>{{ config('settings.email') }}</p>
                            </div>
                        </div>
                        <div class="contact-info">
                            <div class="contact-info-box">
                                <i class="fas fa-phone"></i>
                            </div>
                            <div class="contact-info-box">
                                <p>{{ config('settings.phone') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 match-height">
                <div class="widget-box">
                    <div class="widget-title">
                        <h4>Featured Job Post</h4>
                    </div>
                    <div class="widget-content">
                        @foreach(\App\Job::where('status',1)->where('feature',1)->orderBy('order','asc')->limit(3)->get()
                        as $job)
                        <div class="media footer-pr-widget-v2">
                            <div class="media-left">
                                <a class="media-img" href="{{ route('job_post', $job->slug) }}">
                                    @if (!empty($job->user->firmuser->logo))
                                    <img class="media-object" src="{{ asset('storage/'.$job->user->firmuser->logo) }}"
                                        alt="{{$job->name}}" style="max-height: 100%">
                                    @else
                                    <img class="media-object"
                                        src="{{ asset('storage/'.config('settings.error_image')) }}"
                                        alt="{{$job->name}}">
                                    @endif
                                </a>
                            </div>
                            <div class="media-body">
                                <span><i class="far fa-clock"></i>{{$job->created_at->format('F j, Y')}}</span>
                                <h5 class="media-heading">
                                    <a href="{{ route('job_post', $job->slug) }}">{{$job->name}}</a>
                                </h5>
                                <p class="price">Salary : <span class="color">
                                        @if($job->salary==!null) Rs.{{ number_format($job->salary) }}@endif
                                        @if($job->salary_to == !null)- {{number_format($job->salary_to)}} @endif
                                        {{$job->salarytype->name}}
                                    </span></p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 match-height">
                <div class="widget-box">
                    <div class="widget-title">
                        <h4>Featured Projects</h4>
                    </div>
                    <div class="widget-content">
                        @foreach(\App\Models\Freelancers\Fjob::where('status',1)->where('feature',1)->orderBy('order','asc')->limit(4)->get()
                        as $job)
                        <div class="media footer-pr-widget-v2">

                            <div class="media-body">
                                <span><i class="far fa-clock"></i>{{$job->created_at->format('F j, Y')}}</span>
                                <h5 class="media-heading">
                                    <a href="{{ route('freelancers.show', $job->slug) }}">{{$job->name}}</a>
                                </h5>
                                <p class="price">Budget : <span class="color">
                                        @if($job->budget==!null) Rs.{{ number_format($job->budget) }}@endif
                                        @if($job->budget_to == !null)- {{number_format($job->budget_to)}} @endif
                                    </span></p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 match-height">
                <div class="widget-box">
                    <div class="widget-title">
                        <h4>Featured Pages</h4>
                    </div>
                    <div class="tagcloud">
                        <a href="{{url('/map')}}" class="tag-cloud-link tag-link-53 tag-link-position-234"
                            style="font-size: 22pt;" aria-label="Post a Project">Map</a>

                        <a href="{{url('project/create')}}" class="tag-cloud-link tag-link-53 tag-link-position-234"
                            style="font-size: 22pt;" aria-label="Post a Project">Post a Project</a>
                        <a href="{{url('job-user/create')}}" class="tag-cloud-link tag-link-53 tag-link-position-234"
                            style="font-size: 22pt;" aria-label="Post a Project">Post a Job</a>

                        @foreach(\App\Models\Post::where('status',1)->orderBy('order','asc')->where('is_blog',
                        0)->limit(6)->get() as $post)
                        <a href="{{route('blog',$post->slug)}}"
                            class="tag-cloud-link tag-link-53 tag-link-position-{{$post->id}}" style="font-size: 22pt;"
                            aria-label="{{$post->name}}">{{$post->name}}</a>
                        @endforeach
                        <a href="{{url('contact-us')}}" class="tag-cloud-link tag-link-53 tag-link-position-234"
                            style="font-size: 22pt;" aria-label="COntact Us">Contact Us</a>
                    </div>

                </div>

                <div class="widget-box">
                    <div class="widget-title">
                        <h4>Payment Options</h4>
                    </div>
                    <div class="widget-title">
                        <img class="img-responsive" src="{{ asset('images/payment.png') }}" height="100">
                    </div>



                </div>
            </div>

        </div>
        <!--row-->
    </div>
    <!--container-->
</footer>
<section class="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <p>All rights reserved © <?php echo date("Y");?>, Skyfall Technologies Pvt. Ltd.</p>
            </div>
            <div class="col-lg-6 col-sm-6">
                <ul class="footer-bottom-social-icon">
                    <li><span>Follow Us :</span></li>

                    <li>
                        <a href="//{{ config('settings.facebook') }}" class="rounded text-center" target="_blank">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="//{{ config('settings.twitter') }}" class="rounded text-center" target="_blank">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!--row-->
    </div>
    <!--container-->
</section>
<!-- back to top -->
<a href="#" id="back-to-top" title="Back to top" class="social-icon social-icon-md"><i
        class="fas fa-angle-double-up removeMargin"></i></a>
<script type="text/html" id="tmpl-media-frame">
    <div class="media-frame-title" id="media-frame-title"></div>
    <div class="media-frame-menu"></div>
    <div class="media-frame-router"></div>
    <div class="media-frame-content"></div>
    <div class="media-frame-toolbar"></div>
    <div class="media-frame-uploader"></div>
</script>
