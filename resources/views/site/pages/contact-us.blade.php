@extends('site.partials.main_index')
@section('title', config('settings.site_title'))
@section('content')
<!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v7.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your Chat Plugin code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="1609645282583477"
  theme_color="#009996">
      </div>
<body class="archive category category-uncategorized category-1 logged-in">
    @include('site.partials.navbar')
<div id="classiera_map">
    <div id="classiera-main-map" style="width:100%; height:600px;">
        {!! $map['html'] !!}
        <script type='text/javascript'>
            var centreGot = false;
        </script>
        {!! $map['js'] !!}
    
	</div>
</div>
<!--PageContent-->

<section class="contact-us border-bottom section-pad">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
                
			</div>
		</div><!--row-->
	</div><!--container-->
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<h4 class="text-uppercase">Contact Form</h4>
                @if(Session::has('success'))
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-success">
                            {{Session::get('success')}}
                        </div>
                    </div>
                </div>
                @endif
					<form action="{{route('mcontact.store')}}" method="POST" data-toggle="validator">
                        {{csrf_field()}}
					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-sm-6">
                                <label class="text-capitalize" for="name">Full name : <span class="text-danger">*</span> </label>
                                <div class="inner-addon left-addon">
                                    <i class="left-addon form-icon fas fa-font"></i>
                                    <input value="{{old('name')}}" id="name" type="text" name="name" class="form-control form-control-md" placeholder="Enter your full name" data-error="Name Requried" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div><!--Name Div-->
							<div class="form-group col-sm-6">
                                <label class="text-capitalize" for="email">Email : <span class="text-danger">*</span></label>
                                <div class="inner-addon left-addon">
                                    <i class="left-addon form-icon fas fa-envelope"></i>
                                    <input value="{{old('email')}}" id="email" type="text" name="email" class="form-control form-control-md" placeholder="Enter your email" data-error="Email required" required>
                                    <strong>{{ $errors->first("email") }}</strong>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div><!--Email-->
							<div class="form-group col-sm-6">
                                <label class="text-capitalize" for="phone">Mobile Number : </label>
                                <div class="inner-addon left-addon">
                                    <i class="left-addon form-icon fas fa-phone"></i>
                                    <input value="{{old('phone')}}" id="phone" type="text" name="phone" class="form-control form-control-md" placeholder="Enter your mobile number">
                                    <strong>{{ $errors->first("phone") }}</strong>
                                </div>
                            </div><!--Phone Div-->
							<div class="form-group col-sm-6">
                                <label class="text-capitalize" for="subject">Subject : <span class="text-danger">*</span></label>
                                <div class="inner-addon left-addon">
                                    <i class="left-addon form-icon fas fa-book"></i>
                                    <input value="{{old('subject')}}" id="subject" name="subject" type="text" class="form-control form-control-md" placeholder="Enter Subject" data-error="Subject Requried" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div><!--Subject Div-->
							<div class="form-group col-sm-12">
                                <label class="text-capitalize" for="text">Message : <span class="text-danger">*</span></label>
                                <div class="inner-addon">
                                    <textarea data-error="Please type message" name="body" id="text" placeholder="Type your message here...!" required>{{old('body')}}</textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div><!--Message Div-->
							
							<!--Question Div-->
						</div><!--form-inline row-->
					</div><!--form-group-->
					<div class="form-group">
						<button class="btn btn-primary sharp btn-md btn-style-one" type="submit">Send Message</button>
                    </div>
				</form>
			</div><!--col-lg-8-->
			<div class="col-lg-4">
                <h4 class="text-uppercase">Contact Info</h4>
                <ul class="contact-us-info list-unstyled fa-ul">
				                    <li><i class="fa-li fas fa-map-marker-alt"></i>
					{{ config('settings.location') }}</li>
					
				                    <li><i class="fa-li fas fa-phone"></i>
						{{ config('settings.phone') }}					</li>
				                </ul>
            </div><!--col-lg-4-->
		</div><!--row-->
	</div><!--container-->
</section>
<!--PageContent-->

@include('site.partials.footer')
@endsection