@extends('admin.work.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('contact.store') }}" enctype="multipart/form-data">
                       {{csrf_field()}}

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Org Name*</label>

                            <div class="col-md-6">
                                <input id="org_name" type="text" class="form-control" name="org_name" value="{{ old('org_name') }}">
                                
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('org_name') }}
                                        </strong>
                                    </span>
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Duration</label>

                            <div class="col-md-6">
                                <input id="duration" type="text" class="form-control" name="duration" value="{{ old('duration') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Completion Year</label>

                            <div class="col-md-6">
                                <input id="completion_year" type="text" class="form-control" name="completion_year" value="{{ old('completion_year') }}">
                            </div>
                        </div>

                        

                    <div class="form-group">
                      <label class="col-md-4 control-label">User ID</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="user_id">
                                @foreach($users as $value)
                              <option value="{{$value->id}}">{{$value->name}}</option>
                              @endforeach
                            </select>
                                <span class="text-danger">{{ $errors->first('user_id') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
