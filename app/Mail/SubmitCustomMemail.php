<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Memail;

class SubmitCustomMemail extends Mailable
{
    use Queueable, SerializesModels;
    protected $memail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Memail $memail)
    {
        $this->memail = $memail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->memail->from,'Mero Offer')
                    ->subject($this->memail->subject)
                    ->view('emails.customMemail')
                    ->with([
                        'subject' => $this->memail->subject,
                        'from' => $this->memail->from,
                        'to' => $this->memail->to,
                        'body' => $this->memail->body,
                        'greet' => $this->memail->greet,
                        'end_greet' => $this->memail->end_greet,
                    ]);
    }
}
