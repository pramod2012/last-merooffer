@extends('site.partials.main_index')
@section('title','List of Applied Jobs')
@section('content')
<body class="page-template page-template-template-profile page-template-template-profile-php page page-id-32 logged-in">
@include('site.partials.navbar') 
    <!-- user pages -->
<section class="user-pages section-gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4">
                @include('site.partials.sidebar')
                         </div><!--col-lg-3-->
            <div class="col-lg-9 col-md-8 user-content-height">
                <div class="user-detail-section section-bg-white">
                    <!-- about me -->
                    <!-- my ads -->
                    <div class="user-ads user-my-ads">
                        <h4 class="user-detail-section-heading text-uppercase">
                            MY JOBS                      </h4>
                            @if(count($app) > 0)
                            <div class="row table-responsive">
                            <table class="table table-striped">
                              
                              <tbody>
                                @foreach($app as $job)
	                   		<tr>
						      <td>
						      	<a href="{{ route('job_post',$job->job->slug) }}" target="_blank">{{ $job->job->name }}</a>
						      	
						      	@if ($job->astatus =='hired')
						      		<h4><span class="badge badge-success w-100">{{ $job->astatus }}</span></h4>
						      	@elseif($job->astatus =='rejected')
						      		<h4><span class="badge badge-danger w-100">{{ $job->astatus }}</span></h4>
						      	@else
						      		<h4><span class="badge badge-info w-100">{{ $job->astatus }}</span></h4>
						      	@endif
						      	
						      </td>
						      <td>
						      	@if ($job->astatus !='hired')
						      	
						      	@else	
						      		<a href="#" data-toggle="tooltip" data-placement="top" title="Send Message">
						      			<p class="text-primary text-center">
						      				<i class="far fa-envelope fa-2x"></i>
							      		</p>
							      	</a>
						      	@endif
						      </td>
						    </tr>
	                   	@endforeach
                              </tbody>
                            </table>
                            {{$app->links()}}
                             
                          </div>
                           @else
                        <p class="my-5 text-center">You haven't applied for any jobs.</p>
                    @endif

                        
                    </div>
                    <!-- my ads -->
                    
                </div>
            </div>
        </div><!--row-->
    </div><!--container-->
</section>
<!-- user pages -->
@include('site.partials.footer')
@endsection