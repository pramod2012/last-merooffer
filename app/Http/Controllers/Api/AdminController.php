<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Contracts\ProductContract;
use App\Contracts\UserContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests as RulesOf;
use App\Utils as Utils;
use App\Models\User;

class AdminController extends BaseController
{
    protected $productRepo;
    protected $userRepo;

    public function __construct(
        ProductContract $productRepo,
        UserContract $userRepo
    )
    {
        $this->productRepo = $productRepo;
        $this->userRepo = $userRepo;
    }

    

    
}
