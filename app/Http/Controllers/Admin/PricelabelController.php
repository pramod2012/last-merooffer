<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Contracts\PricelabelContract;
use App\Contracts\CategoryContract;

class PricelabelController extends BaseController
{
    /**
     * @var pricelabelContract
     */
    protected $pricelabelRepo;
    protected $categoryRepo;

    /**
     * pricelabelController constructor.
     * @param pricelabelContract $pricelabelRepository
     */
    public function __construct(
        PricelabelContract $pricelabelRepo,
         CategoryContract $categoryRepo
     )
    {
        $this->pricelabelRepo = $pricelabelRepo;
        $this->categoryRepo = $categoryRepo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $list = $this->pricelebelRepo->list('created_at', 'desc');
        $datas = $this->pricelebelRepo->paginateArrayResults($list->all(), 15);

        $this->setPageTitle('pricelabels', 'List of all pricelabels');
        return view('admin.pricelabel.index', compact('datas'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $options = $this->categoryRepo->list();
        $this->setPageTitle('pricelabels', 'Create pricelabel');
        return view('admin.pricelabel.create',compact('options'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $pricelabel = $this->pricelabelRepo->store($request);

        if (!$pricelabel) {
            return $this->responseRedirectBack('Error occurred while creating pricelabel.', 'error', true, true);
        }
        return $this->responseRedirect('pricelabel.index', 'pricelabel added successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data = $this->pricelabelRepo->findById($id);
        $categories = $this->categoryRepo->list();

        $this->setPageTitle('pricelabels', 'Edit pricelabel : '.$data->name);
        return view('admin.pricelabel.edit', compact('data','categories'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request,$id)
    {
        $pricelabel = $this->pricelabelRepo->edit($request,$id);

        if (!$pricelabel) {
            return $this->responseRedirectBack('Error occurred while updating pricelabel.', 'error', true, true);
        }
        return $this->responseRedirect('pricelabel.index', 'pricelabel updated successfully' ,'success',false, false);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $pricelabel = $this->pricelabelRepo->destroy($id);

        if (!$pricelabel) {
            return $this->responseRedirectBack('Error occurred while deleting pricelabel.', 'error', true, true);
        }
        return $this->responseRedirect('pricelabel.index', 'pricelabel deleted successfully' ,'success',false, false);
    }
}
