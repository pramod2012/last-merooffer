<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddToCartRequest;
use App\Http\Requests\UpdateCartRequest;
use App\Contracts\CartContract;
use App\Contracts\CourierContract;
use App\Contracts\ProductContract;
use App\Contracts\ProductattributeContract;
use App\Models\Product;
use Cart;

class CartController extends Controller
{
	private $cartRepo;
	private $courierRepo;
	private $productRepo;
    private $productattributeRepo;

	public function __construct(
        CartContract $cartRepo,
        CourierContract $courierRepo,
        ProductContract $productRepo,
        ProductattributeContract $productattributeRepo
    )
    {
        $this->cartRepo = $cartRepo;
        $this->courierRepo = $courierRepo;
        $this->productRepo = $productRepo;
        $this->productattributeRepo = $productattributeRepo;
    }

    public function index()
    {
        $courier = $this->courierRepo->findById(request()->get('courierId', 1));
        $shippingFee = $this->cartRepo->getShippingFee($courier);

        return response()->json([
                'status' => 1,
                'message' => 'success',
                'data' => [
                    'cartItems' => $this->cartRepo->getCartItemsTransformed(),
                    'subtotal' => $this->cartRepo->getSubTotal(),
                    'tax' => $this->cartRepo->getTax(),
                    'shippingFee' => $shippingFee,
                    'total' => $this->cartRepo->getTotal(2, $shippingFee)
                ]
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AddToCartRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddToCartRequest $request)
    {
        $product = $this->productRepo->findById($request->input('product'));

        if ($product->productattributes()->count() > 0) {
            $productAttr = $product->productattributes()->where('default', 1)->first();

            if (isset($productAttr->sale_price)) {
                $product->price = $productAttr->price;

                if (!is_null($productAttr->sale_price)) {
                    $product->price = $productAttr->sale_price;
                }
            }
        }

        $options = [];
        if ($request->has('productAttribute')) {

            $attr = $this->productattributeRepo->findById($request->input('productAttribute'));
            $product->price = $attr->price;

            $options['productattribute_id'] = $request->input('productAttribute');
            $options['combination'] = $attr->attributesValues->toArray();
        }

        $cartItems = $this->cartRepo->addToCart($product, $request->input('quantity'), $options);

        $data = [
                'status' => 1,
                'message' => 'Add to cart successful',
                'cartItems' => $cartItems,
            ];
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCartRequest $request, $id)
    {
        $this->cartRepo->updateQuantityInCart($id, $request->input('quantity'));

        $data = [
                'status' => 1,
                'message' => 'Data Updated'
            ];
        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->cartRepo->removeToCart($id);
        
        $data = [
                'status' => 1,
                'message' => 'Data Deleted'
            ];
        return response()->json($data);
    }
}
