@section('meta-tags')
<meta name="description"
        content="Classified Advertising and Job Vacancy. It is the next generation of free online classifieds &amp; It provides a simple solution to the complications involved in selling, buying, trading, discussing, organizing, meeting people near you, finding job vacancy and more.. " />
    <meta name="keywords"
        content="merooffer.com, popular classified in Nepal, real estate buy and rent, next generation of free online classifieds" />
    <meta property='og:title' content='{{$product->name}}' />
    <meta property='og:image'
        content="@if ($product->images->count() > 0) {{ asset('images_small/'.$product->images->first()->full) }} @endif" />
    <meta property='og:description' content='{{$product->desc}}' />
    <meta property='og:url' content="{{app('request')->url()}}">
    <meta property=" og:type" content="website" />
    <meta property="og:site_name" content="{{app('request')->url()}}" />
@endsection
@section('title')
Mero Offer - {{$product->name}}
@endsection
@extends('site.partials.main_index')
@section('content')
<body class="post-template-default single single-post postid-445 single-format-standard logged-in">
    @include('site.partials.navbar')
    @include('site.partials.search')
    <section class="inner-page-content single-post-page" id="app">

        <div class="container">
            <notifications group="foo"></notifications>
            <!-- breadcrumb -->
            <div class="row">
                <div class="col-lg-12">
                    <ul class="breadcrumb">
                        <li><a rel="v:url" property="v:title" href="{{route('index')}}"><i class="fas fa-home"></i></a>
                        </li>&nbsp;
                        @foreach($product->categories as $cat)
                        <li class="cAt"><a rel="v:url" href="{{route('menu', $cat->slug)}}">{{$cat->name}}</a></li>
                        &nbsp;
                        @endforeach
                        <li class="active">{{$product->name}}</li>
                    </ul>
                </div>
            </div> <!-- breadcrumb -->
            <!--Google Section-->
            <!--Google Section-->
            <div class="row">
                <div class="col-md-8">
                    <!-- single post -->
                    <div class="single-post">
                        <!-- post title-->
                        <div class="single-post-title">
                            <div class="post-price visible-xs visible-sm">
                                <h4>Rs. {{number_format($product->price)}} @if($product->pricelabel_id==!null)
                                    {{$product->pricelabel->name}} @endif</h4>
                            </div>
                            <h4 class="text-lowercase">
                                <a href="{{ route('product_details',$product->slug) }}">{{$product->name}}</a>

                                <!--Edit Ads Button-->
                            </h4>
                            <p class="post-category">
                                <i class="far fa-folder-open"></i>:
                                <span>
                                    @foreach($product->categories->where('parent_id',!null) as $cat)
                                    <a href="{{route('menu', $cat->slug)}}"
                                        title="View all posts in {{$cat->name}}">{{$cat->name}}</a>
                                    @endforeach </span>
                                <i class="fas fa-map-marker-alt"></i>:<span><a
                                        href="{{route('findByCity',$product->city->slug)}}">{{$product->city->name}}</a></span>
                            </p>
                        </div>
                        <!-- post title-->
                        <!-- single post carousel-->
                        @if($product->images->count() > 0)
                        <div id="single-post-carousel" class="carousel slide single-carousel" data-ride="carousel"
                            data-interval="3000">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                @if ($product->images->count() > 0)
                                <div class="item active">
                                    <a href="{{ asset('watermark/'.$product->images->first()->full) }}" target="_blank">
                                        <img class="img-responsive"
                                            src="{{ asset('watermark/'.$product->images->first()->full) }}" alt=""></a>
                                </div>
                                @foreach($product->images as $key => $image)
                                @if($key > 0)
                                <div class="item ">
                                    <a href="{{@asset('watermark/'. $image->full)}}" target="_blank">
                                        <img class="img-responsive" src="{{@asset('watermark/'. $image->full)}}"
                                            alt="{{$image->full}}"></a>
                                </div>
                                @endif
                                @endforeach
                                @endif

                            </div>

                            <!-- slides number -->
                            <div class="num">
                                <i class="fas fa-camera"></i>
                                <span class="init-num">1</span>
                                <span>of</span>
                                <span class="total-num"></span>
                            </div>
                            <!-- Left and right controls -->
                            <div class="single-post-carousel-controls">
                                <a class="left carousel-control" href="#single-post-carousel" role="button"
                                    data-slide="prev">
                                    <span class="fas fa-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#single-post-carousel" role="button"
                                    data-slide="next">
                                    <span class="fas fa-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <!-- Left and right controls -->
                        </div>
                        @endif
                        <!-- single post carousel-->
                        <!-- ad deails -->
                        <div class="border-section border details">
                            <h4 class="border-section-heading text-uppercase"><i class="far fa-file-alt"></i>General
                                Details</h4>
                            <div class="post-details">
                                <ul class="list-unstyled clearfix">
                                    <li>
                                        <p>Ad ID:
                                            <span class="pull-right flip">
                                                <i class="fas fa-hashtag IDIcon"></i>
                                                {{$product->id}}</span>
                                        </p>
                                    </li>
                                    <!--PostDate-->
                                    <li>
                                        <p>Added:
                                            <span class="pull-right flip">{{$product->created_at->format('F j,
                                                Y')}}</span>
                                        </p>
                                    </li>
                                    <!--PostDate-->
                                    <!--Price Section -->
                                    <li>
                                        <p>Views:
                                            <span class="pull-right flip">
                                                {{$product->views}} </span>
                                        </p>
                                    </li>
                                    <!--Views-->
                                    @if($product->condition_id==!null)
                                    <li>
                                        <p>
                                            Condition: <span class="pull-right flip">
                                                {{ $product->condition->name }}
                                            </span>
                                        </p>
                                    </li>@endif
                                    <li>
                                        <p>Price type:
                                            <span class="pull-right flip">{{$product->pricetype->name}}</span>
                                        </p>
                                    </li>
                                    <li>
                                        <p>Sale Price:
                                            <span class="pull-right flip">
                                                Rs. {{number_format($product->price)}}

                                                @if($product->pricelabel_id==!null) {{$product->pricelabel->name}}
                                                @endif</span>
                                        </p>
                                    </li>
                                    <!--Sale Price-->
                                    @if($product->rgr_price==!null)
                                    <li>
                                        <p>Regular Price:
                                            <span class="pull-right flip">
                                                Rs. {{number_format($product->rgr_price)}}
                                            </span>
                                        </p>
                                    </li>
                                    <!--Regular Price-->
                                    @endif
                                    <li>
                                        <p>Quantity Available:
                                            <span class="pull-right flip">
                                                {{$product->quantity}}
                                            </span>
                                        </p>
                                    </li>
                                    <!--Regular Price-->
                                    <!--Price Section -->
                                    <li>
                                        <p>Location:
                                            <span class="pull-right flip">{{$product->city->name}}</span>
                                        </p>
                                    </li>
                                    <!--Location-->
                                    @if($product->cell==!null)
                                    <li>
                                        <p>Mobile:
                                            <span class="pull-right flip">
                                                <a href="tel:{{$product->cell}}">
                                                    {{$product->cell}} </a>
                                            </span>
                                        </p>
                                    </li>
                                    <!--Phone-->
                                    @endif

                                </ul>
                            </div>
                            <!--post-details-->
                        </div>
                        @if($product->attributevalues->count() > 0)
                        <div class="border-section border details">
                            <h4 class="border-section-heading text-uppercase"><i class="far fa-file-alt"></i>Specific
                                Details</h4>
                            <div class="post-details">
                                <ul class="list-unstyled clearfix">
                                    @foreach($product->attributevalues as $value)
                                    <li>
                                        <p>{{$value->attribute->name}}:
                                            <span class="pull-right flip">
                                                {{ $value->value }}</span>
                                        </p>
                                    </li>
                                    <!--PostDate-->
                                    @endforeach
                                </ul>
                            </div>
                            <!--post-details-->
                        </div>
                        @endif
                        <!-- ad details -->

                        <!--amenities -->
                        @if($product->amenities->count() > 0)
                        <div class="border-section border details">
                            <h4 class="border-section-heading text-uppercase"><i class="far fa-file-alt"></i>Features
                            </h4>
                            <div class="post-details">
                                <ul class="list-unstyled clearfix">
                                    @foreach($product->amenities as $value)
                                    <li>
                                        <p>{{ $value->value }}
                                            <span class="pull-right flip">
                                                <i class="fas fa-check"></i></span>
                                        </p>
                                    </li>
                                    <!--PostDate-->
                                    @endforeach
                                </ul>
                            </div>
                            <!--post-details-->
                        </div>
                        @endif
                        <!--amenities -->


                        <!-- post description -->
                        @if($product->desc==!null)
                        <div class="border-section border description">
                            <h4 class="border-section-heading text-uppercase">
                                Description
                            </h4>
                            <p>
                                <?php
$str = $product->desc;
echo nl2br($str);
?>
                            </p>

                        </div>
                        <!-- post description -->
                        @endif

                        @if($product->assured->desc==!null)
                        <div class="border-section border description">
                            <h4 class="border-section-heading text-uppercase">
                                Note
                            </h4>
                            <p>
                                <?php
$str = $product->assured->desc;
echo nl2br($str);
?>
                            </p>

                        </div>
                        <!-- post description -->
                        @endif

                        <!--PostVideo-->
                        <?php $postVideo = $product->url; ?>
                        <?php if(!empty($postVideo)) { ?>
                        <div class="border-section border postvideo">
                            <h4 class="border-section-heading text-uppercase">
                                Video
                            </h4>
                            <?php
                        if(preg_match("/youtu.be\/[a-z1-9.-_]+/", $postVideo)) {
                            preg_match("/youtu.be\/([a-z1-9.-_]+)/", $postVideo, $matches);
                            if(isset($matches[1])) {
                                $url = 'https://www.youtube.com/embed/'.$matches[1];
                                $video = '<iframe class="embed-responsive-item" src="'.$url.'" frameborder="0" allowfullscreen></iframe>';
                            }
                        }elseif(preg_match("/youtube.com(.+)v=([^&]+)/", $postVideo)) {
                            preg_match("/v=([^&]+)/", $postVideo, $matches);
                            if(isset($matches[1])) {
                                $url = 'https://www.youtube.com/embed/'.$matches[1];
                                $video = '<iframe class="embed-responsive-item" src="'.$url.'" frameborder="0" allowfullscreen></iframe>';
                            }
                        }elseif(preg_match("#https?://(?:www\.)?vimeo\.com/(\w*/)*(([a-z]{0,2}-)?\d+)#", $postVideo)) {
                            preg_match("/vimeo.com\/([1-9.-_]+)/", $postVideo, $matches);
                            //print_r($matches); exit();
                            if(isset($matches[1])) {
                                $url = 'https://player.vimeo.com/video/'.$matches[1];
                                $video = '<iframe class="embed-responsive-item" src="'.$url.'" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe>';
                            }
                        }else{
                            $video = $postVideo;
                        }
                        ?>
                            <div class="embed-responsive embed-responsive-16by9">
                                <?php echo $video; ?>
                            </div>
                        </div>
                        <?php } ?>
                        <!--PostVideo-->
                        @if($product->bid == 1)
                        <div class="border-section border bids">
                            <h4 class="border-section-heading text-uppercase">Offers</h4>
                            <bid-stats :product_id="{{$product->id}}" :product-bids="{{$product->bids}}"></bid-stats>
                        </div>
                        <!-- classiera bid system -->
                        @endif
                        @include('site.partials.comments')
                        <!--comments-->
                    </div>
                    <!-- single post -->
                </div>
                <!--col-md-8-->
                <div class="col-md-4">
                    <aside class="sidebar">
                        <div class="row">
                            <!--Widget for style 1-->
                            <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                                <div class="widget-box ">
                                    <div class="widget-title price">
                                        <h3 class="post-price">
                                            Rs. {{number_format($product->price)}} @if($product->pricelabel_id==!null)
                                            {{$product->pricelabel->name}} @endif <span class="ad_type_display">
                                                {{$product->adtype->name}} </span>
                                        </h3>
                                    </div>
                                    <!--price-->

                                    <div class="widget-content widget-content-post">
                                        <div class="author-info border-bottom widget-content-post-area">
                                            <div class="media">
                                                <div class="media-left">
                                                    <img class="media-object"
                                                        src="{{asset('frontend/images/user.png')}}"
                                                        alt="{{$product->user->name}}">
                                                </div>
                                                <!--media-left-->
                                                <div class="media-body">
                                                    <h5 class="media-heading text-uppercase">
                                                        <a href="#">{{$product->user->name}}</a>
                                                    </h5>
                                                    <p>Member Since:
                                                        &nbsp;{{$product->user->created_at->format('F j, Y')}}</p>
                                                    <a href="{{route('user',$product->user->slug)}}"
                                                        title="View all ads from {{$product->user->name}}">see all
                                                        ads</a>

                                                    <div class="user-make-offer-message widget-content-post-area"
                                                        id="add-seller">
                                                        <ul class="nav nav-tabs" role="tablist">
                                                            <li role="presentation" class="btnWatch">
                                                                <form method="get" class="fav-form clearfix">
                                                                    <?php if(Auth::check()){
                                                   $auth_id = Auth::user()->id;
                                                   } ?>
                                                                    @if(!Auth::check())<a href="{{url('login')}}">@endif
                                                            @if($product->user_id <> @auth()->user()->id)
                                                                        <add-seller-to-chat
                                                                            auth_id="{{Auth::check()?Auth::user()->id:null}}"
                                                                            user_id="{{$product->user_id}}"
                                                                            added="{{$product->user->isAddedToFollowedList($product->user_id)}}" />@endif
                                                                        {{-- <button class="watch-later text-uppercase"
                                                                            id="{{'btn____'.$product->user_id}}"
                                                                            data-auth_id="@if(Auth::check()) {{ Auth::user()->id }} @endif"
                                                                            data-user_id="{{ $product->user_id }}"
                                                                            type="button" onclick="addToFollow(this)"
                                                                            {{$product->user->isAddedToFollowedList($product->user_id)?"disabled":""}}>

                                                                            {{$product->user->isAddedToFollowedList($product->user_id)?"Added
                                                                            to chat":"Chat Now"}}
                                                                        </button> --}}
                                                                        @if(!Auth::check())
                                                                    </a>@endif
                                                                </form>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                </div>
                                                <!--media-body-->
                                            </div>
                                            <!--media-->
                                        </div>
                                        <!--author-info-->
                                    </div>
                                    <!--widget-content-->
                                    <div class="widget-content widget-content-post">
                                        <div class="contact-details widget-content-post-area">

                                            @if($product->user->show_email ==1 || $product->user->show_mobile == 1 ||
                                            $product->user->show_phone == 1)
                                            <h5 class="text-uppercase">Contact Details :</h5>
                                            @endif
                                            <ul class="list-unstyled fa-ul c-detail" style="margin-left:20px">
                                                @if($product->user->show_email ==1)
                                                <li><i class="fas fa-li fa-envelope"></i>
                                                    <a href="mailto:{{$product->user->email}}">
                                                        {{$product->user->email}}
                                                    </a>
                                                </li>
                                                @endif
                                                @if($product->user->show_mobile == 1)
                                                <li><i class="fas fa-li fa-mobile"></i>&nbsp;
                                                    <span class="phNum"
                                                        data-replace="{{$product->user->mobile}}">{{$product->user->mobile}}</span>
                                                    <button type="button" id="showNum">Reveal</button>
                                                </li>@endif
                                                @if($product->user->show_phone == 1)
                                                <li><i class="fas fa-li fa-phone"></i>&nbsp;
                                                    <span
                                                        data-replace="{{$product->user->phone}}">{{$product->user->phone}}</span>
                                                </li>@endif
                                            </ul>
                                        </div>
                                        <!--contact-details-->
                                    </div>
                                    <!--widget-content-->
                                </div>
                                <!--widget-box-->
                            </div>
                            <!--col-lg-12 col-md-12 col-sm-6 match-height-->
                            <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                                <div class="widget-box ">
                                    <!--ReportAd-->
                                    <div class="widget-content widget-content-post">
                                        <div class="user-make-offer-message border-bottom widget-content-post-area">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="btnWatch">

                                                </li>
                                                <li role="presentation" class="active">
                                                    <a href="#report" aria-controls="report" role="tab"
                                                        data-toggle="tab"><i class="fas fa-shield-alt"></i> Why Assured
                                                        ?</a>
                                                </li>
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active">
                                                    <p>{!! $product->assured->summary !!}</p>
                                                </div>
                                                <!--tabpanel-->
                                            </div>
                                            <!--tab-content-->
                                        </div>
                                        <!--user-make-offer-message-->
                                    </div>
                                    <!--widget-content-->
                                    <!--ReportAd-->
                                </div>
                                <!--widget-box-->
                            </div>
                            @if($product->assured->id !== 1)

                            <div class="col-lg-12 col-md-12 col-sm-6 match-height">

                                <div class="widget-box ">
                                    <div class="widget-content widget-content-post">
                                        <div class="user-make-offer-message widget-content-post-area">
                                            <ul class="nav nav-tabs" role="tablist">

                                                <li role="presentation" class="active">
                                                    <a href="#message" aria-controls="message" role="tab"
                                                        data-toggle="tab"> <i class="fa fa-shopping-cart"></i>Order
                                                        Now</a>
                                                </li>
                                            </ul>
                                            <!--nav nav-tabs-->
                                            <!-- Tab panes -->

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="message">
                                                    @if($errors->any())
                                                    <div class="alert alert-danger">
                                                        {{ implode('', $errors->all(':message')) }}
                                                    </div>
                                                    @endif
                                                    <!--ShownMessage-->
                                                    <form method="post" class="form-horizontal"
                                                        action="{{route('cart.store')}}">
                                                        {{csrf_field()}}
                                                        @if(isset($productAttributes) && !$productAttributes->isEmpty())
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label"
                                                                for="name">Type:</label>
                                                            <div class="col-sm-9">
                                                                <select name="productAttribute" id="productAttribute"
                                                                    class="form-control select2">
                                                                    @foreach($productAttributes as $productAttribute)
                                                                    <option value="{{ $productAttribute->id }}">
                                                                        @foreach($productAttribute->attributesValues as
                                                                        $value)
                                                                        {{ $value->attribute->name }} :
                                                                        {{ ucwords($value->value) }}
                                                                        @endforeach
                                                                        @if(!is_null($productAttribute->price))
                                                                        ( {{ config('cart.currency_symbol') }}
                                                                        {{ $productAttribute->price }})
                                                                        @endif
                                                                    </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!--name-->
                                                        @endif
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label"
                                                                for="email">Quantity:</label>
                                                            <div class="col-sm-9">
                                                                <input type="number" class="form-control"
                                                                    name="quantity" id="quantity" placeholder="Quantity"
                                                                    value="{{ old('quantity', 1) }}" required />
                                                            </div>
                                                        </div>
                                                        <!--Email-->

                                                        <input type="hidden" name="product"
                                                            value="{{ $product->id }}" />


                                                        <input type="hidden" name="submit" value="send_message" />
                                                        <button
                                                            class="btn btn-primary btn-block btn-sm sharp btn-style-one"
                                                            name="send_message" value="send_message" type="submit">Add
                                                            To Cart</button>
                                                    </form>
                                                </div>
                                                <!--message-->

                                            </div>
                                            <!--tab-content-->
                                            <!-- Tab panes -->
                                        </div>
                                        <!--user-make-offer-message-->
                                    </div>
                                    <!--widget-content-->
                                </div>
                                <!--widget-box-->
                            </div>
                            <!--col-lg-12 col-md-12 col-sm-6 match-height-->
                            @endif

                            <!--Widget for style 1-->
                            <div class="col-lg-12 col-md-12 col-sm-6 match-height">

                                <div class="widget-box ">
                                    <div class="widget-content widget-content-post">
                                        <div class="user-make-offer-message widget-content-post-area">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="btnWatch">
                                                    <form method="get" class="fav-form clearfix">
                                                        @if(!Auth::check())<a href="{{url('login')}}">@endif
                                                            <button class="watch-later text-uppercase"
                                                                id="{{'btn___'.$product->id}}"
                                                                data-auth_id="@if(Auth::check()) {{ Auth::user()->id }} @endif"
                                                                data-user_id="{{ $product->user_id }}"
                                                                data-product_id="{{ $product->id }}" type="button"
                                                                onclick="addToLove(this)"
                                                                {{$product->isAddedToLovelist($product->id)?"disabled":""}}>
                                                                <i
                                                                    class="fas fa-heart"></i>{{$product->isAddedToLovelist($product->id)?"Added":"Watch
                                                                Later"}}</button>@if(!Auth::check())</a>@endif
                                                    </form>

                                                </li>
                                                <li role="presentation" class="active">
                                                    <a href="#message" aria-controls="message" role="tab"
                                                        data-toggle="tab"><i class="fas fa-envelope"></i>Send Email</a>
                                                </li>
                                            </ul>
                                            <!--nav nav-tabs-->
                                            <!-- Tab panes -->

                                            <div class="tab-content">
                                                <send-message-to-seller :product="{{$product}}" />
                                                <!--message-->

                                            </div>
                                            <!--tab-content-->
                                            <!-- Tab panes -->
                                        </div>
                                        <!--user-make-offer-message-->
                                    </div>
                                    <!--widget-content-->
                                </div>
                                <!--widget-box-->
                            </div>
                            <!--col-lg-12 col-md-12 col-sm-6 match-height-->
                            <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                                <div class="widget-box ">
                                    <!--ReportAd-->
                                    <div class="widget-content widget-content-post">
                                        <div class="user-make-offer-message border-bottom widget-content-post-area">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="btnWatch">

                                                </li>
                                                <li role="presentation" class="active">
                                                    <a href="#report" aria-controls="report" role="tab"
                                                        data-toggle="tab"><i class="fas fa-exclamation-triangle"></i>
                                                        Report</a>
                                                </li>
                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active">
                                                    <report-product :product="{{$product}}"
                                                        user_id="{{Auth::check()?Auth::user()->id:null}}"
                                                        full_url="{{url()->full()}}" />
                                                </div>
                                                <!--tabpanel-->
                                            </div>
                                            <!--tab-content-->
                                        </div>
                                        <!--user-make-offer-message-->
                                    </div>
                                    <!--widget-content-->
                                    <!--ReportAd-->
                                </div>
                                <!--widget-box-->
                            </div>
                            <!--col-lg-12 col-md-12 col-sm-6 match-height-->


                            <!--Social Widget-->
                            <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                                <div class="widget-box">


                                    <div class="widget-content">
                                        <div class="social-network">
                                            <a class="social-icon social-icon-sm footer-social"
                                                href="http://www.facebook.com/sharer/sharer.php?u={{app('request')->url()}}"
                                                target="_blank">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>

                                            <a class="social-icon social-icon-sm footer-social"
                                                href="https://twitter.com/share?url={{app('request')->url()}}"
                                                target="_blank">
                                                <i class="fab fa-twitter"></i>
                                            </a>

                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                                <div class="widget-box ">
                                    <!--GoogleMAP-->
                                    <div class="widget-content widget-content-post">
                                        <div class="share widget-content-post-area">
                                            <h5>{{$product->address}}</h5>
                                            <div id="classiera_single_map">
                                                <!--<div id="single-page-main-map" id="details_adv_map">-->
                                                <div class="details_adv_map" id="details_adv_map">
                                                    {!! @$map['html'] !!}
                                                </div>








                                                <div id="ad-address">
                                                    <span>
                                                        <i class="fas fa-map-marker-alt"></i>
                                                        <a href="http://maps.google.com/maps?saddr=&daddr={{@$product->address}}"
                                                            target="_blank">
                                                            Get Directions on Google MAPS to: {{$product->address}} </a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--GoogleMAP-->
                                </div>
                                <!--widget-box-->
                            </div>
                            <!--col-lg-12-->
                            <!--SidebarWidgets-->
                            <!--SidebarWidgets-->
                        </div>
                        <!--row-->
                    </aside>
                    <!--sidebar-->
                </div>
                <!--col-md-4-->
            </div>
            <!--row-->
        </div>
        <!--container-->
    </section>
    <!-- related post section -->
    <!-- related blog post section -->
    <section class="blog-post-section related-blog-post-section border-bottom">
        <div class="container" style="overflow: hidden;">
            <div class="row">
                <div class="col-sm-6 related-blog-post-head">
                    <h4 class="text-uppercase">Related ads</h4>
                </div>
                <!--col-sm-6-->
                <div class="col-sm-6">
                    <div class="navText text-right flip hidden-xs">
                        <a class="prev btn btn-primary sharp btn-style-one btn-sm"><i
                                class="fas fa-chevron-left"></i></a>
                        <a class="next btn btn-primary sharp btn-style-one btn-sm"><i
                                class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
                <!--col-sm-6-->
            </div>
            <!--row-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="owl-carousel premium-carousel-v1" data-car-length="4" data-items="4" data-loop="true"
                        data-nav="false" data-autoplay="true" data-autoplay-timeout="3000" data-dots="false"
                        data-auto-width="false" data-auto-height="true" data-right="false" data-responsive-small="1"
                        data-autoplay-hover="true" data-responsive-medium="2" data-responsive-xlarge="4"
                        data-margin="30">
                        <!--SingleItem-->
                        @foreach($related as $value)
                        <div class="classiera-box-div-v1 item match-height">
                            <figure>
                                <div class="premium-img">@if($value->featured == 1)
                                    <div class="featured-tag">
                                        <span class="left-corner"></span>
                                        <span class="right-corner"></span>
                                        <div class="featured">
                                            <p>Featured</p>
                                        </div>
                                    </div>@endif
                                    @if ($value->images->count() > 0)
                                    <img class="image-responsive"
                                        src="{{ asset('images_small/'.$value->images->first()->full) }}"
                                        alt="{{$value->name}}">
                                    @else
                                    <img class="image-responsive"
                                        src="{{ asset('storage/'.config('settings.error_image')) }}"
                                        alt="{{$value->name}}">
                                    @endif
                                    <span class="hover-posts">
                                        <a href="{{route('product_details',$value->slug)}}"
                                            class="btn btn-primary sharp btn-sm active">View Ad</a>
                                    </span>
                                </div>
                                <span class="classiera-price-tag" style="background-color:#00beae; color:#00beae;">
                                    <span class="price-text">
                                        Rs.{{number_format($value->price)}} </span>
                                </span>
                                <figcaption>
                                    <h5><a href="{{route('product_details',$value->slug)}}">{{$value->name}}</a></h5>
                                    <p>
                                        Category :
                                        @foreach($value->categories->where('parent_id',!null) as $cat)
                                        <a href="{{route('menu',$cat->slug)}}"
                                            title="View all posts in {{$cat->name}}">{{$cat->name}}</a>

                                    </p>
                                    <span class="category-icon-box"
                                        style=" background:{{$cat->category->color}}; color:{{$cat->category->color}}; ">
                                        <i class="{{$cat->category->font}}"></i>
                                    </span>
                                    @endforeach
                                </figcaption>
                            </figure>
                        </div>
                        <!--item-->
                        <!--SingleItem-->
                        @endforeach
                    </div>
                    <!--owl-carousel-->
                </div>
                <!--col-lg-12-->
            </div>
            <!--row-->
        </div>
    </section><!-- /.related blog post -->
    <!-- Company Section Start-->
    <!-- Company Section End-->
    @include('site.partials.footer')
    @endsection
    @section('post-js')
    <script type="text/javascript">
        function addToLove(el){
      var btn=jQuery(el);
      var product_id=btn.attr('data-product_id');
      var auth_id=btn.attr('data-auth_id');
      var user_id=btn.attr('data-user_id');
      jQuery.ajax({
        url:'{{route("addToLove")}}',
        method:"get",
        data:{'product_id':product_id,'user_id':user_id,'auth_id':auth_id,'_token':'{{csrf_token()}}'},
        success:function(data){
          if(data.status=="1"){
            console.log("added");
              jQuery("#btn___"+product_id).attr("disabled",true).html("Added");
          }else{
            console.log("else");
          }
        },
        error:function(data){
          console.log("error");
        }
      });
      // alert("Hi");
    }
    function addToFollow(el){
    var btn=jQuery(el);
    var auth_id=btn.attr('data-auth_id');
    var user_id=btn.attr('data-user_id');
    jQuery.ajax({
    url:'{{route("addSeller")}}',
    method:"get",
    data:{'user_id':user_id,'auth_id':auth_id,'_token':'{{csrf_token()}}'},
    success:function(data){
    if(data.status=="1"){
    console.log("added");
    jQuery("#btn____"+user_id).attr("disabled",true).html("Added");
    }else{
    console.log("else");
    }
    },
    error:function(data){
    console.log("error");
    }
    });
    // alert("Hi");
    }
    var centreGot = false;

    </script>
    <!-- {!! $map['js'] !!} -->
    @endsection
