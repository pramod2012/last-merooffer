@extends('site.partials.main_index')
@section('title', 'Chat')
@section('content')
	
<body data-rsssl=1 class="page-template page-template-template-message page-template-template-message-php page page-id-388 logged-in theme-classiera woocommerce-no-js single-author">
	
<section class="user-pages section-gray-bg">
	<div class="container">
        <div class="row">
			
			<div class="col-lg-9 col-md-8 user-content-height">
				<div class="user-detail-section section-bg-white">
					<div class="user_inbox_header">
                        <h4 class="user-detail-section-heading text-uppercase">
							Messages						</h4>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
							<li class="active">
                                <a href="#recieve" aria-controls="profile" role="tab" data-toggle="tab">
									Received Offers								</a>
                            </li>
                            <li>
                                <a href="#sent" aria-controls="home" role="tab" data-toggle="tab">
									Sent Offers								</a>
                            </li>                            
                        </ul>
                    </div><!--user_inbox_header-->
					<!--user_inbox_content-->
					<div class="user_inbox_content">
						<div class="tab-content">
							<span class="classiera--loader">
								<img src="https://demo.joinwebs.com/classiera/ivy/wp-content/themes/classiera/images/loader180.gif" alt="classiera loader">
							</span>
														<!--Sent Offers-->
							<div role="tabpanel" class="tab-pane" id="sent">
								<div class="row">
									<div class="col-sm-4">
										<ul class="nav nav-tabs nav-stacked sent_comments"><!--start loop-->
											<li  class="active" >
                                                <a class="user_comment comment_sent" href="#" id="10">
                                                    <img class="user_comment_img thumbnail" src="https://demo.joinwebs.com/classiera/ivy/wp-content/uploads/2017/05/Photography-01.jpg" alt="author">
                                                    <div class="user_comment_body">
                                                    	<p class="text-uppercase">
															Photography Services</p>
                                                        <p>admin</p>
                                                    </div><!--user_comment_body-->
                                                </a>
                                            </li>
											<!--End loop-->
											<li>
                                                <a class="user_comment comment_sent" href="#" id="1">
                                                    <img class="user_comment_img thumbnail" src="https://demo.joinwebs.com/classiera/ivy/wp-content/uploads/2017/05/05.jpg" alt="author">
                                                    <div class="user_comment_body">
                                                        <p class="text-uppercase">
															Pets for Sales in New Jersey USA</p>
                                                        <p>admin</p>
                                                    </div><!--user_comment_body-->
                                                </a>
                                            </li>
											<!--End loop-->
											<li>
                                                <a class="user_comment comment_sent" href="#" id="6">
                                                    <img class="user_comment_img thumbnail" src="https://demo.joinwebs.com/classiera/ivy/wp-content/uploads/2017/05/02.jpg" alt="author">
                                                    <div class="user_comment_body">
                                                        <p class="text-uppercase">
															Apple iPhone Mobile for sale														</p>
                                                        <p>admin</p>
                                                    </div><!--user_comment_body-->
                                                </a>
                                            </li>
											<!--End loop-->
											<li>
                                                <a class="user_comment comment_sent" href="#" id="4">
                                                    <img class="user_comment_img thumbnail" src="https://demo.joinwebs.com/classiera/ivy/wp-content/uploads/2017/05/03.jpg" alt="author">
                                                    <div class="user_comment_body">
                                                        <p class="text-uppercase">
															Bike For Sales in Paisley UK</p>
                                                        <p>admin</p>
                                                    </div><!--user_comment_body-->
                                                </a>
                                            </li>
											<!--End loop-->
											<li >
                                                <a class="user_comment comment_sent" href="#" id="2">
                                                    <img class="user_comment_img thumbnail" src="https://demo.joinwebs.com/classiera/ivy/wp-content/uploads/2017/05/06-1.jpg" alt="author">
                                                    <div class="user_comment_body">
                                                        <p class="text-uppercase">
															Laptop repairing services at your doorstep														</p>
                                                        <p>admin</p>
                                                    </div><!--user_comment_body-->
                                                </a>
                                            </li>
											<!--End loop-->
											<li >
                                                <a class="user_comment comment_sent" href="#" id="15">
                                                    <img class="user_comment_img thumbnail" src="https://demo.joinwebs.com/classiera/ivy/wp-content/uploads/2017/05/02-2.jpg" alt="author">
                                                    <div class="user_comment_body">
                                                        <p class="text-uppercase">
															Hot Coffee Available in Manchester														</p>
                                                        <p>admin</p>
                                                    </div><!--user_comment_body-->
                                                </a>
                                            </li>
											<!--End loop-->
										</div><!--col-sm-4-->
									<div class="col-sm-8">
										<div class="tab-content classiera_sent_comment">
											<div role="tabpanel" class="tab-pane active">
												<span>Photography Services in USA</span>
													</h5>
													<div class="modal-dialog" role="document">
														<div class="modal-content classiera_comment_ajax">
															<div class="modal-body classiera_show_reply" id="10">
																<!--Offer Author box-->
																<div class="classiera_user_message">
																	<a href="#"><img class="img-circle classiera_user_message_img" src="https://demo.joinwebs.com/classiera/ivy/wp-content/uploads/2019/03/snowboarder-150x150.png" alt="author"></a>
																	<div class="classiera_user_message__box">
																		<span>
																		BID PRICE : 
																		2000</span>
																		<p>
																			Bjhxaslcascnkjnj</p>
																		<p class="classiera_user_message__time">
																			July 5, 2018</p>
																	</div>
																</div>
																<!--Offer Author box-->
																<!--Get Sub Comments-->
																<div class="classiera_user_message"><a href="#">
																	<img class="img-circle classiera_user_message_img" src="https://demo.joinwebs.com/classiera/ivy/wp-content/uploads/2019/03/snowboarder-150x150.png" alt="demo">
																</a>
																<div class="classiera_user_message__box">
																	<p>hi</p>
																	<p class="classiera_user_message__time">July 9, 2018</p>
																</div>
															</div>
															<div class="classiera_user_message">
																<a href="#">
																	<img class="img-circle classiera_user_message_img" src="https://demo.joinwebs.com/classiera/ivy/wp-content/uploads/2019/03/snowboarder-150x150.png" alt="demo">
																</a>
																<div class="classiera_user_message__box">
																	<p>hola</p>
																	<p class="classiera_user_message__time">August 21, 2018</p>
																</div>
															</div>
															<div class="classiera_user_message">
																<a href="#">
																	<img class="img-circle classiera_user_message_img" src="https://demo.joinwebs.com/classiera/ivy/wp-content/uploads/2019/03/snowboarder-150x150.png" alt="demo">
																</a>
																<div class="classiera_user_message__box">
																	<p>Hello</p>
																	<p class="classiera_user_message__time">October 25, 2018</p>
																</div>
															</div>
															<div class="classiera_user_message">
																<a href="#">
																	<img class="img-circle classiera_user_message_img" src="https://demo.joinwebs.com/classiera/ivy/wp-content/uploads/2019/03/snowboarder-150x150.png" alt="demo">
																</a>
																<div class="classiera_user_message__box">
																	<p>hi</p>
																	<p class="classiera_user_message__time">February 3, 2021</p>
																</div>
															</div>																<!--Get Sub Comments-->
															</div><!--modal-body-->
															<form method="post" id="resetReply" class="classiera_user_message__form">
																<textarea class="form-control classiera_comment_reply" placeholder="Type your message.." required></textarea>
																<input type="hidden" value="10" class="main_comment_ID">
																<button type="submit" class="classiera_user_message__form_btn">SEND</button>
															</form>
														</div><!--modal-content-->
													</div><!--modal-dialog-->
													<span>Photography Services in USA</span>
													</h5>
													<div class="modal-dialog" role="document">
														<div class="modal-content classiera_comment_ajax">
															<div class="modal-body classiera_show_reply" id="14">
																<!--Offer Author box-->
																<div class="classiera_user_message">
																	<a href="#"><img class="img-circle classiera_user_message_img" src="https://demo.joinwebs.com/classiera/ivy/wp-content/uploads/2019/03/snowboarder-150x150.png" alt="author"></a>
																	<div class="classiera_user_message__box">
																		<span>
																		BID PRICE : 
																		1450																		</span>
																		<p>
																			dfghfgh																		</p>
																		<p class="classiera_user_message__time">
																			November 15, 2018																		</p>
																	</div>
																</div>
																<!--Offer Author box-->
																<!--Get Sub Comments-->
																<div class="classiera_user_message"><a href="#"><img class="img-circle classiera_user_message_img" src="https://demo.joinwebs.com/classiera/ivy/wp-content/uploads/2019/03/snowboarder-150x150.png" alt="demo"></a><div class="classiera_user_message__box"><p>wdasdasdasd</p><p class="classiera_user_message__time">December 4, 2018</p></div></div>																<!--Get Sub Comments-->
															</div><!--modal-body-->
															<form method="post" id="resetReply" class="classiera_user_message__form">
																<textarea class="form-control classiera_comment_reply" placeholder="Type your message.." required></textarea>
																<input type="hidden" value="14" class="main_comment_ID">
																<button type="submit" class="classiera_user_message__form_btn">SEND</button>
															</form>
														</div><!--modal-content-->
													</div><!--modal-dialog-->
																								</div><!--tabpanel-->
										</div><!--tab-content-->
									</div><!--col-sm-8-->
																	</div><!--row-->
							</div>
							<!--Sent Offers-->
							<!--Received Offers-->
							<div role="tabpanel" class="tab-pane active" id="recieve">
								<div class="row">
																			<div class="col-sm-12">
										<h4>You have not received any offer yet..!</h4>
										</div>
																	</div><!--row-->
							</div>
							<!--Received Offers-->
						</div><!--tab-content-->
					</div><!--user_inbox_content-->
					<!--user_inbox_content-->
				</div><!--user-detail-section-->
			</div><!--col-lg-9-->
		</div><!--row-->
	</div><!-- container-->
	<form>
	<input type="hidden" name="classiera_nonce" class="classiera_nonce" value="20f9cd2ed3">
	</form>
</section>
@include('site.partials.footer')
@endsection	
