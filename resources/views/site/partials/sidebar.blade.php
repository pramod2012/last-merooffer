<aside class="section-bg-white">
  <div class="author-info border-bottom">
    <div class="media">
      <div class="media-left">
        <img class="media-object" src="{{asset('frontend/images/user.png')}}" alt="demo">
      </div><!--media-left-->
      <div class="media-body">@if (Auth::check())
        <h5 class="media-heading text-uppercase">
          {{Auth::user()->name}}         
                  </h5>
        <p>Member Since :&nbsp;{{ Auth::user()->created_at->format('F j, Y')}}</p>
                <span><i class="fas fa-circle"></i>Online</span>@endif
              </div><!--media-body-->
    </div><!--media-->
  </div><!--author-info-->

<?php $r = Request::path(); ?>
@if (!Auth::check())
<ul class="user-page-list list-unstyled">
    <li @if($r == 'cart') class="active" @endif>
      <a href="{{url('/cart')}}">
        <span><i class="fas fa-shopping-cart"></i>Carts</span>
        <span class="in-count pull-right flip"></span>
      </a>
    </li>
</ul>
@else
<ul class="user-page-list list-unstyled">
        <li @if($r == 'profile' || $r == 'edit-profile') class="active" @endif>
            <a href="{{route('profile')}}">
                <span>
                    <i class="fas fa-user"></i>
                    About Me
                </span>
            </a>
        </li><!--About-->
        
        
        
        <li @if($r == 'user-all-ads') class="active" @endif>
            <a href="{{route('user-all-ads')}}">
                <span><i class="fas fa-suitcase"></i>My Ads</span>
                <span class="in-count pull-right flip">{{\App\Models\Product::where('user_id', Auth::user()->id)->count()}}</span>
            </a>
        </li><!--My Ads-->
        
        <li @if($r == 'favourite-ads') class="active" @endif>
            <a href="{{route('favourite-ads')}}">
                <span><i class="fas fa-heart"></i>Watch later Ads</span>
                <span class="in-count pull-right flip">{{\App\Models\Wishlist::where('auth_id', Auth::user()->id)->count()}}</span>
            </a>
        </li><!--Watch later Ads-->
        
    <li @if($r == 'cart') class="active" @endif>
      <a href="{{url('/cart')}}">
        <span><i class="fas fa-shopping-cart"></i>Carts</span>
        <span class="in-count pull-right flip"></span>
      </a>
        </li>
      {{-- <li @if($r == 'follow') class="active" @endif>
            <a href="{{route('follow')}}">
                <span><i class="fas fa-users"></i>Follower</span>
            </a>
        </li> --}}

        <li @if($r == 'inbox') class="active" @endif>
            <a href="{{route('inbox')}}">
                <span><i class="fas fa-envelope"></i>Emails</span>
            </a>
        </li><!--follower-->
        <li @if($r == 'order') class="active" @endif>
            <a href="{{url('order')}}">
                <span><i class="fas fa-link"></i>Order</span>
            </a>
        </li><!--follower-->
        <li @if($r == 'orderAdmin') class="active" @endif>
            <a href="{{url('orderAdmin')}}">
                <span><i class="fas fa-link"></i>Order Status</span>
            </a>
        </li><!--follower-->
        
        <li @if($r == 'editfirmuser') class="active" @endif>
            <a href="{{url('/editfirmuser')}}">
                <span><i class="fas fa-user"></i>Employer Profile</span>
            </a>
        </li>
        
        <li class="">
            <a href="/profile/{{str_slug(strtolower(Auth::user()->name), '-')}}" target="_blank">
                <span><i class="fas fa-user"></i>Profile / CV</span>
            </a>
        </li>
        <li @if($r == 'my-jobs') class="active" @endif>
            <a href="{{url('/my-jobs')}}">
                <span><i class="fas fa-briefcase"></i>My Applied Jobs</span>
            </a>
        </li>
        
        <li @if($r == 'job-user') class="active" @endif>
            <a href="{{url('job-user')}}">
                <span><i class="fas fa-briefcase"></i>My Jobs</span>
                <span class="in-count pull-right flip">{{\App\Job::where('user_id', Auth::user()->id)->count()}}</span>
            </a>
        </li>
        <li @if($r == 'project') class="active" @endif>
            <a href="{{url('project')}}">
                <span><i class="fas fa-paper-plane"></i>My Projects</span>
                <span class="in-count pull-right flip">{{\App\Models\Freelancers\Fjob::where('user_id', Auth::user()->id)->count()}}</span>
            </a>
        </li>
        <li @if($r == 'user-resumes') class="active" @endif>
            <a href="{{route('user-resumes')}}">
                <span><i class="fas fa-file"></i>Resumes</span>
            </a>
        </li>
        

        <li>
            <a href="{{route('logout')}}">
                <span><i class="fas fa-sign-out-alt"></i>Logout</span>
            </a>
        </li><!--Logout-->
</ul>

@endif
<div class="user-submit-ad">
        <a href="{{route('job-user.create')}}" class="btn btn-primary sharp btn-block btn-sm @if($r == 'job-user/create') active @else btn-user-submit-ad @endif">
            <i class="icon-left fas fa-plus-circle"></i>
            POST NEW JOB     </a>
    </div><!--user-submit-ad-->
</aside><!--sideBarAffix-->