<?php

namespace App\Contracts;

/**
 * Interface IndustryContract
 * @package App\Contracts
 */
interface IndustryContract extends MyBaseContract
{
	public function findBySlug($slug);
}
