<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adtype extends Model
{
    protected $fillable = [
        'name',
        'status',
    ];
}
