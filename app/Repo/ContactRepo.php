<?php

namespace App\Repo;

use App\Models\Contact;
use App\Contracts\ContactContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubmitContactEmail;
use App\Mail\SubmitContactEmailReceipient;

/**
 * Class ContactRepository
 *
 * @package \App\Repo
 */
class ContactRepo extends BaseRepo implements ContactContract
{
    /**
     * ContactRepository constructor.
     * @param Contact $model
     */
    public function __construct(Contact $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function store($request)
    {
        try {
            $params = $request->all();

            $collection = collect($params);

            $status = $collection->has('status') ? 1 : 0;

            $merge = $collection->merge(compact('status'));

            $data = new Contact($merge->all());

            $data->save();

            $when = now()->addSeconds(10);

            Mail::to($data->email)->later($when, new SubmitContactEmail($data));

            Mail::to($data->product->user->email)->later($when, new SubmitContactEmailReceipient($data));

            return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
        try
        {
        $params = $request->all();

        $data = $this->findById($id);

        $collection = collect($params);

        $status = $collection->has('status') ? 1 : 0;

        $merge = $collection->merge(compact('status'));

        $data->update($merge->all());

        if($collection->has('status')){

        $when = now()->addSeconds(10);

        Mail::to($data->email)->later($when, new SubmitContactEmail($data));

        Mail::to($data->product->user->email)->later($when, new SubmitContactEmailReceipient($data));

        }

        return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);
        $data->delete();
        return $data;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $data = Contact::where('slug', $slug)->firstOrFail();
        return $data;
    }

    public function myContactEmails()
    {
        $user = auth()->user()->id;
        $contact = Contact::orderBy('created_at', 'desc')->where('user_id','=',$user)->with('user')->paginate(4);
        return $contact;
    }
}
