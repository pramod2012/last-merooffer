<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\CommentContract;
use App\Http\Requests\StoreCommentRequest;

class CommentController extends Controller
{
    protected $commentRepo;

    public function __construct(CommentContract $commentRepo)
    {
        $this->commentRepo = $commentRepo;
        $this->middleware('auth');
    }

    public function storeComment(StoreCommentRequest $request)
    {
        $comment = $this->commentRepo->store($request);
        return response()->json(
            [
                'status' => 1,
                'message' => "Thank for your comment! We will get back to you soon !"
            ]
        );
    }

    public function replyStore(Request $request)
    {
        $comment = $this->commentRepo->replyStore($request);
        return response()->json(
            [
                'status' => 1,
                'message' => "Thank for your reply! We will get back to you soon !"
            ]
        );
        // return back()->with('aim', 'Thank for your reply! We will get back to you soon !');
    }
}
