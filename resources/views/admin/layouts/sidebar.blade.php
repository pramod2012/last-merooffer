  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{auth()->user()->name}}</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li>
          <a href="{{route('admin.dashboard')}}"><i class="fa fa-home"></i><span>Admin Home</span></a>
        </li>
        


        <li class="treeview">
          <a href="#"><i class="fa fa-edit"></i> <span>Blog</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('post.index')}}">Blog Post</a></li>
            <li><a href="{{route('author.index')}}">Author</a></li>
            <li><a href="{{route('mcategory.index')}}">Blog Category</a></li>
            <li><a href="{{route('tag.index')}}">Blog Tag</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-opencart"></i> <span>Listings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('product.index')}}">Product</a></li>
            <li><a href="{{route('contact.index')}}">Contact/Message</a></li>
            <li><a href="{{route('report.index')}}">Report</a></li>
            <li><a href="{{route('category.index')}}">Product Category</a></li>
            <li><a href="{{route('bid.index')}}">Offers</a></li>
            <li><a href="{{route('comment.index')}}">Comments</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-cogs"></i> <span>Product Setting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('cities.index')}}">City</a></li>
            <li><a href="{{route('adtype.index')}}">Adtype</a></li>
            <li><a href="{{route('day.index')}}">Days</a></li>
            <li><a href="{{route('pricetype.index')}}">Price Type</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#"><i class="fa fa-align-center"></i> <span>Category Attributes</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('attribute.index')}}">Attributes</a></li>
            <li><a href="{{route('attributevalue.index')}}">Attribute Values</a></li>
            <li><a href="{{route('amenity.index')}}">Amenities</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-briefcase"></i> <span>Job</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('job.index')}}">Job</a></li>
            <li><a href="{{route('jobcategory.index')}}">Job Category</a></li>
            <li><a href="{{route('jobtype.index')}}">Job Type</a></li>
            <li><a href="{{route('skill.index')}}">Job Skills</a></li>
            <li><a href="{{route('industry.index')}}">Industry</a></li>
            <li><a href="{{route('salarytype.index')}}">Salary Type</a></li>
            <li><a href="{{route('positiontype.index')}}">Positon Type</a></li>
          </ul>
        </li>
        <li>
          <a href="{{route('user.index')}}"><i class="fa fa-user"></i> <span>User Management</span></a>
        </li>
        <li><a href="{{ route('admin.settings') }}"><i class="fa fa-cog"></i> <span>Settings</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>