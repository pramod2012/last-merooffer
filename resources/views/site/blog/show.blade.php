@extends('site.partials.main_index')
@section('title', $post->name)
@section('content')
<body class="archive category category-uncategorized category-1 logged-in">
    @include('site.partials.navbar')
<section class="inner-page-content">
	<div class="container">
		<!-- breadcrumb -->
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li>
						<a rel="v:url" property="v:title" href="{{route('index')}}"><i class="fas fa-home"></i></a>
					</li>&nbsp;<li><a href="{{url('blog')}}">Blog</a></li>
					&nbsp;<li>
						<a rel="v:url" property="v:title" href="{{route('blog.category',$post->mcategory->slug)}}">{{$post->mcategory->name}}</a>
					</li>&nbsp;<li class="active">{{$post->name}}</li></ul></div></div>		<!-- breadcrumb -->
		<div class="row top-pad-50">
			<div class="col-md-8 col-lg-9">
				<article class="blog article-content blog-post">
					<div class="single-post border-bottom">
						<h3>{{$post->name}}</h3>
						<p>
                            <span><i class="fas fa-user"></i>{{$post->author->name}}</span>
							                            <span><i class="far fa-clock"></i>{{$post->created_at->format('F j, Y')}}</span>
							                        </p>
												<div class="blog-img">
														<img class="thumbnail" src="{{@asset('storage/'. $post->image)}}" alt="{{$post->name}}">
						</div>
												<div id="lipsum">
<?php
$str = $post->body;
echo html_entity_decode($str);
?>
</div>
						<!--BlogCategories-->
						@if($post->tags->count() > 0)
						<div class="tags">
							<span><i class="fas fa-tags"></i>Tags :</span>
							  @foreach($post->tags as $value)
							<a href="#" rel="tag">{{$value->name}}</a>
							@endforeach
						</div>
						@endif
					</div>
				</article>
			</div><!--col-md-8-->
			<div class="col-md-4 col-lg-3">
				<aside class="sidebar">
					<div class="row">
						<!--subcategory-->
                        <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                            <div class="widget-box">
                                <div class="widget-title">
                                    <h4>
                                        <i class="zmdi zmdi-menu" style="color:red;"></i>
                                        {{$post->mcategory->name}}                                 </h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="category">
                                        @foreach($mcategory as $value)
                                        <li>
                                            <a href="{{route('blog.category',$value->slug)}}">
                                                <i class="fas fa-angle-right"></i>
                                               {{$value->name}}
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                                                <!--subcategory-->
						        <div class="col-lg-12 col-md-12 col-sm-6 match-height">
						        	<div class="widget-box">
						        		<div class="widget-title">
						        			<h4>Blog Recent Posts</h4>
						        		</div>
						        		<div class="widget-content">
						        			@foreach(\App\Models\Post::orderBy('created_at', 'desc')->limit(3)->get() as $post)
	<div class="media footer-pr-widget-v2">
			<div class="media-left">
				<a class="media-img" href="{{route('blog',$post->slug)}}">
				<img class="media-object img-rounded" src="{{@asset('images_small/'. $post->image)}}" alt="{{$post->name}}">
									</a>
			</div>
			<div class="media-body">
				<span><i class="far fa-clock"></i>{{$post->created_at->format('M j, Y')}}</span>
								<h5 class="media-heading">
					<a href="{{route('blog',$post->slug)}}">{{$post->name}}</a>
				</h5>
			</div>
		</div>
		@endforeach
	</div>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-6 match-height">
	<div class="widget-box">
		<div class="widget-title"><h4>Premium Posts</h4>
		</div>


		<div class="widget-content">


			@foreach(\App\Models\Post::where('feature',1)->limit(5)->get() as $post)
			<div class="media footer-pr-widget-v2">
			<div class="media-left">
				<a class="media-img" href="{{route('blog',$post->slug)}}">
				<img class="media-object img-rounded" src="{{@asset('images_small/'. $post->image)}}" alt="{{$post->name}}">
									</a>
			</div>
			<div class="media-body">
				<span><i class="far fa-clock"></i>{{$post->created_at->format('M j, Y')}}</span>
								<h5 class="media-heading">
					<a href="{{route('blog',$post->slug)}}">{{$post->name}}</a>
				</h5>
			</div>
		</div>
			@endforeach
						
						
						
			</div></div></div>						<!--Social Widget-->
												<!--Social Widget-->
					</div>
				</aside>
			</div>
		</div><!--row-->
	</div><!--container-->
</section><!--inner-page-content-->
@include('site.partials.footer')
@endsection