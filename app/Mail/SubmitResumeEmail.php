<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Resume;

class SubmitResumeEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $resume;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Resume $resume)
    {
        $this->resume = $resume;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@merooffer.com')
                    ->subject('Submit Resume Email')
                    ->view('emails.submitResumeEmail')
                    ->with([
                        'name' => $this->resume->name,
                        'email' => $this->resume->email,
                        'cv' => $this->resume->cv,
                        'body' => $this->resume->message,
                        'job_slug' => $this->resume->job->slug,
                        'job_posted_by' => $this->resume->user->name,
                        'job_posted_company' => $this->resume->user->firmuser->name,
                    ]);
    }
}
