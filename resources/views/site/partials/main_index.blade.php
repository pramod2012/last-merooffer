<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="ws_url" content="{{ env('WS_URL') }}">
    <meta name="user_id" content="{{ Auth::id() }}">
    @yield('meta-tags')
    @yield('og-tags')
    @yield('twitter-tags')
    <link rel="canonical" href="https://merooffer.com" />
    <link rel="shortcut icon" href="{{asset('frontend/images/icon.png')}}" />
    <link rel='stylesheet' id='jquery-ui-css' href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" type='text/css'
        media='all' />
    <link rel='stylesheet' id='bootstrap-css' href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" type='text/css'
        media='all' />
    <link rel='stylesheet' id='classiera-components-css' href="{{ asset('frontend/css/classiera-components.css') }}"
        type='text/css' media='all' />
    <link rel='stylesheet' id='classiera-css' href="{{ asset('frontend/css/classiera.css') }}" type='text/css'
        media='all' />
    <link rel='stylesheet' id='font-awesome-css' href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/fontawesome.min.css" type='text/css'
        media='all' />
    <link rel='stylesheet' id='font-awesome-css' href="{{ asset('frontend/css/fontawesomes.css') }}" type='text/css'
        media='all' />
    <link rel='stylesheet' id='material-design-iconic-font-css'
        href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.css" type='text/css' media='all' />
    <link rel='stylesheet' id='owl.carousel.min-css' href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
        type='text/css' media='all' />
    <link rel='stylesheet' id='owl.theme.default.min-css' href="{{ asset('frontend/css/owl.theme.default.min.css') }}"
        type='text/css' media='all' />
    <link rel='stylesheet' id='responsive-css' href="{{ asset('frontend/css/responsive.css') }}" type='text/css'
        media='all' />
    <link rel='stylesheet' id='redux-google-fonts-redux_demo-css' href="{{ asset('frontend/css/css.css') }}"
        type='text/css' media='all' />
    <link rel='stylesheet' id='main-style-css' href="{{ asset('frontend/css/main_style.css') }}" type='text/css'
        media='all' />
    <style type="text/css">
        .recentcomments a {
            display: inline !important;
            padding: 0 !important;
            margin: 0 !important;
        }
    </style>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <style type="text/css" id="wp-custom-css">
        footer .widget-box .widget-title h4 {
            color: #f2fbfe !important;
        }
    </style>
    <style type="text/css" title="dynamic-css" class="options-output">
        h1,
        h1 a {
            font-family: ubuntu;
            line-height: 36px;
            font-weight: 700;
            font-style: normal;
            color: #232323;
            font-size: 36px;
        }

        h2,
        h2 a,
        h2 span {
            font-family: ubuntu;
            line-height: 30px;
            font-weight: 700;
            font-style: normal;
            color: #232323;
            font-size: 30px;
        }

        h3,
        h3 a,
        h3 span {
            font-family: ubuntu;
            line-height: 24px;
            font-weight: 700;
            font-style: normal;
            color: #232323;
            font-size: 24px;
        }

        h4,
        h4 a,
        h4 span {
            font-family: ubuntu;
            line-height: 18px;
            font-weight: 700;
            font-style: normal;
            color: #232323;
            font-size: 18px;
        }

        h5,
        h5 a,
        h5 span {
            font-family: ubuntu;
            line-height: 24px;
            font-weight: normal;
            font-style: normal;
            color: #232323;
            font-size: 14px;
        }

        h6,
        h6 a,
        h6 span {
            font-family: ubuntu;
            line-height: 24px;
            font-weight: normal;
            font-style: normal;
            color: #232323;
            font-size: 12px;
        }

        html,
        body,
        div,
        applet,
        object,
        iframe p,
        blockquote,
        a,
        abbr,
        acronym,
        address,
        big,
        cite,
        del,
        dfn,
        em,
        img,
        ins,
        kbd,
        q,
        s,
        samp,
        small,
        strike,
        strong,
        sub,
        sup,
        tt,
        var,
        b,
        u,
        i,
        center,
        dl,
        dt,
        dd,
        ol,
        fieldset,
        form,
        label,
        legend,
        table,
        caption,
        tbody,
        tfoot,
        thead,
        tr,
        th,
        td,
        article,
        aside,
        canvas,
        details,
        embed,
        figure,
        figcaption,
        footer,
        header,
        hgroup,
        menu,
        nav,
        output,
        ruby,
        section,
        summary,
        time,
        mark,
        audio,
        video {
            font-family: "Open Sans";
            line-height: 24px;
            font-weight: normal;
            font-style: normal;
            color: #6c6c6c;
            font-size: 14px;
        }
    </style>

    <style>
        ul {
            font-family: "Open Sans";
            line-height: 24px;
            font-weight: bold;
            font-style: normal;
            /* color: #bd3030; */
        }
    </style>
    <style>
        /* width */
        ::-webkit-scrollbar {
            width: 12px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            box-shadow: inset 0 0 5px grey;
            border-radius: 10px;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #F5F5F5;
            border-radius: 10px;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
            background: #009996;
        }

        .select2-selection {
            height: 40px !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 38px !important;
        }

        .select2-results__option {
            cursor: pointer;
        }
    </style>
</head>
@yield('content')



<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
@yield('page-script')

<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight.js"></script>
<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.2/masonry.pkgd.min.js"></script>
<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type='text/javascript' src="https://bitbucket.org/pramod2012/merooffercdn/raw/1fe5ff72e5c9c88c16a6459215f300563e0fdab6/js/classiera.js"></script>
@yield('extra-script')
<script type="text/javascript"
    src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type='text/javascript' src="https://bitbucket.org/pramod2012/merooffercdn/raw/0a8ab8d8c33e663ac14ecbf2e6f7eb179c5189ed/app.js"></script>
@yield('post-js')
</body>

</html>
