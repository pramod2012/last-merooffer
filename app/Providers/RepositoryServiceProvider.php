<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;


class RepositoryServiceProvider extends ServiceProvider
{
    protected $repositories = [
        \App\Contracts\AttributevalueContract::class =>  \App\Repo\AttributevalueRepo::class,
        \App\Contracts\PricetypeContract::class      =>  \App\Repo\PricetypeRepo::class,
        \App\Contracts\UserContract::class           =>  \App\Repo\UserRepo::class,
        \App\Contracts\IndustryContract::class       =>  \App\Repo\IndustryRepo::class,
        \App\Contracts\ContactContract::class        =>  \App\Repo\ContactRepo::class,
        \App\Contracts\JobtypeContract::class        => \App\Repo\JobtypeRepo::class,
        \App\Contracts\LevelContract::class          => \App\Repo\LevelRepo::class,
        \App\Contracts\DayContract::class            =>  \App\Repo\DayRepo::class,
        \App\Contracts\JobcategoryContract::class    =>  \App\Repo\JobcategoryRepo::class,
        \App\Contracts\CityContract::class           =>  \App\Repo\CityRepo::class,
        \App\Contracts\AdtypeContract::class         =>  \App\Repo\AdtypeRepo::class,
        \App\Contracts\CategoryContract::class       =>  \App\Repo\CategoryRepo::class,
        \App\Contracts\McategoryContract::class      =>  \App\Repo\McategoryRepo::class,
        \App\Contracts\AttributeContract::class      =>  \App\Repo\AttributeRepo::class,
        \App\Contracts\BrandContract::class          =>  \App\Repo\BrandRepo::class,
        \App\Contracts\ProductContract::class        =>  \App\Repo\ProductRepo::class,
        \App\Contracts\OrderContract::class          =>  \App\Repo\OrderRepo::class,
        \App\Contracts\TagContract::class            =>  \App\Repo\TagRepo::class,
        \App\Contracts\AuthorContract::class         =>  \App\Repo\AuthorRepo::class,
        \App\Contracts\PostContract::class           =>  \App\Repo\PostRepo::class,
        \App\Contracts\SalarytypeContract::class     =>  \App\Repo\SalarytypeRepo::class,
        \App\Contracts\SkillContract::class          =>  \App\Repo\SkillRepo::class,
        \App\Contracts\CommentContract::class        =>  \App\Repo\CommentRepo::class,
        \App\Contracts\BidContract::class            =>  \App\Repo\BidRepo::class,
        \App\Contracts\JobContract::class            =>  \App\Repo\JobRepo::class,
        \App\Contracts\PositiontypeContract::class   =>  \App\Repo\PositiontypeRepo::class,
        \App\Contracts\ResumeContract::class         =>  \App\Repo\ResumeRepo::class,
        \App\Contracts\ReportContract::class         =>  \App\Repo\ReportRepo::class,
        \App\Contracts\ReportnameContract::class     =>  \App\Repo\ReportnameRepo::class,
        \App\Contracts\FollowerContract::class       =>  \App\Repo\FollowerRepo::class,
        \App\Contracts\WishlistContract::class       =>  \App\Repo\WishlistRepo::class,
        \App\Contracts\ProfileContract::class       =>  \App\Repo\ProfileRepo::class,
        \App\Contracts\EducationContract::class       =>  \App\Repo\EducationRepo::class,
        \App\Contracts\ApplicantContract::class       =>  \App\Repo\ApplicantRepo::class,
        \App\Contracts\WorkContract::class       =>  \App\Repo\WorkRepo::class,
        \App\Contracts\TrainingContract::class       =>  \App\Repo\TrainingRepo::class,
        \App\Contracts\EmailContract::class       =>  \App\Repo\EmailRepo::class,
        \App\Contracts\MemailContract::class       =>  \App\Repo\MemailRepo::class,
        \App\Contracts\PartnerContract::class       =>  \App\Repo\PartnerRepo::class,
        \App\Contracts\FirmuserContract::class       =>  \App\Repo\FirmuserRepo::class,
        \App\Contracts\McontactContract::class       =>  \App\Repo\McontactRepo::class,
        \App\Contracts\AmenityContract::class       =>  \App\Repo\AmenityRepo::class,
        \App\Contracts\ConditionContract::class       =>  \App\Repo\ConditionRepo::class,
        \App\Contracts\ExperienceContract::class       =>  \App\Repo\ExperienceRepo::class,
        \App\Contracts\FcategoryContract::class       =>  \App\Repo\FcategoryRepo::class,
        \App\Contracts\FjobContract::class       =>  \App\Repo\FjobRepo::class,
        \App\Contracts\AddressContract::class       =>  \App\Repo\AddressRepo::class,
        \App\Contracts\CourierContract::class       =>  \App\Repo\CourierRepo::class,
        \App\Contracts\PricelabelContract::class       =>  \App\Repo\PricelabelRepo::class,
        \App\Contracts\OrderStatusContract::class   =>  \App\Repo\OrderStatusRepo::class,
        \App\Contracts\CartContract::class   =>  \App\Repo\CartRepo::class,
        \App\Contracts\ProductattributeContract::class   =>  \App\Repo\ProductattributeRepo::class,
        \App\Shop\Orders\Repositories\Interfaces\OrderRepositoryInterface::class   =>  \App\Shop\Orders\Repositories\OrderRepository::class,
        \App\Contracts\AssuredContract::class   =>  \App\Repo\AssuredRepo::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->repositories as $interface => $implementation)
        {
            $this->app->bind($interface, $implementation);
        }
    }
}
