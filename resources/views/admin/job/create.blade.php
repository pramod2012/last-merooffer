@extends('admin.job.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Job Post</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{route('job.store')}}" enctype="multipart/form-data">
                       {{csrf_field()}}
                       
                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Title / name *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                <span class="text-danger">{{ $errors->first('name') }}</span>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Salary *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="salary" value="{{ old('salary') }}">
                                <span class="text-danger">{{ $errors->first('salary') }}</span>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Salary To*</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="salary_to" value="{{ old('salary_to') }}">
                                <span class="text-danger">{{ $errors->first('salary_to') }}</span>

                            </div>
                        </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Salary Type</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="salarytype_id">
                                  @foreach(\App\Salarytype::all() as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('salarytype_id') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Education *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="education" value="{{ old('education') }}">
                                <span class="text-danger">{{ $errors->first('education') }}</span>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Vacancy Number *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="vac_no" value="{{ old('vac_no') }}">
                                <span class="text-danger">{{ $errors->first('vac_no') }}</span>

                            </div>
                        </div>

                        <div class="form-group">
                      <label class="col-md-2 control-label">Experience</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="experience_id">
                                  @foreach(\App\Experience::all() as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('experience_id') }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Age</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="age" value="{{ old('age') }}">
                                <span class="text-danger">{{ $errors->first('age') }}</span>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Gender</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="gender" value="{{ old('gender') }}">

                            </div>
                        </div>

              <div class="form-group">
                      <label class="col-md-2 control-label">City</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="city_id">
                                  @foreach(\App\Models\City::all() as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('city_id') }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Job Location *</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                                <span class="text-danger">{{ $errors->first('address') }}</span>

                            </div>
                        </div>

                      
                        
                        
                  <div class="form-group">
                      <label class="col-md-2 control-label">Job Category</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="category_id">
                                  @foreach($jobcategories as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('category_id') }}</span>
                            </div>
                        </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label">Industries</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="industry_id">
                                  @foreach($industries as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('industry_id') }}</span>
                            </div>
                        </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label">Position Type</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="positiontype_id">
                                  @foreach($positiontypes as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('positiontype_id') }}</span>
                            </div>
                        </div>



                    <div class="form-group">
                      <label class="col-md-2 control-label">Level</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="level_id">
                                  @foreach($levels as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('level_id') }}</span>
                            </div>
                        </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label">User</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="user_id">
                                  @foreach($users as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('user_id') }}</span>
                            </div>
                        </div>


                         <div class="form-group">
                            <label class="col-md-2 control-label">Last Date *</label>
                            <div class="col-md-10">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" value="{{ date('Y/m/d') }}" name="endate" class="form-control pull-right" id="birthDate">
                                    <span class="text-danger">{{ $errors->first('endate') }}</span>
                                </div>
                            </div>
                        </div>


                         <div class="form-group">
                            <label for="post" class="col-md-2 control-label">Body*</label>

                            <div class="col-md-10">
                                <textarea class="form-control" rows="10" id="news" type="text" name="body" value="{{old('body')}}">{{old('body')}}</textarea>

                                    <span class="text-danger">{{ $errors->first('body') }}</span>
                            </div>
                        </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label">Day</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="day_id">
                                  @foreach($days as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('day_id') }}</span>
                            </div>
                        </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label">Job Type</label>
                          <div class="col-md-10"> 
                            <select class="form-control" name="jobtype_id">
                                  @foreach($jobtypes as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('jobtype_id') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Skills</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" name="skills" value="{{ old('skills') }}">
                                <span class="text-danger">{{ $errors->first('skills') }}</span>

                            </div>
                        </div>

          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="feature" value="0">
                  <input name="feature" type="checkbox" value="1">
                      Feature
                    </label>
                  </div>
              </div>
          </div>


          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="status" value="0">
                  <input name="status" type="checkbox" value="1">
                      Status
                    </label>
                  </div>
              </div>
          </div>

                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
