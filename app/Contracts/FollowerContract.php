<?php

namespace App\Contracts;

/**
 * Interface FollowerContract
 * @package App\Contracts
 */
interface FollowerContract extends MyBaseContract
{
	public function myFollower();
	
	public function myFollowing();
}
