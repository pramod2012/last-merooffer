@extends('site.partials.main_index')
@section('title')
    {{$user->name}} - buy or sell {{$user->name}} in Nepal - merooffer.com
@endsection
@section('meta-tags')
<meta name="author" content="{{$user->name}}" />
@endsection
@section('og-tags')
<meta property='og:title' content="{{$user->name}}" />
<meta property='og:url' content="{{app('request')->url()}}">
<meta property=" og:type" content="website" />
<meta property="og:site_name" content="{{app('request')->url()}}" />
@endsection
@section('content')
<body class="archive category category-uncategorized category-1 logged-in">
    @include('site.partials.navbar')
    
    @include('site.partials.search')
    <!--search-section--><!-- page content -->
    <section class="classiera-simple-bg-slider">
    <div class="classiera-simple-bg-slider-content text-center">
        <h1>Find <span class="color">{{$user->name}}'s </span>items near you.
</h1>
        <h4>Long Live Local.</h4>
        <div class="category-slider-small-box text-center">
            <ul class="list-inline list-unstyled">
               {{-- @foreach($category->children as $value)
                    <li>
                        <a class="match-height" href="{{ route('menu', $value->slug) }}">
                            <i class="{{$value->font}}"></i>
                            <p>{{$value->name}}</p>
                        </a>
                    </li>
                @endforeach --}}
            </ul><!--list-inline list-unstyled-->
        </div><!--category-slider-small-box-->
    </div><!--classiera-simple-bg-slider-content-->
</section><!--classiera-simple-bg-slider-->
<section class="classiera-premium-ads-v5 border-bottom section-pad-80">
    <div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 center-block">
                    <h3 class="text-capitalize">PREMIUM ADVERTISEMENT</h3>
                    <p>Top Premium Advertisements of {{$user->name}}</p>
                </div>
            </div>
        </div>
    </div>
    <div style="overflow: hidden; padding-top:10px;">
        <div style="margin-bottom: 40px;">          
            <div id="owl-demo" class="owl-carousel premium-carousel-v5" data-car-length="5" data-items="5" data-loop="true" data-nav="false" data-autoplay="true" data-autoplay-timeout="3000" data-dots="false" data-auto-width="false" data-auto-height="true" data-right="false" data-responsive-small="1" data-autoplay-hover="true" data-responsive-medium="2" data-responsive-large="5" data-responsive-xlarge="7" data-margin="10">
@forelse($user->featuredProducts as $key => $value)
                <div class="classiera-box-div-v5 item match-height">
                    <figure>
                        <div class="premium-img">
                            <div class="featured-tag">
                                <span class="left-corner"></span>
                                <span class="right-corner"></span>
                                <div class="featured">
                                    <p>Featured</p>
                                </div>
                            </div><!--featured-tag-->
                            @if ($value->images->count() > 0)
                                    <img class="img-responsive" src="{{ asset('upsize_images/'.$value->images->first()->full) }}" alt="{{$value->name}}">
                                    @else
                                    <img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
                                    @endif  
                                                        <span class="hover-posts">
                                <a href="{{ route('product_details', $value->slug) }}">view ad</a>
                            </span>
                        <span class="price">
                                Rs. {{ number_format($value->price) }}</span>
                                <span class="classiera-buy-sel">
                            {{@$value->adtype->name}}</span>
                                                    </div><!--premium-img-->
                        <figcaption>
                            <h5><a href="{{ route('product_details', $value->slug) }}">{{$value->name}}</a></h5>
                            <div class="category">
                                <span>
                    Category : 
                    @foreach($value->categories->where('parent_id',!null) as $cat)
                    <a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a> 
                    @endforeach                         </div>
                        </figcaption>
                    </figure>
                </div>
                @empty
                <div class="classiera-box-div-v5 item match-height">
                    <figure>
                        <div class="premium-img">
                            <div class="featured-tag">
                                <span class="left-corner"></span>
                                <span class="right-corner"></span>
                                <div class="featured">
                                    <p>Featured</p>
                                </div>
                            </div><!--featured-tag-->
                            
                                    <img class="img-responsive" src="{{ asset('frontend/images/empty.png') }}" alt="">
                                        
                                                        <span class="hover-posts">
                                <a href="{{ url('submit-ad') }}">Submit Ad</a>
                            </span>
                        <span class="price">
                                Go on, it's Easy</span>
                                <span class="classiera-buy-sel">
                            Want to see your stuff here??</span>
                                                    </div><!--premium-img-->
                        <figcaption>
                            <h5><a href="{{ url('submit-ad') }}">Start Selling</a></h5>
                            
                        </figcaption>
                    </figure>
                </div>
                @endforelse
                
                                                            </div>
        </div>
        <div class="navText">
            <a class="prev btn btn-primary radius outline btn-style-five">
                <i class="icon-left zmdi zmdi-arrow-back"></i>
                Previous            </a>
            <a class="next btn btn-primary radius outline btn-style-five">
                Next                <i class="icon-right zmdi zmdi-arrow-forward"></i>
            </a>
        </div>
    </div>
</section>
<section class="classiera-advertisement advertisement-v5 section-pad-80 border-bottom">
    <div class="tab-divs">
        <div class="view-head">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-sm-7 col-xs-8">
                        <div class="tab-button">
                            <ul class="nav nav-tabs" role="tablist">                                
                                <li role="presentation" class="active">
                                    <a href="#all" aria-controls="all" role="tab" data-toggle="tab">
                                        Latest Ads                                      <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation">                                   
                                    <a href="#popular" aria-controls="popular" role="tab" data-toggle="tab">
                                        Popular Ads                                     <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation">                                    
                                    <a href="#random" aria-controls="random" role="tab" data-toggle="tab">
                                        Random Ads                              <span class="arrow-down"></span>
                                    </a>
                                </li>
                            </ul><!--nav nav-tabs-->
                        </div><!--tab-button-->
                    </div><!--col-lg-6 col-sm-8-->
                    <div class="col-lg-6 col-sm-5 col-xs-4">
                        <div class="view-as text-right flip">
                            <a id="grid" class="grid" href="#">
                                <i class="fas fa-th-large"></i>
                            </a>
                            <a id="grid" class="grid-medium active" href="#">
                                <i class="fa fa-th"></i>
                            </a>
                            <a id="list" class="list" href="#">
                                <i class="fas fa-th-list"></i>
                            </a>                            
                        </div><!--view-as tab-button-->
                    </div><!--col-lg-6 col-sm-4 col-xs-12-->
                </div><!--row-->
            </div><!--container-->
        </div><!--view-head-->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="all">
                <div class="container">
                    <div class="row">
                    <!--FeaturedAds-->
@forelse($user->latestProducts as $key => $value)
                    <div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid-medium">
    <div class="classiera-box-div classiera-box-div-v5">
        <figure class="clearfix">
            <div class="premium-img">
                @if($value->featured == 1)
                                <div class="featured-tag">
                        <span class="left-corner"></span>
                        <span class="right-corner"></span>
                        <div class="featured">
                            <p>Featured</p>
                        </div>
                    </div>
                    @endif
                    @if ($value->images->count() > 0)
                    <img class="img-responsive" src="{{ asset('upsize_images/'.$value->images->first()->full) }}" alt="{{$value->name}}">
                    @else
                    <img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
                    @endif
                                    <span class="hover-posts">                                          
                    <a href="{{ route('product_details', $value->slug) }}">View Ad</a>
                </span>
                <span class="price">
                        PRICE: Rs.{{ number_format($value->price) }}</span>
                        <span class="classiera-buy-sel">{{@$value->adtype->name}}</span>
                            </div><!--premium-img-->
            <div class="detail text-center">
                                <span class="price">Rs. {{ number_format($value->price) }}</span>
                                <div class="box-icon">@if($value->user->show_email == 1)
                    <a href="mailto:{{ $value->user->email }}?subject">
                        <i class="fas fa-envelope"></i>
                    </a>@endif       @if($value->user->show_mobile == 1)
                                        <a href="tel:{{ $value->user->mobile}}"><i class="fas fa-phone"></i></a>@endif
                                    </div>
                <a href="{{ route('product_details', $value->slug) }}" class="btn btn-primary outline btn-style-five">View Ad</a>
            </div><!--detail text-center-->
            <figcaption>
                                <span class="price visible-xs">
                    Rs.{{ number_format($value->price) }}</span>
                                <h5><a href="{{ route('product_details',$value->slug) }}">{{$value->name}}</a></h5>
                <div class="category">
                    <span>
                    Category : 
                    @foreach($value->categories->where('parent_id',!null) as $cat)
                    
                    <a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a>
                @endforeach </span>
                                        <span>Location : 
                        <a href="{{route('findByCity',$value->city->slug)}}" title="View all ads in {{$value->city->name}}">{{$value->city->name}}</a>
                    </span>
                </div>
                <p class="description">{{substr($value->desc, 0, 340)}}</p>
            </figcaption>
        </figure>
    </div><!--row-->
</div><!--col-lg-4-->
@empty
<div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
    <div class="classiera-box-div classiera-box-div-v5">
        <figure class="clearfix">
            <div class="premium-img">
                
                    <img class="img-responsive" src="{{ asset('frontend/images/empty.png') }}" alt="Submit Ads">
                                    <span class="hover-posts">                                          
                    <a href="{{ url('submit-ad') }}">Submit Ad</a>
                </span>
                <span class="price">
                        Go On, It's Easy</span>
                        <span class="classiera-buy-sel">Want to see your stuff here??</span>
                            </div><!--premium-img-->
            <div class="detail text-center">
                                <span class="price">Go On, It's Easy</span>
                                <div class="box-icon">
                                    </div>
                <a href="{{ url('submit-ad') }}" class="btn btn-primary outline btn-style-five">Submit Ad</a>
            </div><!--detail text-center-->
            <figcaption>
                                <span class="price visible-xs">
                    Go On, It's Easy</span>
                                <h5><a href="{{ url('submit-ad') }}">Start Selling</a></h5>
                <div class="category">
                    
                                        
                </div>
                <p class="description">Make some extra cash by selling things in your community. Go on, It's Quick and Easy .</p>
            </figcaption>
        </figure>
    </div><!--row-->
</div><!--col-lg-4-->
@endforelse

                    </div><!--row-->
                </div><!--container-->
            </div><!--tabpanel-->
            <div role="tabpanel" class="tab-pane fade" id="random">
                <div class="container">
                    <div class="row">
    @foreach($user->randomProducts as $key => $value)
                    <div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid-medium">
    <div class="classiera-box-div classiera-box-div-v5">
        <figure class="clearfix">
            <div class="premium-img">
                @if($value->featured == 1)
                                <div class="featured-tag">
                        <span class="left-corner"></span>
                        <span class="right-corner"></span>
                        <div class="featured">
                            <p>Featured</p>
                        </div>
                    </div>
                    @endif
                    @if ($value->images->count() > 0)
                    <img class="img-responsive" src="{{ asset('images_small/'.$value->images->first()->full) }}" alt="{{$value->name}}" style="max-height: 100%">
                    @else
                    <img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
                    @endif
                                    <span class="hover-posts">                                          
                    <a href="{{ route('product_details', $value->slug) }}">View Ad</a>
                </span>
                <span class="price">
                        PRICE: Rs.{{ number_format($value->price) }}</span>
                        <span class="classiera-buy-sel">{{@$value->adtype->name}}</span>
                            </div><!--premium-img-->
            <div class="detail text-center">
                                <span class="price">Rs. {{ number_format($value->price) }}</span>
                                <div class="box-icon">
                                    @if($value->user->show_email == 1)
                    <a href="mailto:{{ $value->user->email }}?subject">
                        <i class="fas fa-envelope"></i>
                    </a>@endif      @if($value->user->show_mobile == 1)
                                        <a href="tel:{{ $value->user->mobile}}"><i class="fas fa-phone"></i></a>@endif
                                    </div>
                <a href="{{ route('product_details', $value->slug) }}" class="btn btn-primary outline btn-style-five">View Ad</a>
            </div><!--detail text-center-->
            <figcaption>
                                <span class="price visible-xs">
                    Rs.{{ number_format($value->price) }}</span>
                                <h5><a href="{{ route('product_details',$value->slug) }}">{{$value->name}}</a></h5>
                <div class="category">
                    <span>
                    Category : 
                    @foreach($value->categories->where('parent_id',!null) as $cat)
                    
                    <a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a>
                @endforeach </span>
                                        <span>Location : 
                        <a href="{{route('findByCity',$value->city->slug)}}" title="View all ads in {{$value->city->name}}">{{$value->city->name}}</a>
                    </span>
                </div>
                <p class="description">{{substr($value->desc, 0, 340)}}</p>
            </figcaption>
        </figure>
    </div><!--row-->
</div><!--col-lg-4-->
@endforeach
</div><!--row-->
                </div><!--container-->
            </div><!--tabpanel Random-->
            <div role="tabpanel" class="tab-pane fade" id="popular">
                <div class="container">
                    <div class="row">
    @foreach($user->popularProducts as $key => $value)
                    <div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid-medium">
    <div class="classiera-box-div classiera-box-div-v5">
        <figure class="clearfix">
            <div class="premium-img">
                @if($value->featured == 1)
                                <div class="featured-tag">
                        <span class="left-corner"></span>
                        <span class="right-corner"></span>
                        <div class="featured">
                            <p>Featured</p>
                        </div>
                    </div>
                    @endif
                    @if ($value->images->count() > 0)
                    <img class="img-responsive" src="{{ asset('images_small/'.$value->images->first()->full) }}" alt="{{$value->name}}" style="max-height: 100%">
                    @else
                    <img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
                    @endif
                                    <span class="hover-posts">                                          
                    <a href="{{ route('product_details',$value->slug) }}">View Ad</a>
                </span>
                <span class="price">
                        PRICE: Rs.{{ number_format($value->price) }}</span>
                        <span class="classiera-buy-sel">{{@$value->adtype->name}}</span>
                            </div><!--premium-img-->
            <div class="detail text-center">
                                <span class="price">
                    Rs.{{ number_format($value->price) }}</span>
                                <div class="box-icon">
                                    @if($value->user->show_email == 1)
                    <a href="mailto:{{$value->user->email}}?subject">
                        <i class="fas fa-envelope"></i>
                    </a>@endif
                    @if($value->user->show_phone == 1)
                                        <a href="tel:{{$value->user->mobile}}"><i class="fas fa-phone"></i></a>@endif
                                    </div>
                <a href="{{ route('product_details', $value->slug) }}" class="btn btn-primary outline btn-style-five">View Ad</a>
            </div><!--detail text-center-->
            <figcaption>
                                <span class="price visible-xs">
                    Rs.{{ number_format($value->price) }}</span>
                                <h5><a href="{{ route('product_details',$value->slug) }}">{{$value->name}}</a></h5>
                <div class="category">
                    <span>
                    Category : 
                @foreach($value->categories->where('parent_id',!null) as $cat)
                    
                    <a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a>
                @endforeach </span>
                                        <span>Location : 
                        <a href="{{route('findByCity',$value->city->slug)}}" title="View all posts in {{$value->city->name}}">{{$value->city->name}}</a>
                    </span>
                </div>
                <p class="description">{{substr($value->desc, 0, 340)}}</p>
            </figcaption>
        </figure>
    </div><!--row-->
</div><!--col-lg-4-->
@endforeach
                </div><!--row-->
                </div><!--container-->
            </div><!--tabpanel popular-->
                        <div class="view-all text-center">               
                <a href="#" class="btn btn-primary outline btn-md btn-style-five">
                    View All Ads                </a>
            </div>
        </div><!--tab-content-->
    </div><!--tab-divs-->
</section>

@include('site.partials.footer')
@section('page-script')
    <script type='text/javascript' src="{{ asset('frontend/js/validator.min.js') }}"></script>
@stop
@endsection