<?php

namespace App\Contracts;

/**
 * Interface ExperienceContract
 * @package App\Contracts
 */
interface ExperienceContract extends MyBaseContract
{
	public function findBySlug($slug);
}
