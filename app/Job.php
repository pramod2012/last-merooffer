<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Skill;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Expirable;
use App\Traits\Searchable;

class Job extends Model
{
    use SoftDeletes, /*Expirable,*/ Searchable;

    protected $dates = ['deleted_at'];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->order = $model->max('order') + 1;
        });
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City', 'city_id');
    }

    public function category()
    {
        return $this->belongsTo('App\JobCategory', 'category_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    function salarytype()
    {
        return $this->belongsTo('App\Salarytype');
    }
    function jobtype()
    {
        return $this->belongsTo('App\Jobtype');
    }

    function applicants()
    {
        return $this->belongsToMany('App\Applicant', 'applicant_job', 'job_id', 'applicant_user_id');
    }

    function industry()
    {
        return $this->belongsTo('App\Industry');
    }

    function positiontype()
    {
        return $this->belongsTo('App\Positiontype');
    }

    function level()
    {
        return $this->belongsTo('App\Level');
    }
    function day()
    {
        return $this->belongsTo('App\Models\Day');
    }

    function experience()
    {
        return $this->belongsTo('App\Experience');
    }

    public function skillss()
    {
        return $this->belongsToMany(Skill::class, 'job_skill');
    }
}
