<?php

namespace App\Contracts;

/**
 * Interface WishlistContract
 * @package App\Contracts
 */
interface WishlistContract extends MyBaseContract
{
	public function wishlistAds();
}
