<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCategory extends Model
{
   protected $table = 'job_categories';

   protected $fillable = [
        'name',
        'font',
        'color',
        'order',
        'featured',
        'desc',
        'slug',
        'status',
        'meta_title',
        'meta_keywords',
        'meta_body'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->order = $model->max('order') + 1;
        });
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        if (static::whereSlug($slug = str_slug($value))->exists())
        {
            $slug = $this->incrementSlug($slug);
        }

        $this->attributes['slug'] = $slug;
    }

    public function incrementSlug($slug)
    {
        $original = $slug;
        $count = 2;
        while (static::whereSlug($slug)->exists()) 
        {
           $slug = $count++ . "-{$original}";
        }
        return $slug;
    }

    function jobs() {
    	return $this->hasMany('App\Job','category_id');
    }

}
