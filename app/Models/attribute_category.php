<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class attribute_category extends Model
{
    protected $fillable = [
        'attribute_id',
        'category_id',
    ];
}
