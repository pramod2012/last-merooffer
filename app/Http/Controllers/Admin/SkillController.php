<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\SkillContract;
use App\Http\Controllers\BaseController;

class SkillController extends BaseController
{
    private $page="admin.skill.";

    protected $skillRepo;

    public function __construct(
        SkillContract $skillRepo
    )
    {
        $this->skillRepo = $skillRepo;
    }

    public function index()
    {
        $list = $this->skillRepo->list('created_at', 'desc');
        $skills = $this->skillRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('Skills', 'List of Skills');
        return view($this->page.'index',compact('skills'));
    }

    public function create()
    {
        $this->setPageTitle('skill', 'Create skill');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->skillRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('skill.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $skill = $this->skillRepo->findById($id);
        $this->setPageTitle('Skill', 'Edit skill');
        return view($this->page.'edit', compact('skill'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->skillRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('skill.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->skillRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('skill.index', 'Record deleted successfully' ,'success',false, false);
    }
}
