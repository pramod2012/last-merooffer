@extends('site.partials.main_index')
@section('title','Checkout Items')
@section('content')

<body
    class="page-template page-template-template-favorite page-template-template-favorite-php page page-id-22 logged-in">
    @include('site.partials.navbar')
    <section class="user-pages section-gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    @include('site.partials.sidebar')
                </div>
                <!--col-lg-3-->
                <div class="col-lg-9 col-md-8 user-content-height">
                    <div class="user-detail-section section-bg-white">
                        @include('partials.alert')
                        <!-- favorite ads -->
                        <div class="user-ads favorite-ads">
                            <h4 class="user-detail-section-heading text-uppercase">
                                Checkout ( {{Cart::count()}} ) </h4>
                            <!--singlepost-->
                            @if(count($addresses) > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- <div class="row header hidden-xs hidden-sm"> -->
                                    <div class="row hidden-xs hidden-sm" style="height: 40px;">



                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><b>Cover</b></div>
                                            </div>
                                        </div>

                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3"><b>Name</b></div>
                                                <div class="col-lg-3 col-md-3"><b>Quantity</b></div>
                                                <div class="col-lg-1 col-md-1"><b>Remove</b></div>
                                                <div class="col-lg-2 col-md-2"><b>Price</b></div>
                                                <div class="col-lg-2 col-md-2"><b>Total</b></div>
                                            </div>
                                        </div>



                                    </div>
                                    @foreach($cartItems as $cartItem)
                                    <div class="row">

                                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                                    <a href="#" class="hover-border">
                                                        @if(isset($cartItem->cover))
                                                        <img src="{{ asset('images_small/'.$cartItem->cover) }}"
                                                            alt="{{ $cartItem->name }}"
                                                            class="img-responsive img-thumbnail">
                                                        @else
                                                        <img src="https://placehold.it/120x120" alt=""
                                                            class="img-responsive img-thumbnail">
                                                        @endif
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-9 col-xs-8">
                                            <div class="row">


                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                    <h4 style="margin-bottom:5px;">{{$cartItem->name}}</h4>
                                                    <!-- </div> -->
                                                </div>

                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-8">
                                                    <form action="{{ route('cart.update', $cartItem->rowId) }}"
                                                        class="form-inline" method="post">
                                                        @csrf
                                                        {{method_field('PATCH')}}
                                                        <div class="input-group">
                                                            <input type="text" name="quantity"
                                                                value="{{ $cartItem->qty }}"
                                                                class="form-control input-sm" />
                                                            <span class="input-group-btn"><button
                                                                    class="btn btn-primary btn-sm">Update</button></span>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-8 col-xs-4">
                                                    <form action="{{ route('cart.destroy', $cartItem->rowId) }}"
                                                        method="post">
                                                        @csrf
                                                        {{method_field('DELETE')}}
                                                        <button onclick="return confirm('Are you sure?')"
                                                            class="btn btn-danger btn-sm"><i
                                                                class="fa fa-times"></i></button>
                                                    </form>
                                                </div>
                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                    <span class="hidden-lg hidden-md"><small>Price: </span>
                                                    Rs. {{ number_format($cartItem->price, 2) }}</small>
                                                </div>
                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                    <span class="hidden-lg hidden-md"><small>Total: </span>
                                                    Rs. {{ number_format(($cartItem->qty*$cartItem->price), 2)
                                                    }}</small>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    <br>
                                    @endforeach


                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12 content">
                                    <table class="table table-striped">
                                        <tfoot>
                                            <tr>
                                                <td class="bg-warning">Subtotal</td>
                                                <td class="bg-warning"></td>
                                                <td class="bg-warning"></td>
                                                <td class="bg-warning"></td>
                                                <td class="bg-warning">Rs. {{ number_format($subtotal, 2, '.', ',') }}
                                                </td>
                                            </tr>
                                            @if(isset($shippingFee) && $shippingFee != 0)
                                            <tr>
                                                <td class="bg-warning">Shipping</td>
                                                <td class="bg-warning"></td>
                                                <td class="bg-warning"></td>
                                                <td class="bg-warning"></td>
                                                <td class="bg-warning">Rs. {{ $shippingFee }}</td>
                                            </tr>@endif
                                            <tr>
                                                <td class="bg-warning">Tax</td>
                                                <td class="bg-warning"></td>
                                                <td class="bg-warning"></td>
                                                <td class="bg-warning"></td>
                                                <td class="bg-warning">Rs. {{ $tax }}</td>
                                            </tr>
                                            <tr>
                                                <td class="bg-success">Total</td>
                                                <td class="bg-success"></td>
                                                <td class="bg-success"></td>
                                                <td class="bg-success"></td>
                                                <td class="bg-success">Rs. {{ number_format($total, 2, '.', ',') }}</td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                            <form action="{{ url('/bank-transfer') }}" method="get">
                                @if(isset($addresses))

                                <div class="row">
                                    <div class="col-md-12">
                                        <legend><i class="fa fa-home"></i> Addresses</legend>
                                        <table class="table table-striped">
                                            <thead>
                                                <th>Alias</th>
                                                <th>Address</th>
                                                <th>Billing Address</th>
                                                <th>Delivery Address</th>
                                            </thead>
                                            <tbody>
                                                @foreach($addresses as $key => $address)
                                                <tr>
                                                    <td><input type="radio" value="{{$address->id}}"
                                                            style="opacity:1;z-index:1;visibility:visible;position:relative !important"
                                                            checked name="billing_address">
                                                        {{ $address->alias }}</td>
                                                    <td>
                                                        {{ $address->address_1 }} {{ $address->address_2 }} <br />
                                                        @if(!is_null($address->province))
                                                        {{ $address->city }} {{ $address->province }} <br />
                                                        @endif
                                                        {{ $address->city }} {{ $address->state_code }} <br>
                                                        {{ $address->country }} {{ $address->zip }}, {{ $address->phone
                                                        }}
                                                    </td>
                                                    <td>
                                                        {{-- <label class="col-md-6 col-md-offset-3">
                                                            <input type="radio" value="{{ $address->id }}"
                                                                name="billing_address" @if($billingAddress->id ==
                                                            $address->id) checked="checked" @endif>
                                                        </label> --}}
                                                    </td>
                                                    <td>
                                                        @if($billingAddress->id == $address->id)
                                                        <label for="sameDeliveryAddress">
                                                            <input type="checkbox" id="sameDeliveryAddress"
                                                                checked="checked"> Same as billing
                                                        </label>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col-md-12">
                                        <legend><i class="fa fa-credit-card"></i> Payment</legend>

                                        <table class="table table-striped">
                                            <thead>
                                                <th class="col-md-4">Name</th>
                                                <th class="col-md-4">Description</th>
                                                <th class="col-md-4 text-right">Choose payment</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Bank Transfer</td>
                                                    <td>Online / Offline Bank fund transfer</td>
                                                    <td>
                                                        {{-- <form action="{{ url('/bank-transfer') }}" method="get">
                                                            --}}
                                                            {{-- <input type="hidden" class="billing_address"
                                                                name="billing_address" value="{{$billingAddress->id}}">
                                                            --}}
                                                            <button type="submit" class="btn btn-warning pull-right">Pay
                                                                with Bank Transfer <i class="fas fas-bank"></i></button>
                                                            {{--
                                                        </form> --}}
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </form>

                            @else
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="alert alert-warning">No address found. You need to create an address first
                                        here. <a href="{{url('addresses')}}">Create Address</a></p>
                                </div>
                            </div>
                            @endif



                        </div>
                        <!--singlepost-->
                    </div>
                    <!--user-ads-->
                    <!-- favorite ads -->
                </div>
                <!--user-detail-->
            </div>
            <!--col-lg-9-->
        </div>
        <!--row-->
        </div>
        <!--container-->
    </section>
    <!--user-pages-->

    <script type="text/javascript">
        function setTotal(total, shippingCost) {
            let computed = +shippingCost + parseFloat(total);
            $('#total').html(computed.toFixed(2));
        }

        function setShippingFee(cost) {
            el = '#shippingFee';
            $(el).html(cost);
            $('#shippingFeeC').val(cost);
        }

        function setCourierDetails(courierId) {
            $('.courier_id').val(courierId);
        }

        $(document).ready(function () {

            let clicked = false;

            $('#sameDeliveryAddress').on('change', function () {
                clicked = !clicked;
                if (clicked) {
                    $('#sameDeliveryAddressRow').show();
                } else {
                    $('#sameDeliveryAddressRow').hide();
                }
            });

            let billingAddress = 'input[name="billing_address"]';
            $(billingAddress).on('change', function () {
                let chosenAddressId = $(this).val();
                $('.address_id').val(chosenAddressId);
                $('.delivery_address_id').val(chosenAddressId);
            });

            let deliveryAddress = 'input[name="delivery_address"]';
            $(deliveryAddress).on('change', function () {
                let chosenDeliveryAddressId = $(this).val();
                $('.delivery_address_id').val(chosenDeliveryAddressId);
            });

            let courier = 'input[name="courier"]';
            $(courier).on('change', function () {
                let shippingCost = $(this).data('cost');
                let total = $('#total').data('total');

                setCourierDetails($(this).val());
                setShippingFee(shippingCost);
                setTotal(total, shippingCost);
            });

            if ($(courier).is(':checked')) {
                let shippingCost = $(courier + ':checked').data('cost');
                let courierId = $(courier + ':checked').val();
                let total = $('#total').data('total');

                setShippingFee(shippingCost);
                setCourierDetails(courierId);
                setTotal(total, shippingCost);
            }
        });
    </script>
    <!-- Company Section Start-->
    <!-- Company Section End-->
    @include('site.partials.footer')
    @endsection
