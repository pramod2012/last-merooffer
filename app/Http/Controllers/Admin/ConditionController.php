<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\ConditionContract;
use App\Http\Controllers\BaseController;

class ConditionController extends BaseController
{
    private $page="admin.condition.";

    protected $conditionRepo;

    public function __construct(
        ConditionContract $conditionRepo
    )
    {
        $this->conditionRepo = $conditionRepo;
    }

    public function index()
    {
        $list = $this->conditionRepo->list('created_at', 'desc');
        $datas = $this->conditionRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('condition', 'List of conditions');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('condition', 'Create condition');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->conditionRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('condition.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->conditionRepo->findById($id);
        $this->setPageTitle('condition', 'Edit condition');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->conditionRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('condition.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->conditionRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('condition.index', 'Record deleted successfully' ,'success',false, false);
    }
}
