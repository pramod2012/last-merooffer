@extends('site.partials.main_index')
@section('title','Post from ' . $mcategory->name)
@section('content')
<body data-rsssl=1 class="archive post-type-archive post-type-archive-blog logged-in theme-classiera woocommerce-no-js single-author">
@include('site.partials.navbar')
		<section class="inner-page-content border-bottom">
	<div class="container">
		<!-- breadcrumb -->
			<div class="row"><div class="col-lg-12"><ul class="breadcrumb"><li><a rel="v:url" property="v:title" href="{{route('index')}}"><i class="fas fa-home"></i></a></li>&nbsp;<li><a href="{{url('blog')}}">Blog</a></li>&nbsp;<li class="active">{{$mcategory->name}}</li></ul></div></div>		<!-- breadcrumb -->
		<div class="row top-pad-50">			
			<div class="col-md-8 col-lg-9">

@foreach($mcategory->posts as $post)								
<article class="blog article-content">
	<h3><a href="{{route('blog',$post->slug)}}">{{$post->name}}</a></h3>
	<p>
		<span><i class="fas fa-user"></i>{{$post->author->name}}</span>
				<span><i class="far fa-clock"></i>{{$post->created_at->format('F j, Y')}}</span>
			</p>
		<div class="blog-img">
			<img class="thumbnail" src="{{@asset('storage/'. $post->image)}}" alt="{{$post->name}}">
	</div>
		<p>{!! substr($post->body, 0, 1000) !!} [&hellip;]</p>
	<a href="{{route('blog',$post->slug)}}" class="btn btn-primary btn-md sharp btn-style-one">
		<i class="icon-left fas fa-arrow-circle-right"></i>Read More	</a>
</article>
@endforeach										
									
</div><!--col-md-8 col-lg-9-->
			<div class="col-md-4 col-lg-3">
				<aside class="sidebar">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-6 match-height">
							<div class="widget-box">
                                <div class="widget-title">
                                    <h4>
                                        <i class="zmdi zmdi-menu" style="color:#c29bc2;"></i>
                                        Menu                                 </h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="category">
                                        @foreach($mcategories as $value)
                                        <li>
                                            <a href="{{route('blog.category',$value->slug)}}">
                                                <i class="fas fa-angle-right"></i>
                                               {{$value->name}}
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
							<div class="widget-box">
								<div class="widget-title"><h4>Blog Recent Posts</h4></div>
								<div class="widget-content">
@foreach(\App\Models\Post::orderBy('created_at', 'desc')->limit(3)->get() as $post)
	<div class="media footer-pr-widget-v2">
			<div class="media-left">
				<a class="media-img" href="{{route('blog',$post->slug)}}">
				<img class="media-object img-rounded" src="{{@asset('images_small/'. $post->image)}}" alt="{{$post->name}}">
									</a>
			</div>
			<div class="media-body">
				<span><i class="far fa-clock"></i>{{$post->created_at->format('M j, Y')}}</span>
								<h5 class="media-heading">
					<a href="{{route('blog',$post->slug)}}">{{$post->name}}</a>
				</h5>
			</div>
		</div>
		@endforeach
</div>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-6 match-height">
	<div class="widget-box">
		<div class="widget-title"><h4>Premium Posts</h4></div>
		<div class="widget-content">
@foreach(\App\Models\Post::where('feature',1)->limit(5)->get() as $post)
			<div class="media footer-pr-widget-v2">
			<div class="media-left">
				<a class="media-img" href="{{route('blog',$post->slug)}}">
				<img class="media-object img-rounded" src="{{@asset('images_small/'. $post->image)}}" alt="{{$post->name}}">
									</a>
			</div>
			<div class="media-body">
				<span><i class="far fa-clock"></i>{{$post->created_at->format('M j, Y')}}</span>
								<h5 class="media-heading">
					<a href="{{route('blog',$post->slug)}}">{{$post->name}}</a>
				</h5>
			</div>
		</div>
			@endforeach
			</div>
		</div>
	</div>
</div>
				</aside>
			</div>
		</div><!--row top-pad-50-->
	</div><!--container-->
</section>
@include('site.partials.footer')
@endsection