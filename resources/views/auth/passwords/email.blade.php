@extends('site.partials.main_index')
@section('content')  
<body class="page-template page-template-template-login page-template-template-login-php page page-id-28">
@include('site.partials.navbar')
    <!-- page content -->
<section class="inner-page-content border-bottom top-pad-50">
    <div class="login-register login-register-v1">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-11 col-sm-12 center-block">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="classiera-login-register-heading border-bottom text-center">
                                <h3 class="text-uppercase">Forget Password ?</h3>
                                            @if (session('status'))
                                                <div class="alert alert-success">
                                                    {{ session('status') }}
                                                </div>
                                            @endif
                            </div><!--classiera-login-register-heading-->                     
                                    
                                </div>
                            </div><!--social-login-->
                                                    </div><!--col-lg-12-->
                    </div><!--row-->
                    <div class="row">
                        <div class="col-lg-8 center-block">
                            <form method="POST" action="{{ route('password.email') }}" id="classiera_login_form" name="register">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3 single-label">
                                            <label for="username">Email : <span class="text-danger">*</span></label>
                                        </div><!--col-lg-3-->
                                        <div class="col-lg-9">
                                            <div class="inner-addon left-addon">
                                                <i class="left-addon form-icon fas fa-envelope"></i>
                                                <input type="text" id="email" name="email" value="{{ old('email') }}" class="form-control form-control-md sharp-edge" placeholder="Your Email">
                                                @if ($errors->has('email'))
                                                <div class="help-block with-errors">{{ $errors->first('email') }}</div>
                                                @endif
                                            </div>
                                        </div><!--col-lg-9-->
                                    </div><!--row-->
                                </div><!--UserName-->
                                
                                <div class="col-lg-9 pull-right flip">
                                    <div class="form-group clearfix">
                                        
                                        <p class="forget-pass pull-right flip">
                                            Already have account?<a href="{{ route('login') }}"> Login</a>
                                        </p>
                                    </div>
                                    <!--Google-->
                                                                        
                                    <!--Google-->
                                    <div class="form-group">              
                                        <button class="btn btn-primary sharp btn-md btn-style-one" id="edit-submit" name="op" value="submit" type="submit">Send password Reset Link</button>
                                    </div>                                  
                                    <div class="form-group">
                                        <p>If you don’t have account? 
                                            <a href="{{ route('register') }}">Create an account.</a>
                                        </p>
                                    </div>
                                </div><!--Rememberme-->
                            </form>
                        </div><!--col-lg-8-->
                    </div><!--row-->
                </div><!--col-lg-10-->
            </div><!--row-->    
        </div><!--container-->  
    </div><!--login-register login-register-v1-->
</section>
<!-- page content -->
@include('site.partials.footer')
@endsection