<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\FirmuserContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests\StoreFirmuserRequest;

class FirmuserController extends BaseController
{
    private $page="admin.firmuser.";

    protected $firmuserRepo;

    public function __construct(
        FirmuserContract $firmuserRepo
    )
    {
        $this->firmuserRepo = $firmuserRepo;
    }

    public function index()
    {
        $list = $this->firmuserRepo->list('created_at', 'desc');
        $datas = $this->firmuserRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('firmuser', 'List of firmuser');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('firmuser', 'Create firmuser');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->firmuserRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('firmusers.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->firmuserRepo->findById($id);
        $this->setPageTitle('firmuser', 'Edit firmuser');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->firmuserRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('firmusers.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->firmuserRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('firmusers.index', 'Record deleted successfully' ,'success',false, false);
    }
}
