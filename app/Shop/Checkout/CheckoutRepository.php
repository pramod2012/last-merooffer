<?php

namespace App\Shop\Checkout;

use App\Events\OrderCreateEvent;
use App\Repo\CartRepo;
use App\Shop\Carts\ShoppingCart;
use App\Models\Order;
use App\Repo\OrderRepo;

class CheckoutRepository
{
    /**
     * @param array $data
     *
     * @return Order
     */
    public function buildCheckoutItems(array $data) : Order
    {
        $orderRepo = new OrderRepo(new Order);

        $order = $orderRepo->createOrder([
            'reference' => $data['reference'],
            'courier_id' => $data['courier_id'],
            'user_id' => $data['user_id'],
            'address_id' => $data['address_id'],
            'order_status_id' => $data['order_status_id'],
            'payment' => $data['payment'],
            'discounts' => $data['discounts'],
            'total_products' => $data['total_products'],
            'total' => $data['total'],
            'total_paid' => $data['total_paid'],
            'total_shipping' => isset($data['total_shipping']) ? $data['total_shipping'] : 0,
            'tax' => $data['tax']
        ]);

        return $order;
    }
}
