@extends('admin.report.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('report.update', $data->id) }}" enctype="multipart/form-data">
                       {{csrf_field()}}
                       {{method_field('PATCH')}}

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Url</label>

                            <div class="col-md-6">
                                <input id="url" type="text" class="form-control" name="url" value="{{ $data->url }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Body</label>

                            <div class="col-md-6">
                                <textarea type="text" rows="4" class="form-control" name="body">{{ $data->body }}</textarea>
                            </div>
                        </div>

                    <div class="form-group">
                      <label class="col-md-4 control-label">Product Of User</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="user_id">
                                
                              <option value="{{$data->user_id}}">{{$data->user->name}}</option>
                              
                            </select>
                                <span class="text-danger">{{ $errors->first('user_id') }}</span>
                            </div>
                        </div>

                    <div class="form-group">
                      <label class="col-md-4 control-label">Product</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="product_id">
                                
                              <option value="{{$data->product_id}}">{{$data->product->name}}</option>
                             
                            </select>
                                <span class="text-danger">{{ $errors->first('product_id') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                      <label class="col-md-4 control-label">Reported By User</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="auth_id">
                                
                              <option value="{{$data->auth_id}}">{{$data->authuser->name}}</option>
                             
                            </select>
                                <span class="text-danger">{{ $errors->first('auth_id') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                      <label class="col-md-4 control-label">Report Name</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="reportname_id">
                                
                              <option value="{{$data->reportname_id}}">{{$data->reportname->name}}</option>
                             
                            </select>
                                <span class="text-danger">{{ $errors->first('reportname_id') }}</span>
                            </div>
                        </div>

          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                  <input name="status" type="checkbox" {{$data->status?"checked":""}}>
                      Status
                    </label>
                  </div>
              </div>
          </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
