<?php

namespace App\Contracts;

/**
 * Interface AddressContract
 * @package App\Contracts
 */
interface OrderStatusContract extends MyBaseContract
{
    public function findByName(string $name);
}
