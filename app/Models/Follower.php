<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Follower extends Model
{
     protected $fillable = [
        'leader_id',
        'followed_by',
        'status',
        'is_company'
    ];

    public function following(){
    	return $this->belongsTo(User::class,'leader_id');
    }

    public function follower(){
    	return $this->belongsTo(User::class,'followed_by');
    }

}
