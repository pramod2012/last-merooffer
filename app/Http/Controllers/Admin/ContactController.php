<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\ContactContract;
use App\Contracts\UserContract;
use App\Contracts\ProductContract;
use App\Http\Controllers\BaseController;

class ContactController extends BaseController
{
    private $page="admin.contact.";

    protected $contactRepo;
    protected $userRepo;
    protected $productRepo;

    public function __construct(
        ContactContract $contactRepo,
        ProductContract $productRepo,
        UserContract $userRepo
    )
    {
        $this->contactRepo = $contactRepo;
        $this->productRepo = $productRepo;
        $this->userRepo = $userRepo;
    }

    public function index()
    {
        $list = $this->contactRepo->list('created_at', 'desc');
        $datas = $this->contactRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('Contact', 'List of Contacts');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $users = $this->userRepo->list();
        $products = $this->productRepo->list();
        $this->setPageTitle('Contacts', 'Create Contact');
        return view($this->page.'create',compact('users', 'products'));
    }

    public function store(Request $request)
    {
        $data = $this->contactRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('contact.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $users = $this->userRepo->list();
        $products = $this->productRepo->list();
        $data = $this->contactRepo->findById($id);
        $this->setPageTitle('Contacts', 'Edit Contact');
        return view($this->page.'edit', compact('data','users','products'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->contactRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('contact.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->contactRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('contact.index', 'Record deleted successfully' ,'success',false, false);
    }
}
