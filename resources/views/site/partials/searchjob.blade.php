{{-- <section class="search-section search-section-v5" style="background: #39444c;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form data-toggle="validator" role="form" class="search-form search-form-v5 form-inline"
                    action="{{route('search-job')}}" method="get">
                    <!--Select Category-->
                    <div class="form-group clearfix">
                        <div class="input-group side-by-side-input inner-addon right-addon pull-left flip">
                            <i class="form-icon form-icon-size-small fas fa-sort"></i>
                            <select class="form-control form-control-sm" name="category_id" id="ajaxSelectCat">
                                <option value="" selected disabled>All Job Categories</option>
                                @foreach(\App\JobCategory::all() as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="side-by-side-input pull-left flip classieraAjaxInput">
                            <input type="text" name="title" class="form-control form-control-sm"
                                placeholder="Enter keyword..." data-error="Please Type some words..!">
                            <div class="help-block with-errors"></div>

                        </div>
                    </div>
                    <!--Select Category-->
                    <!--Locations-->
                    <div class="form-group">
                        <div class="input-group inner-addon right-addon">
                            <div class="input-group-addon input-group-addon-width-sm"><i
                                    class="fas fa-map-marker-alt"></i></div>
                            <input type="text" id="getCity" name="post_location" class="form-control form-control-sm"
                                placeholder="Please type location">

                        </div>
                    </div>
                    <!--Locations-->
                    <!--PriceRange-->
                    <div class="form-group clearfix">
                        <div class="inner-addon right-addon">
                            <i class="form-icon form-icon-size-small fas fa-sort"></i>
                            <select class="form-control form-control-sm" data-placeholder="Select price range"
                                name="industry_id">
                                <option value="" selected disabled>Industry</option>
                                @foreach(\App\Industry::all() as $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <!--PriceRange-->
                    <div class="form-group">
                        <button class="radius" type="submit" name="search">Search Now</button>
                    </div>
                </form>
            </div>
            <!--col-md-12-->
        </div>
        <!--row-->
    </div>
    <!--container-->
</section>
<!--search-section--> --}}
<div id="nav-bar-search">
    <nav-bar-job-search />
</div>
