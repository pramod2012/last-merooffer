<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\McontactContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests\StoreMcontactRequest;

class McontactController extends BaseController
{
    private $page="admin.mcontact.";

    protected $mcontactRepo;

    public function __construct(
        McontactContract $mcontactRepo
    )
    {
        $this->mcontactRepo = $mcontactRepo;
    }

    public function index()
    {
        $list = $this->mcontactRepo->list('created_at', 'desc');
        $datas = $this->mcontactRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('mcontact', 'List of mcontacts');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('mcontact', 'Create mcontact');
        return view($this->page.'create');
    }

    public function store(StoreMcontactRequest $request)
    {
        $data = $this->mcontactRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('mcontact.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->mcontactRepo->findById($id);
        $this->setPageTitle('mcontact', 'Edit mcontact');
        return view($this->page.'edit', compact('data'));
    }

    public function update(StoreMcontactRequest $request, $id)
    {
        $data = $this->mcontactRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('mcontact.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->mcontactRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('mcontact.index', 'Record deleted successfully' ,'success',false, false);
    }
}
