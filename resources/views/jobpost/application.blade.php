@extends('layouts.app1')

@section('title')
    Application
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
       <div class="col-md-10 my-5">            
            @include('partials.alert')
            <div class="card card-default">  
                <div class="card-header">
                	<h4 class="h4 text-muted">JOB DETAILS</h4>
                </div>
                <div class="card-body pt-0 table-responsive py-3">
                	<div class="row">
	                   <div class="col-sm-8">
	                   	<h5 class="h5 text-info">{{ $job->name }}</h5>
	                   	<article>
	                   	<?php
$str = $job->body;
echo nl2br($str);
?></article>
	                   </div>
	                   <div class="col-sm-4 job-pecification">
	                   		<ul class="list-unstyled">
	                   			<li class="mb-2">
				        			<span class="text-success">
				        				<i class="fas fa-rupee-sign"></i> Salary : 
				        			</span>
				        			@if($job->salary==!null) NRs. {{ number_format($job->salary) }}@endif @if($job->salary_to == !null) - {{number_format($job->salary_to)}} @endif {{$job->salarytype->name}}
				        		</li>
				        		<li class="mb-2">
				        			<span class="text-success">
				        				<i class="fas fa-clock"></i> Posted: 
				        			</span>
				        			{{$job->created_at->diffForHumans()}}
				        		</li>
				        		<li class="mb-2">
				        			<span class="text-success">
				        				<i class="fas fa-briefcase"></i> Position : 
				        			</span>
				        			{{ucwords($job->positiontype->name)}}
				        		</li>
				        		<li class="mb-2">
				        			<span class="text-success">
				        				<i class="fas fa-hourglass-end"></i> Duration:  
				        			</span>
				        			{{ ucwords($job->endate) }}
				        		</li>
				        		<li class="mb-2">
				        			<span class="text-success">
				        				<i class="fas fa-tags"></i> Category: 
				        			</span>
				        			{{ ucwords($job->category->name) }}
				        		</li>
				        	</ul>
	                   </div>
                   </div>
                </div>
            </div>
            <div class="card card-default mt-5">  
            	<form action="{{url("/job/application/$job->id/store")}}" method="POST">
            		{{ csrf_field() }}
	                <div class="card-header">
	                	<h4 class="h4 text-muted">APPLICATION LETTER</h4>
	                </div>
	                <div class="card-body pt-0 table-responsive py-3">
	                	  <div class="form-group">
						    <label for="exampleTextarea">Tell something about yourself</label>
						    <textarea name="application_letter" class="form-control" id="exampleTextarea" rows="8"></textarea>
						  </div>
	                </div>
	                <input type="hidden" value="{{$job->id}}" name="job_id">
	                <div class="card-header">
	                	<button type="submit" class="btn btn-primary btn-lg px-5">SUBMIT</button>
	                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('jsplugins')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Readmore.js/2.2.0/readmore.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('article').readmore({
			  afterToggle: function(trigger, element, expanded) {
			    if(! expanded) { // The "Close" link was clicked
			      $('html, body').animate( { scrollTop: element.offset().top }, {duration: 100 } );			  
			    } 
			  }
			});
		});
	</script>
@endsection

