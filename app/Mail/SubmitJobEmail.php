<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Job;

class SubmitJobEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $job;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Job $job)
    {
        $this->job = $job;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->from('noreply@merooffer.com')
                    ->view('emails.submitJobEmail')
                    ->with([
                        'user' => $this->job->user->name,
                        'title' => $this->job->name,
                        'slug' => $this->job->slug,
                    ]);
    }
}
