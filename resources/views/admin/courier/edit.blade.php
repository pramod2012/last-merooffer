@extends('admin.courier.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form method="POST" action="{{ route('courier.update',$courier->id) }}" aria-label="category" enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{method_field('PATCH')}}
                        

                    <div class="form-group">
                        <label for="name">Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{{ $courier->name ?: old('name') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">Description </label>
                        <textarea name="description" id="description" rows="5" class="form-control" placeholder="Description">{{ $courier->description ?: old('description') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="URL">URL</label>
                        <div class="input-group">
                            <span class="input-group-addon">url</span>
                            <input type="text" name="url" id="url" placeholder="Link" class="form-control" value="{{ $courier->url ?: old('url') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="is_free">Free Delivery? </label>
                        <select name="is_free" id="is_free" class="form-control">
                            <option value="0" @if($courier->is_free == 0) selected="selected" @endif>No</option>
                            <option value="1" @if($courier->is_free == 1) selected="selected" @endif>Yes</option>
                        </select>
                    </div>
                    <div class="form-group" @if($courier->is_free == 1) style="display: none" @endif id="delivery_cost">
                        <label for="cost">Delivery Cost <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <span class="input-group-addon">{{config('cart.currency')}}</span>
                            <input class="form-control" type="text" id="cost" name="cost" placeholder="{{config('cart.currency')}}" value="{{$courier->cost}}">
                        </div>
                    </div>
                    <div class="form-group">
                        @include('admin.shared.status-select', ['status' => $courier->status])
                    </div>
                
                    
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
