<?php

namespace App\Repo;

use App\Models\Follower;
use App\Contracts\FollowerContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubmitFollowerEmail;
use App\Mail\SubmitFollowerEmailReceipient;
use App\Mail\SubmitCompanyFollowerEmail;
use App\Mail\SubmitCompanyFollowerEmailReceipient;

/**
 * Class FollowerRepository
 *
 * @package \App\Repo
 */
class FollowerRepo extends BaseRepo implements FollowerContract
{
    /**
     * FollowerRepository constructor.
     * @param Follower $model
     */
    public function __construct(Follower $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function store($request)
    {
        try {
            $params = $request->all();

            $collection = collect($params);

            $status = $collection->has('status') ? 1 : 0;
            $is_company = $collection->has('is_company') ? 1 : 0;

            $merge = $collection->merge(compact('status','is_company'));

            $data = new Follower($merge->all());

            $data->save();

            return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
    	try
    	{

        $data = $this->findById($id);

        $data->status = $request->status;
        $data->is_company = $request->is_company;
        $data->leader_id = $request->leader_id;
        $data->followed_by = $request->followed_by;
        $data->save();

        if($request->status == '1' && $request->is_company == '0') {
        Mail::to($data->follower->email)->send(new SubmitFollowerEmail($data));
        Mail::to($data->following->email)->send(new SubmitFollowerEmailReceipient($data));
        }
        if($request->status == '1' && $request->is_company == '1') {
        Mail::to($data->follower->email)->send(new SubmitCompanyFollowerEmail($data));
        Mail::to($data->following->email)->send(new SubmitCompanyFollowerEmailReceipient($data));
        }

        return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);
        $data->delete();
        return $data;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $data = Follower::where('slug', $slug)->first();
        return $data;
    }

    public function myFollower()
    {
        return Follower::where('is_company',0)
                ->where('leader_id', auth()->user()->id)
                ->with('follower')
                ->get();
    }

    public function myFollowing()
    {
        return Follower::where('is_company',0)
                ->where('followed_by', auth()->user()->id)
                ->with('following')
                ->get();
    }

}
