@extends('admin.user.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                     @include('admin.partials.flash')
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('user.store') }}" enctype="multipart/form-data">
                        
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="firstname" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input id="name" value="{{old('name')}}" type="text" class="form-control" name="name">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">E-Mail Address*</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('email') }}
                                        </strong>
                                    </span>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Password*</label>

                            <div class="col-md-6">
                                <input id="password" type="text" class="form-control" name="password" value="">

                                @if ($errors->has('password'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('password') }}
                                        </strong>
                                    </span>
                                    @endif
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="image" class="col-md-4 control-label">Image</label>
                            <div class="col-md-6">
                                <input type="file" name="image" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cpassword" class="col-md-4 control-label">Confirm Password*</label>

                            <div class="col-md-6">
                                <input id="cpassword" type="text" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}">
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('password_confirmation') }}
                                        </strong>
                                    </span>
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="mobile" class="col-md-4 control-label">Mobile Number*</label>

                            <div class="col-md-6">
                                <input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}">
                            
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('mobile') }}
                                        </strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="role" class="col-md-4 control-label">Phone Number</label>
                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="role" class="col-md-4 control-label">Address</label>
                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}">
                            </div>
                        </div>
                        <?php use Carbon\Carbon; ?>

                        <div class="form-group">
                            <label for="role" class="col-md-4 control-label">Email Verified</label>
                            <div class="col-md-6">
                                <input id="email_verified_at" type="text" class="form-control" name="email_verified_at" value="{{ old('email_verified_at',Carbon::now()) }}">
                            </div>
                        </div>
        
          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" value="0" name="cell_activated">
                      <input name="cell_activated" type="checkbox" value="1">
                      Cell Activated
                    </label>
                  </div>
              </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="show_mobile" value="0">
                      <input name="show_mobile" type="checkbox" value="1">
                      Show Mobile
                    </label>
                  </div>
              </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="show_email" value="0">
                      <input name="show_email" type="checkbox" value="1">
                      Show Email
                    </label>
                  </div>
              </div>
          </div><div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="show_phone" value="0">
                      <input name="show_phone" type="checkbox" value="1">
                      Show Phone
                    </label>
                  </div>
              </div>
          </div>
                        <p><strong> * Required field  </strong> </p>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
