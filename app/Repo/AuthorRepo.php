<?php

namespace App\Repo;

use App\Models\Author;
use App\Traits\UploadAble;
use Illuminate\Http\UploadedFile;
use App\Contracts\AuthorContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

/**
 * Class AuthorRepository
 *
 * @package \App\Repositories
 */
class AuthorRepo extends BaseRepo implements AuthorContract
{
    use UploadAble;

    /**
     * AuthorRepository constructor.
     * @param Author $model
     */
    public function __construct(Author $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Author|mixed
     */
    public function store($request)
    {
        try {
            $params = $request->all();

            $collection = collect($params);

            $image = null;

            if ($collection->has('image') && ($params['image'] instanceof  UploadedFile)) {
                $image = $this->uploadOne($params['image'], 'authors');
            }
            $status = $collection->has('status') ? 1 : 0;

            $merge = $collection->merge(compact('status', 'image'));

            $Author = new Author($merge->all());

            $Author->save();

            return $Author;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
        $params = $request->all();

        $author = $this->findById($id);

        $collection = collect($params);

        $image = null;

        if ($collection->has('image') && ($params['image'] instanceof  UploadedFile)) {

            if ($author->image != null) {
                $this->deleteOne($Author->image);
            }

            $image = $this->uploadOne($params['image'], 'authors');
        }
        $status = $collection->has('status') ? 1 : 0;

        $merge = $collection->merge(compact('image','status'));

        $author->update($merge->all());

        return $author;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);

        if ($data->image != null) {
            $this->deleteOne($data->image);
        }

        $data->delete();

        return $data;
    }

    
}
