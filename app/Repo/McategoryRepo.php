<?php

namespace App\Repo;

use App\Models\Mcategory;
use App\Traits\UploadAble;
use Illuminate\Http\UploadedFile;
use App\Contracts\McategoryContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

/**
 * Class McategoryRepository
 *
 * @package \App\Repositories
 */
class McategoryRepo extends BaseRepo implements McategoryContract
{
    use UploadAble;

    /**
     * McategoryRepository constructor.
     * @param Mcategory $model
     */
    public function __construct(Mcategory $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Mcategory|mixed
     */
    public function store($request)
    {
        try {
            $params = $request->all();

            $collection = collect($params);

            $status = $collection->has('status') ? 1 : 0;

            $merge = $collection->merge(compact('status'));

            $Mcategory = new Mcategory($merge->all());

            $Mcategory->save();

            return $Mcategory;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
        $params = $request->all();

        $Mcategory = $this->findById($id);

        $collection = collect($params);

        $status = $collection->has('status') ? 1 : 0;

        $merge = $collection->merge(compact('status'));

        $Mcategory->update($merge->all());

        return $Mcategory;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);

        $data->delete();

        return $data;
    }

    /**
     * @return mixed
     */
    public function treeList()
    {
        return Mcategory::orderByRaw('-name ASC')
            ->get()
            ->nest()
            ->setIndent('|–– ')
            ->listsFlattened('name');
    }

    public function findBySlug($slug)
    {
        return Mcategory::where('slug', $slug)
            ->where('status', 1)
            ->firstOrFail();
    }

    public function saveOrders($request){
        $allArticles = $request->get('datas');
        foreach($allArticles as $_article){
            $article = Mcategory::find($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }

    
}
