@extends('admin.author.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          <h3 class="box-title">List of Authors</h3>
        </div>
        <div class="col-sm-4">
          @include('admin.partials.flash')
          <a class="btn btn-primary" href="{{route('author.create')}}">Add new Author</a>
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>
      <form method="POST" action="#">
         
      </form>
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                <th width="1%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending"> ID</th>
                <th width="1%" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Picture: activate to sort column descending" aria-sort="ascending">Picture</th>
                <th width="8%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Address: activate to sort column ascending">Name</th>
                <th width="2%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending">Username</th>
                
                <th width="8%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Birthdate: activate to sort column ascending">Email</th>
                
                <th width="1%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Division: activate to sort column ascending">Status</th>
                <th width="8%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Division: activate to sort column ascending">Created At</th>
                <th width="8%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Division: activate to sort column ascending">Updated At</th>
                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="2" aria-label="Action: activate to sort column ascending">Action</th>
              </tr>
            </thead>
            <tbody>
             @foreach($authors as $value)
                <tr role="row" class="odd">
                  <td class="sorting_1">{{$value->id}}</td>
                  <td>
                    <img src="{{ asset('storage/'.$value->image) }}" width="50px" height="50px"/>
                  </td>
                  <td class="hidden-xs">{{$value->name}}</td>
                  <td class="hidden-xs">{{$value->username}}</td>
                  <td class="hidden-xs">{{$value->email}}</td>
                  
                  <td class="hidden-xs">
                      <i class="{{$value->status == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                    </td>
                  <td class="hidden-xs">{{$value->created_at}}</td>
                  <td class="hidden-xs">{{$value->updated_at}}</td>
                  <td>
                    <form class="row" method="POST" action="{{ route('author.destroy', $value->id) }}" onsubmit = "return confirm('Are you sure?')">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <a href="{{ route('author.edit', $value->id) }}" class="btn btn-warning col-sm-3 col-xs-5 btn-margin">
                        <i class="fa fa-pencil-square-o large"></i>
                        </a>
                         <button type="submit" class="btn btn-danger col-sm-3 col-xs-5 btn-margin">
                          <i class="fa fa-trash large"></i>
                        </button>
                    </form>
                  </td>
              </tr>
                @endforeach         
            </tbody>
            
          </table>
        </div>
      </div>
      {{$authors->links()}}
    </div>
  </div>
  
</div>
    </section>
    
  </div>
@endsection