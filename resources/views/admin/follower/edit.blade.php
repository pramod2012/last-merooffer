@extends('admin.follower.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Follower</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('follower.update',$data->id) }}" enctype="multipart/form-data">
                       {{csrf_field()}}
                       {{method_field('PATCH')}}
                        <div class="form-group">
                      <label class="col-md-4 control-label">Followed To</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="leader_id">
                                
                              <option value="{{$data->leader_id}}">{{$data->following->name}}</option>
                             
                            </select>
                                <span class="text-danger">{{ $errors->first('leader_id') }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                      <label class="col-md-4 control-label">Followed By</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="followed_by">
                                
                              <option value="{{$data->followed_by}}">{{$data->follower->name}}</option>
                             
                            </select>
                                <span class="text-danger">{{ $errors->first('followed_by') }}</span>
                            </div>
                        </div>
        <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="status" value="0">
                      <input name="status" value="1" type="checkbox" {{$data->status?"checked":""}}>
                      Status 
                    </label>
                  </div>
              </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input type="hidden" name="is_company" value="0">
                      <input name="is_company" value="1" type="checkbox" {{$data->is_company?"checked":""}}>
                      Is Company 
                    </label>
                  </div>
              </div>
          </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                 Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
