<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class applicant_job extends Model
{
    protected $fillable =[
    	'applicant_user_id', 'job_id'
    ];
}
