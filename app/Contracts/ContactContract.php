<?php

namespace App\Contracts;

/**
 * Interface ContactContract
 * @package App\Contracts
 */
interface ContactContract extends MyBaseContract
{
	public function myContactEmails();
}
