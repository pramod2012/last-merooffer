@extends('site.partials.main_index')
@section('title','Current Open Jobs')
@section('content')
<body data-rsssl=1 class="page-template page-template-template-user-all-ads page-template-template-user-all-ads-php page page-id-38 logged-in">
  @include('site.partials.navbar')
  <!-- user pages -->
<section class="user-pages section-gray-bg">
  <div class="container">
        <div class="row">
      <div class="col-lg-3 col-md-4">
  @include('site.partials.sidebar')
     </div><!--col-lg-3-->
      <div class="col-lg-9 col-md-8 user-content-height">
        <div class="user-detail-section section-bg-white">
          <!-- my ads -->
          <div class="user-ads user-my-ads">
            @include('partials.alert')
            <h4 class="user-detail-section-heading text-uppercase">
            CURRENT OPEN JOBS             </h4>
            
            @forelse($jobs as $job)
              <div class="media border-bottom">
              <div class="media-left">
                @if (@$job->user->firmuser->logo)
                    <img class="media-object" src="{{ asset('images_small/'.$job->user->firmuser->logo) }}" alt="name">
                    @else
                    <img class="media-object" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="name">
                    @endif
                                            </div><!--media-left-->
              <div class="media-body">
                <h5 class="media-heading">
                  <a href="{{ route('job_post',$job->slug) }}" target="_blank">
                   {{$job->name}}                 </a>
                </h5>
                <p>
                  <span class="published">
                    <i class="{{$job->status == 1?"fa fa-check-circle":"fa fa-times-circle"}}"></i>
                  Published</span>
                                    <span>
                                        <i class="far fa-eye"></i>
                                        {{$job->views}}                                    </span>
                                    <span>
                                        <i class="far fa-clock"></i>                                     
                    {{$job->created_at->format('F j, Y')}}</span>
                  <span>
                    <i class="removeMargin fas fa-hashtag"></i>
                    ID : {{$job->id}}</span>
                                </p>
              </div><!--media-body-->
              <div class="classiera_posts_btns">
                <!--PayPerPostbtn-->
                                <!--PayPerPostbtn-->
                <!--BumpAds-->
                                <!--BumpAds-->
                    <a href="{{route('job-user.edit',$job->id)}}" class="btn btn-primary sharp btn-style-one btn-sm"><i class="icon-left far fa-edit"></i>Edit</a>
                    <form method="POST" action="{{ route('job-user.destroy',$job->id) }}" onsubmit="return confirm('Are you sure?')">
                      {{csrf_field()}}
                      {{method_field('DELETE')}}
                    <button class="thickbox btn btn-primary sharp btn-style-one btn-sm" type="submit" value="submit"><i class="icon-left fas fa-trash-alt"></i>Delete</button></form>
                    <a href="/shortlist/{{$job->slug}}" class="thickbox btn btn-primary sharp btn-style-one btn-sm">Shortlist</a>
                
                <!--Restore Button for Expired Ads-->
                                <!--Restore Button for Expired Ads-->
              </div><!--classiera_posts_btns-->
            </div><!--media border-bottom-->
            @empty
            <div  class="media border-bottom">You haven't posted any jobs! </div>
              @endforelse
            {{$jobs->links()}}            
          </div><!--user-ads user-my-ads-->
          <!-- my ads -->
        </div><!--user-detail-section-->
      </div><!--col-lg-9-->
    </div><!--row-->
  </div><!-- container-->
</section>
@include('site.partials.footer')
@endsection