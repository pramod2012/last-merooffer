<?php

namespace App\Shop\Products\Transformations;

use App\Models\Product;
use Illuminate\Support\Facades\Storage;

trait ProductTransformable
{
    /**
     * Transform the product
     *
     * @param Product $product
     * @return Product
     */
    protected function transformProduct(Product $product)
    {
        $prod = new Product;
        $prod->id = (int) $product->id;
        $prod->name = $product->name;
        $prod->slug = $product->slug;
        $prod->desc = $product->desc;
        $prod->quantity = $product->quantity;
        $prod->price = $product->price;
        $prod->status = $product->status;

        return $prod;
    }
}
