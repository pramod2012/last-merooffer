@extends('site.partials.main_index')
@section('title','User Resumes')
@section('content')
<body class="page-template page-template-template-favorite page-template-template-favorite-php page page-id-22 logged-in">
@include('site.partials.navbar')
<section class="user-pages section-gray-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-4">
      @include('site.partials.sidebar')     
</div><!--col-lg-3-->
      <div class="col-lg-9 col-md-8 user-content-height">
        <div class="user-detail-section section-bg-white">
          <!-- favorite ads -->
          <div class="user-ads favorite-ads">
            <h4 class="user-detail-section-heading text-uppercase">
            User Resumes            </h4>
                                                <!--singlepost-->
                                                @forelse($resumes as $value)
            <div class="media border-bottom">
                            <tr>
                            	
                            	<td>{{$value->message}}</td>
                            	<td>---- <a href="{{asset('storage/resumes/'.$value->cv)}}">Download Resume</a></td><br>
                            	<td>{{$value->name}}</td>
                            </tr>
                        </div><!--media border-bottom-->
            <!--singlepost-->
            @empty
            <div  class="media border-bottom">No Resumes Sent to you! </div>
              @endforelse         
                      </div><!--user-ads-->
          
          <!-- favorite ads -->
        </div><!--user-detail-->
      </div><!--col-lg-9-->
    </div><!--row-->
  </div><!--container-->
</section><!--user-pages-->
<!-- Company Section Start-->
<!-- Company Section End--> 
@include('site.partials.footer')
@endsection