@extends('site.partials.main_index')
@section('title')
Welcome {{$user->name}}
@endsection
@section('content')

<body class="page-template page-template-template-profile page-template-template-profile-php page page-id-32 logged-in">
    @include('site.partials.navbar')
    <!-- user pages -->
    <section class="user-pages section-gray-bg" id="app">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    @include('site.partials.sidebar')
                </div>
                <!--col-lg-3-->
                <div class="col-lg-9 col-md-8 user-content-height">
                    <div class="user-detail-section section-bg-white">
                        <!-- about me -->
                        @include('partials.alert')
                        <div class="about-me">
                            <h4 class="user-detail-section-heading text-uppercase">About Me</h4>
                            <ul class="list-unstyled">
                                <a>{{$user->name}}</a><br>
                                </li>
                            </ul>
                        </div>
                        <!-- about me -->
                        <!-- contact details -->
                        <div class="user-contact-details">
                            <h4 class="user-detail-section-heading text-uppercase">
                                Contact Details
                            </h4>
                            <ul class="list-unstyled">
                                <i class="fas fa-envelope"></i>
                                <a href="mailto:{{$user->email}}">{{$user->email}}</a><br>
                                @if($user->phone==!null)
                                <i class="fas fa-phone"></i>
                                <a> {{$user->phone}}</a><br>@endif
                                @if($user->mobile==!null)
                                <i class="fas fa-mobile"></i>
                                <a> {{$user->mobile}}</a><br>@endif
                                </li>
                            </ul>
                        </div>
                        <!-- contact details -->
                        <!-- social profile -->
                        <div class="user-social-profile-links">
                            <h4 class="user-detail-section-heading text-uppercase">
                                Mobile Phone Verification </h4>
                            <ul class="list-unstyled list-inline">
                                @if(Auth::user()->cell_activated==0)
                                You have not verified your mobile phone number ({{Auth::user()->mobile}}) yet. A SMS
                                will be sent to your mobile phone number to verify your mobile phone number.
                                <verify-mobile mobile="{{Auth::user()->mobile}}" />
                                {{-- <b>Please update your profile by <a href="{{url('/edit-profile')}}"><u>clicking
                                            here</u> </a> and fill up your required details. Your activation key is {{
                                    Auth::user()->activation_key }} .<br> Please send us this activation key from your
                                    mobile number to +977-9849551992 to verify your mobile number.<br> --}}

                                    @endif
                                    @if(Auth::user()->cell_activated==1)
                                    <b>Your mobile phone is activated. Now you are a verified user. Your mobile number
                                        is {{ Auth::user()->mobile }} .</b>
                                    @endif

                            </ul>
                        </div>
                        @if( \App\Address::where('user_id',auth()->user()->id)->count()>0)
                        <div class="user-social-profile-links">
                            <h4 class="user-detail-section-heading text-uppercase">
                                <i class="fa fa-home"></i> Address
                            </h4>


                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped">
                                        <thead>
                                            <th>Alias</th>
                                            <th>Address</th>
                                            <th>Contact</th>
                                            <th>Action</th>
                                        </thead>
                                        <tbody>
                                            @foreach(\App\Models\Address::where('user_id',auth()->user()->id)->get() as
                                            $key => $address)
                                            <tr>
                                                <td>{{ $address->alias }}</td>
                                                <td>
                                                    {{ $address->address_1 }} {{ $address->address_2 }} <br />
                                                    @if(!is_null($address->province))
                                                    {{ $address->city }} {{ $address->province }} <br />
                                                    @endif
                                                    {{ $address->city }} {{ $address->state_code }} <br>
                                                    {{ $address->country }} {{ $address->zip }}
                                                </td>
                                                <td>
                                                    {{$address->phone}}
                                                </td>
                                                <td>
                                                    <form method="POST"
                                                        action="{{route('addresses.destroy',$address->id)}}">
                                                        @csrf
                                                        {{method_field('DELETE')}}
                                                        <button onclick="return confirm('Are you sure?')"
                                                            class="fa fa-trash"></button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>
                        @endif

                        <div class="user-view-all text-center">
                            <a href="{{url('edit-profile')}}" class="btn btn-primary btn-md btn-style-one sharp">
                                Edit Profile </a>
                            <a href="{{url('addresses')}}" class="btn btn-primary btn-md btn-style-one sharp">
                                Create Address </a>
                        </div>

                    </div>
                </div>
            </div>
            <!--row-->
        </div>
        <!--container-->
    </section>
    <!-- user pages -->
    @include('site.partials.footer')
    @endsection
