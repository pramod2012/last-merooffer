<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Product;

class SubmitAdEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $product;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@merooffer.com','Mero Offer')
                    ->view('emails.submitAdEmail')
                    ->with([
                        'name' => $this->product->user->name,
                        'title' => $this->product->name,
                        'slug' => $this->product->slug,
                    ]);
    }
}
