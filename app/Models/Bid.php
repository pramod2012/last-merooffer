<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Bid;
use App\Models\Product;

class Bid extends Model
{
    protected $fillable = [
        'body',
        'bid',
        'url',
        'user_id',
        'bidable_id',
        'bidable_type',
        'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'bidable_id');
    }


    public function commentable(){
    return $this->morphTo();
    }

    public function replies()
    {
        return $this->hasMany(Bid::class,'parent_id');
    }
}
