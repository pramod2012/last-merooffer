<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\CategoryContract;
use App\Contracts\PricetypeContract;
use App\Contracts\DayContract;
use App\Contracts\ProductContract;
use App\Contracts\UserContract;
use App\Contracts\CityContract;
use App\Contracts\AdtypeContract;
use App\Contracts\AttributeContract;
use App\Contracts\AttributevalueContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests\AdminProductRequest;
use App\Models\Category;

class ProductController extends BaseController
{
    protected $categoryRepo;
    protected $productRepo;
    protected $pricetypeRepo;
    protected $dayRepo;
    protected $userRepo;
    protected $cityRepo;
    protected $adtypeRepo;
    protected $attributevalueRepo;

    public function __construct(
        CategoryContract $categoryRepo,
        ProductContract $productRepo,
        PricetypeContract $pricetypeRepo,
        DayContract $dayRepo,
        UserContract $userRepo,
        CityContract $cityRepo,
        AdtypeContract $adtypeRepo,
        AttributevalueContract $attributevalueRepo
    )
    {
        $this->categoryRepo = $categoryRepo;
        $this->productRepo = $productRepo;
        $this->pricetypeRepo = $pricetypeRepo;
        $this->dayRepo = $dayRepo;
        $this->userRepo = $userRepo;
        $this->cityRepo = $cityRepo;
        $this->adtypeRepo = $adtypeRepo;
        $this->attributevalueRepo = $attributevalueRepo;
    }

    public function index()
    {
        $list = $this->productRepo->list('order', 'desc');
        $products = $this->productRepo->paginateArrayResults($list->all(), 20);

        $this->setPageTitle('Products', 'Products List');
        return view('admin.product.index', compact('products'));
    }

    public function create()
    {
        $categories = $this->categoryRepo->list('name', 'asc');
        $users = $this->userRepo->list();
        $city = $this->cityRepo->list();
        $adtype = $this->adtypeRepo->list();
        $pricetypes = $this->pricetypeRepo->list();
        $days = $this->dayRepo->list();

        $this->setPageTitle('Products', 'Create Product');
        return view('admin.product.create', compact('categories','users','city','adtype','pricetypes','days'));
    }

    public function store(AdminProductRequest $request)
    {
        $product = $this->productRepo->store($request);

        if (!$product) {
            return $this->responseRedirectBack('Error occurred while creating product.', 'error', true, true);
        }
        return $this->responseRedirect('product.index', 'Product added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $product = $this->productRepo->findById($id);

        $productAttributes = $product->productattributes()->get();

        $qty = $productAttributes->map(function ($item) {
            return $item->quantity;
        })->sum();

        $categories = $this->categoryRepo->list('name', 'asc');

        foreach($product->categories->where('parent_id',!null) as $cat){
            $cate = $cat->id;
        }
        $category = Category::findOrFail($cate);

        $pricetypes = $this->pricetypeRepo->list();
        $adtypes = $this->adtypeRepo->list();
        $days = $this->dayRepo->list();
        $users = $this->userRepo->list();
        $cities = $this->cityRepo->list();

        $this->setPageTitle('Products', 'Edit Product');
        return view('admin.product.edit', compact('categories','category', 'product','pricetypes', 'adtypes', 'days', 'cities', 'users','productAttributes'));
    }

    public function update(AdminProductRequest $request, $id)
    {
        $product = $this->productRepo->edit($request, $id);

        if (!$product) {
            return $this->responseRedirectBack('Error occurred while updating product.', 'error', true, true);
        }
        return $this->responseRedirect('product.index', 'Product updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->productRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('product.index', 'Record deleted successfully' ,'success',false, false);
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('products');
        foreach($allArticles as $_article){
            $article = $this->productRepo->findById($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
