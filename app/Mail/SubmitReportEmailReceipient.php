<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Report;

class SubmitReportEmailReceipient extends Mailable
{
    use Queueable, SerializesModels;
    protected $report;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Report $report)
    {
        $this->report = $report;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@merooffer.com')
                    ->subject('Submit Report Email')
                    ->view('emails.submitReportEmailReceipient')
                    ->with([
                        'body' => $this->report->body,
                        'report_name' => $this->report->reportname->name,
                        'reported_by' => $this->report->authuser->name,
                        'reported_by_email' => $this->report->authuser->email,
                        'product_of_email' => $this->report->user->email,
                        'product_of_user' => $this->report->user->name,
                        'url' => $this->report->url,
                    ]);
    }
}
