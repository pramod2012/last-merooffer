<?php

namespace App\Models;
use App\Models\Mcategory;
use App\Models\Author;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
       'name',
        'summary',
        'desc',
        'keyword',
        'body',
        'date',
        'status',
        'feature',
        'slug',
        'mcategory_id',
        'is_blog',
        'author_id',
        'image',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->order = $model->max('order') + 1;
        });
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        if (static::whereSlug($slug = str_slug($value))->exists())
        {
            $slug = $this->incrementSlug($slug);
        }

        $this->attributes['slug'] = $slug;
    }

    public function incrementSlug($slug)
    {
        $original = $slug;
        $count = 2;
        while (static::whereSlug($slug)->exists()) 
        {
           $slug = $count++ . "-{$original}";
        }
        return $slug;
    }

    public function mcategory(){
        return $this->belongsTo(Mcategory::class,'mcategory_id');
    }

     public function author(){
        return $this->belongsTo(Author::class,'author_id');
    }

    public function tags(){
        return $this->belongsToMany(Tag::class,'post_tags');
    }
}
