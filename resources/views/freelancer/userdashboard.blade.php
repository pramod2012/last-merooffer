@extends('Frontend.includes.main_index')
@section('content')
<body class="archive category category-uncategorized category-1 logged-in">
    @include('includes.navbarjob')
    @include('includes.searchjob')
    <!--search-section--><!-- page content -->
<section class="inner-page-content border-bottom top-pad-50">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-9">
                <!-- advertisement -->
<section class="classiera-advertisement advertisement-v5 section-pad-80 border-bottom">
    <div class="tab-divs">
        <div class="view-head">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-sm-9 col-xs-10">
                        <div class="tab-button">
                            <ul class="nav nav-tabs" role="tablist">                                
                                <li role="presentation" @if(Request::get('salary') != 'asc' && Request::get('salary') != 'desc' && Request::get('views') != 'desc' ) class="active" @endif>
                                    <a href="{{route('jobs')}}" aria-controls="all">
                                        Recent Jobs
                                        <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation" @if(Request::get('salary') == 'asc') 
                                    class="active" @endif>                                    
                                    <a href="{{ route('jobs', ['salary' =>'asc']) }}" aria-controls="random">
                                        Salary ASC                                      <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation" @if(Request::get('salary') == 'desc') 
                                    class="active" @endif>                                    
                                    <a href="{{ route('jobs', ['salary' =>'desc']) }}" aria-controls="random">
                                        Salary DESC                                      <span class="arrow-down"></span>
                                    </a>
                                </li>
                                <li role="presentation" @if(Request::get('views') == 'desc') 
                                    class="active" @endif>                                   
                                    <a href="{{ route('jobs', ['views' =>'desc']) }}" aria-controls="popular">
                                       Popular Jobs                                    <span class="arrow-down"></span>
                                    </a>
                                </li>
                            </ul><!--nav nav-tabs-->
                        </div><!--tab-button-->
                    </div><!--col-lg-6 col-sm-8-->
                    <div class="col-lg-4 col-sm-3 col-xs-2">
                        <div class="view-as text-right flip">
                            <a id="grid" class="grid active" href="#"><i class="fas fa-th"></i></a>
                            <a id="list" class="list " href="#"><i class="fas fa-th-list"></i></a>                          
                        </div><!--view-as tab-button-->
                    </div><!--col-lg-6 col-sm-4 col-xs-12-->
                </div><!--row-->
            </div><!--container-->
        </div><!--view-head-->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="all">
                <div class="container products">
                    <div id="load" class="row">
                   <!--FeaturedPosts-->@foreach ($jobs as $job)
                        <div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
    <div class="classiera-box-div classiera-box-div-v5">
        <figure class="clearfix">
            <div class="premium-img">
                                <div class="featured-tag">
                        <span class="left-corner"></span>
                        <span class="right-corner"></span>
                        <div class="featured">
                            <p>Featured</p>
                        </div>
                    </div>
                        <img class="img-responsive" src="{{@asset('images/'. $job->user->image)}}" alt="{{$job->title}}">
                                    <span class="hover-posts">                                          
                    <a href="{{ route('job_post', ['id' =>$job->id, 'title'=>str_slug($job->title)]) }}">View Job</a>
                </span>
                                    <span class="price">
                        Salary: NRs. {{$job->salary}}               </span>
                                                <span class="classiera-buy-sel">
                {{ ucwords($job->positiontype->name) }}              </span>
                            </div><!--premium-img-->
            <div class="detail text-center">
                                <span class="price">
                    Salary: NRs. {{$job->salary}}               </span>
                                <div class="box-icon">
                    <a href="mailto:{{$job->user->email}}?subject">
                        <i class="fas fa-envelope"></i>
                    </a>
                                        <a href="tel:{{$job->user->p}}"><i class="fas fa-phone"></i></a>
                                    </div>
                <a href="{{ route('job_post', ['id' =>$job->id, 'title'=>str_slug($job->title)]) }}" class="btn btn-primary outline btn-style-five">View Job</a>
            </div><!--detail text-center-->
            <figcaption>
                                <span class="price visible-xs">
                Salary: NRs. {{$job->salary}}           </span>
                                <h5><a href="{{ route('job_post', ['id' =>$job->id, 'title'=>str_slug($job->title)]) }}">{{$job->title}}</a></h5>
                <div class="category">
                    <span>
                    Category : 
                    <a href="#" title="View all posts in {{$job->category->category_name}}">{{ ucwords($job->category->category_name) }}</a>                 </span>
                                        <span>Location : 
                        <a href="#">{{$job->job_location}}</a>
                    </span>
                </div>
                <p class="description">{!! $job->body !!}</p>
            </figcaption>
        </figure>
    </div><!--row-->
</div><!--col-lg-4-->
@endforeach
<br>
</div><!--row-->
<div class="classiera-pagination">
    <nav aria-label="Page navigation">
        <div id="pagination">
       {!!$jobs->links()!!}
        </div>
    </nav>
</div><!--tab-divs-->
</section><!-- advertisement -->
            </div><!--col-md-8-->
            <div class="col-md-4 col-lg-3">
                <aside class="sidebar">
                    <div class="row">
                        <!--subcategory-->
                        <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                            <div class="widget-box">
                                <div class="widget-title">
                                    <h4>
                                        <i class="zmdi zmdi-male-female" style="color:#c29bc2;"></i>
                                        Categories                                 </h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="category">

                                        @foreach($job_categories as $value)
                                        <li>
                                            <a href="{{ url('userdashboard') }}?cat={{$value->id}}">
                                                <i class="fas fa-angle-right"></i>
                                               {{$value->category_name}}                                               <span class="pull-right flip">
                                                            #
                                                    </span>
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                                                <!--subcategory-->
                                                    <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                                <div class="widget-box">
                                <!--SearchForm-->
<form method="get" action="https://www.nepsal.com">
    <div class="search-form border">
        <div class="search-form-main-heading">
            <a href="#innerSearch" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="innerSearch">
                <i class="fas fa-sync-alt"></i>
                Advanced Search         </a>
        </div><!--search-form-main-heading-->
        <div id="innerSearch" class="collapse in classiera__inner">
            <!--Price Range-->
                        <div class="inner-search-box">
                <h5 class="inner-search-heading">
                    <span class="currency__symbol">
                    Rs. 
                    </span>
                Price Range             </h5>
                                                        <div class="radio">
                        <!--PriceFirst-->
                        <input id="price-range-1" type="radio" value="0,10000" name="price_range">
                        <label for="price-range-1">
                            0&nbsp;&ndash;&nbsp;
                            10000                       </label>
                        <!--PriceSecond-->
                        <input id="price-range-2" type="radio" value="10000,20000" name="price_range">
                        <label for="price-range-2">
                            10001&nbsp;&ndash;&nbsp;
                            20000                       </label>
                        <!--PriceThird-->
                        <input id="price-range-3" type="radio" value="20000,30000" name="price_range">
                        <label for="price-range-3">
                            20001&nbsp;&ndash;&nbsp;
                            30000                       </label>
                        <!--PriceFourth-->
                        <input id="price-range-4" type="radio" value="30000,40000" name="price_range">
                        <label for="price-range-4">
                            30001&nbsp;&ndash;&nbsp;
                            40000                       </label>
                        <!--PriceFive-->
                        <input id="price-range-5" type="radio" value="40000,50000" name="price_range">
                        <label for="price-range-5">
                            40001&nbsp;&ndash;&nbsp;
                            50000                       </label>
                        <!--PriceSix-->
                        <input id="price-range-6" type="radio" value="50000,60000" name="price_range">
                        <label for="price-range-6">
                            50001&nbsp;&ndash;&nbsp;
                            60000                       </label>
                        <!--PriceSeven-->
                        <input id="price-range-7" type="radio" value="60000,70000" name="price_range">
                        <label for="price-range-7">
                            60001&nbsp;&ndash;&nbsp;
                            70000                       </label>
                        <!--PriceEight-->
                        <input id="price-range-8" type="radio" value="70000,80000" name="price_range">
                        <label for="price-range-8">
                            70001&nbsp;&ndash;&nbsp;
                            80000                       </label>
                        <!--PriceNine-->
                        <input id="price-range-9" type="radio" value="80000,90000" name="price_range">
                        <label for="price-range-9">
                            80001&nbsp;&ndash;&nbsp;
                            90000                       </label>
                        <!--Max Price-->
                         <input id="price-range-10" type="radio" value="90000,100000" name="price_range">
                        <label for="price-range-10">
                            90001&nbsp;&ndash;&nbsp;
                            100000                      </label>
                    </div><!--radio-->
                    <div class="classiera_price_slider">
                        <p>
                          <label for="amount">Price range:</label>
                          <input data-cursign="Rs." type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
                        </p>                     
                        <div id="slider-range"></div>
                        <input type="hidden" id="classieraMaxPrice" value="100000">
                        <input type="hidden" id="range-first-val" name="search_min_price" value="">
                        <input type="hidden" id="range-second-val" name="search_max_price" value="">
                    </div>                  
                    
                            </div>
                        <!--Price Range-->
            <div class="inner-search-box">
                <h5 class="inner-search-heading"><i class="fas fa-tag"></i>
                Keywords                </h5>
                <div class="inner-addon right-addon">
                    <i class="right-addon form-icon fas fa-search"></i>
                    <input type="search" name="s" class="form-control form-control-sm" placeholder="Enter Keyword">
                </div>
            </div><!--Keywords-->
            <!--Locations-->
                        <!--Locations-->
            <!--Categories-->
            <div class="inner-search-box">
                <h5 class="inner-search-heading"><i class="far fa-folder-open"></i>
                Categories              </h5>

                <!--SelectCategory-->
                <div class="inner-addon right-addon">
                    <i class="right-addon form-icon fas fa-sort"></i>
                    <select  name='category_name' id='main_cat' class='form-control form-control-sm' >
    <option value='-1' selected='selected'>Select category</option>
    <option class="level-0" value="uncategorized">Art &amp; Craft</option>
    <option class="level-0" value="automotive">Automobiles</option>
    <option class="level-0" value="electronics">Electronics</option>
    <option class="level-0" value="furniture">Furniture</option>
    <option class="level-0" value="jobs">Jobs</option>
    <option class="level-0" value="fasion-life-style">Lifestyle &amp; Fashion</option>
    <option class="level-0" value="pets-animals">Pets &amp; Animals</option>
    <option class="level-0" value="real-estates">Real Estates</option>
    <option class="level-0" value="restaurants-cafe">Restaurants &amp; Cafe</option>
    <option class="level-0" value="services">Services</option>
    <option class="level-0" value="books-sports-hobbies">Sports, Books &amp; Hobbies</option>
    <option class="level-0" value="travel-tourism">Travel &amp; tourism</option>
</select>
                </div>
                <!--Select Sub Category-->
                <div class="inner-addon right-addon classiera_adv_subcat">
                    <i class="right-addon form-icon fas fa-sort"></i>
                    <select name="sub_cat" class="form-control form-control-sm" id="sub_cat" disabled="disabled">
                    </select>
                </div>
                <!--CustomFields-->
                                            <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-cars">
                                    <option value="">Make Year...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-cars">
                                    <option value="">Kilometres...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-cars">
                                    <option value="">Colour...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-cars">
                                    <option value="">Engine(CC)...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-cars">
                                    <option value="">Used For( Year or Month)...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-cars">
                                    <option value="">Brand...</option>
                                                                        <option value="Nissan">Nissan</option><option value=" Maruti Suzuki"> Maruti Suzuki</option><option value=" Mahindra"> Mahindra</option><option value="Mazda">Mazda</option><option value=" Chevrolet"> Chevrolet</option><option value=" Daihatsu"> Daihatsu</option><option value=" Datsun"> Datsun</option><option value=" Fiat"> Fiat</option><option value=" Ford"> Ford</option><option value=" Geely"> Geely</option><option value=" Honda"> Honda</option><option value=" Hyundai"> Hyundai</option><option value=" Kia"> Kia</option><option value=" Land Rover"> Land Rover</option><option value=" Toyota"> Toyota</option><option value=" Tata"> Tata</option><option value="Ssangyong">Ssangyong</option><option value="Skoda">Skoda</option><option value="Renault">Renault</option><option value="Perodua">Perodua</option><option value="Volkswagen">Volkswagen</option><option value=" Mitsubishi"> Mitsubishi</option><option value="Other Brands">Other Brands</option><option value="Other Chinese Brands">Other Chinese Brands</option>                                </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-cars">
                                    <option value="">Type...</option>
                                                                        <option value="Small Hatchback">Small Hatchback</option><option value=" Mid Size Hatchback"> Mid Size Hatchback</option><option value=" Sedan"> Sedan</option><option value=" CUV / Compact SUV"> CUV / Compact SUV</option><option value=" Jeep / SUV"> Jeep / SUV</option><option value=" Pickup"> Pickup</option><option value=" Others"> Others</option>                                </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-cars">
                                    <option value="">Fuel...</option>
                                                                        <option value="Petrol">Petrol</option><option value=" Diesel"> Diesel</option><option value=" Electric"> Electric</option>                              </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-cars">
                                    <option value="">Transmission...</option>
                                                                        <option value="Manual Gear - 2WD">Manual Gear - 2WD</option><option value=" Manual Gear - 4WD"> Manual Gear - 4WD</option><option value=" Automatic Gear - 2WD"> Automatic Gear - 2WD</option><option value=" Automatic Gear - 4WD"> Automatic Gear - 4WD</option>                              </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="58" name="custom_fields[]" value="Power Window">
                                    <label for="58">Power Window</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="59" name="custom_fields[]" value="Leather Seat">
                                    <label for="59">Leather Seat</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="510" name="custom_fields[]" value="Central Lock">
                                    <label for="510">Central Lock</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="511" name="custom_fields[]" value="Sunroof ">
                                    <label for="511">Sunroof </label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="512" name="custom_fields[]" value="Power Steering">
                                    <label for="512">Power Steering</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="513" name="custom_fields[]" value="Alloy Wheels">
                                    <label for="513">Alloy Wheels</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="514" name="custom_fields[]" value="Air Bags">
                                    <label for="514">Air Bags</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="515" name="custom_fields[]" value="Tubeless Tyres">
                                    <label for="515">Tubeless Tyres</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="516" name="custom_fields[]" value="Keyless Remote Entry">
                                    <label for="516">Keyless Remote Entry</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="517" name="custom_fields[]" value="Anti-theft Alarm">
                                    <label for="517">Anti-theft Alarm</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="518" name="custom_fields[]" value="Anti-lock Braking (ABS)">
                                    <label for="518">Anti-lock Braking (ABS)</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="519" name="custom_fields[]" value="Projected Headlight">
                                    <label for="519">Projected Headlight</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="520" name="custom_fields[]" value="Steering Mounted Controls">
                                    <label for="520">Steering Mounted Controls</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="521" name="custom_fields[]" value="Climate Control">
                                    <label for="521">Climate Control</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="522" name="custom_fields[]" value="Air Conditioning">
                                    <label for="522">Air Conditioning</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="523" name="custom_fields[]" value="LCD Touchscreen">
                                    <label for="523">LCD Touchscreen</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="524" name="custom_fields[]" value="Bluetooth Connectivity">
                                    <label for="524">Bluetooth Connectivity</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="525" name="custom_fields[]" value="Electric ORVM">
                                    <label for="525">Electric ORVM</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="526" name="custom_fields[]" value="Fog Lights">
                                    <label for="526">Fog Lights</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-cars" style="display: none;">
                                <div class="checkbox custom-field-cat-cars">
                                    <input type="checkbox" id="527" name="custom_fields[]" value="Audio System">
                                    <label for="527">Audio System</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-commercial-property" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-commercial-property">
                                    <option value="">...</option>
                                                                        <option value=""></option>                              </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-flat-apartment">
                                    <option value="">Built up (sq.ft)...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-flat-apartment">
                                    <option value="">Floors...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-flat-apartment">
                                    <option value="">Bedrooms...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-flat-apartment">
                                    <option value="">Bathroom...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-flat-apartment">
                                    <option value="">Living room...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-flat-apartment">
                                    <option value="">Land Size (in aana/dhur)...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-flat-apartment">
                                    <option value="">Property Type...</option>
                                                                        <option value="Individual">Individual</option><option value=" Colony"> Colony</option><option value=" Semi-commercial"> Semi-commercial</option><option value=" Bungalow"> Bungalow</option><option value=" Apartment Building"> Apartment Building</option><option value=" Commercial Building"> Commercial Building</option><option value=" Others"> Others</option>                              </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-flat-apartment">
                                    <option value="">Access Road...</option>
                                                                        <option value="Less than 5 feet">Less than 5 feet</option><option value=" 5 to 8 feet"> 5 to 8 feet</option><option value=" 9 to 12 feet"> 9 to 12 feet</option><option value=" 13 to 20 feet"> 13 to 20 feet</option><option value=" Above 20 feet"> Above 20 feet</option><option value=" Goreto Bato"> Goreto Bato</option><option value=" No access Road"> No access Road</option>                              </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <div class="checkbox custom-field-cat-flat-apartment">
                                    <input type="checkbox" id="1268" name="custom_fields[]" value="Water Supply">
                                    <label for="1268">Water Supply</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <div class="checkbox custom-field-cat-flat-apartment">
                                    <input type="checkbox" id="1269" name="custom_fields[]" value="Parking Space">
                                    <label for="1269">Parking Space</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <div class="checkbox custom-field-cat-flat-apartment">
                                    <input type="checkbox" id="12610" name="custom_fields[]" value="Servant Quarter">
                                    <label for="12610">Servant Quarter</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <div class="checkbox custom-field-cat-flat-apartment">
                                    <input type="checkbox" id="12611" name="custom_fields[]" value="Garage">
                                    <label for="12611">Garage</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <div class="checkbox custom-field-cat-flat-apartment">
                                    <input type="checkbox" id="12612" name="custom_fields[]" value="Garden">
                                    <label for="12612">Garden</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <div class="checkbox custom-field-cat-flat-apartment">
                                    <input type="checkbox" id="12613" name="custom_fields[]" value="Backup Generator">
                                    <label for="12613">Backup Generator</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <div class="checkbox custom-field-cat-flat-apartment">
                                    <input type="checkbox" id="12614" name="custom_fields[]" value="Security Guards">
                                    <label for="12614">Security Guards</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <div class="checkbox custom-field-cat-flat-apartment">
                                    <input type="checkbox" id="12615" name="custom_fields[]" value="Servant Quarter">
                                    <label for="12615">Servant Quarter</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <div class="checkbox custom-field-cat-flat-apartment">
                                    <input type="checkbox" id="12616" name="custom_fields[]" value="Elevator ">
                                    <label for="12616">Elevator </label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <div class="checkbox custom-field-cat-flat-apartment">
                                    <input type="checkbox" id="12617" name="custom_fields[]" value="Gymnasium">
                                    <label for="12617">Gymnasium</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-flat-apartment" style="display: none;">
                                <div class="checkbox custom-field-cat-flat-apartment">
                                    <input type="checkbox" id="12618" name="custom_fields[]" value="Swimming Pool">
                                    <label for="12618">Swimming Pool</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-house">
                                    <option value="">Floors...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-house">
                                    <option value="">Built up (sq.ft)...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-house">
                                    <option value="">Bedrooms...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-house">
                                    <option value="">Bathroom...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-house">
                                    <option value="">Living room...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-house">
                                    <option value="">Land Size (in aana/dhur)...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-house">
                                    <option value="">Property Type...</option>
                                        <option value="Individual">Individual</option><option value=" Colony"> Colony</option><option value=" Semi-commercial"> Semi-commercial</option><option value=" Bungalow"> Bungalow</option><option value=" Apartment Building"> Apartment Building</option><option value=" Commercial Building"> Commercial Building</option><option value=" Others"> Others</option>                              </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-house">
                                    <option value="">Access Road...</option>
                                        <option value="Less than 5 feet">Less than 5 feet</option><option value=" 5 to 8 feet"> 5 to 8 feet</option><option value=" 9 to 12 feet"> 9 to 12 feet</option><option value=" 13 to 20 feet"> 13 to 20 feet</option><option value=" Above 20 feet"> Above 20 feet</option><option value=" Goreto Bato"> Goreto Bato</option><option value=" No access Road"> No access Road</option>                              </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <div class="checkbox custom-field-cat-house">
                                    <input type="checkbox" id="1217" name="custom_fields[]" value="Water Supply">
                                    <label for="1217">Water Supply</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <div class="checkbox custom-field-cat-house">
                                    <input type="checkbox" id="1219" name="custom_fields[]" value="Garden">
                                    <label for="1219">Garden</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <div class="checkbox custom-field-cat-house">
                                    <input type="checkbox" id="12110" name="custom_fields[]" value="Parking Space">
                                    <label for="12110">Parking Space</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <div class="checkbox custom-field-cat-house">
                                    <input type="checkbox" id="12111" name="custom_fields[]" value="Garage">
                                    <label for="12111">Garage</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <div class="checkbox custom-field-cat-house">
                                    <input type="checkbox" id="12112" name="custom_fields[]" value="Servant Quarter">
                                    <label for="12112">Servant Quarter</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <div class="checkbox custom-field-cat-house">
                                    <input type="checkbox" id="12113" name="custom_fields[]" value="Security Guards">
                                    <label for="12113">Security Guards</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <div class="checkbox custom-field-cat-house">
                                    <input type="checkbox" id="12114" name="custom_fields[]" value="Elevator">
                                    <label for="12114">Elevator</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <div class="checkbox custom-field-cat-house">
                                    <input type="checkbox" id="12115" name="custom_fields[]" value="Swimming Pool">
                                    <label for="12115">Swimming Pool</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-house" style="display: none;">
                                <div class="checkbox custom-field-cat-house">
                                    <input type="checkbox" id="12116" name="custom_fields[]" value="Gymnasium">
                                    <label for="12116">Gymnasium</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-land" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-land">
                                    <option value="">Land Size (in aana/dhur)...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-land" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-land">
                                    <option value="">Built up (sq.ft)...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-land" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-land">
                                    <option value="">Used For (year or month)...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-land" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-land">
                                    <option value="">Property Type...</option>
                                        <option value="Individual">Individual</option><option value=" Plotted"> Plotted</option><option value=" Commercial Use"> Commercial Use</option><option value=" Agriculture"> Agriculture</option><option value=" Godown/Tahara"> Godown/Tahara</option><option value=" Others"> Others</option>                                </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-land" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-land">
                                    <option value="">Access Road...</option>
                                                                        <option value="Less than 5 feet">Less than 5 feet</option><option value=" 5 to 8 feet"> 5 to 8 feet</option><option value=" 9 to 12 feet"> 9 to 12 feet</option><option value=" 13 to 20 feet"> 13 to 20 feet</option><option value=" Above 20 feet"> Above 20 feet</option><option value=" Goreto Bato"> Goreto Bato</option><option value=" No access Road"> No access Road</option>                              </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-land" style="display: none;">
                                <div class="checkbox custom-field-cat-land">
                                    <input type="checkbox" id="1224" name="custom_fields[]" value="Water Supply">
                                    <label for="1224">Water Supply</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-mobile-tablets" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-mobile-tablets">
                                    <option value="">RAM...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-mobile-tablets" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-mobile-tablets">
                                    <option value="">ROM...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-mobile-tablets" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-mobile-tablets">
                                    <option value="">Brands...</option>
                                        <option value=" Apple iPhone"> Apple iPhone</option><option value="Blackberry">Blackberry</option><option value="Colors">Colors</option><option value="Gionee">Gionee</option><option value="HTC">HTC</option><option value="Huawei">Huawei</option><option value="Lava - Xolo">Lava - Xolo</option><option value="Lenovo">Lenovo</option><option value="LG">LG</option><option value="Mi (Xiaomi)">Mi (Xiaomi)</option><option value="Micromax">Micromax</option><option value="Motorola">Motorola</option><option value="Nokia - Microsoft">Nokia - Microsoft</option><option value="OnePlus">OnePlus</option><option value="Oppo">Oppo</option><option value="Other Brands">Other Brands</option><option value="Other Chinese Brands">Other Chinese Brands</option><option value=" Copy-Clone Phones"> Copy-Clone Phones</option><option value="Other Indian Brands">Other Indian Brands</option><option value="Samsung">Samsung</option><option value="Sony">Sony</option><option value=" Vivo "> Vivo </option>                                </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-mobile-tablets" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-mobile-tablets">
                                    <option value="">Home Delivery...</option>
                                                                        <option value="Yes">Yes</option><option value=" No"> No</option>                                </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-motorcycles">
                                    <option value="">Used For ( month or year )...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-motorcycles">
                                    <option value="">Lot Number...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-motorcycles">
                                    <option value="">Make Year...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-motorcycles">
                                    <option value="">Engine ( CC )...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-motorcycles">
                                    <option value="">Kilometres...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-motorcycles">
                                    <option value="">Mileage (km / l)...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-motorcycles">
                                    <option value="">Brand...</option>
                                        <option value="Hero Honda">Hero Honda</option><option value=" Honda"> Honda</option><option value=" Bajaj"> Bajaj</option><option value=" Apollo - Rieju"> Apollo - Rieju</option><option value=" Royal Enfield"> Royal Enfield</option><option value=" Aprilia"> Aprilia</option><option value=" Benelli"> Benelli</option><option value=" Crossfire"> Crossfire</option><option value=" Hartford"> Hartford</option><option value=" KTM"> KTM</option><option value=" Mahindra"> Mahindra</option><option value=" Other Electric Bike"> Other Electric Bike</option><option value=" Other Motorcycle Brands"> Other Motorcycle Brands</option><option value=" Suzuki"> Suzuki</option><option value=" TVS"> TVS</option><option value=" Um"> Um</option><option value=" Vespa"> Vespa</option><option value=" Yamaha "> Yamaha </option>                              </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-motorcycles">
                                    <option value="">Type...</option>
                                        <option value="Standard">Standard</option><option value=" Cruiser"> Cruiser</option><option value=" Sports"> Sports</option><option value="Scooty">Scooty</option><option value=" Dirt"> Dirt</option>                              </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-motorcycles">
                                    <option value="">Anchal...</option>
                                        <option value="Mechi">Mechi</option><option value=" Koshi"> Koshi</option><option value=" Sagarmatha"> Sagarmatha</option><option value=" Janakpur"> Janakpur</option><option value=" Bagmati"> Bagmati</option><option value=" Narayani"> Narayani</option><option value=" Gandaki"> Gandaki</option><option value=" Lumbini"> Lumbini</option><option value=" Dhawalagi"> Dhawalagi</option><option value=" Rapti"> Rapti</option><option value=" Bheri"> Bheri</option><option value=" Seti"> Seti</option><option value=" Karnali"> Karnali</option><option value=" Mahakali"> Mahakali</option>                                </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <div class="checkbox custom-field-cat-motorcycles">
                                    <input type="checkbox" id="1199" name="custom_fields[]" value="LED Tail Light">
                                    <label for="1199">LED Tail Light</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <div class="checkbox custom-field-cat-motorcycles">
                                    <input type="checkbox" id="11910" name="custom_fields[]" value="Projected Headlight">
                                    <label for="11910">Projected Headlight</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <div class="checkbox custom-field-cat-motorcycles">
                                    <input type="checkbox" id="11911" name="custom_fields[]" value="Digital Display Panel">
                                    <label for="11911">Digital Display Panel</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <div class="checkbox custom-field-cat-motorcycles">
                                    <input type="checkbox" id="11912" name="custom_fields[]" value="Tubeless Tyres">
                                    <label for="11912">Tubeless Tyres</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <div class="checkbox custom-field-cat-motorcycles">
                                    <input type="checkbox" id="11913" name="custom_fields[]" value="Alloy Wheels">
                                    <label for="11913">Alloy Wheels</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <div class="checkbox custom-field-cat-motorcycles">
                                    <input type="checkbox" id="11914" name="custom_fields[]" value="Front Disc Brake">
                                    <label for="11914">Front Disc Brake</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <div class="checkbox custom-field-cat-motorcycles">
                                    <input type="checkbox" id="11915" name="custom_fields[]" value="Electric Start">
                                    <label for="11915">Electric Start</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <div class="checkbox custom-field-cat-motorcycles">
                                    <input type="checkbox" id="11916" name="custom_fields[]" value="Anti-lock Braking (ABS)">
                                    <label for="11916">Anti-lock Braking (ABS)</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <div class="checkbox custom-field-cat-motorcycles">
                                    <input type="checkbox" id="11917" name="custom_fields[]" value="Rear Disc Brake">
                                    <label for="11917">Rear Disc Brake</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <div class="checkbox custom-field-cat-motorcycles">
                                    <input type="checkbox" id="11918" name="custom_fields[]" value="Mono Suspension">
                                    <label for="11918">Mono Suspension</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <div class="checkbox custom-field-cat-motorcycles">
                                    <input type="checkbox" id="11919" name="custom_fields[]" value="Tripmeter">
                                    <label for="11919">Tripmeter</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <div class="checkbox custom-field-cat-motorcycles">
                                    <input type="checkbox" id="11920" name="custom_fields[]" value="Split Seat">
                                    <label for="11920">Split Seat</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-motorcycles" style="display: none;">
                                <div class="checkbox custom-field-cat-motorcycles">
                                    <input type="checkbox" id="11921" name="custom_fields[]" value="Low Fuel Indicator">
                                    <label for="11921">Low Fuel Indicator</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-office-space" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-office-space">
                                    <option value="">Floors...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-office-space" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-office-space">
                                    <option value="">Built up (sq.ft)...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-office-space" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-office-space">
                                    <option value="">Bedroom...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-office-space" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-office-space">
                                    <option value="">Bathroom...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-office-space" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-office-space">
                                    <option value="">Land Size (in aana/dhur)...</option>
                                                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-office-space" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-office-space">
                                    <option value="">Property Type...</option>
                                        <option value="Individual">Individual</option><option value=" Colony"> Colony</option><option value=" Semi-commercial"> Semi-commercial</option><option value=" Bungalow"> Bungalow</option><option value=" Apartment Building"> Apartment Building</option><option value=" Commercial Building"> Commercial Building</option><option value=" Others"> Others</option>
                                    </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-office-space" style="display: none;">
                                <i class="right-addon form-icon fas fa-sort"></i>
                                <select name="custom_fields[]" class="form-control form-control-sm autoHide hide-office-space">
                                    <option value="">Access Road...</option>
                                        <option value="Less than 5 feet">Less than 5 feet</option><option value=" 5 to 8 feet"> 5 to 8 feet</option><option value=" 9 to 12 feet"> 9 to 12 feet</option><option value=" 13 to 20 feet"> 13 to 20 feet</option><option value=" Above 20 feet"> Above 20 feet</option><option value=" Goreto Bato"> Goreto Bato</option><option value=" No access Road"> No access Road</option>                              </select>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-office-space" style="display: none;">
                                <div class="checkbox custom-field-cat-office-space">
                                    <input type="checkbox" id="1237" name="custom_fields[]" value="Parking Space">
                                    <label for="1237">Parking Space</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-office-space" style="display: none;">
                                <div class="checkbox custom-field-cat-office-space">
                                    <input type="checkbox" id="1238" name="custom_fields[]" value="Garden">
                                    <label for="1238">Garden</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-office-space" style="display: none;">
                                <div class="checkbox custom-field-cat-office-space">
                                    <input type="checkbox" id="1239" name="custom_fields[]" value="Garage">
                                    <label for="1239">Garage</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-office-space" style="display: none;">
                                <div class="checkbox custom-field-cat-office-space">
                                    <input type="checkbox" id="12310" name="custom_fields[]" value="Security Guards">
                                    <label for="12310">Security Guards</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-office-space" style="display: none;">
                                <div class="checkbox custom-field-cat-office-space">
                                    <input type="checkbox" id="12311" name="custom_fields[]" value="Swimming Pool">
                                    <label for="12311">Swimming Pool</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-office-space" style="display: none;">
                                <div class="checkbox custom-field-cat-office-space">
                                    <input type="checkbox" id="12312" name="custom_fields[]" value="Servant Quarter">
                                    <label for="12312">Servant Quarter</label>
                                </div>
                            </div>
                                                        <div class="inner-addon right-addon custom-field-cat custom-field-cat-office-space" style="display: none;">
                                <div class="checkbox custom-field-cat-office-space">
                                    <input type="checkbox" id="12313" name="custom_fields[]" value="Water Supply">
                                    <label for="12313">Water Supply</label>
                                </div>
                            </div>
                                            <!--CustomFields-->
                <!--Item Condition-->
                                <div class="inner-search-box-child">
                    <p>Select Condition</p>
                    <div class="radio">
                        <input id="use-all" type="radio" name="item-condition" value="All" checked>
                        <label for="use-all">All</label>
                        <input id="new" type="radio" name="item-condition" value="New">
                        <label for="new">New</label>
                        <input id="used" type="radio" name="item-condition" value="Used">
                        <label for="used">Used</label>
                    </div>
                </div>
                                <!--Item Condition-->
                <!--Select Ads Type-->
                                <div class="inner-search-box-child">
                    <p>Type of Ad</p>
                    <div class="radio">
                        <input id="type_all" type="radio" name="classiera_ads_type" value="All" checked>
                        <label for="type_all">All</label>
                                    <input id="sell" type="radio" name="classiera_ads_type" value="sell">
                            <label for="sell">For Sale</label>
                                    <input id="buy" type="radio" name="classiera_ads_type" value="buy">
                            <label for="buy">Wanted</label>
                                    <input id="rent" type="radio" name="classiera_ads_type" value="rent">
                            <label for="rent">For Rent</label>
                                    <input id="hire" type="radio" name="classiera_ads_type" value="hire">
                            <label for="hire">For hire</label>
                                    <input id="lostfound" type="radio" name="classiera_ads_type" value="lostfound">
                            <label for="lostfound">Lost &amp; Found</label>
                                    <input id="event" type="radio" name="classiera_ads_type" value="event">
                            <label for="event">Event</label>
                                    <input id="service" type="radio" name="classiera_ads_type" value="service">
                            <label for="service">Professional service</label>
                    </div>
                </div>
                                <!--Select Ads Type-->
            </div><!--inner-search-box-->
            <button type="submit" name="search" class="btn btn-primary sharp btn-sm btn-style-one btn-block" value="Search">Search</button>
        </div><!--innerSearch-->
    </div><!--search-form-->
</form>
<!--SearchForm-->
</div>
                            </div>        
                            </div><!--row-->
                </aside>
            </div><!--row-->
        </div><!--row-->
    </div><!--container-->
</section>  
<!-- page content -->

@include('Frontend.includes.footer')
@endsection
