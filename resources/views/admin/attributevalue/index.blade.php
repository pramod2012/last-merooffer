@extends('admin.attributevalue.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          <h3 class="box-title">{{$subTitle}}</h3><br>
        </div>
        <div class="col-sm-4"> 
    @include('admin.partials.flash')
          <a class="btn btn-primary" href="{{route('attributevalue.create')}}">Add new Attribute Value</a>
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>
      
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">ID</th>
                <th width="15%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Values</th>
               
                <th width="20%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Attribute Name</th>
                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="2" aria-label="Action: activate to sort column ascending">Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach($datas as $value)  
                <tr role="row" class="odd">
                  <td>{{$value->id}}</td>
                  <td>{{$value->value}}</td>
                  <td>{{$value->attribute->name}}</td>
                  <td> 
                        <a href="{{ route('attributevalue.edit',$value->id)}}" class="btn btn-warning col-sm-3 col-xs-5 btn-margin">
                        <i class="fa fa-pencil-square-o large"></i>
                        </a>
                    <form class="row" method="POST" action="{{ route('attributevalue.destroy',$value->id) }}" onsubmit = "return confirm('Are you sure?')">
                      {{csrf_field()}}
                      {{method_field('DELETE')}}
                                            <button type="submit" class="btn btn-danger col-sm-3 col-xs-5 btn-margin"><i class="fa fa-trash large"></i></button>
                                            
                                        </form>

                   
                  </td>
              </tr>
        @endforeach
            </tbody>
          </table>
          {{$datas->links()}}
        </div>
      </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
</div>
    </section>
    <!-- /.content -->
  </div>
@endsection