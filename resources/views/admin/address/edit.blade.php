@extends('admin.address.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form method="POST" action="{{ route('address.update',$data->id) }}" aria-label="category" enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{method_field('PATCH')}}
                        

                        <div class="form-group">
                        <label for="customer">Users </label>
                        <select name="user_id" id="status" class="form-control select2">
                            @foreach(\App\Models\User::all() as $customer)
                                @if($customer->id == $data->user->id)
                                    <option selected="selected" value="{{ $customer->id }}">{{ $customer->name }}</option>
                                @else
                                    <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="alias">Alias <span class="text-danger">*</span></label>
                        <input type="text" name="alias" id="alias" placeholder="Home or Office" class="form-control" value="{{ old('alias',$data->alias) }}">
                    </div>
                    <div class="form-group">
                        <label for="address_1">Address 1 <span class="text-danger">*</span></label>
                        <input type="text" name="address_1" id="address_1" placeholder="Address 1" class="form-control" value="{{ old('address_1',$data->address_1) }}">
                    </div>
                    <div class="form-group">
                        <label for="address_2">Address 2 </label>
                        <input type="text" name="address_2" id="address_2" placeholder="Address 2" class="form-control" value="{{ old('address_2',$data->address_2) }}">
                    </div>
                    <div class="form-group">
                        <label for="address_2">Country </label>
                        <input type="text" name="country" id="country" placeholder="Country" class="form-control" value="{{ old('country',$data->country) }}">
                    </div>

                    <div class="form-group">
                        <label for="province">Province </label>
                        <input type="text" name="province" id="province" placeholder="Province" class="form-control" value="{{ old('province',$data->province) }}">
                    </div>

                    <div class="form-group">
                        <label for="city">City </label>
                        <input type="text" name="city" id="city" placeholder="City" class="form-control" value="{{ old('city',$data->city) }}">
                    </div>
                    
                    
                    
                    <div class="form-group">
                        <label for="zip">Zip Code </label>
                        <input type="text" name="zip" id="zip" placeholder="Zip code" class="form-control" value="{{ old('zip',$data->zip) }}">
                    </div>

                    <div class="form-group">
                        <label for="zip">State Code </label>
                        <input type="text" name="state_code" id="state_code" placeholder="state_code" class="form-control" value="{{ old('state_code',$data->state_code) }}">
                    </div>
                    <div class="form-group">
                            <div class="col-md-12">
                                  <div class="checkbox">
                                    <label>
                                        <input name="status" type="checkbox" {{$data->status?"checked":""}}>
                                      Status<span class="text-danger">{{ $errors->first('status') }}</span>
                                    </label>
                                  </div>
                              </div>
               </div>
                
                    
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
