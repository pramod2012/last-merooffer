<?php

namespace App\Contracts;

/**
 * Interface JobcategoryContract
 * @package App\Contracts
 */
interface JobcategoryContract extends MyBaseContract
{
	public function findBySlug($slug);
}
