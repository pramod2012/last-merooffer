<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\SalarytypeContract;
use App\Http\Controllers\BaseController;

class SalarytypeController extends BaseController
{
    private $page="admin.salarytype.";

    protected $salarytypeRepo;

    public function __construct(
        SalarytypeContract $salarytypeRepo
    )
    {
        $this->salarytypeRepo = $salarytypeRepo;
    }

    public function index()
    {
        $list = $this->salarytypeRepo->list('created_at', 'desc');
        $salarytypes = $this->salarytypeRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('Salary Types', 'List of Salary Types');
        return view($this->page.'index',compact('salarytypes'));
    }

    public function create()
    {
        $this->setPageTitle('salarytype', 'Create salarytype');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->salarytypeRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('salarytype.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $salarytype = $this->salarytypeRepo->findById($id);
        $this->setPageTitle('Salary Type', 'Edit salarytype');
        return view($this->page.'edit', compact('salarytype'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->salarytypeRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('salarytype.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->salarytypeRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('salarytype.index', 'Record deleted successfully' ,'success',false, false);
    }
}
