<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Day;
use App\Models\Pricetype;
use App\Models\City;
use App\Models\Adtype;
use App\Models\Category;
use App\Models\Product;
use App\Models\Wishlist;
use App\Models\Contact;
use App\Job;
use App\Contracts\ProductContract;
use App\Contracts\ResumeContract;
use App\Contracts\UserContract;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Requests\StoreResumeRequest;
use App\Http\Requests\UpdateProfileUserRequest;


class AdminController extends Controller
{
    private $page = "site.admin.";

    protected $productRepo;
    protected $resumeRepo;
    protected $userRepo;

    public function __construct(
        ProductContract $productRepo,
        ResumeContract $resumeRepo,
        UserContract $userRepo
    ) {
        $this->productRepo = $productRepo;
        $this->resumeRepo = $resumeRepo;
        $this->userRepo = $userRepo;
    }

    public function editProfile()
    {
        return view($this->page . 'edit-profile');
    }

    public function profile()
    {
        $id = Auth()->user()->id;
        $user = User::find($id);
        return view($this->page . 'profile', compact("user"));
    }

    public function updateProfile(UpdateProfileUserRequest $request)
    {
        $id = Auth()->user()->id;
        $user = $this->userRepo->editUser($request, $id);

        return redirect()->intended('profile');
    }

    public function userAllAds()
    {
        $user = auth()->user()->id;
        $products = Product::orderBy('created_at', 'desc')->where('user_id', '=', $user)->paginate(4);
        return view($this->page . 'user-all-ads', compact('products'));
    }

    public function favouriteAds()
    {
        $user = auth()->user()->id;
        $data = Wishlist::orderBy('created_at', 'desc')->where('auth_id', '=', $user)->paginate(6);
        return view($this->page . 'favourite-ads', ['wishlists' => $data]);
    }

    public function follow()
    {
        return view($this->page . 'follow');
    }

    public function inbox()
    {
        $user_id = Auth()->user()->id;
        $contacts = Contact::where('user_id', $user_id)->get();
        return view($this->page . 'inbox', compact("contacts"));
    }

    public function adsList()
    {
        return view($this->page . 'ads-list');
    }

    public function submitAd(
        // $slug
    )
    {
        if (auth()->user()->cell_activated == 2) {
            return redirect()->route('edit-profile')->with('success', 'Please enter your mobile number and verify your mobile number to post ad ! Your mobile will be verified within 1 hour or call us in our number for immediate post !');
        } else {
            $days = Day::where('status', '1')->get();
            $city = City::get();
            $adtype = Adtype::get();
            // $category = Category::where('slug', $slug)->firstOrFail();
            return view($this->page . 'submit-ad', compact(
                // "category",
                "days",
                "city",
                "adtype"
            ));
        }
    }

    public function submitAdEdit($id)
    {
        $days = Day::where('status', '1')->get();
        $city = City::get();
        $adtype = Adtype::get();
        $product = Product::with(['attributevalues', 'images'])->where('user_id', auth()->user()->id)->findOrFail($id);
        foreach ($product->categories->where('parent_id', !null) as $category) {
            $category = $category->id;
        }
        $category = Category::findOrFail($category);
        return view($this->page . 'submit-ad-edit', compact('product', 'days', 'city', 'adtype', 'category'));
    }

    public function deleteJob($id)
    {
        $job = Job::findOrFail($id);
        $job->delete();
    }

    public function productStore(UpdateProductRequest $request)
    {
        $product = $this->productRepo->storeProduct($request);
        return redirect()->route('submit-ad-photo', $product->id)->with('success', 'Your Ad has been submitted and pending for review. After review your Ad will be live for all users. You can upload image in this step.');
    }

    public function updateProduct(UpdateProductRequest $request, $id)
    {
        $product = $this->productRepo->updateProduct($request, $id);
        return redirect()->route('submit-ad-photo', $product->id)->with('success', 'Your Ad has been submitted and pending for review. After review your Ad will be live for all users. You can upload image in this step.');
    }

    public function submitAdPhoto($id)
    {
        $product = Product::where('user_id', auth()->user()->id)->findOrFail($id);
        return view($this->page . 'submit-ad-photo', compact('product'));
    }

    public function deleteProduct($id)
    {
        $product = Product::where('user_id', auth()->user()->id)->findOrFail($id);
        $product = $this->productRepo->destroy($id);
        return redirect()->back();
    }

    public function createResume(StoreResumeRequest $request)
    {
        $resume = $this->resumeRepo->store($request);
        return back()->with('success', 'Your application has been send! You have successfully applied for the job');
    }

    public function userResumes()
    {
        $resumes = \App\Resume::where('user_id', auth()->user()->id)->get();
        return view($this->page . 'user-resumes', compact('resumes'));
    }

    public function sold_action(Request $request)
    {
        $id = $request->product_id;
        $result = \App\Models\Product::where('id', $id)
            ->update([
                'sold' => 1,
            ]);
        if ($result) {
            return redirect()->back()->with('success', 'Your product has been sold out');
        }
        return redirect()->back()->with('success', 'There was a problem');
    }

    public function orderAdmin()
    {
        $products = Product::orderBy('created_at', 'desc')->where('user_id', auth()->user()->id)->get();
        return view('ordered', compact('products'));
    }

    public function cancelOrder($id)
    {
        $order = \App\Models\Order::find($id);
        if ($order && $order->user_id == auth()->user()->id) {
            return view('cart.cancelOrder', compact('order'));
        } else {
            return back();
        }
    }

    public function cancelOrderRequest($id, Request $request)
    {
        $order = \App\Models\Order::find($id);
        if ($order && $order->user_id == auth()->user()->id) {
            \App\Models\Order::where('id', $id)->update(
                ['order_status_id' => 3, 'cancel_reason' => $request->cancel_reason,]

            );
            return redirect()->route('order');
        } else {
            return back()->with('success', 'Failed to cancel order');
        }
    }
    public function getFormFieldsForSubmittingAd(Request $request)
    {
        try {
            return response()->json(
                [
                    'status' => 1,
                    'message' => '',
                    'data' => ''
                ]
            );
        } catch (\Exception $e) {
            return response()->json([
                'status' => 0,
                'message' => $e->getMessage(),
            ]);
        }
    }
}
