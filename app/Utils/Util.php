<?php

namespace App\Utils;

class Util
{
    public static function successJson($data = [], $message = "Success")
    {
        return response()->json([
            'status' => 1,
            'message' => $message,
            'data' => $data
        ]);
    }
}
