<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Contracts\ResumeContract;

class ResumeController extends BaseController
{
    private $page="admin.resume.";

    protected $resumeRepo;

    public function __construct(
        ResumeContract $resumeRepo
    )
    {
        $this->resumeRepo = $resumeRepo;
    }

    public function index()
    {
        $list = $this->resumeRepo->list('created_at', 'desc');
        $datas = $this->resumeRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('Resume', 'List of Resumes');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $jobs = \App\Job::all();
        $this->setPageTitle('resume', 'Create resume');
        return view($this->page.'create',compact('jobs'));
    }

    public function store(Request $request)
    {
        $data = $this->resumeRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('resume.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $jobs = \App\Job::all();
        $data = $this->resumeRepo->findById($id);
        $this->setPageTitle('Resume', 'Edit resume');
        return view($this->page.'edit', compact('data','jobs'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->resumeRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('resume.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->resumeRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('resume.index', 'Record deleted successfully' ,'success',false, false);
    }
}
