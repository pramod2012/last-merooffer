<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $fillable =
    [
    	'from','greet','body','end_greet','status','subject'
    ];
}
