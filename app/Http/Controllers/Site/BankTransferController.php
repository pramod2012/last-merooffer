<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contracts\CartContract;
use App\Contracts\CourierContract;
use App\Contracts\ProductContract;
use App\Contracts\AddressContract;
use App\Repo\OrderStatusRepo;
use App\OrderStatus;
use App\Shop\Checkout\CheckoutRepository;
use App\Repo\OrderRepo;
use Ramsey\Uuid\Uuid;
use App\Models\Order;
use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;

class BankTransferController extends Controller
{
    private $cartRepo;
	private $courierRepo;
	private $productRepo;
	private $addressRepo;

	public function __construct(
        CartContract $cartRepo,
        CourierContract $courierRepo,
        ProductContract $productRepo,
        AddressContract $addressRepo
    )
    {
        $this->cartRepo = $cartRepo;
        $this->courierRepo = $courierRepo;
        $this->productRepo = $productRepo;
        $this->addressRepo = $addressRepo;
    }

    public function index(Request $request){
    	$products = $this->cartRepo->getCartItems();
        $customer = $request->user();
        $billingAddress = $request->input('billing_address');
        $courier = $this->courierRepo->findById(request()->session()->get('courierId', 1));
        $shippingFee = $this->cartRepo->getShippingFee($courier);

        return view('cart.bank-transfer', [
            'customer' => $customer,
            'billingAddress' => $billingAddress,
            'addresses' => $customer->addresses()->get(),
            'products' => $this->cartRepo->getCartItems(),
            'subtotal' => $this->cartRepo->getSubTotal(),
            'tax' => $this->cartRepo->getTax(),
            'shippingFee' => $shippingFee,
            'total' => $this->cartRepo->getTotal(2, $shippingFee),
            'cartItems' => $this->cartRepo->getCartItemsTransformed()
        ]);
    }

    public function store(Request $request)
    {
        $checkoutRepo = new CheckoutRepository;
        $orderStatusRepo = new OrderStatusRepo(new OrderStatus);
        $os = $orderStatusRepo->findByName('ordered');

        $courier = $this->courierRepo->findById(request()->session()->get('courierId', 1));
        $shippingFee = $this->cartRepo->getShippingFee($courier);

        $order = $checkoutRepo->buildCheckoutItems([
            'reference' => Uuid::uuid4()->toString(),
            'courier_id' => 1, // @deprecated
            'user_id' => $request->user()->id,
            'address_id' => $request->input('billing_address'),
            'order_status_id' => $os->id,
            'payment' => 'Cash By hand',
            'discounts' => 0,
            'total_products' => $this->cartRepo->getSubTotal(),
            'total' => $this->cartRepo->getTotal(2, $shippingFee),
            'total_shipping' => $shippingFee,
            'total_paid' => $this->cartRepo->getTotal(2, $shippingFee),
            'tax' => $this->cartRepo->getTax()
        ]);

        $orderRepo = new OrderRepo($order);

        Cart::destroy();

        return redirect('/order')->with('success', 'Order successful!');
    }
}
