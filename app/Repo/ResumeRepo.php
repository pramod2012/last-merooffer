<?php

namespace App\Repo;

use App\Resume;
use App\Traits\UploadAble;
use Illuminate\Http\UploadedFile;
use App\Contracts\ResumeContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubmitResumeEmail;
use App\Mail\SubmitResumeEmailReceipient;

/**
 * Class ResumeRepository
 *
 * @package \App\Repo
 */
class ResumeRepo extends BaseRepo implements ResumeContract
{
    use UploadAble;
    /**
     * ResumeRepository constructor.
     * @param Resume $model
     */
    public function __construct(Resume $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function store($request)
    {
        try {
            if ($request->hasFile('cv')){
                $resume = microtime() . '.' . $request->cv->getClientOriginalExtension();
                $request->cv->move(public_path('storage/resumes'), $resume);
                $data['cv'] = $resume;
            }
            $data['name'] = $request->name;
            $data['email'] = $request->email;
            $data['message'] = $request->message;
            $data['job_id'] = $request->job_id;
            $data['user_id'] = $request->user_id;
            $data = Resume::create($data);
            Mail::to($request->email)->send(new SubmitResumeEmail($data));
            Mail::to($data->job->user->email)->send(new SubmitResumeEmailReceipient($data));
            return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
    	try
    	{
           $data = $this->findById($id);
        if ($request->hasFile('cv')){
                $resume = microtime() . '.' . $request->cv->getClientOriginalExtension();
                $request->cv->move(public_path('storage/resumes'), $resume);
                $data['cv'] = $imageName;
            }
            $data['name'] = $request->name;
            $data['email'] = $request->email;
            $data['message'] = $request->message;
            $data['job_id'] = $request->job_id;
            $data['user_id'] = $request->user_id;
            $data['status'] = $request->status;
            $data->save();
        if($request->status == 1)
        {
            Mail::to($request->email)->send(new SubmitResumeEmail($data));
            Mail::to($data->job->user->email)->send(new SubmitResumeEmailReceipient($data));
        }

        return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);
        $data->delete();
        return $data;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $data = Resume::where('slug', $slug)->firstOrFail();
        return $data;
    }
}
