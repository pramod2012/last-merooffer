<?php

namespace App\Contracts;

/**
 * Interface FcategoryContract
 * @package App\Contracts
 */
interface FcategoryContract extends MyBaseContract
{
 	public function findBySlug($slug);
 	   
    public function listParent();

    public function treeList();
}
