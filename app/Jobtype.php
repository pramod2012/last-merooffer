<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobtype extends Model
{
    protected $table = 'jobtypes';

    protected $fillable = [
        'name','status','slug'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        if (static::whereSlug($slug = str_slug($value))->exists())
        {
            $slug = $this->incrementSlug($slug);
        }

        $this->attributes['slug'] = $slug;
    }

    public function incrementSlug($slug)
    {
        $original = $slug;
        $count = 2;
        while (static::whereSlug($slug)->exists()) 
        {
           $slug = $count++ . "-{$original}";
        }
        return $slug;
    }

    public function jobs() {
    	return $this->hasMany('App\Job');
   }
}
