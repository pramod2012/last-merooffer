<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Comment;

class SubmitCommentEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $comment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@merooffer.com')
                    ->view('emails.submitCommentEmail')
                    ->with([
                        'user' => $this->comment->user->name,
                        'body' => $this->comment->body,
                        'url' => $this->comment->url,
                    ]);
    }
}
