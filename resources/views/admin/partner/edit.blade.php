@extends('admin.partner.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Partner</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('partner.update', $data->id)}}" enctype="multipart/form-data">
             
                      {{csrf_field()}}
                      {{method_field('PATCH')}}
                        <div class="form-group">
                            <label for="name" class="col-md-2 control-label">Name *</label>

                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name', $data->name) }}">
                                <span class="text-danger">{{ $errors->first('name') }}</span>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="link" class="col-md-2 control-label">Link</label>

                            <div class="col-md-8">
                                <input id="link" type="text" class="form-control" name="link" value="{{ old('link', $data->link) }}">
                                <span class="text-danger">{{ $errors->first('link') }}</span>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="image" class="col-md-2 control-label">Image *</label>

                            <div class="col-md-8">
                                <input id="image" type="file" class="form-control" name="image"><br>
                                <span class="text-danger">{{ $errors->first('image') }}</span>
                                <img src="{{@asset('images_small/'.$data->image)}}" height="70">
                                <p> <b>Note: Please upload image with appropriate size.</b></p>

                            </div>
                        </div>

                    <div class="form-group">
                            <label for="firstname" class="col-md-2 control-label">Body *</label>

                            <div class="col-md-8">
                                <textarea rows="6" id="body" type="text" class="form-control" name="body">{{ old('body',$data->body) }}</textarea>
                                <span class="text-danger">{{ $errors->first('body') }}</span>

                            </div>
                        </div>



          <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input name="status" type="checkbox" {{$data->status?"checked":""}}>
                      <b>Status * </b>
                    </label>
                    <span class="text-danger">{{ $errors->first('status') }}</span>
                  </div>
              </div>
          </div>


                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
