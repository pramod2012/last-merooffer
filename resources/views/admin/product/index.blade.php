@extends('admin.product.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          <h3 class="box-title">{{$subTitle}}</h3>
        </div>
        <div class="col-sm-4">
          @include('admin.partials.flash')
          <a class="btn btn-primary" href="{{route('product.create')}}">Add new Product</a>
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>
      <form method="POST" action="#">
         
      </form>
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                <th width="2%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Birthdate: activate to sort column ascending">ID</th>

                <th width="5%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Image</th>
                <th width="10%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Title</th>
                <th width="3%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Views</th>
                <th width="5%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Feature</th>
                <th width="3%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Sold</th>
                <th width="3%" class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending">Status</th>
                <th width="8%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending">Category</th>
                <th width="5%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="HiredDate: activate to sort column ascending">Price</th>
                <th width="7%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Department: activate to sort column ascending">Days</th>
                <th width="2%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Birthdate: activate to sort column ascending">Order</th>
                <th width="8%" class="sorting hidden-xs" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Division: activate to sort column ascending">Expired AT</th>

                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="2" aria-label="Action: activate to sort column ascending">Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach($products as $value)
                <tr role="row" class="odd">
                  <td class="hidden-xs">{{ $value->id }}</td>
                  <td class="hidden-xs">
                    <a href="{{route('product_details',$value->slug)}}" target="_blank">
                    @if ($value->images->count() > 0)
                    <img src="{{ asset('images_small/'.$value->images->first()->full) }}" height="60" width="60">
                    @else
                    <img src="{{ asset('storage/'.config('settings.error_image')) }}" height="60" width="60">
                    @endif
                    </a>
                  </td>
                  
                  <td class="sorting_1">{{ $value->name }}</td>
                  <td>{{$value->views}}</td>
                  <td class="sorting_1">
                    <i class="{{$value->featured == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                  </td>
                  <td class="sorting_1">
                    <i class="{{$value->sold == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                  </td>
                  <td class="sorting_1">
                    <i class="{{$value->status == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                  </td>
                  <td class="hidden-xs">@foreach($value->categories as $cat)
                    <span class="badge badge-info">{{ $cat->name }}</span> @endforeach</td>
                  
                  <td class="hidden-xs">{{ $value->price }}</td>
                  <td class="hidden-xs">  
                    {{$value->day->name}}</td>
                    <td class="hidden-xs">  
                    {{$value->order}}</td>
                  <td class="hidden-xs">{{ $value->expired_at }}</td>
                  <td class="sorting_1">
                    <form method="POST" action="{{route('product.destroy', $value->id)}}" onsubmit = "return confirm('Are you sure?')">
                        {{ csrf_field() }}
                        {{method_field('DELETE')}}
                        <a href="{{ route('product.edit', $value->id) }}" class="btn btn-warning col-sm-3 col-xs-5 btn-margin">
                        <i class="fa fa-pencil-square-o large"></i>
                        </a>
                         <button type="submit" class="btn btn-danger col-sm-3 col-xs-5 btn-margin">
                          <i class="fa fa-trash large"></i>
                        </button>
                    </form>
                  </td>
              </tr>
             @endforeach
            </tbody>
        
          </table>
        </div>
      </div>
      {{$products->links()}}
      
    </div>
  </div>
  
</div>
    </section>
    
  </div>
@endsection