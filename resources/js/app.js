require("./bootstrap");
window.Vue = require("vue").default;
Vue.component(
    "sendMessageToSeller",
    require("./send-message-to-seller.vue").default
);
Vue.component(
    "reportProduct",
    require("./components/report_product.vue").default
);
Vue.component("bidStats", require("./components/bid-stats.vue").default);
Vue.component("errors", require("./components/errors.vue").default);
Vue.component("product-comment", require("./components/comments.vue").default);
Vue.component(
    "advance-search",
    require("./components/advance-search.vue").default
);
Vue.component(
    "advance-job-search",
    require("./components/advance-job-search.vue").default
);
Vue.component("range-slider", require("./components/range-slider.vue").default);
Vue.component(
    "search-product",
    require("./components/search-product.vue").default
);
Vue.component("search-job", require("./components/search-job.vue").default);
Vue.component(
    "paginated-products",
    require("./components/paginated-products.vue").default
);
Vue.component(
    "paginated-jobs",
    require("./components/paginated-jobs.vue").default
);
Vue.component("google-map", require("./components/google-map.vue").default);
Vue.component(
    "nav-bar-search",
    require("./components/nav-bar-search.vue").default
);
Vue.component("chat-head", require("./components/chat-head.vue").default);
Vue.component(
    "verify-mobile",
    require("./components/verify-mobile.vue").default
);
Vue.component(
    "add-seller-to-chat",
    require("./components/add-seller-to-chat.vue").default
);
Vue.component(
    "nav-bar-job-search",
    require("./components/nav-bar-job-search.vue").default
);
Vue.component(
    "ad-submit-form",
    require("./components/ad-submit-form.vue").default
);
import Vuex from "vuex";

Vue.use(Vuex);
const store = new Vuex.Store({
    state: {
        productCategories: [],
        productAttributes: [],
        productSearchParameters: {},
        tinyBadges: []
    },
    mutations: {
        setProductCategory(state, data) {
            state.productCategories = data;
        },
        setProductAttributes(state, data) {
            state.productAttributes = data;
        },
        setProductSearchParameters(state, data) {
            state.productSearchParameters = data;
        },
        appendProductSearchParameters(state, data) {
            state.productSearchParameters = {
                ...state.productSearchParameters,
                ...data
            };
        },
        setTinyBadges(state, data) {
            let index = state.tinyBadges.findIndex(t => {
                return t.key == data.key;
            });
            if (index == -1) {
                state.tinyBadges.push(data);
            } else {
                state.tinyBadges[index] = data;
            }
        },
        removeTinyBadge(state, data) {
            state.tinyBadges.splice(state.tinyBadges.indexOf(data), 1);
        }
    },
    getters: {
        doneTodos: state => {
            return Array.from(
                new Set(
                    Object.keys(state.productSearchParameters).map(k => {
                        if (state.productSearchParameters[k]) {
                            return state.tinyBadges.find(t => {
                                if (Array.isArray(t.code)) {
                                    // return t.code[0] == k || t.code[1] == k;
                                    return t.code.includes(k);
                                } else {
                                    return t.code == k;
                                }
                            });
                        }
                    })
                )
            );
        }
    }
});
import * as GmapVue from "gmap-vue";
import util from "./components/util";

Vue.use(GmapVue, {
    load: {
        key: util.GAPI,
        libraries: "places" // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)

        //// If you want to set the version, you can do so:
        // v: '3.26',
    },

    //// If you intend to programmatically custom event listener code
    //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
    //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
    //// you might need to turn this on.
    // autobindAllEvents: false,

    //// If you want to manually install components, e.g.
    //// import {GmapMarker} from 'gmap-vue/src/components/marker'
    //// Vue.component('GmapMarker', GmapMarker)
    //// then set installComponents to 'false'.
    //// If you want to automatically install all the components this property must be set to 'true':
    installComponents: true
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Notifications from "vue-notification";

/*
or for SSR:
import Notifications from 'vue-notification/dist/ssr.js'
*/

Vue.use(Notifications);
import vSelect from "vue-select";

Vue.component("v-select", vSelect);
import "vue-select/dist/vue-select.css";

import VueFileAgent from "vue-file-agent";
import VueFileAgentStyles from "vue-file-agent/dist/vue-file-agent.css";

Vue.use(VueFileAgent);

let ap = document.getElementById("app");
let mountingPoints = ["app", "nav-bar-search", "classiera_map"];
mountingPoints.forEach(m => {
    console.log(m);
    let point = document.getElementById(m);
    console.log(point);
    point
        ? new Vue({
              el: "#" + m,
              store
          })
        : null;
});
