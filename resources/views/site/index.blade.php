@extends('site.partials.main_index')
@section('title', config('settings.site_title'))
@section('meta-tags')
<meta name="description" content="Merooffer is Nepal's free online classifieds. It helps in selling, buying, trading, discussing, organizing, meeting people near you. " />
<meta name="keywords" content="merooffer.com, popular classified in Nepal, real estate buy and rent, next generation of free online classifieds" />
<meta name="author" content="merooffer.com" />
@endsection
@section('og-tags')
<meta property='og:title' content="{{config('settings.site_title' )}}" />
<meta property='og:image' content="{{asset('frontend/images/merooffer.jpg')}}" />
<meta property='og:description' content='merooffer.com, classified in Nepal, jobsite in Nepal, buy or sell used cars in nepal, job vacancy in Nepal, job site in Nepal recent job vacancies in Nepal.' />
<meta property='og:url' content="{{app('request')->url()}}">
<meta property=" og:type" content="website" />
<meta property="og:site_name" content="{{app('request')->url()}}" />
@endsection
@section('content')
<body class="home page-template page-template-template-homepage-v5 page-template-template-homepage-v5-php page page-id-6 logged-in">
@include('site.partials.navbar')
@include('site.partials.search')
<!--search-section-->
<section class="classiera-simple-bg-slider">
	<div class="classiera-simple-bg-slider-content text-center">
		<h1>Join the  <span class="color">buy local</span> revolution</h1>
		<h4>Browse ads by Popular ads Categories</h4>
		<div class="category-slider-small-box text-center">
			<ul class="list-inline list-unstyled">
				@foreach(\App\Models\Category::where('status',1)->where('featured',1)->limit(5)->get() as $value)
					<li>
						<a class="match-height" href="{{ route('menu', $value->slug) }}">
							<i class="{{$value->font}}"></i>
					        <p>{{$value->name}}</p>
						</a>
					</li>
	            @endforeach
	            <li>
	            	<a class="match-height" href="{{route('jobs')}}">
	            		<i class="zmdi zmdi-case"></i>
	            		<p>Jobs</p>
	            	</a>
	            </li>
	            <li>
	            	<a class="match-height" href="https://social.merooffer.com">
	            		<i class="zmdi zmdi-globe"></i>
	            		<p>Social Cause Community</p>
	            	</a>
	            </li>
			</ul><!--list-inline list-unstyled-->
		</div><!--category-slider-small-box-->
	</div><!--classiera-simple-bg-slider-content-->
</section><!--classiera-simple-bg-slider-->
<section class="section-pad-80 category-v5">
	<div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-9 center-block">
                    <h1 class="text-capitalize">Ads categories</h1>
                    <p></p>
                </div><!--col-lg-7-->
            </div><!--row-->
        </div><!--container-->
    </div><!--section-heading-v1-->
	<div class="container">
		<ul class="list-inline list-unstyled categories">
			@foreach(\App\Models\Category::orderBy('order','asc')->where('status',1)->where('parent_id',null)->get() as $value)
			<li class="match-height">
				<div class="category-content">					
						<i style="color:{{$value->color}};" class="{{$value->font}}"></i>
											<h4>
						<a href="{{ route('menu', $value->slug) }}">
							{{$value->name}}						</a>
					</h4>
				</div><!--category-box-->
			</li><!--col-lg-4-->
			@endforeach
			<li class="match-height">
				<div class="category-content">					
							<i style="color:#009996;" class="zmdi zmdi-case"></i>
						<h4>
						<a href="{{url('/jobs')}}">
							Jobs
						</a>
					</h4>
				</div><!--category-box-->
			</li>
					</ul>
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="view-all text-center">
				<a href="{{url('/all-categories')}}" class="btn btn-primary outline btn-style-five">View All Categories</a>
			</div>
		</div>
	</div>
</section>
<section class="classiera-premium-ads-v5 border-bottom section-pad-80">
	<div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 center-block">
                    <h3 class="text-capitalize">PREMIUM ADVERTISEMENT</h3>
                    <p>Top Premium Advertisements of Nepal</p>
                </div>
            </div>
        </div>
    </div>
	<div style="overflow: hidden; padding-top:10px;">
		<div style="margin-bottom: 40px;">			
			<div id="owl-demo" class="owl-carousel premium-carousel-v5" data-car-length="5" data-items="5" data-loop="true" data-nav="false" data-autoplay="true" data-autoplay-timeout="3000" data-dots="false" data-auto-width="false" data-auto-height="true" data-right="false" data-responsive-small="1" data-autoplay-hover="true" data-responsive-medium="2" data-responsive-large="5" data-responsive-xlarge="7" data-margin="10">
				@forelse($featured as $key => $value)
				<div class="classiera-box-div-v5 item match-height">
					<figure>
						<div class="premium-img">
							<div class="featured-tag">
								<span class="left-corner"></span>
								<span class="right-corner"></span>
								<div class="featured">
									<p>Featured</p>
								</div>
							</div><!--featured-tag-->
							@if ($value->images->count() > 0)
                                    <img class="img-responsive" src="{{ asset('upsize_images/'.$value->images->first()->full) }}" alt="{{$value->name}}">
                                    @else
                                    <img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
                                    @endif	
														<span class="hover-posts">
								<a href="{{ route('product_details', $value->slug) }}">view ad</a>
							</span>
						<span class="price">
								Rs. {{ number_format($value->price) }}</span>
								<span class="classiera-buy-sel">
                            {{@$value->adtype->name}}</span>
													</div><!--premium-img-->
						<figcaption>
							<h5><a href="{{ route('product_details', $value->slug) }}">{{$value->name}}</a></h5>
							<div class="category">
								<span>
					Category : 
					@foreach($value->categories->where('parent_id',!null) as $cat)
                    <a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a> 
                    @endforeach							</div>
						</figcaption>
					</figure>
				</div>
				@empty
				<div class="classiera-box-div-v5 item match-height">
					<figure>
						<div class="premium-img">
							<div class="featured-tag">
								<span class="left-corner"></span>
								<span class="right-corner"></span>
								<div class="featured">
									<p>Featured</p>
								</div>
							</div><!--featured-tag-->
							
                                    <img class="img-responsive" src="{{ asset('frontend/images/empty.png') }}" alt="">
                                    	
														<span class="hover-posts">
								<a href="{{ url('submit-ad') }}">Submit Ad</a>
							</span>
						<span class="price">
								Go on, it's Easy</span>
								<span class="classiera-buy-sel">
                            Want to see your stuff here??</span>
													</div><!--premium-img-->
						<figcaption>
							<h5><a href="{{ url('submit-ad') }}">Start Selling</a></h5>
							
						</figcaption>
					</figure>
				</div>
				@endforelse
				
															</div>
		</div>
		<div class="navText">
			<a class="prev btn btn-primary radius outline btn-style-five">
				<i class="icon-left zmdi zmdi-arrow-back"></i>
				Previous			</a>
			<a class="next btn btn-primary radius outline btn-style-five">
				Next				<i class="icon-right zmdi zmdi-arrow-forward"></i>
			</a>
		</div>
	</div>
</section>
<section class="classiera-advertisement advertisement-v5 section-pad-80 border-bottom">
	<div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 center-block">
                    <h3 class="text-capitalize">ADVERTISEMENTS</h3>
                    <p>A advertisement section where we have two types of ads listing one with grids and the other one with list view latest ads, popular ads &amp; random ads also featured ads will show below.</p>
                </div>
            </div>
        </div>
    </div>
	<div class="tab-divs">
		<div class="view-head">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-sm-7 col-xs-8">
                        <div class="tab-button">
                            <ul class="nav nav-tabs" role="tablist">								
                                <li role="presentation" class="active">
									<a href="#all" aria-controls="all" role="tab" data-toggle="tab">
										Latest Ads										<span class="arrow-down"></span>
									</a>
                                </li>
                                <li role="presentation">                                   
									<a href="#popular" aria-controls="popular" role="tab" data-toggle="tab">
										Popular Ads										<span class="arrow-down"></span>
									</a>
                                </li>
                                <li role="presentation">                                    
									<a href="#jobs" aria-controls="jobs" role="tab" data-toggle="tab">
										Random Ads								<span class="arrow-down"></span>
									</a>
                                </li>
                            </ul><!--nav nav-tabs-->
                        </div><!--tab-button-->
                    </div><!--col-lg-6 col-sm-8-->
					<div class="col-lg-6 col-sm-5 col-xs-4">
						<div class="view-as text-right flip">
							<a id="grid" class="grid" href="#">
								<i class="fas fa-th-large"></i>
							</a>
							<a id="grid" class="grid-medium active" href="#">
								<i class="fa fa-th"></i>
							</a>
							<a id="list" class="list" href="#">
								<i class="fas fa-th-list"></i>
							</a>							
                        </div><!--view-as tab-button-->
					</div><!--col-lg-6 col-sm-4 col-xs-12-->
				</div><!--row-->
			</div><!--container-->
		</div><!--view-head-->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active" id="all">
				<div class="container">
					<div class="row">
					<!--FeaturedAds-->
					@forelse($latest as $key => $value)
					<div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid-medium">
	<div class="classiera-box-div classiera-box-div-v5">
		<figure class="clearfix">
			<div class="premium-img">
				@if($value->featured == 1)
								<div class="featured-tag">
						<span class="left-corner"></span>
						<span class="right-corner"></span>
						<div class="featured">
							<p>Featured</p>
						</div>
					</div>
					@endif
					@if ($value->images->count() > 0)
					<img class="img-responsive" src="{{ asset('upsize_images/'.$value->images->first()->full) }}" alt="{{$value->name}}">
					@else
					<img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
					@endif
									<span class="hover-posts">											
					<a href="{{ route('product_details', $value->slug) }}">View Ad</a>
				</span>
				<span class="price">
						PRICE: Rs.{{ number_format($value->price) }}</span>
						<span class="classiera-buy-sel">{{@$value->adtype->name}}</span>
							</div><!--premium-img-->
			<div class="detail text-center">
								<span class="price">Rs. {{ number_format($value->price) }}</span>
								<div class="box-icon">@if($value->user->show_email == 1)
					<a href="mailto:{{ $value->user->email }}?subject">
						<i class="fas fa-envelope"></i>
					</a>@endif       @if($value->user->show_mobile == 1)
										<a href="tel:{{ $value->user->mobile}}"><i class="fas fa-phone"></i></a>@endif
									</div>
				<a href="{{ route('product_details', $value->slug) }}" class="btn btn-primary outline btn-style-five">View Ad</a>
			</div><!--detail text-center-->
			<figcaption>
								<span class="price visible-xs">
					Rs.{{ number_format($value->price) }}</span>
								<h5><a href="{{ route('product_details',$value->slug) }}">{{$value->name}}</a></h5>
				<div class="category">
					<span>
					Category : 
					@foreach($value->categories->where('parent_id',!null) as $cat)
					
					<a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a>
				@endforeach </span>
										<span>Location : 
						<a href="{{route('findByCity',$value->city->slug)}}" title="View all ads in {{$value->city->name}}">{{$value->city->name}}</a>
					</span>
				</div>
				<p class="description">{{substr($value->desc, 0, 340)}}</p>
			</figcaption>
		</figure>
	</div><!--row-->
</div><!--col-lg-4-->
@empty
<div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid">
	<div class="classiera-box-div classiera-box-div-v5">
		<figure class="clearfix">
			<div class="premium-img">
				
					<img class="img-responsive" src="{{ asset('frontend/images/empty.png') }}" alt="Submit Ads">
									<span class="hover-posts">											
					<a href="{{ url('submit-ad') }}">Submit Ad</a>
				</span>
				<span class="price">
						Go On, It's Easy</span>
						<span class="classiera-buy-sel">Want to see your stuff here??</span>
							</div><!--premium-img-->
			<div class="detail text-center">
								<span class="price">Go On, It's Easy</span>
								<div class="box-icon">
									</div>
				<a href="{{ url('submit-ad') }}" class="btn btn-primary outline btn-style-five">Submit Ad</a>
			</div><!--detail text-center-->
			<figcaption>
								<span class="price visible-xs">
					Go On, It's Easy</span>
								<h5><a href="{{ url('submit-ad') }}">Start Selling</a></h5>
				<div class="category">
					
										
				</div>
				<p class="description">Make some extra cash by selling things in your community. Go on, It's Quick and Easy .</p>
			</figcaption>
		</figure>
	</div><!--row-->
</div><!--col-lg-4-->
@endforelse

					</div><!--row-->
				</div><!--container-->
			</div><!--tabpanel-->
			
			<div role="tabpanel" class="tab-pane fade" id="jobs">
				<div class="container">
					<div class="row">
						@foreach($old as $key => $value)
					<div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid-medium">
	<div class="classiera-box-div classiera-box-div-v5">
		<figure class="clearfix">
			<div class="premium-img">
				@if($value->featured == 1)
								<div class="featured-tag">
						<span class="left-corner"></span>
						<span class="right-corner"></span>
						<div class="featured">
							<p>Featured</p>
						</div>
					</div>
					@endif
					@if ($value->images->count() > 0)
					<img class="img-responsive" src="{{ asset('upsize_images/'.$value->images->first()->full) }}" alt="{{$value->name}}" style="max-height: 100%">
					@else
					<img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
					@endif
									<span class="hover-posts">											
					<a href="{{ route('product_details', $value->slug) }}">View Ad</a>
				</span>
				<span class="price">
						PRICE: Rs.{{ number_format($value->price) }}</span>
						<span class="classiera-buy-sel">{{@$value->adtype->name}}</span>
							</div><!--premium-img-->
			<div class="detail text-center">
								<span class="price">Rs. {{ number_format($value->price) }}</span>
								<div class="box-icon">
									@if($value->user->show_email == 1)
					<a href="mailto:{{ $value->user->email }}?subject">
						<i class="fas fa-envelope"></i>
					</a>@endif      @if($value->user->show_mobile == 1)
										<a href="tel:{{ $value->user->mobile}}"><i class="fas fa-phone"></i></a>@endif
									</div>
				<a href="{{ route('product_details', $value->slug) }}" class="btn btn-primary outline btn-style-five">View Ad</a>
			</div><!--detail text-center-->
			<figcaption>
								<span class="price visible-xs">
					Rs.{{ number_format($value->price) }}</span>
								<h5><a href="{{ route('product_details',$value->slug) }}">{{$value->name}}</a></h5>
				<div class="category">
					<span>
					Category : 
					@foreach($value->categories->where('parent_id',!null) as $cat)
					
					<a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a>
				@endforeach </span>
										<span>Location : 
						<a href="{{route('findByCity',$value->city->slug)}}" title="View all ads in {{$value->city->name}}">{{$value->city->name}}</a>
					</span>
				</div>
				<p class="description">{{substr($value->desc, 0, 340)}}</p>
			</figcaption>
		</figure>
	</div><!--row-->
</div><!--col-lg-4-->
@endforeach
</div><!--row-->
				</div><!--container-->
			</div><!--tabpanel Random-->
			<div role="tabpanel" class="tab-pane fade" id="popular">
				<div class="container">
					<div class="row">
						@foreach($popular as $key => $value)
					<div class="col-lg-4 col-md-4 col-sm-6 match-height item item-grid-medium">
	<div class="classiera-box-div classiera-box-div-v5">
		<figure class="clearfix">
			<div class="premium-img">
				@if($value->featured == 1)
								<div class="featured-tag">
						<span class="left-corner"></span>
						<span class="right-corner"></span>
						<div class="featured">
							<p>Featured</p>
						</div>
					</div>
					@endif
					@if ($value->images->count() > 0)
					<img class="img-responsive" src="{{ asset('upsize_images/'.$value->images->first()->full) }}" alt="{{$value->name}}" style="max-height: 100%">
					@else
					<img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
					@endif
									<span class="hover-posts">											
					<a href="{{ route('product_details',$value->slug) }}">View Ad</a>
				</span>
				<span class="price">
						PRICE: Rs.{{ number_format($value->price) }}</span>
						<span class="classiera-buy-sel">{{@$value->adtype->name}}</span>
							</div><!--premium-img-->
			<div class="detail text-center">
								<span class="price">
					Rs.{{ number_format($value->price) }}</span>
								<div class="box-icon">
									@if($value->user->show_email == 1)
					<a href="mailto:{{$value->user->email}}?subject">
						<i class="fas fa-envelope"></i>
					</a>@endif
					@if($value->user->show_phone == 1)
										<a href="tel:{{$value->user->mobile}}"><i class="fas fa-phone"></i></a>@endif
									</div>
				<a href="{{ route('product_details', $value->slug) }}" class="btn btn-primary outline btn-style-five">View Ad</a>
			</div><!--detail text-center-->
			<figcaption>
								<span class="price visible-xs">
					Rs.{{ number_format($value->price) }}</span>
								<h5><a href="{{ route('product_details',$value->slug) }}">{{$value->name}}</a></h5>
				<div class="category">
					<span>
					Category : 
				@foreach($value->categories->where('parent_id',!null) as $cat)
					
					<a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a>
				@endforeach </span>
										<span>Location : 
						<a href="{{route('findByCity',$value->city->slug)}}" title="View all posts in {{$value->city->name}}">{{$value->city->name}}</a>
					</span>
				</div>
				<p class="description">{{substr($value->desc, 0, 340)}}</p>
			</figcaption>
		</figure>
	</div><!--row-->
</div><!--col-lg-4-->
@endforeach
				</div><!--row-->
				</div><!--container-->
			</div><!--tabpanel popular-->
						<div class="view-all text-center">               
				<a href="{{route('search-product')}}" class="btn btn-primary outline btn-md btn-style-five">
					View All Ads				</a>
            </div>
		</div><!--tab-content-->
	</div><!--tab-divs-->
</section>

<section class="classiera-advertisement advertisement-v5 section-pad-80 border-bottom">
    <div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 center-block">
                    <h3 class="text-capitalize">Jobs</h3>
                    <p>A job section where we have six types of jobs listing  one with top, hot,  featured, premium, platinum and the other one with general job listing.</p>
                </div>
            </div>
        </div>
    </div>

	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-8 col-lg-6">
                        <aside class="sidebar">
                            <div class="row">
                                <!--cit-->
                                
                                    <!--cit-->
                                    <div class="col-lg-12 col-md-12 col-sm-6 match-height" style="height: 730px;">
                                        <div class="widget-box">
                                            <div class="widget-title">
                                                <h4>Top Jobs</h4>
                                            </div>
                                            <div class="widget-content">
        @foreach(\App\Job::orderBy('created_at','desc')->where('jobtype_id',3)->limit(10)->get()->chunk(2) as $jobs)
                                                <div class="media footer-pr-widget-v1">
                                                	@foreach($jobs as $job)
                                                	<div class="col-md-10 col-md-8 col-lg-6">

                                                    <div class="media-left">
                                                        <a class="media-img" href="{{route('jobuser',$job->user->id)}}">
                    @if (!empty($job->user->firmuser->logo))
                    <img class="media-object" src="{{ asset('storage/'.$job->user->firmuser->logo) }}" alt="{{$job->name}}" style="max-height: 100%">
                    @else
                    <img class="media-object" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$job->name}}">
                    @endif  
                                                                            </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <span class="price">{{$job->user->firmuser->name}}</span>
                                                        <h4 class="media-heading">
                                                            <a href="{{ route('job_post', $job->slug) }}">{{$job->name}}</a>
                                                        </h4>
                                                        <span class="category">
                                                    </div>
                                                    </div>
                                                    @endforeach
                                                    
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-6 match-height" style="">
                                        <div class="widget-box">
                                            <div class="widget-title">
                                                <h4>Hot Jobs</h4>
                                            </div>
                                            <div class="widget-content">
                                                @foreach(\App\Job::orderBy('created_at','desc')->where('jobtype_id',4)->limit(10)->get()->chunk(2) as $jobs)
                                                <div class="media footer-pr-widget-v1">
                                                	@foreach($jobs as $job)
                                                	<div class="col-md-10 col-md-8 col-lg-6">

                                                    <div class="media-left">
                                                        <a class="media-img" href="{{route('jobuser',$job->user->id)}}">
                    @if (!empty($job->user->firmuser->logo))
                    <img class="media-object" src="{{ asset('storage/'.$job->user->firmuser->logo) }}" alt="{{$job->name}}" style="max-height: 100%">
                    @else
                    <img class="media-object" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$job->name}}">
                    @endif  
                                                                            </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <span class="price">{{$job->user->firmuser->name}}</span>
                                                        <h4 class="media-heading">
                                                            <a href="{{ route('job_post', $job->slug) }}">{{$job->name}}</a>
                                                        </h4>
                                                        <span class="category">
                                                    </div>
                                                    </div>
                                                    @endforeach
                                                    
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div><!--row-->
                            </aside>
                        </div>




                        <div class="col-md-6 col-md-4 col-lg-3">
                        <aside class="sidebar">
                            <div class="column row">
                                <!--cit-->
                                
                                    <!--cit-->
                                    <div class="col-lg-12 col-md-12 col-sm-6 match-height" style="height: 730px;">
                                        <div class="widget-box">
                                            <div class="widget-title">
                                                <h4>Premium Jobs</h4>
                                            </div>
                                            <div class="widget-content">
     @foreach(\App\Job::orderBy('created_at','desc')->where('jobtype_id',7)->limit(10)->get() as $job)
                                                <div class="media footer-pr-widget-v1">
                                                    <div class="media-left">
                                                        <a class="media-img" href="{{route('jobuser',$job->user->id)}}">
					@if (!empty($job->user->firmuser->logo))
                    <img class="media-object" src="{{ asset('storage/'.$job->user->firmuser->logo) }}" alt="{{$job->name}}" style="max-height: 100%">
                    @else
                    <img class="media-object" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$job->name}}">
                    @endif
                                                                            </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <span class="price">{{$job->user->firmuser->name}}</span>
                                                        <h4 class="media-heading">
                                                            <a href="{{ route('job_post', $job->slug) }}">{{$job->name}}</a>
                                                        </h4>
                                                        <span class="category">
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                    
                                </div><!--row-->
                            </aside>
                        </div>

                        <div class="col-md-6 col-md-4 col-lg-3">
                        <aside class="sidebar">
                            <div class="column row">
                                <!--cit-->
                                
                                    <!--cit-->
                                    <div class="col-lg-12 col-md-12 col-sm-6 match-height" style="height: 730px;">
                                        <div class="widget-box">
                                            <div class="widget-title">
                                                <h4>Platinum Jobs</h4>
                                            </div>
                                            <div class="widget-content">
                                            	@foreach(\App\Job::orderBy('created_at','desc')->where('jobtype_id',6)->limit(10)->get() as $job)
                                                <div class="media footer-pr-widget-v1">
                                                    <div class="media-left">
                                                        <a class="media-img" href="{{route('jobuser',$job->user->id)}}">
					@if (!empty($job->user->firmuser->logo))
                    <img class="media-object" src="{{ asset('storage/'.$job->user->firmuser->logo) }}" alt="{{$job->name}}" style="max-height: 100%">
                    @else
                    <img class="media-object" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$job->name}}">
                    @endif
                                                                            </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <span class="price">{{$job->user->firmuser->name}}</span>
                                                        <h4 class="media-heading">
                                                            <a href="{{ route('job_post', $job->slug) }}">{{$job->name}}</a>
                                                        </h4>
                                                        <span class="category">
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                    
                                </div><!--row-->
                            </aside>
                        </div>





		</div>
	</div>

	<div class="view-all text-center">               
                <a href="{{url('all-jobs')}}" class="btn btn-primary outline btn-md btn-style-five">
                    View All Jobs                </a>
            </div>
	
</section>

@if($posts->count() > 0)
<section class="blog-post-section blog-post-section-bg section-pad-80 border-bottom">
    <!-- section heading with icon -->
	    <div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 center-block">
                    <h1 class="text-capitalize">Latest Blog Posts</h1>
					                </div>
            </div>
        </div>
    </div><!-- section heading with icon -->
	    <div class="container">
        <div class="row">
        	@foreach($posts as $post)
        	<div class="col-lg-3 col-sm-4 col-md-3">
                <div class="blog-post blog-post-v2 match-height">
                    <div class="blog-post-img-sec">
                        <div class="blog-img">
                        	
                            <img width="800" height="500" src="{{@asset('images_small/'. $post->image)}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="blog post" srcset="{{@asset('images_small/'. $post->image)}}" sizes="(max-width: 800px) 100vw, 800px" />
                            <span class="hover-posts">
                                <a href="{{route('blog',$post->slug)}}" class="btn btn-primary radius btn-md active">
                                    View Post
                                </a>
                            </span>
                        </div>
                    </div>
                    <div class="blog-post-content">
                        <h4><a href="{{route('blog',$post->slug)}}">{{$post->name}}</a></h4>
                        <p>
                            <span>
								<i class="fas fa-user"></i>
								<a href="#">{{$post->author->name}}</a>
							</span>
                            <span><i class="far fa-clock"></i>{{$value->created_at->format('M j, Y')}}</span>
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
	    <div class="view-all text-center">
        <a href="{{route('blog.all')}}" class="btn btn-primary btn-style-five">View All</a>
    </div>
</section><!-- /.blog post -->
@endif
<section class="call-to-action parallax__400" style="background-image:url()">
	<div class="container">
		<div class="row gutter-15">
			<div class="col-lg-4 col-md-4 col-sm-6">
                <div class="call-to-action-box match-height">
                    <div class="action-box-heading">
                        <div class="heading-content">
                            <img src="{{ asset('frontend/images/buy-icon.png') }}" alt="Post Classified">
                        </div>
                        <div class="heading-content">
                            <h3>Post a Classified</h3>
                        </div>
                    </div>
                    <div class="action-box-content">
                        <p>
                            Merooffer.com allows you to sell anything from cars, mobile, flat, house, apartment and more.</p>
                    </div>
                </div>
            </div><!--End About Section-->
			<div class="col-lg-4 col-md-4 col-sm-6">
                <div class="call-to-action-box match-height">
                    <div class="action-box-heading">
                        <div class="heading-content">
                            <img src="{{ asset('frontend/images/apply-png.png') }}" alt="Post a job">
                        </div>
                        <div class="heading-content">
                            <h3>Post a job</h3>
                        </div>
                    </div>
                    <div class="action-box-content">
                        <p>
                            You can start posting jobs once you signup your account. Receive a resume or you will receive a list of applicants and simply shortlist the desired candidate. 
                        </p>
                    </div>
                </div>
            </div><!--End Sell Safely Section-->
			<div class="col-lg-4 col-md-4 col-sm-6">
                <div class="call-to-action-box match-height">
                    <div class="action-box-heading">
                        <div class="heading-content">
                            <img src="{{ asset('frontend/images/choose-freelancers.svg') }}" alt="Sell Safely">
                        </div>
                        <div class="heading-content">
                            <h3>Post a project</h3>
                        </div>
                    </div>
                    <div class="action-box-content">
                        <p>
                            It's easy. Simply post a project you need completed and receive competitive offfers from freelancers within minutes.                     </p>
                    </div>
                </div>
            </div><!--End About Section-->
		</div><!--row gutter-15-->
	</div><!--container-->
</section><!--call-to-action-->
<style>.item-grid-medium{
	position: relative;
    min-height: 1px;
    padding-right: 5px;
    padding-left: 5px;
			}
				</style>
{{--
<section class="partners-v3 section-gray-bg">   
    <div class="container" style="overflow: hidden;">
        <div class="row">   
            <div class="col-lg-12">
                <center><h5>Find job by company</h5></center>
                <div class="owl-carousel partner-carousel-v3 partner-carousel-v4" data-car-length="6" data-items="6" data-loop="true" data-nav="false" data-autoplay="true" data-autoplay-timeout="2000" data-dots="false" data-auto-width="false" data-auto-height="true" data-right="false" data-responsive-small="2" data-autoplay-hover="true" data-responsive-medium="4" data-responsive-xlarge="6" data-margin="30">
                    
                    @foreach(\App\Firmuser::all() as $value)
                        <div class="item partner-img match-height">
                            <span class="helper"></span>
                            <a href="{{route('jobuser',$value->user->id)}}" target="_blank">
                                <img src="{{asset('storage/'. $value->logo)}}" alt="partner">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div><!--col-lg-12-->  
        </div><!--row-->
    </div><!--container-->
</section>
--}}
@if($partners->count() > 0)
<section class="partners-v3 section-gray-bg">   
    <div class="container" style="overflow: hidden;">
        <div class="row">   
            <div class="col-lg-12">
                <div class="owl-carousel partner-carousel-v3 partner-carousel-v4" data-car-length="6" data-items="6" data-loop="true" data-nav="false" data-autoplay="true" data-autoplay-timeout="2000" data-dots="false" data-auto-width="false" data-auto-height="true" data-right="false" data-responsive-small="2" data-autoplay-hover="true" data-responsive-medium="4" data-responsive-xlarge="6" data-margin="30">
                    @foreach($partners as $value)
                        <div class="item partner-img match-height">
                            <span class="helper"></span>
                            <a href="//{{$value->link}}" target="_blank">
                                <img src="{{@asset('images_small/'.$value->image)}}" alt="partner">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div><!--col-lg-12-->  
        </div><!--row-->
    </div><!--container-->
</section>
@endif
@include('site.partials.footer')
@endsection