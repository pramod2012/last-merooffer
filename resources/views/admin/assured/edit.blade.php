@extends('admin.assured.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form method="POST" action="{{ route('assured.update',$data->id) }}" aria-label="category" enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{method_field('PATCH')}}
                    <div class="form-group">
                        <label for="alias">Name<span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{{ old('name',$data->name) }}">
                    </div>
                    <div class="form-group">
                        <label for="address_1">Summary <span class="text-danger">*</span></label>
                        <textarea type="text" name="summary" id="summary" placeholder="summary" class="form-control" value="{{ old('summary') }}">{{ old('summary',$data->summary) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="desc">Desc</label>
                        <textarea type="text" name="desc" id="desc" placeholder="Description" class="form-control" value="{{ old('desc') }}">{{ old('desc',$data->desc) }}</textarea>
                    </div>
                    
                    <div class="form-group">
                            <div class="col-md-12">
                                  <div class="checkbox">
                                    <label>
                                        <input name="status" type="checkbox" {{$data->status?"checked":""}}>
                                      Status<span class="text-danger">{{ $errors->first('status') }}</span>
                                    </label>
                                  </div>
                              </div>
               </div> 
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
