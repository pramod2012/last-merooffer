<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\CategoryContract;
use App\Http\Controllers\BaseController;

class CategoryController extends BaseController
{
    private $page="admin.category.";

    protected $categoryRepo;

    public function __construct(
        CategoryContract $categoryRepo
    )
    {
        $this->categoryRepo = $categoryRepo;
    }

    public function index()
    {
        $datas = $this->categoryRepo->list('order', 'asc');
        $this->setPageTitle('Categories', 'List of Categories');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $categories = $this->categoryRepo->list();
        $this->setPageTitle('Categories', 'Create category');
        return view($this->page.'create',compact('categories'));
    }

    public function store(Request $request)
    {
        $data = $this->categoryRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('category.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->categoryRepo->findById($id);
        $categories = $this->categoryRepo->list();
        $this->setPageTitle('Categories', 'Edit category');
        return view($this->page.'edit', compact('data','categories'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->categoryRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('category.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->categoryRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('category.index', 'Record deleted successfully' ,'success',false, false);
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('datas');
        foreach($allArticles as $_article){
            $article = \App\Models\Category::find($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
