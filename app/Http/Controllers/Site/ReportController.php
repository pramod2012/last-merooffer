<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\ReportContract;
use App\Http\Requests\StoreReportRequest;

class ReportController extends Controller
{
    protected $reportRepo;

    public function __construct(
        ReportContract $reportRepo
    ) {
        $this->middleware('auth');
        $this->reportRepo = $reportRepo;
    }

    public function storeReport(StoreReportRequest $request)
    {
        $report = $this->reportRepo->store($request);
        return response()->json(['status' => 1, 'message' => 'Thanks for report, Our Team will take action ASAP. !']);
        // return redirect()->back()->with('success', 'Thanks for report, Our Team will take action ASAP. !');
    }
}
