@extends('site.partials.main_index')
@section('title', 'Login')
@section('content')  
<body class="page-template page-template-template-login page-template-template-login-php page page-id-28">
@include('site.partials.navbar')
<section class="inner-page-content border-bottom top-pad-50">
    <div class="login-register login-register-v1">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-11 col-sm-12 center-block">

                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-lg-12">

                            <div class="classiera-login-register-heading border-bottom text-center">
                                <h3 class="text-uppercase">{{ __('Verify Your Email Address') }}</h3>
                            </div><!--classiera-login-register-heading-->
                                                        
                                    
                                </div>
                            </div><!--social-login-->
                                                    </div><!--col-lg-12-->
                    </div><!--row-->
                    <div class="row">
                        <div class="col-lg-8 alert alert-danger center-block">
                            {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="" onclick="event.preventDefault(); document.getElementById('email-form').submit();">{{ __('click here to request another') }}
</a>.

<form id="email-form" action="{{ route('verification.resend') }}" method="POST" style="display: none;">
                    @csrf
</form>
                        </div><!--col-lg-8-->
                    </div><!--row-->
                </div><!--col-lg-10-->
            </div><!--row-->    
        </div><!--container-->  
    </div><!--login-register login-register-v1-->
</section>
<!-- page content -->
@include('site.partials.footer')
@endsection