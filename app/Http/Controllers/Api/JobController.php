<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Contracts\JobContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests as RulesOf;
use App\Http\Requests\UpdateJobRequest;
use App\Utils as Utils;

class JobController extends BaseController
{
    protected $jobRepo;

    public function __construct(
        JobContract $jobRepo
    )
    {
        $this->jobRepo = $jobRepo;
    }

    public function index()
    {
        try{
            $data = $this->jobRepo->getJobByUser();
            $data = new Utils\SuccessJsonResponse($data,"success");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function show($id)
    {
        try{
            $data1 = $this->jobRepo->findById($id);
            $data = new Utils\SuccessJsonResponse($data1,"success");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function store(Request $request)
    {
        try{
            app()->make(RulesOf\StoreJobRequest::class);
            $data1 = $this->jobRepo->storeJob($request);
            $data = new Utils\SuccessJsonResponse($data1, "Record added Successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function update(Request $request, $id)
    {
        try{
            app()->make(UpdateJobRequest::class);
            $data1 = $this->jobRepo->editJob($request, $id);
            $data = new Utils\SuccessJsonResponse($data1, "Record updated successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }

    public function destroy(Request $request, $id)
    {
        try{
            $data1 = $this->jobRepo->destroy($id);
            $data = new Utils\SuccessJsonResponse($data1, "Record deleted Successfully");
            return response()->json($data->json());
        }
        catch (\Exception $e) {
            $data = new Utils\ExceptionJsonResponse($e);
            return response()->json($data->json());
        }
    }
}
