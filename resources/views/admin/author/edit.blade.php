@extends('admin.author.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update New Author</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('author.update', $author->id)}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                        <div class="form-group">
                            <label for="txtname" class="col-md-4 control-label">Full Name * </label>
                            <div class="col-md-6">
                                <input id="txtname" type="text" class="form-control" name="name" value="{{ $author->name }}">

                            </div>
                        </div>

                            <div class="form-group">
                            <label for="txtname" class="col-md-4 control-label">Image *</label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control" name="image" value="{{ $author->image }}"><br>
                                <span class="text-danger">{{ $errors->first('image') }}</span>
                                <p> <b>Note: If you browse new image the previous file will be deleted and new will be replaced. <br><br>
                                    <img src="{{ asset('storage/'.$author->image) }}" width="150px" height="150px"/>
                                     <br><br>
                           
                                    Please upload image with appropriate size.
                                </b></p>

                            </div>
                        </div>
                        


                        <div class="form-group">
                            <label for="txtusername" class="col-md-4 control-label">Username *</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ $author->username }}">

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtphone" class="col-md-4 control-label">Phone Number </label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ $author->phone }}">

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtremarks" class="col-md-4 control-label">Remarks </label>

                            <div class="col-md-6">
                                <textarea type="text" class="form-control" name="remarks" value="{{ $author->remarks }}">{{ $author->remarks }}</textarea>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtname" class="col-md-4 control-label">Email *</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ $author->email }}">

                            </div>
                        </div>
                        
                <div class="form-group">
                            <div class="col-md-12">
                                  <div class="checkbox">
                                    <label>
                                        <input name="status" type="checkbox" {{$author->status?"checked":""}}>
                                      Publication Status*<span class="text-danger">{{ $errors->first('status') }}</span>
                                    </label>
                                  </div>
                              </div>
               </div>

                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
