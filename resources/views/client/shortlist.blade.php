@extends('layouts.app1')


@section('title')
    Shortlist Candidate
@endsection


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 my-5">
            @include('partials.alert')
            <div class="card card-default text-white">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item col-sm-6 p-0 text-center">
                        <a class="nav-link active py-4" data-toggle="tab" href="#tabs-1" role="tab">
                            <h3 class="m-0">Job Post</h3>
                        </a>
                    </li>
                    <li class="nav-item col-sm-6 p-0 text-center">
                        <a class="nav-link py-4" data-toggle="tab" href="#tabs-2" role="tab">
                            <h3 class="m-0">Review Proposals</h3>
                        </a>
                    </li>
                </ul>

                <div class="tab-content text-muted p-3">
                    <div class="tab-pane active" id="tabs-1" role="tabpanel">
                        <h4>Job Post</h4>
                        <div class="row">
                            <div class="col-sm-9">
                                <h5 class="text-info">{{ $job->title }}</h5>
                                <div> {!! $job->body !!}</div>
                            </div>
                            <div class="col-sm-3">
                                <ul class="list-unstyled">
                                    <li class="mb-2">
                                        <span class="text-success">
                                            <i class="fas fa-rupee-sign"></i> Salary : 
                                        </span>
                                         NRs. {{number_format($job->salary)}}
                                    </li>
                                    <li class="mb-2">
                                        <span class="text-success">
                                            <i class="fas fa-clock"></i> Posted: 
                                        </span>
                                        {{$job->created_at->diffForHumans()}}
                                    </li>
                                    <li class="mb-2">
                                        <span class="text-success">
                                            <i class="fas fa-briefcase"></i> Position : 
                                        </span>
                                        {{ucwords($job->positiontype->name)}}
                                    </li>
                                    <li class="mb-2">
                                        <span class="text-success">
                                            <i class="fas fa-hourglass-end"></i> Last Date:  
                                        </span>
                                        {{ ucwords($job->endate) }}
                                    </li>
                                    <li class="mb-2">
                                        <span class="text-success">
                                            <i class="fas fa-tags"></i> Category: 
                                        </span>
                                        {{ ucwords($job->category->name) }}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tabs-2" role="tabpanel">
                        <div class="table-responsive">
                            <table class="table">                              
                              <tbody>

                               @foreach($applicants->sortByDesc('created_at') as $applicant )
                                    <tr>
                                        @if(!empty($applicant->photo))
                                         <td class="text-center" ><img src="{{@asset('photo/'. $applicant->photo)}}" class="p-0 rounded-circle" style="height: 80px"></td> 
                                        @else 
                                           <td class="text-center" ><i class="fas fa-user-circle fa-6x text-muted"></i></td>
                                        @endif 
                                      <td class="text-nowrap"><h5 class="h5"> <a class="text-info" href="/proposal/{{$job->slug}}/{{$applicant->id}}">{{ $applicant->name }}</a></h5>
                                        <p>{{ $applicant->job_title }}</p>
                                        <p class="small"> 
                                            <span class="mr-5">
                                                <i class="fas fa-envelope"></i> Received: {{ $job->created_at->diffForHumans() }}
                                            </span>                                            
                                            <span><i class="fas fa-map-marker-alt"></i> {{ $applicant->country }}
                                            </span>
                                        </p>
                                      </td>
                                     @if($applicant->astatus == 'hired')
                                        <td>
                                          <h4><span class="badge badge-success w-100"><i class="text-white fas fa-check"></i> <strong>HIRED</strong></span></h4>
                                       </td>
                                     @elseif ($applicant->astatus == 'rejected')
                                          <td>
                                           <h4><span class="badge badge-danger w-100"><i class="text-white fas fa-times"></i> <strong>REJECTED</strong></span></h4>
                                       </td>
                                     @endif
                                        <td>
                                          <a href="/proposal/{{ $job->id }}/{{$applicant->id}}/hire" data-toggle="tooltip" data-placement="top" title="Hire Candidate">
                                             <i class="fas fa-thumbs-up fa-2x"></i>
                                          </a>
                                       </td>
                                      <td>
                                         <a href="/proposal/{{ $job->id }}/{{$applicant->id}}/reject" data-toggle="tooltip" data-placement="top" title="Reject Candidate">
                                            <i class="far fa-thumbs-down fa-2x"></i>
                                         </a>
                                      </td>
                                                                          
                                    </tr>
                                @endforeach
                              </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('jsplugins')
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script>
$(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
</script>
@endsection
