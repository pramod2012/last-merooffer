@extends('admin.skill.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Skills</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('skill.update',$skill->id) }}" enctype="multipart/form-data">
                       {{csrf_field()}}
                       {{method_field('PATCH')}}
                        <div class="form-group">
                            <label for="firstname" class="col-md-4 control-label">Skill</label>

                            <div class="col-md-6">
                                <input id="skill" type="text" class="form-control" name="skill" value="{{$skill->skill }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                 Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
