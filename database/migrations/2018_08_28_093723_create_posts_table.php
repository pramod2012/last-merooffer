<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('views')->default(0);
            $table->text('summary')->nullable();
            $table->longText('desc')->nullable();
            $table->string('keyword')->nullable();
            $table->longText('body');
            $table->boolean('is_blog')->default(1);
            $table->boolean('status')->default(1);
            $table->boolean('feature')->default(0);
            $table->string('image')->nullable();
            $table->string('date')->nullable();
            $table->bigInteger('author_id')->unsigned();
            $table->foreign('author_id')->references('id')->on('authors')->onDelete('cascade');
            $table->bigInteger('mcategory_id')->unsigned();
            $table->foreign('mcategory_id')->references('id')->on('mcategories')->onDelete('cascade');
            $table->bigInteger('order');
            $table->string('slug')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
