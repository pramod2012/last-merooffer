@extends('admin.wishlist.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$pageTitle}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('wishlist.update', $data->id) }}" enctype="multipart/form-data">
                       {{csrf_field()}}
                       {{method_field('PATCH')}}
                        <div class="form-group">
                      <label class="col-md-4 control-label">Product Name</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="product_id">
                                
                              <option value="{{$data->product_id}}">{{$data->product->name}}</option>
                             
                            </select>
                                <span class="text-danger">{{ $errors->first('product_id') }}</span>
                            </div>
                        </div>
                    <div class="form-group">
                      <label class="col-md-4 control-label">Cart Added By</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="auth_id">
                                
                              <option value="{{$data->auth_id}}">{{$data->wishlist_by->name}}</option>
                             
                            </select>
                                <span class="text-danger">{{ $errors->first('auth_id') }}</span>
                            </div>
                        </div>
                    <div class="form-group">
                      <label class="col-md-4 control-label">Product of</label>
                          <div class="col-md-6"> 
                            <select class="form-control" name="user_id">
                                
                              <option value="{{$data->user_id}}">{{$data->users_product->name}}</option>
                             
                            </select>
                                <span class="text-danger">{{ $errors->first('user_id') }}</span>
                            </div>
                        </div>
        <div class="form-group">
            <div class="col-md-12">
                  <div class="checkbox">
                    <label class="col-md-2 control-label">
                      <input name="status" type="checkbox" {{$data->status?"checked":""}}>
                      Status 
                    </label>
                  </div>
              </div>
          </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                 Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
