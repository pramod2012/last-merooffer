@extends('site.partials.main_index')
@section('title','Bank Transfer')
@section('content')
<body class="page-template page-template-template-favorite page-template-template-favorite-php page page-id-22 logged-in">
@include('site.partials.navbar')
<section class="user-pages section-gray-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-4">
      @include('site.partials.sidebar')     
</div><!--col-lg-3-->
      <div class="col-lg-9 col-md-8 user-content-height">
        <div class="user-detail-section section-bg-white">
          <!-- favorite ads -->
          <div class="user-ads favorite-ads">
            <h4 class="user-detail-section-heading text-uppercase">
            Cart Items {{Cart::count()}}            </h4>
                                                <!--singlepost-->
                   <div class="row">
                    <div class="col-md-12 content">
                        <table class="table table-striped">
                            <tfoot>
                                <tr>
                                    <td class="bg-warning">Subtotal</td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning"></td> 
                                    <td class="bg-warning">Rs. {{ number_format($subtotal, 2, '.', ',') }}</td>
                                </tr>
                                @if(isset($shippingFee) && $shippingFee != 0)
                                <tr>
                                    <td class="bg-warning">Shipping/Service Charge</td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning">Rs. {{ $shippingFee }}</td>
                                </tr>@endif
                                                                <tr>
                                    <td class="bg-warning">Tax</td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning"></td>
                                    <td class="bg-warning">Rs. {{ $tax }}</td>
                                </tr>
                                <tr>
                                    <td class="bg-success">Total</td>
                                    <td class="bg-success"></td>
                                    <td class="bg-success"></td>
                                    <td class="bg-success"></td>
                                    <td class="bg-success">Rs. {{ number_format($total, 2, '.', ',') }}</td>
                                </tr>
                                <tr>
                                    <td class="bg-red">Note: Transfer fund</td>
                                    <td class="bg-red"></td>
                                    <td class="bg-red"></td>
                                    <td class="bg-red"></td>
                                    <td class="bg-red">Esewa Id: 9849551992</td>
                                </tr>
                            </tfoot>
                        </table>

                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="btn-group pull-right">
          <form action="{{ route('bank-transfer.store') }}" method="post">
            @csrf
            
            <input type="hidden" id="billing_address" name="billing_address" value="{{ $billingAddress }}">
            <button onclick="return confirm('Are you sure?')" class="btn btn-primary">Pay now with Bank Transfer</button>
        </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>



            <!--singlepost-->
                    
                      </div><!--user-ads-->
          
          <!-- favorite ads -->
        </div><!--user-detail-->
      </div><!--col-lg-9-->
    </div><!--row-->
  </div><!--container-->
</section><!--user-pages-->
<!-- Company Section Start-->
<!-- Company Section End--> 
@include('site.partials.footer')
@endsection