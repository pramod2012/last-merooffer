@extends('site.partials.main_index')
@section('title','List of All Categories')
@section('content')
<body class="archive category category-uncategorized category-1 logged-in">
    @include('site.partials.navbar')
    @include('site.partials.search')
    <!--search-section--><!-- page content -->
                <!-- advertisement -->
<section class="classiera-premium-ads-v6 border-bottom section-pad">
        <div class="section-heading-v6">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 center-block">
                    <h3 class="text-capitalize">PREMIUM ADVERTISEMENT</h3>
                                        <p>List of all the top premium advertisment of Nepal</p>
                                    </div>
            </div>
        </div>
    </div>
        <div style="overflow: hidden;">
        <div style="margin-bottom: 40px;">          
            <div id="owl-demo" class="owl-carousel premium-carousel-v6" data-car-length="4" data-items="4" data-loop="true" data-nav="false" data-autoplay="true" data-autoplay-timeout="3000" data-dots="false" data-auto-width="false" data-auto-height="true" data-right="false" data-responsive-small="1" data-autoplay-hover="true" data-responsive-medium="2" data-responsive-large="4" data-responsive-xlarge="6" data-margin="15">
                @foreach($featured as $key => $value)
                                <div class="classiera-box-div-v6 item match-height">
                    <figure>
                        <div class="premium-img">
                            <div class="featured">
                                <p>Featured</p>
                            </div><!--featured-tag-->
                            @if ($value->images->count() > 0)
                    <img class="img-responsive" src="{{ asset('upsize_images/'.$value->images->first()->full) }}" alt="{{$value->name}}" style="max-height: 100%">
                    @else
                    <img class="img-responsive" src="{{ asset('storage/'.config('settings.error_image')) }}" alt="{{$value->name}}">
                    @endif
                            <span class="price btn btn-primary round btn-style-six active">
                                Price : 
                                Rs. {{ number_format($value->price) }}                           </span>
                                <span class="classiera-buy-sel">{{@$value->adtype->name}}</span>
                                                    </div><!--premium-img-->
                        <figcaption>
                            <div class="content">
                                <h5><a href="{{ route('product_details',$value->slug) }}">{{$value->name}}</a></h5>
                                <div class="category">
                                    <span>Category : 
                                        @foreach($value->categories->where('parent_id',!null) as $cat)
                    <a href="{{route('menu', $cat->slug)}}" title="View all posts in {{$cat->name}}">{{$cat->name}}</a> 
                    @endforeach
                                    </span>
                                </div>
                                <div class="description">
                                    <p>{!! substr($value->desc,0,400) !!}</p>
                                </div>
                                <a href="{{ route('product_details',$value->slug) }}">view ad <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                @endforeach
        </div>
        </div>
        <div class="navText">
            <a class="prev btn btn-primary round outline btn-style-six">
                <i class="icon-left fas fa-long-arrow-alt-left"></i>
                Previous            </a>
            <a class="next btn btn-primary round outline btn-style-six">
                Next                <i class="icon-right fas fa-long-arrow-alt-right"></i>
            </a>
        </div>
    </div>
</section>
            <section class="section-pad-80 category-v5">
        <div class="section-heading-v5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-9 center-block">
                    <h1 class="text-capitalize">Ads categories</h1>
                                        <p>List of all Categories of Classifieds </p>
                                    </div><!--col-lg-7-->
            </div><!--row-->
        </div><!--container-->
    </div><!--section-heading-v1-->
        <div class="container">
        <ul class="list-inline list-unstyled categories">
            <li class="match-height">
                <div class="category-content">                  
                            <i style="color:#009996;" class="zmdi zmdi-case"></i>
                        <h4>
                        <a href="{{url('/jobs')}}">
                            Jobs
                        </a>
                    </h4>
                </div><!--category-box-->
            </li>
        @foreach(\App\Models\Category::where('status',1)->orderBy('order','asc')->where('parent_id',null)->get() as $value)
            <li class="match-height">
                <div class="category-content">                  
                        <i style="color:{{$value->color}};" class="{{$value->font}}"></i>
                                            <h4>
                        <a href="{{ route('menu', $value->slug) }}">
                            {{$value->name}}                        </a>
                    </h4>
                </div><!--category-box-->
            </li><!--col-lg-4-->
            @endforeach
                        
        </ul>
    </div>
</section>
<!-- page content -->
@if($partners->count() > 0)

<section class="partners-v3 section-gray-bg">   
    <div class="container" style="overflow: hidden;">
        <div class="row">   
            <div class="col-lg-12">
                <div class="owl-carousel partner-carousel-v3 partner-carousel-v4" data-car-length="6" data-items="6" data-loop="true" data-nav="false" data-autoplay="true" data-autoplay-timeout="2000" data-dots="false" data-auto-width="false" data-auto-height="true" data-right="false" data-responsive-small="2" data-autoplay-hover="true" data-responsive-medium="4" data-responsive-xlarge="6" data-margin="30">
                    @foreach($partners as $value)
                        <div class="item partner-img match-height">
                            <span class="helper"></span>
                            <a href="{{$value->slug}}" target="_blank">
                                <img src="/images/sliders/{{ isset($value->image) ? $value->image : 'pp.jpg' }}" alt="partner">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div><!--col-lg-12-->  
        </div><!--row-->
    </div><!--container-->
</section>
@endif


@include('site.partials.footer')
@endsection