<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\ProductattributeContract;
use App\Http\Controllers\BaseController;

class ProductattributeController extends BaseController
{
    private $page="admin.productattribute.";

    protected $productattributeRepo;

    public function __construct(
        ProductattributeContract $productattributeRepo
    )
    {
        $this->productattributeRepo = $productattributeRepo;
    }

    public function index()
    {
        $list = $this->productattributeRepo->list('created_at', 'desc');
        $datas = $this->productattributeRepo->paginateArrayResults($list->all(), 5);
        $this->setPageTitle('Productattribute', 'List of Productattributes');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('Productattribute', 'Create Productattribute');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->productattributeRepo->store($request);

        return redirect()->back();
    }

    public function edit($id)
    {
        $data = $this->productattributeRepo->findById($id);
        $this->setPageTitle('Productattribute', 'Edit Productattribute');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->productattributeRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('productattribute.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->productattributeRepo->destroy($id);

        return back();
    }
}
