<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\AdtypeContract;
use App\Http\Controllers\BaseController;

class AdtypeController extends BaseController
{
    private $page="admin.adtype.";

    protected $adtypeRepo;

    public function __construct(
        AdtypeContract $adtypeRepo
    )
    {
        $this->adtypeRepo = $adtypeRepo;
    }

    public function index()
    {
        $list = $this->adtypeRepo->list('created_at', 'desc');
        $adtypes = $this->adtypeRepo->paginateArrayResults($list->all(), 6);
        $this->setPageTitle('Adtype', 'List of Adtypes');
        return view($this->page.'index',compact('adtypes'));
    }

    public function create()
    {
        $this->setPageTitle('Adtype', 'Create adtype');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->adtypeRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('adtype.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $adtype = $this->adtypeRepo->findById($id);
        $this->setPageTitle('Adtype', 'Edit adtype');
        return view($this->page.'edit', compact('adtype'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->adtypeRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('adtype.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->adtypeRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('adtype.index', 'Record deleted successfully' ,'success',false, false);
    }
}
