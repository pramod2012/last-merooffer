<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Models\Attributevalue;
use App\Models\Amenity;

class Attribute extends Model
{
    protected $fillable = [
        'code',
        'name',
        'type',
        'required',
        'filterable',
        'data_type',
        'order',
        'has_combin',
        'interval'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->order = $model->max('order') + 1;
        });
    }

    public function categories(){
    	return $this->belongsToMany(Category::class,'attribute_categories');
    }

    public function attributevalues(){
        return $this->hasMany(Attributevalue::class);
    }

    public function amenities(){
        return $this->hasMany(Amenity::class);
    }

}
