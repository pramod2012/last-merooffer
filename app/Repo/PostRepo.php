<?php

namespace App\Repo;

use App\Models\Post;
use App\Traits\UploadAble;
use Illuminate\Http\UploadedFile;
use App\Contracts\PostContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

/**
 * Class PostRepository
 *
 * @package \App\Repo
 */
class PostRepo extends BaseRepo implements PostContract
{
    use UploadAble;
    /**
     * PostRepository constructor.
     * @param Post $model
     */
    public function __construct(Post $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function store($request)
    {
        try {
            $params = $request->all();

            $collection = collect($params);

            $image = null;

            if ($collection->has('image') && ($params['image'] instanceof  UploadedFile)) {
                $image = $this->uploadOne($params['image'], 'posts');
            }

            $feature = $collection->has('feature') ? 1 : 0;
            $is_blog = $collection->has('is_blog') ? 1 : 0;
            $status = $collection->has('status') ? 1 : 0;

            $merge = $collection->merge(compact('is_blog', 'feature','status', 'image'));

            $post = new Post($merge->all());

            if ($collection->has('tag_id')) {
                $post->tags()->sync($params['tag_id']);
            }

            $post->save();

            return $post;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
    	try
    	{
        $params = $request->all();

        $post = $this->findById($id);

        $collection = collect($params);

        if ($collection->has('image') && ($params['image'] instanceof  UploadedFile)) {

            if ($post->image != null) {
                $this->deleteOne($post->image);
            }

            $image = $this->uploadOne($params['image'], 'posts');
        }

        $featured = $collection->has('featured') ? 1 : 0;
        $menu = $collection->has('menu') ? 1 : 0;
        $status = $collection->has('status') ? 1 : 0;

        $merge = $collection->merge(compact('menu', 'image', 'featured', 'status'));

        $post->update($merge->all());

        if ($collection->has('tag_id')) {
            $post->tags()->sync($params['tag_id']);
        }

        return $post;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);

        if ($data->image != null) {
            $this->deleteOne($data->image);
        }

        $data->delete();

        return $data;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $data = Post::where('slug', $slug)->firstOrFail();
        return $data;
    }
}
