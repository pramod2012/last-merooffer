@extends('site.partials.main_index')
@section('title','List of Ads for Submit')
@section('content')
    
<body class="page-template page-template-template-submit-ads page-template-template-submit-ads-php page page-id-44 logged-in">
@include('site.partials.navbar')
    <section class="user-pages section-gray-bg">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-12 col-md-8 user-content-height">
                                <div class="submit-post section-bg-white">
                    <form class="form-horizontal" action="" role="form" id="primaryPostForm" method="POST" data-toggle="validator" enctype="multipart/form-data">
                        <h4 class="text-uppercase border-bottom">MAKE NEW AD</h4>
                        <!--Category-->
                        <div class="form-main-section classiera-post-cat">
                            <div>

                                <h4 class="classiera-post-inner-heading">
                                    Select a Category :
                                </h4>
                                <ul class="list-unstyled list-inline">
                                    <li class="match-height">
                                            <a id="1212" class="border">                      <i class="fas fa-paper-plane" style="color:#c29bc2;"></i>           <span>Freelancers</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('project/create') }}" id="12122">                      <i class="#" style="color:#c29bc2;"></i>           <span>Post a Project</span>
                                            </a>
                                        </li>
                                        <li class="match-height">
                                            <a id="1212" class="border">                      <i class="zmdi zmdi-case" style="color:#c29bc2;"></i>           <span>Job</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('job-user/create') }}" id="12122">                      <i class="#" style="color:#c29bc2;"></i>           <span>Post a Job</span>
                                            </a>
                                        </li><br><br>
@foreach(\App\Models\Category::where('parent_id',null)->orderBy('order','asc')->get() as $value)
                                        <li class="match-height">
                                            <a id="{{$value->id}}" class="border">                      <i class="{{$value->font}}" style="color:#c29bc2;"></i>           <span>{{$value->name}}</span>
                                            </a>
                                        </li>
                                        @foreach($value->children as $sub)
                                        <li>
                                            <a href="{{ route('submit-ad', $sub->slug) }}" id="{{$sub->id}}">                      <i class="{{$sub->font}}" style="color:#c29bc2;"></i>           <span>{{$sub->name}}</span>
                                            </a>
                                        </li>
                                        @endforeach
                                        <br> <br>

                                    @endforeach                 
                                </ul><!--list-unstyled-->
                                
                            </div>
                            <!--ThirdLevel-->
                        </div>
                        <!--Category-->  
                    </form>
                </div><!--submit-post-->
            </div><!--col-lg-9 col-md-8 user-content-heigh-->
        </div><!--row-->
    </div><!--container-->
</section><!--user-pages-->

@include('site.partials.footer')
@endsection