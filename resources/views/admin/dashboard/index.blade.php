@extends('admin.dashboard.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
     
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          
        </div>
        <div class="col-sm-4">


          @if(Session::has('success'))
    <div class="alert alert-info fade in">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{Session::get('success')}}</strong>
     </div>
     @endif
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      
      
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{\App\Models\Post::count()}}</h3>

              <p>Total Blog Post</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{\App\Job::count()}}</h3>

              <p>Total Jobs</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">

              <h3> {{\App\Models\Product::count()}}</h3>

              <p>Total Products</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{\App\Models\User::count()}}</h3>
              <p>Total Users</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>
      
     <div class="row">
      
      <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-edit"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="{{route('post.index')}}"> Blog</a></span>
              <span class="info-box-text"><a href="{{route('author.index')}}">Author</a></span>
              <span class="info-box-text"> <a href="{{route('mcategory.index')}}">Blog Category</a></span>
              <span class="info-box-text"><a href="{{route('tag.index')}}">Tags</a></span>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-opencart"></i></span>

            <div class="info-box-content">
            
            <span class="info-box-text"> <a href="{{route('product.index')}}">Products</a> </span>
            <span class="info-box-text"> <a href="{{route('bid.index')}}">Product Bids</a> </span>
            <span class="info-box-text"> <a href="{{route('comment.index')}}">Product Comment</a> </span>
            <span class="info-box-text"> <a href="{{route('orders.index')}}">Orders</a> </span>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>       <!-- /.col -->
        
        <div class="col-md-3 col-sm-6 col-xs-12">

          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-bars"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"> <a href="{{route('category.index')}}">Main Menu</a> </span>
              <span class="info-box-number">40</span>
              <span class="info-box-text"> <a href="{{route('contact.index')}}">Contact/Message</a> </span>
            <span class="info-box-text"> <a href="{{route('report.index')}}">Report</a> </span>
              
            </div>
            <!-- /.info-box-content -->
          </div>

          <!-- /.info-box -->
        </div>
        <!-- ./col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"> <a href="{{route('user.index')}}"> User Registrations</a></span>
              <span class="info-box-text"> <a href="{{route('firmusers.index')}}"> Firm Users</a></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          
          <!-- /.info-box -->
        </div>
        
        <!-- ./col -->
      </div>
      <div class="row">
        <!-- ./col -->
         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-opencart"></i></span>

            <div class="info-box-content">
            
            <span class="info-box-text"> <a href="{{route('cities.index')}}">City</a> </span>
            <span class="info-box-text"> <a href="{{route('adtype.index')}}">Adtype</a> </span>
            <span class="info-box-text"> <a href="{{route('pricetype.index')}}">Price Negotiable</a> </span>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-cogs"></i></span>

            <div class="info-box-content">
            <span class="info-box-text"> <a href="{{ route('admin.settings') }}">Settings</a> </span>
            <span class="info-box-text"> <a href="{{route('day.index')}}">Days</a> </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-briefcase"></i></span>
            <div class="info-box-content">
              <span class="info-box-text"> <a href="{{route('job.index')}}">Job</a> </span>
              <span class="info-box-text"> <a href="{{route('jobcategory.index')}}">Job Category</a> </span>
              <span class="info-box-text"> <a href="{{route('jobtype.index')}}">Job Type</a> </span>
              <span class="info-box-text"> <a href="{{route('salarytype.index')}}">Salary Type</a> </span>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- ./col -->
  <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-briefcase"></i></span>

            <div class="info-box-content">
            <span class="info-box-text"> <a href="{{route('skill.index')}}">Job Skills</a> </span>
            <span class="info-box-text"><a href="{{route('industry.index')}}">Industry</a></span>
            <span class="info-box-text"><a href="{{route('positiontype.index')}}">Postion Type</a></span>
            <span class="info-box-text"><a href="{{route('experience.index')}}">Experiences</a></span>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-opencart"></i></span>

            <div class="info-box-content">
            <span class="info-box-text"><a href="{{route('report.index')}}">Report</a></span>
            <span class="info-box-text"><a href="{{route('follower.index')}}">Follower</a></span>
            <span class="info-box-text"><a href="{{route('wishlist.index')}}">Wishlist</a></span>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>   
         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-hand-pointer-o"></i></span>

            <div class="info-box-content">
            <span class="info-box-text"><a href="{{route('profiles.index')}}">Profile</a></span>
            <span class="info-box-text"><a href="{{route('education.index')}}">Education</a></span>
            <span class="info-box-text"> <a href="{{route('work.index')}}">Work</a></span>
            <span class="info-box-text"> <a href="{{route('training.index')}}">Training</a></span>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-file-text-o"></i></span>

            <div class="info-box-content">
              <a href="{{route('applicant.index')}}"><span class="info-box-text"> Applicants</span></a>
              <span class="info-box-number"></span>
              <span class="info-box-text"> <a href="{{route('resume.index')}}"> Resumes</a></span>
              <span class="info-box-number"></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
 
       
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>

            <div class="info-box-content">
            
            <span class="info-box-text"> <a href="{{route('partner.index')}}">Partners</a> </span>
            <span class="info-box-text"> <a href="{{route('mcontact.index')}}">Mcontact</a> </span>
            <span class="info-box-text"> <a href="{{route('email.index')}}">Send Email</a> </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>   
        
      </div>
    </div>
  </div>
  

    </section>
    
  </div>
@endsection