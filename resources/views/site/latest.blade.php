@extends('site.partials.main_index')
@section('title','Latest Ads')
@section('content')
<body class="archive category category-uncategorized category-1 logged-in">
    @include('site.partials.navbar')
    @include('site.partials.search')
    <!--search-section--><!-- page content -->
    <section class="inner-page-content border-bottom top-pad-50" id="scrollbar">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <!--category description section-->
                    <!--category description section-->
                    <!--Google Section-->
                    <!--Google Section-->
                    <!-- advertisement -->
                    <section class="classiera-advertisement advertisement-v5 section-pad-80 border-bottom">
                        <div class="tab-divs">
                            <div class="view-head">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-8 col-sm-9 col-xs-10">
                                            <div class="tab-button">
                                                <ul class="nav nav-tabs" role="tablist">                                
                                                    <li role="presentation"  <?php if(Request::get('price') != 'asc' && Request::get('price') != 'desc' && Request::get('view') != 'desc' ){ 
                                                        echo 'class="active" ';
                                                    } ?>>
                                                    <a href="{{ route('latest') }}" aria-controls="all">
                                                        Recent Ads
                                                        <span class="arrow-down"></span>
                                                    </a>
                                                </li>
                                                <li role="presentation" <?php if(Request::get('price') == 'asc'){ 
                                                    echo 'class="active" ';
                                                } ?>>                                    
                                                <a href="{{ route('latest', ['price' =>'asc']) }}" aria-controls="random">
                                                    Price ASC                                      <span class="arrow-down"></span>
                                                </a>
                                            </li>
                                            <li role="presentation"<?php if(Request::get('price') == 'desc'){ 
                                                echo 'class="active" ';
                                            } ?>>                                    
                                            <a href="{{ route('latest', ['price' =>'desc']) }}" aria-controls="random">
                                                Price DESC                                      <span class="arrow-down"></span>
                                            </a>
                                        </li>
                                        <li role="presentation" <?php if(Request::get('view')){ 
                                            echo 'class="active" ';
                                        } ?>>                                   
                                        <a href="{{ route('latest', ['view' =>'desc']) }}" aria-controls="popular">
                                         Popular Ads                                    <span class="arrow-down"></span>
                                     </a>
                                 </li>
                             </ul><!--nav nav-tabs-->
                         </div><!--tab-button-->
                     </div><!--col-lg-6 col-sm-8-->
                     <div class="col-lg-4 col-sm-3 col-xs-2">
                        <div class="view-as text-right flip">
                            <a id="grid" class="grid" href="#"><i class="fas fa-th"></i></a>
                            <a id="list" class="list active" href="#"><i class="fas fa-th-list"></i></a>                          
                        </div><!--view-as tab-button-->
                    </div><!--col-lg-6 col-sm-4 col-xs-12-->
                </div><!--row-->
            </div><!--container-->
        </div><!--view-head-->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="all">
                <div class="container products" id="table_data">
                    @include('site.pagination')
                         </section><!-- advertisement -->
                     </div><!--col-md-8-->
                     <div class="col-md-4 col-lg-3">
                        <aside class="sidebar">
                            <div class="row">
                                <!--cit-->
                                <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                                    <div class="widget-box">
                                        <div class="widget-title">
                                            <h4>
                                                <i class="zmdi zmdi-menu" style="color:#c29bc2;"></i>
                                                All Ads                             </h4>
                                            </div>
                                            <div class="widget-content">
                                                <ul class="category">
                                                    
                                                    <li>
                                                        <a href="{{route('index')}}">
                                                            <i class="fas fa-angle-right"></i>
                                                            Home
                                                        </a>
                                                    </li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!--cit-->
                                    <div class="col-lg-12 col-md-12 col-sm-6 match-height">
                                        <div class="widget-box">
                                            @include('site.partials.advance_search')
                                            
                                        </div>
                                    </div>

                                    
                                </div><!--row-->
                            </aside>
                        </div><!--row-->
                    </div><!--row-->
                </div><!--container-->
            </section>  
            <!-- page content -->

            @include('site.partials.footer')
@section('extra-script')
    <script type='text/javascript'>
        jQuery(document).ready(function() {

        jQuery(document).on('click', '.pagination a', function(event) {
            event.preventDefault();
            jQuery('html, body').animate({
        scrollTop: jQuery("#scrollbar").offset().top
    }, 200);
            var page = jQuery(this).attr('href').split('page=')[1];
            fetch_data(page);
            var l = window.location;
            window.history.pushState("", "", l.origin + l.pathname + "?page=" + page);
        });

        function fetch_data(page) {
            jQuery.ajax({
                url: "/welcome/pagination?page=" + page,
                success: function(products) {
                    jQuery('#table_data').html(products);
                }
            });
        }

    });
    </script>
@stop
            @endsection