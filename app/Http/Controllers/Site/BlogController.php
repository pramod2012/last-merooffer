<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\McategoryContract;
use App\Contracts\PostContract;
use App\Contracts\TagContract;

class BlogController extends Controller
{
    protected $postRepo;
    protected $mcategoryRepo;
    protected $tagRepo;

    public function __construct(
        PostContract $postRepo,
         McategoryContract $mcategoryRepo,
         TagContract $tagRepo
     )
    {
        $this->postRepo = $postRepo;
        $this->mcategoryRepo = $mcategoryRepo;
        $this->tagRepo = $tagRepo;
    }

    public function show($slug){
    	$mcategory = $this->mcategoryRepo->list();
    	$post = $this->postRepo->findBySlug($slug);
    	return view('site.blog.show', compact('mcategory', 'post'));
    }

    public function all(){
    	$mcategory = $this->mcategoryRepo->list();
    	$posts = $this->postRepo->list();
    	return view('site.blog.allpost',compact('mcategory','posts'));
    }

    public function findByBlogCategory($slug){
        $mcategory = $this->mcategoryRepo->findBySlug($slug);
        $mcategories = $this->mcategoryRepo->list();
        return view('site.blog.findByBlogCategory',compact('mcategory','mcategories'));
    }

    public function findByTag($slug){
        $tag = $this->tagRepo->list();
        return view('site.blog.findByTag',compact('tag'));
    }
}
