<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\ReportnameContract;
use App\Http\Controllers\BaseController;

class ReportnameController extends BaseController
{
    private $page="admin.reportname.";

    protected $reportnameRepo;

    public function __construct(
        ReportnameContract $reportnameRepo
    )
    {
        $this->reportnameRepo = $reportnameRepo;
    }

    public function index()
    {
        $list = $this->reportnameRepo->list('created_at', 'desc');
        $datas = $this->reportnameRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('reportname', 'List of reportname');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('reportname', 'Create reportname');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->reportnameRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('reportname.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->reportnameRepo->findById($id);
        $this->setPageTitle('reportname', 'Edit reportname');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->reportnameRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('reportname.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->reportnameRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('reportname.index', 'Record deleted successfully' ,'success',false, false);
    }
}
