@extends('admin.address.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$subTitle}}</div>
                <div class="panel-body">
                    <form method="POST" action="{{ route('address.store') }}" aria-label="category" enctype="multipart/form-data">
                        {{csrf_field()}}
                        

                        <div class="form-group">
                        <label for="customer">Users </label>
                        <select name="user_id" id="status" class="form-control select2">
                            @foreach(\App\Models\User::all() as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="alias">Alias <span class="text-danger">*</span></label>
                        <input type="text" name="alias" id="alias" placeholder="Home or Office" class="form-control" value="{{ old('alias') }}">
                    </div>
                    <div class="form-group">
                        <label for="address_1">Address 1 <span class="text-danger">*</span></label>
                        <input type="text" name="address_1" id="address_1" placeholder="Address 1" class="form-control" value="{{ old('address_1') }}">
                    </div>
                    <div class="form-group">
                        <label for="address_2">Address 2 </label>
                        <input type="text" name="address_2" id="address_2" placeholder="Address 2" class="form-control" value="{{ old('address_2') }}">
                    </div>
                    <div class="form-group">
                        <label for="address_2">Country </label>
                        <input type="text" name="country" id="country" placeholder="Country" class="form-control" value="{{ old('country') }}">
                    </div>

                    <div class="form-group">
                        <label for="province">Province </label>
                        <input type="text" name="province" id="province" placeholder="Province" class="form-control" value="{{ old('province') }}">
                    </div>

                    <div class="form-group">
                        <label for="city">City </label>
                        <input type="text" name="city" id="city" placeholder="City" class="form-control" value="{{ old('city') }}">
                    </div>
                    
                    
                    
                    <div class="form-group">
                        <label for="zip">Zip Code </label>
                        <input type="text" name="zip" id="zip" placeholder="Zip code" class="form-control" value="{{ old('zip') }}">
                    </div>

                    <div class="form-group">
                        <label for="zip">State Code </label>
                        <input type="text" name="state_code" id="state_code" placeholder="state_code" class="form-control" value="{{ old('state_code') }}">
                    </div>
                    <div class="form-group">
                        <label for="status">Status </label>
                        <select name="status" id="status" class="form-control">
                            <option value="0">Disable</option>
                            <option value="1">Enable</option>
                        </select>
                    </div>
                
                    
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
