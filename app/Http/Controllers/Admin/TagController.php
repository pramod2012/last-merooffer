<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\TagContract;
use App\Http\Controllers\BaseController;

class TagController extends BaseController
{
    private $page="admin.tag.";

    protected $tagRepo;

    public function __construct(
        TagContract $tagRepo
    )
    {
        $this->tagRepo = $tagRepo;
    }

    public function index()
    {
        $list = $this->tagRepo->list('created_at', 'desc');
        $tags = $this->tagRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('Tags', 'List of Tags');
        return view($this->page.'index',compact('tags'));
    }

    public function create()
    {
        $this->setPageTitle('Tags', 'Create tag');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->tagRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('tag.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $tag = $this->tagRepo->findById($id);
        $this->setPageTitle('Cities', 'Edit tag');
        return view($this->page.'edit', compact('tag'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->tagRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('tag.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->tagRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('tag.index', 'Record deleted successfully' ,'success',false, false);
    }
}
