<?php

namespace App\Utils;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Throwable;

class ExceptionJsonResponse
{
    public $exception;
    public $preserve_errors;

    public function __construct($exception, $preserve_errors = true)
    {
        $this->exception = $exception;
        $this->preserve_errors = $preserve_errors;
    }

    public function json()
    {
        if ($this->exception instanceof ValidationException) {
            if ($this->preserve_errors) {
                $errors = $this->exception->validator->errors()->all();
            } else {
                $errors = $this->exception->validator->errors();
            }
        } else if ($this->exception instanceof ModelNotFoundException) {
            $errors = ['Entry for this model not found'];
        } else if ($this->exception instanceof QueryException) {
            $errors = ['Query not found'];
        } else if ($this->exception instanceof InvalidArgumentException) {
            $errors = ['Invalid Argument'];
        } else if ($this->exception instanceof AuthorizationException) {
            $errors = ['You are not authorized to perform this action'];
        } else {
            $errors = [env('APP_ENV') == "local" ? $this->exception->getMessage() : $this->exception->getMessage()];
        }
        return ['status' => 0, 'message' => 'Exception Occurred', 'errors' => $errors];
    }
}
