<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\JobcategoryContract;
use App\Contracts\PositiontypeContract;
use App\Contracts\SalarytypeContract;
use App\Contracts\DayContract;
use App\Contracts\JobContract;
use App\Contracts\JobtypeContract;
use App\Contracts\UserContract;
use App\Contracts\LevelContract;
use App\Contracts\IndustryContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests\StoreJobRequest;

class JobController extends BaseController
{
    protected $jobcategoryRepo;
    protected $salarytypeRepo;
    protected $dayRepo;
    protected $jobRepo;
    protected $userRepo;
    protected $jobtypeRepo;
    protected $levelRepo;
    protected $industryRepo;
    protected $positiontypeRepo;

    public function __construct(
        JobcategoryContract $jobcategoryRepo,
        SalarytypeContract $salarytypeRepo,
        DayContract $dayRepo,
        JobContract $jobRepo,
        UserContract $userRepo,
        JobtypeContract $jobtypeRepo,
        LevelContract $levelRepo,
        IndustryContract $industryRepo,
        PositiontypeContract $positiontypeRepo
    )
    {
        $this->jobcategoryRepo = $jobcategoryRepo;
        $this->salarytypeRepo = $salarytypeRepo;
        $this->dayRepo = $dayRepo;
        $this->jobRepo = $jobRepo;
        $this->userRepo = $userRepo;
        $this->jobtypeRepo = $jobtypeRepo;
        $this->levelRepo = $levelRepo;
        $this->industryRepo = $industryRepo;
        $this->positiontypeRepo = $positiontypeRepo;
    }

    public function index()
    {
        $list = $this->jobRepo->list('order', 'desc');
        $jobs = $this->jobRepo->paginateArrayResults($list->all(), 15);

        $this->setPageTitle('Jobs', 'Jobs List');
        return view('admin.job.index', compact('jobs'));
    }

    public function create()
    {
        $jobcategories = $this->jobcategoryRepo->list('name', 'asc');
        $salarytypes = $this->salarytypeRepo->list('name', 'asc');
        $days = $this->dayRepo->list();
        $jobtypes = $this->jobtypeRepo->list();
        $users = $this->userRepo->list();
        $levels = $this->levelRepo->list();
        $industries = $this->industryRepo->list();
        $positiontypes = $this->positiontypeRepo->list();

        $this->setPageTitle('Jobs', 'Create Job');
        return view('admin.job.create', compact('jobcategories','salarytypes','users','levels','jobtypes','days','industries','positiontypes'));
    }

    public function store(StoreJobRequest $request)
    {
        $job = $this->jobRepo->store($request);

        if (!$job) {
            return $this->responseRedirectBack('Error occurred while creating job.', 'error', true, true);
        }
        return $this->responseRedirect('job.index', 'job added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $job = $this->jobRepo->findById($id);
        $jobcategories = $this->jobcategoryRepo->list('name', 'asc');
        $salarytypes = $this->salarytypeRepo->list('name', 'asc');
        $days = $this->dayRepo->list();
        $jobtypes = $this->jobtypeRepo->list();
        $users = $this->userRepo->list();
        $levels = $this->levelRepo->list();
        $industries = $this->industryRepo->list();
        $positiontypes = $this->positiontypeRepo->list();

        $this->setPageTitle('jobs', 'Edit job');
        return view('admin.job.edit', compact('jobcategories','salarytypes','users','levels','jobtypes','days','industries','positiontypes','job'));
    }

    public function update(StoreJobRequest $request, $id)
    {
        $job = $this->jobRepo->edit($request, $id);

        if (!$job) {
            return $this->responseRedirectBack('Error occurred while updating job.', 'error', true, true);
        }
        return $this->responseRedirect('job.index', 'job updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->jobRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('job.index', 'Record deleted successfully' ,'success',false, false);
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('jobs');
        foreach($allArticles as $_article){
            $article = $this->jobRepo->findById($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
