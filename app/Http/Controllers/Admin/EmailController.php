<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\EmailContract;
use App\Http\Controllers\BaseController;

class EmailController extends BaseController
{
    private $page="admin.email.";

    protected $emailRepo;

    public function __construct(
        EmailContract $emailRepo
    )
    {
        $this->emailRepo = $emailRepo;
    }

    public function index()
    {
        $list = $this->emailRepo->list('created_at', 'desc');
        $datas = $this->emailRepo->paginateArrayResults($list->all(), 15);
        $this->setPageTitle('email', 'List of email');
        return view($this->page.'index',compact('datas'));
    }

    public function create()
    {
        $this->setPageTitle('email', 'Create email');
        return view($this->page.'create');
    }

    public function store(Request $request)
    {
        $data = $this->emailRepo->store($request);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while creating record.', 'error', true, true);
        }
        return $this->responseRedirect('email.index', 'Record added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $data = $this->emailRepo->findById($id);
        $this->setPageTitle('email', 'Edit email');
        return view($this->page.'edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->emailRepo->edit($request, $id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while updating record.', 'error', true, true);
        }
        return $this->responseRedirect('email.index', 'Record updated successfully' ,'success',false, false);
    }

    public function destroy($id)
    {
        $data = $this->emailRepo->destroy($id);

        if (!$data) {
            return $this->responseRedirectBack('Error occurred while deleting record.', 'error', true, true);
        }
        return $this->responseRedirect('email.index', 'Record deleted successfully' ,'success',false, false);
    }
}
