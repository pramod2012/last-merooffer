<?php

namespace App\Contracts;

use App\Models\Order;
use App\Product;
use Illuminate\Support\Collection;
use App\Jsdecena\Baserepo\BaseRepositoryInterface;

/**;
 * Interface AddressContract
 * @package App\Contracts
 */
interface OrderContract extends BaseRepositoryInterface
{
    public function createOrder(array $params) : Order;

    public function updateOrder(array $params) : bool;

    public function findOrderById(int $id) : Order;

    public function listOrders(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Collection;

    public function findProducts(Order $order) : Collection;

    public function associateProduct(Product $product, int $quantity = 1, array $data = []);

    public function transform();

    public function listOrderedProducts() : Collection;

    public function buildOrderDetails(Collection $items);

    public function getAddresses() : Collection;

    public function getCouriers() : Collection;
}
