<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use App\Models\Attribute;

class Category extends Model
{
    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'status',
        'order',
        'image',
        'parent_id',
        'desc',
        'featured',
        'slug',
        'menu',
        'color',
        'font',
        'meta_title',
        'meta_keywords',
        'meta_body'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'feature'  =>  'boolean'
    ];

    /**
     * @param $value
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        if (static::whereSlug($slug = str_slug($value))->exists())
        {
            $slug = $this->incrementSlug($slug);
        }

        $this->attributes['slug'] = $slug;
    }

    public function incrementSlug($slug)
    {
        $original = $slug;
        $count = 2;
        while (static::whereSlug($slug)->exists()) 
        {
           $slug = $count++ . "-{$original}";
        }
        return $slug;
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->order = $model->max('order') + 1;
        });
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id')->where('status',1);
    }

    public function products(){
        return $this->belongsToMany(Product::class,'product_categories')->where('status',1);
    }

    public function assuredProducts(){
        return $this->belongsToMany(Product::class,'product_categories')->where('status',1)->where('assured_id','<>',1);
    }

    public function featuredProducts(){
        return $this->belongsToMany(Product::class,'product_categories')->where('status',1)->where('featured',1);
    }

    public function latestProducts(){
        return $this->belongsToMany(Product::class,'product_categories')->where('status',1)->orderBy('created_at','desc');
    }

    public function popularProducts(){
        return $this->belongsToMany(Product::class,'product_categories')->where('status',1)->orderBy('views', 'desc');
    }

    public function randomProducts(){
        return $this->belongsToMany(Product::class,'product_categories')->where('status',1)->inRandomOrder();
    }

    public function attributes(){
        return $this->belongsToMany(Attribute::class,'attribute_categories')->where('has_combin',0);
    }

    public function combinations(){
        return $this->belongsToMany(Attribute::class,'attribute_categories')->where('has_combin',1);
    }

    public function pricelabels(){
        return $this->belongsToMany(Pricelabel::class,'category_pricelabel');
    }

    public function getProductCount(){
        $count=0;
        $subs=$this->children()->get();
        // dd($subs);
        foreach($subs as $sub){
                $i=$sub->products->count();
                $count+=$i;
        }
        return $count;
    }
}
