@extends('admin.author.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Author</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('author.store')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <label for="txtname" class="col-md-4 control-label">Full Name * </label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtname" class="col-md-4 control-label">Image </label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control" name="image">

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtusername" class="col-md-4 control-label">Username *</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}">

                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtphone" class="col-md-4 control-label">Phone Number </label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}">

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtremarks" class="col-md-4 control-label">Remarks </label>

                            <div class="col-md-6">
                                <textarea id="remarks" type="text" class="form-control" name="remarks" value="{{ old('remarks') }}"></textarea>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtname" class="col-md-4 control-label">Email *</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">
                         
                            </div>
                        </div>



                <div class="form-group">
                            <div class="col-md-12">
                                  <div class="checkbox">
                                    <label>
                                        <input name="status" type="checkbox">
                                     <b> Status</b>
                                    </label>
                                  </div>
                              </div>
               </div>

                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
