<div class="form-group">
                        <label for="name">Meta Title <span class="text-danger">*</span></label>
                        <input type="text" name="meta_title" id="meta_title" placeholder="meta title" class="form-control" value="{{ old('meta_title',$data->meta_title) }}">
                    </div>
                    <div class="form-group">
                            <label for="description">Meta Description </label>
                            <textarea class="form-control" name="meta_body" id="meta_desc" rows="5" placeholder="170 characters with white space">{{ old('meta_body',$data->meta_body) }}</textarea>
                    </div>
                    <div class="form-group">
                            <label for="description">Meta Keywords </label>
                            <textarea class="form-control" name="meta_keywords" id="meta_keywords" rows="5" placeholder="Max 12 phrases separated by comma(,)">{{ old('meta_keywords',$data->meta_keywords) }}</textarea>
                    </div>