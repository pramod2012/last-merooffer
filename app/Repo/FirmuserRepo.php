<?php

namespace App\Repo;

use App\Firmuser;
use App\Contracts\FirmuserContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use App\Traits\UploadAble;
use Illuminate\Http\UploadedFile;
/**
 * Class EducationRepository
 *
 * @package \App\Repo
 */
class FirmuserRepo extends BaseRepo implements FirmuserContract
{
    use UploadAble;
    /**
     * EducationRepository constructor.
     * @param Education $model
     */
    public function __construct(Firmuser $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function list(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function store($request)
    {
        try {
            $data = new Firmuser();

            if ($request->has('logo')) {
                $logo = $this->uploadOne($request->logo, 'firmusers');
                $data->logo = $logo;
            }
            if ($request->has('img1')) {
                $img1 = $this->uploadOne($request->img1, 'firmusers');
                $data->img1 = $img1;
            }
            if ($request->has('img2')) {
                $img2 = $this->uploadOne($request->img2, 'firmusers');
                $data->img2 = $img2;
            }

            $data->name = $request->name;
            $data->desc = $request->desc;
            $data->email = $request->email;
            $data->show_phone = $request->show_phone;
            $data->show_email = $request->show_email;
            $data->url = $request->url;
            $data->phone = $request->phone;
            $data->user_id = $request->user_id;

            $data->insta = $request->insta;
            $data->fb = $request->fb;
            $data->twitter = $request->twitter;
            $data->youtube = $request->youtube;
            $data->linkedin = $request->linkedin;

            $data->save();

            return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }


    public function storeFirmuser($request)
    {
        try {
            $data = new Firmuser();

            if ($request->has('logo')) {
                $logo = $this->uploadOne($request->logo, 'firmusers');
                $data->logo = $logo;
            }
            if ($request->has('img1')) {
                $img1 = $this->uploadOne($request->img1, 'firmusers');
                $data->img1 = $img1;
            }
            if ($request->has('img2')) {
                $img2 = $this->uploadOne($request->img2, 'firmusers');
                $data->img2 = $img2;
            }

            $data->name = $request->name;
            $data->desc = $request->desc;
            $data->email = $request->email;
            $data->show_phone = $request->show_phone;
            $data->show_email = $request->show_email;
            $data->url = $request->url;
            $data->phone = $request->phone;
            $data->user_id = auth()->user()->id;

            $data->insta = $request->insta;
            $data->fb = $request->fb;
            $data->twitter = $request->twitter;
            $data->youtube = $request->youtube;
            $data->linkedin = $request->linkedin;

            $data->save();

            return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function edit($request, $id)
    {
    	try
    	{

        $data = Firmuser::findOrFail($id);

          if ($request->has('logo')) {
            if ($data->logo != null) {
                $this->deleteOne($data->logo);
            }
                $logo = $this->uploadOne($request->logo, 'firmusers');
                $data->logo = $logo;
            }

            if ($request->has('img1')) {
                if ($data->img1 != null) {
                $this->deleteOne($data->img1);
                }
                $img1 = $this->uploadOne($request->img1, 'firmusers');
                $data->img1 = $img1;
            }

            if ($request->has('img2')) {
                if ($data->img2 != null) {
                $this->deleteOne($data->img2);
            }
                $img2 = $this->uploadOne($request->img2, 'firmusers');
                $data->img2 = $img2;
            }

            $data->name = $request->name;
            $data->desc = $request->desc;
            $data->email = $request->email;
            $data->show_phone = $request->show_phone;
            $data->show_email = $request->show_email;
            $data->url = $request->url;
            $data->phone = $request->phone;
            $data->user_id = $request->user_id;

            $data->insta = $request->insta;
            $data->fb = $request->fb;
            $data->twitter = $request->twitter;
            $data->youtube = $request->youtube;
            $data->linkedin = $request->linkedin;

        $data->update();

        return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    public function editFirmuser($request, $id)
    {
        try
        {
        $data = Firmuser::where('user_id',auth()->user()->id)->findOrFail($id);

        if ($request->has('logo')) {
            if ($data->logo != null) {
                $this->deleteOne($data->logo);
            }
                $logo = $this->uploadOne($request->logo, 'firmusers');
                $data->logo = $logo;
            }

            if ($request->has('img1')) {
                if ($data->img1 != null) {
                $this->deleteOne($data->img1);
                }
                $img1 = $this->uploadOne($request->img1, 'firmusers');
                $data->img1 = $img1;
            }

            if ($request->has('img2')) {
                if ($data->img2 != null) {
                $this->deleteOne($data->img2);
            }
                $img2 = $this->uploadOne($request->img2, 'firmusers');
                $data->img2 = $img2;
            }

            $data->name = $request->name;
            $data->desc = $request->desc;
            $data->email = $request->email;
            $data->show_phone = $request->show_phone;
            $data->show_email = $request->show_email;
            $data->url = $request->url;
            $data->phone = $request->phone;
            $data->user_id = auth()->user()->id;

            $data->insta = $request->insta;
            $data->fb = $request->fb;
            $data->twitter = $request->twitter;
            $data->youtube = $request->youtube;
            $data->linkedin = $request->linkedin;

        $data->update();

        return $data;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        $data = $this->findById($id);
        if ($data->logo != null) {
            $this->deleteOne($data->logo);
        }
        if ($data->img1 != null) {
            $this->deleteOne($data->img1);
        }
        if ($data->img2 != null) {
            $this->deleteOne($data->img2);
        }
        $data->delete();
        return $data;
    }
}